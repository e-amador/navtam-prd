﻿
namespace NavCanada.Core.Domain.Model.Enums
{
    public enum NotamProposalStatusCode
    {
        Undefined = 0,

        // User Proposal status     
        Draft = 1,

        Submitted = 2,

        Withdrawn = 3,

        // NOF Proposal status
        Picked = 10,

        Parked = 11,

        Disseminated = 12,

        Rejected = 13,

        SoontoExpire = 14,

        Expired = 15,

        ParkedDraft = 16,

        Terminated = 17,

        DisseminatedModified = 18,

        ParkedPicked = 19,

        //Everybody
        Cancelled = 20,

        Replaced = 21,

        Deleted = 22,

        Discarded = 23,

        Taken = 255,

        // Not in use
        Queued = 100,
     
        Pending = 101,
     
        ModifiedAndSubmitted = 102,
     
        WithdrawnPending = 103,
     
        QueuePending = 104,
     
        Ended = 105,
     
        Enforced = 106,
     
        ModifiedEnforced = 108,
     
        ModifiedCancelled = 109,

        JustForTestingPurposesNotToUseInProduction = 1000, //this status was added in order to unit test the code
    }
}
