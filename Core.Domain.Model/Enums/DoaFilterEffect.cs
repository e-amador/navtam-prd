﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum DoaFilterEffect
    {
        Include,
        Exclude
    }
}
