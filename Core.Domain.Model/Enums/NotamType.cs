﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum NotamType
    {
        N = 0,
        R = 1,
        C = 2,
    }
}
