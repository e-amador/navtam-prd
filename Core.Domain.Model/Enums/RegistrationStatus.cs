﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum RegistrationStatus
    {
        Initial = 0,
        Completed = 1,
        Rejected = 2,
        ConfirmEmail = 3,
        ForgotPassword = 4
    }
}
