﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum SdoEntityType : byte
    {
        Fir,
        Aerodrome,
        Runway,
        Taxiway,
        Service,
        RunwayDirection,
        RunwayDirectionDeclaredDistance
    }
}
