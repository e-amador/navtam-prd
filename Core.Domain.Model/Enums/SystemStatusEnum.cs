﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum SystemStatusEnum : byte
    {
        /// <summary>
        /// The system is up and running OK.
        /// </summary>
        Ok,
        /// <summary>
        /// The NOTAM Distribution System is down (NAVCan HUB connection problems)
        /// </summary>
        NDSDown,
        /// <summary>
        /// An unknow error occurred on the system and the Administrator MUST look into it.
        /// </summary>
        CriticalError
    }
}
