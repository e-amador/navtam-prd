﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum UserLanguage
    {
        En,
        Fr
    }
}
