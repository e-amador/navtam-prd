﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum NotificationType
    {
        Email = 0,
        Sms = 1
    }
}
