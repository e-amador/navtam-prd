﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum PointType
    {
        Lat,
        Lon
    }
}
