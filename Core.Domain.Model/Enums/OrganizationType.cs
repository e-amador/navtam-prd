﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum OrganizationType : byte
    {
        External,
        Fic,
        Nof,
        Administration
    }
}
