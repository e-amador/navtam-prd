﻿

namespace NavCanada.Core.Domain.Model.Enums
{
    public enum AggregatePeriod
    {
        Yearly = 0,
        Monthly = 1,
        Weekly = 2,
        Hourly = 3,
        Daily = 4
    }
}
