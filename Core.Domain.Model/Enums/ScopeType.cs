﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum ScopeType
    {
        A,
        E,
        W,
        AE,
        AW,
        K,
    }
}
