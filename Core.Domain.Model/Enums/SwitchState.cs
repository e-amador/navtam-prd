﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum SwitchState : byte
    {
        Unknown,
        On,
        Off
    }
}
