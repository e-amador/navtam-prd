﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum DisseminationJobStatus : byte
    {
        Pending,
        InProcess,
        Paused,
        Completed,
        Canceled,
        Failed
    }
}
