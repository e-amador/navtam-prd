﻿namespace NavCanada.Core.Domain.Model.Enums
{
   public enum TickType
    {
        Start = 1,
        KeepAlive = 100,
        Stop = 0,
    }
}
