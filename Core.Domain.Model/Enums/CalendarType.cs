﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum CalendarType
    {
        Daily,
        Weekly,
        Date
    }
}