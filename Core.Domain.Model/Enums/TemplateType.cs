﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum TemplateType
    {
        /// <remarks/>
        SoonToExpire = 0,
        Rejected = 1,
        UserApplicationRejected = 2,
        UserApplicationApproved = 3,
        UserApplicationSubmitted = 4,
        Expired = 5,
        Ended = 6,
        UserApplicationEmailConfirmation = 7,
        ResetPasswordRequest = 8
    }
}
