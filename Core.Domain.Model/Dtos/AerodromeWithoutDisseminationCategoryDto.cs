﻿using System;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class AerodromeWithoutDisseminationCategoryDto
    {
        public string Id { get; set; }

        public string Mid { get; set; }

        public string Designator { get; set; }

        public string Name { get; set; }

        public string Fir { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string ServedCity { get; set; }
    }
}
