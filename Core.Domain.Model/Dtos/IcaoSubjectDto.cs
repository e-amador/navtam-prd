﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class IcaoSubjectDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string NameFrench { get; set; }

        public string EntityCode { get; set; }

        public string Code { get; set; }

        public string Scope { get; set; }

        public bool RequiresItemA { get; set; }

        public bool RequiresScope { get; set; }

        public bool RequiresSeries { get; set; }
    }
}
