﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class GeoRegionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
