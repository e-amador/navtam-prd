﻿using System.Configuration;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class MapConfigDto
    {
        public MapConfigDto()
        {
            DarkMatter = ConfigurationManager.AppSettings["DarkMatter"];
            KlokantechBasic = ConfigurationManager.AppSettings["KlokantechBasic"];
            OsmBright = ConfigurationManager.AppSettings["OsmBright"];
            MapAttribution = "&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors | not for operational use";
            MapHealthApi = ConfigurationManager.AppSettings["MapHealthApi"];
        }
        public string DarkMatter { get; set; }
        public string KlokantechBasic { get; set; }
        public string OsmBright { get; set; }
        public string MapAttribution { get; set; }
        public string MapHealthApi { get; set; }
    }
}