﻿using System;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class StatsPartialDto
    {
        public DateTime StartPeriod { get; set; }
        public DateTime EndPeriod { get; set; }
        public int HourPerid { get; set; }
        public string Series { get; set; }
        public DateTime Published { get; set; }
        public int SeriesCount { get; set; }
    }

    public class NotamStatsDto
    {
        public string StartPeriod { get; set; }
        public string EndPeriod { get; set; }
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }
        public int D { get; set; }
        public int E { get; set; }
        public int F { get; set; }
        public int G { get; set; }
        public int H { get; set; }
        public int I { get; set; }
        public int J { get; set; }
        public int K { get; set; }
        public int L { get; set; }
        public int M { get; set; }
        public int N { get; set; }
        public int O { get; set; }
        public int P { get; set; }
        public int Q { get; set; }
        public int R { get; set; }
        public int S { get; set; }
        public int T { get; set; }
        public int U { get; set; }
        public int V { get; set; }
        public int W { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public int Total { get; set; }
    }
}
