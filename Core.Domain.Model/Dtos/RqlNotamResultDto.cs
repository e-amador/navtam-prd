﻿using System.Collections.Generic;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class RqlNotamResultDto // >> TODO: Rename to YearActiveNumbersDto
    {
        public int Year { get; set; }
        public List<int> Numbers { get; set; }
    }
}
