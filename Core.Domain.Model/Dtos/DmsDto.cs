﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    using Enums;

    public struct DmsDto
    {
        public double Degrees { get; set; }
        public double Minutes { get; set; }
        public double Seconds { get; set; }
        public PointType Type { get; set; }
    }
}
