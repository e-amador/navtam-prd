﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class AerodromeDisseminationCategoryDto
    {
        public int Id { get; set; }

        public string AhpMid { get; set; }

        public string AhpCodeId { get; set; }

        public string AhpName { get; set; }

        public DisseminationCategory DisseminationCategory { get; set; }
        public string DisseminationCategoryName { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string ServedCity { get; set; }

    }
}
