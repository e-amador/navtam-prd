﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class OrgPartialDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
