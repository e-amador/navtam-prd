﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class SerieAllocationDto
    {
        public int Id { get; set; }
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public string Series { get; set; }
        public DisseminationCategory DisseminationCategory { get; set; }
        public string DisseminationCategoryName { get; set; }
        public string QCode { get; set; }
        public SeriesAllocationSubject Subject { get; set; }
    }
}
