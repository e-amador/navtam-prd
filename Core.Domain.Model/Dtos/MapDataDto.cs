﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    using System.Collections.Generic;

    public class MapDataDto
    {
        public MapDataDto()
        {
            Features = new List<FeatureDto>();
        }

        public string Error { get; set; }

        public string Name { get; set; }

        public List<FeatureDto> Features { get; set; }
    }
}
