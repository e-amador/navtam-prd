﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class NdsClientAddressDto
    {
        public int Id { get; set; }
        public string Client { get; set; }
        public string Address { get; set; }
        public NdsClientType ClientType { get; set; }
    }

    public class NdsClientIdAddressDto
    {
        public int Id { get; set; }
        public string Address { get; set; }
    }
}
