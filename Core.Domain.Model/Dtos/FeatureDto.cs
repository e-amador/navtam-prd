﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class FeatureDto
    {
        public string Type { get { return GetType().Name; } }
        public GeometryDto Geometry { get; set; }
        public object Properties { get; set; }
    }
}
