﻿using System;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class MessageExchangeTemplateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Addresses { get; set; }
        public DateTime Created { get; set; }
    }
}
