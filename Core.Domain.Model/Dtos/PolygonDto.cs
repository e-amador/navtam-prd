﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class PolygonDto : GeometryDto
    {
        public double[][][] Coordinates { get; set; }
    }
}
