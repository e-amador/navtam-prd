﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class ProposalStatusDto
    {
        public int Id { get; set; }
        public string NotamId { get; set;  }
        public string StartActivity { get; set; }
        public string EndValidity { get; set; }
        public string Received { get; set;  }
        public NotamProposalStatusCode Status { get; set; }
        public string GroupId { get; set; }
        public bool SoonToExpire { get; set; }
        public string ModifiedByUsr { get; set; }
        public string Originator { get; set; }
        public string Operator { get; set; }
        public string ItemE { get; set; }
        public string ItemA { get; set; }
        public string SiteId { get; set; }
        public bool Estimated { get; set; }
        public bool PendingReview { get; set; }
    }

}
