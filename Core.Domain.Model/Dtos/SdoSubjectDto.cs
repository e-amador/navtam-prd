﻿using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class SdoSubjectDto
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string SdoEntityName { get; set; }
        public string Designator { get; set; }
        public string Name { get; set; }
        public string Suffix { get; set; } 
        public string ServedCity { get; set; }
        public DbGeography SubjectGeo { get; set; }
        public DbGeography SubjectGeoRefPoint { get; set; }
        public string TwyType { get; set; }

    }
}
