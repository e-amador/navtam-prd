﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class GeometryDto
    {
        public string Type { get { return GetType().Name; } }
    }
}
