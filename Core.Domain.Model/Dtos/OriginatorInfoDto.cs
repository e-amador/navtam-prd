﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class OriginatorInfoDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int OrganizationId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
