﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class MultiLineStringDto : GeometryDto
    {
        public double[][][] Coordinates { get; set; }
    }
}
