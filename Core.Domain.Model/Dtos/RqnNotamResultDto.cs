﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class RqnNotamResultDto
    {
        public Notam Notam { get; set; }
        public string ReplaceNotamId { get; set; }
        public NotamType ReplaceNotamType { get; set; }
    }
}
