﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class LineStringDto : GeometryDto
    {
        public double[][] Coordinates { get; set; }
    }

}
