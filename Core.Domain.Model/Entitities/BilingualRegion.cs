﻿using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class BilingualRegion
    {
        public int Id { get; set; }

        public DbGeography TheRegion { get; set; }
    }
}