﻿using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NdsClient
    {
        public int Id { get; set; }
        public NdsClientType ClientType { get; set; }
        public string Client { get; set; }
        public string Address { get; set; }
        public string Operator { get; set; }
        public DateTime Updated { get; set; }
        public bool French { get; set; }
        public bool Active { get; set; }
        public DateTime? LastSynced { get; set; }
        public bool AllowQueries { get; set; }

        /// <summary>
        /// GeoRef region
        /// </summary>
        public DbGeography Region { get; set; }
        /// <summary>
        /// Lower limit (FL)
        /// </summary>
        public int LowerLimit { get; set; }
        /// <summary>
        /// Upper limit (FL)
        /// </summary>
        public int UpperLimit { get; set; }

        public virtual ICollection<NdsClientSeries> Series { get; set; }
        public virtual ICollection<NdsClientItemA> ItemsA { get; set; }
    }
}
