﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class Registration
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string EmailAddress { get; set; }

        public string Telephone { get; set; }

        public string TypeOfOperation { get; set; }

        public string HeadOffice { get; set; }

        public int Status { get; set; }

        public DateTime StatusTime { get; set; }

        public string RegisteredUserId { get; set; }

        public string RegisteredUserName { get; set; }

        public string RegisteredUserFirstName { get; set; }

        public string RegisteredUserLastName { get; set; }

        public string RegisteredUserEmail { get; set; }


        public virtual ICollection<UserProfile> Users { get; set; }
    }
}