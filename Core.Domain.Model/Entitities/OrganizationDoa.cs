﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class OrganizationDoa
    {
        public int OrganizationId { get; set; }
        public int DoaId { get; set; }
        public virtual Doa Doa { get; set; }
        public virtual Organization Organization { get; set; }
    }
}