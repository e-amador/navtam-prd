﻿using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class GeoRegion
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DbGeography Geo { get; set; }

        public virtual List<SeriesAllocation> Series { get; set; }

/*
        public GeoRegion()
        {
            Series = new List<SeriesAllocation>();
        }
*/
    }
}