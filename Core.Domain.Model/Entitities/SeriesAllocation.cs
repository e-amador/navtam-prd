﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class SeriesAllocation
    {
        public int Id { get; set; }
        public GeoRegion Region { get; set; }
        public int RegionId { get; set; }
        public string Series { get; set; }
        public DisseminationCategory DisseminationCategory { get; set; }
        public string QCode { get; set; }
        public SeriesAllocationSubject Subject { get; set; }
    }
}