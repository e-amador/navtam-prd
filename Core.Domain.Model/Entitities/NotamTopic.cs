﻿using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NotamTopic
    {
        public Guid Id { get; set; }
        public Guid NotamId { get; set; }
        public Notam Notam { get; set; }
        public string Topic { get; set; }
        public DateTime DateSubmitted { get; set; }
    }
}
