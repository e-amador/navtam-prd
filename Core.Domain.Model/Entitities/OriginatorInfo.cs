﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class OriginatorInfo
    {
        public int Id { get; set; }
        public Organization Organization { get; set; }
        public int OrganizationId { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
