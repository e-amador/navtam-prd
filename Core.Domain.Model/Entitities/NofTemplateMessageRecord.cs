﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NofTemplateMessageRecord
    {
        public int Id { get; set; }
        public Guid TemplateId { get; set; }
        public MessageExchangeTemplate Template { get; set; }
        public string Username { get; set; }
        public int SlotNumber { get; set; }
    }
}
