﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class UserDoa
    {
        public string UserId { get; set; }
        public int DoaId { get; set; }
        public int OrganizationId { get; set; }
        public virtual Doa Doa { get; set; }
    }
}