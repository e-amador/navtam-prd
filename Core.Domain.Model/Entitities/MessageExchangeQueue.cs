﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public enum MessageExchangeType : byte
    {
        Notification = 0,
        Error = 1
    }

    public enum MessageExchangeStatus : byte
    {
        None = 0,
        Important,
        Draft,
        Parked,
        Deleted,
        Reply,
        AlreadyTaken = 200
    }


    public class MessageExchangeQueue
    {
        public Guid Id { get; set;  }
        public MessageExchangeType MessageType { get; set; }
        public NdsClientType ClientType { get; set; }
        public MessageExchangeStatus Status { get; set; }
        public string PriorityCode { get; set; }
        public string ClientAddress { get; set; }
        public string ClientName { get; set; }
        public string Recipients { get; set; }
        public string Body { get; set; }
        public bool Inbound { get; set; }
        public bool Read { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime? Delivered { get; set; }
        public string LastOwner { get; set; }
        public bool Locked { get; set; }
    }
}
