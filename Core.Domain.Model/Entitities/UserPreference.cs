﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class UserPreference
    {
        public int Id { get; set; }

        public int ExpiredRecordsDateFilter { get; set; }
    }
}