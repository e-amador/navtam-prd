﻿using System.Data.Entity.Spatial;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class AerodromeDisseminationCategory
    {
        public int Id { get; set; }

        public string AhpMid { get; set; }

        public string AhpCodeId { get; set; }

        public string AhpName { get; set; }

        public DisseminationCategory DisseminationCategory { get; set; }

        public DbGeography Location { get; set; }
    }
}