﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class AeroRDSCacheStatus
    {
        public int Id { get; set; }
        public DateTime? LastEffectiveDate { get; set; }
        public DateTime? LastActiveDate { get; set; }
        public DateTime? LastEffectiveUpdated { get; set; }
        public DateTime? LastActiveUpdated { get; set; }
    }
}
