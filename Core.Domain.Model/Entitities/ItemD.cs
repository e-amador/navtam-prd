﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ItemD
    {
        public int Id { get; set; }
        public string EventId { get; set; }
        public CalendarType CalendarType { get; set; }
        public Proposal Proposal { get; set; }
        public int ProposalId { get; set; }
        public int CalendarId { get; set; }
        public string Event { get; set; }
    }
}
