﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class DisseminationJob
    {
        public int Id { get; set; }
        public string ClientIds { get; set; }
        public TimeSpan Delay { get; set; }
        public DisseminationJobStatus Status { get; set; }
        public int LastNotam { get; set; }
        public DateTime? BreakDate { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool DisseminateXml { get; set; }
    }
}
