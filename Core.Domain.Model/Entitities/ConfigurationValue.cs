﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ConfigurationValue
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

        public string Category { get; set; }
    }
}