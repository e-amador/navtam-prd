﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NdsClientItemA
    {
        public int Id { get; set; }
        public int NdsClientId { get; set; }
        public NdsClient NdsClient { get; set; }
        public string ItemA { get; set; }
    }
}
