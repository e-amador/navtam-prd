﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class SeriesChecklist
    {
        public string Series { get; set; }
        public string Firs { get; set; }
        public string Coordinates { get; set; }
        public int Radius { get; set; }
    }
}
