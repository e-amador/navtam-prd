﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class Group
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}