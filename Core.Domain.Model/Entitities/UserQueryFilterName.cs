﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class UserQueryFilterName
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int SlotNumber { get; set; }
        public string FilterName { get; set; }
    }
}
