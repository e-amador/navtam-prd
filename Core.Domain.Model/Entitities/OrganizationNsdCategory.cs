﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class OrganizationNsdCategory
    {
        public int OrganizationId { get; set; }
        public int NsdCategoryId { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual NsdCategory NsdCategory { get; set; }
    }
}
