﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ScheduleTask
    {
        public string Id { get; set; }
        public bool IsProcessed { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public Guid RowVersion { get; set; }
    }
}