﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ProposalAttachment
    {
        public Guid Id { get; set; }

        public string UserId { get; set; } 

        public UserProfile User { get; set; }

        public DateTime CreationDate { get; set; }

        public string FileName { get; set; }

        public byte[] Attachment { get; set; }

        public int ProposalId { get; set; }

        public Proposal Proposal { get; set; }
    }
}