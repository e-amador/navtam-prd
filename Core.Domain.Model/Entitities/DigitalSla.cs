﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class DigitalSla
    {
        public int Id { get; set; }

        public float Version { get; set; }

        public string Content { get; set; }
    }
}