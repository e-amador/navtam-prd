﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class Feedback
    {
        public int Id { get; set; }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string EmailAddress { get; set; }
        public string Organization { get; set; }
        public string Department { get; set; }
        public string PhoneNumber { get; set; }
        public string Comment { get; set; }
    }
}