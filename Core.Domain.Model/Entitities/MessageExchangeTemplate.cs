﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class MessageExchangeTemplate
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public string Addresses { get; set; }
        public DateTime Created { get; set; }
    }
}
