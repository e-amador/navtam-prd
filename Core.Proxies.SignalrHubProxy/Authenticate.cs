﻿namespace NavCanada.Core.Proxies.SignalrHubProxy
{
    using System.Net;
    using System.Text;

    using log4net;

    public class Authenticate : IAuthenticate
    {
        private readonly string _username;
        private readonly string _password;
        private readonly string _endpointUrl;
        private static readonly ILog Log = LogManager.GetLogger(typeof(Authenticate));

        public Authenticate(string username, string password, string endpointUrl)
        {
            _username = username;
            _password = password;
            _endpointUrl = endpointUrl;
        }

        public void AuthenticateUser(out bool status, out Cookie authCookie)
        {
            status = false;
            authCookie = null;
            var request = WebRequest.Create(_endpointUrl) as HttpWebRequest;
            if (request != null)
            {
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CookieContainer = new CookieContainer();
                var authCredentials = string.Format("UserName={0}&Password={1}", _username, _password);
                byte[] bytes = Encoding.UTF8.GetBytes(authCredentials);
                request.ContentLength = bytes.Length;

                try
                {
                    using (var requestStream = request.GetRequestStream())
                        requestStream.Write(bytes, 0, bytes.Length);
                }

                catch
                {
                    Log.Error("Was not able to connect to notamwiz site");
                    return;
                }

                try
                {
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        Log.Info("In AuthenticateUser");

                        for (int i = 0; i < response.Cookies.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(response.Cookies[i].Value))
                                authCookie = response.Cookies[i];
                        }
                    }

                    if (authCookie != null)
                    {
                        Log.Info("In AuthenticateUser With successful login ");
                        status = true;
                    }
                }
                catch
                {
                    Log.Error("Was not able to AuthenticateUser");
                }
            }
        }
    }
}

