﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.TaskEngine.Triggers
{
    public class CheckExpireProposalSyncJob : ScheduleJobTriggerBase
    {
        protected override string GetScheduleTaskId() => ScheduleIdentity.ExpiredProposalScheduler;
        protected override MessageTransferObjectId GetSheduleTaskMessageId() => MessageTransferObjectId.ExpireNotamProposal;
        protected override TimeSpan MinExecuteSpan => TimeSpan.FromMinutes(3); 
    }
}

