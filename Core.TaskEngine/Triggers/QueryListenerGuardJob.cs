﻿using NavCanada.Core.TaskEngine.QueueListener;
using Quartz;

namespace NavCanada.Core.TaskEngine.Triggers
{
    public class QueryListenerGuardJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var listener = NdsQueueListener.Instance;
            if (listener != null)
            {
                if (!listener.IsStarting && !listener.IsListening)
                {
                    listener.StartListening();
                }
            }
        }
    }
}
