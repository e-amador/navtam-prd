﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace NavCanada.Core.TaskEngine.Triggers
{
    public class BroadcastNotamsToSubscribersJob : ScheduleJobTriggerBase
    {
        public override void Execute(IJobExecutionContext context)
        {
            DisseminationJob job = null;

            try
            {
                var scheduleContext = context.Scheduler.Context;
                var scheduleManager = context.Scheduler as IScheduleManager;

                using (var clusterStorage = ClusterStorage())
                {
                    var uow = clusterStorage.Uow;

                    Notam nextNotam = null;
                    int lastNotam = 0;
                    uow.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
                    try
                    {
                        job = uow.DisseminationJobRepo.GetLatestJob();
                        if (job != null)
                        {
                            var prevLastNotam = job.LastNotam;
                            nextNotam = ProcessNextNotam(clusterStorage, job);
                            lastNotam = job.LastNotam;
                            job.LastNotam = prevLastNotam; //restore the value to avoid a save commit.
                        }
                        uow.Commit();
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e);
                        uow.Rollback();

                        throw;
                    }

                    uow.BeginTransaction();
                    try
                    {
                        job = uow.DisseminationJobRepo.GetLatestJob();
                        if (nextNotam != null)
                        {
                            if (job.Status == DisseminationJobStatus.Pending || job.Status == DisseminationJobStatus.InProcess)
                            {
                                // if at least one client matched, queue the next job and leave
                                job.LastNotam = lastNotam;

                                uow.DisseminationJobRepo.Update(job);
                                uow.SaveChanges();

                                Thread.Sleep(job.Delay);

                                var mto = CreateBroadcastMto($"{job.Id}_{nextNotam.Id}");
                                clusterStorage.Queues.TaskEngineQueue.Publish(mto);
                            }
                        }
                        else
                        {
                            var mto = CreateBroadcastMto($"{job.Id}_{Guid.Empty}");
                            clusterStorage.Queues.TaskEngineQueue.Publish(mto);
                        }
                        uow.Commit();
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e);
                        uow.Rollback();

                        throw;
                    }

                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);

                //try to re-start process one more time
                try
                {
                    Thread.Sleep(2000);
                    using (var clusterStorage = ClusterStorage())
                    {
                        var uow = clusterStorage.Uow;

                        job = uow.DisseminationJobRepo.GetLatestJob();
                        if (job != null)
                        {
                            var mto = CreateBroadcastMto($"{job.Id}");
                            clusterStorage.Queues.TaskEngineQueue.Publish(mto);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex);
                    //too bad, so let it fail
                    throw;
                }
            }
        }

        protected override string GetScheduleTaskId() => ScheduleIdentity.BroadcastNotamsToSubscribers;
        protected override MessageTransferObjectId GetSheduleTaskMessageId() => MessageTransferObjectId.ExpireNotamProposal;
        protected override TimeSpan MinExecuteSpan => TimeSpan.FromMinutes(3);

        private Notam ProcessNextNotam(IClusterStorage clusterStorage, DisseminationJob job)
        {
            Notam nextNotam = null;

            var idSet = GetIdSet(job.ClientIds);
            var uow = clusterStorage.Uow;

            while (true)
            {
                // try to seek on the next notam to disseminate
                nextNotam = GetNextNotamToDisseminate(uow, job);
                if (nextNotam != null)
                {
                    job.LastNotam = nextNotam.SequenceNumber;
                    if (idSet.Count > 0)
                    {
                        if (job.DisseminateXml || HasAftnSubscriptions(nextNotam, uow, a => idSet.Contains(a.Id.ToString()), false) )
                        {
                            break;
                        }
                    }
                }
                else
                    break;
            }

            return nextNotam;
        }


        private Notam GetNextNotamToDisseminate(IUow uow, DisseminationJob job)
        {
            var refDate = job.BreakDate.HasValue ? job.BreakDate.Value : new DateTime(DateTime.UtcNow.Year, 1, 1);
            return uow.NotamRepo.GetNextBySequenceNumberAndReferenceDate(job.LastNotam, refDate);
        }

        public bool HasAftnSubscriptions(Notam notam, IUow uow, Predicate<NdsClientIdAddressDto> testAddress, bool onlyActive)
        {
            var addresses = new HashSet<string>();

            var itemA = GetNotamItemA(notam);

            // we don't want to send the checklist unless the subscription specify a series
            var allowAllSeries = !notam.SeriesChecklist;

            // get bilingual clients (english & french)
            var matchCount = uow.NdsClientRepo
                                .GetMatchingAftnAddresses(itemA, notam.Series, allowAllSeries, true, onlyActive)
                                .Where(a => testAddress(a))
                                .Select(a => a.Address)
                                .Distinct()
                                .Count();

            if (matchCount > 0) 
                return true;

            // get monolingual clients (english only)
            matchCount = uow.NdsClientRepo
                            .GetMatchingAftnAddresses(itemA, notam.Series, allowAllSeries, false, onlyActive)
                            .Where(a => testAddress(a) && !addresses.Contains(a.Address))
                            .Select(a => a.Address)
                            .Distinct()
                            .Count();

            if (matchCount > 0)
                return true;

            // get bilingual geo ref clients (english & french)
            matchCount = uow.NdsClientRepo
                            .GetMatchingAftnAddressesByGeoRef(notam.EffectArea, notam.LowerLimit, notam.UpperLimit, true, onlyActive)
                            .Where(a => testAddress(a) && !addresses.Contains(a.Address))
                            .Select(a => a.Address)
                            .Distinct()
                            .Count();

            if (matchCount > 0)
                return true;

            // get monolingual clients (english only)
            matchCount = uow.NdsClientRepo
                            .GetMatchingAftnAddressesByGeoRef(notam.EffectArea, notam.LowerLimit, notam.UpperLimit, false, onlyActive)
                            .Where(a => testAddress(a) && !addresses.Contains(a.Address))
                            .Select(a => a.Address)
                            .Distinct()
                            .Count();

            if (matchCount > 0)
                return true;

            return false;
        }

        private string GetNotamItemA(Notam notam)
        {
            if (notam.SeriesChecklist)
                return string.Empty;

            if ("CXXX".Equals(notam.ItemA))
                return ParseNotamItemA(notam.ItemE, notam.ItemA);

            return notam.ItemA;
        }

        private string ParseNotamItemA(string itemE, string defaultItemA)
        {
            var trimmedItemE = (itemE ?? "").Trim();
            return trimmedItemE.Length >= 4 ? trimmedItemE.Substring(0, 4) : defaultItemA;
        }

        private static HashSet<string> GetIdSet(string ids)
        {
            char[] IdSeparators = new char[] { ' ' };
            return new HashSet<string>((ids ?? "").Split(IdSeparators, StringSplitOptions.RemoveEmptyEntries));
        }

        private MessageTransferObject CreateBroadcastMto(string entityId)
        {
            var mto = new MessageTransferObject();

            // add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            // create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            var messageId = (int)MessageTransferObjectId.ProcessDisseminateJobMsg;

            // create inbound type
            mto.Add(MessageDefs.MessageIdKey, messageId, DataType.Int32);

            // add job id
            mto.Add(MessageDefs.EntityIdentifierKey, entityId, DataType.String);

            // date/time when the message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            return mto;
        }

    }
}

