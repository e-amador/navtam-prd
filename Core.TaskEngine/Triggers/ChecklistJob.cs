﻿using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Common.Common;
using System;

namespace NavCanada.Core.TaskEngine.Triggers
{
    internal class ChecklistJob : ScheduleJobTriggerBase
    {
        protected override string GetScheduleTaskId() => ScheduleIdentity.ChecklistScheduler;
        protected override MessageTransferObjectId GetSheduleTaskMessageId() => MessageTransferObjectId.DisseminateChecklistsMsg;
        protected override TimeSpan MinExecuteSpan => TimeSpan.FromHours(1);
    }
}