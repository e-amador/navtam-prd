﻿using System;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.TaskEngine.Triggers
{
    internal class HubConnectionTestJob : ScheduleJobTriggerBase
    {
        protected override string GetScheduleTaskId() => ScheduleIdentity.HubConnectionTestScheduler;

        protected override MessageTransferObjectId GetSheduleTaskMessageId() => MessageTransferObjectId.HubConnectionTestMsg;

        protected override TimeSpan MinExecuteSpan => TimeSpan.FromMinutes(5);
    }
}
