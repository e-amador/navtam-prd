﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using System;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class AftnBroadcastTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();
            var notamNumber = "?";

            try
            {
                logger.LogDebug(loggerType, "AftnBroadcastTask start");

                if (context.Mto[MessageDefs.EntityIdentifierKey] == null)
                {
                    logger.LogWarn(loggerType, "Missing entity identifier.");
                    return;
                }

                var obj = context.Mto[MessageDefs.EntityIdentifierKey].Value;
                Guid id;
                if (!Guid.TryParse(obj.ToString(), out id))
                {
                    var message = "AFTNBroadcastTask failed to deserialize Notam Id!";
                    logger.LogWarn(GetType(), message);
                    throw new InvalidOperationException(message);
                }

                var uow = context.ClusterStorage.Uow;
                var notam = uow.NotamRepo.GetById(id);

                if (notam == null)
                {
                    var message = $"AFTNBroadcastTask NOTAM {id} was NOT FOUND!";
                    logger.LogWarn(GetType(), message);
                    throw new InvalidOperationException(message);
                }

                SubmitForAFTNBroadcast(context.ExternalServices.SolaceEndpoint, uow, notam);

                logger.LogDebug(loggerType, $"AftnBroadcastTask done for NOTAM '{notamNumber}'");
            }
            catch (Exception e)
            {
                logger.LogError(e, loggerType, $"AftnBroadcastTask FAILED for NOTAM {notamNumber}");
                throw;
            }
        }

        private static void SubmitForAFTNBroadcast(ISolaceEndpoint solaceEndpoint, IUow uow, Notam notam)
        {
            var messagePublisher = new MessagePublisher(solaceEndpoint, uow);
            var distributionRelay = new DistributionRelay(uow, messagePublisher);
            distributionRelay.DistributeToAftn(notam, addr => true, true);
        }
    }
}