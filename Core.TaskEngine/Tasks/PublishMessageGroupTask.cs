﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using System;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class PublishMessageGroupTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            logger.LogDebug(loggerType, "PublishMessageGroupTask task start excecuting...");

            if (context.Mto[MessageDefs.EntityIdentifierKey] == null)
            {
                logger.LogWarn(loggerType, "PublishMessageGroupTask is missing entity identifier.", null);
                return;
            }

            var obj = context.Mto[MessageDefs.EntityIdentifierKey].Value;
            Guid messageId;
            if (!Guid.TryParse(obj.ToString(), out messageId))
                throw new InvalidOperationException("PublishMessageGroupTask task failed to deserialize messageId");

            SubmitForDissemination(context.ExternalServices.SolaceEndpoint, context.ClusterStorage.Uow, messageId);

            // report NDS system is up
            context.Mto.Add(MessageDefs.NDSUpTaskKey, true, DataType.Boolean);

            logger.LogDebug(loggerType, $"PublishMessageGroupTask: Task completed. GroupId#: {messageId}");
        }

        private static void SubmitForDissemination(ISolaceEndpoint solaceEndpoint, IUow uow, Guid messageId)
        {
            var message = uow.NdsOutgoingMessageRepo.GetById(messageId);
            var messagePublisher = new MessagePublisher(solaceEndpoint, uow);
            if (message != null)
            {
                if (message.NextId.HasValue)
                {
                    // queue next message
                    messagePublisher.QueueMessageGroupForPublishing(message.NextId.Value);
                }
                messagePublisher.PublishHubMessage(message);
            }
        }
    }
}