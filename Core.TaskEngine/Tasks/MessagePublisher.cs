﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using System;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class MessagePublisher : IMessagePublisher
    {
        public MessagePublisher(ISolaceEndpoint solaceEndpoint, IUow uow)
        {
            _solaceEndpoint = solaceEndpoint;
            _uow = uow;
        }

        public void PublishHubMessage(NdsOutgoingMessage message)
        {
            // update the message status
            message.SubmitDate = DateTime.UtcNow;
            message.Status = NdsMessageStatus.Sent;

            _uow.NdsOutgoingMessageRepo.Update(message);
            _uow.SaveChanges();

            // publish message to hub topic
            _solaceEndpoint.PostToTopic(message.Destination, message.Content, message.GetMessageProperties());
        }

        public void QueueNotamForDistribution(Guid notamId)
        {
            SubmitToServiceBrokerQueue(CreateMessageTransferObject(notamId, MessageTransferObjectId.DistributeAftnBroadcastMessage));
        }

        public void QueueMessageGroupForPublishing(Guid nextId)
        {
            SubmitToServiceBrokerQueue(CreateMessageTransferObject(nextId, MessageTransferObjectId.PublishMessageGroup));
        }

        private void SubmitToServiceBrokerQueue(MessageTransferObject mto)
        {
            var taskEngineQueue = new SqlServiceBrokerQueue(_uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            taskEngineQueue.Publish(mto);
        }

        private MessageTransferObject CreateMessageTransferObject(Guid relayMessageId, MessageTransferObjectId messageTypeId)
        {
            var mto = new MessageTransferObject();

            // add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            // create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            var messageId = (int)messageTypeId;

            // create inbound type
            mto.Add(MessageDefs.MessageIdKey, messageId, DataType.Int32);

            // add unique identifier
            mto.Add(MessageDefs.EntityIdentifierKey, relayMessageId, DataType.String);

            // date/time when the message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            return mto;
        }

        private readonly ISolaceEndpoint _solaceEndpoint;
        private readonly IUow _uow;
    }
}