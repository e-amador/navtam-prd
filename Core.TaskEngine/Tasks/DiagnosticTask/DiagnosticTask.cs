﻿namespace NavCanada.Core.TaskEngine.Tasks.DiagnosticTask
{
    using Domain.Model.Enums;
    using Contracts;
    using Mto;
    using System.Configuration;

    public class DiagnosticTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            //context.ExternalServices.Logger.LogDebug(GetType(), "DiagnosticTask: Task start excecuting...");

            var healthCheck = context.Mto[MessageDefs.DiagnosticType].Value.ToString();

            if(healthCheck == MessageDefs.TaskEngineHealthCheck)
            {
                CheckTaskEngineHealth(context);
            }
        }

        private void CheckTaskEngineHealth(IMessageTaskContext context)
        {
            var configValue = context.ClusterStorage.Uow.ConfigurationValueRepo.GetByName("Health-TE");
            configValue.Value = "1";
            configValue.Description = ConfigurationManager.AppSettings["TaskEngineInstanceName"];

            context.ClusterStorage.Uow.ConfigurationValueRepo.Update(configValue);
            context.ClusterStorage.Uow.SaveChanges();
        }
    }
}