﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using NavCanada.Core.TaskEngine.Helpers;
using Nds.Common.Exceptions;
using NDS.Relay;
using System;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class DisseminateNotamTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();
            var notamId = "?";
            Notam notam = null;
            try
            {
                if (context.Mto[MessageDefs.EntityIdentifierKey] == null)
                {
                    logger.LogWarn(loggerType, "DisseminateNotamTask is missing entity identifier.");
                    return;
                }

                var obj = context.Mto[MessageDefs.EntityIdentifierKey].Value;

                Guid id;
                if (!Guid.TryParse(obj.ToString(), out id))
                {
                    logger.LogWarn(loggerType, $"DisseminateNotamTask task failed to deserialize NotamPropsal Id: {obj}", obj);
                    return;
                }

                // save it for logging purposes
                notamId = id.ToString();

                var uow = context.ClusterStorage.Uow;
                notam = uow.NotamRepo.GetById(id);

                // this should never happen
                if (notam == null)
                    throw new Exception($"DisseminateNotamTask failed because Notam with Id: {id} was NOT FOUND!");

                // a CheckList NOTAM dissemination failure won't put the system down (error will be sent to Admin via email)
                if (notam.SeriesChecklist)
                {
                    context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);
                }

                logger.LogDebug(loggerType, $"Begin disseminating NOTAM: '{notam.NotamId}' ID: '{notam.Id}'");

                // execute dissemination
                SubmitForDissemination(context.ExternalServices.SolaceEndpoint, uow, notam);

                // report NDS system is up
                context.Mto.Add(MessageDefs.NDSUpTaskKey, true, DataType.Boolean);

                logger.LogDebug(loggerType, $"Dissemination completed for NOTAM: '{notam.NotamId}' ID: '{notam.Id}'");
            }
            catch (NdsCommunicationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, $"DisseminateNotamTask task on NOTAM {notam?.Summarize()} failed!", notamId);
                throw;
            }
        }

        private static void SubmitForDissemination(ISolaceEndpoint solaceEndpoint, IUow uow, Notam notam)
        {
            var messagePublisher = new MessagePublisher(solaceEndpoint, uow);
            var distributionRelay = new DistributionRelay(uow, messagePublisher);

            // disseminate and distribute to subscriptions
            distributionRelay.Disseminate(notam, true);
        }
    }
}