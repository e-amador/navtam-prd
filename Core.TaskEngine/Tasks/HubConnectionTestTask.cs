﻿using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using Nds.Common.Exceptions;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class HubConnectionTestTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var sytemStatus = context.ClusterStorage.Uow.SystemStatusRepo.GetStatus();
            var systemWasOk = sytemStatus?.Status == SystemStatusEnum.Ok;
            try
            {
                // test the connection to the hub
                TestHubConnection(context.ExternalServices.SolaceEndpoint);
                // report NDS system is up
                context.Mto.Add(MessageDefs.NDSUpTaskKey, true, DataType.Boolean);
            }
            catch (NdsCommunicationException)
            {
                // only propagate the error if the system was in OK status
                if (systemWasOk)
                    throw;
            }
        }

        private void TestHubConnection(ISolaceEndpoint solaceEndpoint)
        {
            solaceEndpoint.TestConnection();
        }
    }
}
