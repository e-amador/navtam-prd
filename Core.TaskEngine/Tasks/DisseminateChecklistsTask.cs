﻿using Business.Common;
using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using System;
using System.Data.Entity.Spatial;
using System.Diagnostics;
using System.Linq;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class DisseminateChecklistsTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = typeof(DisseminateChecklistsTask);

            // if this were to fail we don't want to put the system down
            context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

            try
            {
                logger.LogDebug(loggerType, "Disseminate CHECKLISTS Task task started");

                var sw = Stopwatch.StartNew();

                DisseminateChecklist(context);

                sw.Stop();

                logger.LogDebug(loggerType, $"Disseminate CHECKLISTS Task completed in {sw.Elapsed}");
            }
            catch (CheckListDbConfigException ex)
            {
                logger.LogError(ex, loggerType, "Failed to execute checklist dissemination task due to DB configuration issues", context.Mto);
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "Failed to execute checklist dissemination task");
                throw;
            }
        }

        private static void DisseminateChecklist(IMessageTaskContext context)
        {
            var uow = context.ClusterStorage.Uow;

            var header = "CHECKLIST";
            var footer = GetCheckListPublicationFooter(uow);

            var series = uow.SeriesChecklistRepo.GetAvailableSeries();
            foreach (var s in series)
            {
                var list = uow.NotamRepo.GetActiveNotamNumbersBySeries(s, false).ToList();
                var itemE = RelayUtils.CreateChecklistMessage(list, header, footer);
                DisseminateSeriesChecklist(uow, s, itemE);
            }
        }

        private static string GetCheckListPublicationFooter(IUow uow)
        {
            var footerMessage = uow.ConfigurationValueRepo.GetByName("CheckListPubMessage")?.Value ?? string.Empty;
            return $"LATEST PUBLICATIONS\r\n{footerMessage}";
        }

        private static void DisseminateSeriesChecklist(IUow uow, string series, string itemE)
        {
            var notamId = CreateChecklistNotam(uow, series, itemE);
            QueueDisseminateNotamMessage(uow, notamId);
        }

        private const int MaxSeriesNumber = 9999;

        private static Guid CreateChecklistNotam(IUow uow, string series, string itemE)
        {
            var year = DateTime.UtcNow.Year;

            // pick up the series
            var seriesNumber = uow.SeriesNumberRepo.GetSeriesNumber(series[0], year);
            if (seriesNumber == null)
                throw new CheckListDbConfigException($"Series {series} has not been initialized for year {year}!");

            var number = Math.Max(1, seriesNumber.Number);

            var checklist = uow.NotamRepo.FindActiveSeriesChecklist(series);
            var notamId = ReplaceChecklist(uow, checklist, series, year, number, itemE);

            // update the series number
            seriesNumber.Number = number < MaxSeriesNumber ? number + 1 : 1;
            uow.SeriesNumberRepo.SafeUpdateSeriesNumber(seriesNumber);

            return notamId;
        }

        private static Guid ReplaceChecklist(IUow uow, Notam prevChecklist, string series, int year, int number, string itemE)
        {
            Proposal proposal;
            if (prevChecklist != null)
            {
                proposal = uow.ProposalRepo.GetById(prevChecklist.ProposalId);
                if (proposal == null)
                    throw new CheckListDbConfigException($"Cannot find checklist proposal id: {prevChecklist.ProposalId}");

                UpdateChecklistProposal(uow, proposal, year, number, itemE);
            }
            else
            {
                proposal = CreateChecklistProposal(uow, series, year, number, itemE);
            }

            return CreateNotamFromProposal(uow, proposal, prevChecklist);
        }

        private static void UpdateActivePeriod(Proposal proposal)
        {
            proposal.Estimated = true;
            proposal.StartActivity = DateTime.UtcNow.RemoveSeconds();
            proposal.EndValidity = DateTime.UtcNow.AddMonths(1).RemoveSeconds();
            proposal.Received = DateTime.UtcNow.RemoveSeconds();
        }

        private static Guid CreateNotamFromProposal(IUow uow, Proposal proposal, Notam replaceNotam)
        {
            var toDisseminate = new Notam();
            proposal.CopyPropertiesTo(toDisseminate);

            toDisseminate.Id = Guid.NewGuid();
            toDisseminate.ProposalId = proposal.Id;
            toDisseminate.Type = proposal.ProposalType;

            toDisseminate.Coordinates = proposal.Coordinates;
            toDisseminate.EffectArea = CalculateEffectArea(proposal.ItemA, uow);
            toDisseminate.Published = DateTime.UtcNow.RemoveSeconds();

            if (replaceNotam != null)
            {
                toDisseminate.ReferredNotamId = ICAOTextUtils.FormatNotamId(replaceNotam.Series, replaceNotam.Number, replaceNotam.Year);
                toDisseminate.RootId = replaceNotam.Id;

                replaceNotam.ReplaceNotamId = toDisseminate.Id;
                replaceNotam.EffectiveEndValidity = proposal.StartActivity;

                uow.NotamRepo.Update(replaceNotam);
            }
            else
            {
                toDisseminate.RootId = toDisseminate.Id;
            }

            // save the Icao Text in the NOTAM
            toDisseminate.IcaoText = RelayUtils.GetAftnMessageTextFromNotam(toDisseminate, string.Empty, true);

            // roll notam number
            var toRoll = uow.NotamRepo.GetReferredNotam(toDisseminate.Series, toDisseminate.Number, toDisseminate.Year);
            if (toRoll != null)
            {
                toRoll.NumberRolled = true;
                uow.NotamRepo.Update(toRoll);
            }

            uow.NotamRepo.Add(toDisseminate);

            uow.SaveChanges();

            return toDisseminate.Id;
        }

        private static DbGeography CalculateEffectArea(string itemA, IUow uow)
        {
            DbGeography geo = null;
            foreach (var d in SplitFirs(itemA))
            {
                var firGeo = uow.SdoCacheRepo.GetFirGeography(d);
                geo = geo != null ? geo.Union(firGeo) : firGeo;
            }
            return geo;
        }

        static string[] SplitFirs(string itemA) => (itemA ?? "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        private static Proposal CreateChecklistProposal(IUow uow, string series, int year, int number, string itemE)
        {
            var proposal = new Proposal()
            {
                Name = "Checklist",
                Fir = "CZXX",
                Code23 = "KK",
                Code45 = "KK",
                Traffic = "K",
                Purpose = "K",
                Scope = "K",
                LowerLimit = 0,
                UpperLimit = 999,
                Nof = "CYHQ",

                ProposalType = NotamType.N,
                Status = NotamProposalStatusCode.Disseminated,

                Operator = CommonDefinitions.NofUser,
                Originator = CommonDefinitions.NofUser,

                Series = series,
                Number = number,
                Year = year,
                NotamId = ICAOTextUtils.FormatNotamId(series, number, year),

                ItemE = itemE,

                SeriesChecklist = true
            };

            #region Proposal User

            var user = uow.UserProfileRepo.GetByUserName(CommonDefinitions.BackendUser);
            if (user == null)
                throw new CheckListDbConfigException("Cannot find the Backend User in the DB!");

            proposal.UserId = user.Id;
            proposal.User = user;
            proposal.OrganizationId = user.OrganizationId ?? 0;
            proposal.ModifiedByUsr = CommonDefinitions.BackendUser;
            proposal.ModifiedByOrg = CommonDefinitions.BackendUser;

            #endregion Proposal User

            #region Proposal Category

            var checklistCategory = uow.NsdCategoryRepo.GetByName("CHECKLIST");
            if (checklistCategory == null)
                throw new CheckListDbConfigException("Cannot find the CHECKLIST category in the DB!");

            proposal.CategoryId = checklistCategory.Id;
            proposal.Category = checklistCategory;

            #endregion Proposal Category

            #region Icao Subject

            var icaoSubject = uow.IcaoSubjectRepo.GetByCode("KK");
            if (icaoSubject == null)
                throw new CheckListDbConfigException("Cannot find the CHECKLIST ICAO Subject in the DB!");

            proposal.IcaoSubjectId = icaoSubject.Id;
            proposal.IcaoSubject = icaoSubject;

            #endregion Icao Subject

            #region Icao Subject Condition

            var icaoCondition = uow.IcaoSubjectConditionRepo.GetByCode(icaoSubject.Id, "KK");
            if (icaoCondition == null)
                throw new CheckListDbConfigException("Cannot find the CHECKLIST ICAO Subject Condition in the DB!");

            proposal.IcaoConditionId = icaoCondition.Id;
            proposal.IcaoSubjectCondition = icaoCondition;

            #endregion Icao Subject Condition

            #region itemA, Location and Radius

            UpdateSeriesConfiguredValues(uow, proposal);

            #endregion itemA, Location and Radius

            UpdateActivePeriod(proposal);

            uow.ProposalRepo.Add(proposal);
            UpdateProposalHistory(uow, proposal);

            uow.SaveChanges();

            return proposal;
        }

        private static void UpdateChecklistProposal(IUow uow, Proposal proposal, int year, int number, string itemE)
        {
            proposal.ProposalType = NotamType.R;

            proposal.ReferredSeries = proposal.Series;
            proposal.ReferredYear = proposal.Year;
            proposal.ReferredNumber = proposal.Number;

            proposal.Year = year;
            proposal.Number = number;
            proposal.ItemE = itemE;
            proposal.NotamId = ICAOTextUtils.FormatNotamId(proposal.Series, proposal.Number, proposal.Year);

            UpdateSeriesConfiguredValues(uow, proposal);

            UpdateActivePeriod(proposal);

            uow.ProposalRepo.Update(proposal);
            UpdateProposalHistory(uow, proposal);

            uow.SaveChanges();
        }

        private static void UpdateSeriesConfiguredValues(IUow uow, Proposal proposal)
        {
            var seriesChecklist = uow.SeriesChecklistRepo.GetById(proposal.Series);
            if (seriesChecklist == null)
                throw new CheckListDbConfigException($"Cannot find the Series ({proposal.Series}) Checklist config in the DB!");

            // itemA
            proposal.ItemA = seriesChecklist.Firs;

            var dmsLocation = DMSLocation.FromDMS(seriesChecklist.Coordinates);
            if (dmsLocation == null)
                throw new CheckListDbConfigException($"Cannot parse Series ({proposal.Series}) DMS coordinates found in the DB!");

            // location
            proposal.Coordinates = seriesChecklist.Coordinates;
            proposal.ExactCoordinates = proposal.Coordinates;
            proposal.Location = DbGeography.FromText(dmsLocation.GeoPointText, DbGeography.DefaultCoordinateSystemId);

            // radius
            proposal.Radius = seriesChecklist.Radius;
        }

        private static void UpdateProposalHistory(IUow uow, Proposal proposal)
        {
            var proposalHistory = new ProposalHistory();

            var id = proposalHistory.Id;
            proposal.CopyPropertiesTo(proposalHistory);

            proposalHistory.Id = id;
            proposalHistory.ProposalId = proposal.Id;
            proposalHistory.Proposal = proposal;

            uow.ProposalHistoryRepo.Add(proposalHistory);
        }

        private static void QueueDisseminateNotamMessage(IUow uow, Guid notamId)
        {
            var mto = MtoHelper.CreateMtoMessageWithKeyValue(MessageTransferObjectId.DisseminateNotam, notamId);
            SubmitToServiceBrokerQueue(uow, mto);
        }

        private static void SubmitToServiceBrokerQueue(IUow uow, MessageTransferObject mto)
        {
            var taskEngineQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            taskEngineQueue.Publish(mto);
        }
    }

    public class CheckListDbConfigException : Exception
    {
        public CheckListDbConfigException(string message) : base(message)
        {
        }
    }
}