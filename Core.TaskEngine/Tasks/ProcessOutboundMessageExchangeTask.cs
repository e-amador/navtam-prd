﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using System;
using System.Linq;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class ProcessOutboundMessageExchangeTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            try
            {
                // if this were to fail we don't want to put the system down
                context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

                logger.LogDebug(loggerType, "ProcessOutboundMessageExchangeTask task start excecuting...");

                if (context.Mto[MessageDefs.EntityIdentifierKey] == null)
                {
                    context.ExternalServices.Logger.LogWarn(typeof(ProcessOutboundMessageExchangeTask), "Missing entity identifier...", null);
                    return;
                }

                var obj = context.Mto[MessageDefs.EntityIdentifierKey].Value;
                Guid id;
                if (!Guid.TryParse(obj.ToString(), out id))
                    throw new InvalidOperationException("ProcessOutboundMessageExchangeTask task failed to deserialize message Id");

                var uow = context.ClusterStorage.Uow;
                var message = uow.MessageExchangeQueueRepo.GetById(id);

                if (message == null)
                    throw new Exception($"ProcessOutboundMessageExchangeTask could not read Messsage with ID: {id}");

                ProccesExchangeMessage(message, context);

                logger.LogDebug(loggerType, $"ProcessOutboundMessageExchangeTask: Task completed.");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "Error with ProcessOutboundMessageExchangeTask");
                throw;
            }
        }

        private static void ProccesExchangeMessage(MessageExchangeQueue message, IMessageTaskContext context)
        {
            var uow = context.ClusterStorage.Uow;
            var queue = context.ClusterStorage.Queues.TaskEngineQueue;
            var groupId = Guid.NewGuid();

            // split the receipient addresses on the space char
            var addresses = (message.Recipients ?? "").Split(new char[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (addresses.Length == 0)
                throw new Exception($"Message with id: {message.Id} contains no recipients addresses.");

            // get aften message(s) from the text
            var ndsMessages = RelayUtils.GetAftnExchangeMessages(groupId, message.ClientAddress, addresses, message.Body, message.PriorityCode);

            // make a group
            var group = RelayUtils.CreateMessageGroupOrdering(ndsMessages);

            // add them to the outgoing message table
            foreach (var ndsMessage in group)
            {
                uow.NdsOutgoingMessageRepo.Add(ndsMessage);
            }

            // mark exchange message as delivered
            message.Delivered = DateTime.UtcNow;
            //message.Status = MessageExchangeStatus.Delivered;
            uow.MessageExchangeQueueRepo.Update(message);
            // save DB changes
            uow.SaveChanges();

            // first message
            var first = group.First();

            // post publish the message group action
            queue.Publish(MtoHelper.CreateMtoMessageWithKeyValue(MessageTransferObjectId.PublishMessageGroup, first.Id));
        }
    }
}