﻿using NavCanada.Core.Common.Airac;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using System;
using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class AeroRDSActiveDataProvider : IAeroRDSDataProvider
    {
        public AeroRDSActiveDataProvider(IAeroRdsFeatureProxy aeroRdsProxy)
        {
            _aeroRdsProxy = aeroRdsProxy;
        }

        public IEnumerable<LoadRecord> GetLoadRecords()
        {
            return _aeroRdsProxy.GetFeature<LoadRecords>(UrlBuilder.GetLoadRecordsUrl())?.Value;
        }

        public IEnumerable<Airspace> GetAirspaces()
        {
            var queryUrl = UrlBuilder.GetAirspacesActiveDateUrl(AeroRDSDataProviderConfig.GetNonCandianAdjacentFirs());
            return _aeroRdsProxy.GetFeature<Airspaces>(queryUrl)?.Value;
        }

        public IEnumerable<Aerodrome> GetAerodromes()
        {
            var queryUrl = UrlBuilder.GetAerodromesActiveDateUrl(AeroRDSDataProviderConfig.GetNonCandianAerdromes());
            return _aeroRdsProxy.GetFeature<Aerodromes>(queryUrl)?.Value;
        }

        public IEnumerable<Runway> GetRunways()
        {
            return _aeroRdsProxy.GetFeature<Runways>(UrlBuilder.GetFeatureActiveDateUrl(FeatureNames.RunwayFeature))?.Value;
        }

        public IEnumerable<RunwayDirection> GetRunwayDirections()
        {
            return _aeroRdsProxy.GetFeature<RunwayDirections>(UrlBuilder.GetFeatureActiveDateUrl(FeatureNames.RunwayDirectionFeature))?.Value;
        }

        public IEnumerable<RunwayDirectionDeclaredDistance> GetRunwayDirectionDeclaredDistances()
        {
            return _aeroRdsProxy.GetFeature<RunwayDirectionDeclaredDistances>(UrlBuilder.GetFeatureActiveDateUrl(FeatureNames.RunwayDirectionDeclaredDistanceFeature))?.Value;
        }

        public IEnumerable<ServiceUnit> GetServiceUnits()
        {
            return _aeroRdsProxy.GetFeature<ServiceUnits>(UrlBuilder.GetFeatureActiveDateUrl(FeatureNames.ServiceUnitFeature))?.Value;
        }

        public IEnumerable<Taxiway> GetTaxiways()
        {
            return _aeroRdsProxy.GetFeature<Taxiways>(UrlBuilder.GetFeatureActiveDateUrl(FeatureNames.TaxiwayFeature))?.Value;
        }

        public DateTime GetActiveDate() => AIRACUtils.GetActiveAIRACCycleDate(DateTime.UtcNow);

        private IAeroRDSUrlBuilder UrlBuilder => _aeroRdsProxy?.GetUrlBuilder();

        readonly IAeroRdsFeatureProxy _aeroRdsProxy;
    }
}