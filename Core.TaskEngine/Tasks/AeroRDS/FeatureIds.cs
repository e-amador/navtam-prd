﻿namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    static class FeatureIds
    {
        public static string Load => "LDR";
        public static string Airspace => "FIR";
        public static string Aerodrome => "AHP";
        public static string Runway => "RWY";
        public static string RunwayDirection => "RWD";
        public static string RunwayDirectionDeclaredDistance => "RDD";
        public static string ServiceUnit => "SU";
        public static string Taxiway => "TWY";
    }

}
