﻿using NavCanada.Core.Common.Airac;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Mto;
using System;
using System.Diagnostics;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class AeroRdsLoadCachedDataTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            try
            {
                if (ValidTriggerDate(context))
                {
                    // if this were to fail we don't want to put the system down
                    context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

                    logger.LogDebug(loggerType, "AeroRDS load cached data task STARTED excecuting.");

                    var sw = Stopwatch.StartNew();

                    if (IsSDOCacheUpToPreviousAiracCycle(context.ClusterStorage.Uow))
                    {
                        // only apply the next cycle cached data if the SDO cache is up to date
                        ExecuteSyncCachedData(context.ClusterStorage.Uow, logger);
                    }
                    else
                    {
                        // trigger the full-sync otherwise
                        QueueGlobalDataSync(context.ClusterStorage.Uow, context.ClusterStorage.Queues.TaskEngineQueue);
                    }

                    sw.Stop();

                    logger.LogDebug(loggerType, $"AeroRDS load cached data task COMPLETED in {sw.Elapsed}!");
                }
                else
                {
                    logger.LogDebug(loggerType, $"AeroRDS load cached data task SKIPPED on {DateTime.UtcNow}.");
                }
            }
            catch (Exception ex)
            {
                logger.LogFatal(ex, loggerType, "AeroRDS load cached data task FAILED!");
                throw;
            }
        }

        static bool ValidTriggerDate(IMessageTaskContext context)
        {
            if (context.Mto[MessageDefs.ManuallyScheduledTaskKey]?.Value != null)
                return true;

            var nowUtc = DateTime.UtcNow;
            var nowBareDate = new DateTime(nowUtc.Year, nowUtc.Month, nowUtc.Day);

            // is today the airac cycle day?
            return nowBareDate == AIRACUtils.GetActiveAIRACCycleDate(nowBareDate);
        }

        static bool IsSDOCacheUpToPreviousAiracCycle(IUow uow)
        {
            var lastEffectiveDate = uow.SdoCacheRepo.GetLatestEffectiveDate();
            return lastEffectiveDate.HasValue && lastEffectiveDate.Value.AddDays(28 + 1) > DateTime.UtcNow;
        }

        static void ExecuteSyncCachedData(IUow uow, ILogger logger)
        {
            var aeroRDSDataProvider = new AeroRDSCachedDataProvider(uow);
            var syncEngine = new SdoCacheSyncEngine(aeroRDSDataProvider, uow, logger);
            syncEngine.Execute();
        }

        static void QueueGlobalDataSync(IUow uow, IQueue queue)
        {
            var lastEffectiveDate = uow.SdoCacheRepo.GetLatestEffectiveDate();
            var nextDate = lastEffectiveDate.HasValue ? lastEffectiveDate.Value.AddDays(28).ToShortDateString() : "GlobalSync";
            var mto = MtoHelper.CreateMtoMessage(MessageTransferObjectId.AeroRDSSyncActiveDataMsg, m => m.Add(MessageDefs.NextAiracCycleDate, nextDate, DataType.String));
            queue.Publish(mto);
        }
    }
}