﻿using System.Configuration;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    static class AeroRDSDataProviderConfig
    {
        public static string GetNonCandianAdjacentFirs() => ConfigurationManager.AppSettings["NonCandianAdjacentFirs"];
        public static string GetNonCandianAerdromes() => ConfigurationManager.AppSettings["NonCandianAerodromes"];
    }
}