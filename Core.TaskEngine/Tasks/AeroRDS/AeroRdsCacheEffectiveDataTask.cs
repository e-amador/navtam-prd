﻿using NavCanada.Core.Common.Airac;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Linq;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class AeroRdsCacheEffectiveDataTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            try
            {
                if (ValidTriggerDate(context))
                {
                    // if this were to fail we don't want to put the system down
                    context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

                    logger.LogDebug(loggerType, "AeroRDS cache effective data STARTED excecuting...");

                    var sw = Stopwatch.StartNew();

                    ExecuteEffectiveCache(context.ClusterStorage.Uow, context.ExternalServices.AeroRdsFeatureProxy);

                    sw.Stop();

                    logger.LogDebug(loggerType, $"AeroRDS cache effective data COMPLETED in {sw.Elapsed}!");
                }
                else
                {
                    logger.LogDebug(loggerType, $"AeroRDS cache effective data SKIPPED on {DateTime.UtcNow}.");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "AeroRDS cache effective data FAILED!");
                throw;
            }
        }

        static bool ValidTriggerDate(IMessageTaskContext context)
        {
            if (context.Mto[MessageDefs.ManuallyScheduledTaskKey]?.Value != null)
                return true;

            var nowUtc = DateTime.UtcNow;
            var nowBareDate = new DateTime(nowUtc.Year, nowUtc.Month, nowUtc.Day);

            // is today a week after the airac cycle day?
            return nowBareDate >= AIRACUtils.GetActiveAIRACCycleDate(nowBareDate).AddDays(7);
        }

        static void ExecuteEffectiveCache(IUow uow, IAeroRdsFeatureProxy aeroRdsProxy)
        {
            // Note: this is run aprox week after the airac date, so don't worry about the UTC jumping to the next cycle
            var airacDate = AIRACUtils.GetActiveAIRACCycleDate(DateTime.UtcNow).AddDays(28);
            var aeroRDSDataProvider = new AeroRDSEffectiveDataProvider(aeroRdsProxy, airacDate);

            var loads = aeroRDSDataProvider.GetLoadRecords(); // todo: optimize this
            var loaded = loads != null && loads.Any(load => load.Date == airacDate);

            if (loaded)
            {
                CacheEffectiveData(aeroRDSDataProvider, airacDate, uow);
                UpdateAeroRDSCacheStatus(uow, airacDate);
            }
            else
            {
                var delta = airacDate - DateTime.UtcNow;
                if (delta.Days <= 5)
                {
                    ReportMissingAiracDataToNof(uow, delta.Days);
                }
            }
        }

        static void UpdateAeroRDSCacheStatus(IUow uow, DateTime airacDate)
        {
            var status = uow.AeroRDSCacheStatusRepo.GetStatus();
            if (status != null)
            {
                status.LastEffectiveDate = airacDate;
                status.LastEffectiveUpdated = DateTime.UtcNow;
            }
            else
            {
                status = new AeroRDSCacheStatus()
                {
                    LastEffectiveDate = airacDate,
                    LastEffectiveUpdated = DateTime.UtcNow
                };
                uow.AeroRDSCacheStatusRepo.Add(status);
            }
            uow.SaveChanges();
        }

        static void ReportMissingAiracDataToNof(IUow uow, int days)
        {
            var message = new MessageExchangeQueue
            {
                Id = Guid.NewGuid(),
                PriorityCode = "GG",
                ClientAddress = AftnConsts.NOFAddress,
                Recipients = AftnConsts.NOFAddress,
                Status = MessageExchangeStatus.Important,
                ClientType = Domain.Model.Enums.NdsClientType.Aftn,
                Body = $"WARNING: The next AIRAC cycle date is in {days} days and AERORDS effective date changes are not available yet!",
                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow,
                ClientName = "nof",
                MessageType = MessageExchangeType.Notification,
                Locked = false,
                Inbound = true,
                Read = false
            };
            uow.MessageExchangeQueueRepo.Add(message);
            uow.SaveChanges();
        }

        static void CacheEffectiveData(IAeroRDSDataProvider provider, DateTime date, IUow uow)
        {
            CacheFeature(FeatureIds.Load, provider.GetLoadRecords(), date, uow);
            CacheFeature(FeatureIds.Airspace, provider.GetAirspaces(), date, uow);
            CacheFeature(FeatureIds.Aerodrome, provider.GetAerodromes(), date, uow);
            CacheFeature(FeatureIds.Runway, provider.GetRunways(), date, uow);
            CacheFeature(FeatureIds.RunwayDirection, provider.GetRunwayDirections(), date, uow);
            CacheFeature(FeatureIds.RunwayDirectionDeclaredDistance, provider.GetRunwayDirectionDeclaredDistances(), date, uow);
            CacheFeature(FeatureIds.Taxiway, provider.GetTaxiways(), date, uow);
            CacheFeature(FeatureIds.ServiceUnit, provider.GetServiceUnits(), date, uow);
        }

        static void CacheFeature(string featureId, object data, DateTime date, IUow uow)
        {
            var jsonData = JsonConvert.SerializeObject(data);
            var featureCache = uow.AeroRDSCacheRepo.GetById(featureId);
            if (featureCache == null)
            {
                featureCache = new AeroRDSCache()
                {
                    Id = featureId,
                    Cache = jsonData,
                    EffectiveDate = date
                };
                uow.AeroRDSCacheRepo.Add(featureCache);
            }
            else
            {
                featureCache.Cache = jsonData;
                featureCache.EffectiveDate = date;
                uow.AeroRDSCacheRepo.Update(featureCache);
            }
            uow.SaveChanges();
        }
    }
}