﻿using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using System;
using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class AeroRDSEffectiveDataProvider : IAeroRDSDataProvider
    {
        public AeroRDSEffectiveDataProvider(IAeroRdsFeatureProxy aeroRdsProxy, DateTime effectiveDate)
        {
            _aeroRdsProxy = aeroRdsProxy;
            _effectiveDate = effectiveDate;
        }

        public IEnumerable<LoadRecord> GetLoadRecords()
        {
            return _aeroRdsProxy.GetFeature<LoadRecords>(UrlBuilder.GetLoadRecordsUrl())?.Value;
        }

        public IEnumerable<Airspace> GetAirspaces()
        {
            var queryUrl = UrlBuilder.GetAirspacesEffectiveDateUrl(_effectiveDate, AeroRDSDataProviderConfig.GetNonCandianAdjacentFirs());
            return _aeroRdsProxy.GetFeature<Airspaces>(queryUrl)?.Value;
        }

        public IEnumerable<Aerodrome> GetAerodromes()
        {
            var queryUrl = UrlBuilder.GetAerodromesEffectiveDateUrl(_effectiveDate, AeroRDSDataProviderConfig.GetNonCandianAerdromes());
            return _aeroRdsProxy.GetFeature<Aerodromes>(queryUrl)?.Value;
        }

        public IEnumerable<Runway> GetRunways()
        {
            return _aeroRdsProxy.GetFeature<Runways>(UrlBuilder.GetFeatureEffectiveDateUrl(FeatureNames.RunwayFeature, _effectiveDate))?.Value;
        }

        public IEnumerable<RunwayDirection> GetRunwayDirections()
        {
            return _aeroRdsProxy.GetFeature<RunwayDirections>(UrlBuilder.GetFeatureEffectiveDateUrl(FeatureNames.RunwayDirectionFeature, _effectiveDate))?.Value;
        }

        public IEnumerable<RunwayDirectionDeclaredDistance> GetRunwayDirectionDeclaredDistances()
        {
            return _aeroRdsProxy.GetFeature<RunwayDirectionDeclaredDistances>(UrlBuilder.GetFeatureEffectiveDateUrl(FeatureNames.RunwayDirectionDeclaredDistanceFeature, _effectiveDate))?.Value;
        }

        public IEnumerable<ServiceUnit> GetServiceUnits()
        {
            return _aeroRdsProxy.GetFeature<ServiceUnits>(UrlBuilder.GetFeatureEffectiveDateUrl(FeatureNames.ServiceUnitFeature, _effectiveDate))?.Value;
        }

        public IEnumerable<Taxiway> GetTaxiways()
        {
            return _aeroRdsProxy.GetFeature<Taxiways>(UrlBuilder.GetFeatureEffectiveDateUrl(FeatureNames.TaxiwayFeature, _effectiveDate))?.Value;
        }

        public DateTime GetActiveDate() => _effectiveDate;

        private IAeroRDSUrlBuilder UrlBuilder => _aeroRdsProxy?.GetUrlBuilder();

        readonly IAeroRdsFeatureProxy _aeroRdsProxy;
        readonly DateTime _effectiveDate;
    }
}