﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class SdoCacheSyncEngine
    {
        public SdoCacheSyncEngine(IAeroRDSDataProvider aeroRDSDataProvider, IUow uow, ILogger logger)
        {
            _aeroRDSDataProvider = aeroRDSDataProvider;
            _uow = uow;
            _logger = logger;
        }

        public void Execute()
        {
            var context = new SdoCacheSyncContext(_uow);
            _logger.LogDebug(GetType(), "Start SdoCache Sync..");

            // firs/airspaces
            _logger.LogDebug(GetType(), "Caching firs/airspaces..");
            SyncAirspaces(context, _aeroRDSDataProvider.GetAirspaces());

            // aerodromes
            _logger.LogDebug(GetType(), "Caching aerodromes..");
            SyncAerodromes(context, _aeroRDSDataProvider.GetAerodromes());

            context.ClearFirs();

            // runways
            _logger.LogDebug(GetType(), "Caching runways..");
            SyncRunways(context, _aeroRDSDataProvider.GetRunways());

            // taxiways
            _logger.LogDebug(GetType(), "Caching taxiways..");
            SyncTaxiways(context, _aeroRDSDataProvider.GetTaxiways());

            // service units
            _logger.LogDebug(GetType(), "Caching service units..");
            SyncServiceUnits(context, _aeroRDSDataProvider.GetServiceUnits());

            context.ClearAerodromes();

            // runway directions
            _logger.LogDebug(GetType(), "Caching runway directions..");
            SyncRunwayDirections(context, _aeroRDSDataProvider.GetRunwayDirections());

            context.ClearRunways();

            // runway direction declared distances
            _logger.LogDebug(GetType(), "Caching runway direction declared distances..");
            SyncRunwayDirectionDeclaredDistances(context, _aeroRDSDataProvider.GetRunwayDirectionDeclaredDistances());

            // update aerords cache data
            _logger.LogDebug(GetType(), "Updating AeroRDS cache status..");
            UpdateAeroRDSCacheStatus(_uow, _aeroRDSDataProvider.GetActiveDate());

            _logger.LogDebug(GetType(), "End SdoCacheSync");
        }

        static void UpdateAeroRDSCacheStatus(IUow uow, DateTime airacDate)
        {
            var status = uow.AeroRDSCacheStatusRepo.GetStatus();
            if (status != null)
            {
                status.LastActiveDate = airacDate;
                status.LastActiveUpdated = DateTime.UtcNow;
                uow.AeroRDSCacheStatusRepo.Update(status);
            }
            else
            {
                status = new AeroRDSCacheStatus()
                {
                    LastActiveDate = airacDate,
                    LastActiveUpdated = DateTime.UtcNow
                };
                uow.AeroRDSCacheStatusRepo.Add(status);
            }
            uow.SaveChanges();
        }

        static void SyncAirspaces(SdoCacheSyncContext context, IEnumerable<Airspace> airspaces)
        {
            var uow = context.Uow;
            var repo = uow.SdoCacheRepo;

            // get the existing Firs
            context.AddFirs(repo.GetAllByTypeIncludingDeleted(SdoEntityType.Fir));

            // sync each fir
            foreach (var airspace in airspaces)
            {
                var fir = context.TryGetFir(airspace.AirspaceId);
                if (fir != null)
                {
                    // update existing fir
                    UpdateAirspaceCache(airspace, fir, repo.Update);
                }
                else
                {
                    // add new fir if not deleted
                    if (!"D".Equals(airspace.Status))
                    {
                        UpdateAirspaceCache(airspace, new SdoCache(), c =>
                        {
                            repo.Add(c);
                            context.AddFir(c);
                        });
                    }
                }
            }

            uow.SaveChanges();
        }

        static void SyncAerodromes(SdoCacheSyncContext context, IEnumerable<Aerodrome> aerodromes)
        {
            var uow = context.Uow;
            var repo = uow.SdoCacheRepo;

            // cache existing aerodromes
            context.AddAerodromes(repo.GetAllByTypeIncludingDeleted(SdoEntityType.Aerodrome));

            // sync each aerodrome
            foreach (var aerodrome in aerodromes)
            {
                var cached = context.TryGetAerodrome(aerodrome.AerodromeId);
                if (cached != null)
                {
                    // update existing aerodrome
                    UpdateAerodromeCache(aerodrome, cached, repo.Update);
                }
                else
                {
                    // add new aerodrome if not deleted
                    if (IsValidAerodrome(aerodrome))
                    {
                        UpdateAerodromeCache(aerodrome, new SdoCache(), c =>
                        {
                            repo.Add(c);
                            context.AddAerodrome(c);
                        });
                    }
                }
            }

            uow.SaveChanges();
        }

        static void SyncRunways(SdoCacheSyncContext context, IEnumerable<Runway> runways)
        {
            var uow = context.Uow;
            var repo = uow.SdoCacheRepo;

            // cache existing runways
            context.AddRunways(repo.GetAllByTypeIncludingDeleted(SdoEntityType.Runway));

            // sync each runway
            foreach (var runway in runways)
            {
                var aerodrome = context.TryGetAerodrome(runway.FK_Aerodrome);
                if (aerodrome != null)
                {
                    var cached = context.TryGetRunway(runway.RunwayId); // repo.GetById(runway.RunwayId);
                    if (cached != null)
                    {
                        UpdateRunwayCache(runway, cached, aerodrome, repo.Update);
                    }
                    else
                    {
                        if (!"D".Equals(runway.Status))
                        {
                            UpdateRunwayCache(runway, new SdoCache(), aerodrome, c =>
                            {
                                repo.Add(c);
                                context.AddRunway(c);
                            });
                        }
                    }
                }
            }

            uow.SaveChanges();
        }

        static void SyncRunwayDirections(SdoCacheSyncContext context, IEnumerable<RunwayDirection> runwayDirections)
        {
            var uow = context.Uow;
            var repo = uow.SdoCacheRepo;

            // cache existing runway directions
            context.AddRunwayDirections(repo.GetAllByTypeIncludingDeleted(SdoEntityType.RunwayDirection));

            foreach (var rwd in runwayDirections)
            {
                var runway = context.TryGetRunway(rwd.FK_Runway);
                if (runway != null)
                {
                    var cached = context.TryGetRunwayDirection(rwd.RunwayDirectionId);
                    if (cached != null)
                    {
                        UpdateRunwayDirectionCache(rwd, cached, runway, repo.Update);
                    }
                    else
                    {
                        if (!"D".Equals(rwd.Status))
                        {
                            UpdateRunwayDirectionCache(rwd, new SdoCache(), runway, c =>
                            {
                                repo.Add(c);
                                context.AddRunwayDirection(c);
                            });
                        }
                    }
                }
            }

            uow.SaveChanges();
        }

        static void SyncRunwayDirectionDeclaredDistances(SdoCacheSyncContext context, IEnumerable<RunwayDirectionDeclaredDistance> rdds)
        {
            var uow = context.Uow;
            var repo = uow.SdoCacheRepo;

            // cache existing runway direction declared distances
            context.AddRunwayDirectionDeclaredDistances(repo.GetAllByTypeIncludingDeleted(SdoEntityType.RunwayDirectionDeclaredDistance));

            foreach (var rdd in rdds)
            {
                var rwd = context.TryGetRunwayDirection(rdd.FK_RunwayDirection);
                if (rwd != null)
                {
                    var cached = context.TryGetRunwayDirectionDeclaredDistance(rdd.RunwayDirectionDeclaredDistanceId);
                    if (cached != null)
                    {
                        UpdateRunwayDirectionDeclaredDistance(rdd, cached, rwd, repo.Update);
                    }
                    else
                    {
                        if (!"D".Equals(rdd.Status))
                        {
                            UpdateRunwayDirectionDeclaredDistance(rdd, new SdoCache(), rwd, c =>
                            {
                                repo.Add(c);
                                context.AddRunwayDirectionDeclaredDistance(c);
                            });
                        }
                    }
                }
            }

            uow.SaveChanges();
        }

        static void SyncTaxiways(SdoCacheSyncContext context, IEnumerable<Taxiway> taxiways)
        {
            var uow = context.Uow;
            var repo = uow.SdoCacheRepo;

            // sync each taxiway
            foreach (var taxiway in taxiways)
            {
                var aerodrome = context.TryGetAerodrome(taxiway.FK_Aerodrome);
                if (aerodrome != null)
                {
                    var cached = repo.GetById(taxiway.TaxiwayId);
                    if (cached != null)
                    {
                        // fix migrations (use effective date)
                        UpdateTaxiwayCache(taxiway, cached, aerodrome, repo.Update);
                    }
                    else
                    {
                        if (!"D".Equals(taxiway.Status))
                        {
                            cached = new SdoCache();
                            UpdateTaxiwayCache(taxiway, cached, aerodrome, repo.Add);
                        }
                    }
                }
            }

            uow.SaveChanges();
        }

        static void SyncServiceUnits(SdoCacheSyncContext context, IEnumerable<ServiceUnit> serviceUnits)
        {
            var uow = context.Uow;
            var repo = uow.SdoCacheRepo;

            // sync each service
            foreach (var svcUnit in serviceUnits)
            {
                var aerodrome = context.TryGetAerodrome(svcUnit.FK_Aerodrome);
                if (aerodrome != null)
                {
                    var cached = repo.GetById(svcUnit.ServiceUnitId);
                    if (cached != null)
                    {
                        // fix migrations (use effective date)
                        UpdateServiceUnitCache(svcUnit, cached, aerodrome, repo.Update);
                    }
                    else
                    {
                        if (!"D".Equals(svcUnit.Status))
                        {
                            cached = new SdoCache();
                            UpdateServiceUnitCache(svcUnit, cached, aerodrome, repo.Add);
                        }
                    }
                }
            }

            uow.SaveChanges();
        }

        static void UpdateAirspaceCache(Airspace airspace, SdoCache cached, Action<SdoCache> updateAction)
        {
            if (cached.EffectiveDate != airspace.EffectiveDate)
            {
                var wkValue = airspace.GeographicalLocation.WellKnownValue;

                // since the ref point is calculated, lets repect the exisiting value (we may have edited them)
                var refPoint = cached.RefPoint ?? GetRegionCenter(wkValue);

                var geography = GetGeographyFromRegion(wkValue);

                cached.Id = airspace.AirspaceId;
                cached.Type = SdoEntityType.Fir;
                cached.Name = airspace.Name;
                cached.Designator = airspace.Identifier;
                cached.Fir = airspace.Identifier;
                cached.RefPoint = refPoint;
                cached.Geography = geography;
                cached.Attributes = JsonConvert.SerializeObject(airspace, serializerSettings);
                cached.Keywords = FormatKeywords(airspace.Identifier, airspace.Name, string.Empty);
                cached.Deleted = "D".Equals(airspace.Status);
                cached.EffectiveDate = airspace.EffectiveDate;

                updateAction.Invoke(cached);
            }
        }

        static void UpdateAerodromeCache(Aerodrome aerodrome, SdoCache cached, Action<SdoCache> updateAction)
        {
            var aerodromeDeleted = "D".Equals(aerodrome.Status);

            if (cached.EffectiveDate != aerodrome.EffectiveDate)
            {
                var refPoint = GetRefPoint(aerodrome.GeographicalLocation);

                cached.Id = aerodrome.AerodromeId;
                cached.Type = SdoEntityType.Aerodrome;
                cached.Name = aerodrome.Name;
                // When an aerodrome is deleted from SDO database, the Designator field received contains extra characters (> 4). 
                // In this case we are not updating the Designator field in our database otherwise unpredictable  
                // errors can ocurre.   
                cached.Designator = aerodromeDeleted ? cached.Designator : aerodrome.Identifier; 
                cached.Fir = aerodrome.FIR;
                cached.RefPoint = refPoint;
                cached.Attributes = JsonConvert.SerializeObject(aerodrome, serializerSettings);
                cached.Keywords = FormatKeywords(aerodrome.Identifier, aerodrome.Name, aerodrome.ServedCity);
                cached.Deleted = (aerodromeDeleted || aerodrome.Identifier.Length != 4);
                cached.EffectiveDate = aerodrome.EffectiveDate;

                updateAction.Invoke(cached);
            }
        }

        static string FormatKeywords(string designator, string name, string servedCity)
        {
            return $"{designator ?? string.Empty}||{name ?? string.Empty}||{servedCity ?? string.Empty}";
        }

        static DbGeography GetRefPoint(GeographicalLocation location)
        {
            var wkValue = location?.WellKnownValue;
            return wkValue != null ? DbGeography.FromText(wkValue.WellKnownText, wkValue.CoordinateSystemId) : null;
        }

        static void UpdateRunwayCache(Runway runway, SdoCache cached, SdoCache parent, Action<SdoCache> updateAction)
        {
            if (cached.EffectiveDate != runway.EffectiveDate)
            {
                cached.Id = runway.RunwayId;
                cached.ParentId = parent.Id;
                cached.Type = SdoEntityType.Runway;
                cached.Designator = runway.Designator;
                //cached.Fir = parent.Fir;
                cached.RefPoint = parent.RefPoint;
                cached.Attributes = JsonConvert.SerializeObject(runway, serializerSettings);
                cached.Keywords = FormatKeywords(runway.Designator, string.Empty, parent.Keywords);
                cached.Deleted = "D".Equals(runway.Status);
                cached.EffectiveDate = runway.EffectiveDate;

                updateAction.Invoke(cached);
            }
        }

        static void UpdateRunwayDirectionCache(RunwayDirection rwd, SdoCache cached, SdoCache parent, Action<SdoCache> updateAction)
        {
            if (cached.EffectiveDate != rwd.EffectiveDate)
            {
                var refPoint = GetRefPoint(rwd.GeographicalLocation);

                cached.Id = rwd.RunwayDirectionId;
                cached.ParentId = parent.Id;
                cached.Type = SdoEntityType.RunwayDirection;
                cached.Designator = rwd.Designator;
                //cached.Fir = parent.Fir;
                cached.RefPoint = refPoint;
                cached.Attributes = JsonConvert.SerializeObject(rwd, serializerSettings);
                cached.Deleted = "D".Equals(rwd.Status);
                cached.EffectiveDate = rwd.EffectiveDate;

                updateAction.Invoke(cached);
            }
        }

        static void UpdateRunwayDirectionDeclaredDistance(RunwayDirectionDeclaredDistance rdd, SdoCache cached, SdoCache parent, Action<SdoCache> updateAction)
        {
            if (cached.EffectiveDate != rdd.EffectiveDate)
            {
                cached.Id = rdd.RunwayDirectionDeclaredDistanceId;
                cached.ParentId = parent.Id;
                cached.Type = SdoEntityType.RunwayDirectionDeclaredDistance;
                cached.Attributes = JsonConvert.SerializeObject(rdd, serializerSettings);
                cached.Deleted = "D".Equals(rdd.Status);
                cached.EffectiveDate = rdd.EffectiveDate;

                updateAction.Invoke(cached);
            }
        }

        static void UpdateTaxiwayCache(Taxiway taxiway, SdoCache cached, SdoCache parent, Action<SdoCache> updateAction)
        {
            if (cached.EffectiveDate != taxiway.EffectiveDate)
            {
                cached.Id = taxiway.TaxiwayId;
                cached.ParentId = parent.Id;
                cached.Type = SdoEntityType.Taxiway;
                cached.Designator = taxiway.Designator;
                cached.Fir = parent.Fir;
                cached.RefPoint = parent.RefPoint;
                cached.Attributes = JsonConvert.SerializeObject(taxiway, serializerSettings);
                cached.Keywords = FormatKeywords(taxiway.Designator, string.Empty, parent.Keywords);
                cached.Deleted = "D".Equals(taxiway.Status);
                cached.EffectiveDate = taxiway.EffectiveDate;

                updateAction.Invoke(cached);
            }
        }

        static void UpdateServiceUnitCache(ServiceUnit svcUnit, SdoCache cached, SdoCache parent, Action<SdoCache> updateAction)
        {
            if (cached.EffectiveDate != svcUnit.EffectiveDate)
            {
                cached.Id = svcUnit.ServiceUnitId;
                cached.ParentId = parent.Id;
                cached.Type = SdoEntityType.Service;
                cached.Name = svcUnit.Name;
                cached.Designator = svcUnit.Name?.EndsWith("CARS") == true ? "CARS" : null;
                cached.Attributes = JsonConvert.SerializeObject(svcUnit, serializerSettings);
                cached.Keywords = FormatKeywords(cached.Designator, string.Empty, parent.Keywords);
                cached.Deleted = "D".Equals(svcUnit.Status);
                cached.EffectiveDate = svcUnit.EffectiveDate;

                updateAction.Invoke(cached);
            }
        }

        static DbGeography GetGeographyFromRegion(WellKnownValue wkValue)
        {
            return DbGeography.FromText(wkValue.WellKnownText, wkValue.CoordinateSystemId);
        }

        static DbGeography GetRegionCenter(WellKnownValue regionAsText)
        {
            return GetRegionCenterFromCentroid(regionAsText) ?? GetRegionCenterFromAverage(regionAsText);
        }

        static DbGeography GetRegionCenterFromCentroid(WellKnownValue regionAsText)
        {
            try
            {
                var geo = DbGeometry.FromText(regionAsText.WellKnownText, regionAsText.CoordinateSystemId);
                var centroid = geo.Centroid;
                return centroid != null ? DbGeography.FromText(centroid.AsText(), centroid.CoordinateSystemId) : null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static DbGeography GetRegionCenterFromAverage(WellKnownValue regionAsText)
        {
            var geo = DbGeography.FromText(regionAsText.WellKnownText, regionAsText.CoordinateSystemId);

            double latAvg = 0.0;
            double lngAvg = 0.0;

            if (geo?.PointCount != null)
            {
                int pointCount = (int)geo.PointCount;
                for (int i = 1; i <= pointCount; i++)
                {
                    var p = geo.PointAt(i);
                    latAvg += (double)p.Latitude;
                    lngAvg += (double)p.Longitude;
                }
                latAvg /= pointCount;
                lngAvg /= pointCount;
            }

            return DbGeography.PointFromText($"POINT({lngAvg} {latAvg})", geo.CoordinateSystemId);
        }

        static string FormatCurrentDate()
        {
            var dt = DateTime.UtcNow;
            return $"{dt.Year}-{dt.Month}-{dt.Day}";
        }

        static bool IsValidAerodrome(Aerodrome aerodrome)
        {
            return aerodrome.Status != "D" &&
                   !"Closed".Equals(aerodrome.AerodromeStatus, StringComparison.InvariantCultureIgnoreCase) &&
                   !string.IsNullOrEmpty(aerodrome.Identifier) &&
                   aerodrome.Identifier.Length == 4;
        }

        static string GetEqFilter(string field, IEnumerable<string> values)
        {
            return string.Join(" or ", values.Select(s => $"{field} eq '{s}'"));
        }

        private readonly IUow _uow;
        private readonly IAeroRDSDataProvider _aeroRDSDataProvider;
        private readonly ILogger _logger;
        static JsonSerializerSettings serializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
    }
}