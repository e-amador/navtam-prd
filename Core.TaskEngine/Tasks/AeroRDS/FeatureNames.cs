﻿namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    static class FeatureNames
    {
        public static string AirspaceFeature => nameof(AirspaceFeature);
        public static string AerodromeFeature => nameof(AerodromeFeature);
        public static string RunwayFeature => nameof(RunwayFeature);
        public static string RunwayDirectionFeature => nameof(RunwayDirectionFeature);
        public static string RunwayDirectionDeclaredDistanceFeature => nameof(RunwayDirectionDeclaredDistanceFeature);
        public static string ServiceUnitFeature => nameof(ServiceUnitFeature);
        public static string TaxiwayFeature => nameof(TaxiwayFeature);
    }
}
