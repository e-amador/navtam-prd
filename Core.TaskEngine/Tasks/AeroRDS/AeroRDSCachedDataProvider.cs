﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class AeroRDSCachedDataProvider : IAeroRDSDataProvider
    {
        public AeroRDSCachedDataProvider(IUow uow)
        {
            _uow = uow;
        }

        public IEnumerable<LoadRecord> GetLoadRecords() => DeserializeFeature<LoadRecord>(FeatureIds.Load);
        public IEnumerable<Airspace> GetAirspaces() => DeserializeFeature<Airspace>(FeatureIds.Airspace);
        public IEnumerable<Aerodrome> GetAerodromes() => DeserializeFeature<Aerodrome>(FeatureIds.Aerodrome);
        public IEnumerable<Runway> GetRunways() => DeserializeFeature<Runway>(FeatureIds.Runway);
        public IEnumerable<RunwayDirection> GetRunwayDirections() => DeserializeFeature<RunwayDirection>(FeatureIds.RunwayDirection);
        public IEnumerable<RunwayDirectionDeclaredDistance> GetRunwayDirectionDeclaredDistances() => DeserializeFeature<RunwayDirectionDeclaredDistance>(FeatureIds.RunwayDirectionDeclaredDistance);
        public IEnumerable<ServiceUnit> GetServiceUnits() => DeserializeFeature<ServiceUnit>(FeatureIds.ServiceUnit);
        public IEnumerable<Taxiway> GetTaxiways() => DeserializeFeature<Taxiway>(FeatureIds.Taxiway);

        public DateTime GetActiveDate()
        {
            var effectiveDate = _uow.AeroRDSCacheRepo.GetById(FeatureIds.Load)?.EffectiveDate;
            return effectiveDate.HasValue ? effectiveDate.Value : DateTime.UtcNow;
        }

        private T[] DeserializeFeature<T>(string name)
        {
            var cacheEntry = _uow.AeroRDSCacheRepo.GetById(name);
            if (cacheEntry == null)
                throw new Exception($"Missing AeroRDS cache for feature '{name}'");
            return JsonConvert.DeserializeObject<T[]>(cacheEntry.Cache);
        }

        readonly IUow _uow;
    }
}