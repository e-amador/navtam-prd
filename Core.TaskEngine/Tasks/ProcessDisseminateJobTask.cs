﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using System;
using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Tasks
{
    internal class ProcessDisseminateJobTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var loggerType = GetType();
            var logger = context.ExternalServices.Logger;
            bool shouldFireTrigger = false;

            try
            {
                logger.LogDebug(loggerType, "ProcessDisseminateJobTask task started...");

                var keyObj = context.Mto[MessageDefs.EntityIdentifierKey];
                if (keyObj == null)
                {
                    logger.LogWarn(loggerType, "Mto message missing entity identifier...");
                    return;
                }

                var jobIdRaw = keyObj.Value.ToString();

                string notamId = null;
                int jobId;
                if (jobIdRaw.Contains("_"))
                {
                    var parts = jobIdRaw.Split('_');
                    if (!int.TryParse(parts[0], out jobId))
                    {
                        logger.LogWarn(loggerType, $"Failed to convert Job Id '{jobIdRaw}'", new { jobId = jobIdRaw });
                        return;
                    }
                    if( parts[1].Length > 0 )
                    {
                        notamId = parts[1];
                    }
                }
                else
                {
                    if (!int.TryParse(jobIdRaw, out jobId))
                    {
                        logger.LogWarn(loggerType, $"Failed to convert Job Id '{jobIdRaw}'", new { jobId = jobIdRaw });
                        return;
                    }
                }

                var uow = context.ClusterStorage.Uow;
                
                var job = uow.DisseminationJobRepo.GetById(jobId);
                if (job == null)
                {
                    logger.LogWarn(loggerType, $"Task completed but Job {jobId} was NOT FOUND in DB.", new { jobId });
                    return;
                }

                var scheduleManager = context.ExternalServices.ScheduleManager;
                if (scheduleManager != null)
                {
                    try
                    {
                        var currentJobStatus = job.Status;
                        if (IsJobActive(currentJobStatus))
                        {
                            shouldFireTrigger = ProcessNotam(context, job,notamId, scheduleManager);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogError(e, loggerType, $"Disseminate job with id '{jobId}' has failed!", job);
                        job.Status = DisseminationJobStatus.Failed;
                    }
                }
                else
                {
                    logger.LogError(new InvalidOperationException("ScheduleManager not found."), loggerType, $"Disseminate job with id '{jobId}' has failed!", job);
                    job.Status = DisseminationJobStatus.Failed;
                }

                // update the job
                uow.DisseminationJobRepo.Update(job);
                uow.SaveChanges();

                if (shouldFireTrigger)
                {
                    scheduleManager.FireJob(ScheduleIdentity.BroadcastNotamsToSubscribers);
                }

                logger.LogDebug(loggerType, $"ProcessDisseminateJobTask finished dissemination of notam# {job.LastNotam}.");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "ProcessDisseminateJobTask has FAILED!");
                throw;
            }
        }

        static bool ProcessNotam(IMessageTaskContext context, DisseminationJob job, string notamId, IScheduleManager scheduleManager)
        {
            bool shouldFireTrigger = false;

            job.Status = DisseminationJobStatus.InProcess;
            if ( string.IsNullOrEmpty(notamId) )
            {
                //First time (message sent by Veb App), Start first trigger
                shouldFireTrigger = true;
            }
            else
            {
                Guid id;
                if (Guid.TryParse(notamId, out id))
                {
                    var uow = context.ClusterStorage.Uow;

                    // set job in process
                    var notam = uow.NotamRepo.GetById(id);
                    if (notam != null)
                    {
                        // update the last notam in the job
                        job.LastNotam = notam.SequenceNumber;
                        if (DisseminateNotamToAddresses(context, notam, job.ClientIds, job.DisseminateXml) > 0)
                        {
                            // if at least one client matched, queue the next job and leave
                            shouldFireTrigger = true;
                        }
                    }
                    else
                    {
                        // none found
                        job.Status = DisseminationJobStatus.Completed;
                    }
                }
            }

            // update job
            job.LastUpdate = DateTime.UtcNow;

            return shouldFireTrigger;
        }

        //static void ProcessNextNotam(IMessageTaskContext context, DisseminationJob job)
        //{
        //    var uow = context.ClusterStorage.Uow;

        //    // set job in process
        //    job.Status = DisseminationJobStatus.InProcess;

        //    while (true)
        //    {
        //        // try to seek on the next notam to disseminate
        //        var nextNotam = GetNextNotamToDisseminate(uow, job);
        //        if (nextNotam != null)
        //        {
        //            // update the last notam in the job
        //            job.LastNotam = nextNotam.SequenceNumber;

        //            if (DisseminateNotamToAddresses(context, nextNotam, job.ClientIds, job.DisseminateXml) > 0)
        //            {
        //                // if at least one client matched, queue the next job and leave
        //                //QueueNextDisseminationJob(uow, job.Id);
        //                break;
        //            }
        //        }
        //        else
        //        {
        //            // none found
        //            job.Status = DisseminationJobStatus.Completed;
        //            break;
        //        }
        //    }

        //    // update job
        //    job.LastUpdate = DateTime.UtcNow;
        //}


        //static Notam GetNextNotamToDisseminate(IUow uow, DisseminationJob job)
        //{
        //    var refDate = job.BreakDate.HasValue ? job.BreakDate.Value : new DateTime(DateTime.UtcNow.Year, 1, 1);
        //    return uow.NotamRepo.GetNextBySequenceNumberAndReferenceDate(job.LastNotam, refDate);
        //}

        static readonly char[] IdSeparators = new char[] { ' ' };

        static HashSet<string> GetIdSet(string ids)
        {
            return new HashSet<string>((ids ?? "").Split(IdSeparators, StringSplitOptions.RemoveEmptyEntries));
        }

        static int DisseminateNotamToAddresses(IMessageTaskContext context, Notam notam, string ids, bool disseminateXml)
        {
            var uow = context.ClusterStorage.Uow;

            var messagePublisher = new MessagePublisher(context.ExternalServices.SolaceEndpoint, uow);
            var distributionRelay = new DistributionRelay(uow, messagePublisher);

            var totalDisseminated = 0;

            if (disseminateXml)
            {
                // disseminate to the hub, but do not distribute it
                distributionRelay.Disseminate(notam, false);
                totalDisseminated++;
            }
            else
            {
                var idSet = GetIdSet(ids);
                if (idSet.Count > 0)
                {
                    totalDisseminated += distributionRelay.DistributeToAftn(notam, a => idSet.Contains(a.Id.ToString()), false);
                }
            }

            return totalDisseminated;
        }

        static bool IsJobActive(DisseminationJobStatus status)
        {
            return status == DisseminationJobStatus.Pending || status == DisseminationJobStatus.InProcess;
        }
    }
}