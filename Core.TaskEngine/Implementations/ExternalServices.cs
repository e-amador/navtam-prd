﻿using NavCanada.Core.NotificationEngine;
using NavCanada.Core.Proxies.DiagnosticProxy;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.Common.Contracts;

namespace NavCanada.Core.TaskEngine.Implementations
{
    public class ExternalServices : IExternalServices
    {
        public ExternalServices(IDiagnosticProxy diagnosticProxy, INotifier notifier,
            ISolaceEndpoint solaceEndpoint, IAeroRdsFeatureProxy aeroRdsFeatureProxy, ILogger logger, IScheduleManager schedulerManager)
        {
            DiagnosticProxy = diagnosticProxy;
            Notifier = notifier;
            SolaceEndpoint = solaceEndpoint;
            AeroRdsFeatureProxy = aeroRdsFeatureProxy;
            Logger = logger;
            ScheduleManager = schedulerManager;
        }

        public IDiagnosticProxy DiagnosticProxy { get; }
        public INotifier Notifier { get; }
        public ISolaceEndpoint SolaceEndpoint { get; }
        public IAeroRdsFeatureProxy AeroRdsFeatureProxy { get; }
        public ILogger Logger { get; }
        public IScheduleManager ScheduleManager { get; }

    }
}