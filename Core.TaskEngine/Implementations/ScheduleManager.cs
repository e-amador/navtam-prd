﻿
using NavCanada.Core.Common.Common;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Triggers;
using Quartz;
using Quartz.Impl;
using System;
using System.Configuration;

namespace NavCanada.Core.TaskEngine.Implementations
{
    public class ScheduleManager : IScheduleManager
    {
        public ScheduleManager()
        {
            _scheduler = StdSchedulerFactory.GetDefaultScheduler();
        }

        public void Start()
        {
            if (_scheduler != null)
            {
                _scheduler.Start();
            }
        }

        public void AddToContext(string key, object value)
        {
            try
            {
                lock (safetyLock)
                {
                    _scheduler.Context.Add(key, value);
                }
            }
            catch (Exception)
            {
                //todo: ignore
            }
        }

        public void RemoveFromContext(string key)
        {
            try
            {
                lock (safetyLock)
                {
                    if (_scheduler.Context.ContainsKey(key))
                    {
                        _scheduler.Context.Remove(key);
                    }
                }
            }
            catch (Exception)
            {
                //todo: ignore
            }
        }

        public bool CheckIfJobExist(string jobName)
        {
            try
            {
                lock (safetyLock)
                {
                    return _scheduler.CheckExists(new JobKey(jobName + "_JOB", jobName + "_GROUP"));
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteJob(string jobName )
        {
            try
            {
                lock (safetyLock)
                {
                    var jobKey = new JobKey(jobName + "_JOB", jobName + "_GROUP");
                    if (_scheduler.CheckExists(jobKey))
                    {
                        _scheduler.DeleteJob(jobKey);
                    }
                }
            }
            catch (Exception)
            {
                //todo: ignore
            }
        }

        public void PauseJob(string jobName)
        {
            try
            {
                lock (safetyLock)
                {
                    var jobKey = new JobKey(jobName + "_JOB", jobName + "_GROUP");
                    if (_scheduler.CheckExists(jobKey))
                    {
                        _scheduler.PauseJob(jobKey);
                    }
                }
            }
            catch (Exception)
            {
                //todo: ignore
            }
        }

        public void ResumeJob(string jobName)
        {
            try
            {
                lock (safetyLock)
                {
                    var jobKey = new JobKey(jobName + "_JOB", jobName + "_GROUP");
                    if (_scheduler.CheckExists(jobKey))
                    {
                        _scheduler.ResumeJob(jobKey);
                    }
                }
            }
            catch (Exception)
            {
                //todo: ignore
            }
        }

        public void FireJob(string jobName)
        {
            try
            {
                lock (safetyLock)
                {
                    var jobKey = new JobKey(jobName + "_JOB", jobName + "_GROUP");
                    if (_scheduler.CheckExists(jobKey))
                    {
                        _scheduler.TriggerJob(jobKey);
                    }
                }
            }
            catch (Exception)
            {
                //todo: ignore
            }
        }

        public void Stop()
        {
            try
            {
                if (_scheduler != null)
                {
                    _scheduler.Shutdown();
                }
            }
            catch (Exception)
            {
                //todo: ignore
            }
        }

        public void CreateSdoEffectiveCacheSyncSchedulerJob()
        {
            var cronScheduler = ConfigurationManager.AppSettings["SdoCacheSyncCronScheduler"] ?? "0 0 22 * * ?"; // or daily at @22:00

            var jobKey = new JobKey(ScheduleIdentity.AeroRdsEffectiveCacheSyncScheduler + "_JOB", ScheduleIdentity.AeroRdsEffectiveCacheSyncScheduler + "_GROUP");
            var job = JobBuilder.Create<AeroRDSCacheSyncJob>()
                .WithIdentity(jobKey)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity(ScheduleIdentity.AeroRdsEffectiveCacheSyncScheduler + "_TRIGGER", jobKey.Group)
                .StartNow()
                .WithCronSchedule(cronScheduler)
                .Build();

            _scheduler.ScheduleJob(job, trigger);
        }

        public void CreateSdoEffectiveCacheLoadSchedulerJob()
        {
            var cronScheduler = ConfigurationManager.AppSettings["SdoCacheLoadCronScheduler"] ?? "0 30 0 * * ?"; // or daily at @00:30

            var jobKey = new JobKey(ScheduleIdentity.AeroRdsEffectiveCacheLoadScheduler + "_JOB", ScheduleIdentity.AeroRdsEffectiveCacheLoadScheduler + "_GROUP");
            var job = JobBuilder.Create<AeroRDSCacheLoadJob>()
                .WithIdentity(jobKey)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity(ScheduleIdentity.AeroRdsEffectiveCacheLoadScheduler + "_TRIGGER", jobKey.Group)
                .StartNow()
                .WithCronSchedule(cronScheduler)
                .Build();

            _scheduler.ScheduleJob(job, trigger);
        }

        public void CreateChecklistSchedulerJob()
        {
            var cronScheduler = ConfigurationManager.AppSettings["ChecklistCronScheduler"] ?? "0 30 23 L * ?"; // last day of every month @23:30
            
            var jobKey = new JobKey(ScheduleIdentity.ChecklistScheduler + "_JOB", ScheduleIdentity.ChecklistScheduler + "_GROUP");
            var job = JobBuilder.Create<ChecklistJob>()
                .WithIdentity(jobKey)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity(ScheduleIdentity.ChecklistScheduler + "_TRIGGER", jobKey.Group)
                .StartNow()
                .WithCronSchedule(cronScheduler)
                .Build();

            _scheduler.ScheduleJob(job, trigger);
        }

        public void CreateCheckSeriesNumbersSchedulerJob()
        {
            JobKey jobKey = new JobKey(ScheduleIdentity.CheckSeriesNumbersScheduler + "_JOB", ScheduleIdentity.CheckSeriesNumbersScheduler + "_GROUP");
            IJobDetail jobDetails = JobBuilder.Create<CheckSeriesNumbersJob>()
                .WithIdentity(jobKey)
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity(ScheduleIdentity.CheckSeriesNumbersScheduler + "TRIGGER", jobKey.Group)
                .StartNow()
#if DEBUG
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(5) // every 5 minutes
#else
                .WithSimpleSchedule(x => x.WithIntervalInHours(1) // every hour
#endif
                .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(jobDetails, trigger);
        }

        public void CreateExpiredProposalJob()
        {
            var cronScheduler = ConfigurationManager.AppSettings["ExpiredNotamsCronScheduler"] ?? "2 0 0 * * ?"; // or daily at @02:00
            
            JobKey jobKey = new JobKey(ScheduleIdentity.ExpiredProposalScheduler + "_JOB", ScheduleIdentity.ExpiredProposalScheduler + "_GROUP");
            IJobDetail expireProposalJob = JobBuilder.Create<CheckExpireProposalSyncJob>()
                .WithIdentity(jobKey)
                .Build();

            ITrigger expireProposalTrigger = TriggerBuilder.Create()
                .WithIdentity(ScheduleIdentity.ExpiredProposalScheduler + "_TRIGGER", jobKey.Group)
                .StartNow()
                .WithCronSchedule(cronScheduler)
                .Build();

            _scheduler.ScheduleJob(expireProposalJob, expireProposalTrigger);
        }

        public void CreateQueryListenerGuardJob()
        {
            var jobKey = new JobKey(ScheduleIdentity.NdsQueryGuard + "_JOB", ScheduleIdentity.NdsQueryGuard + "_GROUP");
            var job = JobBuilder.Create<QueryListenerGuardJob>()
                .WithIdentity(jobKey)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity(ScheduleIdentity.NdsQueryGuard + "_TRIGGER", jobKey.Group)
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(30)
                .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(job, trigger);
        }

        public void CreateHubConnectionTestSchedulerJob()
        {
            JobKey jobKey = new JobKey(ScheduleIdentity.HubConnectionTestScheduler + "_JOB", ScheduleIdentity.HubConnectionTestScheduler + "_GROUP" );

            IJobDetail testHubConnectionJob = JobBuilder.Create<HubConnectionTestJob>()
                .WithIdentity(jobKey)
                .Build();

            ITrigger testHubConnectionTrigger = TriggerBuilder.Create()
                .WithIdentity(ScheduleIdentity.HubConnectionTestScheduler + "_TRIGGER", jobKey.Group)
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(5) // every 5 minutes (check!!)
                .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(testHubConnectionJob, testHubConnectionTrigger);
        }

        public void CreateBroadcastNotamsToSubscribersJob()
        {
            lock (safetyLock)
            {

                JobKey jobKey = new JobKey(ScheduleIdentity.BroadcastNotamsToSubscribers + "_JOB", ScheduleIdentity.BroadcastNotamsToSubscribers + "_GROUP");
                var job = JobBuilder.Create<BroadcastNotamsToSubscribersJob>()
                    .WithIdentity(jobKey)
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithSimpleSchedule()
                    .StartAt(DateTime.UtcNow.AddYears(2000))
                    .WithIdentity(ScheduleIdentity.BroadcastNotamsToSubscribers + "_TRIGGER", jobKey.Group)
                    .Build();

                _scheduler.ScheduleJob(job, trigger);
            }
        }

        private IScheduler _scheduler;
        private readonly object safetyLock = new object();
    }
}
