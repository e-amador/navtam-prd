using NDS.SolaceVMR;

namespace NavCanada.Core.TaskEngine.Implementations
{
    using System;
    using System.Configuration;

    using Common.Contracts;
    using Data.NotamWiz.EF;
    using Data.NotamWiz.EF.Helpers;
    using NotificationEngine;
    using Proxies.DiagnosticProxy;
    using SqlServiceBroker;
    using Contracts;
    using MessageTask;
    using MtoProcessor;
    using QueueListener;
    using Proxies.AeroRdsProxy;
    using NavCanada.Core.Common.Security;

    public class TaskEngineSettings
    {
        public int MtoProcessorThreadCount { get; set; }
        public int MtoProcessorNapTime { get; set; }
        public int MaxPoisonMtoRetryCount { get; set; }
        public string ClusterDbConnectionString { get; set; }
    }

    public class TaskEngineShell : IClusterStorageFactory, IMtoProcessorFactory
    {
        public TaskEngineShell(ILogger logger)
        {
            _settings = ReadTaskEngineSettings();
            _logger = logger;

            var messageTaskFactory = new MessageTaskFactory();
            
            _mtoDispatcher = new TaskEngineMtoDispatcher(
                this,
                messageTaskFactory,
                this,
                logger,

                new TaskEngineDispatcherSettings
                {
                    MaxPoisonMtoRetryCount = _settings.MaxPoisonMtoRetryCount,
                    EnabledPerfCounter = ConfigurationManager.AppSettings["EnablePerfCounter"] != null && 
                        Convert.ToBoolean(ConfigurationManager.AppSettings["EnablePerfCounter"]),
                    SoonToExpireHrsDifference = Convert.ToInt32(ConfigurationManager.AppSettings["SoontoExpireHrsDifference"]),
                    InstanceName = ConfigurationManager.AppSettings["TaskEngineInstanceName"]
                });

            _externalServices = SetExternalServices();

            SetSchedulersWithJobs();


            var hubConnectionString = GetHubConnectionString();
            var hubQueue = ConfigurationManager.AppSettings["HubQueue"];
            _ndsQueueListener = NdsQueueListener.Create(new NdsQueueListenerContext(this, _logger, hubConnectionString, hubQueue));


        }

        private TaskEngineSettings ReadTaskEngineSettings()
        {
            return new TaskEngineSettings
            {
                MtoProcessorThreadCount = Convert.ToInt32(ConfigurationManager.AppSettings["MtoProcessorThreadCount"]),
                MtoProcessorNapTime = Convert.ToInt32(ConfigurationManager.AppSettings["MtoProcessorNapTime"]),
                MaxPoisonMtoRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPoisonMtoRetryCount"]),
                ClusterDbConnectionString = GetAppConnectionString(),
            };
        }

        private void SetSchedulersWithJobs()
        {
            _scheduleManager.CreateSdoEffectiveCacheSyncSchedulerJob();
            _scheduleManager.CreateSdoEffectiveCacheLoadSchedulerJob();
            _scheduleManager.CreateHubConnectionTestSchedulerJob();
            _scheduleManager.CreateChecklistSchedulerJob();
            _scheduleManager.CreateCheckSeriesNumbersSchedulerJob();
            _scheduleManager.CreateExpiredProposalJob();
            _scheduleManager.CreateQueryListenerGuardJob();
            _scheduleManager.CreateBroadcastNotamsToSubscribersJob();
        }

        public void Start()
        {
            _mtoDispatcher.StartProcessing(_settings.MtoProcessorThreadCount, _settings.MtoProcessorNapTime);
            _scheduleManager.Start();
            _ndsQueueListener.StartListening();
        }

        public void Stop()
        {
            _ndsQueueListener.StopListening();
            _scheduleManager.Stop();
            _mtoDispatcher.StopProcessing(1000);
        }

        public bool NewStorageConnection(out IClusterStorage clusterStorage)
        {
            try
            {
                var provider = new RepositoryProvider(new RepositoryFactories());
                var uow = new Uow(provider);

                IClusterQueues clusterQueues = new ClusterQueues(
                    new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings),
                    new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings));

                clusterStorage = new ClusterStorage(uow, clusterQueues);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogFatal(ex, typeof(TaskEngineShell));
                clusterStorage = null;
                return false;
            }
        }

        public IMtoProcessor CreateMtoProcessor(IClusterStorage repositories, IMessageTaskFactory taskFactory, MtoProcessorSettings mtoProcessorSettings)
        {
            return new MtoProcessor(repositories, _externalServices, taskFactory, mtoProcessorSettings);
        }

        #region Jobs and Schedulers

#endregion

        private IExternalServices SetExternalServices()
        {
            var useNotificationEngine = ConfigurationManager.AppSettings["AddNotifications"];
            var dianosticServiceEndpoint = ConfigurationManager.AppSettings["DianosticServiceEndpoint"];

            IDiagnosticProxy diagnosticProxy = null;
            if (!string.IsNullOrEmpty(dianosticServiceEndpoint))
            {
                var taskEngineInstanceName = ConfigurationManager.AppSettings["TaskEngineInstanceName"];
                if (taskEngineInstanceName != null)
                    diagnosticProxy = new DiagnosticProxy(dianosticServiceEndpoint + "/api/keepalive");
            }

            INotifier notifier = null;
            if (useNotificationEngine != null && useNotificationEngine == "True")
            {
                var host = ConfigurationManager.AppSettings["SmtpHost"];
                var port = ConfigurationManager.AppSettings["SmtpPort"];

                var senderEmailAddress = ConfigurationManager.AppSettings["SenderEmail"];

                if (string.IsNullOrEmpty(host))
                    throw new InvalidOperationException("The smtp host for notifications is required");

                if (string.IsNullOrEmpty(port))
                    throw new InvalidOperationException("The smtp port for notifications is required");

                if (string.IsNullOrEmpty(senderEmailAddress))
                    throw new InvalidOperationException("The sender email address for notifications is required");

                notifier = new Notifier(host, int.Parse(port), senderEmailAddress);
            }

            var solaceConnectionString = GetHubConnectionString();
            var solaceEndpoint = new SolaceNotificationEndpoint(solaceConnectionString);

            var aeroRdsFeaturesUrl = ConfigurationManager.AppSettings["AeroRDSFeaturesUrl"] ?? string.Empty;
            var aeroRdsProxy = new AeroRdsFeatureProxy(aeroRdsFeaturesUrl);

            _scheduleManager = new ScheduleManager();

            return new ExternalServices(diagnosticProxy, notifier, solaceEndpoint, aeroRdsProxy, _logger, _scheduleManager);
        }

        static string GetAppConnectionString() => 
            CipherUtils.DecryptConnectionString(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        static string GetHubConnectionString() =>
            CipherUtils.DecryptConnectionString(ConfigurationManager.ConnectionStrings["HubConnectionString"]?.ConnectionString);

        private readonly ILogger _logger;
        private readonly IMultithreadProcessor _mtoDispatcher;
        private readonly IExternalServices _externalServices;
        private readonly NdsQueueListener _ndsQueueListener;
        private readonly TaskEngineSettings _settings;
        private IScheduleManager _scheduleManager; 
    }
}
