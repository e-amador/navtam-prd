﻿namespace NavCanada.Core.TaskEngine.Implementations
{
    using System;
    using System.Threading;
    using Contracts;
    using NavCanada.Core.Common.Contracts;

    public enum SchedulerType
    {
        WorkFlow,
        AeroRds,
        Aimsl,
        PoisonMto,
        DeleteCompletedRecords,
        KeepAlive
    }

    /// <summary>
    /// Provides the logic for using multiple threads performing a parallel task 
    /// </summary>
    public abstract class MultithreadProcessor : IMultithreadProcessor
    {
        protected readonly ILogger _logger;
        private bool _processing;
        private CountdownEvent _countdownEvent;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private string _processorName = "Unknown";

        public MultithreadProcessor(ILogger logger)
        {
            _logger = logger;
        }

        protected string ProcessorName
        {
            get { return _processorName; }
            set { _processorName = value; }
        }

        /// <summary>
        /// Initializes and starts the processing of messages
        /// </summary>
        /// <param name="threadCount"/>Number of threads that will be running in parallel/>
        /// <param name="millisecondsNapTime">
        ///     Milliseconds that a thread sleeps, when the Process method returns false (indicating there is nothing to process),
        ///     before calling Process again.
        /// </param>
        public virtual void StartProcessing(int threadCount, int millisecondsNapTime)
        {
            if (_processing)
                throw new InvalidOperationException("Dispatcher is already processing messages");

           _logger.LogDebug(typeof(MultithreadProcessor), $"MultithreadProcessor.StartProcessing() begin {_processorName}");

            lock (this)
            {
                _processing = true;
                _countdownEvent = new CountdownEvent(threadCount);

                //Creating and starting all working threads
                for (var i = 0; i < threadCount; i++)
                {
                    var t = new Thread(() =>
                    {
                        while (_processing)
                        {
                            try
                            {
                                if (!Process() && millisecondsNapTime > 0)
                                    Thread.Sleep(millisecondsNapTime);
                            }
                            catch (Exception)
                            {
                                // this may shock the logging system for no reasons
                                //_logger.LogError(ex, typeof(MultithreadProcessor), null);
                            }
                        }

                        _countdownEvent.Signal();
                    });

                    _logger.LogDebug(typeof(MultithreadProcessor), $"Thread number {i} starting...");

                    t.Start();
                }
            }

            _logger.LogDebug(typeof(MultithreadProcessor), "MultithreadProcessor.StartProcessing() end");
        }

        /// <summary>
        /// Stops all threads and waits for all threads to exit processing for the specified time out time
        /// </summary>
        /// <param name="millisecondsTimeOut">Milliseconds that the method waits for all processes to stop</param>
        public virtual void StopProcessing(int millisecondsTimeOut)
        {
            lock (this)
            {
                if (!_processing)
                    throw new InvalidOperationException("Dispatcher is already processing messages");

                _processing = false;
                _cancellationTokenSource.Cancel();

               _logger.LogDebug(typeof(MultithreadProcessor), $"MultithreadProcessor.StopProcessing() fimished {_processorName}");
            }

            _countdownEvent.Wait(millisecondsTimeOut);
        }

        /// <summary>
        /// Override this method to specify your parallel processing logic. This method is re-entrant.
        /// </summary>
        /// <returns>True if any processing was done. False if nothing was processed.</returns>
        protected abstract bool Process();
    }
}
