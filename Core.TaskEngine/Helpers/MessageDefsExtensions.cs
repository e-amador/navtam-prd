﻿namespace NavCanada.Core.TaskEngine.Helpers
{
    using System;

    using Domain.Model.Enums;

    public static class MessageDefsExtensions
    {
        /// <summary>
        /// translates a rawmessagename string into an OBUInboundType
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static MessageTransferObjectId TranslateInboundMessageKeyToEnum(int messageId)
        {
            try
            {
                return (MessageTransferObjectId)messageId;
            }
            catch (ArgumentException)
            {
                return MessageTransferObjectId.Unhandled;
            }
        }
    }
}
