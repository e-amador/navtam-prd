﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Mto;
using System.Configuration;

namespace NavCanada.Core.TaskEngine.Helpers
{
    static class EmailHelper
    {
        static EmailHelper()
        {
            InDisasterRecoveryMode = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";
            AdministratorEmail = ConfigurationManager.AppSettings["AdministratorEmail"];
        }

        public static void QueueEmailMessage(IQueue queue, string recipients, string subject, string content)
        {
            // would not try to send emails when in 'disaster recovery mode'.
            if (!InDisasterRecoveryMode)
            {
                var emailMto = MtoHelper.CreateMtoMessage(MessageTransferObjectId.SendEmailNotitication, m =>
                {
                    m.Add(MessageDefs.EmailNotificationKey, true, DataType.Boolean);
                    m.Add(MessageDefs.EmailRecipientKey, recipients, DataType.String);
                    m.Add(MessageDefs.EmailSubjectKey, subject, DataType.String);
                    m.Add(MessageDefs.EmailContentKey, content, DataType.String);
                });
                queue.Publish(emailMto);
            }
        }

        public static string AdministratorEmail { get; }
        public static bool InDisasterRecoveryMode { get; }
    }
}
