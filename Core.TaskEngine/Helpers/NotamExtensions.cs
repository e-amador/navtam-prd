﻿using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.TaskEngine.Helpers
{
    static class NotamExtensions
    {
        public static string Summarize(this Notam notam) => $"ID: '{notam.Id}' Notam# {notam.NotamId} ItemA: '{notam.ItemA}' Operator: '{notam.Operator}' Originator: {notam.Originator} Originator Email: {notam.OriginatorEmail}";
    }
}
