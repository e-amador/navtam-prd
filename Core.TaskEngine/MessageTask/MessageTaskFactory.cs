﻿namespace NavCanada.Core.TaskEngine.MessageTask
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Model.Enums;
    using Contracts;
    using MtoProcessor;
    using Tasks;
    using Tasks.AeroRDS;
    using NavCanada.Core.TaskEngine.Tasks.NotificationTasks;
    using NavCanada.Core.TaskEngine.Tasks.DiagnosticTask;

    /// <summary>
    /// Default TaskFactory implementation. Resolves a list of tasks based on a provided MessageTaskContext
    /// <remarks>
    /// This class is thread safe. It supports multiple readers concurrently.
    /// </remarks>
    /// </summary>
    public class MessageTaskFactory : IMessageTaskFactory
    {
        /// <summary>
        /// Private Task instance dictionary
        /// </summary>
        private static readonly Dictionary<MessageTransferObjectId, List<IMessageTask>> registered_tasks = new Dictionary<MessageTransferObjectId, List<IMessageTask>>();

        static MessageTaskFactory()
        {
            RegisterTasks();
        }

        public List<IMessageTask> GetTasks(IMessageTaskContext context)
        {
            var inboundType = context.Mto.GetInboundType();
            List<IMessageTask> result;
            if (inboundType != MessageTransferObjectId.Unhandled && registered_tasks.TryGetValue(inboundType, out result))
                return result;

            var mtoId = context.Mto.GetMessageId();
            context.ExternalServices.Logger.LogWarn(typeof(MessageTaskFactory), $"No tasks registered for message id: '{mtoId.ToString() ?? "unknown" }");

            return new List<IMessageTask>();
        }

        private static void RegisterTasks()
        {
            // Send email notification task
            RegisterTasksForMessageTypes(new[]
            {
                typeof(EmailNotificationTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.SendEmailNotitication
            });
            // Set Expired proposal
            RegisterTasksForMessageTypes(new[]
            {
                typeof(SetExpiredProposalTask)
                //typeof(SignalrTask),
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.ExpireNotamProposal,
            });
            // Disseminate NOTAM
            RegisterTasksForMessageTypes(new[]
            {
                typeof(DisseminateNotamTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.DisseminateNotam
            });
            // AFTN Broadcast
            RegisterTasksForMessageTypes(new[]
            {
                typeof(AftnBroadcastTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.DistributeAftnBroadcastMessage
            });
            // Disseminate Message (any)
            RegisterTasksForMessageTypes(new[]
            {
                typeof(PublishMessageGroupTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.PublishMessageGroup
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(ProcessNdsRequestMessageTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.ProcessNdsRequestMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(ProcessOutboundMessageExchangeTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.ProccessExchangeMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(ProcessDisseminateJobTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.ProcessDisseminateJobMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(QueueNextDisseminationJobTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.QueueNextDisseminateJobMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(DisseminateChecklistsTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.DisseminateChecklistsMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(AeroRdsSyncActiveDataTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.AeroRDSSyncActiveDataMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(AeroRdsCacheEffectiveDataTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.AeroRDSCacheEffectiveDataMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(AeroRdsLoadCachedDataTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.AeroRDSLoadCachedDataMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(CheckSeriesNumbersTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.CheckSeriesNumbersMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(HubConnectionTestTask)
            }, new List<MessageTransferObjectId>
            {
                 MessageTransferObjectId.HubConnectionTestMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(DiagnosticTask)
            }, new List<MessageTransferObjectId>
            {
                 MessageTransferObjectId.Diagnostic
            });


            /*
            RegisterTasksForMessageTypes(new[]
            {
                typeof(EndNotamProposalTask),
                typeof(NotamEndedEmailTask),
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.EndNotamProposal,
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(NotamSoonToExpireEmailTask),
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.NotamSoonToExpireEmail
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(RegistrationEmailTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.RegistrationNotificationEmail
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(NotamRetryRejectEmailTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.NotamRetryRejectEmail
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(NotamRetryEndedEmailTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.NotamRetryEndedEmail
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(NotamRetryExpiredEmailTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.NotamRetryExpiredEmail
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(NotamRetrySoonToExpireEmailTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.NotamRetrySoonToExpiredEmail
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(RetryRegistrationEmailTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.RetryRegistrationEmailTask
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(ChatDiagnosticTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.Diagnostic
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(KeepAliveTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.KeepAliveSchedulerMsg
            });

            RegisterTasksForMessageTypes(new[]
            {
                typeof(DeleteCompletedRecordsTask)
            }, new List<MessageTransferObjectId>
            {
                MessageTransferObjectId.DeleteCompletedRecordsSchedulerMsg
            });
            */
        }

        private static void RegisterTasksForMessageTypes(IEnumerable<Type> taskTypes, List<MessageTransferObjectId> inboundTypes)
        {
            foreach (var taskType in taskTypes)
            {
                RegisterTaskForMessageTypes(taskType, inboundTypes);
            }
        }

        private static void RegisterTaskForMessageTypes(Type taskType, IEnumerable<MessageTransferObjectId> inboundTypes)
        {
            var taskInstance = Activator.CreateInstance(taskType) as IMessageTask;
            if (taskInstance == null) // taskType was successfully activated but it doesn't implement the IMessageTask interface
            {
                throw new ArgumentException(@"TaskType doesn't implement the IMessageTask interface", "taskType");
            }
            foreach (var inboundType in inboundTypes)
            {
                List<IMessageTask> inboundTypeTasks;
                if (!registered_tasks.TryGetValue(inboundType, out inboundTypeTasks))
                {
                    inboundTypeTasks = new List<IMessageTask>();
                    registered_tasks.Add(inboundType, inboundTypeTasks);
                }
                if (!inboundTypeTasks.Any(t => t.GetType().Equals(taskType)))
                {
                    inboundTypeTasks.Add(taskInstance);
                }
            }
        }
    }
}