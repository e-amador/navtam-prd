﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.TaskEngine.Contracts;

namespace NavCanada.Core.TaskEngine.NdsTasks
{
    public interface INdsTask
    {
        void Execute(NdsIncomingRequest request, IClusterStorage clusterStorage, ILogger logger);
    }
}