﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using System;

namespace NavCanada.Core.TaskEngine.NdsTasks
{
    public class AftnConfirmationTask : INdsTask
    {
        public void Execute(NdsIncomingRequest request, IClusterStorage clusterStorage, ILogger logger)
        {
            Guid messageGuid;
            if (Guid.TryParse(request.MessageId, out messageGuid))
            {
                var uow = clusterStorage.Uow;
                var message = uow.NdsOutgoingMessageRepo.GetById(messageGuid);
                if (message != null)
                {
                    message.Status = NdsMessageStatus.Confirmed;
                    uow.NdsOutgoingMessageRepo.Update(message);
                    uow.SaveChanges();
                    logger.LogDebug(GetType(), $"AFTN confirmation recieved for ID: {message.Id}");
                }
                else
                {
                    // unknown incoming acknowledgement message, ignore for now
                    logger.LogWarn(GetType(), "unknown incoming acknowledgement message");
                }
            }
        }
    }
}