﻿using System;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.TaskEngine.Contracts;
using NDS.Relay;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Common.Contracts;

namespace NavCanada.Core.TaskEngine.NdsTasks
{
    public class AftnMessageTask : INdsTask
    {
        public void Execute(NdsIncomingRequest request, IClusterStorage clusterStorage, ILogger logger)
        {
            var uow = clusterStorage.Uow;

            // try to parse the message, if fails it will return null
            var aftnMessage = AftnMessageParser.ParseMessage(request.Body);

            var recipients = aftnMessage?.DestinationAddress ?? AftnConsts.NOFAddress;
            var priorityCode = aftnMessage?.Priority ?? "GG";
            var body = aftnMessage?.Text ?? request.Body;
            var clientAddress = aftnMessage?.OriginAddress ?? AftnConsts.NOFAddress;
            var clientName = aftnMessage != null ? (uow.NdsClientRepo.GetClientName(aftnMessage.OriginAddress) ?? aftnMessage.OriginAddress) : "AFTN";

            var message = new MessageExchangeQueue
            {
                Id = Guid.NewGuid(),
                MessageType = MessageExchangeType.Notification,
                ClientType = NdsClientType.Aftn,
                Recipients = recipients,
                PriorityCode = priorityCode,
                Body = body,
                Inbound = true,
                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow,
                ClientAddress = clientAddress,
                ClientName = clientName
            };

            uow.MessageExchangeQueueRepo.Add(message);

            uow.SaveChanges();

        }
    }
}