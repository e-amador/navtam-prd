﻿namespace NavCanada.Core.TaskEngine.PerformanceCounters
{
    public class DummySimpleCounter : ISimpleCounter
    {
        public void Reset()
        {
        }

        public void Increment()
        {
        }

        public void IncrementBy(long amount)
        {
        }

        public void Decrement()
        {
        }
    }
}
