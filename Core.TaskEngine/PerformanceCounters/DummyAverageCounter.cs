﻿namespace NavCanada.Core.TaskEngine.PerformanceCounters
{
    class DummyAverageCounter : IAverageCounter
    {
        public long Start()
        {
            return 0;
        }

        public void Stop(long ticks)
        {
        }

        public void Reset()
        {
        }
    }
}
