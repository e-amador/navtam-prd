﻿namespace NavCanada.Core.TaskEngine.PerformanceCounters
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;

    public class AverageCounter : IAverageCounter
    {
        [DllImport("Kernel32.dll")]
        public static extern void QueryPerformanceCounter(ref long ticks);

        internal AverageCounter(PerformanceCounter counter, PerformanceCounter counterBase)
        {
            this.counter = counter;
            this.counterBase = counterBase;
        }

        public long Start()
        {
            long startTime = 0;
            QueryPerformanceCounter(ref startTime);

            return startTime;
        }

        public void Stop(long startTime)
        {
            long endTime = 0;
            QueryPerformanceCounter(ref endTime);
            this.counter.IncrementBy(endTime - startTime); // increment the timer by the time cost of the operation
            this.counterBase.Increment(); // increment base counter only by 1
        }

        public void Reset()
        {
            lock (this.lockObject)
            {
                this.counter.RawValue = 0;
                this.counterBase.RawValue = 0;
            }
        }

        private readonly PerformanceCounter counter;
        private readonly PerformanceCounter counterBase;
        private readonly object lockObject = new Object();
    }
}
