﻿namespace NavCanada.Core.TaskEngine.PerformanceCounters
{
    using System.Diagnostics;

    public class SimpleCounter : ISimpleCounter
    {
        internal SimpleCounter(PerformanceCounter counter)
        {
            this.counter = counter;
        }

        public void Reset()
        {
            this.counter.RawValue = 0;
        }

        public void Increment()
        {
            this.counter.Increment();
        }

        public void IncrementBy(long amount)
        {
            this.counter.IncrementBy(amount);
        }

        public void Decrement()
        {
            this.counter.Decrement();
        }

        private readonly PerformanceCounter counter;
    }
}
