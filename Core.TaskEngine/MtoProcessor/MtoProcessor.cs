﻿namespace NavCanada.Core.TaskEngine.MtoProcessor
{
    using Contracts;
    using MessageTask;
    using Mto;
    using NavCanada.Core.Common.Contracts;
    using PerformanceCounters;
    using SqlServiceBroker.Contracts;
    using System;
    using System.Runtime.InteropServices;

    public enum TaskEngineCounters
    {
        MtoNumberOperationCount,
        TaskEngineOutgoingMessageCount,
        MtoPerOperationAverageTime,
        MessagesProcessedPerSecond,
        MessagesInErrorPerSecond
    }

    public class MtoProcessor : IMtoProcessor
    {
        private readonly ILogger _logger;
        private readonly IClusterStorage _repos;

        private readonly IMessageTaskFactory _taskFactory;
        private readonly IExternalServices _externalServices;
        private readonly MtoProcessorSettings _mtoProcessorSettings;

        private ISimpleCounter _mtoNumberOperation;
        private ISimpleCounter _taskNumberOperation;
        private ISimpleCounter _messagesProcessedPerSecondCounter;
        private ISimpleCounter _messagesInErrorPerSecondCounter;
        private IAverageCounter _mtoAverageTimePerOperation;

        public MtoProcessor(IClusterStorage repositories, IExternalServices externalServices, IMessageTaskFactory taskFactory, MtoProcessorSettings mtoProcessorSettings)
        {
            _logger = externalServices.Logger;
            _logger.LogDebug(typeof(MtoProcessor), Message: "Task Engine MtoProcessor initialising...");

            if (repositories == null) throw new ArgumentNullException("repositories");
            if (taskFactory == null) throw new ArgumentNullException("taskFactory");
            _taskFactory = taskFactory;
            _repos = repositories;

            _externalServices = externalServices;
            _mtoProcessorSettings = mtoProcessorSettings;

            InitPerfCounters();
        }

        #region IMtoProcessor Implementation

        public void Run(MessageTransferObject mto)
        {
            var loggerType = GetType();
            try
            {
                CheckForMsgConversation(mto);

                var context = new MessageTaskContext
                {
                    ClusterStorage = _repos,
                    Mto = mto,
                    ExternalServices = _externalServices,
                    MtoProcessorSettings = _mtoProcessorSettings,
                };

                var tasks = _taskFactory.GetTasks(context);
                if (tasks != null)
                {
                    _mtoNumberOperation.Increment();
                    _messagesProcessedPerSecondCounter.Increment();

                    var startTime = _mtoAverageTimePerOperation.Start();
                    try
                    {
                        foreach (var task in tasks)
                        {
                            _taskNumberOperation.Increment();
                            task.Execute(context);
                        }
                    }
                    finally
                    {
                        _mtoAverageTimePerOperation.Stop(startTime);
                        _taskNumberOperation.Reset();
                    }
                }
                else
                {
                    _logger.LogWarn(loggerType, $"MtoProcessor got empty task list from task factory for mto: {mto}");
                }
            }
            catch (Exception)
            {
                _messagesInErrorPerSecondCounter.Increment();
                throw; 
            }
        }

        #endregion IMtoProcessor Implementation

        [DllImport("Kernel32.dll")]
        public static extern void QueryPerformanceCounter(ref long ticks);

        private void CheckForMsgConversation(MessageTransferObject mto)
        {
            var queueConversation = _repos.Queues.TaskEngineQueue as IConversationHandler;
            var senderConversationIdProp = mto[MessageDefs.SenderConversationIdKey];
            if (queueConversation != null && senderConversationIdProp != null)
            {
                var senderConversationId = Guid.Parse(senderConversationIdProp.Value.ToString());
                queueConversation.SendOnConversation(senderConversationId);
            }
        }

        private void InitPerfCounters()
        {
            try
            {
                const string categoryName = "NotamWiz.SingleInstance.TaskEngine";

                _mtoNumberOperation = PerfCounterManager.GetSimplePerfCounter(categoryName, TaskEngineCounters.MtoNumberOperationCount.ToString());
                _taskNumberOperation = PerfCounterManager.GetSimplePerfCounter(categoryName, TaskEngineCounters.TaskEngineOutgoingMessageCount.ToString());
                _messagesProcessedPerSecondCounter = PerfCounterManager.GetSimplePerfCounter(categoryName,
                   TaskEngineCounters.MessagesProcessedPerSecond.ToString());
                _messagesInErrorPerSecondCounter = PerfCounterManager.GetSimplePerfCounter(categoryName,
                   TaskEngineCounters.MessagesInErrorPerSecond.ToString());

                _mtoAverageTimePerOperation = PerfCounterManager.GetAveragePerfCounter(categoryName, TaskEngineCounters.MtoPerOperationAverageTime.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogError(Exception: ex, Type: typeof(MtoProcessor), Message: "InitPerfCounters(), There was an error configuring Performance Counters");
            }
        }
    }
}