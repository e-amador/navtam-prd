﻿namespace NavCanada.Core.TaskEngine.MtoProcessor
{
    using System;

    using Domain.Model.Enums;
    using Helpers;
    using Mto;

    /// <summary>
    /// Base message builder that allows parsing of the inbound message type.
    /// </summary>
    public static class MtoReader
    {
        public static int GetMessageId(this MessageTransferObject mto)
        {
            return Convert.ToInt32(mto[MessageDefs.MessageIdKey].Value);
        }

        public static MessageTransferObjectId GetInboundType(this MessageTransferObject mto)
        {
            return MessageDefsExtensions.TranslateInboundMessageKeyToEnum(mto.GetMessageId());
        }

        public static DateTime GetMessageTime(this MessageTransferObject mto)
        {
            if (mto[MessageDefs.MessageTimeKey] != null && mto[MessageDefs.MessageTimeKey].Value != null && (mto[MessageDefs.MessageTimeKey].Value is DateTime))
            {
                return Convert.ToDateTime(mto[MessageDefs.MessageTimeKey].Value);
            }
            return Convert.ToDateTime(mto[MessageDefs.ReceivedTimeKey].Value);
        }

        public static DateTime GetReceivedTime(this MessageTransferObject mto)
        {
            return Convert.ToDateTime(mto[MessageDefs.ReceivedTimeKey].Value);
        }

        public static MessageType GetMessageType(this MessageTransferObject mto)
        {
            object prop = mto[MessageDefs.MessageTypeKey];
            return (MessageType)byte.Parse(prop.ToString());
        }
    }
}