﻿namespace NavCanada.Core.Proxies.AimslProxy.CustomEncodings
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;
    using System.Xml;
    using System.Xml.Schema;

    using log4net;

    public class CustomParameterInspector : IParameterInspector, IDispatchMessageInspector, IClientMessageInspector
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(CustomParameterInspector));
        readonly XmlSchemaSet _schemas;

        public CustomParameterInspector()
        {
            _schemas = LoadSchemaSet();
        }

        private XmlSchemaSet LoadSchemaSet()
        {
            string[] schemaFiles = new[]
            {
                "aimsl_dp.xsd", "ino_dp_common.xsd", "ino_dp_notam_common.xsd", "ino_dp_notam_request.xsd",
                "ino_dp_notam_response.xsd"
            };

            XmlSchemaSet schemaSet = new XmlSchemaSet();

            var assembly = Assembly.GetExecutingAssembly();

            foreach (var xsd in schemaFiles)
            {
                var resourceName = assembly.GetManifestResourceNames().Single(a => a.EndsWith(xsd));

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    XmlReader xmlReader = new XmlTextReader(streamReader);
                    schemaSet.Add(XmlSchema.Read(xmlReader, null));
                }

            }

            return schemaSet;
        }

        void ValidateMessage(ref Message message)
        {
            XmlDocument bodyDoc = new XmlDocument();

            bodyDoc.Load(message.GetReaderAtBodyContents());

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(_schemas);
            settings.ValidationFlags = XmlSchemaValidationFlags.ProcessSchemaLocation;
            settings.ValidationType = ValidationType.Schema;
            XmlReader r = XmlReader.Create(new XmlNodeReader(bodyDoc), settings);
            while (r.Read()) ; // do nothing, just validate
            // Create new message 
            Message newMsg = Message.CreateMessage(message.Version, null,
                new XmlNodeReader(bodyDoc.DocumentElement));
            newMsg.Headers.CopyHeadersFrom(message);
            foreach (string propertyKey in message.Properties.Keys)
                newMsg.Properties.Add(propertyKey, message.Properties[propertyKey]);
            // Close the original message and return new message 
            message.Close();
            message = newMsg;
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            return null;
        }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            throw new NotImplementedException();
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            throw new NotImplementedException();
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            try
            {
                ValidateMessage(ref request);
            }
            catch (XmlSchemaValidationException xmlValidationEx)
            {
                _log.Error("AIMSL XML Message did not pass XSD validation", xmlValidationEx);
                throw;
            }
            catch (FaultException e)
            {
                _log.Error("Error validating AIMSL XML Message", e);
                throw new FaultException<string>(e.Message);
            }
            return null;
        }
    }
}
