﻿using NavCanada.Core.Proxies.AimslProxy.Common;

namespace NavCanada.Core.Proxies.AimslProxy.Mappers
{
    using AutoMapper;

    using Domain.Model.Entitities;
    using Domain.Model.Enums;
    using AimslService;
    using Converters;

    public static class AutoMapperConfig
    {
        public static void RegisterMaps()
        {
            Mapper.CreateMap<NotamProposal, NotamProposalStoreMessageType>().ConvertUsing(new NotamProposal2ProxyNotamProposalStoreMessageTypeConvertor());
            Mapper.CreateMap<SingleNotamProposalStatusResponseType, NotamProposalStatusCode>().ConvertUsing(new NotamProposalStatusType2NotamProposalStatusCode());
            Mapper.CreateMap<long, NotamProposalStatusMessageType>().ConvertUsing(new Long2NotamProposalStatusMessageType());
            Mapper.CreateMap<long, NotamProposalRejectMessageType>().ConvertUsing(new Long2NotamProposalRejectMessageType());

            Mapper.CreateMap<long[], NotamProposalStatusMessageType>().ConvertUsing(new LongArray2NotamProposalStatusMessageType());

            Mapper.CreateMap<NotamProposalStoreResponseMessageType, AismlResponse>().ConvertUsing(new NotamProposalStoreResponseMessageType2AismlResponse());
            Mapper.CreateMap<NotamProposalStatusResponseMessageType, AismlResponse>().ConvertUsing(new NotamProposalStatusResponseMessageType2AimlsResponse());
            Mapper.CreateMap<NotamProposalRejectResponseMessageType, AismlResponse>().ConvertUsing(new NotamProposalRejectResponseMessageType2AimlsResponse());
            Mapper.CreateMap<NotamProposalBaseType, Notam>()
                 .IgnoreAllNonExisting()
                 .ForMember(dest => dest.Nof, opt => opt.MapFrom(src => src.NOF))
                 .ForMember(dest => dest.Code23, opt => opt.MapFrom(src => src.QLine.Code23))
                 .ForMember(dest => dest.Code45, opt => opt.MapFrom(src => src.QLine.Code45))
                 .ForMember(dest => dest.Fir, opt => opt.MapFrom(src => src.QLine.FIR))
                 .ForMember(dest => dest.Lower, opt => opt.MapFrom(src => src.QLine.Lower))
                 .ForMember(dest => dest.Purpose, opt => opt.MapFrom(src => src.QLine.Purpose))
                 .ForMember(dest => dest.Scope, opt => opt.MapFrom(src => src.QLine.Scope))
                 .ForMember(dest => dest.Traffic, opt => opt.MapFrom(src => src.QLine.Traffic))
                 .ForMember(dest => dest.Upper, opt => opt.MapFrom(src => src.QLine.Upper))
                 .ForMember(dest => dest.ItemA, opt => opt.MapFrom(src =>  string.Join(" ", src.ItemA)));

            Mapper.CreateMap<NotamProposal, AimslServiceMock.NotamProposal>().ConvertUsing(new NotamMProposal2ProxyNotamProposalConvertor());
            Mapper.CreateMap<AimslServiceMock.NotamProposalStatusMessage, AismlResponse>().ConvertUsing(new NotamProposalStatusMessage2AimslMockResultMock());
            Mapper.CreateMap<AimslServiceMock.NotamProposalStatusType, NotamProposalStatusCode>().ConvertUsing(new NotamProposalStatusType2NotamProposalStatusCodeMock());

            Mapper.CreateMap<AimslServiceMock.Notam, Notam>()
                .ForMember(a => a.Code23, opt => opt.MapFrom(s => s.QLine.Code23))
                .ForMember(a => a.Code45, opt => opt.MapFrom(s => s.QLine.Code45))
                .ForMember(a => a.Fir, opt => opt.MapFrom(s => s.QLine.FIR))
                .ForMember(a => a.Lower, opt => opt.MapFrom(s => s.QLine.Lower))
                .ForMember(a => a.Purpose, opt => opt.MapFrom(s => s.QLine.Purpose))
                .ForMember(a => a.Scope, opt => opt.MapFrom(s => s.QLine.Scope))
                .ForMember(a => a.Traffic, opt => opt.MapFrom(s => s.QLine.Traffic))
                .ForMember(a => a.Upper, opt => opt.MapFrom(s => s.QLine.Upper))
                .ForMember(a => a.Permanent, opt => opt.Ignore());
        }

    }
}
