﻿using AutoMapper;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    public class NotamMProposal2ProxyNotamProposalConvertor : ITypeConverter<NotamProposal, AimslServiceMock.NotamProposal>
    {
        public AimslServiceMock.NotamProposal Convert(ResolutionContext context)
        {

            var aismlNotamProposal = context.SourceValue as NotamProposal;


            AimslServiceMock.QLineType qLine = new AimslServiceMock.QLineType();

            qLine.FIR = aismlNotamProposal.Fir;
            qLine.Code23 = aismlNotamProposal.Code23;
            qLine.Code45 = aismlNotamProposal.Code45;
            //qLine.Traffic = (AimslServiceMock.TrafficType)aismlNotamProposal.Traffic;
            qLine.Purpose = (AimslServiceMock.PurposeType)aismlNotamProposal.Purpose;
            //qLine.Scope = (AimslServiceMock.ScopeType)aismlNotamProposal.Scope;
            qLine.Lower = aismlNotamProposal.Lower;
            qLine.Upper = aismlNotamProposal.Upper;

            AimslServiceMock.NotamProposal nv = new AimslServiceMock.NotamProposal
            {
                Id = aismlNotamProposal.Id,
                NOF = aismlNotamProposal.Nof,
                Series = aismlNotamProposal.Series,
                Number = aismlNotamProposal.Number,
                Year = aismlNotamProposal.Year,
                Type = (AimslServiceMock.NotamTypeType)aismlNotamProposal.Type,
                ReferredSeries = aismlNotamProposal.ReferredSeries,
                ReferredNumber = aismlNotamProposal.ReferredNumber,
                ReferredYear = aismlNotamProposal.ReferredYear,
                QLine = qLine,
                Coordinates = aismlNotamProposal.Coordinates,
                Radius = aismlNotamProposal.Radius,
                ItemA = aismlNotamProposal.ItemA,
                StartValidity = System.Convert.ToString(aismlNotamProposal.StartValidity),
                EndValidity = System.Convert.ToString(aismlNotamProposal.EndValidity),
                Estimated = aismlNotamProposal.Estimated,
                ItemD = aismlNotamProposal.ItemD,
                ItemE = aismlNotamProposal.ItemE,
                ItemEFrench = aismlNotamProposal.ItemEFrench,
                ItemF = aismlNotamProposal.ItemF,
                ItemG = aismlNotamProposal.ItemG,
                ItemX = aismlNotamProposal.ItemX,
                Operator = aismlNotamProposal.Operator,
                Status = (AimslServiceMock.NotamProposalStatusType)aismlNotamProposal.Status,
                StatusTime = aismlNotamProposal.StatusTime,
                RejectionReason = aismlNotamProposal.RejectionReason,
                NoteToNOF = aismlNotamProposal.NoteToNof

            };

            if (aismlNotamProposal.IsCatchAll)
            {
                nv.NoteToNOF += "\n#CATCH ALL#";
            }
            else if (aismlNotamProposal.ContainFreeText)
            {
                nv.NoteToNOF += "\n#FREE TEXT#";
            }

            return nv;
        }
    }

}
