﻿using NavCanada.Core.Proxies.AimslProxy.Common;

namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    using System;
    using System.Linq;

    using AutoMapper;

    using Domain.Model.Enums;
    using AimslService;

    public class NotamProposalStoreResponseMessageType2AismlResponse : ITypeConverter<NotamProposalStoreResponseMessageType, AismlResponse>
    {
        public AismlResponse Convert(ResolutionContext context)
        {
            var source = context.SourceValue as NotamProposalStoreResponseMessageType;

            if (source == null)
                throw new NullReferenceException("NotamProposalStoreResponseMessageType can't be null after a call to Aimls");

            var response = new AismlResponse
            {
                Code = source.Errors.Any() ? ApiStatusCode.Error : ApiStatusCode.Success,
            };

            if (response.Code == ApiStatusCode.Success)
            {
                response.ProposalStatus = new NotamProposalStatus
                {
                    Status = NotamProposalStatusCode.Queued,
                    StatusDateTime = DateTime.Now,
                    InoNotamProposalId = long.Parse(source.NotamProposalStore.NotamProposal.NotamProposalIdentifier)
                };
            }
            else
            {
                var errorMessage = "";
                foreach (var error in source.Errors)
                    errorMessage += string.Format("{0}[{1}][{2}]:{3}", errorMessage.Length > 0 ? ", " : "", error.msgType, error.msgNumber, error.msgText);

                response.ErrorMessage = errorMessage;
            }
            
            return response;
        }
    }
}

