﻿using AutoMapper;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AimslProxy.AimslServiceMock;

namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    class NotamProposalStatusType2NotamProposalStatusCodeMock : ITypeConverter<NotamProposalStatusType, NotamProposalStatusCode>
    {
        public NotamProposalStatusCode Convert(ResolutionContext context)
        {
            NotamProposalStatusType source = (NotamProposalStatusType)context.SourceValue;

            switch (source)
            {
                case NotamProposalStatusType.Pending:
                    return NotamProposalStatusCode.Pending;
                case NotamProposalStatusType.ModifiedAndSubmitted:
                    return NotamProposalStatusCode.ModifiedAndSubmitted;
                case NotamProposalStatusType.Queued:
                    return NotamProposalStatusCode.Queued;
                case NotamProposalStatusType.Rejected:
                    return NotamProposalStatusCode.Rejected;
                case NotamProposalStatusType.Submitted:
                    return NotamProposalStatusCode.Submitted;
                case NotamProposalStatusType.Withdrawn:
                    return NotamProposalStatusCode.Withdrawn;
                case NotamProposalStatusType.Undefined:
                default:
                    return NotamProposalStatusCode.Undefined;
            }

        }

    }
}
