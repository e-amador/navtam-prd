﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AimslProxy.Common;
using NotamProposalStatus = NavCanada.Core.Proxies.AimslProxy.Common.NotamProposalStatus;

namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    using System;
    using System.Linq;

    using AutoMapper;

    using Domain.Model.Enums;
    using AimslService;

    public class NotamProposalStatusResponseMessageType2AimlsResponse : ITypeConverter<NotamProposalStatusResponseMessageType, AismlResponse>
    {
        public AismlResponse Convert(ResolutionContext context)
        {
            var source = context.SourceValue as NotamProposalStatusResponseMessageType;

            if (source == null)
                throw new NullReferenceException("NotamProposalStatusResponseMessageType can't be null after a call to Aimls");

            var response = new AismlResponse
            {
                Code = source.Errors.Any() ? ApiStatusCode.Error : ApiStatusCode.Success,
            };

            if (response.Code == ApiStatusCode.Success)
            {
                response.ProposalStatus = new NotamProposalStatus
                {
                    //Status = NotamProposalStatusCode.Queued,
                    Status = Mapper.Map<SingleNotamProposalStatusResponseType, NotamProposalStatusCode>(source.NotamProposalStatus.NotamProposal[0]),
                    StatusDateTime = DateTime.Now,
                    InoNotamProposalId = long.Parse(source.NotamProposalStatus.NotamProposal[0].NotamProposalIdentifier),
                    SubmittedNotam = Mapper.Map<NotamProposalBaseType, Notam>(source.NotamProposalStatus.NotamProposal[0].Notam),
                    RejectionReason = source.NotamProposalStatus.NotamProposal[0].Reason
                };
            }
            else
            {
                var errorMessage = "";
                foreach (var error in source.Errors)
                    errorMessage += string.Format("{0}[{1}][{2}]:{3}", errorMessage.Length > 0 ? ", " : "", error.msgType, error.msgNumber, error.msgText);

                response.ErrorMessage = errorMessage;
            }

            return response;
        }
    }
}


