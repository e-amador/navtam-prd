﻿using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    using System;

    using AutoMapper;

    using AimslService;

    public class LongArray2NotamProposalStatusMessageType : ITypeConverter<long[], NotamProposalStatusMessageType>
    {
        public NotamProposalStatusMessageType Convert(ResolutionContext context)
        {
            var obj = context.SourceValue;
            if (obj == null)
                throw new NullReferenceException("NotamPorposalIds can't be null");

            long[] notamProposalIds = (long[])obj;

            var proposals = new List<SingleNotamProposalStatusType>();
            for (var i = 0; i < notamProposalIds.Length; i++)
            {
                proposals.Add(new SingleNotamProposalStatusType
                {
                    NotamProposalIdentifier = notamProposalIds[i].ToString()
                });
            }

            var response = new NotamProposalStatusMessageType
            {
                transaction = new transactionType
                {
                    uuid = Guid.NewGuid().ToString()
                },
                RequestHeader = new RequestHeaderNotamType
                {
                    Version = "2"
                },
                NotamProposalStatus = new NotamProposalStatusType
                {
                    Action = ActionType.Query,
                    NotamProposal = proposals.ToArray()
                }
            };

            return response;
        }
    }
}

