﻿namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    using System;

    using AutoMapper;

    using Domain.Model.Entitities;
    using AimslService;

    public class NotamProposal2ProxyNotamProposalStoreMessageTypeConvertor : ITypeConverter<NotamProposal, NotamProposalStoreMessageType>
    {
        public NotamProposalStoreMessageType Convert(ResolutionContext context)
        {
            var notamProposal = context.SourceValue as NotamProposal;

            if (notamProposal == null)
                return null;

            var  notamProposalStoreMessageType = new NotamProposalStoreMessageType
            {
                RequestHeader = new RequestHeaderNotamType
                {
                    Version = "2"
                },
                transaction = new transactionType
                {
                    uuid = Guid.NewGuid().ToString()
                },
                NotamProposalStore = new NotamProposalStoreType
                {
                    Action = ActionType.Store,
                    NotamProposal = new NotamProposalBaseType
                    {
                        Series = notamProposal.Series,
                        Number = notamProposal.Number == 0 ? null : notamProposal.Number.ToString(),
                        NOF = notamProposal.Nof,
                        NotesToNof = notamProposal.NoteToNof + (notamProposal.IsCatchAll ? "\n#CATCH ALL#" : (notamProposal.ContainFreeText ? "\n#FREE TEXT#" : "")),
                        Coordinates = notamProposal.Coordinates.Replace(" ",string.Empty),
                        StartValidity = notamProposal.StartValidity.HasValue ? notamProposal.StartValidity.Value.ToString("yyMMddHHmm") : null,
                        EndValidity = notamProposal.EndValidity.HasValue ? notamProposal.EndValidity.Value.ToString("yyMMddHHmm") : null,
                        ItemA = new[] { notamProposal.ItemA },
                        ItemE = notamProposal.ItemE,
                        ItemD = notamProposal.ItemD,
                        ItemF = notamProposal.ItemF,
                        ItemG = notamProposal.ItemG,
                        ItemX = notamProposal.ItemX,
                        MultiLanguage = !string.IsNullOrEmpty(notamProposal.ItemEFrench) 
                        ?   new MultiLanguageType
                            {
                                ItemE = new[]
                                {
                                    new MultiLanguageTypeItemE
                                    {
                                        language = "FRENCH",
                                        Value = notamProposal.ItemEFrench
                                    }
                                }
                            }
                        : null,
                        EstimationSpecified = notamProposal.Estimated,
                        ReferredSeries = notamProposal.ReferredSeries,
                        ReferredNumber = notamProposal.ReferredNumber.ToString(),
                        ReferredYear = notamProposal.ReferredYear.ToString(),
                        Operator = notamProposal.Operator,
                        QLine = new QLineProposalType
                        {
                            Code23 = notamProposal.Code23,
                            Code45 = notamProposal.Code45,
                            FIR = notamProposal.Fir,
                            Lower = notamProposal.Lower.ToString(),
                            Upper = notamProposal.Upper.ToString(),
                            Purpose = (PurposeType)notamProposal.Purpose,
                            PurposeSpecified = true,
                            //Scope = (ScopeType)notamProposal.Scope,
                            ScopeSpecified = true,
                            //Traffic = (TrafficType)notamProposal.Traffic,
                            TrafficSpecified = true,
                        },
                        Radius = notamProposal.Radius.ToString(),
                        Type = (NotamTypeType)notamProposal.Type,
                        TypeSpecified = true,
                        Year = notamProposal.Year.ToString()
                    }
                }
            };

            return notamProposalStoreMessageType;
        }
    }
}
