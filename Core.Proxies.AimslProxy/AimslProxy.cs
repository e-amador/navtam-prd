﻿using System.Collections.Generic;
using System.Linq;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AimslProxy.Common;
using NotamProposalStatus = NavCanada.Core.Proxies.AimslProxy.Common.NotamProposalStatus;

namespace NavCanada.Core.Proxies.AimslProxy
{
    using System;
    using System.Security.Cryptography.X509Certificates;
    using System.ServiceModel;
    using System.ServiceModel.Security;

    using AutoMapper;

    using log4net;

    using Domain.Model.Enums;
    using AimslService;
    using Mappers;

    public class AimslProxy : IAimslProxy
    {
        readonly ILog _logger = LogManager.GetLogger(typeof(AimslProxy));
        private readonly notamPortTypeClient _client;

        public AimslProxy(string endoointName, EndpointAddress endpointAddress, X509Certificate2 certificate)
        {
            AutoMapperConfig.RegisterMaps();

            _client = new notamPortTypeClient(endoointName, endpointAddress);
            var clientCredentials = _client.ClientCredentials;
            if (clientCredentials == null)
                throw new NullReferenceException("ClientCredentials");

            clientCredentials.ClientCertificate.Certificate = certificate;
            clientCredentials.ServiceCertificate.DefaultCertificate = certificate;
            clientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
        }

        public AismlResponse Submit(NotamProposal proposal, string accountableSource)
        {
            try
            {
                var notamProposalStoreMessageType = Mapper.Map<NotamProposal, NotamProposalStoreMessageType>(proposal);
                notamProposalStoreMessageType.NotamProposalStore.NotamProposal.AccountableSource = accountableSource;

                NotamProposalStoreResponseMessageType response = _client.notamProposalStore(notamProposalStoreMessageType);

                return Mapper.Map<NotamProposalStoreResponseMessageType, AismlResponse>(response);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }


        public AismlResponse CheckStatus(long notamProposalId)
        {
            try
            {
                NotamProposalStatusResponseMessageType response =
                    _client.notamProposalStatus(Mapper.Map<long, NotamProposalStatusMessageType>(notamProposalId));

                return Mapper.Map<NotamProposalStatusResponseMessageType, AismlResponse>(response);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }

        public List<AismlResponse> CheckStatus(long[] notamProposalIds)
        {
            var aimsResponseList = new List<AismlResponse>();
            try
            {
                NotamProposalStatusResponseMessageType response = _client.notamProposalStatus(Mapper.Map<long[], NotamProposalStatusMessageType>(notamProposalIds));

                var aimsResponse = new AismlResponse
                {
                    Code = response.Errors.Any() ? ApiStatusCode.Error : ApiStatusCode.Success,
                };

                if (aimsResponse.Code == ApiStatusCode.Success)
                {
                    foreach (var proposal in response.NotamProposalStatus.NotamProposal)
                    {
                        aimsResponse.ProposalStatus = new NotamProposalStatus
                        {
                            Status = Mapper.Map<SingleNotamProposalStatusResponseType, NotamProposalStatusCode>(proposal),
                            StatusDateTime = DateTime.Now,
                            InoNotamProposalId = long.Parse(proposal.NotamProposalIdentifier),
                            SubmittedNotam = Mapper.Map<NotamProposalBaseType, Notam>(proposal.Notam)

                        };
                        aimsResponseList.Add(aimsResponse);
                    }
                }
                else
                {
                    var errorMessage = "";
                    foreach (var error in response.Errors)
                        errorMessage += string.Format("{0}[{1}][{2}]:{3}", errorMessage.Length > 0 ? ", " : "", error.msgType, error.msgNumber, error.msgText);

                    aimsResponseList.Add(new AismlResponse
                    {
                        Code = ApiStatusCode.ConnectionError,
                        ErrorMessage = errorMessage
                    });
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                aimsResponseList.Add( new AismlResponse 
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                });
            }

            return aimsResponseList;
        }


        public AismlResponse Widthraw(long notamProposalId)
        {
            try
            {
                NotamProposalRejectResponseMessageType response =
                    _client.notamProposalReject(Mapper.Map<long, NotamProposalRejectMessageType>(notamProposalId));

                return Mapper.Map<NotamProposalRejectResponseMessageType, AismlResponse>(response);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }
    }
}
