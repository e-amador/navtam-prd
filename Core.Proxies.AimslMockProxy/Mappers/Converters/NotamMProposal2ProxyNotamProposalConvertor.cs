﻿namespace NavCanada.Core.Proxies.AimslMockProxy.Mappers.Converters
{
    using AutoMapper;

    using NavCanada.Core.Domain.Model.Entitities;

    public class NotamMProposal2ProxyNotamProposalConvertor : ITypeConverter<NotamProposal, Proxy.NotamProposal>
    {
        public Proxy.NotamProposal Convert(ResolutionContext context)
        {

            var aismlNotamProposal = context.SourceValue as NotamProposal;


            Proxy.QLineType qLine = new Proxy.QLineType();

            qLine.FIR = aismlNotamProposal.Fir;
            qLine.Code23 = aismlNotamProposal.Code23;
            qLine.Code45 = aismlNotamProposal.Code45;
            qLine.Traffic = (Proxy.TrafficType)aismlNotamProposal.Traffic;
            qLine.Purpose = (Proxy.PurposeType)aismlNotamProposal.Purpose;
            qLine.Scope = (Proxy.ScopeType)aismlNotamProposal.Scope;
            qLine.Lower = aismlNotamProposal.Lower;
            qLine.Upper = aismlNotamProposal.Upper;

            Proxy.NotamProposal nv = new Proxy.NotamProposal
            {
                Id = aismlNotamProposal.Id,
                NOF = aismlNotamProposal.Nof,
                Series = aismlNotamProposal.Series,
                Number = aismlNotamProposal.Number,
                Year = aismlNotamProposal.Year,
                Type = (Proxy.NotamTypeType)aismlNotamProposal.Type,
                ReferredSeries = aismlNotamProposal.ReferredSeries,
                ReferredNumber = aismlNotamProposal.ReferredNumber,
                ReferredYear = aismlNotamProposal.ReferredYear,
                QLine = qLine,
                Coordinates = aismlNotamProposal.Coordinates,
                Radius = aismlNotamProposal.Radius,
                ItemA = aismlNotamProposal.ItemA,
                StartValidity = System.Convert.ToString(aismlNotamProposal.StartValidity),
                EndValidity = System.Convert.ToString(aismlNotamProposal.EndValidity),
                Estimated = aismlNotamProposal.Estimated,
                ItemD = aismlNotamProposal.ItemD,
                ItemE = aismlNotamProposal.ItemE,
                ItemEFrench = aismlNotamProposal.ItemEFrench,
                ItemF = aismlNotamProposal.ItemF,
                ItemG = aismlNotamProposal.ItemG,
                ItemX = aismlNotamProposal.ItemX,
                Operator = aismlNotamProposal.Operator,
                Status = (Proxy.NotamProposalStatusType)aismlNotamProposal.Status,
                StatusTime = aismlNotamProposal.StatusTime,
                RejectionReason = aismlNotamProposal.RejectionReason,
                NoteToNOF = aismlNotamProposal.NoteToNof

            };

            if (aismlNotamProposal.IsCatchAll)
            {
                nv.NoteToNOF += "\n#CATCH ALL#";
            }
            else if (aismlNotamProposal.ContainFreeText)
            {
                nv.NoteToNOF += "\n#FREE TEXT#";
            }

            return nv;
        }
    }

}
