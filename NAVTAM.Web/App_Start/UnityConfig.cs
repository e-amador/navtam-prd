﻿using Core.Diagnoistics.SeriLog;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.SqlServiceBroker;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.Helpers;
using NAVTAM.SignalrHub;
using System.Configuration;
using System.Data.Entity;
using System.Web;

namespace NAVTAM
{
    public static class UnityConfig
    {
        public static void RegisterComponents(System.Web.Http.HttpConfiguration config)
        {
			var container = new UnityContainer();

            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            //Register as a singleton
            container.RegisterInstance(new RepositoryFactories(), new ContainerControlledLifetimeManager());
            container.RegisterInstance(new NsdManagerFactories(), new ContainerControlledLifetimeManager());
            container.RegisterType<INsdManagerResolver, NsdManagerResolver>(new ContainerControlledLifetimeManager());

            //Register as always new instance

            container.RegisterInstance(new RepositoryProvider(container.Resolve<RepositoryFactories>()));
            container.RegisterType<IRepositoryProvider, RepositoryProvider>(new TransientLifetimeManager());
            container.RegisterType<INotamQueuePublisher, ServiceBrokerQueuePublisher>(new TransientLifetimeManager());
            container.RegisterType<IEventHubContext, EventHubContext>(new TransientLifetimeManager());

            //AeroRDS proxy
            //var aeroRdsProxy = new AeroRdsProxy(ConfigurationManager.AppSettings.Get("aeroRDS_Services_URL"));
            //container.RegisterInstance(aeroRdsProxy);
            container.RegisterType<IAeroRdsProxy, AeroRdsProxy>(new ContainerControlledLifetimeManager(), new InjectionConstructor(ConfigurationManager.AppSettings.Get("aeroRDS_Services_URL")));

            container.RegisterType<IUserStore<UserProfile>, UserStore<UserProfile>>();

            container.RegisterType<DbContext, AppDbContext>(new InjectionConstructor("DefaultConnection"));
            var uow = new Uow(container.Resolve<AppDbContext>(), container.Resolve<RepositoryProvider>());

            //Register as always a new instance
            container.RegisterInstance(uow);
            container.RegisterType<IUow, Uow>(new TransientLifetimeManager());

            container.RegisterType<IDashboardContext, DashboardContext>(new TransientLifetimeManager());

            container.RegisterType<IAuthenticationManager>(
                   new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            IQueue initiatorToTaskEngineSsbSettings = new SqlServiceBrokerQueue(container.Resolve<AppDbContext>(), SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            IQueue taskEngineToInitiatorSsbSettings = new SqlServiceBrokerQueue(container.Resolve<AppDbContext>(), SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings);

            container.RegisterType<IQueue, SqlServiceBrokerQueue>(new TransientLifetimeManager());

            IClusterQueues queues = new ClusterQueues(
                    initiatorToTaskEngineSsbSettings,
                    taskEngineToInitiatorSsbSettings);

            container.RegisterInstance(queues);
            container.RegisterType<IClusterQueues, ClusterQueues>(new TransientLifetimeManager());

            // Register GeoLocationCache as a singleton
            container.RegisterType<IGeoLocationCache, HttpContextGeoLocationCache>(new ContainerControlledLifetimeManager());
            // Register Logging as a singleton
            container.RegisterType<ILogger, LoggerSeri>(new ContainerControlledLifetimeManager());            

            config.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
            System.Web.Http.GlobalConfiguration.Configuration.Dependency‌​Resolver = new Unity.WebApi.UnityDependencyResolver(container);
            System.Web.Mvc.DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
        }
    }
}