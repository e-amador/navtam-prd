﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NAVTAM.Helpers
{
    public class DisseminationCoordinator
    {
        private DisseminationCoordinator(IUow uow, int year, int maxSeriesNumber)
        {
            _uow = uow;
            _year = year;
            _maxSeriesNumber = maxSeriesNumber;
            _seriesNumbers = new Dictionary<char, SeriesNumber>();
        }

        public static DisseminationCoordinator Create(IUow uow, int year, int maxSeriesNumber)
        {
            return new DisseminationCoordinator(uow, year, maxSeriesNumber);
        }

        public void CommitGroup()
        {
            foreach (var seriesNumber in _seriesNumbers.Values)
            {
                _uow.SeriesNumberRepo.SafeUpdateSeriesNumber(seriesNumber);
            }
        }

        public void DiscardGroup()
        {
            _seriesNumbers.Clear();
        }

        public async Task<StepResult> Disseminate(char series, Func<int, Task<StepResult>> disseminateAction)
        {
            SeriesNumber seriesNumber;
            if (!_seriesNumbers.TryGetValue(series, out seriesNumber))
            {
                seriesNumber = _uow.SeriesNumberRepo.GetSeriesNumber(series, _year);
                if (seriesNumber == null)
                    return StepResult.Fail($"Series '{series}' not initialized!");
                _seriesNumbers.Add(series, seriesNumber);
            }

            // Note: Series never used, the number is zero.
            var number = Math.Max(1, seriesNumber.Number);

            seriesNumber.Number = number < _maxSeriesNumber ? number + 1 : 1;
            return await disseminateAction.Invoke(number);
        }

        readonly IUow _uow;
        readonly int _maxSeriesNumber;
        readonly int _year;
        readonly Dictionary<char, SeriesNumber> _seriesNumbers;
    }
}