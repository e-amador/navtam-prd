﻿using Microsoft.SqlServer.Types;
using NavCanada.Core.Proxies.AeroRdsProxy.System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Device.Location;

namespace NAVTAM.Helpers
{
    public static class DbGeographyExt
    {
        public static System.Data.Entity.Spatial.DbGeography ToEntityDbGeography(this DbGeography geo)
        {
            return System.Data.Entity.Spatial.DbGeography.FromText(geo.Geography.WellKnownText, (int) geo.Geography.CoordinateSystemId);
        }

        public static DbGeography ToAeroRdsDbGeography(this global::System.Data.Entity.Spatial.DbGeography geo)
        {
            return new DbGeography { Geography = new DbGeographyWellKnownValue() { WellKnownText = geo.WellKnownValue.WellKnownText, CoordinateSystemId = geo.WellKnownValue.CoordinateSystemId}};
        }
        public static SqlGeography ToSqlGeography(this global::System.Data.Entity.Spatial.DbGeography geo)
        {
            return SqlGeography.STGeomFromWKB(new SqlBytes(geo.AsBinary()), GeoDataService.SRID).MakeValid();
        }

        public static System.Data.Entity.Spatial.DbGeography MakeValid(this System.Data.Entity.Spatial.DbGeography geo)
        {
            return System.Data.Entity.Spatial.DbGeography.FromText(SqlGeography.Parse(geo.AsText()).MakeValid().ToString(),  GeoDataService.SRID);
        }

        public static GeoCoordinate GetReferancePoint(this  System.Data.Entity.Spatial.DbGeography geo)
        {
            var refPoint = new GeoCoordinate { Latitude = 0, Longitude = 0 };

            double latitudeAvarage = 0;
            double longitudeAvarage = 0;

            if (geo.PointCount != null)
            {
                int pointCount = (int)geo.PointCount;
                for (int i = 1; i <= pointCount; i++)
                {
                    var p = geo.PointAt(i);
                    latitudeAvarage += (double)p.Latitude;
                    longitudeAvarage += (double)p.Longitude;
                }
                refPoint.Latitude = latitudeAvarage / pointCount;
                refPoint.Longitude = longitudeAvarage / pointCount;
            }

            return refPoint;
        }
    }
}