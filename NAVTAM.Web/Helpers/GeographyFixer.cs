﻿using NavCanada.Core.Common.Geography;
using System;
using System.Data.Entity.Spatial;

namespace NAVTAM.Helpers
{
    public static class GeographyFixer
    {
        public static DbGeography Normalize(this DbGeography geo)
        {
            try
            {
                var geoText = geo.AsText();
                var fixedGeoText = GeoRegionFixer.Process(geoText);
                return fixedGeoText != geoText ? DbGeography.FromText(fixedGeoText, geo.CoordinateSystemId) : geo;
            }
            catch (Exception)
            {
                return geo;
            }
        }
    }
}