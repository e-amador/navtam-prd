﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using System;
using System.Data.Entity.Spatial;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace NAVTAM.Helpers
{
    public interface IGeoLocationCache
    {
        Task<DbGeography> GetUserDoa(IUow uow, string userId);
        void CleanGeoLocationCache(string userId);
    }

    public class HttpContextGeoLocationCache : IGeoLocationCache
    {
        public void CleanGeoLocationCache(string userId)
        {
            var cacheKey = GetUserDoaGeoKey(userId);
            var cacheObject = HttpContext.Current.Cache;
            var geoLocation = cacheObject.Get(cacheKey) as DbGeography;
            if (geoLocation != null)
            {
                cacheObject.Remove(cacheKey);
            }
        }

        public async Task<DbGeography> GetUserDoa(IUow uow, string userId)
        {
            var cacheObject = HttpContext.Current.Cache;
            var cacheKey = GetUserDoaGeoKey(userId);
            var geoLocation = cacheObject.Get(cacheKey) as DbGeography;
            if (geoLocation == null)
            {
                var doa = await uow.UserDoaRepo.JoinUserDoas(userId);
                if (doa != null)
                {
                    cacheObject.Add(cacheKey, doa, null, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(1), CacheItemPriority.Normal, null);
                    geoLocation = doa;
                }
            }
            return geoLocation;
        }

        static string GetUserDoaGeoKey(string userId) => $"Doa_Geo_{userId}";
    }
}