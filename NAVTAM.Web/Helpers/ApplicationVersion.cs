﻿using System;
using System.IO;

namespace NAVTAM.Helpers
{
    public class ApplicationVersion
    {
        public ApplicationVersion()
        {
            var assembly = typeof(ApplicationVersion).Assembly;
            Version = assembly.GetName().Version;
            BuildDate = File.GetLastWriteTime(assembly.Location);
        }

        public Version Version { get; private set; }
        public DateTime BuildDate { get; private set; }
    }
}