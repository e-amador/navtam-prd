﻿using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Mto;
using System;

namespace NAVTAM.Helpers
{
    public class MtoHelper
    {
        public static MessageTransferObject CreateMessage(int messageId, Action<MessageTransferObject> updateMessage = null)
        {
            var mto = new MessageTransferObject();

            // add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            // create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            // create inbound type
            mto.Add(MessageDefs.MessageIdKey, messageId, DataType.Int32);

            updateMessage?.Invoke(mto);

            // add unique identifier
            //mto.Add(MessageDefs.EntityIdentifierKey, subjectId, DataType.String);

            // date/time when the message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            return mto;
        }
    }
}