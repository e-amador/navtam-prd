﻿using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using System;
using System.Web.Mvc;

namespace NAVTAM.Helpers.CustomModelBinders
{
    public class TokenViewModelBinder : DefaultModelBinder
    {
        private INsdManagerResolver _nsdManagerResolver;

        public TokenViewModelBinder(INsdManagerResolver nsdManagerResolver) : base()
        {
            _nsdManagerResolver = nsdManagerResolver;
        }

        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            var type = bindingContext.ModelType;

            if (type == typeof(TokenViewModel))
            {
                var categoryId = bindingContext.ValueProvider.GetValue("categoryId").RawValue;
                var version = bindingContext.ValueProvider.GetValue("version").RawValue;

                if (categoryId is string[]) 
                {
                    categoryId = ((string[])categoryId)[0];
                    version = ((string[])version)[0];
                }

                var nsdManager = _nsdManagerResolver.GetNsdManager($"{categoryId}-{version}");

                var instanceType = Type.GetType(nsdManager.FullyQualifiedModelName);
                var instance = Activator.CreateInstance(instanceType);

                bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => instance, instanceType);

                return instance;
            }

            return null;
        }
    }
}