﻿using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace NAVTAM.Helpers.CustomModelBinders
{
    public class TokenViewModelApiBinder<T> : IModelBinder
    {
        private INsdManagerResolver _nsdManagerResolver;

        public TokenViewModelApiBinder(INsdManagerResolver nsdManagerResolver)
        {
            _nsdManagerResolver = nsdManagerResolver;
        }

        public bool BindModel(HttpActionContext actionContext,
            ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(T))
                return false;

            try
            {
                var json = ExtractRequestJson(actionContext);

                bindingContext.Model = DeserializeObjectFromJson(json);

                return true;
            }
            catch (JsonException exception)
            {
                bindingContext.ModelState.AddModelError("JsonDeserializationException", exception);

                return false;
            }
        }

        private T DeserializeObjectFromJson(string json)
        {
            var binder = new TypeNameSerializationBinder("");

            var proposal = JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                Binder = binder
            });

            if(proposal != null )
            {
                var vm = proposal as ProposalViewModel;

                var nsdManager = _nsdManagerResolver.GetNsdManager($"{vm.CategoryId}-{vm.Version}");

                var instanceType = Type.GetType(nsdManager.FullyQualifiedModelName);
                var instance = Activator.CreateInstance(instanceType);

                vm.Token = instance as TokenViewModel;
            }

            return proposal;
        }

        private static string ExtractRequestJson(HttpActionContext actionContext)
        {
            var content = actionContext.Request.Content;
            string json = content.ReadAsStringAsync().Result;
            return json;
        }
    }

    public class TypeNameSerializationBinder : SerializationBinder
    {
        public string TypeFormat { get; private set; }

        public TypeNameSerializationBinder(string typeFormat)
        {
            TypeFormat = typeFormat;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.Name;
        }

        public override Type BindToType(string assemblyName, string typeName)
        {
            string resolvedTypeName = string.Format(TypeFormat, typeName);

            return Type.GetType(resolvedTypeName, true);
        }
    }
}