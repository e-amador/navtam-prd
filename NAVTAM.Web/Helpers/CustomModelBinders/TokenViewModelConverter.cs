﻿using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace NAVTAM.Helpers.CustomModelBinders
{
    public class TokenViewModelConverter : JsonConverter
    {
        private INsdManagerResolver _nsdManagerResolver;

        public TokenViewModelConverter(INsdManagerResolver nsdManagerResolver) : base()
        {
            _nsdManagerResolver = nsdManagerResolver;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ProposalViewModel);
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream 
            JObject jObject = JObject.Load(reader);

            var categoryId = (string)jObject["categoryId"];
            var version = (string)jObject["version"];

            var nsdManager = _nsdManagerResolver.GetNsdManager($"{categoryId}-{version}");

            var instanceType = Type.GetType(nsdManager.FullyQualifiedModelName);

            var target = Activator.CreateInstance(typeof(ProposalViewModel));
            serializer.Populate(jObject.CreateReader(), target);

            var token = Activator.CreateInstance(instanceType);
            serializer.Populate(jObject["token"].CreateReader(), token);

            var vm = target as ProposalViewModel;
            vm.Token = token as TokenViewModel;
            return vm;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, JArray.Parse(value as string));
        }
    }
}