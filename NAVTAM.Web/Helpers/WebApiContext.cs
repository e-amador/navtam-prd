﻿using Microsoft.AspNet.Identity;
using NAVTAM.App_Start;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using NavCanada.Core.Common.Common;

namespace NAVTAM.Helpers
{
    public class WebApiContext : IApplicationContext
    {
        public WebApiContext(HttpContext httpContext, ApplicationUserManager userManager)
        {
            _httpContext = httpContext;
            _userManager = userManager;
        }

        public string GetCookie(string cookieName, string defaultValue = "")
        {
            var cookie = _httpContext.Request.Cookies[cookieName];
            return cookie != null ? cookie.Value : defaultValue;
        }

        public void SetCookie(string name, string value, int ttl = 30)
        {
            _httpContext.Response.Cookies.Add(new HttpCookie(name, value) { Expires = DateTime.Now.AddMinutes(ttl) });
        }

        public Task<bool> IsUserInRole(string userId, string role)
        {
            return _userManager.IsInRoleAsync(userId, role);
        }

        public string GetUserId()
        {
            return _httpContext.User.Identity.GetUserId();
        }

        public string GetUserName()
        {
            return _httpContext.User.Identity.GetUserName();
        }

        public string GetConfigValue(string name, string defaultValue)
        {
            return ConfigurationManager.AppSettings[name] ?? defaultValue;
        }

        public bool IsValidUserRequest(string userIdCookie = "user_id")
        {
            var userId = GetCookie(userIdCookie, string.Empty);
            return !string.IsNullOrEmpty(userId) && userId.Equals(GetUserId());
        }

        public void SetResponseStatusCode(int code)
        {
            _httpContext.Response.StatusCode = code;
        }

        readonly HttpContext _httpContext;
        readonly ApplicationUserManager _userManager;
    }
}