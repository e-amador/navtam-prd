﻿using Microsoft.Ajax.Utilities;
using NavCanada.Core.Data.NotamWiz.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text.RegularExpressions;

namespace NAVTAM.Helpers
{
    public static class GeoDataService
    {
        public static readonly int SRID = 4326;

        private static readonly List<DirectionAccordingToRunwayOrintation> directionAccordingToRunwayOrintations = new List<DirectionAccordingToRunwayOrintation>
        {
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 1,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 2,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 3,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 4,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 5,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 6,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 7,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SE",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 8,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 9,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 10,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 11,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 12,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 13,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 14,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 15,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 16,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 17,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 18,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 19,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 20,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 21,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 22,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 23,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 24,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 25,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 26,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 27,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 28,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 29,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 30,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 31,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 32,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 33,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 34,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 35,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 36,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            }
        };

        private static readonly List<MinMaxDirection> directions = new List<MinMaxDirection>
        {
            new MinMaxDirection
            {
                Name = "N",
                Min = 349.01, //350,
                Max = 360
            },
            new MinMaxDirection
            {
                Name = "N",
                Min = 0,
                Max = 11 //11.99
            },
            new MinMaxDirection
            {
                Name = "NNE",
                Min = 11.01, //12,
                Max = 34 //34.99
            },
            new MinMaxDirection
            {
                Name = "NE",
                Min = 34.01, //35,
                Max = 56 //56.99
            },
            new MinMaxDirection
            {
                Name = "ENE",
                Min = 56.01, //57,
                Max = 79 //79.99
            },
            new MinMaxDirection
            {
                Name = "E",
                Min = 79.01, //80,
                Max = 101 //101.99
            },
            new MinMaxDirection
            {
                Name = "ESE",
                Min = 101.01, //102,
                Max = 124 //124.99
            },
            new MinMaxDirection
            {
                Name = "SE",
                Min = 124.01, //125,
                Max = 146 //146.99
            },
            new MinMaxDirection
            {
                Name = "SSE",
                Min = 146.01, //147,
                Max = 169 //169.99
            },
            new MinMaxDirection
            {
                Name = "S",
                Min = 169.01, //170,
                Max = 191 //191.99
            },
            new MinMaxDirection
            {
                Name = "SSW",
                Min = 191.01, //192,
                Max = 214 //214.99
            },
            new MinMaxDirection
            {
                Name = "SW",
                Min = 214.01, //215,
                Max = 236 //236.99
            },
            new MinMaxDirection
            {
                Name = "WSW",
                Min = 236.01, //237,
                Max = 259 //259.99
            },
            new MinMaxDirection
            {
                Name = "W",
                Min = 259.01, //260,
                Max = 281 //281.99
            },
            new MinMaxDirection
            {
                Name = "WNW",
                Min = 281.01, //282,
                Max = 304 //304.99
            },
            new MinMaxDirection
            {
                Name = "NW",
                Min = 304.01, //305,
                Max = 326 //326.99
            },
            new MinMaxDirection
            {
                Name = "NNW",
                Min = 326.01, //327,
                Max = 349 //349.99
            }
        };

        public static string DecimalToDMS(double dec, bool isLatitude)
        {
            var minus = dec < 0;
            dec = Math.Abs(dec);
            var degree = Math.Floor(dec);
            var tmp = (dec - degree) * 60;
            var mintue = Math.Floor(tmp);
            var seconds = Math.Floor((tmp - mintue) * 60);

            if (isLatitude)
                return degree.ToString("00") + mintue.ToString("00") + seconds.ToString("00") + (minus ? 'S' : 'N');

            return degree.ToString("000") + mintue.ToString("00") + seconds.ToString("00") + (minus ? 'W' : 'E');
        }


        static char[] SpaceArray = new char[] { ' ' };

        private static string[] SplitOnSpaces(string str)
        {
            return (str ?? "").Split(SpaceArray, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string GetDirection(double d)
        {
            d = Math.Round((d + 360) % 360, 2);
            return directions.Where(a => a.Min <= d && a.Max >= d).First().Name;
        }

        public static bool IsInBilingualRegion(DbGeography geoLocation, IUow uow)
        {
            if (geoLocation == null)
                return false;

            var region = uow.DoaRepo.GetBilingualRegion()?.DoaGeoArea;
            if (region == null)
                return false;

            return geoLocation.Intersects(region);
        }

        public static bool IsValidDecimalLocation(string str)
        {
            if (str.IsNullOrWhiteSpace())
                return false;

            var parts = SplitOnSpaces(str);
            if (parts.Length != 2)
                return false;

            var latStr = parts[0];
            var lngStr = parts[1];

            var regex = new Regex(@"^-?\d*(\.\d+)?$");
            if (!regex.Match(latStr).Success || !regex.Match(lngStr).Success)
                return false;

            return double.Parse(latStr) < 90.0 && double.Parse(lngStr) < 180.0;
        }

        public static bool IsValidSubfixedDecimalLocation(string str)
        {
            if (str.IsNullOrWhiteSpace())
                return false;

            var parts = SplitOnSpaces(str);
            if (parts.Length != 2)
                return false;

            var latStr = parts[0];
            var lngStr = parts[1];

            var regex = new Regex(@"^[0-9]{0,3}([.][0-9]+)?[NSEW]$");
            if (!regex.Match(latStr).Success || !regex.Match(lngStr).Success)
                return false;

            return double.Parse(latStr.Substring(0, latStr.Length - 1)) < 90.0 && double.Parse(lngStr.Substring(0, lngStr.Length - 1)) < 180.0;
        }

        public static string DecimalToDMSLocation(string location)
        {
            var parts = SplitOnSpaces(location);

            var lat = double.Parse(parts[0]);
            var lng = double.Parse(parts[1]);

            return $"{DecimalToDMS(lat, true)} {DecimalToDMS(lng, false)}";
        }

        public static string SubfixedDecimalToDMSLocation(string location)
        {
            var parts = SplitOnSpaces(location);

            var latStr = parts[0];
            var lngStr = parts[1];

            var lat = double.Parse(latStr.Substring(0, latStr.Length - 1)) * GetSubfixSign(latStr);
            var lng = double.Parse(lngStr.Substring(0, lngStr.Length - 1)) * GetSubfixSign(lngStr);

            return $"{DecimalToDMS(lat, true)} {DecimalToDMS(lng, false)}";
        }

        private static double GetSubfixSign(string str)
        {
            return str.EndsWith("N", StringComparison.OrdinalIgnoreCase) || str.EndsWith("E", StringComparison.OrdinalIgnoreCase) ? 1.0 : -1.0;
        }

        public static bool IsValidDMS(string str, bool lat)
        {
            var pattern = lat ? "^([0-9]){6}[NS]$" : "^([0-9]){6,7}[WE]$";

            if (!Regex.Match(str, pattern).Success)
                return false;

            var odd = str.Length == 8; // meaning 7 digits
            var deg = str.Substring(0, odd ? 3 : 2);
            var min = str.Substring(odd ? 3 : 2, 2);
            var sec = str.Substring(odd ? 5 : 4, 2);
            var maxDeg = lat ? 90 : 180;

            return int.Parse(deg) < maxDeg && int.Parse(min) < 60 && int.Parse(sec) < 60;
        }

        public static bool IsValidDMSLocation(string str)
        {
            if (str.IsNullOrWhiteSpace())
                return false;

            var parts = SplitOnSpaces(str);
            if (parts.Length != 2)
                return false;

            return IsValidDMS(parts[0], true) && IsValidDMS(parts[1], false);
        }

        public static double MetersToNM(double met)
        {
            return met * 0.000539957;
        }

        public static double MeterToFeet(double m)
        {
            return m * 3.28084;
        }

        public static double FeetToMeter(double f)
        {
            return f / 3.28084;
        }

        public static double NMToMeters(double nm)
        {
            return nm / 0.000539957;
        }

        public static string RunwayDirectionToCardinal(int runwayDirection, bool right, bool french)
        {
            var info = directionAccordingToRunwayOrintations.Single(r => r.RwyNum == runwayDirection);
            return french ? (right ? info.RightRCLFrench : info.LeftRCLFrench) : (right ? info.RightRCL : info.LeftRCL);
        }

        internal class DirectionAccordingToRunwayOrintation
        {
            public string LeftRCL
            {
                get;
                set;
            }

            public string LeftRCLFrench
            {
                get;
                set;
            }

            public string RightRCL
            {
                get;
                set;
            }

            public string RightRCLFrench
            {
                get;
                set;
            }

            public int RwyNum
            {
                get;
                set;
            }
        }

        internal class MinMaxDirection
        {
            public double Max
            {
                get;
                set;
            }

            public double Min
            {
                get;
                set;
            }

            public string Name
            {
                get;
                set;
            }
        }
    }
}