﻿using Core.Common.Geography;
using Microsoft.SqlServer.Types;
using NavCanada.Core.Common.Geography;
using NavCanada.Core.Domain.Model.Dtos;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Device.Location;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace NAVTAM.Helpers
{
    public static class GeoCoordinateExt
    {
        public static double BearingTo(this GeoCoordinate from, GeoCoordinate to)
        {
            var d1 = ToRadians(from.Latitude);
            var d2 = ToRadians(to.Latitude);
            var dd = ToRadians(to.Longitude - from.Longitude);

            // see http://mathforum.org/library/drmath/view/55417.html
            var y = Math.Sin(dd) * Math.Cos(d2);
            var x = Math.Cos(d1) * Math.Sin(d2) -
                    Math.Sin(d1) * Math.Cos(d2) * Math.Cos(dd);
            var r = Math.Atan2(y, x);

            return (ToDegrees(r) + 360) % 360;
        }

        public static double ToRadians(double v)
        {
            return v * Math.PI / 180;
        }


        public static double ToDegrees(double v)
        {
            return v * 180 / Math.PI;
        }


        public static GeoCoordinate GetCentralGeoCoordinate(GeoCoordinate from, GeoCoordinate to)
        {
            var latitude = ToRadians(from.Latitude);
            var longitude = ToRadians(from.Longitude);

            var x = Math.Cos(latitude) * Math.Cos(longitude);
            var y = Math.Cos(latitude) * Math.Sin(longitude);
            var z = Math.Sin(latitude);

            latitude = ToRadians(to.Latitude);
            longitude = ToRadians(to.Longitude);

            x += Math.Cos(latitude) * Math.Cos(longitude);
            y += Math.Cos(latitude) * Math.Sin(longitude);
            z += Math.Sin(latitude);

            x /= 2.0;
            y /= 2.0;
            z /= 2.0;

            var centralLongitude = Math.Atan2(y, x);
            var centralSquareRoot = Math.Sqrt(x * x + y * y);
            var centralLatitude = Math.Atan2(z, centralSquareRoot);

            return new GeoCoordinate(ToDegrees(centralLatitude), ToDegrees(centralLongitude));
        }

        public static GeoCoordinate FindBestCenter(List<GeoCoordinate> coordinates)
        {
            return GetBestResult(coordinates, CalcSmallCircleEnvelopeCenter, BBoxCenter, FindDiagonalCenter); // FindDiagonalCenter, FindAverageCenter, CalcSmallCircleCenter
        }

        static GeoCoordinate GetBestResult(List<GeoCoordinate> coords, params Func<List<GeoCoordinate>, CenterCandidate>[] funcs)
        {
            var best = funcs.Select(f => f.Invoke(coords)).OrderBy(r => r.Radius).FirstOrDefault();

            Debug.WriteLine($"Algorithm: {best.Algorithm}"); 

            return best.Point;
        }

        struct CenterCandidate
        {
            public CenterCandidate(GeoCoordinate c, double r, string algorithm = "")
            {
                Point = c;
                Radius = r;
                Algorithm = algorithm;
            }

            public GeoCoordinate Point { get; set; }
            public double Radius { get; set; }
            public string Algorithm { get; set; }
        }

        static double GetRadius(GeoCoordinate center, List<GeoCoordinate> coordinates) => coordinates.Max(c => c.GetDistanceTo(center));

        static CenterCandidate CalcSmallCircleCenter(List<GeoCoordinate> geoPoints)
        {
            var points = geoPoints.Select(gp => new Point(gp.Latitude, gp.Longitude)).ToList();

            var circle = SmallestEnclosingCircle.MakeCircle(points);
            var center = new GeoCoordinate(circle.c.x, circle.c.y);

            return new CenterCandidate(center, GetRadius(center, geoPoints), "SmallCircle");
        }

        static CenterCandidate FindDiagonalCenter(List<GeoCoordinate> coordinates)
        {
            var pair = CreatePairs(coordinates).ToList().OrderByDescending(d => d.Radius).FirstOrDefault();
            return new CenterCandidate(pair.Center, GetRadius(pair.Center, coordinates), "Diagonal");
        }

        static CenterCandidate CalcSmallCircleEnvelopeCenter(List<GeoCoordinate> coordinates)
        {
            var pointsText = string.Join(", ", coordinates.Select(p => $"{p.Longitude} {p.Latitude}"));
            var lineString = $"LINESTRING({pointsText})";
            var geo = SqlGeography.STGeomFromText(new SqlChars(new SqlString(lineString)), 4326).MakeValid();

            var env = geo.EnvelopeCenter();
            var center = new GeoCoordinate(env.Lat.Value, env.Long.Value);

            center = MoveCenter(coordinates, center, GetRadius(center, coordinates));

            return new CenterCandidate(center, GetRadius(center, coordinates), "Envelope Center");
        }

        static bool CompareRadius(double r1, double r2, double epsilon) => r2 <= r1 && r2 >= r1 - epsilon;

        static GeoCoordinate MoveCenterToward(GeoCoordinate from, GeoCoordinate to, double delta, Predicate<GeoCoordinate> stopLambda)
        {
            var bearings = from.BearingTo(to);
            while (from.GetDistanceTo(to) > delta)
            {
                var last = DestinationPoint(from, bearings, delta);
                if (!stopLambda(last))
                    break;
                from = last;
            }
            return from;
        }

        static GeoCoordinate MoveCenter(List<GeoCoordinate> coordinates, GeoCoordinate center, double radius)
        {
            var delta = 50.0; // meters

            var candidates = coordinates.Select(c => new CenterCandidate(c, c.GetDistanceTo(center)))
                                        .OrderByDescending(c => c.Radius)
                                        .Take(2)
                                        .ToArray();

            var farthest = candidates[0].Point;

            // move center toward farthest point
            center = MoveCenterToward(center, farthest, delta, c => {
                var r = c.GetDistanceTo(farthest);
                // stop when a second point (not the current farthest) is at the border of the current circle
                return coordinates.Exists(p => p != farthest && CompareRadius(r, c.GetDistanceTo(p), delta));
            });

            candidates = coordinates.Select(c => new CenterCandidate(c, c.GetDistanceTo(center))).OrderByDescending(c => c.Radius).Take(2).ToArray();

            farthest = candidates[0].Point;

            var lineCenter = GetCentralGeoCoordinate(candidates[0].Point, candidates[1].Point);

            // move center toward the center of the line bethween the two farthest points
            center = MoveCenterToward(center, lineCenter, delta, c => {
                var r = c.GetDistanceTo(farthest);
                // stop when not all the points are in the circle
                return coordinates.All(p => r >= p.GetDistanceTo(c));
            });

            return center;
        }

        static CenterCandidate BBoxCenter(List<GeoCoordinate> coordinates)
        {
            var minLat = coordinates.Min(c => c.Latitude);
            var maxLat = coordinates.Max(c => c.Latitude);
            var minLng = coordinates.Min(c => c.Longitude);
            var maxLng = coordinates.Max(c => c.Longitude);
            var center = GetCentralGeoCoordinate(new GeoCoordinate(minLat, minLng), new GeoCoordinate(maxLat, maxLng));

            center = MoveCenter(coordinates, center, GetRadius(center, coordinates));

            return new CenterCandidate(center, GetRadius(center, coordinates), "BBox");
        }

        static CenterCandidate FindAverageCenter(List<GeoCoordinate> coordinates)
        {
            var x = 0.0;
            var y = 0.0;
            var z = 0.0;

            foreach (var c in coordinates)
            {
                var latitude = ToRadians(c.Latitude);
                var longitude = ToRadians(c.Longitude);

                x += Math.Cos(latitude) * Math.Cos(longitude);
                y += Math.Cos(latitude) * Math.Sin(longitude);
                z += Math.Sin(latitude);
            }

            var count = coordinates.Count;
            x /= count;
            y /= count;
            z /= count;

            var centralLongitude = Math.Atan2(y, x);
            var centralSquareRoot = Math.Sqrt(x * x + y * y);
            var centralLatitude = Math.Atan2(z, centralSquareRoot);

            var center = new GeoCoordinate(ToDegrees(centralLatitude), ToDegrees(centralLongitude));

            center = MoveCenter(coordinates, center, GetRadius(center, coordinates));

            return new CenterCandidate(center, GetRadius(center, coordinates), "AverageCenter");
        }

        static IEnumerable<Diagonal> CreatePairs(List<GeoCoordinate> cordinates)
        {
            var count = cordinates.Count;
            for (var i = 0; i < count - 1; i++)
                for (var j = i + 1; j < count; j++)
                {
                    var center = GetCentralGeoCoordinate(cordinates[i], cordinates[j]);
                    var radius = center.DistanceToEx(cordinates[i]);
                    if (radius > 0)
                    {
                        yield return new Diagonal()
                        {
                            Center = center,
                            Radius = radius
                        };
                    }
                }
        }

        struct Diagonal
        {
            public GeoCoordinate Center { get; set; }
            public double Radius { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="brng"></param>
        /// <param name="dist"> in meters</param>
        /// <returns></returns>
        public static GeoCoordinate DestinationPoint(this GeoCoordinate from, double brng, double dist)
        {
            var θ = ToRadians(brng);
            var δ = dist / 6371 / 1000; // angular distance in radians

            var φ1 = ToRadians(from.Latitude);
            var λ1 = ToRadians(from.Longitude);

            var φ2 = Math.Asin(Math.Sin(φ1) * Math.Cos(δ) +
                                Math.Cos(φ1) * Math.Sin(δ) * Math.Cos(θ));
            var λ2 = λ1 + Math.Atan2(Math.Sin(θ) * Math.Sin(δ) * Math.Cos(φ1),
                                     Math.Cos(δ) - Math.Sin(φ1) * Math.Sin(φ2));
            λ2 = (λ2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI; // normalise to -180..+180º

            return new GeoCoordinate(ToDegrees(φ2), ToDegrees(λ2));
        }

        public static double DistanceToEx(this GeoCoordinate from, GeoCoordinate to)
        {
            var R = 6371e3; // metres
            var φ1 = ToRadians(from.Latitude);
            var φ2 = ToRadians(to.Latitude);

            var Δφ = ToRadians(to.Latitude - from.Latitude);
            var Δλ = ToRadians(to.Longitude - from.Longitude);

            var a = Math.Sin(Δφ / 2) * Math.Sin(Δφ / 2) +
                    Math.Cos(φ1) * Math.Cos(φ2) *
                    Math.Sin(Δλ / 2) * Math.Sin(Δλ / 2);

            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            var d = R * c;

            return d;
        }

        /// <summary>
        /// Convert DMS or Decimal location to GeoCoordinate
        /// </summary>
        /// <param name="geo"></param>
        /// <param name="latlon"></param>
        /// <returns></returns>
        public static GeoCoordinate FromLocation(this GeoCoordinate geo, string location)
        {
            if (GeoDataService.IsValidDMSLocation(location))
                return geo.FromDMS(location);

            if (GeoDataService.IsValidSubfixedDecimalLocation(location))
                return geo.FromDMS(GeoDataService.SubfixedDecimalToDMSLocation(location));

            if (GeoDataService.IsValidDecimalLocation(location))
                return geo.FromDMS(GeoDataService.DecimalToDMSLocation(location));

            return null;
        }

        private static string GetDMS(double dec, bool isLat)
        {
            bool minus = dec < 0;
            dec = Math.Abs(dec);
            double degrees = Math.Floor(dec);
            double seconds = (dec - degrees) * 3600;
            double minutes = Math.Floor(seconds / 60);
            seconds = Math.Floor(seconds - (minutes * 60));
            return isLat ? ((int)degrees).ToString("D2") + ((int)minutes).ToString("D2") + ((int)seconds).ToString("D2") + (minus ? 'S' : 'N') : ((int)degrees).ToString("D3") + ((int)minutes).ToString("D2") + ((int)seconds).ToString("D2") + (minus ? 'W' : 'E');

        }

        public static GeoCoordinate FromDMS(this GeoCoordinate geo, string latlon)
        {
            string[] loc = (latlon ?? "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return loc.Length == 2 ? geo.FromDMS(loc[0], loc[1]) : null;
        }

        public static GeoCoordinate FromDMS(this GeoCoordinate geo, string lat, string lon)
        {
            DmsPointDto point = new DmsPointDto(lat, lon);
            geo.Latitude = point.Latitude;
            geo.Longitude = point.Longitude;
            return geo;
        }

        public static double DistanceToLine(this GeoCoordinate from, GeoLineDto line)
        {
            var a = line.X.GetDistanceTo(line.Y);
            var b = line.X.GetDistanceTo(from);
            var c = line.Y.GetDistanceTo(from);


            var s = (a + b + c) / 2;

            var area = Math.Sqrt(s * (s - a) * (s - b) * (s - c));


            var h = area / (a * 0.5);

            return h;
        }

        /// <summary>
        /// Calculate angle a b c
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static double Angle(this GeoCoordinate b, GeoCoordinate a, GeoCoordinate c)
        {
            var p12 = b.GetDistanceTo(a);
            var p13 = b.GetDistanceTo(c);
            var p23 = a.GetDistanceTo(c);

            return ToDegrees(Math.Acos((Math.Pow(p12, 2) + Math.Pow(p13, 2) - Math.Pow(p23, 2)) / (2 * p12 * p13)));
        }

        public static DMSLocation ConvertToDMSLocation(this GeoCoordinate value)
        {
            return DMSLocation.FromDecimal($"{value.Latitude.ToString(CultureInfo.InvariantCulture)} {value.Longitude.ToString(CultureInfo.InvariantCulture)}");
        }

        public static GeoCoordinate ConvertToGeoCoordinate(this DMSLocation value) => new GeoCoordinate(value.LatitudeDecimal, value.LongitudeDecimal);
    }
}