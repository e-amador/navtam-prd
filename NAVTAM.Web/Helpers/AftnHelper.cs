﻿using NDS.Relay;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NAVTAM.Helpers
{
    public static class AftnHelper
    {
        const string ValidAftnChars = " ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-?:().,'=/+\"\r\n";

        static AftnHelper()
        {
            _validAftnChars = new HashSet<char>(ValidAftnChars);
        }

        /// <summary>
        /// Validates the text contains only valid AFTN chars. 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="errorMessageTemplate">Must contain a placeholder for the error char. E.g. 'Invalid char {0}'</param>
        public static void ValidateAftnChars(string text, string errorMessageTemplate)
        {
            var invalidChar = (text ?? "").FirstOrDefault(c => !_validAftnChars.Contains(c));
            if (invalidChar != default(char))
                throw new Exception(string.Format(errorMessageTemplate, invalidChar));
        }

        public static bool TryValidateAftnChars(string text)
        {
            return (text ?? "").FirstOrDefault(c => !_validAftnChars.Contains(c)) == default(char);
        }

        public static bool ContainsForbiddenWord(string text, string forbiddenWords)
        {
            if (string.IsNullOrEmpty(text))
                return false;

            var forbidden = forbiddenWords.Split('|');
            return forbidden.Any(f => f.Length > 0 && text.IndexOf(f, StringComparison.OrdinalIgnoreCase) > -1);
        }

        public static string ReformatItemE(string itemE) => RelayUtils.ReformatAftnTextLines(itemE, ItemEWrapLength);

        public static void ValidateAftnAddresses(string addresses)
        {
            var addrArray = (addresses ?? "").Split(new char[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);

            if (addrArray.Length == 0)
                throw new Exception("No address found!");

            if (addrArray.Any(a => a.Length != 8))
                throw new Exception("Address with invalid length found!");

            if (!addrArray.All(ValidateAddressLetters))
                throw new Exception("Address with invalid characters found!");
        }

        static bool ValidateAddressLetters(string address) => address.All(l => l >= 'A' && l <= 'Z');

        static readonly HashSet<char> _validAftnChars;

        /// <summary>
        /// ItemE must be wrapped at 66 chars. This is to accomodate for the max 69 chars per line
        /// requirement for AFTN messages.
        /// </summary>
        const int ItemEWrapLength = RelayUtils.AftnLineLength - 3;
    }
}