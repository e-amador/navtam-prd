﻿using Microsoft.Ajax.Utilities;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NAVTAM.Helpers;
using System;
using System.Data.Entity.Spatial;
using System.Device.Location;
using System.Linq;
using System.Text.RegularExpressions;
using NavCanada.Core.Domain.Model.Dtos;
using System.Collections.Generic;
using Microsoft.SqlServer.Types;

namespace NAVTAM.NsdEngine.Helpers
{
    public static class GeoDataService
    {
        public static readonly int SRID = 4326;

        public static bool IsValidDMS(string str)
        {
            if (str.Length > 8 || str.Length < 7)
            {
                return false;
            }
            var regex = new Regex(@"((0[0-9]{2}|1[0-7][0-9])([0-5][0-9][0-5][0-9])([WE]))|([0-7][0-9][0-5][0-9][0-5][0-9][NS])");
            return regex.Match(str).Success;
        }

        public static string DecimalToDMS(double dec, bool isLatitude)
        {
            var minus = dec < 0;
            dec = Math.Abs(dec);
            var degree = Math.Floor(dec);
            var tmp = (dec - degree) * 60;
            var mintue = Math.Floor(tmp);
            var seconds = Math.Floor((tmp - mintue) * 60);
            if (isLatitude)
            {
                return degree.ToString("00") + mintue.ToString("00") + seconds.ToString("00") + (minus ? 'S' : 'N');
            }
            return degree.ToString("000") + mintue.ToString("00") + seconds.ToString("00") + (minus ? 'W' : 'E');
        }

        public static bool IsValidDMSLocation(string str)
        {
            if (str.IsNullOrWhiteSpace())
                return false;

            var parts = str.Split(' ');
            return parts.Length == 2 && parts[0].Length == 7 && parts[1].Length == 8 && IsValidDMS(parts[0]) && IsValidDMS(parts[1]);
        }

        public static bool IsValidDecimalLocation(string str)
        {
            if (str.IsNullOrWhiteSpace())
                return false;

            var parts = str.Split(' ');
            var regex = new Regex(@"^-?\d+\.\d*$");
            return regex.Match(parts[0]).Success && regex.Match(parts[1]).Success && 
                IsValidDMS(DecimalToDMS(double.Parse(parts[0]), true)) && IsValidDMS(DecimalToDMS(double.Parse(parts[1]), false));
        }

        public static bool IsValidLocation(string location)
        {
            return IsValidDMSLocation(location) || IsValidDecimalLocation(location);
        }

        public static bool IsLocationWithinDOA(GeoCoordinate loc, Doa doa)
        {
            var location = DbGeography.PointFromText("POINT(" + loc.Longitude + " " + loc.Latitude + ")", SRID);
            return location.Intersects(doa.DoaGeoArea);
        }
        
        public static bool IsInBilingualRegion(double lat, double lon, IUow db)
        {
            return IsInBilingualRegion(DbGeography.PointFromText("Point(" + lon + " " + lat + ")", SRID), db);
        }

        public static bool IsInBilingualRegion(DbGeography geoLocation, IUow db)
        {
            if (geoLocation == null)
            {
                return false;
            }
            var region = db.BilingualRegionRepo.GetAll().FirstOrDefault();
            if (region == null || region.TheRegion == null || region.TheRegion.IsEmpty)
            {
                return false;
            }
            return region.TheRegion.ToSqlGeography().STIntersects(geoLocation.ToSqlGeography()).IsTrue;
        }

        public static string DMSToRoundedDM(string coordinates)
        {
            if (!IsValidDMSLocation(coordinates))
            {
                throw new ArgumentOutOfRangeException("coordinates");
            }
            var latitudeLongitude = coordinates.Split(' ');
            var latitude = latitudeLongitude[0];
            var longitude = latitudeLongitude[1];
            if (!IsValidDMS(latitude) || !IsValidDMS(longitude))
            {
                throw new ArgumentOutOfRangeException("coordinates");
            }
            var latitudeDegrees = Convert.ToDecimal(latitude.Substring(0, 2));
            var latitudeMinutes = Convert.ToDecimal(latitude.Substring(2, 2));
            var latitudeSeconds = Convert.ToDecimal(latitude.Substring(4, 2));
            var latitudeDirection = latitude.Substring(6, 1);
            var longitudeDegrees = Convert.ToDecimal(longitude.Substring(0, 3));
            var longitudeMinutes = Convert.ToDecimal(longitude.Substring(3, 2));
            var longitudeSeconds = Convert.ToDecimal(longitude.Substring(5, 2));
            var longitudeDirection = longitude.Substring(7, 1);
            if (latitudeSeconds > 29)
            {
                latitudeMinutes = latitudeMinutes + 1;
            }
            if (latitudeMinutes > 59)
            {
                latitudeMinutes = latitudeMinutes - 60;
                latitudeDegrees = latitudeDegrees + 1;
            }
            if (latitudeDegrees > 89)
            {
                latitudeDegrees = latitudeDegrees - 90;
            }
            if (longitudeSeconds > 29)
            {
                longitudeMinutes = longitudeMinutes + 1;
            }
            if (longitudeMinutes > 59)
            {
                longitudeMinutes = longitudeMinutes - 60;
                longitudeDegrees = longitudeDegrees + 1;
            }
            if (longitudeDegrees > 179)
            {
                longitudeDegrees = longitudeDegrees - 180;
            }
            return string.Format("{0:00}{1:00}{2} {3:000}{4:00}{5}", latitudeDegrees, latitudeMinutes, latitudeDirection, longitudeDegrees, longitudeMinutes, longitudeDirection);
        }

        public static MapDataDto GetJsonGeoFromDBGeography(DbGeography dbGeo, string type, string name)
        {
            var data = new MapDataDto();
            var feature = new FeatureDto
            {
                Geometry = GeoJsonMultiPolygonFromGeography(dbGeo),
                Properties = new Dictionary<string, string>
                {
                    { "NAME", name }
                }
            };
            data.Name = type;
            data.Features.Add(feature);
            return data;
        }

        public static MultiPolygonDto GeoJsonMultiPolygonFromGeography(DbGeography geography)
        {
            return geography == null ? null : GeoJsonMultiPolygonFromGeography(SqlGeography.Parse(geography.AsText()));
        }

        /// <summary>
        /// Create a GeoJson MultiPolygon from an SQL Server geography type.
        /// States and Countries are MultiPolygons on the client since they can come in many pieces.
        /// In the database, the geography may be a MultiPolygon or Polygon.
        /// Due to reduction, the geography may be a GeometryCollection containing 
        /// any types. This method extracts all external polygons from a geography
        /// and constructs a GeoJsonMultiPolygon.
        /// 
        /// To keep things simple in the client, we treat a Polygon as a Multi-polygon
        /// containing a single Polygon.
        /// </summary>
        /// <remarks>
        /// Indices in a MultiPolygon are:
        /// 1. polygon 
        /// 2. ring (only first ring is used since only exterior is drawn)
        /// 3. point
        /// 4. lat, long
        /// </remarks>
        /// <param name="geography">geography</param>
        /// <returns>MultiPolygon</returns>
        public static MultiPolygonDto GeoJsonMultiPolygonFromGeography(SqlGeography geography)
        {
            geography = geography.MakeValid();
            var polycoords = new List<double[][][]>();
            if (geography.IsNull)
            {
                return new MultiPolygonDto
                {
                    Coordinates = polycoords.ToArray()
                };
            }
            var geoType = (string)geography.STGeometryType();
            if (geoType == "Polygon")
            {
                polycoords.Add(PolygonCoordinatesFromGeography(geography));
            }

            // For a MultiPolygon or GeometryCollection, the same is repeated for each contained polygon
            else if (geoType == "MultiPolygon" || geoType == "GeometryCollection")
            {
                for (var g = 1; g <= geography.STNumGeometries(); g++)
                {
                    var geo = geography.STGeometryN(g);
                    if ((string)geo.STGeometryType() == "Polygon")
                    {
                        polycoords.Add(PolygonCoordinatesFromGeography(geo));
                    }
                    else
                    {
                        //Debug.WriteLine("Ignoring " + (string)geo.STGeometryType());
                    }
                }
            }
            else if (geoType == "Point")
            {
                var r = new double[1][][];
                r[0] = new double[1][];
                r[0][0] = new double[2]
                {
                    geography.Lat.Value,
                    geography.Long.Value
                };
                polycoords.Add(r);
            }
            else
            {
                throw new ArgumentException(Resources.GeoJsonMultiPolygonError);
            }

            // return a MultiPolygon containing the coordinates of the polygons
            var mpoly = new MultiPolygonDto
            {
                Coordinates = polycoords.ToArray()
            };
            return mpoly;
        }

        public static double[] ConvertTextCordinatesToLatitudeLongitude(string coordinates)
        {
            var parts = (coordinates ?? "").Split(' ');

            if (parts.Length != 2)
                throw new ArgumentException($"Invalid coordinates: {coordinates}");

            if (parts[0].Length > 8 || parts[0].Length < 7)
                throw new ArgumentException($"Invalid coordinates: {coordinates}");

            if (parts[1].Length > 8 || parts[1].Length < 7)
                throw new ArgumentException($"Invalid coordinates: {coordinates}");

            var result = new double[2];

            result[0] = ParseTextCoordinateValue(parts[0]);
            result[1] = ParseTextCoordinateValue(parts[1]);

            return result;
        }

        public static double ParseTextCoordinateValue(string text)
        {
            try
            {
                var isShort = text.Length == 7;
                var degrees = int.Parse(isShort ? text.Substring(0, 2) : text.Substring(0, 3));
                var minutes = int.Parse(isShort ? text.Substring(2, 2) : text.Substring(3, 2));
                var seconds = int.Parse(isShort ? text.Substring(4, 2) : text.Substring(5, 2));

                var subfix = (isShort ? text.Substring(6) : text.Substring(7)).ToUpper();
                var sing = (subfix == "W" || subfix == "S") ? -1 : 1;

                return sing * (degrees + ((minutes * 60.0 + seconds) / 3600.0));
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"Invalid coordinate value: {text}", ex);
            }
        }

        private static double[][][] PolygonCoordinatesFromGeography(SqlGeography polygon)
        {
            //Debug.Assert((string)polygon.STGeometryType() == "Polygon");
            var ringsCount = (int)polygon.NumRings();
            var coordinates = new double[ringsCount][][];
            for (var r = 1; r <= ringsCount; r++)
            {
                // convert only the first (exterior) ring
                var ring = polygon.RingN(r);
                var points = new List<double[]>();
                for (var i = 1; i <= ring.STNumPoints(); i++)
                {
                    var point = new double[2];
                    var sp = ring.STPointN(i);
                    //we can safely round the lat/long to 5 decimal places as that's 1.11m at equator, reduces data transferred to client
                    point[0] = Math.Round((double)sp.Lat, 5);
                    point[1] = Math.Round((double)sp.Long, 5);
                    points.Add(point);
                }
                coordinates[r - 1] = points.ToArray();
            }
            return coordinates;
        }
    }
}