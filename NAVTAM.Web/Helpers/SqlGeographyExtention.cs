﻿using System.Data.SqlTypes;
using Microsoft.SqlServer.Types;

namespace NAVTAM.Helpers
{
    public static class SqlGeographyExtention
    {
        public static readonly int SRID = 4326;

        public static SqlGeography FixOrentation(this SqlGeography polygon)
        {
            var invertedSqlGeography = polygon.ReorientObject();
            return polygon.STArea() > invertedSqlGeography.STArea() ? invertedSqlGeography : polygon;
        }

        public static SqlGeography AddPolygon(this SqlGeography multi, SqlGeography newPolygon)
        {
            multi = multi.MakeValid();
            newPolygon = newPolygon.MakeValid();

            var geob = new SqlGeographyBuilder();
            geob.SetSrid(SRID);
            geob.BeginGeography(OpenGisGeographyType.MultiPolygon);
            geob.BeginGeography(OpenGisGeographyType.Polygon);
            if (multi.STGeometryType() == new SqlString("MultiPolygon"))
            {
                for (int e = 1; e <= multi.STNumGeometries(); e++)
                {
                    SqlGeography ele = multi.STGeometryN(e);
                    AddPolygon(ref geob, ele);
                }
            }
            else
            {
                AddPolygon(ref geob, multi);
            }

            AddPolygon(ref geob, newPolygon);
            geob.EndGeography();
            geob.EndGeography();

            return geob.ConstructedGeography.MakeValid();
        }

        public static void AddPolygon(ref SqlGeographyBuilder geob, SqlGeography ele)
        {
            geob.BeginFigure((double)ele.STPointN(1).Lat, (double)ele.STPointN(1).Long);
            for (int p = 1; p <= ele.STNumPoints(); p++)
            {

                geob.AddLine((double)ele.STPointN(p).Lat, (double)ele.STPointN(p).Long);
            }
            geob.EndFigure();
        }
    }
}