﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NAVTAM.Helpers
{
    public static class TableFormatter
    {
        const char OpenBracket = '[';
        const char CloseBracket = ']';
        const char Apostrophe = '`';

        public static string FormatSquareBracketTables(string text)
        {
            if (ContainsValidTables(text))
            {
                var parts = SplitTables(text).ToList();
                var formattedParts = parts.Select(p => FormatSquareBracketTable(p)).ToList();
                return string.Join("", formattedParts);
            }
            return text;
        }

        public static bool ContainsValidTables(string text)
        {
            if (string.IsNullOrEmpty(text))
                return false;

            var opens = 0;
            var brakets = 0;
            foreach (var c in text)
            {
                if (c == OpenBracket) opens++;
                if (c == CloseBracket) opens--;
                if (opens < 0 || opens > 1) return false;
                brakets += opens; 
            }

            return brakets > 0 && opens == 0;
        }

        static string FormatSquareBracketTable(string text)
        {
            if (text[0] == OpenBracket)
            {
                var length = text.Length;
                if (length > 0 && text[0] == OpenBracket && text[length - 1] == CloseBracket)
                {
                    var rows = SplitTable(text.Substring(1, length - 2));

                    FormatTable(rows);

                    return PrintTable(rows);
                }
            }
            return text;
        }

        static IEnumerable<string> SplitTables(string text)
        {
            var index = 0;
            var length = text.Length;
            while (index < length)
            {
                var nextIndex = 0;

                if (text[index] == OpenBracket)
                {
                    var closePos = text.IndexOf(CloseBracket, index);
                    nextIndex = closePos + 1;
                }
                else
                {
                    var openPos = text.IndexOf(OpenBracket, index);
                    nextIndex = openPos >= 0 ? openPos : text.Length;
                }

                yield return text.Substring(index, nextIndex - index);

                index = nextIndex;
            }
        }

        static string PrintTable(List<List<string>> rows)
        {
            var printedRows = rows.Select(r => string.Join(" ", r));
            var sb = new StringBuilder();
            foreach (var row in printedRows)
            {
                if (sb.Length != 0)
                    sb.Append("\r\n");
                sb.Append(row);
            }
            return sb.ToString();
        }

        static void FormatTable(List<List<string>> rows)
        {
            var maxCols = SquareTable(rows);
            for (var i = 0; i < maxCols; i++)
                PadColum(rows, i);
        }

        static int SquareTable(List<List<string>> rows)
        {
            var maxCols = rows.Max(r => r.Count);
            rows.ForEach(r => SetMaxColumns(r, maxCols));
            return maxCols;
        }

        static void PadColum(List<List<string>> rows, int index)
        {
            var maxWidth = rows.Max(r => r[index].Length);
            rows.ForEach(r => r[index] = PadCell(r[index], maxWidth));
        }

        static string PadCell(string text, int length)
        {
            var delta = Math.Max(0, length - text.Length);
            if (delta == 0)
                return text;

            if (delta == 1)
                return $"{text} ";

            var before = delta / 2;
            var after = delta - before;

            return $"{new string(' ', before)}{text}{new string(' ', after)}";
        }

        static void SetMaxColumns(List<string> cols, int count)
        {
            while (cols.Count < count)
                cols.Add(string.Empty);
        }

        static char[] Separators = new char[] { '\n', '\r' };

        static List<List<string>> SplitTable(string text)
        {
            var rows = text.Split(Separators, StringSplitOptions.RemoveEmptyEntries).Select(r => SplitRow(r).ToList());
            return rows.ToList();
        }

        static IEnumerable<string> SplitRow(string row)
        {
            var index = 0;
            while (SkipSpaces(row, ref index))
            {
                yield return ReadString(row, ref index);
            }
        }

        static bool SkipSpaces(string text, ref int index)
        {
            var length = (text ?? string.Empty).Length;
            for (var i = index; i < length && text[i] == ' '; i++)
                index++;
            return index < length;
        }

        static string ReadString(string text, ref int index)
        {
            var sep = ' ';
            if (text[index] == Apostrophe)
            {
                sep = Apostrophe;
                index++;
            }
            return ReadTillNext(text, sep, ref index);
        }

        static string ReadTillNext(string text, char sep, ref int index)
        {
            var last = index;
            var length = text.Length;

            while (index < length && text[index] != sep)
                index++;

            var count = index - last;

            if (index < length)
                index++;

            return text.Substring(last, count);
        }
    }
}