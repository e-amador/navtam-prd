﻿using NAVTAM.NsdEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.ViewModels
{
    public class UpdateAnnotationViewModel
    {
        public int ProposalId { get; set; } 
        public List<ProposalAnnotationViewModel> Annotations { get; set; }
    }
}