﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class UserQueryFilterNameViewModel
    {
        public int? Id { get; set; }

        [Required]
        public string FilterName { get; set; }
        [Required]
        public string Username { get; set; }
        public int SlotNumber { get; set; }
    }
}