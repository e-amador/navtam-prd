﻿namespace NAVTAM.ViewModels
{
    public enum RegionSource
    {
        None,
        Geography,
        FIR,
        Points,
        PointAndRadius
    }

    public class RegionViewModel
    {
        public RegionSource Source { get; set; }
        /// <summary>
        /// Geography parsed from the uploaded file
        /// </summary>
        public string Geography { get; set; }
        public string GeoFeatures { get; set; }
        /// <summary>
        /// Fir designator expected
        /// </summary>
        public string Fir { get; set; }
        /// <summary>
        /// List of points (polygon) expected
        /// </summary>
        public string Points { get; set; }
        /// <summary>
        /// A decimal location expected
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// A radius in nautical miles expected
        /// </summary>
        public int Radius { get; set; }
    }
}