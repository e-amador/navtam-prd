﻿using NavCanada.Core.Domain.Model.Enums;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class SignalrViewModel
    {
        [Required]
        public string Username { get; set;  }
        [Required]
        public string Password { get; set; }
        public int Id { get; set; }
        public NotamProposalStatusCode Status { get; set; }
    }
}