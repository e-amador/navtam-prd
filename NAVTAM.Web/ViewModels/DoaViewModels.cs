﻿using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class DoaViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public RegionType DoaType { get; set; }

        [Required]
        public RegionViewModel Region { get; set; }

        public List<DoaRegionChange> Changes { get; set; }
    }
    
    public enum DoaRegionChangeType : byte
    {
        Add = 0,
        Remove
    }

    public class DoaRegionChange
    {
        public string Location { get; set; }
        public int Radius { get; set; }
        public bool InDoa { get; set; }
        public DoaRegionChangeType ChangeType => InDoa ? DoaRegionChangeType.Remove : DoaRegionChangeType.Add;
    }

    public class DoaPointModel
    {
        public string Source { get; set; }
        public string Location { get; set; }
        public bool InDoa { get; set; }
    }

    public class LocationInRegionViewModel
    {
        [Required]
        public RegionViewModel Region { get; set; }
        [Required]
        public string Location { get; set; }
    }

    public class RegionMapRenderViewModel
    {
        [Required]
        public RegionViewModel Region { get; set; }
        public List<DoaRegionChange> Changes { get; set; }
    }
}