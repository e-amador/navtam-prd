﻿using NavCanada.Core.Domain.Model.Enums;

namespace NAVTAM.ViewModels
{
    public class NdsClientViewModel
    {
        public int? Id { get; set; }
        public NdsClientType ClientType { get; set; }
        public string Client { get; set; }
        public string Address { get; set; }
        public string Operator { get; set; }
        public bool French { get; set; }
        public bool AllowQueries { get; set; }
        public string[] Series { get; set; }
        public string[] ItemsA { get; set; }
        public bool Active { get; set; }
        public bool IsRegionSubscription { get; set; }
        public RegionViewModel Region { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
    }
}