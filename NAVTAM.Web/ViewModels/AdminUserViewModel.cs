﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class UserViewModel
    {
        [Required]
        [StringLength(16, MinimumLength = 3)]
        [RegularExpression(@"^([a-zA-Z0-9 _-]+)$", ErrorMessage = "Invalid Username")]
        public string Username { get; set; }

        [Required]
        public string RoleId { get; set; }

        [Required]
        [RegularExpression(@"^([a-zA-Z0-9 ._-]+)$", ErrorMessage = "Invalid Firstname")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^([a-zA-Z0-9 ._-]+)$", ErrorMessage = "Invalid Lastname")]
        public string LastName { get; set; }

        //[Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        //[Compare("Password")]
        public string ConfirmPassword { get; set; }

        public string Position { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
        public string Fax { get; set; }

        [Required]
        public int OrganizationId { get; set; }

        [Required]
        [EmailAddress()]
        public string Email { get; set; }
        public string Address { get; set; }

        public bool Disabled { get; set; }
        public bool Approved { get; set; }
        public bool Deleted { get; set; }

        public string DoaIds { get; set; } //
    }

    //public class UserCreateViewModel
    //{
    //    [Required]
    //    [StringLength(16, MinimumLength = 3)]
    //    public string Username { get; set; }

    //    [Required]
    //    public string RoleIds { get; set; }

    //    [Required]
    //    public string FirstName { get; set; }

    //    [Required]
    //    public string LastName { get; set; }

    //    [Required]
    //    [DataType(DataType.Password)]
    //    public string Password { get; set; }

    //    [DataType(DataType.Password)]
    //    [Compare("Password")]
    //    public string ConfirmPassword { get; set; }

    //    public string Position { get; set; }

    //    [Required]
    //    [DataType(DataType.PhoneNumber)]
    //    [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
    //    public string PhoneNumber { get; set; }

    //    [DataType(DataType.PhoneNumber)]
    //    [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
    //    public string Fax { get; set; }

    //    [Required]
    //    public int OrganizationId { get; set; }

    //    [Required]
    //    [EmailAddress()]
    //    public string Email { get; set; }
    //    public string Address { get; set; }

    //    public bool Disabled { get; set; }
    //    public bool Approved { get; set; }

    //    public string DoaIds { get; set; }
    //}

    //public class UserUpdateViewModel
    //{
    //    [Required]
    //    [StringLength(16, MinimumLength = 3)]
    //    public string Username { get; set; }

    //    [Required]
    //    public string RoleIds { get; set; }

    //    [Required]
    //    public string FirstName { get; set; }

    //    [Required]
    //    public string LastName { get; set; }

    //    public string Position { get; set; }

    //    [Required]
    //    [DataType(DataType.PhoneNumber)]
    //    [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
    //    public string PhoneNumber { get; set; }

    //    [DataType(DataType.PhoneNumber)]
    //    [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
    //    public string Fax { get; set; }

    //    [Required]
    //    public int OrganizationId { get; set; }

    //    [Required]
    //    [EmailAddress()]
    //    public string Email { get; set; }
    //    public string Address { get; set; }

    //    public bool Disabled { get; set; }
    //    public bool Approved { get; set; }

    //    public string DoaIds { get; set; }
    //}

    public class UserResetPasswordViewModel
    {
        [Required]
        [StringLength(16, MinimumLength = 3)]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }

    public class UsersDeleteViewModel
    {
        [Required]
        public string[] Usernames { get; set; }
    }

}