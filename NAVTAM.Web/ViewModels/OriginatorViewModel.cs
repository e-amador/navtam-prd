﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NAVTAM.ViewModels
{
    public class OriginatorViewModel
    {
        public int ProposalId { get; set; }
        [Required]
        public string Originator { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}