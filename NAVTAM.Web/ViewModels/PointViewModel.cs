﻿using System.Collections.Generic;

namespace NAVTAM.ViewModels
{
    public class PointViewModel
    {
        public string[] Points { get; set; }
        public string CalculatedPoint { get; set; }
        public double? CalculatedRadius { get; set; }
        public double? PointRadius { get; set; }
        public string Error { get; set; }
        public List<InternationalAerodromeDto> InternationalAerodromesDto { get; set; }
    }

    public class InternationalAerodromeDto
    {
        public string CodeId { get; set; }
        public double Distance { get; set; }
        public string ReferencePoint { get; set; }
        
        public bool IsPointRadiusQuery = false;
    }
}