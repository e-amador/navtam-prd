﻿using System;

namespace NAVTAM.ViewModels
{
    public class SyncProposalStatusViewModel
    {
        public int[] Ids { get; set;  }
        public DateTime Since { get; set;  }
    }
}