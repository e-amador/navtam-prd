﻿using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class IcaoSubjectViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string NameFrench { get; set; }

        [Required]
        public string EntityCode { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Scope { get; set; }

        public bool RequiresItemA { get; set; }

        public bool RequiresScope { get; set; }

        public bool RequiresSeries { get; set; }
    }
}