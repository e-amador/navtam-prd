﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "UserName is required.")]
        [Display(Name = "UserName: *")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "'Password' must contains only alphanumeric characters and be between 8 and 16 characters long.")]
        [Display(Name = "Password: *")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "'ConfirmPassword' must contains only alphanumeric characters and be between 8 and 16 characters long.")]
        [Display(Name = "Confirm password: *")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public long? OrganizationId { get; set; }

        [Required(ErrorMessage = "Email Address is required.")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "Email address: *")]
        public string Email { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "First Name: *")]
        public string FisrtName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [Display(Name = "Last Name: *")]
        public string LastName { get; set; }

        [Display(Name = "Address (if different from Organization):")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Title/Position is required.")]
        [Display(Name = "Title/Position: *")]
        public string Position { get; set; }

        [Display(Name = "Fax number:")]
        public string Fax { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]
        [Display(Name = "Phone number: *")]
        public string Telephone { get; set; }

        [Display(Name = "Why you need access to the application:")]
        public string Reason { get; set; }

        [Required(ErrorMessage = "Organization name is required.")]
        [Display(Name = "Organization Name: *")]
        public string OrgName { get; set; }

        [Required(ErrorMessage = "Organization address is required.")]
        [Display(Name = "Organization Address: *")]
        public string OrgAddress { get; set; }

        [Required(ErrorMessage = "Organization email is required.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Organization Email: *")]
        [EmailAddress]
        public string OrgEmailAddress { get; set; }

        [Required(ErrorMessage = "Organization phone number is required.")]
        [Display(Name = "Organization Phone number: *")]
        public string OrgTelephone { get; set; }

        [Required(ErrorMessage = "Type of Operation is required.")]
        [Display(Name = "Type of Operation: *")]
        public string TypeOfOperation { get; set; }

        [Display(Name = "Head office information (if different from above):")]
        public string HeadOffice { get; set; }
    }
}