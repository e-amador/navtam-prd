﻿insert into NsdCategory (Id, ParentCategoryId, Name) Values (1, null, 'ROOT') 
insert into NsdCategory (Id, ParentCategoryId, Name) Values (2, 1, 'CATCHALL') 
insert into NsdCategory (Id, ParentCategoryId, Name) Values (3, 1, 'OBSTACLES') 
insert into NsdCategory (Id, ParentCategoryId, Name) Values (4, 3, 'OBST') 
insert into NsdCategory (Id, ParentCategoryId, Name) Values (5, 3, 'OBST LGT U/S') 

------------------------------------

insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYYJ', 2, 'V10', 0, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYYY', 2, 'V10', 1, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYWO', 2, 'V10', 2, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYW', 2, 'V10', 3, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYW', 2, 'V20', 0, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYYY', 2, 'V20', 1, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYVR', 2, 'V20', 2, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYO', 2, 'V20', 3, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CWIV', 2, 'V10', 0, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYA', 2, 'V20', 1, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYYJ', 4, 'V10', 0, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYYY', 4, 'V10', 1, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYWO', 4, 'V10', 2, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYW', 4, 'V10', 3, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYW', 4, 'V20', 0, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYYY', 4, 'V20', 1, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CYVR', 4, 'V20', 2, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYO', 4, 'V20', 3, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CWIV', 5, 'V10', 0, SYSDATETIME(), SYSDATETIME()) 
insert into NotamProposal2 (Name, CategoryId, Version, Status, StartDate, EndDate) Values ('CZYA', 5, 'V20', 1, SYSDATETIME(), SYSDATETIME()) 




