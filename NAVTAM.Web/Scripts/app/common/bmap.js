﻿var app = app || {};
app.bmap = (function () {
    var map;

    var initialized = false;
    var initMap = function () {
        map = new Microsoft.Maps.Map(document.getElementById('map'), {
            credentials: 'AnuhluTsAiyp-biSnICtTpyGBNrsn5VzOQ6ju9dEbmBjFGE-PIPxwEOYfhS5Cpps', //Esteban Credentials
            center: new Microsoft.Maps.Location(45.4215, -75.6973),
            mapTypeId: Microsoft.Maps.MapTypeId.road,
        });
        initialized = true;
    }

    var isMapInnitialized = function() {
        return initialized;
    }

    var addDoa = function (geoLocation, featureName) {
        var layer = getLayerOnMap('DOA');
        if (!layer) layer = new Microsoft.Maps.Layer('DOA');
        var geojson = JSON.parse(geoLocation);
        AddFeaturesToMap(layer, featureName, geojson, 'rgba(0, 255, 255, 0.2)');
    }
    var addMarker = function (geojson, featureName) {
        var layer = getLayerOnMap('PIN');
        if (!layer) layer = new Microsoft.Maps.Layer('PIN');
        AddFeaturesToMap(layer, featureName, geojson, 'blue');
    }
    var addFeature = function (geoLocation, featureName) {
        var layer = getLayerOnMap('LOC');
        if (!layer) layer = new Microsoft.Maps.Layer('LOC');
        var geojson = JSON.parse(geoLocation);
        AddFeaturesToMap(layer, featureName, geojson, 'rgba(255, 0, 0, 0.4)');
    }
    var setNewMarkerLocation = function (geojson) {
        if (geojson.features.length !== 1) return;

        var pinLayer = getLayerOnMap('PIN');
        if (pinLayer) {
            var pushpins = pinLayer.getPrimitives();
            if (pushpins.length === 1) {
                var currentLocation = pushpins[0].getLocation();
                latitudeDifference = Math.abs(currentLocation.latitude - geojson.features[0].geometry.coordinates[1]);
                longitudeDifference = Math.abs(currentLocation.longitude - geojson.features[0].geometry.coordinates[0]);
                var updatedLayer = getLayerOnMap('PIN_UPDATED');
                if (!updatedLayer) {
                    updatedLayer = new Microsoft.Maps.Layer('PIN_UPDATED');
                } else {
                    updatedLayer.clear();
                }
                AddFeaturesToMap(updatedLayer, 'Location', geojson, 'red');
            }
        }
    }
    var clearFeaturesOnLayers = function (excludingLayerIds) {
        excludingLayerIds = excludingLayerIds || [];
        for (var i = 0; i < map.layers.length; i++) {
            var layer = map.layers[i];
            if (excludingLayerIds.length > 0) {
                for (var j = 0; j < excludingLayerIds.length; j++) {
                    if (layer.getId() !== excludingLayerIds[j]) {
                        layer.clear();
                    }
                }
            }
            else {
                layer.clear();
            }
        }
    }
    var textCordinatesToGeojson = function (textCoordinates) {

        function convert(str) {
            try {
                var degrees = parseInt(str.length === 7 ? str.substring(0, 2) : str.substring(0, 3));
                var minutes = parseInt(str.length === 7 ? str.substring(2, 4) : str.substring(3, 5));
                var seconds = parseInt(str.length === 7 ? str.substring(4, 6) : str.substring(5, 7));
                var sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);

                var sing = (sufx === 'W' || sufx === 'S') ? -1 : 1;
                return sing * (degrees + ((minutes * 60.0 + seconds) / 3600.0));
            }
            catch (e) {
                return -1;
            }
        }

        var parts = textCoordinates.split(' ');
        if (parts.length !== 2)
            return null;

        if (parts[0].length > 8 || parts[0].length < 7) {
            return null;
        }
        if (parts[1].length > 8 || parts[1].length < 7) {
            return null;
        }

        var lng = -1;
        var lat = convert(parts[0]);
        if (lat !== 1) {
            lng = convert(parts[1]);
        }

        if (lat === -1 || lng === -1) {
            return null;
        }

        return latitudeLongitudeToGeoJson(lat, lng);
    }
    var geojsonToTextCordinates = function (geojson) {

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        };

        function convert(dec, type) {
            try {
                var minus = dec < 0;
                dec = Math.abs(dec);
                var degrees = Math.floor(dec);
                var seconds = (dec - degrees) * 3600;
                var minutes = Math.floor(seconds / 60);
                seconds = Math.floor(seconds - (minutes * 60));

                if (type === 'lat') {
                    return pad(degrees, 2) + pad(minutes, 2) + pad(seconds, 2) + (minus ? 'S' : 'N');
                }

                return pad(degrees, 3) + pad(minutes, 2) + pad(seconds, 2) + (minus ? 'W' : 'E');
            }
            catch (e) {
                return ""
            }
        }

        var coordinates = geojson.features[0].geometry.coordinates;

        var lat = convert(coordinates[1], 'lat');
        var lng = convert(coordinates[0], 'lng');

        return lat + ' ' + lng;
    }
    var latitudeLongitudeToGeoJson = function (lat, lng) {
        return {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Point",
                        "bbox": [lat, lng, lat, lng],
                        "coordinates": [lng, lat]
                    }
                }]
        };
    }
    
    function AddFeaturesToMap(layer, featureName, geojson, fillColor) {
        function getviewBoundaries(geojson) {
            var scaleBounds = 0;
            var bboxs = [];
            for (var i = 0; i < geojson.features.length; i++) {
                switch (layer.getId()) {
                    case 'LOC':
                        if (geojson.features[i].geometry.type === 'GeometryCollection') {
                            scaleBounds = geojson.features[i].geometry.geometries.length > 2 ? 0 : 10;
                        }
                        break;
                    case 'PIN_UPDATED': scaleBounds = 20; break;
                }

                if (geojson.features[i].geometry.bbox) {
                    var lat, lng;
                    for (var j = 0; j < geojson.features[i].geometry.bbox.length; j++) {
                        if (j % 2 !== 0) {
                            lng = geojson.features[i].geometry.bbox[j];
                            bboxs.push(new Microsoft.Maps.Location(lat, lng));
                        } else {
                            lat = geojson.features[i].geometry.bbox[j];
                        }
                    }
                }
            }
            var viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(bboxs);
            if (scaleBounds > 0) {
                viewBoundaries.buffer(scaleBounds);
            }

            return viewBoundaries;
        }
        Microsoft.Maps.loadModule('Microsoft.Maps.GeoJson', function () {
            var options = {
                polygonOptions:
                    {
                        name: featureName,
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: fillColor
                    }
            }
            var featureCollection = Microsoft.Maps.GeoJson.read(geojson, options);
            for (var i = 0; i < featureCollection.length; i++) {

                if (layer.getId() === 'PIN' || layer.getId() === 'PIN_UPDATED') {
                    if (featureCollection[i].geometryType === 1) {
                        var center = map.getCenter();
                        center.latitude = featureCollection[i].geometry.y;
                        center.longitude = featureCollection[i].geometry.x;
                        var pushpin;
                        if (layer.getId() === 'PIN_UPDATED') {
                            pushpin = new Microsoft.Maps.Pushpin(center, {
                                icon: '/content/images/poi_custom.png',
                                title: featureName,
                                subTitle: '',//featureName,
                                anchor: new Microsoft.Maps.Point(12, 39)
                            });
                            layer.add(pushpin);
                        } else {
                            pushpin = new Microsoft.Maps.Pushpin(center, {
                                title: ' ',
                                subTitle: featureName,
                                anchor: new Microsoft.Maps.Point(12, 39)
                            });
                            layer.add(pushpin);
                        }
                    }
                }
                else layer.add(featureCollection[i]);
            }

            if (!getLayerOnMap(layer.getId())) {
                map.layers.insert(layer);
            }
            if (layer.getId() !== 'PIN_UPDATED') {
                map.setView({ bounds: getviewBoundaries(geojson) });
            }
        });
    }
    function getLayerOnMap(layerId) {
        if (map) {
            for (var i = 0; i < map.layers.length; i++) {
                if (map.layers[i].getId() === layerId) {
                    return map.layers[i];
                }
            }
        }
        return null;
    }

    return {
        initMap: initMap,
        isMapInnitialized: isMapInnitialized,

        addDoa: addDoa,
        addMarker: addMarker,
        addFeature: addFeature,

        clearFeaturesOnLayers: clearFeaturesOnLayers,
        setNewMarkerLocation: setNewMarkerLocation,

        textCordinatesToGeojson: textCordinatesToGeojson,
        geojsonToTextCordinates: geojsonToTextCordinates,
        latitudeLongitudeToGeoJson: latitudeLongitudeToGeoJson
    }
})();
