﻿var app = app || {};

app.datagrid = (function (config) {

    var itemsPerPage = [3, 6, 10, 15];
    var sortDir = [];
    var sliderSize = 3;
    var items = ko.observableArray([]);
    var selectedItem = ko.observable(null);
    var lastUrl = "";
    var loadingData = ko.observable(false);  

    var paging = (function () {
        var currentPage = ko.observable(1);
        var currentSlider = ko.observable(0);
        var sliderLowerBound = ko.observable(0);
        var sliderUpperBound = ko.observable(sliderSize);
        var sliderlastBound = ko.observable(0);
        var totalCount = ko.observable(0);
        var pageSize = ko.observable(itemsPerPage[0]);
        var totalPages = ko.observable(0);

        var pages = ko.computed(function () {
            var arr = [];
            var lowerBound = (currentSlider() * sliderSize);
            var upperBound = (currentSlider() * sliderSize) + sliderSize;
            if( upperBound > totalPages() ) {
                upperBound = totalPages();
            }
            sliderLowerBound(lowerBound);
            sliderUpperBound(upperBound);
            sliderlastBound(Math.ceil(totalPages() / sliderSize));

            for (var i = lowerBound ; i < upperBound ; i++) {
                arr.push(i + 1);
            }
            //for (var i = 0; i < totalPages() ; i++) {
            //    arr.push(i + 1);
            //}
            return arr;
        });
        var set = function (obj) {
            currentPage(obj.currentPage);
            totalCount(obj.totalCount);
            pageSize(obj.pageSize);
            totalPages(obj.totalPages);
        }

        return {
            currentPage: currentPage,
            currentSlider: currentSlider,
            sliderLowerBound: sliderLowerBound,
            sliderUpperBound: sliderUpperBound,
            sliderlastBound: sliderlastBound,
            totalCount: totalCount,
            pageSize: pageSize,
            totalPages: totalPages,
            pages: pages,
            set: set
        }
    })();

    paging.pageSize.subscribe(function (newValue) {
        paging.pageSize(newValue);
        if (lastUrl.length > 0) {
            getItems(lastUrl, 1);
        }
    });

    var queryOptions = {
        page: 1,
        pageSize: paging.pageSize(),
        sort: "",
    };

    var init = function (config) {
        itemsPerPage = config.itemsPerPage || [3, 6, 10, 15];
        paging.pageSize(itemsPerPage[0]);
        for (var i = 0; i < config.columnFilters.length; i++) {

            if (config.initialColumnSort && i === config.initialColumnSort.index) {
                sortDir.push({
                    dir: ko.observable(config.initialColumnSort.dir),
                    field: config.columnFilters[i]
                })
            }
            else {
                sortDir.push({
                    dir: ko.observable(0),
                    field: config.columnFilters[i]
                })
            }
        }
        queryOptions.sort = sortDir[0].field;
    }(config);

    var getItems = function (url, pageNumber) {

        lastUrl = url,
        queryOptions.page = pageNumber || 1;
        queryOptions.pageSize = paging.pageSize();
        loadingData(true);
        $.ajax({
            type: 'POST',
            url: url,
            data: JSON.stringify(queryOptions),
            contentType: 'application/json; charset=utf-8',
            cache: false,
            dataType: 'json'
        })
        .done(function (response, textStatus, jqXHR) {
            items([]);
            //$("#data-response").fadeOut(0).fadeIn(500);
            paging.set(response.paging);
            for (var i = 0; i < response.data.length; i++) {
                var item = response.data[i];
                item.isSelected = ko.observable(i === 0);
                items.push(item);
            }
            if (response.data.length > 0) {
                selectedItem(response.data[0])
            }
        })
         .fail(function (qXHR, textStatus, errorThrown) {
             console.log(textStatus);
         })
         .always(function (ataOrjqXHR, textStatus, jqXHRorErrorThrown) {
             loadingData(false);
         });
    }
    var prevPage = function () {
        if (paging.currentPage() === paging.sliderLowerBound() + 1 ) {
            prevSlider();
        }
        else if (paging.currentPage() > 1) {
            getItems(lastUrl, paging.currentPage() - 1);
        }
    }
    var nextPage = function () {
        if (paging.currentPage() === paging.sliderUpperBound()) {
            nextSlider();
        }
        else if (paging.currentPage() < paging.totalPages()) {
            getItems(lastUrl, paging.currentPage() + 1);
        }
    }
    var prevSlider = function () {
        if (paging.currentSlider() > 0) {
            getItems(lastUrl, paging.currentSlider() * sliderSize);
            paging.currentSlider(paging.currentSlider() - 1);
        }
    }
    var nextSlider = function () {
        if (paging.currentSlider() + sliderSize < paging.totalPages()) {
            paging.currentSlider(paging.currentSlider() + 1);
            getItems(lastUrl, (paging.currentSlider() * sliderSize) + 1);
        }
    }
    var toPage = function (page) {
        if (page >= 1 && page <= paging.totalPages() && page !== paging.currentPage()) {
            getItems(lastUrl, page);
        }
    }
    var sort = function (index) {
        var dir = sortDir[index].dir();

        for (var i = 0; i < sortDir.length; i++) {
            sortDir[i].dir(0);
        }

        if (dir > 0) {
            sortDir[index].dir(-1);
            queryOptions.sort = '_' + sortDir[index].field;
        }
        else if (dir <= 0) {
            sortDir[index].dir(1);
            queryOptions.sort = sortDir[index].field;
        }

        getItems(lastUrl, paging.currentPage());
    }

    var selectRow = function (index) {
        for (var i = 0; i < items().length; i++) {
            items()[i].isSelected(false);
        }
        items()[index].isSelected(true);
        selectedItem(items()[index]);
    }

    return {
        itemsPerPage: itemsPerPage,
        sortDir: sortDir,
        paging: paging,
        items: items,
        selectedItem: selectedItem,

        getItems: getItems,
        sort: sort,
        prevPage: prevPage,
        nextPage: nextPage,
        nextSlider: nextSlider,
        prevSlider: prevSlider,
        toPage: toPage,
        loadingData : loadingData,
        selectRow: selectRow
    }
});
