﻿using Microsoft.AspNet.SignalR;

namespace NAVTAM.SignalrHub
{
    public class EventHubContext : IEventHubContext
    {
        public IHubContext Context
        {
            get
            {
                return GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            }
        }
    }
}