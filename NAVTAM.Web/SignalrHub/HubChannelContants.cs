﻿namespace NAVTAM.SignalrHub
{
    public class HubChannelContants
    {
        public const string AdminChannel = "ADMIN_CHANNEL";
        public const string ProposalChannel = "PROPOSAL_STATE_CHANNEL";
        public const string NofChannel = "NOF_CHANNEL";
    }
}