﻿using System;
using System.Collections.Generic;

namespace NAVTAM.NsdEngine.Helpers
{
    public class NsdManagerResolver : INsdManagerResolver
    {
        private readonly NsdManagerFactories _nsdManagerFactories;

        public NsdManagerResolver(NsdManagerFactories nsdManagerFactories)
        {
            _nsdManagerFactories = nsdManagerFactories;
             NsdManagers = new Dictionary<string, INsdManager>();
        }

        protected Dictionary<string, INsdManager> NsdManagers
        {
            get;
            private set;
        }

        public virtual INsdManager GetNsdManager(string key)
        {
            // Look for T dictionary cache under typeof(T).
            INsdManager nsdManagerObj;
            NsdManagers.TryGetValue(key, out nsdManagerObj);
            if (nsdManagerObj != null)
            {
                return nsdManagerObj;
            }

            // Not found or null; make one, add to dictionary cache, and return it.
            return MakeNsdManager(key);
        }

        protected virtual INsdManager MakeNsdManager(string key)
        {
            var f = _nsdManagerFactories.GetAgentFactory(key);
            if (f == null)
                throw new NotImplementedException(Resources.NoFactoryRepositoriy);

            var nsdManager = f();
            NsdManagers[key] = nsdManager;
            return nsdManager;
        }
    }
}
