﻿
using FParsec;
using Microsoft.FSharp.Core;
using NavCanada.Core.Data.NotamWiz.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace NAVTAM.NsdEngine.Helpers
{
    public static class CommonValidators
    {
        public static bool IsValidFreeText(string notamFreeText, IUow uow)
        {
            if (string.IsNullOrEmpty(notamFreeText))
                return false;

            notamFreeText = Regex.Replace(notamFreeText.ToUpper(), @"\t|\n|\r", " ");

            var setting = uow.ConfigurationValueRepo.GetAll().Where(c => c.Category == "NOTAM").Select(c => new { c.Name, c.Value });
            string allowedSymbols = setting.Where(s => s.Name == "NOTAMAllowedSymbols").Select(s => s.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(allowedSymbols))
            {
                Regex regex = new Regex(allowedSymbols);
                Match match = regex.Match(notamFreeText);
                if (!match.Success)
                    return false;
            }

            string forbiddenWords = setting.Where(s => s.Name == "NOTAMForbiddenWords").Select(s => s.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(forbiddenWords))
            {
                var textWords = notamFreeText.Split(' ');
                var words = forbiddenWords.Split('|').Distinct().ToDictionary(w => w, w => true);
                if (textWords.Any(w => words.ContainsKey(w)))
                    return false;
            }

            return true;
        }
    }

}