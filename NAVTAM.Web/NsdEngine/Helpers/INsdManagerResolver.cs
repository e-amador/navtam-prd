﻿using NavCanada.Core.Data.NotamWiz.Contracts;

namespace NAVTAM.NsdEngine.Helpers
{
    public interface INsdManagerResolver
    {
        INsdManager GetNsdManager(string key);
    }
}
