﻿using NAVTAM.Helpers;
using System;

namespace NAVTAM.NsdEngine.Helpers
{
    public static class ObstacleHelper
    {
        //public static string BuildItemEDistanceNM(double distanceFromSubject)
        //{
        //    /*
        //    Distances of 0 < 50FT from an AD shall be expressed as AT AD
        //    Distances of 50Ft </= 0.1NM from an object shall be expressed in FT
        //    Distances of 0.1 < 2NM from an object shall be expressed in NM with 2 decimal places
        //    Distances of 2 < 10NM  from an object shall be expressed in NM with 1 decimal places
        //    Distances of 10NM and Greater from an object shall be expressed in NM with no decimal places
        //    GeoDataService.MetersToNM(distanceFromSubject) give the distance in NM without round.
        //    */

        //    var realDistance = GeoDataService.MetersToNM(distanceFromSubject);
        //    if (realDistance >= 0.1 && realDistance < 2)
        //        return Math.Round(realDistance, 2).ToString() + "NM";
        //    else if (realDistance >= 2 && realDistance < 10)
        //        return Math.Round(realDistance, 1).ToString() + "NM";
        //    else if (realDistance >= 10)
        //        return Math.Round(realDistance).ToString() + "NM";
        //    else //in feets
        //    {
        //        realDistance = GeoDataService.MeterToFeet(distanceFromSubject);
        //        if (realDistance < 50) return "AT";
        //        else return Math.Round(realDistance) + "FT";
        //    }
        //}


        /// <summary>
        /// Distances of 0 to 50FT from an AD shall be expressed as AT AD
        /// Distances of 0 to 6076 FT have to be expressed in FT, otherwise in NM
        /// Distances of gt 1 NM to 2 NM have to be expressed in NM with 2 decimal places
        /// Distances of gt 2 NM to 5 NM have to be expressed in NM with 1 decimal places
        /// Distances of gt 5 NM have to be expressed in NM with 0 decimal places
        /// </summary>
        /// <param name="distanceFromSubject"></param>
        /// <returns></returns>
        public static string BuildItemEDistanceNM(double distanceFromSubject)
        {
            // Note: 6076 FT = 1 NM
            var distance = GeoDataService.MetersToNM(distanceFromSubject);
            if (distance > 1)
                return FormatDistanceInNM(distance);

            distance = GeoDataService.MeterToFeet(distanceFromSubject);
            if (distance <=50) 
                return "AT";

            return FormatDistanceInFT(distance);
        }

        /// <summary>
        /// Format distance in Nautical Miles
        /// </summary>
        /// <param name="distance">Distance in NM.</param>
        /// <returns></returns>
        public static string FormatDistanceInNM(double distance)
        {
            if (distance <= 2)
                return $"{Math.Round(distance, 2, MidpointRounding.AwayFromZero).ToString()}NM";   

            if (distance <= 5)
                return $"{Math.Round(distance, 1, MidpointRounding.AwayFromZero).ToString()}NM";

            return $"{(int)Math.Round(distance, MidpointRounding.AwayFromZero)}NM";
        }

        /// <summary>
        /// Format distance in feet
        /// </summary>
        /// <param name="distance">Distance in feet.</param>
        /// <returns></returns>
        public static string FormatDistanceInFT(double distance)
        {
            return $"{Math.Round(distance)}FT";
        }
    }
}