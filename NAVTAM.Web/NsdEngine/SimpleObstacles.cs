﻿using System.Collections.Generic;

namespace NAVTAM.NsdEngine
{
    public class SimpleObstacles : ObstacleTypes
    {
        protected override Dictionary<string, ObstacleType> GetLanguageDictionary(string language) => _simpleObstacles[language];

        static readonly Dictionary<string, Dictionary<string, ObstacleType>> _simpleObstacles = new Dictionary<string, Dictionary<string, ObstacleType>>
        {
            {
                "en", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", BuildObstacleTypeEnglish("Crane", "CRANE") },
                    { "Obs02", BuildObstacleTypeEnglish("Tower", "TOWER") },
                    { "Obs03", BuildObstacleTypeEnglish("Wind Turbine", "WIND TURBINE") },
                    { "Obs04", BuildObstacleTypeEnglish("Antenna", "ANTENNA") },
                    { "Obs05", BuildObstacleTypeEnglish("NDB Tower", "NDB TOWER") },
                    { "Obs06", BuildObstacleTypeEnglish("Smoke Stack", "SMOKE STACK") },
                    { "Obs07", BuildObstacleTypeEnglish("Drill Rig", "DRILL RIG") },
                    { "Obs08", BuildObstacleTypeEnglish("Bridge", "BRIDGE") },
                    { "Obs09", BuildObstacleTypeEnglish("Building", "BUILDING") },
                    { "Obs10", BuildObstacleTypeEnglish("Rig On Land", "RIG") },
                    { "Obs11", BuildObstacleTypeEnglish("Rig On Water", "RIG") },
                    { "Obs12", BuildObstacleTypeEnglish("Tethered Balloon/Captive Balloon", "TETHERED BALLOON") },
                }
            },
            {
                "fr", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", BuildObstacleTypeFrench("Grue", "GRUE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs02", BuildObstacleTypeFrench("Tour", "TOUR", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs03", BuildObstacleTypeFrench("Éolienne", "EOLIENNE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs04", BuildObstacleTypeFrench("Antenne", "ANTENNE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs05", BuildObstacleTypeFrench("Tour NDB", "TOUR NDB", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs06", BuildObstacleTypeFrench("Cheminée", "CHEMINEE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs07", BuildObstacleTypeFrench("Équipement de forage", "EQUIPEMENT DE FORAGE", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs08", BuildObstacleTypeFrench("Pont", "PONT", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs09", BuildObstacleTypeFrench("Bâtiment", "BATIMENT", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs10", BuildObstacleTypeFrench("Derrick", "DERRICK", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs11", BuildObstacleTypeFrench("Plate-forme", "PLATE-FORME", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs12", BuildObstacleTypeFrench("Ballon Captif", "BALLON CAPTIF", "PEINT", "NON PEINT", "ENLEVE") },
                }
            }
        };
    }
}