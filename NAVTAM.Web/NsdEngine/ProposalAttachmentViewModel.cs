﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.NsdEngine
{
    public class ProposalAttachmentViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string FileName { get; set; }

        public bool ReadOnly { get; set; }
    }
}