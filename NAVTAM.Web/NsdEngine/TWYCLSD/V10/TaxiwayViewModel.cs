﻿namespace NAVTAM.NsdEngine
{
    public class TaxiwayViewModel
	{

        public string Id { get; set; }
        public string Mid { get; set; }
        public string ParentId { get; set; }
        public string SdoEntityName { get; set; }
        public string Designator { get; set; }
        public string Name { get; set; }

        public bool Selected { get; set; }
        public bool Full { get; set; }
        public string Start { get; set; }
        public string StartFr { get; set; }
        public string End { get; set; }
        public string EndFr { get; set; }

        public string FullName { get { return SdoEntityName + ' ' + Designator; } }
    }
}