﻿namespace NAVTAM.NsdEngine
{
    public class BilingualText
    {
        public BilingualText()
        {
        }

        public BilingualText(string english, string french, bool toUpper = false)
        {
            English = toUpper ? english?.ToUpper() : english;
            French = toUpper ? french?.ToUpper() : french;
        }

        public BilingualText Append(BilingualText other) => new BilingualText($"{English}{other.English}", $"{French}{other.French}");
        public BilingualText Append(string text) => Append(new BilingualText(text, text));
        public bool Empty => string.IsNullOrEmpty(English);

        public string English { get; set; }
        public string French { get; set; }
    }
}