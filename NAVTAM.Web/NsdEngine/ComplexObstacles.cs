﻿using System.Collections.Generic;

namespace NAVTAM.NsdEngine
{
    public class ComplexObstacles : ObstacleTypes
    {
        protected override Dictionary<string, ObstacleType> GetLanguageDictionary(string language) => _complexObstacles[language];
    
        static readonly Dictionary<string, Dictionary<string, ObstacleType>> _complexObstacles = new Dictionary<string, Dictionary<string, ObstacleType>>
        {
            {
                "en", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", BuildObstacleTypeEnglish("Wind farm", "WIND FARM") },
                    { "Obs02", BuildObstacleTypeEnglish("Wind turbine group", "WIND TURBINE GROUP") },
                    { "Obs03", BuildObstacleTypeEnglish("Cable crossing", "CABLE CROSSING") },
                    { "Obs04", BuildObstacleTypeEnglish("Cable span", "CABLE SPAN") },
                    { "Obs05", BuildObstacleTypeEnglish("Mobile crane", "MOBILE CRANE") },
                    { "Obs06", BuildObstacleTypeEnglish("Mobile cranes", "MOBILE CRANES") },
                    { "Obs07", BuildObstacleTypeEnglish("Multiple cranes", "MULTIPLE CRANES") },
                    { "Obs08", BuildObstacleTypeEnglish("Buildings", "BUILDINGS") },
                    { "Obs09", BuildObstacleTypeEnglish("Multiple towers", "MULTIPLE TOWERS") },
                    { "Obs10", BuildObstacleTypeEnglish("Multiple cable crossings", "MULTIPLE CABLE CROSSINGS") },
                    { "Other", BuildObstacleTypeEnglish("Other", "OTHER") }
                }
            },
            {
                "fr", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", BuildObstacleTypeFrench("Groupe d'eoliennes (Wind farm)", "GROUPE D'EOLIENNES", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs02", BuildObstacleTypeFrench("Groupe d'eoliennes (Wind turbine group)", "GROUPE D'EOLIENNES", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs03", BuildObstacleTypeFrench("Traverse de cables (Cable crossing)", "TRAVERSE DE CABLES", "PEINTE", "NON PEINTE", "ENLEVE") },
                    { "Obs04", BuildObstacleTypeFrench("Traverse de cables (Cable span)", "TRAVERSE DE CABLES", "PEINTE", "NON PEINTE", "ENLEVE") },
                    { "Obs05", BuildObstacleTypeFrench("Grue mobile", "GRUE MOBILE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs06", BuildObstacleTypeFrench("Grues mobiles", "GRUES MOBILES", "PEINTES", "NON PEINTES", "ENLEVEES") },
                    { "Obs07", BuildObstacleTypeFrench("Grues multiples", "GRUES MULTIPLES", "PEINTES", "NON PEINTES", "ENLEVEES") },
                    { "Obs08", BuildObstacleTypeFrench("Bâtiments", "BATIMENTS", "PEINTS", "NON PEINTS", "ENLEVES") },
                    { "Obs09", BuildObstacleTypeFrench("Tours multiples", "TOURS MULTIPLES", "PEINTES", "NON PEINTES", "ENLEVEES") },
                    { "Obs10", BuildObstacleTypeFrench("Traverses de cables multiples", "TRAVERSES DE CABLES MULTIPLES", "PEINTES", "NON PEINTES", "ENLEVES") },
                    { "Other", BuildObstacleTypeFrench("Autre", "AUTRE", "AVEC PEINTURE", "SANS PEINTURE", "ENLEVE") }
                }
            }
        };
    }
}