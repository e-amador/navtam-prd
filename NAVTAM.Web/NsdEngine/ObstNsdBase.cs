﻿using Core.Common.Geography;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.Helpers;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;

namespace NAVTAM.NsdEngine
{
    public abstract class ObstNsdBase : NsdManager
    {
        public ObstNsdBase(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {
        }

        protected static BilingualText GenerateItemEReferenceFeature(ProposalContext context)
        {
            var subject = context.NearestSubject.Subject;
            var designator = subject.Designator;

            var ahpAttrs = GetCachedAttributes<Aerodrome>(subject);

            var isWaterAerodrome = context.Uow.SdoCacheRepo.IsWaterAerodrome(subject.Id);
            var isHeliport = "HP".Equals(ahpAttrs?.Type);

            var surfaceE = GetSurfaceTextEnglish(isWaterAerodrome, isHeliport);
            var surfaceF = GetSurfaceTextFrench(isWaterAerodrome, isHeliport);

            var composedSubjectName = GetAerodromeComposedName(subject.Name, ahpAttrs?.ServedCity);

            var retValue = new BilingualText()
            {
                // Note: When there is a defined surface (water or heli) do not print AD.
                English = (surfaceE.Length > 0) ? $"{composedSubjectName} {surfaceE}" : $"{composedSubjectName} AD",
                French = (surfaceF.Length > 0) ? $"{composedSubjectName} {surfaceF}" : $"AD {composedSubjectName}"
            };

            return retValue;
        }

        protected static BilingualText BuildObstacleCenterAproxLine(BilingualText refFeature, double distanceFromSubject, string cardinalDirection)
        {
            var distanceNM = ObstacleHelper.BuildItemEDistanceNM(distanceFromSubject);
            return new BilingualText
            {
                English = $"(APRX {distanceNM} {cardinalDirection} {refFeature.English})",
                French = $"(APRX {distanceNM} {cardinalDirection} {refFeature.French})"
            };
        }

        protected static BilingualText BuildObstacleGroupBoundsAproxLine(BilingualText refFeature, double distanceFromSubject1, double distanceFromSubject2, string cardinalDirection)
        {
            var distanceNM1 = ObstacleHelper.BuildItemEDistanceNM(distanceFromSubject1);
            var distanceNM2 = ObstacleHelper.BuildItemEDistanceNM(distanceFromSubject2);
            return new BilingualText
            {
                English = $"(APRX {distanceNM1} TO {distanceNM2} {cardinalDirection} {refFeature.English})",
                French = $"(APRX {distanceNM1} A {distanceNM2} {cardinalDirection} {refFeature.French})",
            };
        }

        //const int BeamLineThreshold = 50;
        //const int RoundingDistance = 10;
        static double BeamLineThreshold = GeoDataService.FeetToMeter(50);
        double RoundingDistance = GeoDataService.FeetToMeter(10);

        protected static BilingualText BuildObstacleCenterAproxLineFromTHR(List<Runway> runways, GeoCoordinate obstacleLocation, GeoCoordinate subjectLocation)
        {
            var aprxLine = new BilingualText();

            var minDistance = double.MaxValue;
            RunwayDirection nearestRdn = null;
            GeoCoordinate nearestRdnGeo = null;
            GeoCoordinate otherRdnGeo = null;

            foreach (var runway in runways)
            {
                foreach (var runwayRdn in runway.RunwayDirections)
                {
                    var runwayDirectionGeo = new GeoCoordinate().FromDMS(runwayRdn.Latitude, runwayRdn.Longitude);
                    var distance = runwayDirectionGeo.GetDistanceTo(obstacleLocation);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        nearestRdnGeo = runwayDirectionGeo;
                        nearestRdn = runwayRdn;
                        var otherRdn = runway.RunwayDirections.First(rdn => rdn.RunwayDirectionId != runwayRdn.RunwayDirectionId);
                        otherRdnGeo = new GeoCoordinate().FromDMS(otherRdn.Latitude, otherRdn.Longitude);
                    }
                }
            }

            var runwayBearing = nearestRdnGeo.BearingTo(otherRdnGeo);

            //TODO: Esteban=> Use meters for all calculations (avoid conversions) 
            var rcl = new GeoLineDto(nearestRdnGeo, otherRdnGeo);
            //var distanceToRcl = GeoDataService.MeterToFeet(obstacleLocation.DistanceToLine(rcl));
            var distanceToRcl = obstacleLocation.DistanceToLine(rcl);

            var beam = new GeoLineDto(nearestRdnGeo.DestinationPoint(runwayBearing + 90, minDistance * 2), nearestRdnGeo.DestinationPoint(runwayBearing - 90, minDistance * 2));
            //var distanceToBeam = GeoDataService.MeterToFeet(obstacleLocation.DistanceToLine(beam));
            var distanceToBeam = obstacleLocation.DistanceToLine(beam);

            var isRunwayDirectionDisplaced = nearestRdn.RunwayDirectionDeclaredDistances.Any(rdd => "DPLM".Equals(rdd.Type));
            var displacedThreshold = isRunwayDirectionDisplaced ? "DTHR" : "THR";

            var rwyDirectionDesignator = nearestRdn.Designator;

            // find location of obstacle
            var ax = rcl.X.Latitude;
            var ay = rcl.X.Longitude;
            var bx = rcl.Y.Latitude;
            var by = rcl.Y.Longitude;
            var x = obstacleLocation.Latitude;
            var y = obstacleLocation.Longitude;
            var position = Math.Sign((double)((bx - ax) * (y - ay) - (by - ay) * (x - ax)));

            var nearestRdnBearing = int.Parse(new String(nearestRdn.Designator.TakeWhile(Char.IsDigit).ToArray()));

            var itemECardinalPoint = GeoDataService.RunwayDirectionToCardinal(nearestRdnBearing, position == 1, false);
            var itemECardinalPointFrench = GeoDataService.RunwayDirectionToCardinal(nearestRdnBearing, position == 1, true);
            var angleOfObstacle = nearestRdnGeo.Angle(obstacleLocation, otherRdnGeo);

            if (distanceToBeam <= BeamLineThreshold) // at beam
            {
                //var distanceZUom = ToDistanceUom(Math.Round(distanceToRcl / RoundingDistance) * RoundingDistance);
                var distanceZUom = ObstacleHelper.BuildItemEDistanceNM(distanceToRcl);

                aprxLine.English = string.Format("ABEAM {0} {1} AND {2} {3} RCL", displacedThreshold, rwyDirectionDesignator, distanceZUom, itemECardinalPoint);
                aprxLine.French = string.Format("PAR LE TRAVERS {0} {1} ET {2} {3} DE RCL", displacedThreshold, rwyDirectionDesignator, distanceZUom, itemECardinalPointFrench);
            }
            else if (angleOfObstacle > 90) // before runway
            {
                //var distanceXYUom = ToDistanceUom(Math.Round(distanceToBeam / RoundingDistance) * RoundingDistance);
                //var distanceZUom = ToDistanceUom(Math.Round(distanceToRcl / RoundingDistance) * RoundingDistance);
                var distanceXYUom = ObstacleHelper.BuildItemEDistanceNM(distanceToBeam);
                var distanceZUom = ObstacleHelper.BuildItemEDistanceNM(distanceToRcl);


                aprxLine.English = string.Format("APRX {0} BFR {1} {2} AND {3} {4} EXTENDED RCL", distanceXYUom, displacedThreshold, rwyDirectionDesignator, distanceZUom, itemECardinalPoint);
                aprxLine.French = string.Format("APRX {0} BFR {1} {2} ET {3} {4} DU PROLONGEMENT DE RCL", distanceXYUom, displacedThreshold, rwyDirectionDesignator, distanceZUom, itemECardinalPointFrench);
            }
            else // next to runway
            {
                //var distanceXYUom = distanceToBeam / RoundingDistance) * RoundingDistance);
                //var distanceZUom = ToDistanceUom(Math.Round(distanceToRcl / RoundingDistance) * RoundingDistance);

                var distanceXYUom = ObstacleHelper.BuildItemEDistanceNM(distanceToBeam);
                var distanceZUom = ObstacleHelper.BuildItemEDistanceNM(distanceToRcl);

                aprxLine.English = string.Format("APRX {0} BEYOND {1} {2} AND {3} {4} RCL", distanceXYUom, displacedThreshold, rwyDirectionDesignator, distanceZUom, itemECardinalPoint);
                aprxLine.French = string.Format("APRX {0} APRES {1} {2} ET {3} {4} DE RCL", distanceXYUom, displacedThreshold, rwyDirectionDesignator, distanceZUom, itemECardinalPointFrench);
            }

            return aprxLine;
        }

        protected static bool RunwaysContainTHRInfo(List<Runway> runways)
        {
            return runways.Any() && !runways.Any(r => r.RunwayDirections.Any(rdn => string.IsNullOrEmpty(rdn.Latitude)));
        }

        protected static double CalcDistanceFromLocationToSubject(DMSLocation location, SdoCache subject)
        {
            var geoCoord = location.ToGeoCoordinate();
            var subjectGeoCoord = new GeoCoordinate(subject.RefPoint.Latitude ?? 0.0, subject.RefPoint.Longitude ?? 0.0);
            return geoCoord.GetDistanceTo(subjectGeoCoord);
        }

        protected static string GetObstacleStatusesText(SwitchState painted, SwitchState lighted, SwitchState ballmarkers, Func<bool, string> GetPainted, Func<bool, string> GetLighted, Func<bool, string> GetBallmarks)
        {
            var status = new List<string>();
            if (painted != SwitchState.Unknown)
                status.Add(GetPainted(painted == SwitchState.On));

            if (lighted != SwitchState.Unknown)
                status.Add(GetLighted(lighted == SwitchState.On));

            if (ballmarkers != SwitchState.Unknown)
                status.Add(GetBallmarks(ballmarkers == SwitchState.On));

            var text = string.Join(", ", status);

            return text.Length > 0 ? $"{text}." : string.Empty;
        }

        protected static string BuildLineItemE(string prefix, string obstacleName, string fromWord, string toWord, string fromLatLong, string toLatLong,
            string description, string aproxLine, int height, int elevation, string status)
        {
            var sb = new StringBuilder();

            sb.Append($"{prefix}{obstacleName} {fromWord} {fromLatLong} {toWord} {toLatLong} ");

            if (!string.IsNullOrEmpty(description))
                sb.Append($"({description}) ");

            sb.Append($"{aproxLine}. {height}FT AGL {elevation}FT AMSL.");

            if (!string.IsNullOrEmpty(status))
                sb.Append(" ").Append(status);

            return sb.ToString();
        }

        protected static string BuildCircleItemE(string prefix, string obstacleName, string radius, string latLong, string aproxLine, int height, int elevation, string status)
        {
            return $"{prefix}{obstacleName} WITHIN {radius} RADIUS CENTRED ON {latLong} {aproxLine}. {height}FT AGL {elevation}FT AMSL. {status}".Trim();
        }

        protected static string BuildCircleItemEFrench(string prefix, string obstacleName, string radius, string latLong, string aproxLine, int height, int elevation, string status)
        {
            return $"{prefix}{obstacleName} DANS UN RAYON DE {radius} CENTRE {latLong} {aproxLine}. {height}FT AGL {elevation}FT AMSL. {status}".Trim();
        }

        protected static string FormatRadius(double radius, ObstacleRadiusUnit units)
        {
            return units == ObstacleRadiusUnit.NM ? ObstacleHelper.FormatDistanceInNM(radius) : ObstacleHelper.FormatDistanceInFT(radius);
        }
        protected static string ToDistanceUom(double distanceInFeet)
        {
            var distanceInMeter = GeoDataService.FeetToMeter(distanceInFeet);
            return ObstacleHelper.BuildItemEDistanceNM(GeoDataService.FeetToMeter(distanceInMeter));
        }
        //protected static string ToDistanceUom(double distanceInFeet)
        //{
        //    const int NM_TO_FT = 6076;
        //    if (distanceInFeet > NM_TO_FT)
        //    {
        //        var distance = distanceInFeet / NM_TO_FT;
        //        var placeFormat = distance < 2 ? "0.00" : (distance < 5 ? "0.0" : "0");
        //        return string.Format("{0}NM", distance.ToString(placeFormat));
        //    }

        //    return string.Format("{0}FT", distanceInFeet.ToString("0"));
        //}

    }
}