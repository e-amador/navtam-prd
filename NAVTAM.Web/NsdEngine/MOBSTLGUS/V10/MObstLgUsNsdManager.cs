﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NAVTAM.ViewModels;
using NAVTAM.NsdEngine.MOBST.V10;
using Core.Common.Geography;
using NAVTAM.Helpers;
using NavCanada.Core.Domain.Model.Enums;
using System.Device.Location;

namespace NAVTAM.NsdEngine.MOBSTLGUS.V10
{
    public class MObstLgUsNsdManager : ObstNsdBase
    {
        public MObstLgUsNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {
        }

        public override TokenViewModel DeserializeToken(string tokens)
        {
            return Deserialize<MObstLgUsViewModel>(tokens);
        }

        protected override TokenViewModel CreateTokenViewModel()
        {
            return new MObstLgUsViewModel();
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as MObstLgUsViewModel;
            if (token == null)
                return ValidationResult.Fail(Resources.InvalidTokenType);

            var mobstTypes = new ComplexObstacles();
            if (!mobstTypes.HasType(token.ObstacleType))
                return ValidationResult.Fail(Resources.InvalidObstacleType);

            if (context.NearestAD() == null)
                return ValidationResult.Fail(Resources.NoNearbyAerodromeFound);

            if (context.NearestAerodromeCache() == null)
                return ValidationResult.Fail(Resources.NoNearbyAerodromeFound);

            #region check forbidden words

            var forbiddenWords = context.GetContext<string>("ForbiddenItemEWords");

            if (ContainsForbiddenWord(token.OtherObstacleNameE, forbiddenWords))
                return ValidationResult.Fail(Resources.ObstacleNameEnglishWithForbiddenWords);

            if (ContainsForbiddenWord(token.OtherObstacleNameF, forbiddenWords))
                return ValidationResult.Fail(Resources.ObstacleNameFrenchWithForbiddenWords);

            if (ContainsForbiddenWord(token.ObstacleDescriptionE, forbiddenWords))
                return ValidationResult.Fail(Resources.ObstacleDescEnglishWithForbiddenWords);

            if (ContainsForbiddenWord(token.ObstacleDescriptionF, forbiddenWords))
                return ValidationResult.Fail(Resources.ObstacleDescFrenchWithForbiddenWords);

            if (!AftnHelper.TryValidateAftnChars(token.OtherObstacleNameE))
                return ValidationResult.Fail(Resources.OtherObstacleNameEForbiddenChars);

            if (!AftnHelper.TryValidateAftnChars(token.OtherObstacleNameF))
                return ValidationResult.Fail(Resources.OtherObstacleNameFForbiddenChars);

            if (!AftnHelper.TryValidateAftnChars(token.ObstacleDescriptionE))
                return ValidationResult.Fail(Resources.ObstacleDescriptionEForbiddenChars);

            if (!AftnHelper.TryValidateAftnChars(token.ObstacleDescriptionF))
                return ValidationResult.Fail(Resources.ObstacleDescriptionFForbiddenChars);

            #endregion

            return ValidationResult.Succeed();
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as MObstLgUsViewModel).Location;
        }

        protected override void PreProcessToken(TokenViewModel rawToken)
        {
            base.PreProcessToken(rawToken);

            var token = rawToken as MObstLgUsViewModel;
            if (token.AreaType == ObstacleAreaType.Line)
            {
                token.FromPoint = token.FromPoint?.ToUpper();
                token.ToPoint = token.ToPoint?.ToUpper();
                var fromDMS = DMSLocation.FromKnownFormat(token.FromPoint);
                var toDMS = DMSLocation.FromKnownFormat(token.ToPoint);
                if (fromDMS != null && toDMS != null)
                {
                    var fromGeo = fromDMS.ToGeoCoordinate();
                    var toGeo = toDMS.ToGeoCoordinate();
                    var midGeo = GeoCoordinateExt.GetCentralGeoCoordinate(fromGeo, toGeo);
                    var radius = fromGeo.GetDistanceTo(midGeo); // radius in meters
                    token.Location = $"{midGeo.Latitude} {midGeo.Longitude}";
                    token.Radius = GeoDataService.MetersToNM(radius);
                    token.RadiusUnit = ObstacleRadiusUnit.NM;
                }
                else
                {
                    token.Location = string.Empty;
                    token.Radius = 0;
                }
            }
            // make all caps the input text
            token.OtherObstacleNameE = token.OtherObstacleNameE?.ToUpper();
            token.OtherObstacleNameF = token.OtherObstacleNameF?.ToUpper();
            token.ObstacleDescriptionE = token.ObstacleDescriptionE?.ToUpper();
            token.ObstacleDescriptionF = token.ObstacleDescriptionF?.ToUpper();
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            var radius = GetRadiusInNM(token as MObstLgUsViewModel);

            // add nearest aerodromes which 5NM radius touches the area of influence of this NOTAM
            var affectedAerodromes = await GetNearestAerodromes(context.GeoLocation(), context.Uow, radius + 5);
            context.AddContext("affectedAerodromes", affectedAerodromes);

            affectedAerodromes.Sort(CompareAerodromesByDisseminationCategoryAndDistanceInArea);

            var nearestSubject = affectedAerodromes.FirstOrDefault() ?? await GetNearestAerodrome(context.GeoLocation(), context.Uow, radius + 5);
            context.AddContext("nearestAD", nearestSubject);

            var aerodrome = await SdoCacheBroker.GetAerodromeCache(context.Uow, nearestSubject.Subject, true, false);
            context.AddContext("nearestADCache", aerodrome);

            context.AddContext("IsFIR", IsFirNotam(token, context) ? "Y" : "N");
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context) => context.NearestAD();

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            return context.IsFirNotam() ? GetLocationFir(context.GeoLocation(), context.Uow) : context.NearestAD().Subject.Fir;
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            var nearestAD = context.NearestAD();

            // AD notam
            if (context.IsAdNotam())
                return CovertAerodromeDesignatorToItemA(nearestAD.Subject.Designator);

            // fir notam
            var nearbyFirs = GetIntersectingOrNearbyFirs(context.GeoLocation(), GetRadiusInNM(token as MObstLgUsViewModel), context.Uow);
            return string.Join(" ", nearbyFirs);
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            return !context.IsCancelation ? GenerateNonCancelItemE(context, token as MObstLgUsViewModel) : GenerateCancelItemE(context);
        }

        protected override int GetRadius(TokenViewModel token, ProposalContext context)
        {
            var radius = GetRadiusInNM(token as MObstLgUsViewModel);

            var affectedADCount = context.GetAffectedAerodromeCount();

            // the minimum radius depends on the number of affected aerodromes
            var minRadius = affectedADCount != 0 ? 5.0 : 2.0;

            return (int)Math.Ceiling(Math.Max(minRadius, radius));
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            var affectedAerodromes = context.AffectedAerodromes();

            if (affectedAerodromes.Count == 0)
                return "E";

            // there is only one AD
            if (affectedAerodromes.Count == 1)
                return "AE";

            var radiusNM = GetRadiusInNM(token as MObstLgUsViewModel);
            if (radiusNM <= 1.0)
                return "AE";

            return "E";
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            var affectedCount = context.GetAffectedAerodromeCount();

            // is AD Notam
            if (context.IsAdNotam())
                return await GetNotamSeriesForSubject(context.Uow, context.NearestAD().Subject, "OL", false);

            // none (FIR)
            if (affectedCount == 0)
                return await GetNotamSeriesForDisseminationCategory(context.Uow, context.GeoLocation, DisseminationCategory.National, "OB", SeriesAllocationSubject.FIR);

            // many (FIR)
            var dcs = context.ExtractDisseminationCategories();
            return await GetNotamSeriesForDisseminationCategory(context.Uow, context.GeoLocation, dcs[0], "OL", SeriesAllocationSubject.FIR);
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context) => context.GetAffectedAerodromeCount() == 0 ? "V" : "IV";
        protected override string GetCode23(TokenViewModel token, ProposalContext context) => "OL";
        protected override string GetCode45(TokenViewModel token, ProposalContext context) => !context.IsCancelation ? "AS" : "AK";
        protected override string GetPurpose(TokenViewModel token, ProposalContext context) => "M";
        protected override int GetUpperLimit(TokenViewModel token, ProposalContext context) => RoundUpperLimitToFlightLevel((token as MObstLgUsViewModel).Elevation);
        protected override string GetDisplayName() => Resources.MObstLgUsDisplayName;

        private BilingualText GenerateNonCancelItemE(ProposalContext context, MObstLgUsViewModel token)
        {
            var nearestAD = context.NearestAD();
            var centerLocation = context.Location;

            var isLine = token.AreaType == ObstacleAreaType.Line;
            var fromLocation = isLine ? DMSLocation.FromKnownFormat(token.FromPoint) : null;
            var toLocation = isLine ? DMSLocation.FromKnownFormat(token.ToPoint) : null;

            var aerodrome = context.NearestAerodromeCache();

            var obstacleLocation = centerLocation.ToGeoCoordinate();
            var subjectLocation = new GeoCoordinate(nearestAD.Subject.RefPoint.Latitude ?? 0, nearestAD.Subject.RefPoint.Longitude ?? 0);
            var distanceFromSubject = obstacleLocation.GetDistanceTo(subjectLocation);

            var bearings = subjectLocation.BearingTo(obstacleLocation) - NSDUtilities.GetAerodromeMagneticVariation(context.Uow, aerodrome, nearestAD.Subject.RefPoint);
            var itemECardinalPoint = GeoDataService.GetDirection(bearings);

            var affectedADCount = context.GetAffectedAerodromeCount();

            var amendPub = GetAmendPubMessagePrefix(token);
            var adNamePrefix = GetItemEAerodromeNamePrefix(context, nearestAD.Subject.Name);

            //var itemEPrefix = affectedADCount == 0 ? amendPub : adNamePrefix.Append(amendPub);
            var itemEPrefix = context.IsFirNotam() ? amendPub : adNamePrefix.Append(amendPub);

            //var refFeature = affectedADCount == 0 ? GenerateItemEReferenceFeature(context) : new BilingualText("AD", "AD");
            var refFeature = context.IsFirNotam() ? GenerateItemEReferenceFeature(context) : new BilingualText("AD", "AD");

            BilingualText itemEAprxLine = null;

            if (isLine)
            {
                var fromDistance = CalcDistanceFromLocationToSubject(fromLocation, nearestAD.Subject);
                var toDistance = CalcDistanceFromLocationToSubject(toLocation, nearestAD.Subject);
                itemEAprxLine = BuildObstacleGroupBoundsAproxLine(refFeature, fromDistance, toDistance, itemECardinalPoint);
            }
            else
            {
                itemEAprxLine = BuildObstacleCenterAproxLine(refFeature, distanceFromSubject, itemECardinalPoint);
            }

            var obstacleName = GetObstacleType(token);

            var obstacleTypes = new ComplexObstacles();
            var obstacleTypeE = obstacleTypes["en"][token.ObstacleType];
            var obstacleTypeF = obstacleTypes["fr"][token.ObstacleType];

            var obstStatusTextE = GetObstacleStatusesText(SwitchState.Unknown, SwitchState.Unknown, SwitchState.Unknown, obstacleTypeE.LightedText, obstacleTypeE.PaintedText, obstacleTypeE.Ballmarked);
            var obstStatusTextF = GetObstacleStatusesText(SwitchState.Unknown, SwitchState.Unknown, SwitchState.Unknown, obstacleTypeF.LightedText, obstacleTypeF.PaintedText, obstacleTypeF.Ballmarked);

            itemEPrefix = itemEPrefix.Append("OBST LGT U/S ");

            if (isLine)
            {
                return new BilingualText
                {
                    English = BuildLineItemE(itemEPrefix.English, obstacleName.English, "FM", "TO", fromLocation.ToDMS(), toLocation.ToDMS(), token.ObstacleDescriptionE, itemEAprxLine.English, token.Height, token.Elevation, obstStatusTextE),
                    French = context.IsBilingual ? BuildLineItemE(itemEPrefix.French, obstacleName.French, "DE", "A", fromLocation.ToDMS(), toLocation.ToDMS(), token.ObstacleDescriptionF, itemEAprxLine.French, token.Height, token.Elevation, obstStatusTextF) : string.Empty
                };
            }
            else
            {
                var radius = FormatRadius(token.Radius, token.RadiusUnit);
                var centerDMS = centerLocation.ToDMS();
                return new BilingualText
                {
                    English = BuildCircleItemE(itemEPrefix.English, obstacleName.English, radius, centerDMS, itemEAprxLine.English, token.Height, token.Elevation, obstStatusTextE),
                    French = context.IsBilingual ? BuildCircleItemEFrench(itemEPrefix.French, obstacleName.French, radius, centerDMS, itemEAprxLine.French, token.Height, token.Elevation, obstStatusTextF) : string.Empty
                };
            }
        }

        private BilingualText GenerateCancelItemE(ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            var adNamePrefix = !context.IsFirNotam() ? GetItemEAerodromeNamePrefix(context, nearestAD.Subject.Name) : new BilingualText();

            var itemEEnglish = $"{adNamePrefix.English}OBST LGT {context.Location.ToDMS()} SVCBL";
            var itemEFrench = $"{adNamePrefix.French}OBST LGT {context.Location.ToDMS()} SVCBL";

            return new BilingualText(itemEEnglish, context.IsBilingual ? itemEFrench : string.Empty);
        }

        static bool IsFirNotam(TokenViewModel token, ProposalContext context)
        {
            var affectedADCount = context.GetAffectedAerodromeCount();

            // if not affected aerodrome, then it is an FIR NOTAM
            if (affectedADCount == 0)
                return true;

            // if there is only one then it is an AD NOTAM
            if (affectedADCount == 1)
                return false;

            // at this point there are many affected ADs, if the radius is more than a NM then it is an FIR NOTAM
            return GetRadiusInNM(token as MObstLgUsViewModel) > 1.0;
        }

        static BilingualText GetObstacleType(MObstLgUsViewModel token)
        {
            var obstType = new BilingualText();
            if (token.ObstacleType == "Other")
            {
                obstType.English = token.OtherObstacleNameE;
                obstType.French = token.OtherObstacleNameF;
            }
            else
            {
                var obstacleTypes = new ComplexObstacles();
                obstType.English = obstacleTypes["en"][token.ObstacleType].EFieldName;
                obstType.French = obstacleTypes["fr"][token.ObstacleType].EFieldName;
            }
            return obstType;
        }

        static int CompareAerodromesByDisseminationCategoryAndDistanceInArea(NearSdoSubject ahp1, NearSdoSubject ahp2)
        {
            // same dissemination category by take closest aerodrome
            if (ahp1.DisseminationCategory == ahp2.DisseminationCategory)
                return ahp1.Distance.CompareTo(ahp2.Distance);

            // take the highest dissemination category otherwise
            var catCompare = ahp2.DisseminationCategory.CompareTo(ahp1.DisseminationCategory);
            if (catCompare != 0)
                return catCompare;

            // compare by designator desc
            return ahp1.Subject.Designator.CompareTo(ahp2.Subject.Designator);
        }

        static double GetRadiusInNM(MObstLgUsViewModel token)
        {
            return token.RadiusUnit == ObstacleRadiusUnit.NM ? token.Radius : 0.000164579 * token.Radius;
        }

        protected override string GetNsdHelpLink()
        {
            return Resources.MultiObstLgtHelpLink;
        }
    }
}