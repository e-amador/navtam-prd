﻿using System;
using System.Text;

namespace NAVTAM.NsdEngine.RWYCLSD.V10
{
    public class RwyClsdItemEGenerator
    {
        public static BilingualText BuildItemE(RwyClsdViewModel token, RwyClsdReasons closureReasons)
        {
            return new BilingualText(ConstructItemE(token, closureReasons), ConstructItemE(token, closureReasons, true), toUpper: true);
        }

        private static string ConstructItemE(RwyClsdViewModel token, RwyClsdReasons closureReasons, bool french = false)
        {
            SetTokenClosureType(token, closureReasons, french);

            StringBuilder sb = new StringBuilder();
            sb.Append($"{EffectedRunway(token.EffectedRunwayName)}")
               .Append($"{ClosureReason(token.ClosureReason, token.ClosureReasonOtherFreeText, token.ClosureReasonOtherFreeTextFr, french)}")
               .Append($"{TaxiwayAvailability(token.AvailableAsTaxiway, french)}")
               .Append((token.TxyRestrictions) ? $"{TaxiwayRestrictions(token.FreeTextTaxiwayRestriction, token.FreeTextTaxiwayRestrictionFr, french)}" : "")
               .Append($"{WingRestrictions(token.MaxWingSpanFt, french)}")
               .Append(GenerateAnd(token.MaxWeightLbs, token.MaxWingSpanFt, french))
               .Append($"{WeightRestrictions(token.MaxWeightLbs, french)}")
               .Append(".");

            return sb.ToString();
        }

        private static void SetTokenClosureType(RwyClsdViewModel token, RwyClsdReasons closureReasons, bool french)
        {
            if (!token.ClosureReasonId.Equals("-1"))
            {
                switch (french)
                {
                    case false:
                        token.ClosureReason = closureReasons["en"][token.ClosureReasonId].EFieldName;
                        break;
                    case true:
                        token.ClosureReason = closureReasons["fr"][token.ClosureReasonId].EFieldName;
                        break;
                }
            }
        }

        private static string GenerateAnd(string maxWeightLbs, string maxWingSpanFt, bool french)
        {
            if (string.IsNullOrWhiteSpace(maxWeightLbs) || string.IsNullOrWhiteSpace(maxWingSpanFt)) return string.Empty;

            return (french) ? " ET" : " AND";
        }

        private static string WingRestrictions(string wing, bool french = false)
        {
            if (string.IsNullOrWhiteSpace(wing)) return string.Empty;

            return GenerateWingRestrictions(wing, french);
        }

        private static string WeightRestrictions(string weight, bool french = false)
        {
            if (string.IsNullOrWhiteSpace(weight)) return string.Empty;

            return GenerateWeightRestrictions(weight, french);
        }     

        private static string GenerateWeightRestrictions(string weightRestrictions, bool french)
        {
            return (french) ? $" POUR ACFT AVEC POIDS DE MOINS DE {weightRestrictions} LIVRES" : $" FOR ACFT WITH WEIGHT LESS THAN {weightRestrictions} POUNDS";
        }

        private static string GenerateWingRestrictions(string wingRestrictions, bool french)
        {
            return (french) ? $" POUR ACFT D'UNE ENVERGURE DE MOINS DE {wingRestrictions} FT" : $" FOR ACFT WITH WINGSPAN LESS THAN {wingRestrictions} FT";
        }

        private static string TaxiwayRestrictions(string freeTextEnglish, string freeTextFrench, bool french)
        {
            return (french) ? $" {freeTextFrench}" : $" {freeTextEnglish}";
        }

        private static string TaxiwayAvailability(bool availableAsTaxiway, bool french = false)
        {
            if (availableAsTaxiway) return (french) ? $" AVBL COMME TWY" : " AVBL AS TWY";

            return string.Empty;
        }

        private static string ClosureReason(string closureReason, string freeTextClosureReason, string ClosureReasonOtherFreeTextFr, bool french = false)
        {
            if (string.IsNullOrWhiteSpace(closureReason)) return string.Empty;

            switch (french)
            {
                case true:
                    return (!string.IsNullOrWhiteSpace(ClosureReasonOtherFreeTextFr)) ? $" CAUSE {ClosureReasonOtherFreeTextFr}" : $" CAUSE {closureReason}";
                case false:
                    return (!string.IsNullOrWhiteSpace(freeTextClosureReason)) ? $" DUE {freeTextClosureReason}" : $" DUE {closureReason}";
                default:
                    return string.Empty;
            }            
        }
        private static string EffectedRunway(string effectedRunwayName)
        {
            return string.IsNullOrWhiteSpace(effectedRunwayName) ? string.Empty : $"{effectedRunwayName.Replace('-', ' ')} CLSD";
        }
    }
}