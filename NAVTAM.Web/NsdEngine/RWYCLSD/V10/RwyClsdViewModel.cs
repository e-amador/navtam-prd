﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.NsdEngine.RWYCLSD.V10
{
    public class RwyClsdViewModel : TokenViewModel
    {
        public string EffectedAerodrome { get; set; }
        public string EffectedAerodromeName { get; set; }
        public string SubjectLocation { get; set; } //ALWAYS SET TO AERODROME
        public string EffectedRunway { get; set; }
        public string EffectedRunwayName { get; set; }
        public bool ClosureQuestion { get; set; }
        public string ClosureReason { get; set; }
        public string ClosureReasonId { get; set; }
        public string ClosureReasonOtherFreeText { get; set; }
        public string ClosureReasonOtherFreeTextFr { get; set; }
        public bool AvailableAsTaxiway{ get; set; }
        public bool AcftRestrictions { get; set; }
        public bool TxyRestrictions { get; set; }
        public string MaxWingSpanFt { get; set; }
        public string MaxWeightLbs { get; set; }
        public string FreeTextTaxiwayRestriction { get; set; }
        public string FreeTextTaxiwayRestrictionFr { get; set; }               
    }
}

