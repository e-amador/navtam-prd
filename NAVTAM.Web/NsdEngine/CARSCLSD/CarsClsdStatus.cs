﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.NsdEngine.CARSCLSD
{
    public static class CarsClsdStatus
    {
        public static List<CarsStatus> GetCarsStatus (string lang)
        {
            var carsStatus = new List<CarsStatus>();

            if(lang == "en")
            {
                carsStatus.Add(new CarsStatus(0, "CLOSED"));
                carsStatus.Add(new CarsStatus(1, "HOURS OF OPERATION"));
                carsStatus.Add(new CarsStatus(2, "HOURS OF OPERATION EXTENDED"));
            }
            else
            {
                carsStatus.Add(new CarsStatus(0, "FERMÉE"));
                carsStatus.Add(new CarsStatus(1, "HEURES D'EXPLOITATION"));
                carsStatus.Add(new CarsStatus(2, "HEURES D'EXPLOITATION PROLONGÉES"));
            }
            //var carsStatus = new List<CarsStatus>()
            //{
            //    new CarsStatus(0,"Closed","Ferme"),
            //    new CarsStatus(1,"Hrs of OPS","Hrs of OPS"),
            //    new CarsStatus(2,"Hrs of OPS EXT","Hrs of OPS EXT"),
            //};
            return carsStatus;
        }
    }

    public class CarsStatus
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public CarsStatus(int id, string text)
        {
            Id = id;
            Text = text;
        }
    }
}