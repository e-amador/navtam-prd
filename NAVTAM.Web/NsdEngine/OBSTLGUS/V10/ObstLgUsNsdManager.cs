﻿using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.Helpers;
using NAVTAM.ViewModels;
using System.Device.Location;
using System.Threading.Tasks;

namespace NAVTAM.NsdEngine.OBSTLGUS.V10
{
    public class ObstLgUsNsdManager : ObstNsdBase
    {
        public ObstLgUsNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {
        }

        public override TokenViewModel DeserializeToken(string tokens)
        {
            return Deserialize<ObstLgUsViewModel>(tokens);
        }

        protected override TokenViewModel CreateTokenViewModel()
        {
            return new ObstLgUsViewModel();
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as ObstLgUsViewModel;
            if (token == null)
                return ValidationResult.Fail(Resources.InvalidTokenType);

            var obstTypes = new SimpleObstacles();
            if (!obstTypes.HasType(token.ObstacleType))
                return ValidationResult.Fail(Resources.InvalidObstacleType);

            if (context.NearestAD() == null)
                return ValidationResult.Fail(Resources.NoNearbyAerodromeFound);

            if (context.NearestAerodromeCache() == null)
                return ValidationResult.Fail(Resources.NoNearbyAerodromeFound);

            return ValidationResult.Succeed();
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as ObstLgUsViewModel).Location;
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            // add nearest aerodrome
            var nearestSubject = await GetNearestAerodrome(context.GeoLocation(), context.Uow);
            var aerodrome = await SdoCacheBroker.GetAerodromeCache(context.Uow, nearestSubject.Subject, true, false);
            context.AddContext("nearestAD", nearestSubject);
            context.AddContext("nearestADCache", aerodrome);
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            return context.NearestAD();
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            return nearestAD.Distance > 5 ? GetLocationFir(context.GeoLocation(), context.Uow) : nearestAD.Subject.Fir;
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            if (nearestAD.Distance > 5)
            {
                // all nearby firs
                var nearbyFirs = GetAlternateNearbyFirs(context.GeoLocation(), string.Empty, context.Uow);
                return string.Join(" ", nearbyFirs);
            }
            // Aerodrome Item A
            return CovertAerodromeDesignatorToItemA(nearestAD.Subject.Designator);
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            return !context.IsCancelation ? GenerateNonCancelItemE(context, token as ObstLgUsViewModel) : GenerateCancelItemE(context);
        }

        protected override int GetRadius(TokenViewModel token, ProposalContext context)
        {
            return context.NearestAD().Distance > 5 ? 2 : 5;
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            return context.NearestAD().Distance > 5 ? "E" : "AE";
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            return await GetNotamSeriesForSubject(context.Uow, nearestAD.Subject, "OL", nearestAD.Distance > 5);
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context) => context.NearestAD().Distance > 5 ? "V" : "IV";
        protected override string GetCode23(TokenViewModel token, ProposalContext context) => "OL";
        protected override string GetCode45(TokenViewModel token, ProposalContext context) => !context.IsCancelation ? "AS" : "AK";
        protected override string GetPurpose(TokenViewModel token, ProposalContext context) => "M";
        protected override int GetUpperLimit(TokenViewModel token, ProposalContext context) => RoundUpperLimitToFlightLevel((token as ObstLgUsViewModel).Elevation);

        private BilingualText GenerateNonCancelItemE(ProposalContext context, ObstLgUsViewModel token)
        {
            var uow = context.Uow;

            var location = context.Location;
            var obstacleLocation = location.ToGeoCoordinate();

            var nearestAD = context.NearestAD();
            var nearestSubject = nearestAD.Subject;

            var aerodrome = context.NearestAerodromeCache();

            var subjectLocation = new GeoCoordinate(nearestAD.Subject.RefPoint.Latitude ?? 0, nearestAD.Subject.RefPoint.Longitude ?? 0);
            var bearings = subjectLocation.BearingTo(obstacleLocation) - NSDUtilities.GetAerodromeMagneticVariation(context.Uow, aerodrome, nearestAD.Subject.RefPoint);
            var itemECardinalPoint = GeoDataService.GetDirection(bearings);

            var distToSubject = obstacleLocation.GetDistanceTo(subjectLocation);
            var itemEDistance = ObstacleHelper.BuildItemEDistanceNM(distToSubject);

            var isFirNotam = nearestAD.Distance > 5;

            var amendPub = GetAmendPubMessagePrefix(token);
            var adNamePrefix = isFirNotam ? new BilingualText() : GetItemEAerodromeNamePrefix(context, nearestAD.Subject.Name);
            var itemEPrefix = adNamePrefix.Append(amendPub);
            var refFeature = isFirNotam ? GenerateItemEReferenceFeature(context) : new BilingualText("AD", "AD");

            var obstTypes = new SimpleObstacles();
            var englishObstacles = obstTypes["en"];
            var frenchObstacles = obstTypes["fr"];

            var itemEObstacleType = englishObstacles[token.ObstacleType].EFieldName;
            var itemEObstacleTypeFrench = frenchObstacles[token.ObstacleType].EFieldName;

            var itemELatLong = location.ToDMS();

            var itemEObstacleHeight = token.Height;
            var itemEObstacleElevation = token.Elevation;

            var itemE = $"{itemEPrefix.English}OBST LGT U/S {itemEObstacleType} {itemELatLong} (APRX {itemEDistance} {itemECardinalPoint} {refFeature.English}) {itemEObstacleHeight}FT AGL, {itemEObstacleElevation}FT AMSL.";
            var itemEFrench = $"{itemEPrefix.French}OBST LGT U/S {itemEObstacleTypeFrench} {itemELatLong} (APRX {itemEDistance} {itemECardinalPoint} {refFeature.French}) {itemEObstacleHeight}FT AGL, {itemEObstacleElevation}FT AMSL.";

            return new BilingualText()
            {
                English = itemE,
                French = context.IsBilingual ? itemEFrench : string.Empty
            };
        }

        private BilingualText GenerateCancelItemE(ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            var adNamePrefix = nearestAD.Distance <= 5 ? GetItemEAerodromeNamePrefix(context, nearestAD.Subject.Name) : new BilingualText();

            var itemEEnglish = $"{adNamePrefix.English}OBST LGT {context.Location.ToDMS()} SVCBL";
            var itemEFrench = $"{adNamePrefix.French}OBST LGT {context.Location.ToDMS()} SVCBL";

            return new BilingualText(itemEEnglish, context.IsBilingual ? itemEFrench : string.Empty);
        }

        protected override string GetDisplayName()
        {
            return Resources.ObstLgUsDisplayName;
        }

        protected override string GetNsdHelpLink()
        {
            return Resources.ObstLgtHelpLink;
        }
    }

    internal static class ObstLgUsNsdManagerExt
    {
        internal static NearSdoSubject NearestAD(this ProposalContext context)
        {
            return context?.GetContext<NearSdoSubject>("nearestAD");
        }

        internal static Aerodrome NearestAerodromeCache(this ProposalContext context)
        {
            return context?.GetContext<Aerodrome>("nearestADCache");
        }
    }
}
