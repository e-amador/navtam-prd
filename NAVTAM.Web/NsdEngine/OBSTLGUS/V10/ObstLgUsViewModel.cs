﻿namespace NAVTAM.NsdEngine.OBSTLGUS.V10
{
    public class ObstLgUsViewModel : TokenViewModel
    {
        public string Location { get; set; }
        public string ObstacleType { get; set; }
        public int Elevation { get; set; }
        public int Height { get; set; }
    }
}