﻿using NavCanada.Core.Domain.Model.Enums;

namespace NAVTAM.NsdEngine.OBST.V10
{
    public class ObstViewModel : TokenViewModel
    {
        public string Location { get; set; }
        public string ObstacleType { get; set; }
        public int Elevation { get; set; }
        public int Height { get; set; }
        public SwitchState Lighted { get; set; }
        public SwitchState Painted { get; set; }
    }
}
