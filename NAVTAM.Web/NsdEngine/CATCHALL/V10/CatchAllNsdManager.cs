﻿using Microsoft.Ajax.Utilities;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NAVTAM.NsdEngine.CATCHALL.V10
{
    public class CatchAllNsdManager : NsdManager
    {
        public CatchAllNsdManager()
        {
        }

        public CatchAllNsdManager(int categoryId, string categoryName, string version)
            : base(categoryId, categoryName, version)
        {
        }

        public override TokenViewModel DeserializeToken(string tokens)
        {
            var token = Deserialize<CatchAllViewModel>(tokens);
            //Cleaning ItemEFrenchValue because an oldest error that allowed proposals to be saved with Token.ItemEFrenchValue in a not bilingual region. 
            token.ItemEFrenchValue = token.IsBilingual || token.OverrideBilingual ? token.ItemEFrenchValue : null;
            return token;
        }

        protected override TokenViewModel CreateTokenViewModel() => new CatchAllViewModel();

        protected override async Task UpdateNsdContext(TokenViewModel baseToken, ProposalContext context)
        {
            var token = baseToken as CatchAllViewModel;

            var subjectViewModel = await GetSubjectViewModel(token.SubjectId, context.Uow);
            context.AddContext("Subject", subjectViewModel);

            var usedSeries = await context.Uow.SeriesChecklistRepo.GetSeriesInUseAsync();
            context.AddContext("UsedSeries", usedSeries);

            //var forbiddenItemEWords = await context.Uow.ConfigurationValueRepo.GetValueAsync("NOTAM", "NOTAMForbiddenWords");
            //context.AddContext("ForbiddenItemEWords", forbiddenItemEWords);

            var itemACache = await CreateItemACache(token.ItemA, context);
            context.AddContext("ItemACache", itemACache);

            var designators = (token.ItemA ?? "").ToUpper().Split(SPACE, StringSplitOptions.RemoveEmptyEntries).ToList();
            var itemA = await context.Uow.SdoCacheRepo.GetByDesignatorAsync(designators[0]);
            context.AddContext("ItemA", itemA);
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as CatchAllViewModel;
            if (token == null)
                return ValidationResult.Fail(Resources.InvalidTokenType);

            //GuaranteeCapitals(model.Token as CatchAllViewModel);

            //Validate ItemA
            var itemA = context.ItemA();
            if(itemA == null && token.ItemA != "CXXX") // PLEASE CHECK THIS LATER WITH ERIK
                return ValidationResult.Fail(Resources.InvalidItemA);

            // validate subject
            var subject = context.Subject();
            if (subject == null)
                return ValidationResult.Fail(Resources.InvalidSubjectId);

            // aerodrome name
            if (subject.Ad.Any(char.IsDigit) &&  token.AerodromeName.IsNullOrWhiteSpace())
                return ValidationResult.Fail(Resources.MissingAerodromeName);

            return !context.IsCancelation ? ValidateNonCancelationModel(model, context) : ValidateCancelationModel(model, context);
        }

        protected override bool IsCatchAll => true;
        protected override bool ContainFreeText => true;

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as CatchAllViewModel).SubjectLocation;
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            return new NearSdoSubject(context.Subject().Subject, 0.0);
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            return context.Subject()?.Fir?.ToUpper();
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            return (token as CatchAllViewModel).ItemA?.ToUpper();
        }

        protected override BilingualText GenerateItemE(TokenViewModel baseToken, ProposalContext context)
        {
            var token = baseToken as CatchAllViewModel;
            var subject = context.Subject();

            var itemEPrefix = GetItemEAerodromeNamePrefix(context, subject.Subject.Name, token.AerodromeName).Append(GetAmendPubMessagePrefix(token));

            var itemE = (itemEPrefix.English + TableFormatter.FormatSquareBracketTables(token.ItemEValue ?? "")).ToUpper();

            //var itemEFrench = context.IsBilingual ? (itemEPrefix.French + TableFormatter.FormatSquareBracketTables(token.ItemEFrenchValue ?? "")).ToUpper() : string.Empty;
            var itemEFrench = (context.IsBilingual || baseToken.OverrideBilingual) ? (itemEPrefix.French + TableFormatter.FormatSquareBracketTables(token.ItemEFrenchValue ?? "")).ToUpper() : string.Empty;

            return new BilingualText(itemE, itemEFrench);
        }

        protected override int GetRadius(TokenViewModel token, ProposalContext context)
        {
            return (token as CatchAllViewModel).Radius;
        }

        protected override string GenerateItemF(TokenViewModel token, ProposalContext context)
        {
            return !context.IsCancelation && context.IcaoCondition.AllowFgEntry ? GetItemF(token as CatchAllViewModel) : string.Empty;
        }

        protected override string GenerateItemG(TokenViewModel token, ProposalContext context)
        {
            return !context.IsCancelation && context.IcaoCondition.AllowFgEntry ? GetItemG(token as CatchAllViewModel) : string.Empty;
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            var scope = context.IcaoSubject.AllowScopeChange ? (token as CatchAllViewModel).Scope : context.IcaoSubject.Scope;
            return scope?.ToUpper();
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            var series = !context.IcaoSubject.AllowSeriesChange ? await GetDefaultSeries(context) : (token as CatchAllViewModel).Series;
            return series?.ToUpper();
        }

        static Task<string> GetDefaultSeries(ProposalContext context)
        {
            var sdoCache = context.Subject().Subject;
            return GetNotamSeriesForSubject(context.Uow, sdoCache, context.IcaoSubject.Code, sdoCache.Type == SdoEntityType.Fir);
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            return GetTraffic(token as CatchAllViewModel);
        }

        protected override Task<IcaoSubject> GetIcaoSubject(TokenViewModel token, ProposalContext context)
        {
            return context.Uow.IcaoSubjectRepo.GetByIdAsync((token as CatchAllViewModel).IcaoSubjectId);
        }

        protected override Task<IcaoSubjectCondition> GetIcaoCondition(TokenViewModel token, ProposalContext context, int icaoSubjectId)
        {
            return context.Uow.IcaoSubjectConditionRepo.GetByIdAsync((token as CatchAllViewModel).IcaoConditionId);
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            return context.IcaoSubject.Code;
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            return context.IcaoCondition.Code;
        }

        protected override string GetPurpose(TokenViewModel baseToken, ProposalContext context)
        {
            var token = baseToken as CatchAllViewModel;
            return context.IsCancelation ? GetPurposeFromModel(token) : GetPurpose(context.IcaoCondition, token);
        }

        protected override int GetLowerLimit(TokenViewModel baseToken, ProposalContext context)
        {
            var token = baseToken as CatchAllViewModel;
            if (context.IsCancelation || context.IcaoCondition.AllowFgEntry)
            {
                if (token.ItemFUnits == UnitOfMeasure.AGL || token.ItemGUnits == UnitOfMeasure.AGL) return token.LowerLimit;
                else return GetLowerLimit(token);
            }
            else return token.LowerLimit;

            //return context.IsCancelation || context.IcaoCondition.AllowFgEntry ? GetLowerLimit(token) : token.LowerLimit;
        }

        protected override int GetUpperLimit(TokenViewModel baseToken, ProposalContext context)
        {
            var token = baseToken as CatchAllViewModel;
            if (context.IsCancelation || context.IcaoCondition.AllowFgEntry)
            {
                if (token.ItemFUnits == UnitOfMeasure.AGL || token.ItemGUnits == UnitOfMeasure.AGL) return token.UpperLimit;
                else return GetUpperLimit(token);
            }
            else return token.UpperLimit;

            //return context.IsCancelation || context.IcaoCondition.AllowFgEntry ? GetUpperLimit(token) : token.UpperLimit;
        }

        protected override void PreProcessToken(TokenViewModel rawToken)
        {
            base.PreProcessToken(rawToken);
            var token = rawToken as CatchAllViewModel;
            // trim spaces in itemA
            var splitted = (token.ItemA ?? "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            token.ItemA = string.Join(" ", splitted);
        }

        static ValidationResult ValidateCancelationModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as CatchAllViewModel;

            if (!token.Immediate)
                return ValidationResult.Fail(Resources.ErrorCancellationStartActivity);

            if (token.Permanent || token.EndValidity.HasValue)
                return ValidationResult.Fail(Resources.ErrorCancellationEndValidity);

            return ValidationResult.Succeed();
        }

        static ValidationResult ValidateNonCancelationModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as CatchAllViewModel;

            // scope
            if (!ValidateScope(context.IcaoSubject, token.Scope))
                return ValidationResult.Fail(Resources.InvalidScopeValue);

            // purpose
            var validation = ValidatePurpose(context.IcaoCondition, token);
            if (validation.HasErrors)
                return validation;

            // traffic
            validation = ValidateTraffic(context.IcaoCondition, token);
            if (validation.HasErrors)
                return validation;

            // item F & G
            validation = ValidateItemFandG(context.IcaoCondition, token);
            if (validation.HasErrors)
                return validation;

            // lower limit
            if (token.LowerLimit < 0 || token.LowerLimit > 999)
                return ValidationResult.Fail(Resources.LowerLimitInvalid);

            // upper limit
            if (token.UpperLimit < 1 || token.UpperLimit > 999)
                return ValidationResult.Fail(Resources.UpperLimitInvalid);

            // lower vs upper limit
            if (token.LowerLimit > token.UpperLimit)
                return ValidationResult.Fail(Resources.LowerAndUpperLimitInvalid);

            // radius
            if (token.Radius < 1 || token.Radius > 999)
                return ValidationResult.Fail(Resources.RadiusInvalidValue);

            // series
            validation = ValidateSeries(context.IcaoSubject, token, context.GetContext<string>("UsedSeries"));
            if (validation.HasErrors)
                return validation;

            // item E
            validation = ValidateItemE(token, context.IsBilingual || token.OverrideBilingual, context.GetContext<string>("ForbiddenItemEWords"));
            if (validation.HasErrors)
                return validation;

            // item A 
            validation = ValidateItemA(token.ItemA.Trim(), context.GetContext<ItemACache>("ItemACache"));
            if (validation.HasErrors)
                return validation;

            return ValidationResult.Succeed();
        }

        static ValidationResult ValidateItemA(string itemA, ItemACache itemACache)
        {
            if (itemA.IsNullOrWhiteSpace())
                return ValidationResult.Fail(Resources.ItemARequired);

            if (itemA.Any(char.IsDigit))
                return ValidationResult.Fail(Resources.ItemANoDigits);

            if (itemACache.Designators.Any(d => !d.Valid || !d.Exists))
                return ValidationResult.Fail(Resources.ItemAInvalid);

            /* We are not validating if the FIR is in the user DOA because the user can reference a FIR that 
             * it is adjacent to his/her FIR and out of his/her DOA
             * 
            if (itemACache.Designators.Any(d => !d.InDoa))
                return ValidationResult.Fail(Resources.ItemANotInDOA);
            */

            //Validate repeated itemA on the list
            foreach (var item in itemACache.Designators) {
                if (itemACache.Designators.Where(x => x.Designator == item.Designator).Count() > 1) {
                    return ValidationResult.Fail(Resources.InvalidItemAFirsRepeated);
                }
            }
            /* As per Erik request: We are NOT validating adjacency between FIRs on the list
             * Feb 12, 2018
             * 
            if (!ValidateAdjacentFirs(itemACache.Designators))
                return ValidationResult.Fail(Resources.InvalidItemAFirsNotAdjacent);
            */
            return ValidationResult.Succeed();
        }

        static bool ValidateAdjacentFirs(List<DesignatorInfo> designators)
        {
            for (int i = 1; i < designators.Count - 1; i++)
            {
                if (!AreFirsAdjacent(designators[i - 1].Designator, designators[i].Designator))
                    return false;
            }
            return true;
        }

        static HashSet<string> AdjacentPairs = new HashSet<string>()
        {
            "CZVR,CZEG",
            "CZEG,CZWG",
            "CZEG,CZUL",
            "CZEG,CZQX",
            "CZWG,CZYZ",
            "CZWG,CZUL",
            "CZYZ,CZUL",
            "CZUL,CZQM",
            "CZUL,CZQX",
            "CZQM,CZQX",
            "KZSE,CZVR",
            "KZSE,CZEG",
            "KZSE,KZLC",
            "KZLC,CZEG",
            "KZLC,CZWG",
            "KZLC,KZMP",
            "KZMP,CZWG",
            "KZMP,CZYZ",
            "KZMP,KZOB",
            "KZOB,CZYZ",
            "KZOB,KZBW",
            "KZBW,CZYZ",
            "KZBW,CZUL",
            "KZBW,CZQM",
            "PAZA,CZEG",
            "PAZA,CZVR",
            "BGGL,CZQZ",
            "BGGL,CZEG",
            "BGGL,BIRD",
            "BIRD,CZQX",
            "BIRD,EGGX",
            "EGGX,CZQX",
            "EGGX,LPPO",
            "LPPO,CZQX"
        };

        static bool AreFirsAdjacent(string fir1, string fir2)
        {
            return AdjacentPairs.Contains($"{fir1},{fir2}") || AdjacentPairs.Contains($"{fir2},{fir1}");
        }

        static ValidationResult ValidateItemE(CatchAllViewModel token, bool isBilingual, string forbiddenWords)
        {
            if (token.ItemEValue.IsNullOrWhiteSpace())
                return ValidationResult.Fail(Resources.ItemERequired);

            if (ContainsForbiddenWord(token.ItemEValue, forbiddenWords))
                return ValidationResult.Fail(Resources.ItemENotValid);

            if (isBilingual)
            {
                if (token.ItemEFrenchValue.IsNullOrWhiteSpace())
                    return ValidationResult.Fail(Resources.ItemEFrenchRequired);

                if (ContainsForbiddenWord(token.ItemEFrenchValue, forbiddenWords))
                    return ValidationResult.Fail(Resources.ItemEFrenchNotValid);
            }
            else
            {
                //Cleaning ItemEFrenchValue because an oldest error that allowed proposals to be saved with Token.ItemEFrenchValue in a not bilingual region. 
                if (!token.ItemEFrenchValue.IsNullOrWhiteSpace())
                {
                    token.ItemEFrenchValue = null;
                }

            }

            return ValidationResult.Succeed();
        }

        static ValidationResult ValidateSeries(IcaoSubject subject, CatchAllViewModel token, string usedSeries)
        {
            if (subject.AllowSeriesChange)
            {
                if (token.Series.IsNullOrWhiteSpace())
                    return ValidationResult.Fail(Resources.SeriesRequired);

                if (token.Series.Length != 1)
                    return ValidationResult.Fail(Resources.SeriesRange);

                var series = token.Series[0];
                if (!usedSeries.Any(s => s == series))
                    return ValidationResult.Fail(Resources.SeriesNotInUse);
            }
            else
            {
                if (!token.Series.IsNullOrWhiteSpace())
                    return ValidationResult.Fail(Resources.ChangeSerieNotAllowed);
            }
            return ValidationResult.Succeed();
        }

        static ValidationResult ValidateItemFandG(IcaoSubjectCondition condition, CatchAllViewModel token)
        {
            if (!condition.AllowFgEntry)
            {
                var expected = (token.ItemFValue == null || token.ItemFValue == 0) && !token.ItemFSurface;
                if (!expected)
                    return ValidationResult.Fail(Resources.ItemFInvalid);

                expected = (token.ItemGValue == null || token.ItemGValue == 1) && !token.ItemGUnlimited;
                if (!expected)
                    return ValidationResult.Fail(Resources.ItemGNotAllowed);
            }
            else //icaoCondition.AllowFgEntry
            {
                if (!token.ItemFValue.HasValue && !token.ItemFSurface)
                    return ValidationResult.Fail(Resources.ItemFRequired);

                if (!token.ItemGValue.HasValue && !token.ItemGUnlimited)
                    return ValidationResult.Fail(Resources.ItemGRequired);

                //if (token.ItemFUnits == UnitOfMeasure.AGL || token.ItemGUnits == UnitOfMeasure.AGL)
                //    return ValidationResult.Fail(Resources.ErrorOnUnits);

                if (token.ItemFUnits == UnitOfMeasure.AMSL)
                {
                    if (token.ItemFValue > 99899 && !token.ItemFSurface)
                        return ValidationResult.Fail(Resources.ItemFRange2);

                    if (token.ItemFValue < 1 && !token.ItemFSurface)
                        return ValidationResult.Fail(Resources.ItemFRange5);
                }
                else if (token.ItemFUnits == UnitOfMeasure.FL)
                {
                    if (token.ItemFValue > 998 && !token.ItemFSurface)
                        return ValidationResult.Fail(Resources.ItemFRange3);

                    if (token.ItemFValue < 0 && !token.ItemFSurface)
                        return ValidationResult.Fail(Resources.ItemFRange1);
                }
                else //UnitOfMeasure.AGL
                {
                    if (token.ItemFValue > 99899 && !token.ItemFSurface)
                        return ValidationResult.Fail(Resources.ItemFRange2);

                    if (token.ItemFValue < 1 && !token.ItemFSurface)
                        return ValidationResult.Fail(Resources.ItemFRange5);
                }

                if (token.ItemGUnits == UnitOfMeasure.AMSL)
                {
                    if (token.ItemGValue > 99999 && !token.ItemGUnlimited)
                        return ValidationResult.Fail(Resources.ItemGRange1);

                    if (token.ItemGValue < 1 && !token.ItemGUnlimited)
                        return ValidationResult.Fail(Resources.ItemGRange4);
                }
                else if (token.ItemGUnits == UnitOfMeasure.FL)
                {
                    if (token.ItemGValue.HasValue && token.ItemGValue > 999 && !token.ItemGUnlimited)
                        return ValidationResult.Fail(Resources.ItemGRange2);

                    if (token.ItemGValue.HasValue && token.ItemGValue < 1 && !token.ItemGUnlimited)
                        return ValidationResult.Fail(Resources.ItemGRange5);
                }
                else // UnitOfMeasure.AGL
                {
                    if (token.ItemGValue > 99999 && !token.ItemGUnlimited)
                        return ValidationResult.Fail(Resources.ItemGRange1);

                    if (token.ItemGValue < 1 && !token.ItemGUnlimited)
                        return ValidationResult.Fail(Resources.ItemGRange4);
                }

                if (token.ItemFValue.HasValue && token.ItemGValue.HasValue)
                {
                    if(token.ItemFUnits != UnitOfMeasure.AGL && token.ItemGUnits != UnitOfMeasure.AGL)
                    {
                        if (token.ItemFUnits == token.ItemGUnits)
                        {
                            if (token.ItemFValue > token.ItemGValue && !token.ItemFSurface && !token.ItemGUnlimited)
                                return ValidationResult.Fail(Resources.ItemGRange3);
                        }
                        else
                        {
                            if (token.ItemFUnits != UnitOfMeasure.AGL && token.ItemGUnits != UnitOfMeasure.AGL)
                            {
                                var fvalue = token.ItemFValue.Value * (token.ItemFUnits == UnitOfMeasure.FL ? 100 : 1);
                                var gvalue = token.ItemGValue.Value * (token.ItemGUnits == UnitOfMeasure.FL ? 100 : 1);
                                if (fvalue > gvalue && !token.ItemFSurface && !token.ItemGUnlimited)
                                    return ValidationResult.Fail(Resources.ItemGRange3);
                            }
                        }
                    }

                }
                else
                {
                    if (!token.ItemFValue.HasValue && !token.ItemFSurface)
                        return ValidationResult.Fail(Resources.ItemFRequired);

                    if (!token.ItemGValue.HasValue && !token.ItemGUnlimited)
                        return ValidationResult.Fail(Resources.ItemGRequired);
                }
            }
            return ValidationResult.Succeed();
        }

        static ValidationResult ValidateTraffic(IcaoSubjectCondition condition, CatchAllViewModel token)
        {
            // traffic (test db configuration)
            if (!condition.I && !condition.V)
                return ValidationResult.Fail(Resources.TrafficConfigDBError);

            // compare token to condition
            if (condition.I && condition.V)
            {
                // both IV enabled, then valid a combination expected (I, V, IV)
                if (!token.TrafficI && !token.TrafficV)
                    return ValidationResult.Fail(Resources.TrafficRequired);
            }
            else
            {
                // Both I & V must match the condition
                if (token.TrafficI != condition.I || token.TrafficV != condition.V)
                    return ValidationResult.Fail(Resources.TrafficRequired);
            }

            return ValidationResult.Succeed();
        }

        static ValidationResult ValidatePurpose(IcaoSubjectCondition condition, CatchAllViewModel token)
        {
            if (!condition.AllowPurposeChange)
            {
                // no purpose or matches the icao condition
                if (PurposeMismatch(token.PurposeB, condition.B) ||
                    PurposeMismatch(token.PurposeM, condition.M) ||
                    PurposeMismatch(token.PurposeN, condition.N) ||
                    PurposeMismatch(token.PurposeO, condition.O))
                    return ValidationResult.Fail(Resources.PurposeValuesError);

                // produces a proper purpose enum given the condition
                PurposeType purpose;
                if (!Enum.TryParse(GetPurposeFromIcaoSubjectCondition(condition), out purpose))
                    return ValidationResult.Fail($"Invalid Purpose for condition {condition.Id} on the DB.");
            }
            else
            {
                var purpose = GetPurposeFromModel(token);

                // no purpose flag passed
                if (string.IsNullOrEmpty(purpose))
                    return ValidationResult.Fail(Resources.PurposeRequired);

                // more than 3 purposes passed
                if (purpose.Length > 3)
                    return ValidationResult.Fail(Resources.MorePurposesDefined);

                // invalid purpose flag combination
                PurposeType purposeType;
                if (!Enum.TryParse(purpose, true, out purposeType))
                    return ValidationResult.Fail(Resources.PurposeCombinationNotAllowed);
            }
            return ValidationResult.Succeed();
        }

        static bool ValidateScope(IcaoSubject subject, string scope)
        {
            if (subject.AllowScopeChange)
            {
                var validScopes = new List<string>() { "A", "E", "W", "AE", "AW", "K" };
                return validScopes.Any(s => string.Equals(scope, s, StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                // send no scope or send the same as in the icao subject
                return string.IsNullOrEmpty(scope) || scope == subject.Scope;
            }
        }

        static bool PurposeMismatch(bool? tokenPurpose, bool conditionPurpose)
        {
            return tokenPurpose.HasValue && tokenPurpose.Value != conditionPurpose;
        }

        static int GetLowerLimit(CatchAllViewModel token)
        {
            if (token.ItemFSurface)
                return 0;

            if (token.ItemFValue.HasValue && token.ItemFValue == 0)
                return token.LowerLimit;

            if (!token.ItemFValue.HasValue)
                return token.LowerLimit;

            if (token.ItemFUnits == UnitOfMeasure.FL)
                return token.ItemFValue.Value;

            return RoundLowerLimitToFlightLevel(token.ItemFValue.Value);
        }

        static int GetUpperLimit(CatchAllViewModel token)
        {
            if (token.ItemGUnlimited)
                return MaxUpperLimit;

            if (token.ItemGValue.HasValue && token.ItemGValue == 1)
                return token.UpperLimit;

            if (!token.ItemGValue.HasValue)
                return token.UpperLimit;

            if (token.ItemGUnits == UnitOfMeasure.FL)
                return token.ItemGValue.Value;

            return RoundUpperLimitToFlightLevel(token.ItemGValue.Value);
        }

        static string GetItemF(CatchAllViewModel token)
        {
            if (token.ItemFSurface) return NsdDefinitions.SFC;

            var itemF = token.ItemFValue.HasValue ? token.ItemFValue.Value.ToString() : "";

            if (token.ItemFUnits == UnitOfMeasure.FL) {
                return NsdDefinitions.FL + itemF.PadLeft(3, '0');
            }

            if (token.ItemFUnits == UnitOfMeasure.AMSL) return itemF + "FT " + NsdDefinitions.AMSL;

            return itemF + "FT " + NsdDefinitions.AGL;
        }

        static string GetItemG(CatchAllViewModel token)
        {
            if (token.ItemGUnlimited) return NsdDefinitions.UNL;

            var itemG = token.ItemGValue.HasValue ? token.ItemGValue.Value.ToString() : "";

            if (token.ItemGUnits == UnitOfMeasure.FL) { return NsdDefinitions.FL + itemG.PadLeft(3,'0'); }
            if (token.ItemGUnits == UnitOfMeasure.AMSL) return itemG + "FT " + NsdDefinitions.AMSL;

            return itemG + "FT " + NsdDefinitions.AGL;
        }

        static string GetPurpose(IcaoSubjectCondition icaoCond, CatchAllViewModel token)
        {
            return icaoCond.AllowPurposeChange ? GetPurposeFromModel(token) : GetPurposeFromIcaoSubjectCondition(icaoCond);
        }

        static string GetPurposeFromModel(CatchAllViewModel token)
        {
            return  (token.PurposeN == true ? NsdDefinitions.PurposeN : string.Empty) +
                    (token.PurposeB == true ? NsdDefinitions.PurposeB : string.Empty) +
                    (token.PurposeO == true ? NsdDefinitions.PurposeO : string.Empty) +
                    (token.PurposeM == true ? NsdDefinitions.PurposeM : string.Empty);
        }

        static string GetPurposeFromIcaoSubjectCondition(IcaoSubjectCondition cond)
        {
            return (cond.N ? NsdDefinitions.PurposeN : string.Empty) +
                   (cond.B ? NsdDefinitions.PurposeB : string.Empty) +
                   (cond.O ? NsdDefinitions.PurposeO : string.Empty) +
                   (cond.M ? NsdDefinitions.PurposeM : string.Empty);
        }

        static string GetTraffic(CatchAllViewModel token)
        {
            if (token.TrafficI && token.TrafficV) return "IV";
            if (token.TrafficI) return "I";
            if (token.TrafficV) return "V";
            return "K";
        }

        static char[] SPACE = new char[] { ' ' };

        static async Task<ItemACache> CreateItemACache(string itemA, ProposalContext context)
        {
            var designators = (itemA ?? "").Trim().ToUpper().Split(SPACE, StringSplitOptions.RemoveEmptyEntries).ToList();
            var pairs = new List<DesignatorInfo>();
            if (designators.Count > 1)
            {
                foreach (var designator in designators)
                {
                    var pair = new DesignatorInfo(designator);

                    // valid designator
                    pair.Valid = designator.Length == 4 && !designator.Any(char.IsDigit);

                    // get the subject
                    var fir = await context.Uow.SdoCacheRepo.GetFirByDesignatorAsync(designator);

                    // designator exists (todo: add method to repo)
                    pair.Exists = fir != null;

                    // subject is in context Doa - We are not validating this for now
                    pair.InDoa = context.UserDoa != null && fir?.RefPoint?.Intersects(context.UserDoa) == true;

                    if (pair.Valid && pair.Exists)
                        pairs.Add(pair);
                }
            }
            return new ItemACache(pairs);
        }

        public override TokenViewModel CopyTokenForGroupCancellation(TokenViewModel master, TokenViewModel current)
        {
            var masterToken = master as CatchAllViewModel;
            var currentToken = current as CatchAllViewModel;
            currentToken.IcaoConditionId = masterToken.IcaoConditionId;
            currentToken.ItemEValue = masterToken.ItemEValue;
            currentToken.ItemEFrenchValue = masterToken.ItemEFrenchValue;

            return currentToken;
        }

        protected override string GetDisplayName()
        {
            return Resources.CatchAllDisplayName;
        }

        protected override string GetNsdHelpLink()
        {
            return Resources.CatchAllHelpLink;
        }
    }

    internal static class CatchAllNsdManagerExt
    {
        internal static SubjectViewModel Subject(this ProposalContext context)
        {
            return context?.GetContext<SubjectViewModel>("Subject");
        }

        internal static SdoCache ItemA(this ProposalContext context)
        {
            return context?.GetContext<SdoCache>("ItemA");
        }
    }

    internal class DesignatorInfo
    {
        public DesignatorInfo(string designator)
        {
            Designator = designator;
            Valid = true;
            Exists = true;
            InDoa = true;
        }

        public string Designator { get; set; }
        public bool Valid { get; set; }
        public bool Exists { get; set; }
        public bool InDoa { get; set; }
    }

    internal class ItemACache
    {
        public ItemACache(IEnumerable<DesignatorInfo> designators)
        {
            Designators = designators.ToList();
        }

        public List<DesignatorInfo> Designators { get; private set; }
    }
}
