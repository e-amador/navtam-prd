﻿using NavCanada.Core.Domain.Model.Enums;

namespace NAVTAM.NsdEngine.CATCHALL.V10
{

    public class CatchAllViewModel : TokenViewModel
    {
        public CatchAllViewModel()
        {
        }

        public int IcaoSubjectId { get; set; }
        public int IcaoConditionId { get; set; }

        public string SubjectLocation { get; set; }

        public int? ItemFValue { get; set; }
        public UnitOfMeasure ItemFUnits { get; set; }
        public bool ItemFSurface { get; set; }
        public bool RequiresItemFG { get; set; }
        public int? ItemGValue { get; set; }
        public UnitOfMeasure ItemGUnits { get; set; }
        public bool ItemGUnlimited { get; set; }

        public bool RequiresItemA { get; set; }
        public bool RequiresScope { get; set; }
        public bool RequiresSeries { get; set; }

        public bool RequiresPurpose { get; set; }
        public bool? PurposeB { get; set; }
        public bool? PurposeM { get; set; }
        public bool? PurposeN { get; set; }
        public bool? PurposeO { get; set; }

        public bool RequiresTraffic { get; set; }
        public bool TrafficI { get; set; }
        public bool TrafficV { get; set; }

        public string AerodromeName { get; set; }
        public string ItemEValue { get; set; }
        public string ItemEFrenchValue { get; set; }

        public string ItemA { get; set; }
        public string Series { get; set; }
        public int Radius { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public string Scope { get; set; }

        //Add on April 7, 2017 - Because Omar needs to work with this more exact data instead of subjectLocation
        //they are going to be float
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

    public enum UnitOfMeasure
    {
        FL = 0,
        AMSL = 1,
        AGL = 2,
    }
}