﻿using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NAVTAM.NsdEngine.Helpers;

namespace NAVTAM.NsdEngine
{
    public class NsdValidator
    {
        private readonly ProposalViewModel _model;
        private readonly ProposalContext _context;

        public NsdValidator(ProposalViewModel model, ProposalContext context)
        {
            _model = model;
            _context = context;
        }

        public async Task<ValidationResult> Validate()
        {
            var ErrorList = new List<string>();
            var success = true;
            try
            {
                //if(!_model.Immediate)
                //{
                //    var vsp = _context.Vsp;
                //    if(_model.StartActivity == null)
                //    {
                //        ErrorList.Add(Resources.InvalidStartValidity);
                //        return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //    }
                //    if (_model.StartActivity < DateTime.UtcNow)
                //    {
                //        ErrorList.Add(Resources.InvalidStartValidity);
                //        return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //    }

                //    //vsp use
                //    var hours = (_model.StartActivity - DateTime.UtcNow).Value.TotalHours;
                //    success = vsp == 0 || hours <= vsp;
                //    if(!success)
                //    {
                //        ErrorList.Add(
                //            String.Format(Resources.StartValidityNotInRange, vsp));
                //        return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //    }
                //}
                //if(!_model.Permanent)
                //{
                //    if( _model.EndValidity == null)
                //    {
                //        ErrorList.Add(Resources.EndValidityRequired);
                //        return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //    }
                //    if( !_model.Immediate)
                //    {
                //        if (_model.EndValidity > _model.StartActivity.Value.AddHours(2208))
                //        {
                //            ErrorList.Add(Resources.ValidityPeriodRangeExceed);
                //            return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //        }
                //        if (_model.EndValidity < _model.StartActivity.Value.AddMinutes(30))
                //        {
                //            ErrorList.Add(Resources.ValidityPeriodRangeShort);
                //            return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //        }
                //    }
                //    else //_model.Immediate
                //    {
                //        if (_model.EndValidity < DateTime.UtcNow.AddMinutes(30))
                //        {
                //            ErrorList.Add(Resources.ValidityPeriodRangeShort);
                //            return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //        }
                //    }
                //}

                //if( !String.IsNullOrWhiteSpace(_model.ItemD))
                //{
                //    success = CommonValidators.ValidateTimeSchedule(_model.ItemD, (DateTime)_model.StartActivity, (DateTime)_model.EndValidity);
                //    if( !success)
                //    {
                //        ErrorList.Add(Resources.InvalidItemD);
                //        return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
                //    }
                //}
            }
            catch (Exception e)
            {
                ErrorList.Add(e.Message);
                return await Task.FromResult(ValidationResult.FailWidth(ErrorList));
            }

            return await Task.FromResult(ValidationResult.Succeed());
        }
    }
}