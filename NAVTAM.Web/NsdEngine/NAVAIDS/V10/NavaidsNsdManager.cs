﻿using System;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System.Threading.Tasks;
using NavCanada.Core.Common.Common;
using System.Data.Entity.Spatial;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.ViewModels;

namespace NAVTAM.NsdEngine.NAVAIDS.V10
{
    public class NavaidsNsdManager : NsdManager
    {
        public NavaidsNsdManager() { }
      

        public NavaidsNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version) { }
        //public override string FullyQualifiedModelName => $"{typeof(NavaidsNsdManager).Namespace}.{CategoryName}ViewModel";
        protected override TokenViewModel CreateTokenViewModel() => new NavaidsViewModel();
        protected override TokenViewModel DeserializeToken(string tokens) => Deserialize<NavaidsViewModel>(tokens);
        


        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as NavaidsViewModel).Location;
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            // add nearest aerodrome
            context.AddContext("nearestAD", await GetNearestAerodrome(context.Location, context.Uow));
        }
        
        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            return context.NearestSubject.Subject.Fir;
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            //var nearestAD = context.NearestAD();
            //if (nearestAD.Distance > 5)
            //{
            //    // Fir Item A
            //    var fir = nearestAD.Subject.Fir;
            //    var point = DbGeography.PointFromText(context.Location.GeoPointText, CommonDefinitions.Srid);
            //    var nearbyFirs = GetAlternateNearbyFirs(point, fir, context.Uow);
            //    return $"{fir} {string.Join(" ", nearbyFirs)}".Trim();
            //}
            //// Aerodrome Item A
            //return CovertAerodromeDesignatorToItemA(nearestAD.Subject.Designator);


            throw new NotImplementedException();
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
            //return !context.IsCancelation ? GenerateNonCancelItemE(context, token as NavaidsViewModel) : GenerateCancelItemE(context, token as NavaidsViewModel);
        }

        private BilingualText GenerateCancelItemE(ProposalContext context, NavaidsViewModel navViewModel)
        {
            throw new NotImplementedException();
        }

        private BilingualText GenerateNonCancelItemE(ProposalContext context, NavaidsViewModel navViewModel)
        {
            throw new NotImplementedException();
        }

        protected override int GetRadius(TokenViewModel token, ProposalContext context)
        {
            //return context.NearestAD().Distance > 5 ? 2 : 5;
            throw new NotImplementedException();
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            //TODO Check if associated with an aerodrome
            //TODO Check if codeId is I
            throw new NotImplementedException();
        }


        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            //var nearestAD = context.NearestAD();

            //if (nearestAD.Distance > 5)
            //    return GetNotamSeriesByGeoRegion(context.Uow, context.GeoLocation, "OB");

            //return await GetNotamProposalSeriesBySubjectCode(context.Uow, nearestAD.Subject.Designator, "OB");

            throw new NotImplementedException();
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            if ((token as NavaidsViewModel).NavaidType == "DME")
            {
                if (!CheckCodeIDIdentifier((token as NavaidsViewModel).Identifier))
                    return TrafficType.I.ToString();
            }

            return TrafficType.IV.ToString();
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            return GenerateCode23(token as NavaidsViewModel); ;
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            return (token as NavaidsViewModel).Condition.Equals("Unmonitored") ? "XX" : "AS";
        }

        protected override string GetPurpose(TokenViewModel token, ProposalContext context)
        {
            //if(CheckIfCode23IsId)
            //  return "NBO"

            return "BO";
        }

        protected override int GetUpperLimit(TokenViewModel token, ProposalContext context)
        {
            return 999;
        }

        private string GenerateCode23(NavaidsViewModel token)
        {
            switch (token.Identifier)
            {
                case "VOR/DME":
                    return "NM";
                case "VOR":
                    return "NV";
                case "DME":
                    if(!token.Identifier.IndexOf('I').Equals(0) || CheckCodeIDIdentifier(token.Identifier))
                        return "ND";

                    return "ID";             
                case "NDB":
                    return "NB";
                case "TACAN":
                    return "NN";
                case "VORTAC":
                    return "NT";
                default:
                    return "";
            }
        }

        private bool CheckCodeIDIdentifier(string Identifier)
        {
            string[] dmeCodeIds =
            {
                "SP",
                "XCG",
                "XYF",
                "XPP"
            };

            foreach (var ident in dmeCodeIds)
            {
                if (Identifier.StartsWith(ident))
                    return true;
            }

            return false;
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        //TODO CHECK IF CODE ID == "ID"

        //internal static class NavAidExtn
        //{

        //    internal static 





        //}
    }
}