﻿using System;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.Helpers;
using System.Collections.Generic;
using Business.Common;
using NavCanada.Core.Common.Extensions;

namespace NAVTAM.NsdEngine
{
    public class ProposalViewModel : INotam
    {
        public ProposalViewModel()
        {
            ProposalType = NotamType.N;
        }

        public ProposalViewModel(int categoryId, string version, string categoryName, string displayName, string helpLink, Proposal proposal)
        {
            CategoryId = categoryId;
            Name = categoryName;
            Version = version;
            ProposalType = NotamType.N;
            DisplayName = displayName;
            CopyFromProposal(proposal);
            HelpLink = helpLink;
        }

        public string DisplayName { get; set; }
        public int? ProposalId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public NsdCategory Category { get; set; }
        public string Version { get; set; }
        public int OrganizationId { get; set; } // a.k.a. OnBehalfOfOrganizationId
        public string Nof { get; set; }
        public string Series { get; set; }
        public int Number { get; set; }
        public int Year { get; set; }
        public NotamType ProposalType { get; set; } // a.k.a. type
        public string ReferredSeries { get; set; }
        public int ReferredNumber { get; set; }
        public int ReferredYear { get; set; }
        public string Fir { get; set; }
        public string Code23 { get; set; }
        public int IcaoSubjectId { get; set; }
        public string Code45 { get; set; }
        public int IcaoConditionId { get; set; }
        public string Traffic { get; set; }
        public string Purpose { get; set; }
        public string Scope { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public string Coordinates { get; set; }
        public int Radius { get; set; }
        public string ItemA { get; set; }
        public bool Immediate { get; set; }
        public DateTime? StartActivity { get; set; }
        public DateTime? EndValidity { get; set; }
        public bool Estimated { get; set; }
        public string ItemD { get; set; }
        public string ItemE { get; set; }
        public string ItemEFrench { get; set; }
        public string ItemF { get; set; }
        public string ItemG { get; set; }
        public string ItemX { get; set; }
        public string Operator { get; set; }
        public NotamProposalStatusCode Status { get; set; }
        public bool StatusUpdated { get; set; }
        public DateTime? StatusTime { get; set; }
        public bool Delete { get; set; } // a.k.a. EntityDelete
        public string NoteToNof { get; set; }
        public bool ContainFreeText { get; set; }
        public bool ModifiedByNof { get; set; }
        public bool IsCatchAll { get; set; }
        public bool Permanent { get; set; }
        public string Originator { get; set; } // a.k.a. OriginatorName 
        public string OriginatorEmail { get; set; }
        public string OriginatorPhone { get; set; }
        public string Tokens { get; set; }
        public DateTime Received { get; set; }
        public TokenViewModel Token { get; set; }
        //public ICAOText IcaoText { get; set; }
        public string IcaoText { get; set; }
        public List<ProposalAttachmentViewModel> Attachments { get; set; }
        public string NotamId { get; set; }
        public string ParentNotamId { get; set; }
        public bool ReadOnlyMode { get; set; }
        public string RejectionReason { get; set; }
        public bool Grouped { get; set; }
        public bool IsGroupMaster { get; set; }
        public Guid? GroupId { get; set; }
        public List<ProposalViewModel> GroupedProposals { get; set;  }
        public string HelpLink { get; set; }
        public bool Urgent { get; set; }
        public bool CanBeRejected { get; set; }
        public string ModifiedByUsr { get; set; }
        public byte[] RowVersion { get; set; }
        public bool PendingReview { get; set; }

        #region INotam extras
        public NotamType? ReferenceType => ProposalType;
        #endregion

        public void FillProposal(Proposal proposal)
        {
            if (proposal != null)
            {
                //Save the Creator
                var originalOperator = proposal.Operator;

                this.CopyPropertiesTo(proposal);
                //Restore Creator
                proposal.Operator = originalOperator;

                if (ProposalId.HasValue)
                {
                    proposal.Id = ProposalId.Value;
                }
                if (Immediate)
                {
                    proposal.StartActivity = null; 
                }
                if (Permanent)
                {
                    proposal.EndValidity = null;
                }
                // remove seconds
                if (proposal.StartActivity.HasValue)
                {
                    proposal.StartActivity = proposal.StartActivity.Value.RemoveSeconds();
                }
                if (proposal.EndValidity.HasValue)
                {
                    proposal.EndValidity = proposal.EndValidity.Value.RemoveSeconds();
                }
            }
        }

        private void CopyFromProposal(Proposal proposal)
        {
            if (proposal != null)
            {
                proposal.CopyPropertiesTo(this);
                ProposalId = proposal.Id;
            }
        }
    }
}
