﻿using NAVTAM.Helpers.CustomModelBinders;
using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NAVTAM
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(
                typeof(TokenViewModel),
                new TokenViewModelBinder(new NsdManagerResolver(new NsdManagerFactories())));

            

            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
        }
    }
}
