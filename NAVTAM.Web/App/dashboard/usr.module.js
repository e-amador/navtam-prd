"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = exports.initLocalization = exports.LocalizationConfig = void 0;
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ng2_select2_1 = require("ng2-select2");
var ng2_date_time_picker_1 = require("../common/components/ng2-date-time-picker");
//import { StartupService } from './startup.service';
//import { SignalRModule, SignalRConfiguration, ConnectionTransports } from 'ng2-signalr';
var angular_l10n_1 = require("angular-l10n");
var index_1 = require("../common/index");
var usr_component_1 = require("./usr.component");
var usr_index_1 = require("./usr.index");
var usr_routes_1 = require("./usr.routes");
//declare let $: Object;
//export function startupServiceFactory(startupService: StartupService): Function {
//    return () => startupService.load();
//}
//export function createConfig(): SignalRConfiguration {
//    const c = new SignalRConfiguration();
//    c.hubName = 'SignalRHub';
//    //c.qs = { user: 'user.subscribed' };
//    c.url = window.location.origin;
//    c.logging = true;
//    return c;
//}
var LocalizationConfig = /** @class */ (function () {
    function LocalizationConfig(locale, translation) {
        this.locale = locale;
        this.translation = translation;
    }
    LocalizationConfig.prototype.load = function () {
        var _this = this;
        this.locale.addConfiguration()
            .addLanguage('en', 'ltr')
            .addLanguage('fr', 'ltr')
            //.setCookieExpiration(30)
            .defineLanguage(window['app'].cultureInfo)
            .defineCurrency("CAD")
            .defineDefaultLocale('en', 'CA');
        this.locale.init();
        this.translation.addConfiguration()
            .addProvider('./app/resources/locale-');
        var promise = new Promise(function (resolve) {
            _this.translation.translationChanged.subscribe(function () {
                resolve(true);
            });
        });
        this.translation.init();
        return promise;
    };
    LocalizationConfig = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [angular_l10n_1.LocaleService, angular_l10n_1.TranslationService])
    ], LocalizationConfig);
    return LocalizationConfig;
}());
exports.LocalizationConfig = LocalizationConfig;
// AoT compilation requires a reference to an exported function.
function initLocalization(localizationConfig) {
    return function () { return localizationConfig.load(); };
}
exports.initLocalization = initLocalization;
var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                router_1.RouterModule.forRoot(usr_routes_1.userRoutes),
                ng2_select2_1.Select2Module,
                ng2_date_time_picker_1.DateTimePickerModule,
                index_1.Ng2ICheckModule,
                //SignalRModule.forRoot(createConfig),
                angular_l10n_1.LocalizationModule.forRoot() // New instance of TranslationService
            ],
            declarations: [
                usr_component_1.UserComponent,
                //common
                index_1.DatexPipe,
                index_1.DateTimeFormatDirective,
                index_1.ModalTriggerDirective,
                index_1.SimpleModalComponent,
                index_1.ServerPaginationComponent,
                //Nsd Form,
                usr_index_1.NsdFormComponent,
                usr_index_1.NsdButtonBarComponent,
                usr_index_1.NsdGeoRefToolComponent,
                usr_index_1.NsdIcaoComponent,
                usr_index_1.NsdIcaoFormatComponent,
                usr_index_1.NsdSchedulerDailyComponent,
                usr_index_1.NsdSchedulerDateComponent,
                usr_index_1.NsdSchedulerDaysOfWeekComponent,
                usr_index_1.NsdSchedulerFreeTextComponent,
                //UserComponent,
                usr_index_1.UserListComponent,
                usr_index_1.UserCreateComponent,
                usr_index_1.UserEditComponent,
                usr_index_1.UserViewComponent,
                usr_index_1.UserViewHistoryComponent,
                usr_index_1.ProposalFilterComponent,
                usr_index_1.DynamicComponent,
                usr_index_1.ReportFilterComponent,
                usr_index_1.ReportContainerComponent,
                usr_index_1.ReportNotamComponent,
                usr_index_1.ReportStatsComponent,
                usr_index_1.ReportActiveComponent,
                // CatchAll
                usr_index_1.CatchAllDraftComponent,
                usr_index_1.CatchAllCancelComponent,
                // Obstacle
                usr_index_1.ObstComponent,
                usr_index_1.ObstCancelComponent,
                // Obstacle LG US
                usr_index_1.ObstLgUsComponent,
                usr_index_1.ObstLgUsCancelComponent,
                //Multi Obstacle
                usr_index_1.MObstComponent,
                usr_index_1.MObstCancelComponent,
                //Multi Obstacle LG US
                usr_index_1.MObstLgUsComponent,
                usr_index_1.MObstLgUsCancelComponent,
                // Runway closed
                usr_index_1.RwyClsdComponent,
                usr_index_1.RwyClsdCancelComponent,
                // Taxyway closed
                usr_index_1.TwyClsdComponent,
                usr_index_1.TwyClsdCancelComponent,
                // CARS closed
                usr_index_1.CarsClsdComponent,
                usr_index_1.CarsClsdCancelComponent
            ],
            providers: [
                usr_index_1.DataService,
                usr_index_1.NotifyService,
                usr_index_1.MemoryStorageService,
                usr_index_1.LocationService,
                usr_index_1.CatchAllService,
                usr_index_1.ObstacleService,
                usr_index_1.ObstacleLgUsService,
                usr_index_1.MObstService,
                usr_index_1.RwyClsdService,
                usr_index_1.TwyClsdService,
                usr_index_1.CarsClsdService,
                usr_index_1.CategoriesResolver,
                usr_index_1.ProposalModelResolver,
                usr_index_1.ProposalReadOnlyModelResolver,
                usr_index_1.ProposalCreateModelResolver,
                usr_index_1.ProposalCancelModelResolver,
                usr_index_1.ProposalReplaceModelResolver,
                usr_index_1.ProposalCloneModelResolver,
                usr_index_1.ProposalHistoryModelResolver,
                //StartupService,
                LocalizationConfig,
                {
                    provide: core_1.APP_INITIALIZER,
                    useFactory: initLocalization,
                    deps: [LocalizationConfig],
                    multi: true
                },
                //common
                //{
                //    // Provider for APP_INITIALIZER
                //    provide: APP_INITIALIZER,
                //    useFactory: startupServiceFactory,
                //    deps: [StartupService],
                //    multi: true
                //},
                { provide: index_1.TOASTR_TOKEN, useValue: toastr },
                //{ provide: JQUERY_TOKEN, useValue: $ }, 
                { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
                //BearerTokenService,
                index_1.WindowRef,
                { provide: http_1.RequestOptions, useClass: index_1.CustomRequestOptions }
            ],
            bootstrap: [usr_component_1.UserComponent]
        })
    ], UserModule);
    return UserModule;
}());
exports.UserModule = UserModule;
function checkDirtyState(component) {
    var el = $('#modal-unsave-changes');
    var $modal = el.modal();
    return new Promise(function (resolve, reject) {
        if (!component.nsdForm.isSaveButtonDisabled) {
            var alreadyResolved_1 = false;
            $("#btn-yes").click(function () {
                resolve(true);
                alreadyResolved_1 = true;
            });
            $("#btn-no").click(function () {
                resolve(false);
                alreadyResolved_1 = true;
            });
            el.on('hide.bs.modal', function (e) {
                if (!alreadyResolved_1) {
                    resolve(false);
                }
            });
            $modal.modal("show");
        }
        else {
            resolve(true);
        }
    });
}
//# sourceMappingURL=usr.module.js.map