﻿import { Routes } from '@angular/router';
import { UserListComponent } from './user/user-list.component';
import { UserCreateComponent } from './user/user-create.component';
import { UserEditComponent } from './user/user-edit.component';
import { UserViewComponent } from './user/user-view.component';
import { UserViewHistoryComponent } from './user/user-viewhistory.component';
import { ReportContainerComponent } from './shared/report-container.component'

import { CategoriesResolver } from './resolvers/categories.resolver';
import { ProposalModelResolver} from './resolvers/proposal-model.resolver';
import { ProposalReadOnlyModelResolver } from './resolvers/proposal-readonlymodel.resolver';
import { ProposalCreateModelResolver } from './resolvers/proposal-createmodel.resolver';
import { ProposalReplaceModelResolver } from './resolvers/proposal-replacemodel.resolver';
import { ProposalCancelModelResolver } from './resolvers/proposal-cancelmodel.resolver';
import { ProposalCloneModelResolver } from './resolvers/proposal-clonemodel.resolver';

import { ProposalHistoryModelResolver} from './resolvers/proposal-historymodel.resolver';

export const userRoutes: Routes = [
    { path: 'dashboard', component: UserListComponent,
        resolve: { categories: CategoriesResolver }
    },
    {
        path: 'dashboard/create/:catid', component: UserCreateComponent, 
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalCreateModelResolver }
    },
    {
        path: 'dashboard/edit/:id', component: UserEditComponent,
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalModelResolver }
    },
    {
        path: 'dashboard/clone/:id/:history', component: UserEditComponent,
        data: [{ type: 'clone' }],
        canDeactivate: ['canDeactivateCreateEvent'], 
        resolve: { model: ProposalCloneModelResolver }
    },
    {
        path: 'dashboard/view/:id', component: UserViewComponent,
        data: [{ type: 'draft' }],
        resolve: { model: ProposalReadOnlyModelResolver }
    },
    {
        path: 'dashboard/replace/:id', component: UserEditComponent,
        data: [{ type: 'replace' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalReplaceModelResolver }
    },
    {
        path: 'dashboard/reports', component: ReportContainerComponent,
    },
    {
        path: 'dashboard/cancel/:id', component: UserEditComponent,
        data: [{ type: 'cancel' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalCancelModelResolver }
    },
    {
        path: 'dashboard/viewhistory/:id', component: UserViewHistoryComponent,
        data: [{ type: 'draft' }],
        resolve: { model: ProposalHistoryModelResolver }
    },
    {
        path: 'path/*', redirectTo: '/dashboard', pathMatch: 'full'
    }
]