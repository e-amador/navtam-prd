﻿import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { DataService } from './../shared/data.service'

@Injectable()
export class NotamCloneModelResolver implements Resolve<any> {

    constructor(private dataService: DataService) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        var id = route.params['id']; 
        return this.dataService.cloneNotam(id).map(model => model);
    }
}