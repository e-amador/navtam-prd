﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@Injectable()
export class StartupService {

    constructor(private http: Http) { }

    private authData: any;
    // This is the method you want to call at bootstrap 
    // Important: It should return a Promise
    load(): Promise<any> {
        // This is an authorize endpoint in the MVC controllers that does not requires bearer token auth
        const apiUrl = `${window['app'].rootUrl}account/accesstoken?userid=${window['app'].usrId}`;
        return this.http
            .get(apiUrl)
            .map((res: Response) => res.json())
            .toPromise()
            .then((authData: any) => this.setAuthData(authData))
            .catch((err: any) => Promise.resolve());
    }

    setAuthData(authData): any {
        this.authData = authData;
        window['app'].authData = authData;
        return this.authData;
    }
}