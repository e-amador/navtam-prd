﻿import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Localization, LocaleService, TranslationService } from 'angular-l10n';
//import { NotifyService } from './shared/notify.service'

declare var $: any;

@Component({
    selector: 'my-app',
    template: `<router-outlet></router-outlet>`
})
export class UserComponent extends Localization implements OnInit, AfterViewInit {

    constructor(public locale: LocaleService, public translation: TranslationService)
    {
        super(locale, translation);
        this.locale.setDefaultLocale(window['app'].cultureInfo, "CA");
        this.locale.defaultLocaleChanged.subscribe((item: string) => { this.onLanguageCodeChangedDataRecieved(item); });
    }

    ngOnInit() {
       //let initialCategoryId = 2; //CatchAll
       //this.notifyService.emitCategorySubjectEvent(initialCategoryId);
    }   

    public ChangeCulture(language: string, country: string, currency: string) {
        this.locale.setDefaultLocale(language, country);
        this.locale.setCurrentCurrency(currency);
    }

    public ChangeCurrency(currency: string) {
        this.locale.setCurrentCurrency(currency);
    }

    private onLanguageCodeChangedDataRecieved(item: string) {
        console.log('onLanguageCodeChangedDataRecieved App');
        console.log(item);
    }

    ngAfterViewInit() {
        $(document).ready(function () {
            window['templateReady'].call();
        });
        setTimeout(function() {
            $('.loader-overlay').addClass('loaded');
            $('body > section').animate({
                opacity: 1,
            }, 200);
        }, 400);
    } 
}