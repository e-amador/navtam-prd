"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var nof_module_1 = require("./nof.module");
var core_1 = require("@angular/core");
var platform = platform_browser_dynamic_1.platformBrowserDynamic();
core_1.enableProdMode();
platform.bootstrapModule(nof_module_1.NofModule);
//# sourceMappingURL=nof.main.js.map