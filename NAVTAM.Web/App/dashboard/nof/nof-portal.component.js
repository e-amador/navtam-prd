"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofPortalComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var NofPortalComponent = /** @class */ (function () {
    function NofPortalComponent(route, dataService, memStorageService) {
        this.route = route;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.pendingCount = 0;
        this.parkCount = 0;
        this.messageCount = 0;
        this.reviewListCount = 0;
        this.runPendingBellAnimation = false;
        this.runParkBellAnimation = false;
        this.runUnreadBellAnimation = false;
        this.tabsDisabled = false;
        this.intervalHandler = null;
        this.alarms = [
            { id: 0, name: 'Bell Default', ext: 'wav' },
            { id: 1, name: 'Chime Default', ext: 'wav' },
            { id: 2, name: 'Bell Sound', ext: 'mp3' },
            { id: 3, name: 'Police Warning', ext: 'mp3' },
            { id: 4, name: 'Burglary Alarm', ext: 'mp3' },
            { id: 5, name: 'Security Alarm', ext: 'mp3' },
        ];
    }
    NofPortalComponent.prototype.ngOnInit = function () {
        var pendingQueueSoundId = this.memStorageService.get(this.memStorageService.PENDING_QUEUE_SOUND_KEY) || 0;
        var messageQueueSoundId = this.memStorageService.get(this.memStorageService.MESSAGE_QUEUE_SOUND_KEY) || 1;
        this.currentPendingQueueSound = this.alarms[pendingQueueSoundId];
        this.currentMessageQueueSound = this.alarms[messageQueueSoundId];
        var pendingCount = this.memStorageService.get(this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY) || 0;
        if (pendingCount === 0) {
            this.memStorageService.save(this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY, pendingCount);
        }
        var messageCount = this.memStorageService.get(this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY) || 0;
        if (messageCount === 0) {
            this.memStorageService.save(this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY, messageCount);
        }
        this.currentTab = this.memStorageService.get(this.memStorageService.ACTIVE_QUEUE_KEY) || "pending";
        this.checkQueueCounter();
    };
    NofPortalComponent.prototype.ngOnDestroy = function () {
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }
    };
    Object.defineProperty(NofPortalComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    NofPortalComponent.prototype.onTabClick = function (queueName) {
        this.currentTab = queueName;
        this.memStorageService.save(this.memStorageService.ACTIVE_QUEUE_KEY, this.currentTab);
    };
    NofPortalComponent.prototype.onNotify = function (message) {
        switch (message) {
            case "disable-tabs":
                this.tabsDisabled = true;
                break;
            case "enable-tabs":
                this.tabsDisabled = false;
                break;
        }
    };
    NofPortalComponent.prototype.changePendingQueueAlarm = function (event, soundId) {
        event.preventDefault();
        this.currentPendingQueueSound = this.alarms[soundId];
        this.memStorageService.save(this.memStorageService.PENDING_QUEUE_SOUND_KEY, soundId);
    };
    NofPortalComponent.prototype.changeMessagegQueueAlarm = function (event, soundId) {
        event.preventDefault();
        this.currentMessageQueueSound = this.alarms[soundId];
        this.memStorageService.save(this.memStorageService.MESSAGE_QUEUE_SOUND_KEY, soundId);
    };
    NofPortalComponent.prototype.getPendingQueueStats = function () {
        var _this = this;
        try {
            this.dataService.getNofTotalPending()
                .subscribe(function (count) {
                if (count != _this.pendingCount) {
                    _this.pendingCount = count;
                    _this.updatePendingTemplateCountToPage(count);
                }
                _this.memStorageService.save(_this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY, count);
            });
        }
        catch (e) {
        }
    };
    NofPortalComponent.prototype.getMessageCount = function () {
        var _this = this;
        this.dataService.getTotalUnreadMessages()
            .subscribe(function (count) {
            if (_this.messageCount != count) {
                _this.messageCount = count;
                _this.updateUnreadCountToPage(count);
            }
            _this.memStorageService.save(_this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY, count);
        });
    };
    NofPortalComponent.prototype.getParkingQueueStats = function () {
        var _this = this;
        try {
            this.dataService.getNofTotalParked()
                .subscribe(function (count) {
                if (count != _this.parkCount) {
                    _this.parkCount = count;
                    _this.updateParkTemplateCountToPage();
                }
            });
        }
        catch (e) {
        }
    };
    NofPortalComponent.prototype.checkQueueCounter = function () {
        var self = this;
        this.intervalHandler = window.setInterval(function () {
            self.getMessageCount.call(self);
            self.getPendingQueueStats.call(self);
            self.getParkingQueueStats.call(self);
        }, 5000);
    };
    NofPortalComponent.prototype.updatePendingTemplateCountToPage = function (newCount) {
        var self = this;
        var audioElem;
        var prevCount = this.memStorageService.get(this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY);
        var shouldPlaySound = (prevCount === 0 && newCount > 0);
        if (shouldPlaySound) {
            audioElem = document.getElementById('pending-queue-sound');
            audioElem.load();
            audioElem.play();
        }
        self.runPendingBellAnimation = true;
        setTimeout(function () {
            self.runPendingBellAnimation = false;
            if (shouldPlaySound) {
                audioElem.pause();
                audioElem.currentTime = 0;
            }
        }, 10000);
    };
    NofPortalComponent.prototype.updateUnreadCountToPage = function (newCount) {
        var self = this;
        var audioElem;
        var prevCount = this.memStorageService.get(this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY);
        var shouldPlaySound = (prevCount === 0 && newCount > 0);
        if (shouldPlaySound) {
            audioElem = document.getElementById('message-queue-sound');
            audioElem.load();
            audioElem.play();
        }
        self.runUnreadBellAnimation = true;
        setTimeout(function () {
            self.runUnreadBellAnimation = false;
            if (shouldPlaySound) {
                audioElem.pause();
                audioElem.currentTime = 0;
            }
        }, 10000);
    };
    NofPortalComponent.prototype.updateParkTemplateCountToPage = function () {
        var self = this;
        self.runParkBellAnimation = true;
        setTimeout(function () {
            self.runParkBellAnimation = false;
        }, 10000);
    };
    NofPortalComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-portal.component.html'
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService])
    ], NofPortalComponent);
    return NofPortalComponent;
}());
exports.NofPortalComponent = NofPortalComponent;
//# sourceMappingURL=nof-portal.component.js.map