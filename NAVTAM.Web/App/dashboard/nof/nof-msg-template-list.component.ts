﻿import { Component, OnInit, Inject, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from '../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'

import { Subject } from 'rxjs/Subject';

import { LocaleService, TranslationService } from 'angular-l10n';

import { DataService } from '../shared/data.service'

import {
    IMessageTemplate,
    ITableHeaders,
} from '../shared/data.model'

declare var $: any;

@Component({
    templateUrl: '/app/dashboard/nof/nof-msg-template-list.component.html'
})
export class NofMsgTemplateListComponent implements OnInit {

    templates: IMessageTemplate [];
    selectedTemplate: IMessageTemplate;

    tableHeaders: ITableHeaders;
    loadingData: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private router: Router,
        private route: ActivatedRoute,
        private translation: TranslationService) {

        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'Name', direction: 0 },
                    { columnName: 'Addresses', direction: 0 },
                    { columnName: 'Created', direction: 0 }
                ],
            itemsPerPage:
                [
                    { id: '15', text: '15' },
                    { id: '30', text: '30' },
                    { id: '75', text: '75' },
                    { id: '100', text: '100' },
                ]
        };
    }

    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        switch ($event.code) {
            case "ArrowUp"://NAVIGATE TABLE ROW UP
                if (this.selectedTemplate.index > 0) {
                    this.onRowSelected(this.selectedTemplate.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedTemplate.index < this.templates.length - 1) {
                    this.onRowSelected(this.selectedTemplate.index + 1);
                }
                break;
        }
    }

    ngOnInit() {
        this.getMessageTemplates();
    }

    confirmDelete() {
        $('#modal-delete').modal({});
    }

    backToMsgList() {
        this.router.navigate(['/dashboard/']);
    }

    deleteTemplate() {
        if (this.selectedTemplate) {
            this.dataService.deleteMessageTemplate(this.selectedTemplate.id)
                .subscribe(result => {
                    this.getMessageTemplates();
                    //let msg = this.translation.translate('NdsClient.SuccessClientDeleted');
                    let msg = "Template successfully deleted!";
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    //let msg = this.translation.translate('NdsClient.FailureClientDeleted');
                    let msg = "An error has ocurred deleting the template!";
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    onRowSelected(index: number) {
        this.selectMessageTemplate(this.templates[index]);
    }

    onPageChange(page: number) {
        this.getMessageTemplates();
    }

    private selectMessageTemplate(template) {
        if (this.selectedTemplate) {
            this.selectedTemplate.isSelected = false;
        }
        this.selectedTemplate = template
        this.selectedTemplate.isSelected = true;
    }

    private getMessageTemplates() {
        try {
            this.loadingData = true;
            this.dataService.getMessageTemplates()
                .subscribe((templates: any) => {
                    this.processMessageTemplates(templates);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processMessageTemplates(templates: any) {
        for (var iter = 0; iter < templates.length; iter++) {
            templates[iter].index = iter;
        }

        if (templates.length > 0) {
            this.templates = templates;
            let index = 0;
            this.templates[index].isSelected = true;
            this.selectedTemplate = this.templates[index];
            this.loadingData = false;
            this.onRowSelected(index);
        } else {
            this.templates = [];
            this.selectedTemplate = null;
            this.loadingData = false;
        }
    }

    confirmSubmit(): void {
        $('#modal-sendnotams').modal({});
    }
}

