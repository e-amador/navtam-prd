﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { WindowRef } from '../../common/windowRef.service'

import { DataService } from '../shared/data.service'
import { Select2OptionData } from 'ng2-select2';

import { NsdFormComponent } from '../nsds/nsd-form.component'

import {
    IUserMessageTemplate,
    INdsClientFilter,
    IMessageQueue,
    MessageStatus,
    ClientType
} from '../shared/data.model'

class MessageType {
    static Notification = 0
    static Error = 1;
}

@Component({
    templateUrl: '/app/dashboard/nof/nof-msg-read.component.html'
})
export class NofMsgReadComponent implements OnInit {

    //model: any = null;
    model: IMessageQueue = null;
    action: string;

    msgForm: FormGroup; 

    userTemplates: IUserMessageTemplate[] = [];
    maxNumberOfTemplates = 4;

    subscriptions: INdsClientFilter[];
    selectedSubscription: INdsClientFilter = null;
    searchFilter: INdsClientFilter = {
        clientTypeStr: "",
        client: "",
        address: ""
    };
    addresses: string[] = [];
    addressIndexSelected: number = -1;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        this.model = this.activatedRoute.snapshot.data['model'];

        this.configureReactiveForm();
    }

    backToMsgList() {
        this.dataService.unlockMessage(this.model.id)
            .subscribe((response: any) => {
                if (response) {
                    this.router.navigate(['/dashboard/']);
                } else {

                }
            });
    }

    updateParkStatus() {
        this.model.prevStatus = this.model.status;
        this.model.status = this.model.status === MessageStatus.Parked ? MessageStatus.None : MessageStatus.Parked;
        this.dataService.updateMessage(this.model)
            .subscribe(result => {
                if (result.anyError) {
                    this.model.status = this.model.prevStatus;
                    let error = result.status === MessageStatus.AlreadyTaken
                        ? `This message was already taken by ${result.lastOwner}`
                        : `The message was changed by ${result.lastOwner} to ${result.status}`

                    this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                    return;
                }


                let message = this.model.status === MessageStatus.Parked
                    ? this.translation.translate('MessageQueue.SuccessMessageParked')
                    : this.translation.translate('MessageQueue.SuccessMessageUnparked');

                this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(['/dashboard/']);
            }, error => {
                this.toastr.error(this.translation.translate('MessageQueue.FailureMessageParked') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    updateImportantStatus() {
        this.model.prevStatus = this.model.status;
        this.model.status = this.model.status === MessageStatus.Important ? MessageStatus.None : MessageStatus.Important;
        this.dataService.updateMessage(this.model)
            .subscribe(result => {
                if (result.anyError) {
                    this.model.status = this.model.prevStatus;
                    let error = result.status === MessageStatus.AlreadyTaken
                        ? `This message was already taken by ${result.lastOwner}`
                        : `The message was changed by ${result.lastOwner} to ${result.status}`

                    this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                    return;
                }


                let message = this.model.status === MessageStatus.Important
                    ? this.translation.translate('MessageQueue.SuccessMessageImportant')
                    : this.translation.translate('MessageQueue.SuccessMessageUnimportant');

                this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(['/dashboard/']);
            }, error => {
                this.toastr.error(this.translation.translate('MessageQueue.FailureMessageImportant') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    updateDeleteStatus() {
        this.model.prevStatus = this.model.status;
        this.model.status = this.model.status === MessageStatus.Deleted ? MessageStatus.None : MessageStatus.Deleted;
        this.dataService.updateMessage(this.model)
            .subscribe(result => {
                if (result.anyError) {
                    this.model.status = this.model.prevStatus;
                    let error = result.status === MessageStatus.AlreadyTaken
                        ? `This message was already taken by ${result.lastOwner}`
                        : `The message was changed by ${result.lastOwner} to ${result.status}`

                    this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                    return;
                }


                let message = this.model.status === MessageStatus.Deleted
                    ? this.translation.translate('MessageQueue.SuccessMessageDeleted')
                    : this.translation.translate('MessageQueue.SuccessMessageUndeleted');

                this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(['/dashboard/']);
            }, error => {
                this.toastr.error(this.translation.translate('MessageQueue.FailureMessageDeleted') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    toClientTypeStr(clientType) {
        switch (clientType) {
            case ClientType.AFTN: return 'AFTN';
            case ClientType.SWIM: return 'SWIM';
            case ClientType.REST: return 'REST';
            default: return '';
        }
    }

    private configureReactiveForm() {
        this.msgForm = this.fb.group({
            originator: [{ value: this.model.clientName, disabled: true }],
            recipients: [{ value: this.model.recipients, disabled: true }],
            messageBody: [{ value: this.model.body, disabled: true }],
            priorityCode: [{ value: this.model.priorityCode, disabled: true }],
        });
    }
}

