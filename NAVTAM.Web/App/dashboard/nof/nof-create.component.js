"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofCreateComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var nsd_form_component_1 = require("../nsds/nsd-form.component");
var NofCreateComponent = /** @class */ (function () {
    function NofCreateComponent(dataService, activatedRoute, router) {
        this.dataService = dataService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.model = null;
    }
    Object.defineProperty(NofCreateComponent.prototype, "form", {
        set: function (v) {
            this.nsdForm = v;
        },
        enumerable: false,
        configurable: true
    });
    NofCreateComponent.prototype.ngOnInit = function () {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    };
    NofCreateComponent.prototype.ngOnDestroy = function () {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    };
    Object.defineProperty(NofCreateComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        core_1.ViewChild(nsd_form_component_1.NsdFormComponent),
        __metadata("design:type", nsd_form_component_1.NsdFormComponent),
        __metadata("design:paramtypes", [nsd_form_component_1.NsdFormComponent])
    ], NofCreateComponent.prototype, "form", null);
    NofCreateComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-create.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], NofCreateComponent);
    return NofCreateComponent;
}());
exports.NofCreateComponent = NofCreateComponent;
//# sourceMappingURL=nof-create.component.js.map