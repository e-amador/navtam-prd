﻿import { Component, OnInit, AfterViewInit, OnDestroy, Inject, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core';
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router,  } from '@angular/router';

//import { Subject } from 'rxjs/Subject';

import { DataService } from '../shared/data.service';
import { LocationService } from '../shared/location.service';
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service';
import { WindowRef } from '../../common/windowRef.service';
import { NofNotamFilterComponent } from './nof-notam-filter.component';

import {
    INotam,
    IQueryOptions,
    IPaging,
    ITableHeaders,
    ProposalType,
    ISdoSubject,
    IProposal,
    ProposalStatus,
    Icao
} from '../shared/data.model';

declare var $: any;

@Component({
    selector: 'nof-notam',
    styleUrls: ['app/dashboard/nof/nof-notam.component.css'],
    templateUrl: '/app/dashboard/nof/nof-notam.component.html'
})
export class NofNotamComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(NofNotamFilterComponent) filter: NofNotamFilterComponent;

    notams: INotam[];
    notamTrakingList: INotam[];
    queryOptions: IQueryOptions;
    selectedNotam: INotam;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterPaging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterTotalPages: number = 0;
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;

    frsPrevKeyCodeEvent: any;
    sndPrevKeyCodeEvent: any;
    sdoSubject: ISdoSubject = null;

    leafletmap: any = null;
    mapHealthy: boolean = false;
    isLeavingForm: boolean = false;

    isFilterApplied: boolean = false;
    filterPannelOpen: boolean = false;
    icaoFormat: string = "";

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private locationService: LocationService,
        private route: ActivatedRoute,
        public translation: TranslationService) {

        this.tableHeaders = {
            sorting:
            [
                { columnName: 'NotamId', direction: 0 },
                { columnName: 'Type', direction: 0 },
                { columnName: 'ReferredNotamId', direction: 0 },
                { columnName: 'SiteID', direction: 0 },
                { columnName: 'Series', direction: 0 },
                { columnName: 'ItemA', direction: 0 },
                { columnName: 'Code23', direction: 0 },
                { columnName: 'Code45', direction: 0 },
                { columnName: 'Permanent', direction: 0 },
                { columnName: 'Estimated', direction: 0 },
                //{ columnName: 'Operator', direction: 0 },
                { columnName: 'Originator', direction: 0 },
                { columnName: 'ModifiedByUsr', direction: 0 },
                { columnName: 'StartActivity', direction: 0 },
                { columnName: 'EndValidity', direction: 1 },
                { columnName: 'Published', direction: 0 },
            ],
            itemsPerPage:
            [
                { id: '10', text: '10' }, 
                { id: '25', text: '25' }, 
                { id: '50', text: '50' }, 
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[13].columnName //Default EndValidity
        }
        this.clearIcao();
    }

    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        switch ($event.code) {
            case "ArrowUp"://NAVIGATE TABLE ROW UP
                if (this.selectedNotam.index > 0) {
                    this.onRowSelected(this.selectedNotam.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedNotam.index < this.notams.length - 1) {
                    this.onRowSelected(this.selectedNotam.index + 1);
                }
                break;
        }
    }

    ngOnInit() {
        //Set latest query options
        var faKey = this.memStorageService.get(this.memStorageService.FILTER_APPLIED_KEY);
        if (faKey) this.isFilterApplied = faKey;
        //this.isFilterApplied = this.memStorageService.get(this.memStorageService.FILTER_APPLIED_KEY);
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        } else {
            let queryOptions = this.memStorageService.get(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY);
            if (queryOptions !== null) {
                this.queryOptions = queryOptions;
            }
        }

        if (this.isFilterApplied) {
            this.filter.runFilter(false);
        } else {
            this.getNotams(this.queryOptions.page);
        }        
    }

    ngAfterViewInit() {
        this.filter.parent = this;
        this.mapHealthCheck();
        $(document).ready(function () {
            $(window).scrollTop(0);
        })
    }

    runCloneAction(): void {
        if (this.selectedNotam && this.selectedNotam.notamType !== ProposalType.Cancel) {
            this.isLeavingForm = true;
            //This we don't pass this.isFilterApplied, because with a NOTAM, always the Clone have to go to Proposal table
            this.router.navigate(["/dashboard/notamclone", this.selectedNotam.id]);
        }
    }

    runReviewAction(): void {
        if (this.selectedNotam) {
            this.isLeavingForm = true;
            this.router.navigate(["/dashboard/notamview", this.selectedNotam.id]);
        }
    }

    ngOnDestroy() {
        this.disposeMap();
    }

    onFilterChanged(event: any) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        } else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processNotams(event.data);
            } else {
                this.queryOptions = this.memStorageService.get(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY);
                this.getNotams(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }

        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1);
        } else {
            this.getNotams(1);
        }
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    rowColor(notam) {
        if (notam.isExpired) {
            return "bg-expired"; // here we are actually returning only the expired and replaced notams
        }

        switch (notam.notamType) {
            case ProposalType.New: return '';
            case ProposalType.Replace: return 'bg-submitted';
            case ProposalType.Cancel: return 'bg-widthdrawn';
            default: return '';
        }
    }

    itemsPerPageChanged(e: any): void {
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, e.value);
        }
        else {
            this.queryOptions.pageSize = e.value;
            this.getNotams(1);
        }
    }

    onRowSelected(index: number) {
        this.selectNotam(this.notams[index]);
        this.updateMap(this.notams[index], false);
    }

    onPageChange(page: number) {
        this.getNotams(page);
    }

    replaceClick(): void {
        this.isLeavingForm = true;
        this.dataService.verifyObsoleteNotam(this.selectedNotam.id)
            .subscribe((response: boolean) => {
                this.selectedNotam.isObsolete = response;
                if (!this.selectedNotam.isObsolete) {
                    //Note: We are using "ItemX = ReplaceNotam=1" to determine when a NOF operator is replacing/cancelling a NOTAM directly from the NOTAM panel
                   this.dataService.updateProposalItemX(this.selectedNotam.proposalId, "ReplaceNotam=1")
                        .subscribe(resp => {
                            this.router.navigate(["/dashboard/replace", this.selectedNotam.proposalId, 'notam']);
                        }, error => {
                            this.toastr.error(error.message + " NotamID: " + this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
                            this.isLeavingForm = true;
                        });
                }
            },
            (error: any) => {
                this.toastr.error(error.message + " NotamID: " + this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
                this.isLeavingForm = false;
            });
    }

    cancelClick(): void {
        this.isLeavingForm = true;
        this.dataService.verifyObsoleteNotam(this.selectedNotam.id)
            .subscribe((response: boolean) => {
                if (!this.selectedNotam.isObsolete) {
                    //Note: We are using "ItemX = ReplaceNotam=1" to determine when a NOF operator is replacing/cancelling a NOTAM directly from the NOTAM panel
                    this.dataService.updateProposalItemX(this.selectedNotam.proposalId, "ReplaceNotam=1")
                        .subscribe(resp => {
                            this.router.navigate(["/dashboard/cancel", this.selectedNotam.proposalId, 'notam']);
                        }, error => {
                            this.toastr.error(error.message + " NotamID: " + this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
                            this.isLeavingForm = true;
                        });
                }
            },
            (error: any) => {
                this.toastr.error(error.message + " NotamID: " + this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
                this.isLeavingForm = true;
            });
    }

    isCloneButtonDisabled() {
        if (!this.selectedNotam) {
            return true;
        }

        if (this.selectedNotam.notamType === ProposalType.Cancel) {
            return true;
        }
        if (this.selectedNotam.seriesChecklist) {
            return true;
        }

        if (!this.selectedNotam) {
            return true;
        }

        return false;
    }

    isReviewButtonDissabled() {
        if (!this.selectedNotam) {
            return true;
        }

        if (this.selectedNotam.seriesChecklist) {
            return true;
        }

        return false;
    }

    isReplaceButtonDisabled() {
        if (!this.selectedNotam) {
            return true;
        }

        if (this.selectedNotam.seriesChecklist) {
            return true;
        }

        if (this.selectedNotam.notamType === ProposalType.Cancel) {
            return true;
        }

        if (this.selectedNotam.isObsolete) return true;

        return false;
    }

    isCancelButtonDisabled() {
        if (!this.selectedNotam) {
            return true;
        }

        if (this.selectedNotam.seriesChecklist) {
            return true;
        }

        if (this.selectedNotam.notamType === ProposalType.Cancel) {
            return true;
        }

        if (this.selectedNotam.isObsolete) return true;

        return false;
    }

    isRunReportButtonEnabled() {
        return true;
    }

    isHistoryButtonDisabled() {
        if (!this.selectedNotam) {
            return true;
        }

        if (this.selectedNotam.seriesChecklist) {
            return true;
        }
    }

    getOperatorString(notam: INotam): string {
        if (notam.modifiedByUsr === null || notam.modifiedByUsr.length === 0) return notam.operator;
        else return notam.modifiedByUsr;
    }

    getOriginatorString(originator: string): string {
        if (originator.length > 10) {
            return originator.substring(0, 10) + '..';
        }

        return originator;
    }

    getApproverString(notam: any): string {
        if (notam.isProposal) {
            if (notam.status === ProposalStatus.Disseminated ||
                notam.status === ProposalStatus.DisseminatedModified ||
                notam.status === ProposalStatus.Rejected) return notam.modifiedByUsr;
            else return '';
        } else
        {
            return notam.modifiedByUsr;
        }
    }

    onProposalHistory(rowIndex, parentIndex, proposalId) {
        if (this.notamTrakingList[rowIndex]['historyOpen']) {
            this.notamTrakingList[rowIndex]['historyOpen'] = false;
            this.notamTrakingList = this.notamTrakingList.filter(function (item: any) {
                return !(item.isProposal && item.rowIndex === parentIndex);
            });     
        } else {
            this.dataService.getProposalHistory(proposalId)
                .subscribe((proposals: any[]) => {
                    if (proposals.length > 0) {
                        for (let i = 0; i < proposals.length; i++) {
                            let notamId = proposals[i].notamId;
                            if (notamId) {
                                let pos = notamId.indexOf('/');
                                if (pos > 0) {
                                    let proposalNumberStr = notamId.substring(pos - 4, 5);
                                    let proposalNumber = parseInt(proposalNumberStr);

                                    let notamNumberStr = this.notamTrakingList[rowIndex].notamId.substring(pos - 4, 5);;
                                    let notamNumber = parseInt(notamNumberStr);

                                    if (notamNumber >= proposalNumber ) {
                                        proposals[i].isProposal = true;
                                        proposals[i].rowIndex = parentIndex;
                                        proposals[i].published = proposals[i].received;

                                        this.notamTrakingList[rowIndex]['historyOpen'] = true;
                                        this.notamTrakingList.splice(rowIndex + 1, 0, proposals[i]);
                                    }
                                }
                            } else {
                                proposals[i].isProposal = true;
                                proposals[i].rowIndex = parentIndex;
                                proposals[i].published = proposals[i].received;
                                this.notamTrakingList[parentIndex]['historyOpen'] = true;
                                this.notamTrakingList.splice(rowIndex + 1, 0, proposals[i]);
                            }
                        }
                    } else {
                    }
                });
        }
    }

    showHistory() {
        $('#notam-tracking-list').modal({});
        this.dataService.getNofNotamTrackingList(this.selectedNotam.proposalId)
            .subscribe((result: any) => {
                if (result.length > 0) {
                    let selectedIndex = result.findIndex(x => x.id === this.selectedNotam.id);
                    if (selectedIndex < 0) {
                        selectedIndex = 0;
                    }
                    for (var iter = 0; iter < result.length; iter++) {
                        result[iter].index = iter;
                        result[iter].historyOpen = false;
                        result[iter].typeStr = this.toNotamTypeStr(result[iter].notamType);
                    }
                    this.notamTrakingList = result;
                    this.notamTrakingList[selectedIndex].isSelected = true;
                } else {
                    this.notamTrakingList = [];
                }
            });
    }

    private selectNotam(notam) {
        if (this.selectedNotam) {
            this.notams[this.selectedNotam.index].isSelected = false;
        }

        notam.isSelected = true;
        this.selectedNotam = JSON.parse(JSON.stringify(notam)); //notam
        this.memStorageService.save(this.memStorageService.NOTAM_ID_KEY, this.selectedNotam.id);

        this.dataService.verifyObsoleteNotam(this.selectedNotam.id)
            .subscribe((response: boolean) => {
                notam.isObsolete = response;
                this.selectedNotam.isObsolete = notam.isObsolete;
            },
            (error: any) => {
                this.toastr.error(error.message + " NotamID: " + this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
            });

        this.getIcaoFormat(notam);
    }

    @debounceFunc(500)
    private getIcaoFormat(notam) {
        this.startIcaoRequests();
        this.dataService.getNotamIcaoFormat(notam.id)
            .subscribe((icao: any) => {
                this.icaoFormat = icao;
                this.endIcaoRequests();
            },
            error => {
                this.endIcaoRequests();
            });
    }

    copyIcaoText() {
        this.copyToClipBoard(this.icaoFormat);
    }

    private copyToClipBoard(text) {
        const el = document.createElement('textarea');
        el.value = text;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    private getNotams(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runFilter(false, page, this.queryOptions.pageSize);
            }
            else {
                this.memStorageService.save(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY, this.queryOptions);
                this.dataService.getNofNotams(this.queryOptions)
                    .subscribe((notams: any) => {
                        this.processNotams(notams);
                    });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processNotams(notams: any) {
        if (this.isFilterApplied) {
            this.filterPaging = notams.paging;
            this.filterTotalPages = notams.paging.totalPages;
        } 

        this.paging = notams.paging;
        this.totalPages = notams.paging.totalPages;

        for (var iter = 0; iter < notams.data.length; iter++) {
            notams.data[iter].index = iter;
            notams.data[iter].typeStr = this.toNotamTypeStr(notams.data[iter].notamType);
        }

        const lastNotamSelected = this.memStorageService.get(this.memStorageService.NOTAM_ID_KEY);
        if (notams.data.length > 0) {
            this.notams = notams.data;
            let index = 0;
            if (lastNotamSelected) {
                index = this.notams.findIndex(x => x.id === lastNotamSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.notams[index].isSelected = true;
            this.selectedNotam = this.notams[index];
            this.loadingData = false;
            this.onRowSelected(0);
        } else {
            this.notams = [];
            this.selectedNotam = null;
            this.loadingData = false;
        }
    }

    toNotamTypeStr(notamType) {
        switch (notamType) {
            case ProposalType.New: return 'N';
            case ProposalType.Replace: return 'R';
            case ProposalType.Cancel: return 'C';
            default: return '';
        }
    }

    private clearIcao() {
        this.icaoFormat = "";
    }

    private startIcaoRequests(): void {
        $("#icao-bulb").addClass("animated");
        $("table.icao").animate({
            opacity: 0.75
        }, 250, function () {
        });
    }

    private endIcaoRequests(): void {
        $("#icao-bulb").removeClass("animated");
        $("table.icao").animate({
            opacity: 1
        }, 250, function () {
            // Animation complete.
        });
    }

    private disposeMap() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    }

    private updateMap(notam: INotam, acceptSubjectLocation: boolean) {
        if (this.mapHealthy) {
            this.leafletmap.clearFeaturesOnLayers();
            this.leafletmap.addDoa(notam.effectAreaGeoJson, 'DOA');
        }       
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }

    private initMap() {
        const winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;

        if (this.leafletmap) {
            this.leafletmap.initMap()
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.mapHealthy = true;
                this.initMap();
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        });
    }
}

export function debounceFunc(delay: number = 300): MethodDecorator {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        let timeout = null

        const original = descriptor.value;

        descriptor.value = function (...args) {
            clearTimeout(timeout);
            timeout = setTimeout(() => original.apply(this, args), delay);
        };

        return descriptor;
    };
}