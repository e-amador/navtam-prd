"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofMsgTemplateCreateComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var NofMsgTemplateCreateComponent = /** @class */ (function () {
    function NofMsgTemplateCreateComponent(toastr, fb, dataService, router, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
    }
    NofMsgTemplateCreateComponent.prototype.ngOnInit = function () {
        this.configureReactiveForm();
    };
    NofMsgTemplateCreateComponent.prototype.validateName = function () {
        var ctrl = this.form.get("name");
        return ctrl.valid || this.form.pristine;
    };
    NofMsgTemplateCreateComponent.prototype.validateAddress = function () {
        var ctrl = this.form.get("address");
        return ctrl.valid || this.form.pristine;
    };
    NofMsgTemplateCreateComponent.prototype.validateBody = function () {
        var ctrl = this.form.get("body");
        return ctrl.valid || this.form.pristine;
    };
    NofMsgTemplateCreateComponent.prototype.backToMessageTemplates = function () {
        this.router.navigate(['/dashboard/messages/templates']);
    };
    NofMsgTemplateCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        var template = this.prepareTemplate();
        this.dataService.saveMessageTemplate(template)
            .subscribe(function (data) {
            //let msg = this.translation.translate('NdsClient.SuccessClientCreated');
            var msg = "Message template successfully saved";
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.backToMessageTemplates();
        }, function (error) {
            //let msg = this.translation.translate('NdsClient.FailureClientCreated');
            var msg = "An error has ocurred: ";
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgTemplateCreateComponent.prototype.prepareTemplate = function () {
        var name = this.form.get('name').value;
        var address = this.form.get('address').value.toUpperCase();
        var body = this.form.get('body').value;
        var template = {
            name: name,
            addresses: address,
            body: body
        };
        return template;
    };
    NofMsgTemplateCreateComponent.prototype.configureReactiveForm = function () {
        this.form = this.fb.group({
            name: [{ value: '', disabled: false }, [forms_1.Validators.required, forms_1.Validators.minLength(3), forms_1.Validators.maxLength(100)]],
            address: [{ value: '', disabled: false }, [forms_1.Validators.required, forms_1.Validators.pattern('^[a-zA-Z]{8}(?:;[a-zA-Z]{8})*$')]],
            body: [{ value: '', disabled: false }, [forms_1.Validators.required]],
        });
    };
    NofMsgTemplateCreateComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-msg-template-create.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            router_1.Router,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], NofMsgTemplateCreateComponent);
    return NofMsgTemplateCreateComponent;
}());
exports.NofMsgTemplateCreateComponent = NofMsgTemplateCreateComponent;
//# sourceMappingURL=nof-msg-template-create.component.js.map