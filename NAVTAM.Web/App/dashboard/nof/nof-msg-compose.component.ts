﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { WindowRef } from '../../common/windowRef.service'

import { DataService } from '../shared/data.service'
import { Select2OptionData } from 'ng2-select2';

import { NsdFormComponent } from '../nsds/nsd-form.component'

import {
    IUserMessageTemplate,
    INdsClientFilter,
    IMessageQueue,
    MessageStatus,
    ClientType
} from '../shared/data.model'

class MessageType {
    static Notification = 0
    static Error = 1;
}

@Component({
    templateUrl: '/app/dashboard/nof/nof-msg-compose.component.html'
})
export class NofMsgComposeComponent implements OnInit {

    model: any = null;
    action: string;

    msgForm: FormGroup; 

    isReadOnly: boolean;

    prioriTyCodeOptions: Select2Options;
    selectedPriorityCodeId: string = "-1";
    priorityCodes: Array<Select2OptionData>;

    templateOptions: Select2Options;
    selectedTemplateId: string = "-1";
    templates: Array<Select2OptionData>;

    userTemplates: IUserMessageTemplate[] = [];
    maxNumberOfTemplates = 4;

    subscriptions: INdsClientFilter[];
    selectedSubscription: INdsClientFilter = null;
    searchFilter: INdsClientFilter = {
        clientTypeStr: "",
        client: "",
        address: ""
    };
    addresses: string[] = [];
    addressIndexSelected: number = -1;
    replyToAddress: string = "";
    replyToId: string = "";

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        this.prioriTyCodeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('')
            },
            width: "100%"
        }
        this.setPriorityCodes();

        this.templateOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('MessageQueue.SelectTemplate')
            },
            width: "100%"
        }

        let model = this.activatedRoute.snapshot.data['model'];
        if (model) {
            this.replyToAddress = model.clientAddress;
            this.replyToId = model.id;
        }

        this.configureReactiveForm();

        this.loadTemplates();
    }

    onPriorityCodeChanged(data: { value: string }) {
        this.selectedPriorityCodeId = data.value;
    }

    onTemplateChanged(data: { value: string }) {
        if (data.value === "-1") {
            return;
        }

        this.selectedTemplateId = data.value;
        this.dataService.getMessageTemplate(this.selectedTemplateId)
            .subscribe(template => {
                const bodyCtrl = this.msgForm.get('messageBody');
                const recipientsCtrl = this.msgForm.get('recipients');

                bodyCtrl.setValue(template.body);
                recipientsCtrl.setValue(template.addresses);
                bodyCtrl.markAsDirty();
                recipientsCtrl.markAsDirty();
            }, error => {
                let msg = this.translation.translate('MessageQueue.FailureTemplateLoad');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    backToMsgList() {
        if (this.replyToId) {
            this.dataService.unlockMessage(this.replyToId)
                .subscribe(result => {
                    this.router.navigate(['/dashboard/']);
                });
        } else {
            this.router.navigate(['/dashboard/']);
        }
    }

    isOriginatorRequired() {
        const fc = this.msgForm.get('originator');
        return !this.msgForm.pristine && fc.errors && fc.errors.required; 
    }

    isOriginatorFormatInvalid() {
        const fc = this.msgForm.get('originator');
        return !this.msgForm.pristine && fc.errors && !fc.errors.required && fc.errors.invalidOriginatorFormat;
    }

    validateRecipients() {
        const fc = this.msgForm.get('recipients');
        return fc.value || this.msgForm.pristine;
    }

    validateMessageBody() {
        const fc = this.msgForm.get('messageBody');
        return fc.value || this.msgForm.pristine;
    }

    openSearchDialog() {
        this.subscriptions = [];
        this.dataService.getNdsClientAddresses()
            .subscribe(subscriptions => {

                this.selectedSubscription = null;
                for (var iter = 0; iter < subscriptions.length; iter++) {
                    let subscription = subscriptions[iter];
                    subscription.index = iter;
                    subscription.clientTypeStr = this.toClientTypeStr(subscription.clientType);
                    subscription.isSelected = false;

                    this.subscriptions.push(subscription);
                }

                if (subscriptions.length > 0) {
                    this.selectedSubscription = this.subscriptions[0];
                    this.selectedSubscription.isSelected = true;
                };

            }, error => {
                //TODO
            });

        let elm = <any>$('#modal-search');
        const self = this;
        elm.modal({});
        elm.on('hidden.bs.modal', function (e) {
            self.closeSearchDialog();
        })
    }

    selectSubscription(client: INdsClientFilter) {
        this.selectedSubscription.isSelected = false;
        this.selectedSubscription = client;
        this.selectedSubscription.isSelected = true;
    }

    addAddress() {
        if (this.addresses.length < 14) {
            this.addresses.push(this.selectedSubscription.address);
        }
    }

    selectAddress(index: number) {
        this.addressIndexSelected = this.addressIndexSelected !== index ? index : -1;
    }

    deleteAddress(index: number) {
        if (this.addressIndexSelected >= 0) {
            this.addresses.splice(this.addressIndexSelected, 1);
        }
    }

    appendAddressToRecipientList() {
        const ctrl = this.msgForm.get('recipients');
        let len = ctrl.value.length;

        let recipients = ctrl.value.split(";");
        let addressesCount = 0;
        for (var i = 0; i < recipients.length; i++) {
            if (recipients[i] !== "") {
                addressesCount++;
            }
        }

        let addresses = len === 0 ? "" : (ctrl.value[len - 1] === ";" ? ctrl.value : (ctrl.value + ";"));
        for (let i = 0; i < this.addresses.length; i++) {
            if (addressesCount + i < 21) {
                if (addresses.length > 0 && addresses[addresses.length - 1] !== ";") {
                    addresses += ";"
                }
                addresses += this.addresses[i]
            }
        }

        ctrl.setValue(addresses );
        ctrl.markAsDirty();
    }

    closeSearchDialog() {
        this.addresses = [];
        this.addressIndexSelected = -1;
    }

    toClientTypeStr(clientType) {
        switch (clientType) {
            case ClientType.AFTN: return 'AFTN';
            case ClientType.SWIM: return 'SWIM';
            case ClientType.REST: return 'REST';
            default: return '';
        }
    }

    onSubmit() {
        let queueMessage = this.prepareQueueMessagePayload();
        this.dataService.sendQueueMessage(queueMessage)
            .subscribe(data => {
                let msg = this.translation.translate('MessageQueue.SuccessMessageSent');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(['/dashboard']);
            }, error => {
                let msg = this.translation.translate('MessageQueue.FailureMessageSent');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private prepareQueueMessagePayload(): IMessageQueue {
        let now = new Date()
        let messageQueue: IMessageQueue = {
            messageType: MessageType.Notification,
            clientType: ClientType.AFTN,
            priorityCode: this.selectedPriorityCodeId,
            inbound: false,
            recipients: this.msgForm.get('recipients').value,
            body: this.msgForm.get('messageBody').value,
            clientName: this.winRef.appConfig.usrName,
            clientAddress: this.msgForm.get('originator').value,
            created: now,
            read: true,
            locked: false,
            replyToId: this.replyToId,
            status: MessageStatus.None
        }

        var lastChar = messageQueue.recipients.slice(-1);
        if (lastChar == ';') {
            messageQueue.recipients = messageQueue.recipients.slice(0, -1);
        }
        return messageQueue;
    }

    private configureReactiveForm() {
        this.msgForm = this.fb.group({
            originator: [{ value: this.winRef.appConfig.nofAddress, disabled: false }, [Validators.required, originatorValidator]],
            recipients: [{ value: this.replyToAddress, disabled: false }, [Validators.required, recipientsValidator]],
            messageBody: [{ value: "", disabled: false }, [Validators.required]],
        });
    }

    private setPriorityCodes(): void {
        this.priorityCodes = [
            { id: "GG", text: "GG" },
            { id: "SS", text: "SS" },
            { id: "DD", text: "DD" },
            { id: "FF", text: "FF" },
            { id: "KK", text: "KK" }];

        this.selectedPriorityCodeId = this.priorityCodes[0].id;
    }

    private loadTemplates(): void {
        this.dataService.getMessageTemplateNames()
            .subscribe((templates: any) => {
                this.templates = [{ id: "-1", text: "" }];
                for (let i = 0; i < templates.length; i++) {
                    let r = templates[i];
                    this.templates.push(<Select2OptionData>{ id: r.id, text: r.name });
                }
                this.selectedTemplateId = "-1";
            }, error => {
                //TODO
            });
    }

}

function recipientsValidator(c: AbstractControl): { [key: string]: boolean } | null {

    let addresses = c.value.split(";")

    let reg = /^[a-z]+$/i;

    var isValid = true;

    for (let i = 0; i < addresses.length; i++) {
        let address = addresses[i];

        if (address.length === 0) {
            continue;
        }

        if (address.length !== 8) {
            isValid = false;
            break;
        }

        if (!reg.test(address)) {
            isValid = false;
            break;
        }
    }

    return isValid ? null : { 'invalidRecipientFormat': true };
}

function originatorValidator(c: AbstractControl): { [key: string]: boolean } | null {

    let address = c.value || "";

    let reg = /^[a-z]+$/i;

    var isValid = address.length === 8 && reg.test(address);

    return isValid ? null : { 'invalidOriginatorFormat': true };
}

