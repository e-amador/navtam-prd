"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofQueueComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_model_2 = require("../shared/data.model");
var syncFrecuency = 10; //seconds
var NofQueueComponent = /** @class */ (function () {
    function NofQueueComponent(toastr, dataService, memStorageService, router, winRef, translation, route) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.translation = translation;
        this.route = route;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.parentCategories = [];
        this.childCategories = [];
        this.lastSyncTime = null;
        this.loadingData = false;
        this.handler = null;
        this.intervalHandler = null;
        this.expiringCount = 0;
        this.runExpiringBellAnimation = false;
        this.expiringApplied = false;
        this.isLeavingForm = false;
        this.alarms = [
            { id: 0, name: 'Bell Default', ext: 'wav' },
            { id: 1, name: 'Chime Default', ext: 'wav' },
            { id: 2, name: 'Bell Sound', ext: 'mp3' },
            { id: 3, name: 'Police Warning', ext: 'mp3' },
            { id: 4, name: 'Burglary Alarm', ext: 'mp3' },
            { id: 5, name: 'Security Alarm', ext: 'mp3' },
        ];
        this.tableHeaders = {
            sorting: [
                { columnName: 'NotamId', direction: 0 },
                { columnName: 'CategoryId', direction: 0 },
                { columnName: 'ItemA', direction: 0 },
                { columnName: 'SiteID', direction: 0 },
                { columnName: 'ItemE', direction: 0 },
                { columnName: 'StartActivity', direction: 0 },
                { columnName: 'EndValidity', direction: 0 },
                { columnName: 'Estimated', direction: 0 },
                { columnName: 'Received', direction: 1 },
                { columnName: 'Operator', direction: 0 },
                { columnName: 'Originator', direction: 0 },
                { columnName: 'ModifiedByUsr', direction: 0 },
                { columnName: 'Status', direction: 0 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '25', text: '25' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' }
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[8].columnName //Default Received Ascending
        };
    }
    NofQueueComponent.prototype.ngOnInit = function () {
        //If we want to change the default, we need to work here
        this.currentEstimatedQueueSound = this.alarms[0];
        this.categoriesData = this.route.snapshot.data['categories'];
        this.addAllToComboFilter(this.parentCategories);
        for (var i = 0; i < this.categoriesData.length; i++) {
            var category = this.categoriesData[i];
            this.addItemToComboFilter(this.parentCategories, category);
        }
        this.categoryStartValue = this.categoriesData[0].id;
        this.currentCategoryId = +this.categoryStartValue;
        //Set latest combo box status 
        var catId = this.memStorageService.get(this.memStorageService.CATEGORY_ID_KEY);
        var subCatId = this.memStorageService.get(this.memStorageService.SUB_CATEGORY_ID_KEY);
        this.categoryStartValue = catId ? catId : this.parentCategories[0].id;
        this.currentCategoryId = +this.categoryStartValue;
        if (subCatId) {
            this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
            this.currentSubCategoryId = subCatId;
            this.subCategoryStartValue = subCatId;
        }
        //Set latest query options
        this.queryStoreName = this.queueName + "QUERY_OPTIONS_KEY";
        var queryOptions = this.memStorageService.get(this.queryStoreName);
        if (queryOptions) {
            this.queryOptions = queryOptions;
        }
        this.getProposalsByCategory(this.queryOptions.page);
        if (this.queueName === data_model_1.NofQueues.Pending) {
            this.getParkingQueueStats();
        }
        else {
            this.getPendingQueueStats();
        }
        this.checkProposalStatus();
        this.getExpiringQueueStats();
        this.checkExpireCounter();
    };
    NofQueueComponent.prototype.ngAfterViewInit = function () {
        $(document).ready(function () {
            $(window).scrollTop(0);
        });
    };
    NofQueueComponent.prototype.ngOnDestroy = function () {
        if (this.handler) {
            clearInterval(this.handler);
        }
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }
    };
    NofQueueComponent.prototype.ngOnChanges = function () {
        switch (this.queueName) {
            case data_model_1.NofQueues.Pending:
                this.getPendingQueueStats();
                break;
            case data_model_1.NofQueues.Park:
                this.getParkingQueueStats();
                break;
        }
    };
    NofQueueComponent.prototype.getItemEString = function (proposal, history) {
        var maxsize = history ? 35 : 54;
        var result = proposal.itemE.substring(0, maxsize);
        if (proposal.itemE.length > maxsize)
            result += '...';
        return result;
    };
    NofQueueComponent.prototype.getOriginatorString = function (proposal, history) {
        var maxsize = history ? 10 : 8;
        var result = proposal.originator.substring(0, maxsize);
        if (proposal.originator.length > maxsize)
            result += '...';
        return result;
    };
    NofQueueComponent.prototype.getOperatorString = function (proposal) {
        if (proposal.modifiedByUsr === null || proposal.modifiedByUsr.length === 0)
            return proposal.operator;
        else
            return proposal.modifiedByUsr;
    };
    NofQueueComponent.prototype.getApproverString = function (proposal) {
        if (proposal.status === data_model_2.ProposalStatus.Disseminated ||
            proposal.status === data_model_2.ProposalStatus.DisseminatedModified ||
            proposal.status === data_model_2.ProposalStatus.Rejected)
            return proposal.modifiedByUsr;
        else
            return '';
    };
    NofQueueComponent.prototype.getApproverStringHistory = function (proposal) {
        if (proposal.status === data_model_2.ProposalStatus.Disseminated ||
            proposal.status === data_model_2.ProposalStatus.DisseminatedModified ||
            proposal.status === data_model_2.ProposalStatus.Rejected)
            return proposal.modifiedByUsr;
        else
            return '';
    };
    NofQueueComponent.prototype.checkExpireCounter = function () {
        var self = this;
        this.intervalHandler = window.setInterval(function () {
            self.getExpiringQueueStats.call(self);
        }, syncFrecuency * 1000);
    };
    NofQueueComponent.prototype.getExpiringQueueStats = function () {
        var _this = this;
        try {
            //Run it just if the user is on the Review Queue
            if (this.queueName === 'REVIEW_QUEUE') {
                this.dataService.getExpiringTotal()
                    .subscribe(function (count) {
                    var self = _this;
                    var audioElem;
                    var shouldPlaySound = false;
                    if (count != _this.expiringCount) {
                        if (count > _this.expiringCount) {
                            shouldPlaySound = true;
                            audioElem = document.getElementById('estimated-queue-sound');
                            audioElem.load();
                            audioElem.play();
                        }
                        _this.expiringCount = count;
                        self.runExpiringBellAnimation = true;
                        setTimeout(function () {
                            self.runExpiringBellAnimation = false;
                            if (shouldPlaySound) {
                                audioElem.pause();
                                audioElem.currentTime = 0;
                            }
                        }, 10000);
                        if (_this.expiringApplied)
                            _this.getProposalsByCategory(_this.queryOptions.page);
                    }
                    //Here we need to hide the button and switch to the normal query
                    if (_this.expiringCount === 0 && _this.expiringApplied) {
                        _this.executeFilter();
                    }
                });
            }
        }
        catch (e) {
        }
    };
    NofQueueComponent.prototype.executeFilter = function () {
        this.expiringApplied = !this.expiringApplied;
        if (this.expiringApplied) {
            this.getProposalsByCategory(this.queryOptions.page);
        }
        else {
            this.changeCategory(this.parentCategories[0].id);
        }
    };
    NofQueueComponent.prototype.runCloneAction = function () {
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.router.navigate(["/dashboard/clone", this.selectedProposal.id, false]);
        }
    };
    NofQueueComponent.prototype.onKeyup = function ($event) {
        //this seems like it will be messy in the future
        switch ($event.code) {
            case "ArrowUp": //NAVIGATE TABLE ROW UP
                if (this.selectedProposal.index > 0) {
                    this.selectProposal(this.proposals[this.selectedProposal.index - 1]);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedProposal.index < this.proposals.length - 1) {
                    this.selectProposal(this.proposals[this.selectedProposal.index + 1]);
                }
                break;
            case "KeyV": //ALT-V: REVIEW
                if ($event.altKey && this.selectedProposal) {
                    this.router.navigate(["/dashboard/view", this.selectedProposal.id]);
                }
                break;
            case "Enter": //ENTER: PICK
                if (this.selectedProposal && !this.isPickedButtonDisabled()) {
                    this.router.navigate(["/dashboard/pick", this.selectedProposal.id]);
                }
                break;
            case "KeyP": //ALT-P: PICK
                if ($event.altKey) {
                    if (this.selectedProposal && !this.isPickedButtonDisabled()) {
                        this.router.navigate(["/dashboard/pick", this.selectedProposal.id]);
                    }
                }
                break;
            case "KeyH": //ALT-H: HISTORY
                if ($event.altKey && this.selectedProposal && !this.isHistoryButtonDisabled()) {
                    this.showHistory(this.selectedProposal.id);
                }
                break;
            case "KeyZ": //ALT-Z: DISCARD
                if ($event.altKey && this.selectedProposal) {
                    if (!this.isDiscardButtonHidden(this.selectedProposal.status)) {
                        this.confirmDiscardProposal();
                    }
                }
                break;
            case "KeyC":
                if ($event.altKey && this.selectedProposal) { //ALT-C CLONE PROPOSAL
                    this.router.navigate(["/dashboard/clone", this.selectedProposal.id, false]);
                }
                else { //CTRL-C START NOTAM CREATION
                    if ($event.ctrlKey) {
                        this.frsPrevKeyCodeEvent = $event;
                    }
                    else {
                        if (this.frsPrevKeyCodeEvent) { //CTRL-C C: CATCHALL
                            this.currentCategoryId = data_model_2.NsdCategory.CatchAll;
                            this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                        }
                    }
                }
                break;
            case "KeyO": //CTRL-C O: OBSTACLE
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyR": //CTRL-C R: RUNWAY
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyT": // CTRL-C T: Taxiway
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyA": // CTRL-C T: 
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "Digit1":
                //CTRL-C O 1: OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_2.NsdCategory.Obst;
                    this.currentSubCategoryId = 4;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                // CTRL-C R 1: RWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyR") {
                    this.currentCategoryId = data_model_2.NsdCategory.RwyClsd;
                    this.currentSubCategoryId = 7;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C T 1: TWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyT") {
                    debugger;
                    this.currentCategoryId = data_model_2.NsdCategory.TwyClsd;
                    this.currentSubCategoryId = 11;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C A 1: CARS CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyA") {
                    this.currentCategoryId = data_model_2.NsdCategory.CarsClsd;
                    this.currentSubCategoryId = 13;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit2":
                //CTRL-C O 2: OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_2.NsdCategory.ObstLGT;
                    this.currentSubCategoryId = 5;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit3":
                //CTRL-C O 3: MULTI OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_2.NsdCategory.MultiObst;
                    this.currentSubCategoryId = 14;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit4":
                //CTRL-C O 3: MULTI OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_2.NsdCategory.MultiObstLGT;
                    this.currentSubCategoryId = 15;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
        }
    };
    NofQueueComponent.prototype.onCategoryChanged = function (e) {
        this.changeCategory(e.value);
    };
    NofQueueComponent.prototype.onChildCategoryChanged = function (e) {
        this.changeChildCategory(e.value);
    };
    NofQueueComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getProposalsByCategory(this.queryOptions.page);
    };
    NofQueueComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    NofQueueComponent.prototype.confirmDiscardProposal = function () {
        $('#modal-delete').modal({});
    };
    NofQueueComponent.prototype.discardProposal = function () {
        var _this = this;
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.dataService.discardNofProposal(this.selectedProposal.id)
                .subscribe(function (response) {
                _this.getProposalsByCategory(_this.queryOptions.page);
                _this.toastr.success("Proposal successfully discarded!", "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                _this.isLeavingForm = false;
                _this.toastr.error("Discard proposal error: " + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NofQueueComponent.prototype.resolveCategoryId = function () {
        if (this.childCategories.length > 0) {
            return this.currentSubCategoryId !== data_model_2.NsdCategory.All ? this.currentSubCategoryId : this.currentCategoryId;
        }
        return this.currentCategoryId;
    };
    NofQueueComponent.prototype.isCreateButtonDisable = function () {
        return this.childCategories.length > 0 ? this.currentSubCategoryId === data_model_2.NsdCategory.All : this.currentCategoryId === data_model_2.NsdCategory.All;
    };
    NofQueueComponent.prototype.cssIcon = function (proposal) {
        switch (proposal.status) {
            case data_model_2.ProposalStatus.Draft: return 'fa-save';
            case data_model_2.ProposalStatus.Submitted: return 'fa-upload';
            case data_model_2.ProposalStatus.Withdrawn: return 'fa-download';
            case data_model_2.ProposalStatus.Picked: return 'fa-hourglass-half';
            case data_model_2.ProposalStatus.Expired: return 'fa-clock-o';
            case data_model_2.ProposalStatus.Replaced: return 'fa-code-fork';
            case data_model_2.ProposalStatus.Parked: return 'fa-lock';
            case data_model_2.ProposalStatus.ParkedDraft: return 'fa-lock';
            case data_model_2.ProposalStatus.ParkedPicked: return 'fa-lock';
            case data_model_2.ProposalStatus.Disseminated: return 'fa-wifi';
            case data_model_2.ProposalStatus.DisseminatedModified: return 'fa-wifi';
            case data_model_2.ProposalStatus.Cancelled: return 'fa-hand-paper-o';
            case data_model_2.ProposalStatus.Rejected: return 'fa-ban';
            case data_model_2.ProposalStatus.Discarded: return 'fa-trash';
            case data_model_2.ProposalStatus.Terminated: return 'fa-hand-paper-o';
        }
    };
    NofQueueComponent.prototype.rowColor = function (proposal) {
        if (proposal.grouped) {
            return "bg-rejected";
        }
        if (proposal.soonToExpire && this.queueName === 'REVIEW_QUEUE') {
            return 'bg-soontoexpire';
        }
        if (proposal.isSelected) {
            return 'success';
        }
        switch (proposal.status) {
            case data_model_2.ProposalStatus.Draft: return 'bg-saved';
            case data_model_2.ProposalStatus.Submitted: return '';
            case data_model_2.ProposalStatus.Withdrawn: return 'bg-widthdrawn';
            case data_model_2.ProposalStatus.Picked: return 'bg-picked';
            case data_model_2.ProposalStatus.Parked: return 'bg-parked';
            case data_model_2.ProposalStatus.ParkedDraft: return 'bg-parked';
            case data_model_2.ProposalStatus.ParkedPicked: return 'bg-parked';
            case data_model_2.ProposalStatus.Replaced: return 'bg-replace';
            case data_model_2.ProposalStatus.Rejected: return 'bg-rejected';
            case data_model_2.ProposalStatus.Expired: return 'bg-expired';
            case data_model_2.ProposalStatus.Cancelled: return 'bg-cancel';
            case data_model_2.ProposalStatus.Terminated: return 'bg-cancel';
            case data_model_2.ProposalStatus.DisseminatedModified: return 'bg-nof-modified';
            default: return '';
        }
    };
    NofQueueComponent.prototype.isDiscardButtonHidden = function (proposal) {
        if (proposal) {
            return proposal.status === data_model_2.ProposalStatus.Picked ||
                proposal.status === data_model_2.ProposalStatus.Parked ||
                proposal.status === data_model_2.ProposalStatus.ParkedDraft ||
                proposal.status === data_model_2.ProposalStatus.ParkedPicked;
        }
        return false;
    };
    NofQueueComponent.prototype.isReviewButtonDissabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        return false;
    };
    NofQueueComponent.prototype.isCloneButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.proposalType === data_model_2.ProposalType.Cancel) {
            return true;
        }
        return false;
    };
    NofQueueComponent.prototype.isReplaceButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.grouped && !this.selectedProposal.groupId) {
            return true;
        }
        if (this.selectedProposal.isObsolete)
            return true;
        return this.selectedProposal.status === data_model_2.ProposalStatus.Terminated ||
            (this.selectedProposal.status !== data_model_2.ProposalStatus.Disseminated && this.selectedProposal.status !== data_model_2.ProposalStatus.DisseminatedModified);
    };
    NofQueueComponent.prototype.isCancelButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.proposalType === data_model_2.ProposalType.Cancel || this.selectedProposal.proposalType === data_model_2.ProposalStatus.Expired) {
            return true;
        }
        if (this.selectedProposal.isObsolete)
            return true;
        return this.selectedProposal.status === data_model_2.ProposalStatus.Terminated ||
            (this.selectedProposal.status !== data_model_2.ProposalStatus.Disseminated && this.selectedProposal.status !== data_model_2.ProposalStatus.DisseminatedModified);
    };
    NofQueueComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getProposalsByCategory(data_model_2.NsdCategory.All);
    };
    NofQueueComponent.prototype.selectProposal = function (proposal) {
        if (this.selectedProposal) {
            this.selectedProposal.isSelected = false;
        }
        this.selectedProposal = proposal;
        this.selectedProposal.isSelected = true;
        this.memStorageService.save(this.memStorageService.PROPOSAL_ID_KEY, this.selectedProposal.id);
    };
    NofQueueComponent.prototype.showGroup = function (rowIndex) {
        var _this = this;
        var pos = rowIndex + 1;
        if (this.proposals[rowIndex].groupOpen) {
            if (this.proposals[rowIndex].group.length > 0) {
                this.proposals.splice(pos, this.proposals[rowIndex].group.length);
            }
            this.proposals[rowIndex].groupOpen = false;
        }
        else {
            this.proposals[rowIndex].groupOpen = true;
        }
        this.proposals[rowIndex].group = [];
        if (this.proposals[rowIndex].groupOpen) {
            this.dataService.getNofGroupedProposals(this.proposals[rowIndex].groupId, this.queueName)
                .subscribe(function (proposals) {
                if (proposals.length > 0) {
                    for (var i = 0; i < proposals.length; i++) {
                        proposals[i].isSelected = false;
                        proposals[i].grouped = true;
                        proposals[i].groupId = null;
                        _this.proposals[rowIndex].group.push(proposals[i]);
                    }
                    var pos_1 = rowIndex + 1;
                    if (_this.proposals[rowIndex].group) {
                        var isLastRow = _this.proposals.length === pos_1;
                        for (var i = 0; i < _this.proposals[rowIndex].group.length; i++) {
                            var pg = _this.proposals[rowIndex].group[i];
                            if (isLastRow) {
                                _this.proposals.push(pg);
                            }
                            else {
                                _this.proposals.splice(pos_1 + i, 0, pg);
                            }
                        }
                    }
                }
            });
        }
    };
    NofQueueComponent.prototype.changeRoute = function (routeValue) {
        var _this = this;
        this.isLeavingForm = true;
        this.router.navigate(routeValue).catch(function () {
            _this.isLeavingForm = false;
            _this.toastr.error("This action is not allowed at this time.", "Action failed!", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofQueueComponent.prototype.pickProposal = function (proposal) {
        this.selectProposal(proposal);
        if (!this.isPickedButtonDisabled()) {
            this.router.navigate(["/dashboard/pick", this.selectedProposal.id]);
        }
    };
    NofQueueComponent.prototype.onPageChange = function (page) {
        this.getProposalsByCategory(page);
    };
    NofQueueComponent.prototype.showHistory = function (proposalId) {
        var _this = this;
        $('#proposal-history').modal({});
        this.dataService.getProposalHistory(proposalId)
            .subscribe(function (proposals) {
            if (proposals.length > 0) {
                _this.proposalHistory = proposals;
                _this.proposalHistory[0].isSelected = true;
            }
            else {
                _this.proposalHistory = [];
            }
        });
    };
    NofQueueComponent.prototype.isPickedButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        return this.selectedProposal.status === data_model_2.ProposalStatus.Disseminated;
    };
    NofQueueComponent.prototype.isHistoryButtonDisabled = function () {
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        return this.selectedProposal.status === data_model_2.ProposalStatus.Draft;
    };
    NofQueueComponent.prototype.isNSDAllowed = function (catName) {
        var id = this.categoriesData.findIndex(function (x) { return x.name === catName; });
        if (id === -1)
            return false;
        return true;
    };
    NofQueueComponent.prototype.changeCategory = function (catId) {
        this.currentCategoryId = +catId;
        this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
        var queryOptions = this.memStorageService.get(this.queryStoreName);
        this.memStorageService.save(this.memStorageService.CATEGORY_ID_KEY, this.currentCategoryId);
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, data_model_2.NsdCategory.All); //set all by default
        this.getProposalsByCategory(data_model_2.NsdCategory.All);
    };
    NofQueueComponent.prototype.changeChildCategory = function (subCatId) {
        this.currentSubCategoryId = +subCatId;
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, this.currentSubCategoryId);
        this.getProposalsByCategory(data_model_2.NsdCategory.All);
    };
    NofQueueComponent.prototype.getProposalsByCategory = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.queryStoreName, this.queryOptions);
            if (this.childCategories.length > 0) {
                if (this.currentSubCategoryId !== data_model_2.NsdCategory.All) {
                    this.dataService.getNofProposalsByQueueName(this.currentSubCategoryId, this.queueName, this.queryOptions)
                        .subscribe(function (proposals) {
                        _this.processProposals(proposals);
                    });
                }
                else {
                    this.dataService.getNOfProposalsByParentCategoryAndQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                        .subscribe(function (proposals) {
                        _this.processProposals(proposals);
                    });
                }
            }
            else if (this.expiringApplied) {
                this.dataService.getExpiringProposals(this.queryOptions)
                    .subscribe(function (proposals) {
                    _this.processProposals(proposals);
                });
            }
            else {
                this.dataService.getNofProposalsByQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                    .subscribe(function (proposals) {
                    _this.processProposals(proposals);
                });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    NofQueueComponent.prototype.processProposals = function (proposals) {
        this.paging = proposals.paging;
        this.totalPages = proposals.paging.totalPages;
        for (var iter = 0; iter < proposals.data.length; iter++)
            proposals.data[iter].index = iter;
        var lastProposalSelected = this.memStorageService.get(this.memStorageService.PROPOSAL_ID_KEY);
        if (proposals.data.length > 0) {
            this.proposals = proposals.data;
            var index = 0;
            if (lastProposalSelected) {
                index = this.proposals.findIndex(function (x) { return x.id === lastProposalSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.proposals[index].isSelected = true;
            this.selectedProposal = this.proposals[index];
            this.loadingData = false;
        }
        else {
            this.proposals = [];
            this.selectedProposal = null;
            this.loadingData = false;
        }
    };
    NofQueueComponent.prototype.checkProposalStatus = function () {
        var self = this;
        this.handler = window.setInterval(function () {
            if (self.proposals !== undefined) {
                if (self.proposals.length > 0) {
                    var proposalsIds = self.proposals
                        .map(function (p) {
                        return p.id;
                    });
                    self.dataService.getProposalStatus(proposalsIds, syncFrecuency, self.lastSyncTime)
                        .subscribe(function (response) {
                        var proposalStatus = response.proposalStatus;
                        self.lastSyncTime = response.lastSyncTime;
                        for (var _i = 0, proposalStatus_1 = proposalStatus; _i < proposalStatus_1.length; _i++) {
                            var p = proposalStatus_1[_i];
                            self.updateProposalStatus(p);
                        }
                    });
                }
            }
        }, syncFrecuency * 1000);
    };
    NofQueueComponent.prototype.updateProposalStatus = function (data) {
        if (data) {
            var index = this.proposals.findIndex(function (x) { return x.id === data.id; });
            if (index >= 0) {
                if (data.status === data_model_2.ProposalStatus.Deleted || data.status === data_model_2.ProposalStatus.Discarded ||
                    data.status === data_model_2.ProposalStatus.Expired || data.status === data_model_2.ProposalStatus.Terminated ||
                    data.status === data_model_2.ProposalStatus.Withdrawn) {
                    this.deleteRow(index);
                }
                else {
                    var $rowElem = $("#p_" + data.id);
                    var iniBgColorClass = this.rowColor(this.proposals[index]);
                    this.proposals[index].notamId = data.notamId;
                    if (data.startActivity) {
                        this.proposals[index].startActivity = data.startActivity;
                    }
                    this.proposals[index].endValidity = data.endValidity; //clean endValidity when cancellation...
                    this.proposals[index].status = data.status;
                    this.proposals[index].soonToExpire = data.soonToExpire;
                    this.proposals[index].modifiedByUsr = data.modifiedByUsr;
                    this.proposals[index].originator = data.originator;
                    this.proposals[index].operator = data.operator;
                    this.proposals[index].itemE = data.itemE;
                    this.proposals[index].itemA = data.itemA;
                    this.proposals[index].siteId = data.siteId;
                    this.proposals[index].estimated = data.estimated;
                    var endBgColorClass = this.rowColor(this.proposals[index]);
                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.removeClass(iniBgColorClass, { duration: 500 });
                    }
                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.addClass(endBgColorClass, { duration: 500 });
                    }
                }
            }
        }
    };
    NofQueueComponent.prototype.deleteRow = function (index) {
        this.proposals.splice(index, 1);
        var selectedIndex = this.proposals.findIndex(function (x) { return x.isSelected; });
        if (this.proposals.length > 0) {
            this.selectedProposal = this.proposals[0];
            if (selectedIndex >= 0 && this.selectedProposal.id !== selectedIndex) {
                this.proposals[selectedIndex].isSelected = false;
            }
            this.selectedProposal.isSelected = true;
        }
    };
    NofQueueComponent.prototype.addItemToComboFilter = function (comboData, category) {
        comboData.push({
            groupCategoryId: category.groupCategoryId,
            id: category.id,
            isSelected: category.isSelected,
            name: category.name,
            text: category.name
        });
    };
    NofQueueComponent.prototype.addChildCategoryToComboFilter = function (comboData, currentCategoryId) {
        comboData = [];
        var categoryIndex = this.categoriesData.findIndex(function (x) { return x.id === currentCategoryId; });
        if (categoryIndex >= 0) {
            if (this.categoriesData[categoryIndex].children) {
                this.addAllToComboFilter(comboData);
                for (var i = 0; i < this.categoriesData[categoryIndex].children.length; i++) {
                    var subCategory = this.categoriesData[categoryIndex].children[i];
                    this.addItemToComboFilter(comboData, subCategory);
                }
            }
        }
        this.currentSubCategoryId = data_model_2.NsdCategory.All;
        return comboData;
    };
    NofQueueComponent.prototype.addAllToComboFilter = function (comboData) {
        comboData.push({
            groupCategoryId: 0,
            id: 1,
            isSelected: true,
            name: 'Root',
            text: this.translation.translate('Dashboard.MessageSaveAttachment1')
        });
    };
    NofQueueComponent.prototype.getPendingQueueStats = function () {
        if (this.proposals) {
            var proposalsWithGroupOpen = this.proposals.filter(function (x) { return x.groupOpen; });
            var childrenProposals = 0;
            for (var _i = 0, proposalsWithGroupOpen_1 = proposalsWithGroupOpen; _i < proposalsWithGroupOpen_1.length; _i++) {
                var p = proposalsWithGroupOpen_1[_i];
                childrenProposals += p["group"].length;
            }
            if (this.proposals.length - childrenProposals !== this.queueCount) {
                this.getLatestProposals(this.queueCount);
            }
        }
    };
    NofQueueComponent.prototype.getParkingQueueStats = function () {
        if (this.proposals) {
            var proposalsWithGroupOpen = this.proposals.filter(function (x) { return x.groupOpen; });
            var childrenProposals = 0;
            for (var _i = 0, proposalsWithGroupOpen_2 = proposalsWithGroupOpen; _i < proposalsWithGroupOpen_2.length; _i++) {
                var p = proposalsWithGroupOpen_2[_i];
                childrenProposals += p["group"].length;
            }
            if (this.proposals.length - childrenProposals !== this.queueCount) {
                this.getLatestProposals(this.queueCount);
            }
        }
    };
    NofQueueComponent.prototype.getLatestProposals = function (totalCount) {
        var _this = this;
        try {
            if (this.childCategories.length > 0) {
                if (this.currentSubCategoryId !== data_model_2.NsdCategory.All) {
                    this.dataService.getNofProposalsByQueueName(this.currentSubCategoryId, this.queueName, this.queryOptions)
                        .subscribe(function (proposals) {
                        _this.refreshProposals(proposals, totalCount);
                    });
                }
                else {
                    this.dataService.getNOfProposalsByParentCategoryAndQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                        .subscribe(function (proposals) {
                        _this.refreshProposals(proposals, totalCount);
                    });
                }
            }
            else {
                this.dataService.getNofProposalsByQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                    .subscribe(function (proposals) {
                    _this.refreshProposals(proposals, totalCount);
                });
            }
        }
        catch (e) {
        }
    };
    NofQueueComponent.prototype.refreshProposals = function (proposals, queueCount) {
        if (proposals) {
            //let difference = proposals.data.filter(x => !this.proposals.find(y => y.id === x.id));
            //for (let d of difference) {
            //    const index = proposals.data.findIndex(x => x.id === d.id);
            //    if (index > 0) {
            //        proposals.data.splice(index, 1);
            //        proposals.data.unshift(d);
            //    }
            //}
            for (var iter = 0; iter < proposals.data.length; iter++) {
                proposals.data[iter].isSelected = false;
                proposals.data[iter].index = iter;
            }
            if (proposals.data.length > 0) {
                var currentSelectedId_1 = this.selectedProposal ? this.selectedProposal.id : -1;
                var lastProposalSelected = proposals.data.find(function (x) { return x.id == currentSelectedId_1; });
                if (lastProposalSelected) {
                    this.selectedProposal = lastProposalSelected;
                    this.selectedProposal.isSelected = true;
                }
                else {
                    this.selectedProposal = proposals.data[0];
                    this.selectedProposal.isSelected = true;
                }
                this.proposals = proposals.data;
            }
            else {
                this.proposals = [];
            }
        }
    };
    NofQueueComponent.prototype.animateIncomingProposals = function (newProposals) {
        if (newProposals) {
            window.setTimeout(function () {
                for (var _i = 0, newProposals_1 = newProposals; _i < newProposals_1.length; _i++) {
                    var p = newProposals_1[_i];
                    var rowElem = $("#data-response #p_" + p.id);
                    if (rowElem.length) {
                        rowElem.addClass('bg-received', 250, 'linear', function (elm, classes) {
                            window.setTimeout(function () {
                                elm.removeClass('bg-received');
                            }, 10000);
                        }(rowElem));
                    }
                }
            }, 500);
        }
    };
    __decorate([
        core_1.Input('name'),
        __metadata("design:type", String)
    ], NofQueueComponent.prototype, "queueName", void 0);
    __decorate([
        core_1.Input('count'),
        __metadata("design:type", Number)
    ], NofQueueComponent.prototype, "queueCount", void 0);
    __decorate([
        core_1.HostListener('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NofQueueComponent.prototype, "onKeyup", null);
    NofQueueComponent = __decorate([
        core_1.Component({
            selector: 'nof-queue',
            templateUrl: '/app/dashboard/nof/nof-queue.component.html',
            styleUrls: ['app/dashboard/nof/nof-queue.component.css']
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            angular_l10n_1.TranslationService,
            router_1.ActivatedRoute])
    ], NofQueueComponent);
    return NofQueueComponent;
}());
exports.NofQueueComponent = NofQueueComponent;
//# sourceMappingURL=nof-queue.component.js.map