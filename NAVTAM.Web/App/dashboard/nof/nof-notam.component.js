"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.debounceFunc = exports.NofNotamComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
//import { Subject } from 'rxjs/Subject';
var data_service_1 = require("../shared/data.service");
var location_service_1 = require("../shared/location.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var nof_notam_filter_component_1 = require("./nof-notam-filter.component");
var data_model_1 = require("../shared/data.model");
var NofNotamComponent = /** @class */ (function () {
    function NofNotamComponent(toastr, dataService, memStorageService, router, winRef, locationService, route, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.locationService = locationService;
        this.route = route;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterPaging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterTotalPages = 0;
        this.totalPages = 0;
        this.loadingData = false;
        this.sdoSubject = null;
        this.leafletmap = null;
        this.mapHealthy = false;
        this.isLeavingForm = false;
        this.isFilterApplied = false;
        this.filterPannelOpen = false;
        this.icaoFormat = "";
        this.tableHeaders = {
            sorting: [
                { columnName: 'NotamId', direction: 0 },
                { columnName: 'Type', direction: 0 },
                { columnName: 'ReferredNotamId', direction: 0 },
                { columnName: 'SiteID', direction: 0 },
                { columnName: 'Series', direction: 0 },
                { columnName: 'ItemA', direction: 0 },
                { columnName: 'Code23', direction: 0 },
                { columnName: 'Code45', direction: 0 },
                { columnName: 'Permanent', direction: 0 },
                { columnName: 'Estimated', direction: 0 },
                //{ columnName: 'Operator', direction: 0 },
                { columnName: 'Originator', direction: 0 },
                { columnName: 'ModifiedByUsr', direction: 0 },
                { columnName: 'StartActivity', direction: 0 },
                { columnName: 'EndValidity', direction: 1 },
                { columnName: 'Published', direction: 0 },
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '25', text: '25' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[13].columnName //Default EndValidity
        };
        this.clearIcao();
    }
    NofNotamComponent.prototype.onKeyup = function ($event) {
        switch ($event.code) {
            case "ArrowUp": //NAVIGATE TABLE ROW UP
                if (this.selectedNotam.index > 0) {
                    this.onRowSelected(this.selectedNotam.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedNotam.index < this.notams.length - 1) {
                    this.onRowSelected(this.selectedNotam.index + 1);
                }
                break;
        }
    };
    NofNotamComponent.prototype.ngOnInit = function () {
        //Set latest query options
        var faKey = this.memStorageService.get(this.memStorageService.FILTER_APPLIED_KEY);
        if (faKey)
            this.isFilterApplied = faKey;
        //this.isFilterApplied = this.memStorageService.get(this.memStorageService.FILTER_APPLIED_KEY);
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        }
        else {
            var queryOptions = this.memStorageService.get(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY);
            if (queryOptions !== null) {
                this.queryOptions = queryOptions;
            }
        }
        if (this.isFilterApplied) {
            this.filter.runFilter(false);
        }
        else {
            this.getNotams(this.queryOptions.page);
        }
    };
    NofNotamComponent.prototype.ngAfterViewInit = function () {
        this.filter.parent = this;
        this.mapHealthCheck();
        $(document).ready(function () {
            $(window).scrollTop(0);
        });
    };
    NofNotamComponent.prototype.runCloneAction = function () {
        if (this.selectedNotam && this.selectedNotam.notamType !== data_model_1.ProposalType.Cancel) {
            this.isLeavingForm = true;
            //This we don't pass this.isFilterApplied, because with a NOTAM, always the Clone have to go to Proposal table
            this.router.navigate(["/dashboard/notamclone", this.selectedNotam.id]);
        }
    };
    NofNotamComponent.prototype.runReviewAction = function () {
        if (this.selectedNotam) {
            this.isLeavingForm = true;
            this.router.navigate(["/dashboard/notamview", this.selectedNotam.id]);
        }
    };
    NofNotamComponent.prototype.ngOnDestroy = function () {
        this.disposeMap();
    };
    NofNotamComponent.prototype.onFilterChanged = function (event) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        }
        else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processNotams(event.data);
            }
            else {
                this.queryOptions = this.memStorageService.get(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY);
                this.getNotams(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    };
    NofNotamComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1);
        }
        else {
            this.getNotams(1);
        }
    };
    NofNotamComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    NofNotamComponent.prototype.rowColor = function (notam) {
        if (notam.isExpired) {
            return "bg-expired"; // here we are actually returning only the expired and replaced notams
        }
        switch (notam.notamType) {
            case data_model_1.ProposalType.New: return '';
            case data_model_1.ProposalType.Replace: return 'bg-submitted';
            case data_model_1.ProposalType.Cancel: return 'bg-widthdrawn';
            default: return '';
        }
    };
    NofNotamComponent.prototype.itemsPerPageChanged = function (e) {
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, e.value);
        }
        else {
            this.queryOptions.pageSize = e.value;
            this.getNotams(1);
        }
    };
    NofNotamComponent.prototype.onRowSelected = function (index) {
        this.selectNotam(this.notams[index]);
        this.updateMap(this.notams[index], false);
    };
    NofNotamComponent.prototype.onPageChange = function (page) {
        this.getNotams(page);
    };
    NofNotamComponent.prototype.replaceClick = function () {
        var _this = this;
        this.isLeavingForm = true;
        this.dataService.verifyObsoleteNotam(this.selectedNotam.id)
            .subscribe(function (response) {
            _this.selectedNotam.isObsolete = response;
            if (!_this.selectedNotam.isObsolete) {
                //Note: We are using "ItemX = ReplaceNotam=1" to determine when a NOF operator is replacing/cancelling a NOTAM directly from the NOTAM panel
                _this.dataService.updateProposalItemX(_this.selectedNotam.proposalId, "ReplaceNotam=1")
                    .subscribe(function (resp) {
                    _this.router.navigate(["/dashboard/replace", _this.selectedNotam.proposalId, 'notam']);
                }, function (error) {
                    _this.toastr.error(error.message + " NotamID: " + _this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
                    _this.isLeavingForm = true;
                });
            }
        }, function (error) {
            _this.toastr.error(error.message + " NotamID: " + _this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
            _this.isLeavingForm = false;
        });
    };
    NofNotamComponent.prototype.cancelClick = function () {
        var _this = this;
        this.isLeavingForm = true;
        this.dataService.verifyObsoleteNotam(this.selectedNotam.id)
            .subscribe(function (response) {
            if (!_this.selectedNotam.isObsolete) {
                //Note: We are using "ItemX = ReplaceNotam=1" to determine when a NOF operator is replacing/cancelling a NOTAM directly from the NOTAM panel
                _this.dataService.updateProposalItemX(_this.selectedNotam.proposalId, "ReplaceNotam=1")
                    .subscribe(function (resp) {
                    _this.router.navigate(["/dashboard/cancel", _this.selectedNotam.proposalId, 'notam']);
                }, function (error) {
                    _this.toastr.error(error.message + " NotamID: " + _this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
                    _this.isLeavingForm = true;
                });
            }
        }, function (error) {
            _this.toastr.error(error.message + " NotamID: " + _this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
            _this.isLeavingForm = true;
        });
    };
    NofNotamComponent.prototype.isCloneButtonDisabled = function () {
        if (!this.selectedNotam) {
            return true;
        }
        if (this.selectedNotam.notamType === data_model_1.ProposalType.Cancel) {
            return true;
        }
        if (this.selectedNotam.seriesChecklist) {
            return true;
        }
        if (!this.selectedNotam) {
            return true;
        }
        return false;
    };
    NofNotamComponent.prototype.isReviewButtonDissabled = function () {
        if (!this.selectedNotam) {
            return true;
        }
        if (this.selectedNotam.seriesChecklist) {
            return true;
        }
        return false;
    };
    NofNotamComponent.prototype.isReplaceButtonDisabled = function () {
        if (!this.selectedNotam) {
            return true;
        }
        if (this.selectedNotam.seriesChecklist) {
            return true;
        }
        if (this.selectedNotam.notamType === data_model_1.ProposalType.Cancel) {
            return true;
        }
        if (this.selectedNotam.isObsolete)
            return true;
        return false;
    };
    NofNotamComponent.prototype.isCancelButtonDisabled = function () {
        if (!this.selectedNotam) {
            return true;
        }
        if (this.selectedNotam.seriesChecklist) {
            return true;
        }
        if (this.selectedNotam.notamType === data_model_1.ProposalType.Cancel) {
            return true;
        }
        if (this.selectedNotam.isObsolete)
            return true;
        return false;
    };
    NofNotamComponent.prototype.isRunReportButtonEnabled = function () {
        return true;
    };
    NofNotamComponent.prototype.isHistoryButtonDisabled = function () {
        if (!this.selectedNotam) {
            return true;
        }
        if (this.selectedNotam.seriesChecklist) {
            return true;
        }
    };
    NofNotamComponent.prototype.getOperatorString = function (notam) {
        if (notam.modifiedByUsr === null || notam.modifiedByUsr.length === 0)
            return notam.operator;
        else
            return notam.modifiedByUsr;
    };
    NofNotamComponent.prototype.getOriginatorString = function (originator) {
        if (originator.length > 10) {
            return originator.substring(0, 10) + '..';
        }
        return originator;
    };
    NofNotamComponent.prototype.getApproverString = function (notam) {
        if (notam.isProposal) {
            if (notam.status === data_model_1.ProposalStatus.Disseminated ||
                notam.status === data_model_1.ProposalStatus.DisseminatedModified ||
                notam.status === data_model_1.ProposalStatus.Rejected)
                return notam.modifiedByUsr;
            else
                return '';
        }
        else {
            return notam.modifiedByUsr;
        }
    };
    NofNotamComponent.prototype.onProposalHistory = function (rowIndex, parentIndex, proposalId) {
        var _this = this;
        if (this.notamTrakingList[rowIndex]['historyOpen']) {
            this.notamTrakingList[rowIndex]['historyOpen'] = false;
            this.notamTrakingList = this.notamTrakingList.filter(function (item) {
                return !(item.isProposal && item.rowIndex === parentIndex);
            });
        }
        else {
            this.dataService.getProposalHistory(proposalId)
                .subscribe(function (proposals) {
                if (proposals.length > 0) {
                    for (var i = 0; i < proposals.length; i++) {
                        var notamId = proposals[i].notamId;
                        if (notamId) {
                            var pos = notamId.indexOf('/');
                            if (pos > 0) {
                                var proposalNumberStr = notamId.substring(pos - 4, 5);
                                var proposalNumber = parseInt(proposalNumberStr);
                                var notamNumberStr = _this.notamTrakingList[rowIndex].notamId.substring(pos - 4, 5);
                                ;
                                var notamNumber = parseInt(notamNumberStr);
                                if (notamNumber >= proposalNumber) {
                                    proposals[i].isProposal = true;
                                    proposals[i].rowIndex = parentIndex;
                                    proposals[i].published = proposals[i].received;
                                    _this.notamTrakingList[rowIndex]['historyOpen'] = true;
                                    _this.notamTrakingList.splice(rowIndex + 1, 0, proposals[i]);
                                }
                            }
                        }
                        else {
                            proposals[i].isProposal = true;
                            proposals[i].rowIndex = parentIndex;
                            proposals[i].published = proposals[i].received;
                            _this.notamTrakingList[parentIndex]['historyOpen'] = true;
                            _this.notamTrakingList.splice(rowIndex + 1, 0, proposals[i]);
                        }
                    }
                }
                else {
                }
            });
        }
    };
    NofNotamComponent.prototype.showHistory = function () {
        var _this = this;
        $('#notam-tracking-list').modal({});
        this.dataService.getNofNotamTrackingList(this.selectedNotam.proposalId)
            .subscribe(function (result) {
            if (result.length > 0) {
                var selectedIndex = result.findIndex(function (x) { return x.id === _this.selectedNotam.id; });
                if (selectedIndex < 0) {
                    selectedIndex = 0;
                }
                for (var iter = 0; iter < result.length; iter++) {
                    result[iter].index = iter;
                    result[iter].historyOpen = false;
                    result[iter].typeStr = _this.toNotamTypeStr(result[iter].notamType);
                }
                _this.notamTrakingList = result;
                _this.notamTrakingList[selectedIndex].isSelected = true;
            }
            else {
                _this.notamTrakingList = [];
            }
        });
    };
    NofNotamComponent.prototype.selectNotam = function (notam) {
        var _this = this;
        if (this.selectedNotam) {
            this.notams[this.selectedNotam.index].isSelected = false;
        }
        notam.isSelected = true;
        this.selectedNotam = JSON.parse(JSON.stringify(notam)); //notam
        this.memStorageService.save(this.memStorageService.NOTAM_ID_KEY, this.selectedNotam.id);
        this.dataService.verifyObsoleteNotam(this.selectedNotam.id)
            .subscribe(function (response) {
            notam.isObsolete = response;
            _this.selectedNotam.isObsolete = notam.isObsolete;
        }, function (error) {
            _this.toastr.error(error.message + " NotamID: " + _this.selectedNotam.notamId, "", { 'positionClass': 'toast-bottom-right' });
        });
        this.getIcaoFormat(notam);
    };
    NofNotamComponent.prototype.getIcaoFormat = function (notam) {
        var _this = this;
        this.startIcaoRequests();
        this.dataService.getNotamIcaoFormat(notam.id)
            .subscribe(function (icao) {
            _this.icaoFormat = icao;
            _this.endIcaoRequests();
        }, function (error) {
            _this.endIcaoRequests();
        });
    };
    NofNotamComponent.prototype.copyIcaoText = function () {
        this.copyToClipBoard(this.icaoFormat);
    };
    NofNotamComponent.prototype.copyToClipBoard = function (text) {
        var el = document.createElement('textarea');
        el.value = text;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };
    NofNotamComponent.prototype.getNotams = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runFilter(false, page, this.queryOptions.pageSize);
            }
            else {
                this.memStorageService.save(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY, this.queryOptions);
                this.dataService.getNofNotams(this.queryOptions)
                    .subscribe(function (notams) {
                    _this.processNotams(notams);
                });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    NofNotamComponent.prototype.processNotams = function (notams) {
        if (this.isFilterApplied) {
            this.filterPaging = notams.paging;
            this.filterTotalPages = notams.paging.totalPages;
        }
        this.paging = notams.paging;
        this.totalPages = notams.paging.totalPages;
        for (var iter = 0; iter < notams.data.length; iter++) {
            notams.data[iter].index = iter;
            notams.data[iter].typeStr = this.toNotamTypeStr(notams.data[iter].notamType);
        }
        var lastNotamSelected = this.memStorageService.get(this.memStorageService.NOTAM_ID_KEY);
        if (notams.data.length > 0) {
            this.notams = notams.data;
            var index = 0;
            if (lastNotamSelected) {
                index = this.notams.findIndex(function (x) { return x.id === lastNotamSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.notams[index].isSelected = true;
            this.selectedNotam = this.notams[index];
            this.loadingData = false;
            this.onRowSelected(0);
        }
        else {
            this.notams = [];
            this.selectedNotam = null;
            this.loadingData = false;
        }
    };
    NofNotamComponent.prototype.toNotamTypeStr = function (notamType) {
        switch (notamType) {
            case data_model_1.ProposalType.New: return 'N';
            case data_model_1.ProposalType.Replace: return 'R';
            case data_model_1.ProposalType.Cancel: return 'C';
            default: return '';
        }
    };
    NofNotamComponent.prototype.clearIcao = function () {
        this.icaoFormat = "";
    };
    NofNotamComponent.prototype.startIcaoRequests = function () {
        $("#icao-bulb").addClass("animated");
        $("table.icao").animate({
            opacity: 0.75
        }, 250, function () {
        });
    };
    NofNotamComponent.prototype.endIcaoRequests = function () {
        $("#icao-bulb").removeClass("animated");
        $("table.icao").animate({
            opacity: 1
        }, 250, function () {
            // Animation complete.
        });
    };
    NofNotamComponent.prototype.disposeMap = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    };
    NofNotamComponent.prototype.updateMap = function (notam, acceptSubjectLocation) {
        if (this.mapHealthy) {
            this.leafletmap.clearFeaturesOnLayers();
            this.leafletmap.addDoa(notam.effectAreaGeoJson, 'DOA');
        }
    };
    NofNotamComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    NofNotamComponent.prototype.initMap = function () {
        var winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;
        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    };
    NofNotamComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.mapHealthy = true;
                _this.initMap();
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        });
    };
    __decorate([
        core_1.ViewChild(nof_notam_filter_component_1.NofNotamFilterComponent),
        __metadata("design:type", nof_notam_filter_component_1.NofNotamFilterComponent)
    ], NofNotamComponent.prototype, "filter", void 0);
    __decorate([
        core_1.HostListener('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NofNotamComponent.prototype, "onKeyup", null);
    __decorate([
        debounceFunc(500),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NofNotamComponent.prototype, "getIcaoFormat", null);
    NofNotamComponent = __decorate([
        core_1.Component({
            selector: 'nof-notam',
            styleUrls: ['app/dashboard/nof/nof-notam.component.css'],
            templateUrl: '/app/dashboard/nof/nof-notam.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            location_service_1.LocationService,
            router_1.ActivatedRoute,
            angular_l10n_1.TranslationService])
    ], NofNotamComponent);
    return NofNotamComponent;
}());
exports.NofNotamComponent = NofNotamComponent;
function debounceFunc(delay) {
    if (delay === void 0) { delay = 300; }
    return function (target, propertyKey, descriptor) {
        var timeout = null;
        var original = descriptor.value;
        descriptor.value = function () {
            var _this = this;
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            clearTimeout(timeout);
            timeout = setTimeout(function () { return original.apply(_this, args); }, delay);
        };
        return descriptor;
    };
}
exports.debounceFunc = debounceFunc;
//# sourceMappingURL=nof-notam.component.js.map