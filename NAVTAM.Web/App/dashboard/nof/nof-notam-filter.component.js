"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofNotamFilterComponent = void 0;
var core_1 = require("@angular/core");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var moment = require("moment");
var data_model_1 = require("../shared/data.model");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var NofNotamFilterComponent = /** @class */ (function () {
    function NofNotamFilterComponent(dataService, memStorageService, translation, winRef, changeDetectionRef) {
        var _this = this;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.translation = translation;
        this.winRef = winRef;
        this.changeDetectionRef = changeDetectionRef;
        this.onFilterChanged = new core_1.EventEmitter();
        this.filterColumnStartValue = [];
        this.filterSlotsActivated = [];
        this.defaultFilterNames = ['F  -  1', 'F  -  2', 'F  -  3', 'F  -  4', 'F  -  5', 'F  -  6', 'F  -  7', 'F  -  8', 'F  -  9', 'F  -  10', 'F  -  11', 'F  -  12', 'F  -  13'];
        this.filterNames = [];
        this.isChangeFilterNameDisabled = true;
        this.parent = null;
        this.filters = [];
        this.slots = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
        this.filterErrorMessage = [];
        this.currentFilterName = "";
        this.currentFilterNameIndex = -1;
        var emptyFilter = {
            field: "",
            operator: data_model_1.FilterOperator.EQ,
            value: "",
            type: "text"
        };
        this.filterQueryOptions = {
            page: 1,
            pageSize: 10,
            sort: "_Published" //Default Published Descending
        };
        this.filters.push(emptyFilter);
        this.filterColumnsData = [
            { id: 'EMPTY', text: this.translation.translate('Filter.SelectFilterCriteria'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'NotamId', text: this.translation.translate('Filter.NotamId'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ReferredNotamId', text: this.translation.translate('Filter.ReferredNotamId'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ItemA', text: this.translation.translate('Filter.ItemA'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ItemE', text: this.translation.translate('Filter.ItemE'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Type', text: this.translation.translate('Filter.NotamType'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Series', text: this.translation.translate('Filter.Series'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Scope', text: this.translation.translate('Filter.Scope'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Code23', text: this.translation.translate('Filter.Code23'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Code45', text: this.translation.translate('Filter.Code45'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Traffic', text: this.translation.translate('Filter.Traffic'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Purpose', text: this.translation.translate('Filter.Purpose'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Radius', text: this.translation.translate('Filter.Radius'), type: "number", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'LowerLimit', text: this.translation.translate('Filter.LowerLimit'), type: "number", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'UpperLimit', text: this.translation.translate('Filter.UpperLimit'), type: "number", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Published', text: this.translation.translate('Filter.Published'), type: "datetime", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'StartActivity', text: this.translation.translate('Filter.StartActivity'), type: "datetime", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'EndValidity', text: this.translation.translate('Filter.EndValidity'), type: "datetime", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ModifiedByUsr', text: this.translation.translate('Filter.Approver'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Originator', text: this.translation.translate('Filter.Originator'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
        ];
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);
        this.filterOperatorsData = [
            { id: data_model_1.FilterOperator.EQ, text: this.translation.translate('Filter.Equal') },
            { id: data_model_1.FilterOperator.NEQ, text: this.translation.translate('Filter.NotEqual') },
            { id: data_model_1.FilterOperator.LT, text: this.translation.translate('Filter.LessThan') },
            { id: data_model_1.FilterOperator.GT, text: this.translation.translate('Filter.GreaterThan') },
            { id: data_model_1.FilterOperator.LTE, text: this.translation.translate('Filter.LessThanOrEqual') },
            { id: data_model_1.FilterOperator.GTE, text: this.translation.translate('Filter.GreaterThanOrEqual') },
            { id: data_model_1.FilterOperator.SW, text: this.translation.translate('Filter.StartWith') },
            { id: data_model_1.FilterOperator.EW, text: this.translation.translate('Filter.EndWith') },
            { id: data_model_1.FilterOperator.CT, text: this.translation.translate('Filter.Contains') },
        ];
        for (var index = 0; index < this.filterColumnsData.length; index++) {
            this.filterColumnsData[index].operators = this.getAllowFilterOperator(index);
            this.filterErrorMessage.push("");
        }
        this.defaultFilterNames.forEach(function (e) {
            _this.filterNames.push(e);
        });
    }
    NofNotamFilterComponent.prototype.ngOnChanges = function () {
    };
    NofNotamFilterComponent.prototype.ngOnInit = function () {
        this.getActiveUserQueryFilters();
        this.getUserQueryFilterNames();
    };
    NofNotamFilterComponent.prototype.ngAfterViewInit = function () {
        $(document).ready(function () {
            $(".select2-container").attr('style', 'width: 100% !important');
        });
    };
    NofNotamFilterComponent.prototype.changeFilterSort = function (sortField) {
        this.filterQueryOptions.sort = sortField;
    };
    NofNotamFilterComponent.prototype.onFilterColumnChanged = function (e, index) {
        this.setFilterStatus(e.value, index, "");
        this.filters[index].field = e.value;
        var operIndex = this.filterColumnsData.findIndex(function (x) { return x.id == e.value; });
        this.filterColumnsData[index].operators = this.getAllowFilterOperator(operIndex);
        if (this.filterColumnStartValue[index] != e.value) {
            setTimeout(function () {
                $(".select2-container").attr('style', 'width: 100% !important');
            }, 100);
        }
        else {
            $(".select2-container").attr('style', 'width: 100% !important');
        }
        this.filterColumnStartValue[index] = e.value;
    };
    NofNotamFilterComponent.prototype.getFilterOperators = function (fieldName) {
        var pos = this.filterColumnsData.findIndex(function (x) { return x.id === fieldName; });
        return this.getAllowFilterOperator(pos < 0 ? 0 : pos);
    };
    NofNotamFilterComponent.prototype.onFilterValueChanged = function (e, index) {
        this.filters[index].value = e.value;
        $(".select2-container").attr('style', 'width: 100% !important');
    };
    NofNotamFilterComponent.prototype.onFilterOperatorChanged = function (e, index) {
        this.filters[index].operator = e.value;
        this.filterColumnsData[index].currentOper = e.value;
        $(".select2-container").attr('style', 'width: 100% !important');
    };
    NofNotamFilterComponent.prototype.isFilterSlotActivated = function (slotNumber) {
        return this.filterSlotsActivated.findIndex(function (x) { return x == slotNumber; }) >= 0;
    };
    NofNotamFilterComponent.prototype.isFilterAddButtonEnabled = function (index) {
        if (index < this.filters.length - 1) {
            return false;
        }
        var f = this.filters[index];
        var columnData = this.filterColumnsData.find(function (x) { return x.id == f.field; });
        if (columnData === undefined) {
            this.updateFilterError(index, null); //ignore if not field selected.
            return false;
        }
        if (f.value === "") {
            this.updateFilterError(index, this.translation.translate('Filter.MatchValueRequired'));
            return false;
        }
        switch (columnData.type) {
            case "datetime":
                if (f.field === "StartActivity" && f.value === "IMMEDIATE") {
                    this.updateFilterError(index, null);
                }
                else if (f.field === "EndValidity" && f.value === "EndValidity") {
                    this.updateFilterError(index, null);
                }
                else {
                    this.filterErrorMessage[index] = this.translation.translate('Filter.InvalidDateFormat');
                    if ($.isNumeric(f.value) && f.value.length === 10) {
                        var momentDate = moment(f.value, "YYMMDDHHmm");
                        if (momentDate.isValid()) {
                            this.updateFilterError(index, null);
                        }
                    }
                }
                break;
            case "combobox":
                this.updateFilterError(index, null);
                break;
            case "number":
                if ($.isNumeric(f.value)) {
                    if (f.field !== "EMPTY") {
                        this.updateFilterError(index, null);
                    }
                }
                else {
                    this.updateFilterError(index, this.translation.translate('Filter.NumericValueRequired'));
                }
                break;
            default: //text
                if (f.value !== "" && f.field !== "EMPTY") {
                    this.updateFilterError(index, null);
                }
                break;
        }
        return this.filterErrorMessage[index] === null;
    };
    NofNotamFilterComponent.prototype.isApplyFilterButtonDisabled = function () {
        if (this.parent && !this.parent.isRunReportButtonEnabled()) {
            return true;
        }
        return !this.isFilterAddButtonEnabled(this.filters.length - 1);
    };
    NofNotamFilterComponent.prototype.addFilterCondition = function () {
        this.filters.push({
            field: "",
            operator: data_model_1.FilterOperator.EQ,
            value: "",
            type: "text"
        });
        var index = this.filters.length - 1;
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);
        this.filterColumnsData[index].currentOper = data_model_1.FilterOperator.EQ;
        setTimeout(function () {
            $(".select2-container").attr('style', 'width: 100% !important');
        }, 100);
    };
    NofNotamFilterComponent.prototype.removeFilterCondition = function (index) {
        this.filters.splice(index, 1);
        this.filterColumnStartValue.splice(index, 1);
        //let pos = this.filterColumnStartValue.length >= index ? this.filterColumnsData.findIndex(x => x.id === this.filterColumnStartValue[index]) : 0;
        this.filterColumnsData[index].currentOper = data_model_1.FilterOperator.EQ;
    };
    NofNotamFilterComponent.prototype.activateFilter = function (page) {
        page = page || 1;
        this.memStorageService.save(this.memStorageService.FILTER_APPLIED_KEY, true);
        this.memStorageService.save(this.memStorageService.NOTAM_FILTER_KEY, this.filters);
        this.runFilter(true, page, this.pageSize, "All");
    };
    NofNotamFilterComponent.prototype.loadFilterState = function () {
        this.filterQueryOptions = this.memStorageService.get(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY);
        this.updateFilters(this.memStorageService.get(this.memStorageService.NOTAM_FILTER_KEY));
    };
    NofNotamFilterComponent.prototype.runFilter = function (togglePannel, page, pageSize, filterBy) {
        var _this = this;
        this.filterQueryOptions.pageSize = pageSize || this.filterQueryOptions.pageSize;
        this.filterQueryOptions.page = page || this.filterQueryOptions.page;
        this.memStorageService.save(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY, this.filterQueryOptions);
        this.showProgressBar();
        this.dataService.filterNotams(this.filters, this.filterQueryOptions, filterBy)
            .subscribe(function (result) {
            if (togglePannel) {
                _this.toogleFilterPanel();
            }
            _this.hideProgressBar();
            _this.onFilterChanged.emit({
                isFilterApplied: true,
                data: result
            });
        }, function (error) {
            _this.hideProgressBar();
            _this.onFilterChanged.emit({
                error: error
            });
        });
    };
    NofNotamFilterComponent.prototype.runReportFilter = function (togglePannel, startdate, enddate, filterBy) {
        var _this = this;
        this.filterQueryOptions.pageSize = 100000000;
        this.filterQueryOptions.page = 1;
        this.memStorageService.save(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY, this.filterQueryOptions);
        this.showProgressBar();
        this.dataService.filterReportNotams(this.filters, this.filterQueryOptions, startdate, enddate, filterBy)
            .subscribe(function (result) {
            if (togglePannel) {
                _this.toogleFilterPanel();
            }
            _this.hideProgressBar();
            _this.onFilterChanged.emit({
                isFilterApplied: true,
                data: result
            });
        }, function (error) {
            _this.hideProgressBar();
            _this.onFilterChanged.emit({
                error: error
            });
        });
    };
    NofNotamFilterComponent.prototype.clearFilter = function (event, openPannel) {
        if (event) {
            event.preventDefault();
        }
        this.filters = [];
        this.addFilterCondition();
        //if (openPannel !== false) {
        //     this.toogleFilterPanel();
        //}
        this.memStorageService.remove(this.memStorageService.FILTER_APPLIED_KEY);
        this.memStorageService.remove(this.memStorageService.NOTAM_FILTER_KEY);
        for (var i = 0; i < this.filterColumnsData.length; i++) {
            this.filterColumnsData[i].operators = this.getAllowFilterOperator(i);
            this.filterColumnsData[i].currentOper = data_model_1.FilterOperator.EQ;
        }
        this.filterColumnStartValue = [];
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);
        this.onFilterChanged.emit({ isFilterApplied: false });
        this.filterSlotsActive = -1;
    };
    NofNotamFilterComponent.prototype.getUserQueryFilter = function (slotNumber) {
        var _this = this;
        this.dataService.getQueryFilter(slotNumber, this.filterType)
            .subscribe(function (result) {
            _this.updateFilters(result);
            _this.filterSlotsActive = slotNumber;
            setTimeout(function () {
                $(".select2-container").attr('style', 'width: 100% !important');
            }, 100);
        }, function (error) {
            //TODO: ERROR
        });
    };
    NofNotamFilterComponent.prototype.getUserQueryFilterNames = function () {
        var self = this;
        this.dataService.getQueryFilterNames(this.winRef.appConfig.usrName)
            .subscribe(function (filters) {
            for (var i = 0; i < filters.length; i++) {
                var filter = filters[i];
                self.filterNames[filter.slotNumber - 1] = filter.filterName;
            }
        }, function (error) {
            //TODO: ERROR
        });
    };
    NofNotamFilterComponent.prototype.saveFilterName = function () {
        var _this = this;
        if (this.currentFilterNameIndex >= 0 && this.currentFilterName) {
            this.dataService.saveQueryFilterName(this.winRef.appConfig.usrName, this.currentFilterNameIndex + 1, this.currentFilterName)
                .subscribe(function (result) {
                _this.filterNames[_this.currentFilterNameIndex] = _this.currentFilterName;
                _this.isChangeFilterNameDisabled = true;
                _this.currentFilterName = "";
                _this.currentFilterNameIndex = -1;
            }, function (error) {
                //TODO: ERROR
            });
        }
    };
    NofNotamFilterComponent.prototype.getActiveUserQueryFilters = function () {
        var _this = this;
        this.dataService.getActiveQueryFilters(this.filterType)
            .subscribe(function (result) {
            _this.filterSlotsActivated = result;
        }, function (error) {
            //TODO: ERROR
        });
    };
    NofNotamFilterComponent.prototype.saveUserQueryFilter = function (event, slotNumber) {
        event.preventDefault();
        var self = this;
        this.dataService.saveQueryFilter(slotNumber, this.filters, this.filterType)
            .subscribe(function (result) {
            self.filterSlotsActive = slotNumber;
            if (self.filterSlotsActivated.findIndex(function (x) { return x == slotNumber; }) < 0) {
                self.filterSlotsActivated.push(slotNumber);
            }
        }, function (error) {
            //TODO: ERROR
        });
    };
    NofNotamFilterComponent.prototype.deleteUserQueryFilter = function (event, slotNumber) {
        var _this = this;
        event.preventDefault();
        var self = this;
        var filterExist = this.filterSlotsActivated.find(function (e) { return e === slotNumber; });
        if (filterExist) {
            this.dataService.deleteQueryFilter(slotNumber, this.filterType)
                .subscribe(function () {
                self.filterSlotsActive = -1;
                var index = self.filterSlotsActivated.findIndex(function (x) { return x == slotNumber; });
                if (index >= 0) {
                    self.filterSlotsActivated.splice(index, 1);
                    self.filterNames[slotNumber - 1] = _this.defaultFilterNames[slotNumber - 1];
                }
            }, function (error) {
                //TODO: ERROR
            });
        }
    };
    NofNotamFilterComponent.prototype.changeFilterName = function (event, index) {
        event.preventDefault();
        this.currentFilterName = this.filterNames[index];
        this.currentFilterNameIndex = index;
        this.isChangeFilterNameDisabled = false;
    };
    NofNotamFilterComponent.prototype.updateFilterError = function (index, errorMessage) {
        if (this.filterErrorMessage[index] !== errorMessage) {
            this.filterErrorMessage[index] = errorMessage;
        }
    };
    NofNotamFilterComponent.prototype.updateFilters = function (filters) {
        this.filterColumnStartValue = [];
        this.filters = filters;
        for (var i = 0; i < this.filters.length; i++) {
            this.setFilterStatus(filters[i].field, i, filters[i].value);
            this.filterColumnStartValue.push(filters[i].field);
            this.filterColumnsData[i].currentOper = this.filters[i].operator;
        }
    };
    NofNotamFilterComponent.prototype.setFilterStatus = function (field, index, currentValue) {
        var filterColumnsDataPos = this.filterColumnsData.findIndex(function (x) { return x.id == field; });
        switch (field) {
            case "EMPTY":
            case "NotamId":
            case "ReferredNotamId":
            case "ItemA":
            case "ItemE":
            case "Originator":
            case "Operator":
            case "ModifiedByUsr":
            case "Code23":
            case "Code45":
            case "Code45":
                this.setNonComboboxFilterData(currentValue, index, filterColumnsDataPos, "text");
                break;
            case "Radius":
            case "LowerLimit":
            case "UpperLimit":
                this.setNonComboboxFilterData(currentValue, index, filterColumnsDataPos, "number");
                break;
            case "Published":
            case "StartActivity":
            case "EndValidity":
                this.setNonComboboxFilterData(currentValue, index, filterColumnsDataPos, "datetime");
                break;
            case "Type":
                this.setComboboxFilterData(["N", "C", "R"], index, filterColumnsDataPos, currentValue);
                break;
            case "Traffic":
                this.setComboboxFilterData(["I", "V", "IV"], index, filterColumnsDataPos, currentValue);
                break;
            case "Scope":
                this.setComboboxFilterData(["A", "AE", "E", "K", "W"], index, filterColumnsDataPos, currentValue);
                break;
            case "Purpose":
                this.setComboboxFilterData(["B", "BO", "K", "M", "NBO"], index, filterColumnsDataPos, currentValue);
                break;
            case "Series":
                this.setComboboxFilterData(["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"], index, filterColumnsDataPos, currentValue);
                break;
        }
    };
    NofNotamFilterComponent.prototype.setComboboxFilterData = function (data, index, pos, currentValue) {
        this.filterColumnsData[pos].type = "combobox";
        this.filterColumnsData[pos].operators = this.getAllowFilterOperator(pos);
        this.filterColumnsData[pos].values = data;
        currentValue = currentValue !== "" ? currentValue : this.filterColumnsData[pos].values[0];
        this.filterColumnsData[pos].currentValue = currentValue;
        this.filters[index].type = "combobox";
        this.filters[index].valueList = data;
        this.filters[index].value = currentValue;
    };
    NofNotamFilterComponent.prototype.setNonComboboxFilterData = function (currentValue, index, pos, type) {
        this.filterColumnsData[pos].type = type;
        this.filterColumnsData[pos].operators = this.getAllowFilterOperator(pos);
        this.filterColumnsData[pos].currentValue = currentValue;
        this.filters[index].type = type;
        //this.filters[index].valueList = data;
        this.filters[index].value = currentValue;
    };
    NofNotamFilterComponent.prototype.getAllowFilterOperator = function (index) {
        var filterColumnsData = [];
        var filterName = this.filterColumnsData[index].id;
        switch (filterName) {
            case "NotamId":
            case "ReferredNotamId":
            case "ItemA":
            case "ItemE":
            case "Code23":
            case "Code45":
                filterColumnsData = this.addTextFieldFilters(filterColumnsData);
                break;
            case "StartActivity":
            case "EndValidity":
            case "Published":
                filterColumnsData = this.addDateFieldFilters(filterColumnsData);
                break;
            case "Series":
            case "Type":
                filterColumnsData = this.addBooleanFieldFilters(filterColumnsData);
                break;
            case "Traffic":
            case "Purpose":
            case "Scope":
                filterColumnsData = this.addBooleanExFieldFilters(filterColumnsData);
                break;
            case "Radius":
            case "LowerLimit":
            case "UpperLimit":
                filterColumnsData = this.addNumberFieldFilters(filterColumnsData);
                break;
            case "Operator":
            case "Originator":
            case "ModifiedByUsr":
                filterColumnsData = this.addTextFieldFilters(filterColumnsData);
                break;
        }
        return filterColumnsData;
    };
    NofNotamFilterComponent.prototype.addAllFilters = function (filterColumnsData) {
        for (var i = 0; i < this.filterOperatorsData.length; i++) {
            filterColumnsData.push(this.filterOperatorsData[i]);
        }
        return filterColumnsData;
    };
    NofNotamFilterComponent.prototype.addTextFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[6]);
        filterColumnsData.push(this.filterOperatorsData[7]);
        filterColumnsData.push(this.filterOperatorsData[8]);
        return filterColumnsData;
    };
    NofNotamFilterComponent.prototype.addBooleanFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        return filterColumnsData;
    };
    NofNotamFilterComponent.prototype.addBooleanExFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[8]);
        return filterColumnsData;
    };
    NofNotamFilterComponent.prototype.addDateFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[2]);
        filterColumnsData.push(this.filterOperatorsData[3]);
        filterColumnsData.push(this.filterOperatorsData[4]);
        filterColumnsData.push(this.filterOperatorsData[5]);
        return filterColumnsData;
    };
    NofNotamFilterComponent.prototype.addNumberFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[2]);
        filterColumnsData.push(this.filterOperatorsData[3]);
        filterColumnsData.push(this.filterOperatorsData[4]);
        filterColumnsData.push(this.filterOperatorsData[5]);
        return filterColumnsData;
    };
    NofNotamFilterComponent.prototype.showProgressBar = function () {
        if (this.parent) {
            this.parent.loadingData = true;
        }
    };
    NofNotamFilterComponent.prototype.hideProgressBar = function () {
        if (this.parent) {
            this.parent.loadingData = false;
        }
    };
    NofNotamFilterComponent.prototype.toogleFilterPanel = function () {
        $(".panel-toggle").toggleClass("closed").parents(".panel:first").find(".panel-content").slideToggle();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], NofNotamFilterComponent.prototype, "pageSize", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], NofNotamFilterComponent.prototype, "filterType", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], NofNotamFilterComponent.prototype, "onFilterChanged", void 0);
    NofNotamFilterComponent = __decorate([
        core_1.Component({
            selector: 'notam-filter',
            templateUrl: '/app/dashboard/nof/nof-notam-filter.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            angular_l10n_1.TranslationService,
            windowRef_service_1.WindowRef,
            core_1.ChangeDetectorRef])
    ], NofNotamFilterComponent);
    return NofNotamFilterComponent;
}());
exports.NofNotamFilterComponent = NofNotamFilterComponent;
//# sourceMappingURL=nof-notam-filter.component.js.map