"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofPickComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var toastr_service_1 = require("./../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var nsd_form_component_1 = require("../nsds/nsd-form.component");
//import { SignalR, SignalRConnection, BroadcastEventListener, ConnectionStatus } from 'ng2-signalr';
var NofPickComponent = /** @class */ (function () {
    function NofPickComponent(toastr, dataService, activatedRoute, 
    //private _signalR: SignalR
    router, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.translation = translation;
        this.model = null;
        this.isOwnershipActionTaken = false;
        this.isOwnershipActionLost = false;
        switch (activatedRoute.snapshot.data[0]['type']) {
            case 'draft':
            case 'clone':
                this.title = this.translation.translate('Common.Edit'); //"Edit";
                this.faIcon = "icon-plus";
                break;
            case 'replace':
                this.title = this.translation.translate('Common.Replace'); //"Replace";
                this.faIcon = "fa fa-code-fork";
                break;
            case 'cancel':
                this.title = this.translation.translate('Common.Cancel'); //"Cancel";
                this.faIcon = "fa fa-hand-paper-o";
                break;
            default:
                this.title = this.translation.translate('Common.Edit'); //"Edit";
                this.faIcon = "icon-plus";
                break;
        }
    }
    Object.defineProperty(NofPickComponent.prototype, "form", {
        set: function (v) {
            this.nsdForm = v;
        },
        enumerable: false,
        configurable: true
    });
    NofPickComponent.prototype.ngOnInit = function () {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
        if (this.model.status === data_model_1.ProposalStatus.Taken) {
            this.proposalOwner = this.model.modifiedByUsr;
            var self_1 = this;
            var el = $('#modal-takeownership');
            el.modal({});
            el.on('hide.bs.modal', function (e) {
                window.setTimeout(function () {
                    if (!self_1.isOwnershipActionTaken) {
                        self_1.router.navigate(['/dashboard']);
                    }
                }, 500);
            });
        }
    };
    NofPickComponent.prototype.ngOnDestroy = function () {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    };
    Object.defineProperty(NofPickComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    NofPickComponent.prototype.takeProposalOwnership = function () {
        var _this = this;
        this.isOwnershipActionTaken = true;
        this.dataService.getProposalModelEnforced(this.model.proposalId)
            .subscribe(function (data) {
            _this.model = data;
        }, function (error) {
            var msg = _this.translation.translate('NofPick.TakeOwnershipError') + error.message;
            _this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.backToDashboard();
        });
    };
    NofPickComponent.prototype.showOwnershipActionLostNotification = function (data) {
        var userId = window['app']['usrId'];
        if (data.userId !== userId && this.model.proposalId === data.id) {
            var self_2 = this;
            var el = $('#modal-ownershiptaken');
            el.modal({});
            el.on('hide.bs.modal', function (e) {
                window.setTimeout(function () {
                    if (!self_2.isOwnershipActionLost) {
                        self_2.router.navigate(['/dashboard']);
                    }
                }, 500);
            });
        }
    };
    NofPickComponent.prototype.backToDashboard = function () {
        this.proposalOwner = null;
    };
    __decorate([
        core_1.ViewChild(nsd_form_component_1.NsdFormComponent),
        __metadata("design:type", nsd_form_component_1.NsdFormComponent),
        __metadata("design:paramtypes", [nsd_form_component_1.NsdFormComponent])
    ], NofPickComponent.prototype, "form", null);
    NofPickComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-pick.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            router_1.ActivatedRoute,
            router_1.Router,
            angular_l10n_1.TranslationService])
    ], NofPickComponent);
    return NofPickComponent;
}());
exports.NofPickComponent = NofPickComponent;
//# sourceMappingURL=nof-pick.component.js.map