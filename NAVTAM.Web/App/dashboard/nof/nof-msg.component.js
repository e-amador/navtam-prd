"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofMsgComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_model_1 = require("../shared/data.model");
var MessageType = /** @class */ (function () {
    function MessageType() {
    }
    MessageType.Notification = 0;
    MessageType.Error = 1;
    return MessageType;
}());
var MessagePanel = /** @class */ (function () {
    function MessagePanel() {
    }
    MessagePanel.Read = 0;
    MessagePanel.Compose = 1;
    return MessagePanel;
}());
var NofMsgComponent = /** @class */ (function () {
    function NofMsgComponent(toastr, dataService, memStorageService, router, winRef, route, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.translation = translation;
        this.onUpdateCounter = new core_1.EventEmitter();
        this.notify = new core_1.EventEmitter();
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterPaging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterTotalPages = 0;
        this.totalPages = 0;
        this.loadingData = false;
        this.currentMessageStatusView = data_model_1.MessageStatus.None;
        this.savedMessageStatusView = data_model_1.MessageStatus.None;
        this.inbound = true;
        this.unreaded = 0;
        this.parked = 0;
        this.messagePanel = MessagePanel.Read;
        this.selectedSubscription = null;
        this.searchFilter = {
            clientTypeStr: "",
            client: "",
            address: ""
        };
        this.addresses = [];
        this.recipients = "";
        this.messageBody = "";
        this.addressIndexSelected = -1;
        this.selectedPriorityCodeId = "-1";
        this.selectedTemplateId = "-1";
        this.userTemplates = [];
        this.maxNumberOfTemplates = 4;
        this.tableHeaders = {
            sorting: [
                { columnName: 'PriorityCode', direction: 0 },
                { columnName: 'Originator', direction: 1 },
                { columnName: 'Created', direction: 0 },
            ],
            itemsPerPage: [
                { id: '15', text: '15' },
                { id: '30', text: '30' },
                { id: '75', text: '75' },
                { id: '100', text: '100' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[1].columnName //Default ClientType
        };
    }
    NofMsgComponent.prototype.onKeyup = function ($event) {
        switch ($event.code) {
            case "ArrowUp": //NAVIGATE TABLE ROW UP
                if (this.selectedMessage.index > 0) {
                    this.onRowSelected(this.selectedMessage.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedMessage.index < this.messages.length - 1) {
                    this.onRowSelected(this.selectedMessage.index + 1);
                }
                break;
        }
    };
    NofMsgComponent.prototype.ngOnInit = function () {
        this.loadUserMessageTemplates();
        this.prioriTyCodeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('')
            },
            width: "100%"
        };
        this.setPriorityCodes();
        this.templateOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('MessageQueue.SelectTemplate')
            },
            width: "100%"
        };
        this.loadTemplates();
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            this.queryOptions = queryOptions;
        }
        this.getMessages(this.queryOptions.page);
        this.getParkedMessageCount();
    };
    NofMsgComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getMessages(this.queryOptions.page);
    };
    NofMsgComponent.prototype.changeMessageView = function (status, inbound, confirmLeave) {
        if (inbound === void 0) { inbound = true; }
        if (confirmLeave === void 0) { confirmLeave = true; }
        if (this.messagePanel === MessagePanel.Compose) {
            if (confirmLeave) {
                this.savedMessageStatusView = status;
                if (!this.disableSendButton()) {
                    var elm = $('#modal-discard-message');
                    var self_1 = this;
                    elm.modal({});
                    return;
                }
            }
            this.toggleComposeMessage(MessagePanel.Read, "");
        }
        this.currentMessageStatusView = status;
        this.inbound = inbound;
        this.getMessages(1);
    };
    NofMsgComponent.prototype.discardMessage = function () {
        this.changeMessageView(this.savedMessageStatusView, true, false);
    };
    NofMsgComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    NofMsgComponent.prototype.rowColor = function (message) {
        switch (message.messageType) {
            case MessageType.Notification: return 'bg-submitted';
            case MessageType.Error: return 'bg-cancel';
            default: return '';
        }
    };
    NofMsgComponent.prototype.toggleComposeMessage = function (panel, recipients) {
        this.recipients = recipients || "";
        this.messageBody = "";
        this.selectedPriorityCodeId = this.priorityCodes[0].id;
        this.messagePanel = panel;
    };
    NofMsgComponent.prototype.replyTo = function () {
        this.toggleComposeMessage(MessagePanel.Compose, this.selectedMessage.clientAddress);
    };
    NofMsgComponent.prototype.updateMessageStatus = function (status) {
        var _this = this;
        if (this.selectedMessage) {
            this.selectedMessage.prevStatus = this.selectedMessage.status;
            switch (this.currentMessageStatusView) {
                case data_model_1.MessageStatus.Important:
                    this.selectedMessage.status = status === data_model_1.MessageStatus.Important ? data_model_1.MessageStatus.None : this.selectedMessage.status = status;
                    break;
                case data_model_1.MessageStatus.Parked:
                    this.selectedMessage.status = status === data_model_1.MessageStatus.Parked ? data_model_1.MessageStatus.None : this.selectedMessage.status = status;
                    break;
                case data_model_1.MessageStatus.Deleted:
                    this.selectedMessage.status = status === data_model_1.MessageStatus.Deleted ? data_model_1.MessageStatus.None : this.selectedMessage.status = status;
                    break;
                default:
                    this.selectedMessage.status = status;
            }
            this.dataService.updateMessage(this.selectedMessage)
                .subscribe(function (result) {
                if (result.anyError) {
                    _this.selectedMessage.status = _this.selectedMessage.prevStatus;
                    var error = "The message was changed by " + result.lastOwner + " to " + result.status;
                    _this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                    return;
                }
                if (_this.messages.length === 1) {
                    _this.paging.currentPage = _this.paging.currentPage > 1 ? _this.paging.currentPage - 1 : 1;
                }
                _this.getMessages(_this.paging.currentPage);
                var msg = "";
                switch (_this.selectedMessage.status) {
                    case data_model_1.MessageStatus.Parked:
                        msg = _this.translation.translate('MessageQueue.SuccessMessageArchived');
                        break;
                    case data_model_1.MessageStatus.Important:
                        msg = _this.translation.translate('MessageQueue.SuccessMessageImportant');
                        break;
                    case data_model_1.MessageStatus.Deleted:
                        msg = _this.translation.translate('MessageQueue.SuccessMessageDeleted');
                        break;
                    case data_model_1.MessageStatus.None:
                        if (status === data_model_1.MessageStatus.Parked) {
                            msg = _this.translation.translate('MessageQueue.SuccessMessageUnarchived');
                        }
                        else if (status === data_model_1.MessageStatus.Important) {
                            msg = _this.translation.translate('MessageQueue.SuccessMessageUnimportant');
                        }
                        else if (status === data_model_1.MessageStatus.Deleted) {
                            msg = _this.translation.translate('MessageQueue.SuccessMessageUndeleted');
                        }
                        break;
                }
                //msg = this.translation.translate('MessageQueue.SuccessMessageArchived');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = "";
                switch (status) {
                    case data_model_1.MessageStatus.Parked:
                        msg = _this.translation.translate('MessageQueue.FailureMessageArchived');
                        ;
                        break;
                    case data_model_1.MessageStatus.Important:
                        msg = _this.translation.translate('MessageQueue.FailureMessageImportant');
                        break;
                    case data_model_1.MessageStatus.Deleted:
                        msg = _this.translation.translate('MessageQueue.FailureMessageDeleted');
                        break;
                    case data_model_1.MessageStatus.None:
                        msg = _this.translation.translate('MessageQueue.FailureMessageStatusChange');
                        break;
                }
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NofMsgComponent.prototype.confirmDelete = function () {
        if (this.currentMessageStatusView === data_model_1.MessageStatus.Deleted) {
            this.updateMessageStatus(data_model_1.MessageStatus.Deleted);
        }
        else {
            $('#modal-delete').modal({});
        }
    };
    NofMsgComponent.prototype.openSearchDialog = function () {
        var _this = this;
        this.notify.emit('disable-tabs');
        this.subscriptions = [];
        this.dataService.getNdsClientAddresses()
            .subscribe(function (subscriptions) {
            _this.selectedSubscription = null;
            for (var iter = 0; iter < subscriptions.length; iter++) {
                var subscription = subscriptions[iter];
                subscription.index = iter;
                subscription.clientTypeStr = _this.toClientTypeStr(subscription.clientType);
                subscription.isSelected = false;
                _this.subscriptions.push(subscription);
            }
            if (subscriptions.length > 0) {
                _this.selectedSubscription = _this.subscriptions[0];
                _this.selectedSubscription.isSelected = true;
            }
            ;
        }, function (error) {
            //TODO
        });
        var elm = $('#modal-search');
        var self = this;
        elm.modal({});
        elm.on('hidden.bs.modal', function (e) {
            self.closeSearchDialog();
        });
    };
    NofMsgComponent.prototype.closeSearchDialog = function () {
        this.addresses = [];
        this.addressIndexSelected = -1;
        this.notify.emit('enable-tabs');
    };
    NofMsgComponent.prototype.deleteMessage = function () {
        var _this = this;
        if (this.selectedMessage) {
            this.dataService.deleteIncomingMessage(this.selectedMessage.id)
                .subscribe(function (result) {
                if (_this.messages.length === 1) {
                    _this.paging.currentPage = _this.paging.currentPage > 1 ? _this.paging.currentPage - 1 : 1;
                }
                _this.getMessages(_this.paging.currentPage);
                var msg = _this.translation.translate('MessageQueue.SuccessMessageDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('MessageQueue.FailureMessageDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NofMsgComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getMessages(1);
    };
    NofMsgComponent.prototype.toggleReadStatus = function () {
        var _this = this;
        this.selectedMessage.read = !this.selectedMessage.read;
        this.dataService.updateMessage(this.selectedMessage)
            .subscribe(function (result) {
            if (_this.currentMessageStatusView === data_model_1.MessageStatus.None) {
                _this.unreaded = 0;
                for (var iter = 0; iter < _this.messages.length; iter++) {
                    if (!_this.messages[iter].read) {
                        _this.unreaded++;
                    }
                }
            }
        }, function (error) {
            _this.selectedMessage.read = !_this.selectedMessage.read;
        });
    };
    NofMsgComponent.prototype.onRowSelected = function (index) {
        var _this = this;
        if (this.selectedMessage) {
            this.selectedMessage.isSelected = false;
        }
        this.selectedMessage = this.messages[index];
        this.selectedMessage.isSelected = true;
        this.memStorageService.save(this.memStorageService.MESSAGE_QUEUE_ID_KEY, this.selectedMessage.id);
        if (!this.selectedMessage.read) {
            this.selectedMessage.read = true;
            this.dataService.updateMessage(this.selectedMessage)
                .subscribe(function (result) {
                if (_this.currentMessageStatusView === data_model_1.MessageStatus.None) {
                    _this.unreaded = 0;
                    for (var iter = 0; iter < _this.messages.length; iter++) {
                        if (!_this.messages[iter].read) {
                            _this.unreaded++;
                        }
                    }
                }
            }, function (error) {
                _this.selectedMessage.read = false;
            });
        }
    };
    NofMsgComponent.prototype.onPageChange = function (page) {
        this.getMessages(page);
    };
    NofMsgComponent.prototype.selectSubscription = function (client) {
        this.selectedSubscription.isSelected = false;
        this.selectedSubscription = client;
        this.selectedSubscription.isSelected = true;
    };
    NofMsgComponent.prototype.addAddress = function () {
        this.addresses.push(this.selectedSubscription.address);
    };
    NofMsgComponent.prototype.selectAddress = function (index) {
        this.addressIndexSelected = this.addressIndexSelected !== index ? index : -1;
    };
    NofMsgComponent.prototype.deleteAddress = function (index) {
        if (this.addressIndexSelected >= 0) {
            this.addresses.splice(this.addressIndexSelected, 1);
        }
    };
    NofMsgComponent.prototype.appendAddressToRecipientList = function () {
        var len = this.recipients.length;
        var addresses = len == 0 ? "" : (this.recipients[len - 1] === ";" ? "" : ";");
        for (var i = 0; i < this.addresses.length; i++) {
            addresses += this.addresses[i] + ";";
        }
        this.recipients += addresses;
    };
    NofMsgComponent.prototype.disableSendButton = function () {
        if (!this.recipients) {
            return true;
        }
        if (!(this.recipients.length > 0 && this.messageBody.length > 0)) {
            return true;
        }
        var disabled = false;
        var addresses = this.recipients.split(";");
        var reg = /^[a-z]+$/i;
        for (var i = 0; i < addresses.length; i++) {
            var address = addresses[i];
            if (address.length === 0) {
                continue;
            }
            if (address.length !== 8) {
                disabled = true;
                break;
            }
            if (!reg.test(address)) {
                disabled = true;
                break;
            }
        }
        return disabled;
    };
    NofMsgComponent.prototype.sendMessage = function () {
        var _this = this;
        var queueMessage = this.prepareQueueMessagePayload();
        this.dataService.sendQueueMessage(queueMessage)
            .subscribe(function (data) {
            var msg = _this.translation.translate('MessageQueue.SuccessMessageSent');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.toggleComposeMessage(MessagePanel.Read, "");
        }, function (error) {
            var msg = _this.translation.translate('MessageQueue.FailureMessageSent');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgComponent.prototype.onPriorityCodeChanged = function (data) {
        this.selectedPriorityCodeId = data.value;
    };
    NofMsgComponent.prototype.onTemplateChanged = function (data) {
        var _this = this;
        this.selectedTemplateId = data.value;
        this.dataService.getMessageTemplate(this.selectedTemplateId)
            .subscribe(function (template) {
            _this.messageBody = template.body;
        }, function (error) {
            var msg = _this.translation.translate('MessageQueue.FailureTemplateLoad');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgComponent.prototype.getTemplateBySlot = function (slotNumber, event) {
        if (event === void 0) { event = null; }
        if (this.userTemplates[slotNumber - 1] !== null) {
            return "" + this.userTemplates[slotNumber - 1].templateName;
        }
        return "Preference " + slotNumber;
    };
    NofMsgComponent.prototype.setTemplate = function (slotNumber) {
        //    let userTemplate: IUserMessageTemplate = {
        //        slotNumber: slotNumber,
        //        templateId: this.selectedTemplateId,
        //        username: this.winRef.appConfig.usrName
        //    };  
        //    this.dataService.saveUserTemplate(userTemplate)
        //        .subscribe(template => {
        //            this.messageBody = template.body
        //            this.loadUserMessageTemplates();
        //        }, error => {
        //            let msg = this.translation.translate('MessageQueue.FailureTemplateLoad');
        //            this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        //        });
    };
    NofMsgComponent.prototype.composeMessageUsingTemplate = function (slotNumber, event) {
        this.onTemplateChanged({ value: this.userTemplates[slotNumber - 1].templateId });
        this.toggleComposeMessage(MessagePanel.Compose, this.selectedMessage.clientAddress);
        event.stopPropagation();
        return false;
    };
    NofMsgComponent.prototype.loadUserMessageTemplates = function () {
        //var userName = this.winRef.appConfig.usrName;
        //this.userTemplates = [];
        //for (let i = 0; i < this.maxNumberOfTemplates; i++) {
        //    this.userTemplates.push(null);
        //};
        //this.dataService.getUserTemplates(userName)
        //    .subscribe(templates => {
        //        for (let i = 0; i < this.maxNumberOfTemplates; i++) {
        //            let template = templates[i];
        //            if (templates.length - 1 >= i) {
        //                this.userTemplates[template.slotNumber - 1] = template;
        //            }
        //        }
        //    }, error => {
        //        //TODO
        //    });
    };
    NofMsgComponent.prototype.getMessages = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY, this.queryOptions);
            if (this.inbound) {
                this.dataService.getIncomingMessages(this.queryOptions, this.currentMessageStatusView)
                    .subscribe(function (messages) {
                    _this.processMessages(messages);
                });
            }
            else {
                this.dataService.getOutgoingMessages(this.queryOptions, this.currentMessageStatusView)
                    .subscribe(function (messages) {
                    _this.processMessages(messages);
                });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    NofMsgComponent.prototype.processMessages = function (messages) {
        this.paging = messages.paging;
        this.totalPages = messages.paging.totalPages;
        if (this.inbound && this.currentMessageStatusView === data_model_1.MessageStatus.None) {
            this.unreaded = 0;
        }
        if (this.inbound && this.currentMessageStatusView === data_model_1.MessageStatus.Parked) {
            this.parked = 0;
        }
        for (var iter = 0; iter < messages.data.length; iter++) {
            var message = messages.data[iter];
            message.index = iter;
            message.clientTypeStr = this.toClientTypeStr(messages.data[iter].clientType);
            if (this.inbound && this.currentMessageStatusView === data_model_1.MessageStatus.None && !message.read) {
                this.unreaded++;
            }
            if (this.inbound && this.currentMessageStatusView === data_model_1.MessageStatus.Parked) {
                this.parked++;
            }
        }
        var lastMessageSelected = this.memStorageService.get(this.memStorageService.NSD_CLIENT_ID_KEY);
        if (messages.data.length > 0) {
            this.messages = messages.data;
            var index = 0;
            if (lastMessageSelected) {
                index = this.messages.findIndex(function (x) { return x.id === lastMessageSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.messages[index].isSelected = true;
            this.selectedMessage = this.messages[index];
            this.loadingData = false;
            this.onRowSelected(index);
        }
        else {
            this.messages = [];
            this.selectedMessage = null;
            this.loadingData = false;
        }
    };
    NofMsgComponent.prototype.setPriorityCodes = function () {
        this.priorityCodes = [
            { id: "GG", text: "GG" },
            { id: "SS", text: "SS" },
            { id: "DD", text: "DD" },
            { id: "FF", text: "FF" },
            { id: "KK", text: "KK" }
        ];
        this.selectedPriorityCodeId = this.priorityCodes[0].id;
    };
    NofMsgComponent.prototype.loadTemplates = function () {
        var _this = this;
        this.dataService.getMessageTemplates()
            .subscribe(function (template) {
            _this.templates = template.map(function (r) {
                return {
                    id: r.id,
                    text: r.name
                };
            });
            //this.templates.splice(0, 0, this.templates.splice(0, 1)[0]);
            //this.selectedTemplateId = "-1";
        });
    };
    NofMsgComponent.prototype.getParkedMessageCount = function () {
        var _this = this;
        try {
            this.dataService.getTotalParkedMessages()
                .subscribe(function (count) {
                _this.parked = count;
            });
        }
        catch (e) {
        }
    };
    NofMsgComponent.prototype.prepareQueueMessagePayload = function () {
        var now = new Date();
        var messageQueue = {
            messageType: MessageType.Notification,
            clientType: data_model_1.ClientType.AFTN,
            priorityCode: this.selectedPriorityCodeId,
            inbound: false,
            recipients: this.recipients,
            body: this.messageBody,
            clientName: this.winRef.appConfig.usrName,
            clientAddress: "",
            created: now,
            locked: false,
            read: true,
            status: data_model_1.MessageStatus.None
        };
        return messageQueue;
    };
    NofMsgComponent.prototype.toClientTypeStr = function (clientType) {
        switch (clientType) {
            case data_model_1.ClientType.AFTN: return 'AFTN';
            case data_model_1.ClientType.SWIM: return 'SWIM';
            case data_model_1.ClientType.REST: return 'REST';
            default: return '';
        }
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], NofMsgComponent.prototype, "onUpdateCounter", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], NofMsgComponent.prototype, "notify", void 0);
    __decorate([
        core_1.HostListener('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NofMsgComponent.prototype, "onKeyup", null);
    NofMsgComponent = __decorate([
        core_1.Component({
            selector: 'nof-message',
            templateUrl: '/app/dashboard/nof/nof-msg.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.TranslationService])
    ], NofMsgComponent);
    return NofMsgComponent;
}());
exports.NofMsgComponent = NofMsgComponent;
//# sourceMappingURL=nof-msg.component.js.map