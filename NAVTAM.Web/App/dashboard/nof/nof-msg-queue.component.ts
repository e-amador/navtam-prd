﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'

import { TranslationService } from 'angular-l10n';


import { Select2OptionData } from 'ng2-select2';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import {
    IMessageQueuePartial,
    ClientType,
    MessageStatus,
    IQueryOptions,
    IPaging,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

const syncFrecuency: number = 10; //seconds

class MessageType {
    static Notification = 0
    static Error = 1;
}

class MessageView {
    static Received = 0;
    static Sent = 1;
    static Parked = 2;
}

@Component({
    selector: 'nof-msg-queue',
    styleUrls: ['app/dashboard/nof/nof-msg-queue.component.css'],
    templateUrl: '/app/dashboard/nof/nof-msg-queue.component.html'
})
export class NofMsgQueueComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('msgFilter') filter;
    @Input('count') messageCount: number;
    @Output() onUpdateCounter: EventEmitter<string> = new EventEmitter<string>()

    messages: IMessageQueuePartial[];
    queryOptions: IQueryOptions;
    selectedMessage: IMessageQueuePartial;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterPaging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterTotalPages: number = 0;
    totalPages: number = 0;
    unread: number = 0;
    parked: number = 0;

    messageViewActive: MessageView;
    currentMessageStatusView: MessageStatus = MessageStatus.None;
    currentPanelSelected: number;

    itemsPerPageData: Array<any>;
    loadingData: boolean = false;
    dataReceived: boolean = false;
    tableHeaders: ITableHeaders;
    refreshHandler: any = null;
    messageOwner: string = "";


    public selectedMessageFilterOptionId: string = "A";
    public messageFilter: Array<Select2OptionData>;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        private translation: TranslationService) {

        this.tableHeaders = {
            sorting:
            [
                { columnName: 'ClientName', direction: 0 },
                { columnName: 'ClientAddress', direction: 0 },
                { columnName: 'Recipients', direction: 0 },
                { columnName: 'LastOwner', direction: 0 },
                { columnName: 'PriorityCode', direction: 0 },
                { columnName: 'Created', direction: -1 },
                { columnName: 'Status', direction: 0 },
            ],
            itemsPerPage:
            [
                { id: '10', text: '10' },
                { id: '25', text: '25' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.resetQueryOptions();
    }

    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        switch ($event.code) {
            case "ArrowUp"://NAVIGATE TABLE ROW UP
                if (this.selectedMessage.index > 0) {
                    this.onRowSelected(this.selectedMessage.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedMessage.index < this.messages.length - 1) {
                    this.onRowSelected(this.selectedMessage.index + 1);
                }
                break;
        }
    }

    ngOnInit() {
        this.setMessageFilterOptions();
        this.getTotalParkedMessages();

        //Set latest query options
        let queryOptions = this.memStorageService.get(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY);
        if (queryOptions !== null) {
            this.queryOptions = queryOptions;
        }

        this.messageViewActive = MessageView.Received;

        let lastTabSelected = this.memStorageService.get(this.memStorageService.MSG_TAB_SELECTED_KEY);
        if (lastTabSelected) {
            this.messageViewActive = lastTabSelected;
        }

        this.getMessages(this.queryOptions.page);

        this.refreshMessages();
    }

    ngOnChanges() {
        this.getMessages(this.queryOptions.page);
        this.getTotalParkedMessages();
    }

    ngOnDestroy() {
        if (this.refreshHandler) {
            clearInterval(this.refreshHandler);
        }
    }

    //onFilterChanged(event: any) {
    //    if (event.error) {
    //        this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
    //    } else {
    //        this.queryOptions = this.memStorageService.get(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY);
    //        this.getMessages(this.queryOptions.page);
    //    }
    //    this.loadingData = false;
    //}

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getMessages(this.queryOptions.page);
    }

    onReceived() {
        if (this.messageViewActive !== MessageView.Received) {
            this.memStorageService.save(this.memStorageService.MSG_TAB_SELECTED_KEY, MessageView.Received);

            this.messageViewActive = MessageView.Received;
            this.resetQueryOptions();
            this.getMessages(this.queryOptions.page);
        }
    }

    onSent() {
        if (this.messageViewActive !== MessageView.Sent) {
            this.memStorageService.save(this.memStorageService.MSG_TAB_SELECTED_KEY, MessageView.Sent);

            this.messageViewActive = MessageView.Sent;
            this.resetQueryOptions();
            this.getMessages(this.queryOptions.page);
        }
    }

    onParked() {
        if (this.messageViewActive !== MessageView.Parked) {
            this.memStorageService.save(this.memStorageService.MSG_TAB_SELECTED_KEY, MessageView.Parked);

            this.messageViewActive = MessageView.Parked;
            this.resetQueryOptions();
            this.getMessages(this.queryOptions.page);
        }
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    rowColor(message) {
        switch (message.status) {
            //case ProposalType.New: return '';
            //case ProposalType.Replace: return 'bg-submitted';
            //case ProposalType.Cancel: return 'bg-widthdrawn';
            default: return '';
        }
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getMessages(1);
    }

    onStatusFilterChanged(e: any) {
        switch (e.value) {
            case "A": this.currentMessageStatusView = MessageStatus.None; break;
            case "D": this.currentMessageStatusView = MessageStatus.Deleted; break;
            case "I": this.currentMessageStatusView = MessageStatus.Important; break;
            case "R": this.currentMessageStatusView = MessageStatus.Reply; break;
        }

        this.resetQueryOptions();
        this.getMessages(1);
    }

    onRowSelected(index: number) {
        this.selectMessage(this.messages[index]);
    }

    onPageChange(page: number) {
        this.getMessages(page);
    }

    viewMessage(takeOwnership: boolean = false) {
        this.dataService.lockMessage(this.selectedMessage.id, takeOwnership)
            .subscribe((response: any) => {
                if (response.success) {
                    this.router.navigate(["/dashboard/messages/read", this.selectedMessage.id]);
                } else {
                    const self = this;

                    self.messageOwner = response.owner;
                    let elm = $('#modal-takeownership');
                    elm.modal({});
                    elm.on('hidden.bs.modal', function (e) {
                        //this.router.navigate(["/dashboard/messages/read", this.selectedMessage.id]);
                    })
                }
            });
    }

    //takeMessageOwnership() {
    //    this.viewMessage(true);
    //}

    private refreshMessages() {
        let self = this;
        this.refreshHandler = window.setInterval(function () {
            if (self.dataReceived) { //refresh only when data has been loaded once 
                self.getMessages(self.queryOptions.page);
            }
        }, syncFrecuency * 1000);
    }

    private selectMessage(message) {
        if (this.selectedMessage) {
            this.selectedMessage.isSelected = false;
        }
        this.selectedMessage = message
        this.selectedMessage.isSelected = true;
        this.memStorageService.save(this.memStorageService.NSD_CLIENT_ID_KEY, this.selectedMessage.id);
    }

    private getMessages(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY, this.queryOptions);

            switch (this.messageViewActive) {
                case MessageView.Received:
                    this.dataService.getIncomingMessages(this.queryOptions, this.currentMessageStatusView)
                        .subscribe((messages: any) => {
                            this.processMessages(messages);
                        });
                    break;
                case MessageView.Sent:
                    this.dataService.getOutgoingMessages(this.queryOptions, this.currentMessageStatusView)
                        .subscribe((messages: any) => {
                            this.processMessages(messages);
                        });
                    break;
                case MessageView.Parked:
                    this.dataService.getParkedMessages(this.queryOptions, this.currentMessageStatusView)
                        .subscribe((messages: any) => {
                            this.processMessages(messages);
                        });
                    break;
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    public getLockedByName(msg: IMessageQueuePartial): string {
        if (msg.locked) return msg.lastOwner;
        else return '';
    }

    private getTotalParkedMessages() {
        this.dataService.getTotalParkedMessages()        
            .subscribe((count: number) => {
                this.parked = count;
            });
    }

    private processMessages(messages: any) {
        this.dataReceived = true;
        this.paging = messages.paging;
        this.totalPages = messages.paging.totalPages;

        for (var iter = 0; iter < messages.data.length; iter++) {
            let msg = messages.data[iter];
            msg.index = iter;
            msg.typeStr = this.toClientTypeStr(messages.data[iter].clientType);
            if (msg.recipients.length > 88) {
                msg.recipients = msg.recipients.substring(0, 88) + " ..."; 
            }
        }

        const lastMessageSelected = this.memStorageService.get(this.memStorageService.NSD_CLIENT_ID_KEY);
        if (messages.data.length > 0) {
            this.messages = messages.data;
            let index = 0;
            if (lastMessageSelected) {
                index = this.messages.findIndex(x => x.id === lastMessageSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.messages[index].isSelected = true;
            this.selectedMessage = this.messages[index];
            this.loadingData = false;
            this.onRowSelected(index);
        } else {
            this.messages = [];
            this.selectedMessage = null;
            this.loadingData = false;
        }
    }

    toClientTypeStr(clientType) {
        switch (clientType) {
            case ClientType.AFTN: return 'AFTN';
            case ClientType.SWIM: return 'SWIM';
            case ClientType.REST: return 'REST';
            default: return '';
        }
    }

    cssIcon(status: MessageStatus, locked) {
        if (locked) {
            return "fa fa-hourglass-o"
        }

        switch (status) {
            case MessageStatus.Deleted: return "fa fa-times"
            case MessageStatus.Important: return "fa fa-bookmark"
            case MessageStatus.Parked: return "fa fa-lock"
            case MessageStatus.Reply: return "fa fa-mail-reply"
            default: return ""
        }
    }

    private setMessageFilterOptions() {
        this.messageFilter = [
            { id: "A", text: this.translation.translate("MessageQueue.Pending") },
            { id: "I", text: this.translation.translate("MessageQueue.Important") },
            { id: "D", text: this.translation.translate("MessageQueue.Deleted") },
            { id: "R", text: this.translation.translate("MessageQueue.Replied") }];

        this.selectedMessageFilterOptionId = this.messageFilter[0].id;
    }

    private resetQueryOptions() {
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[5].columnName //Default Received Descending
        }
    }
}


