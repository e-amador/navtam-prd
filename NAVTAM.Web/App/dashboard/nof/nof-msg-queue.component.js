"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofMsgQueueComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_model_1 = require("../shared/data.model");
var syncFrecuency = 10; //seconds
var MessageType = /** @class */ (function () {
    function MessageType() {
    }
    MessageType.Notification = 0;
    MessageType.Error = 1;
    return MessageType;
}());
var MessageView = /** @class */ (function () {
    function MessageView() {
    }
    MessageView.Received = 0;
    MessageView.Sent = 1;
    MessageView.Parked = 2;
    return MessageView;
}());
var NofMsgQueueComponent = /** @class */ (function () {
    function NofMsgQueueComponent(toastr, dataService, memStorageService, router, winRef, route, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.translation = translation;
        this.onUpdateCounter = new core_1.EventEmitter();
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterPaging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterTotalPages = 0;
        this.totalPages = 0;
        this.unread = 0;
        this.parked = 0;
        this.currentMessageStatusView = data_model_1.MessageStatus.None;
        this.loadingData = false;
        this.dataReceived = false;
        this.refreshHandler = null;
        this.messageOwner = "";
        this.selectedMessageFilterOptionId = "A";
        this.tableHeaders = {
            sorting: [
                { columnName: 'ClientName', direction: 0 },
                { columnName: 'ClientAddress', direction: 0 },
                { columnName: 'Recipients', direction: 0 },
                { columnName: 'LastOwner', direction: 0 },
                { columnName: 'PriorityCode', direction: 0 },
                { columnName: 'Created', direction: -1 },
                { columnName: 'Status', direction: 0 },
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '25', text: '25' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.resetQueryOptions();
    }
    NofMsgQueueComponent.prototype.onKeyup = function ($event) {
        switch ($event.code) {
            case "ArrowUp": //NAVIGATE TABLE ROW UP
                if (this.selectedMessage.index > 0) {
                    this.onRowSelected(this.selectedMessage.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedMessage.index < this.messages.length - 1) {
                    this.onRowSelected(this.selectedMessage.index + 1);
                }
                break;
        }
    };
    NofMsgQueueComponent.prototype.ngOnInit = function () {
        this.setMessageFilterOptions();
        this.getTotalParkedMessages();
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY);
        if (queryOptions !== null) {
            this.queryOptions = queryOptions;
        }
        this.messageViewActive = MessageView.Received;
        var lastTabSelected = this.memStorageService.get(this.memStorageService.MSG_TAB_SELECTED_KEY);
        if (lastTabSelected) {
            this.messageViewActive = lastTabSelected;
        }
        this.getMessages(this.queryOptions.page);
        this.refreshMessages();
    };
    NofMsgQueueComponent.prototype.ngOnChanges = function () {
        this.getMessages(this.queryOptions.page);
        this.getTotalParkedMessages();
    };
    NofMsgQueueComponent.prototype.ngOnDestroy = function () {
        if (this.refreshHandler) {
            clearInterval(this.refreshHandler);
        }
    };
    //onFilterChanged(event: any) {
    //    if (event.error) {
    //        this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
    //    } else {
    //        this.queryOptions = this.memStorageService.get(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY);
    //        this.getMessages(this.queryOptions.page);
    //    }
    //    this.loadingData = false;
    //}
    NofMsgQueueComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getMessages(this.queryOptions.page);
    };
    NofMsgQueueComponent.prototype.onReceived = function () {
        if (this.messageViewActive !== MessageView.Received) {
            this.memStorageService.save(this.memStorageService.MSG_TAB_SELECTED_KEY, MessageView.Received);
            this.messageViewActive = MessageView.Received;
            this.resetQueryOptions();
            this.getMessages(this.queryOptions.page);
        }
    };
    NofMsgQueueComponent.prototype.onSent = function () {
        if (this.messageViewActive !== MessageView.Sent) {
            this.memStorageService.save(this.memStorageService.MSG_TAB_SELECTED_KEY, MessageView.Sent);
            this.messageViewActive = MessageView.Sent;
            this.resetQueryOptions();
            this.getMessages(this.queryOptions.page);
        }
    };
    NofMsgQueueComponent.prototype.onParked = function () {
        if (this.messageViewActive !== MessageView.Parked) {
            this.memStorageService.save(this.memStorageService.MSG_TAB_SELECTED_KEY, MessageView.Parked);
            this.messageViewActive = MessageView.Parked;
            this.resetQueryOptions();
            this.getMessages(this.queryOptions.page);
        }
    };
    NofMsgQueueComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    NofMsgQueueComponent.prototype.rowColor = function (message) {
        switch (message.status) {
            //case ProposalType.New: return '';
            //case ProposalType.Replace: return 'bg-submitted';
            //case ProposalType.Cancel: return 'bg-widthdrawn';
            default: return '';
        }
    };
    NofMsgQueueComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getMessages(1);
    };
    NofMsgQueueComponent.prototype.onStatusFilterChanged = function (e) {
        switch (e.value) {
            case "A":
                this.currentMessageStatusView = data_model_1.MessageStatus.None;
                break;
            case "D":
                this.currentMessageStatusView = data_model_1.MessageStatus.Deleted;
                break;
            case "I":
                this.currentMessageStatusView = data_model_1.MessageStatus.Important;
                break;
            case "R":
                this.currentMessageStatusView = data_model_1.MessageStatus.Reply;
                break;
        }
        this.resetQueryOptions();
        this.getMessages(1);
    };
    NofMsgQueueComponent.prototype.onRowSelected = function (index) {
        this.selectMessage(this.messages[index]);
    };
    NofMsgQueueComponent.prototype.onPageChange = function (page) {
        this.getMessages(page);
    };
    NofMsgQueueComponent.prototype.viewMessage = function (takeOwnership) {
        var _this = this;
        if (takeOwnership === void 0) { takeOwnership = false; }
        this.dataService.lockMessage(this.selectedMessage.id, takeOwnership)
            .subscribe(function (response) {
            if (response.success) {
                _this.router.navigate(["/dashboard/messages/read", _this.selectedMessage.id]);
            }
            else {
                var self_1 = _this;
                self_1.messageOwner = response.owner;
                var elm = $('#modal-takeownership');
                elm.modal({});
                elm.on('hidden.bs.modal', function (e) {
                    //this.router.navigate(["/dashboard/messages/read", this.selectedMessage.id]);
                });
            }
        });
    };
    //takeMessageOwnership() {
    //    this.viewMessage(true);
    //}
    NofMsgQueueComponent.prototype.refreshMessages = function () {
        var self = this;
        this.refreshHandler = window.setInterval(function () {
            if (self.dataReceived) { //refresh only when data has been loaded once 
                self.getMessages(self.queryOptions.page);
            }
        }, syncFrecuency * 1000);
    };
    NofMsgQueueComponent.prototype.selectMessage = function (message) {
        if (this.selectedMessage) {
            this.selectedMessage.isSelected = false;
        }
        this.selectedMessage = message;
        this.selectedMessage.isSelected = true;
        this.memStorageService.save(this.memStorageService.NSD_CLIENT_ID_KEY, this.selectedMessage.id);
    };
    NofMsgQueueComponent.prototype.getMessages = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY, this.queryOptions);
            switch (this.messageViewActive) {
                case MessageView.Received:
                    this.dataService.getIncomingMessages(this.queryOptions, this.currentMessageStatusView)
                        .subscribe(function (messages) {
                        _this.processMessages(messages);
                    });
                    break;
                case MessageView.Sent:
                    this.dataService.getOutgoingMessages(this.queryOptions, this.currentMessageStatusView)
                        .subscribe(function (messages) {
                        _this.processMessages(messages);
                    });
                    break;
                case MessageView.Parked:
                    this.dataService.getParkedMessages(this.queryOptions, this.currentMessageStatusView)
                        .subscribe(function (messages) {
                        _this.processMessages(messages);
                    });
                    break;
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    NofMsgQueueComponent.prototype.getLockedByName = function (msg) {
        if (msg.locked)
            return msg.lastOwner;
        else
            return '';
    };
    NofMsgQueueComponent.prototype.getTotalParkedMessages = function () {
        var _this = this;
        this.dataService.getTotalParkedMessages()
            .subscribe(function (count) {
            _this.parked = count;
        });
    };
    NofMsgQueueComponent.prototype.processMessages = function (messages) {
        this.dataReceived = true;
        this.paging = messages.paging;
        this.totalPages = messages.paging.totalPages;
        for (var iter = 0; iter < messages.data.length; iter++) {
            var msg = messages.data[iter];
            msg.index = iter;
            msg.typeStr = this.toClientTypeStr(messages.data[iter].clientType);
            if (msg.recipients.length > 88) {
                msg.recipients = msg.recipients.substring(0, 88) + " ...";
            }
        }
        var lastMessageSelected = this.memStorageService.get(this.memStorageService.NSD_CLIENT_ID_KEY);
        if (messages.data.length > 0) {
            this.messages = messages.data;
            var index = 0;
            if (lastMessageSelected) {
                index = this.messages.findIndex(function (x) { return x.id === lastMessageSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.messages[index].isSelected = true;
            this.selectedMessage = this.messages[index];
            this.loadingData = false;
            this.onRowSelected(index);
        }
        else {
            this.messages = [];
            this.selectedMessage = null;
            this.loadingData = false;
        }
    };
    NofMsgQueueComponent.prototype.toClientTypeStr = function (clientType) {
        switch (clientType) {
            case data_model_1.ClientType.AFTN: return 'AFTN';
            case data_model_1.ClientType.SWIM: return 'SWIM';
            case data_model_1.ClientType.REST: return 'REST';
            default: return '';
        }
    };
    NofMsgQueueComponent.prototype.cssIcon = function (status, locked) {
        if (locked) {
            return "fa fa-hourglass-o";
        }
        switch (status) {
            case data_model_1.MessageStatus.Deleted: return "fa fa-times";
            case data_model_1.MessageStatus.Important: return "fa fa-bookmark";
            case data_model_1.MessageStatus.Parked: return "fa fa-lock";
            case data_model_1.MessageStatus.Reply: return "fa fa-mail-reply";
            default: return "";
        }
    };
    NofMsgQueueComponent.prototype.setMessageFilterOptions = function () {
        this.messageFilter = [
            { id: "A", text: this.translation.translate("MessageQueue.Pending") },
            { id: "I", text: this.translation.translate("MessageQueue.Important") },
            { id: "D", text: this.translation.translate("MessageQueue.Deleted") },
            { id: "R", text: this.translation.translate("MessageQueue.Replied") }
        ];
        this.selectedMessageFilterOptionId = this.messageFilter[0].id;
    };
    NofMsgQueueComponent.prototype.resetQueryOptions = function () {
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[5].columnName //Default Received Descending
        };
    };
    __decorate([
        core_1.ViewChild('msgFilter'),
        __metadata("design:type", Object)
    ], NofMsgQueueComponent.prototype, "filter", void 0);
    __decorate([
        core_1.Input('count'),
        __metadata("design:type", Number)
    ], NofMsgQueueComponent.prototype, "messageCount", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], NofMsgQueueComponent.prototype, "onUpdateCounter", void 0);
    __decorate([
        core_1.HostListener('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NofMsgQueueComponent.prototype, "onKeyup", null);
    NofMsgQueueComponent = __decorate([
        core_1.Component({
            selector: 'nof-msg-queue',
            styleUrls: ['app/dashboard/nof/nof-msg-queue.component.css'],
            templateUrl: '/app/dashboard/nof/nof-msg-queue.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.TranslationService])
    ], NofMsgQueueComponent);
    return NofMsgQueueComponent;
}());
exports.NofMsgQueueComponent = NofMsgQueueComponent;
//# sourceMappingURL=nof-msg-queue.component.js.map