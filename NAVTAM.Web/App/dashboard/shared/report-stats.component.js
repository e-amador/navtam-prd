"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportStatsComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("../../common/toastr.service");
var router_1 = require("@angular/router");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var moment = require("moment");
var ReportStatsComponent = /** @class */ (function () {
    function ReportStatsComponent(toastr, dataService, memStorageService, router, winRef, route, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.translation = translation;
        this.loadingData = true;
        this.start = null;
        this.end = null;
        this.periodAggregation = 0;
        this.currentPeriodOption = '0';
        this.periodOptions =
            [
                { id: '0', text: this.translation.translate('Reports.Yearly') },
                { id: '1', text: this.translation.translate('Reports.Monthly') },
                { id: '2', text: this.translation.translate('Reports.Weekly') },
                { id: '4', text: this.translation.translate('Reports.Daily') },
                { id: '3', text: this.translation.translate('Reports.Hourly') }
            ];
        this.stats = [];
    }
    ReportStatsComponent.prototype.ngOnInit = function () {
        this.setDataPickers("years");
        this.getStats();
    };
    ReportStatsComponent.prototype.ngAfterViewInit = function () {
        this.exportReport();
    };
    ReportStatsComponent.prototype.dateChanged = function (ev) {
        switch (ev.currentTarget.id) {
            case "rpt-stats-startdate":
                this.start = moment(new Date(ev.currentTarget.value));
                if (this.end && this.end.isValid()) {
                    var el = $('#rpt-stats-startdate');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
            case "rpt-stats-enddate":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    var el = $('#rpt-stats-enddate');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        var period = +this.currentPeriodOption;
                        if (period < 2) { //years or months
                            this.end = period === 0 ? moment(this.end).endOf('year') : moment(this.end).endOf('month');
                            el.bootstrapDatepicker('update', this.end.format('MM/DD/YYYY'));
                        }
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    };
    ReportStatsComponent.prototype.onPeriodOptionsChanged = function (e) {
        this.currentPeriodOption = e.value;
        switch (this.currentPeriodOption) {
            case '0':
                this.setDataPickers("years");
                break;
            case '1':
                this.setDataPickers("months");
                break;
            default:
                this.setDataPickers("days");
                break;
        }
        //if (this.currentPeriodOption == '3') {
        $('#rpt-stats-enddate').bootstrapDatepicker('setDate', null);
        $('#rpt-stats-startdate').bootstrapDatepicker('setDate', null);
        //}
    };
    ReportStatsComponent.prototype.onRowSelected = function (rowId) {
    };
    ReportStatsComponent.prototype.isStartHasValue = function () {
        return $('#rpt-stats-startdate').bootstrapDatepicker('getDate') != null;
    };
    ReportStatsComponent.prototype.isEndHaveValue = function () {
        return $('#rpt-stats-enddate').bootstrapDatepicker('getDate') != null;
    };
    ReportStatsComponent.prototype.isRunReportButtonEnabled = function () {
        return this.currentPeriodOption !== '3' ? this.isStartHasValue() && this.isEndHaveValue() : this.isStartHasValue();
    };
    ReportStatsComponent.prototype.runReport = function () {
        this.getStats();
    };
    ReportStatsComponent.prototype.exportReport = function () {
        var self = this;
        $("#export").click(function (event) {
            var outputFile = window.prompt(self.translation.translate('Reports.PromptFilename'));
            ;
            if (outputFile) {
                outputFile = outputFile.replace('.csv', '') + '.csv';
                // CSV
                self.exportFunc.apply(this, [$('#rpt-stats-data'), outputFile]);
            }
        });
    };
    ReportStatsComponent.prototype.getStats = function () {
        var _this = this;
        try {
            this.loadingData = true;
            var start = this.start.toDate();
            var end = this.currentPeriodOption !== '3' ? this.end.toDate() : start;
            this.dataService.getNofReportStats(start, end, +this.currentPeriodOption)
                .subscribe(function (stats) {
                _this.processStats(stats);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    ReportStatsComponent.prototype.processStats = function (stats) {
        for (var iter = 0; iter < stats.length; iter++) {
            stats[iter].index = iter;
            stats[iter].totals = false;
            if (stats[iter].startPeriod === "" && stats[iter].endPeriod === "") {
                stats[iter].startPeriod = "Totals";
                stats[iter].totals = true;
            }
        }
        if (stats.length > 0) {
            this.stats = stats;
            var index = 0;
            //this.stats[0].isSelected = true;
            this.loadingData = false;
            //this.onRowSelected(0);
        }
        else {
            this.stats = [];
            //this.selectedNotam = null;
            this.loadingData = false;
        }
    };
    ReportStatsComponent.prototype.setDataPickers = function (period) {
        var self = this;
        $('.rpt-stats-datepicker').each(function () {
            $(this).bootstrapDatepicker().off('changeDate');
        });
        $("#rpt-stats-startdate").bootstrapDatepicker("remove");
        $("#rpt-stats-enddate").bootstrapDatepicker("remove");
        $('.rpt-stats-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                viewMode: period,
                minViewMode: period,
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0,
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "",
                calendarWeeks: false,
                toggleActive: true,
                multidate: false,
            }).on('changeDate', function event_handler(e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });
    };
    __decorate([
        core_1.Input('exportFunc'),
        __metadata("design:type", Object)
    ], ReportStatsComponent.prototype, "exportFunc", void 0);
    ReportStatsComponent = __decorate([
        core_1.Component({
            selector: 'report-stats',
            styleUrls: ['app/dashboard/shared/report-stats.component.css'],
            templateUrl: '/app/dashboard/shared/report-stats.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.TranslationService])
    ], ReportStatsComponent);
    return ReportStatsComponent;
}());
exports.ReportStatsComponent = ReportStatsComponent;
//# sourceMappingURL=report-stats.component.js.map