﻿import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ISdoSubject, IFIRSubject, IMessageTemplate } from './data.model';

import { WindowRef } from '../../common/windowRef.service'
//import { BearerTokenService } from '../../common/bearer-token.service'
import { Select2OptionData } from 'ng2-select2';

import * as moment from 'moment';

import {
    IAuthData,
    IProposal,
    INotam,
    IProposalDetails,
    IQueryOptions,
    IcaoSubject,
    IcaoCondition,
    INdsClient,
    INdsClientPartial,
    INdsClientFilter,
    IMessageQueue,
    IMessageQueuePartial,
    NofQueues,
    MessageStatus,
    IUserMessageTemplate,
    IFilterCriteria,
    IOriginatorInfo,
    IPoint,
    Icao,
    INsdCategory,
    ItemD,
    IRegionMapRender,
    ICloneRequest,
    IPointsViewModel,
    IInDoaValidationResult
} from './data.model';

@Injectable()
export class DataService {
    nsdCategories: INsdCategory[]; //keeping cached values
    series: Array<Select2OptionData>; //keeping cached values

    selectedCategoryId: number;    //current category Id selected
    apiUrl: string;

    overrideBilingual: boolean = false;
    override: BehaviorSubject<boolean>;

    constructor(private http: Http, private winRef: WindowRef /*, private tokenService: BearerTokenService*/) {
        this.apiUrl = winRef.nativeWindow['app'].apiUrl;
        this.app = winRef.nativeWindow['app'];
        this.override = new BehaviorSubject(this.overrideBilingual);
    }

    public app

    getSdoExtendedName(sdo: ISdoSubject): string {
        var extName = sdo.designator + ' ';
        if (sdo.servedCity.length > 0) extName += sdo.servedCity + '/';
        extName += sdo.name;
        return extName;
    }

    getSdoSubject(subId: string): Observable<ISdoSubject> {
        //this.setBearerToken();
        return this.http.get(this.app.apiUrl + `sdosubjects/find/?subId=${subId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getAerodromes(): Observable<[string, string]> {
        //this.setBearerToken();
        return this.http.get(this.app.apiUrl + `sdosubjects/aerodromes`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getFirs(): Observable<IFIRSubject[]> {
        return this.http.get(this.app.apiUrl + `sdosubjects/GetAllFIRAsync`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getMapHealth(): Observable<boolean> {
        return this.http.get(this.apiUrl + `map/getmaphealth`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getNsdCategories(): Observable<INsdCategory[]> {
        if (this.nsdCategories) {
            return Observable.of(this.nsdCategories);
        }

        //get series first time categories are loaded;
        this.getSeries().subscribe((series: Array<Select2OptionData>) => { this.series = series; });

        return this.http.get(`${this.apiUrl}nsdcategories/activeorg`).map((response: Response) => {
            this.nsdCategories = response.json();
            return this.nsdCategories;
        }).catch(this.handleError);
    }

    getSeries(): Observable<Array<Select2OptionData>> {
        if (this.series) {
            return Observable.of(this.series);
        }

        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}configvalues/series/`)
            .map((response: Response) => {
                const series: Array<Select2OptionData> = [];
                response.json().forEach((serie: string) => {
                    series.push({ id: serie, text: serie });
                });
                return series;
            }).catch(this.handleError);
    }

    getItemD(proposalId: number): Observable<ItemD[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}itemd/${proposalId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveItemD(itemD: ItemD[]): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}itemd`, itemD)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateItemD(freetext: string): Observable<any> {
        //this.setBearerToken();
        let viewmodel = { itemD: freetext };
        return this.http.post(`${this.apiUrl}itemd/validate`, viewmodel)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    loadItemD(proposalId): Observable<ItemD[]> {
        return this.http.get(`${this.apiUrl}itemd/${proposalId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteItemD(proposalId): Observable<boolean> {
        //this.setBearerToken();
        return this.http.delete(`${this.apiUrl}itemd/${proposalId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getProposalsByCategory(categoryId: number, queryOptions: IQueryOptions): Observable<IProposal[]> {
        this.selectedCategoryId = categoryId;

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/${categoryId}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getGroupedProposals(groupId: string): Observable<IProposal[]> {
        //this.setBearerToken();

        return this.http.get(`${this.apiUrl}proposals/grouped/${groupId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    searchProposals(filterCriteria: IFilterCriteria[], queryOptions: IQueryOptions): Observable<IProposal[]> {
        const data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions
        };

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposalhistory/filter`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    filterProposals(filterCriteria: IFilterCriteria[], queryOptions: IQueryOptions): Observable<IProposal[]> {
        const data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions
        };

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/filter`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getProposalStatus(ids: number[], refreshRate: number, lastSyncStatusCheck?: moment.Moment): Observable<any> {
        let date;
        try {
            date = lastSyncStatusCheck.toISOString();
        } catch (err) {
            date = "";
        }
        return this.http.post(`${this.apiUrl}proposals/status/?refreshRate=${refreshRate}&since=${date}`, ids)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNotamIcaoFormat(notamId: string): Observable<Icao> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/notams/icaoformat/${notamId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    canNotamBeReplaced(notamId: string): Observable<boolean> {
        //this.setBearerToken();

        let notamIdStr = notamId.replace("/", "_");
        return this.http.get(`${this.apiUrl}nof/notams/canbereplaced/${notamIdStr}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    verifyObsoleteNotam(id: string): Observable<boolean> {
        //this.setBearerToken();

        return this.http.get(`${this.apiUrl}nof/notams/verifyobsolete/${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    //NDS Clients subscriptions
    getNdsClient(id: number): Observable<INdsClient> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/ndsclients/${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNdsClients(queryOptions: IQueryOptions): Observable<INdsClientPartial[]> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/ndsclients/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNdsClientAddresses(): Observable<INdsClientFilter[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/ndsclients`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveNdsClient(client: INdsClient): Observable<INdsClient> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/ndsclients/`, client)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateNdsClient(client: INdsClient): Observable<INdsClient> {
        //this.setBearerToken();
        return this.http.put(`${this.apiUrl}nof/ndsclients/`, client)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteNdsClient(clientId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.delete(`${this.apiUrl}nof/ndsclients/${clientId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    sendNotamsToSubscriptions(clientIds: string): Observable<any> {
        let viewmodel = { subsClientIds: clientIds };
        return this.http.post(`${this.apiUrl}transition/queuejob/`, viewmodel)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    existsClientName(id: number, name: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}nof/ndsclients/exists/?Id=${id}&name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //GeoRef functions
    saveGeoFile(data: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/ndsclients/uploadfile`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getGeoFeatures(region: IRegionMapRender): Observable<string> {
        return this.http.post(`${this.apiUrl}nof/ndsclients/GetGeoFeatures/`, region)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateGeoLocation(loc: string): Observable<string> {
        return this.http.get(`${this.apiUrl}nof/ndsclients/validatepoint/?value=${loc}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateGeoPoints(points: string): Observable<string> {
        return this.http.get(`${this.apiUrl}nof/ndsclients/validatepoints/?value=${points}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //End GeoRef functions

    //Message Exchange Queues
    lockMessage(id: string, takeOwnership: boolean): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/lock/${id}/${takeOwnership}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    isMessageLocked(id: string): Observable<any> {
        return this.http.get(`${this.apiUrl}nof/messages/islocked/${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    unlockMessage(id: string): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/unlock/${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getMessage(id: string): Observable<IMessageQueue> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getIncomingMessages(queryOptions: IQueryOptions, status: MessageStatus): Observable<IMessageQueuePartial[]> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/incomingmessages/page/${status}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getOutgoingMessages(queryOptions: IQueryOptions, status: MessageStatus): Observable<IMessageQueuePartial[]> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/outgoingmessages/page/${status}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getParkedMessages(queryOptions: IQueryOptions, status: MessageStatus): Observable<IMessageQueuePartial[]> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/parkedmessages/page/${status}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateMessage(message: IMessageQueue): Observable<IMessageQueue> {
        //this.setBearerToken();
        return this.http.put(`${this.apiUrl}nof/incomingmessages`, message)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    sendQueueMessage(message: IMessageQueue): Observable<IMessageQueue> {
        return this.http.post(`${this.apiUrl}nof/outgoingmessages`, message)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteIncomingMessage(messageId): Observable<any> {
        //this.setBearerToken();
        return this.http.delete(`${this.apiUrl}nof/incomingmessages/${messageId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    getMessageTemplates(): Observable<IMessageTemplate[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/templates`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getMessageTemplateNames(): Observable<IMessageTemplate[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/templates/names`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteMessageTemplate(templateId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.delete(`${this.apiUrl}nof/messages/templates/${templateId}`)
            .map((response: Response) => {
                return response; //Non-content (204)
            }).catch(this.handleError);
    }

    getMessageTemplate(id: string): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/templates/${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveMessageTemplate(template: IMessageTemplate): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/messages/templates/`, template)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateMessageTemplate(template: IMessageTemplate): Observable<any> {
        //this.setBearerToken();
        return this.http.put(`${this.apiUrl}nof/messages/templates/`, template)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getTotalParkedMessages(): Observable<number> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/park/count`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getTotalUnreadMessages(): Observable<number> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/messages/unread/count`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    filterNotams(filterCriteria: IFilterCriteria[], queryOptions: IQueryOptions, filterBy): Observable<INotam[]> {
        const data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions,
            filterBy: filterBy
        };

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/notams/filter`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    filterReportNotams(filterCriteria: IFilterCriteria[], queryOptions: IQueryOptions, startdate, enddate, filterBy): Observable<INotam[]> {
        const data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions,
            start: startdate,
            end: enddate,
            filterBy : filterBy
        };

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/notams/reportfilter`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    filterReportActiveNotams(filterCriteria: IFilterCriteria[], queryOptions: IQueryOptions, startdate, starttime, endtime): Observable<INotam[]> {
        const data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions,
            start: startdate,
            startTime: starttime,
            endTime: endtime
        };

        return this.http.post(`${this.apiUrl}nof/notams/reportactivefilter`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveQueryFilter(slotNumber: number, filterCriteria: IFilterCriteria[], filterType: number): Observable<any> {
        const data = {
            username: window['app'].usrName,
            filterName: 'F' + slotNumber,
            slotNumber: slotNumber,
            userFilterType: filterType,
            filterConditions: filterCriteria
        };
        const endpoint = this.app.isNof
            ? `${this.apiUrl}nof/queryfilter`
            : `${this.apiUrl}proposalhistory/queryfilter`;

        //this.setBearerToken();
        return this.http.post(endpoint, data)
            .map((response: Response) => {
                return response.status;
            }).catch(this.handleError);
    }

    deleteQueryFilter(slotNumber: number, filterType: number): Observable<any> {
        const endpoint = this.app.isNof
            ? `${this.apiUrl}nof/queryfilter/${slotNumber}/${filterType}`
            : `${this.apiUrl}proposalhistory/queryfilter/${slotNumber}`;

        //this.setBearerToken();
        return this.http.delete(endpoint)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    getActiveQueryFilters(filterType: number): Observable<number[]> {
        const endpoint = this.app.isNof
            ? `${this.apiUrl}nof/queryfilter/active/${this.app.usrName}/${filterType}`
            : `${this.apiUrl}proposalhistory/queryfilter/active/${this.app.usrName}`;

        //this.setBearerToken();
        return this.http.get(endpoint).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getQueryFilterNames(username: string): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/queryfiltername?username=${username}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    saveQueryFilterName(username: string, slotNumber, filterName): Observable<any> {
        //this.setBearerToken();
        let data = {
            username,
            slotNumber,
            filterName
        }
        return this.http.post(`${this.apiUrl}nof/queryfiltername`, data).map((response: Response) => {
            return response;
        }).catch(this.handleError);
    }


    getQueryFilter(slotNumber: number, filterType: number): Observable<IFilterCriteria[]> {
        const endpoint = this.app.isNof
            ? `${this.apiUrl}nof/queryfilter/${slotNumber}/${this.app.usrName}/${filterType}`
            : `${this.apiUrl}proposalhistory/queryfilter/${slotNumber}/${this.app.usrName}`;

        //this.setBearerToken();
        return this.http.get(endpoint).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getProposalsByParentCategory(parentCategoryId: number, queryOptions: IQueryOptions): Observable<IProposal[]> {
        this.selectedCategoryId = parentCategoryId;

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/children/${parentCategoryId}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofTotalPending(): Observable<Number> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/pending/count`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getNofTotalParked(): Observable<Number> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/park/count`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getExpiringTotal(): Observable<Number> {
        return this.http.get(`${this.apiUrl}proposals/expiringcount`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getExpiringProposals(queryOptions: IQueryOptions): Observable<Number> {
        return this.http.post(`${this.apiUrl}proposals/expiring`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofTotalReviewed(): Observable<Number> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/review/count`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getNofProposalsByQueueName(categoryId: number, queueName: string, queryOptions: IQueryOptions): Observable<IProposal[]> {
        const actionName = this.endpointActionName(queueName);

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/${actionName}/${categoryId}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNOfProposalsByParentCategoryAndQueueName(parentCategoryId: number, queueName: string, queryOptions: IQueryOptions): Observable<IProposal[]> {
        this.selectedCategoryId = parentCategoryId;
        const actionName = this.endpointActionName(queueName);

        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/${actionName}/children/${parentCategoryId}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofGroupedProposals(groupId: string, queueName: string): Observable<IProposal[]> {
        //this.setBearerToken();

        const actionName = this.endpointActionName(queueName);
        return this.http.get(`${this.apiUrl}nof/proposals/grouped/${actionName}/${groupId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofNotams(queryOptions: IQueryOptions): Observable<INotam[]> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/notams`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofReportNotams(start, end, queryOptions: IQueryOptions, filterBy): Observable<INotam[]> {
        //this.setBearerToken();
        const data = {
            start: start,
            end: end,
            queryOptions: queryOptions,
            filterBy: filterBy
        };
        return this.http.post(`${this.apiUrl}nof/reportnotams`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofReportActiveNotams(start, starttime, endtime, queryOptions: IQueryOptions): Observable<INotam[]> {
        const data = {
            start: start,
            queryOptions: queryOptions,
            startTime: starttime,
            endTime: endtime,
        };
        return this.http.post(`${this.apiUrl}nof/reportactivenotams`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    exportNofReportNotams(payload): Observable<INotam[]> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/reportnotams/csv`, payload)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    exportNofReportActiveNotams(payload): Observable<INotam[]> {
        return this.http.post(`${this.apiUrl}nof/reportactivenotams/csv`, payload)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofReportStats(start, end, type): Observable<any[]> {
        //this.setBearerToken();
        const data = {
            start: start,
            end: end,
            aggregatePeriod: type
        };
        return this.http.post(`${this.apiUrl}nof/reportstats`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNofNotamTrackingList(proposalId: number): Observable<INotam[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nof/notams/trackinglist/${proposalId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getProposalDetails(proposalId: number): Observable<IProposalDetails[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}nsdcategories/`).map((response: Response) => {
            return {
            };
        }).catch(this.handleError);
    }

    getIcaoSubjects(entityCode: string): Observable<IcaoSubject[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}icaosubjects/filter/?code=${entityCode}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getDoaLocation(doaId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}doas/GeoJson/?doaId=${doaId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getSerie(subjectId: string, qCode: string): Observable<string> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}series/getserie/?subjectId=${subjectId}&qCode=${qCode}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getIcaoConditions(icaoSubjectId: number, cancelledOnly: boolean): Observable<IcaoCondition[]> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}icaoconditions/filter/?subId=${icaoSubjectId}&cancelledOnly=${cancelledOnly}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getEmptyProposalModel(categoryId: number) {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposals/getemptymodel?catid=${categoryId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getProposalModel(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposals/getmodel/${proposalId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getProposalModelEnforced(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposals/getmodelenforced/${proposalId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getProposalReadOnlyModel(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposals/getreadonlymodel?id=${proposalId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getProposalHistoryModel(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposalhistory/getmodel?id=${proposalId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getProposalHistory(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposalhistory/getall?proposalId=${proposalId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    generateIcao(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}icao/GenerateIcao`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveDraftProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/SaveDraft`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveDraftGroupProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/SaveDraftGrouped`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    parkProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/parkproposal`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    parkGroupedProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/ParkGroupedProposals`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    rejectProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/rejectproposal`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    disseminateProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/diseminateproposal`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    disseminateGroupedProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/diseminategroupedproposal`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    replaceProposal(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/replaceproposal/${proposalId}`, null)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    cancelProposal(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/cancelproposal/${proposalId}`, null)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    cloneProposal(cloneReq: ICloneRequest): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/cloneproposal`, cloneReq)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    cloneNotam(id: string): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/clonenotam/${id}`, null)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNotamProposalReadOnlyModel(id: string): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposals/getnotamreadonlymodel?id=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    acknowledgeProposal(proposalId: number): Observable<any> {
        return this.http.post(`${this.apiUrl}proposals/acknowledge/${proposalId}`, null)
            .map((response: Response) => {
                return response; //Non-content (204)
            }).catch(this.handleError);
    }

    discardProposal(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.delete(`${this.apiUrl}proposals/discard/${proposalId}`)
            .map((response: Response) => {
                return response; //Non-content (204)
            }).catch(this.handleError);
    }


    discardNofProposal(proposalId: number): Observable<any> {
        //this.setBearerToken();
        return this.http.delete(`${this.apiUrl}nof/discard/${proposalId}`)
            .map((response: Response) => {
                return response; //Non-content (204)
            }).catch(this.handleError);
    }

    submitProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/SubmitProposal`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    submitGroupedProposal(model: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/SubmitGroupedProposal`, model)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveProposalAttachments(data: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/SaveAttachments`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveProposalAnnotations(proposalId, annotations): Observable<any> {
        //this.setBearerToken();
        let data = {
            proposalId,
            annotations
        }
        return this.http.post(`${this.apiUrl}proposals/annotations`, data)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    updateOriginatorContactInfo(proposalId, originator, email, phone): Observable<any> {
        //this.setBearerToken();
        let data = {
            proposalId,
            originator,
            email,
            phone
        }
        return this.http.put(`${this.apiUrl}proposals/contactinfo`, data)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }


    loadProposalAttachment(id: string): Observable<any> {
        //this.setBearerToken();
        return this.http.get(`${this.apiUrl}proposals/attachments/${id}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    deleteProposalAttachment(data: any): Observable<any> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/DeleteAttachments`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    restoreProposalStatus(proposalId: number, statusUpdated: boolean): Observable<boolean> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}proposals/restorestatus/${proposalId}/${statusUpdated}`, null)
            .map((response: Response) => {
                return response.status === 200;
            }).catch(this.handleError);
    }

    restoreNofProposalStatus(proposalId: number, statusUpdated: boolean): Observable<boolean> {
        //this.setBearerToken();
        return this.http.post(`${this.apiUrl}nof/proposals/restorestatus/${proposalId}/${statusUpdated}`, null)
            .map((response: Response) => {
                return response.status === 200;
            }).catch(this.handleError);
    }

    addOriginatorInfo(originator: IOriginatorInfo): Observable<boolean> {
        return this.http.post(`${this.apiUrl}originatorinfo/save`, originator)
            .map((response: Response) => {
                return response.status === 200;
            }).catch(this.handleError);
    }

    removeOriginatorInfo(originatorId: number): Observable<boolean> {
        return this.http.delete(`${this.apiUrl}originatorinfo/${originatorId}`)
            .map((response: Response) => {
                return response.status === 200;
            }).catch(this.handleError);
    }

    findMatchingOriginatorInfos(originatorName: string, orgId: number): Observable<any> {
        originatorName = encodeURIComponent(originatorName);
        return this.http.get(`${this.apiUrl}originatorinfo/filter?query=${originatorName}&size=10&orgId=${orgId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    private handleError(error: Response) {
        if (error.status === 401) {
            window.location.reload();
        }
        return Observable.throw(error.json());
    }

    getNsds(): Observable<any> {
        return this.http.get(`${this.apiUrl}dashboard/getnsds`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    private endpointActionName(queueName: string): string {
        switch (queueName) {
            case NofQueues.Pending: return "pending";
            case NofQueues.Park: return "park";
            case NofQueues.Review: return "review";
        }
    }

    getPrevNotamIcaoText(notamId: string, action: number): Observable<string> {
        //this.setBearerToken();

        notamId = notamId.replace('/', '_');
        return this.http.get(`${this.apiUrl}notams/getPrevIcaoText?notamId=${notamId}&action=${action}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    calculateGeoRef(data: IPointsViewModel): Observable<IPointsViewModel> {
        return this.http.post(`${this.apiUrl}georef/calculate/`, data).map((response: Response) => {
            return response.json();
        });//.catch(this.handleError);
    }

    calculateGeoRefByPoint(data: IPointsViewModel): Observable<IPointsViewModel> {
        return this.http.post(`${this.apiUrl}georef/pointradius/`, data).map((response: Response) => {
            return response.json();
        });//.catch(this.handleError);
    }

    validateLocation(location: string): Observable<IInDoaValidationResult> {
        return this.http
            .get(`${this.app.apiUrl}doas/indoa?location=${location}`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    updateProposalItemX(id: number, itemX: string): Observable<boolean> {
        //this.setBearerToken();
        return this.http.put(`${this.apiUrl}nof/itemX`, { proposalId: id, value: itemX })
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    nextOverrideBilingual(value: boolean) {
        this.overrideBilingual = value;
        this.override.next(this.overrideBilingual);
    }
    
    /*
    private setBearerToken() {
        //TODO: Verify accesstoken before request;
        const authData = <IAuthData>this.winRef.authData;
        this.tokenService.addAuthorization(authData.access_token);
    }
    */
}