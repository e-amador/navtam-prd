"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationService = void 0;
var core_1 = require("@angular/core");
var LocationService = /** @class */ (function () {
    function LocationService() {
    }
    LocationService.prototype.parse = function (value) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        var parts = this.splitLocation(value);
        if (parts.length !== 2)
            return null;
        return this.tryParseDMSCordinates(parts) || this.tryParseDMCordinates(parts) || this.tryParseDecimalCordinates(parts) || this.tryParseSubfixedCordinates(parts);
    };
    LocationService.prototype.splitDecimalLocation = function (value) {
        if (!value)
            return null;
        var parts = this.splitLocation(value);
        if (parts.length !== 2)
            return null;
        return { latitude: +parts[0], longitude: +parts[1] };
    };
    LocationService.prototype.validDMSLocation = function (value) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        var parts = this.splitLocation(value);
        return parts.length === 2 && this.tryParseDMSCordinates(parts) !== null;
    };
    LocationService.prototype.validDMLocation = function (value) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        var parts = this.splitLocation(value);
        return parts.length === 2 && this.tryParseDMCordinates(parts) !== null;
    };
    LocationService.prototype.validLatitude = function (value) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        var lat = this.isFloatANumber(value) ? +value : 999;
        return lat >= -90 && lat <= 90;
    };
    LocationService.prototype.validLongitude = function (value) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        var lng = this.isFloatANumber(value) ? +value : 999;
        return lng >= -180 && lng <= 180;
    };
    LocationService.prototype.convertToDMS = function (latitude, longitude) {
        var latDMS = this.decimalToDMS(latitude, latitude >= 0 ? "N" : "S");
        var lngDMS = this.decimalToDMS(longitude, longitude >= 0 ? "E" : "W");
        return latDMS + " " + lngDMS;
    };
    LocationService.prototype.convertLatToDMS = function (latitude) {
        var latDMS = this.decimalToDMS(latitude, latitude >= 0 ? "N" : "S");
        return "" + latDMS;
    };
    LocationService.prototype.convertLongToDMS = function (longitude) {
        var lngDMS = this.decimalToDMS(longitude, longitude >= 0 ? "E" : "W");
        return "" + lngDMS;
    };
    LocationService.prototype.nmToMeters = function (nm) {
        return nm / 0.000539957;
    };
    LocationService.prototype.feetToMeters = function (ft) {
        return ft / 3.28084;
    };
    LocationService.prototype.decimalToDMS = function (value, dir) {
        var abs = Math.abs(value);
        var deg = Math.trunc(abs);
        var min = Math.trunc((abs - deg) * 60.0);
        var sec = Math.round((abs - deg - min / 60.0) * 3600.0);
        return "" + deg + this.pad2Digits(min) + this.pad2Digits(sec > 59 ? 59 : sec) + dir;
    };
    LocationService.prototype.pad2Digits = function (n) {
        var nStr = "" + n;
        return nStr.length < 2 ? "0" + nStr : nStr.substring(0, 2);
    };
    LocationService.prototype.splitLocation = function (value) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        return (value || "").split(/\s+/).filter(function (s) { return !!s; });
    };
    LocationService.prototype.tryParseDMSCordinates = function (cords) {
        var latPat = /^([0-9]){6}[NS]$/;
        var lngPat = /^([0-9]){6,7}[WE]$/;
        return cords[0].match(latPat) && cords[1].match(lngPat) ? this.validateAndConvertCordinates(cords, this.parseDMSValue) : null;
    };
    LocationService.prototype.tryParseDMCordinates = function (cords) {
        var latPat = /^([0-9]){4}[NS]$/;
        var lngPat = /^([0-9]){4,5}[WE]$/;
        return cords[0].match(latPat) && cords[1].match(lngPat) ? this.validateAndConvertCordinates(cords, this.parseDMValue) : null;
    };
    LocationService.prototype.tryParseDecimalCordinates = function (cords) {
        var latPatD = /^[+-]?[0-9]{1,2}([.][0-9]+)?$/;
        var lngPatD = /^[+-]?[0-9]{1,3}([.][0-9]+)?$/;
        if (cords[0].match(latPatD) && cords[1].match(lngPatD))
            return this.validateAndConvertCordinates(cords, parseSafeValue);
        var latPatDM = /^[+-]?[0-9]{4,4}([.][0-9]+)?$/;
        var lngPatDM = /^[+-]?[0-9]{4,5}([.][0-9]+)?$/;
        if (cords[0].match(latPatDM) && cords[1].match(lngPatDM))
            return this.validateAndConvertCordinates(cords, parseSafeDMValue);
        return null;
    };
    LocationService.prototype.tryParseSubfixedCordinates = function (cords) {
        var latPatD = /^[0-9]{1,2}([.][0-9]+)?[NS]$/;
        var lngPatD = /^[0-9]{1,3}([.][0-9]+)?[WE]$/;
        if (cords[0].match(latPatD) && cords[1].match(lngPatD))
            return this.validateAndConvertCordinates(cords, this.parseSufixedValue);
        var latPatDM = /^[0-9]{4,4}([.][0-9]+)?[NS]$/;
        var lngPatDM = /^[0-9]{4,5}([.][0-9]+)?[WE]$/;
        if (cords[0].match(latPatDM) && cords[1].match(lngPatDM))
            return this.validateAndConvertCordinates(cords, this.parseSufixedDMValue);
        return null;
    };
    LocationService.prototype.validateAndConvertCordinates = function (cords, parseFun) {
        var lat = parseFun.apply(this, [cords[0], 360.0]);
        var lng = parseFun.apply(this, [cords[1], 360.0]);
        if (this.validateDecimalCoordinates(lat, lng)) {
            return {
                latitude: lat,
                longitude: lng
            };
        }
        return null;
    };
    LocationService.prototype.validateDecimalCoordinates = function (lat, lng) {
        return this.validateDegrees(lat, 90) && this.validateDegrees(lng, 180);
    };
    LocationService.prototype.validateDegrees = function (value, max) {
        return value >= -max && value <= max;
    };
    LocationService.prototype.parseSufixedValue = function (str, defValue) {
        var len = str.length;
        var sufix = str[len - 1];
        var sing = sufix === 'W' || sufix === 'S' ? -1 : 1;
        return sing * parseSafeValue(str.substring(0, len - 1), defValue);
    };
    LocationService.prototype.parseSufixedDMValue = function (str, defValue) {
        var len = str.length;
        var sufix = str[len - 1];
        var sing = sufix === 'W' || sufix === 'S' ? -1 : 1;
        return sing * parseSafeDMValue(str.substring(0, len - 1), defValue);
    };
    LocationService.prototype.parseDMSValue = function (str, defValue) {
        try {
            var len = str.length;
            var degrees = parseInt(len === 7 ? str.substring(0, 2) : str.substring(0, 3));
            var minutes = parseInt(len === 7 ? str.substring(2, 4) : str.substring(3, 5));
            var seconds = parseInt(len === 7 ? str.substring(4, 6) : str.substring(5, 7));
            if (+minutes > 59 || +seconds > 59)
                return defValue;
            var sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);
            var sing = sufx === 'W' || sufx === 'S' ? -1 : 1;
            return sing * (degrees + (minutes * 60.0 + seconds) / 3600.0);
        }
        catch (e) {
            return defValue;
        }
    };
    LocationService.prototype.parseDMValue = function (str, defValue) {
        var len = str.length;
        if (len > 1) {
            var coord = str.substring(0, len - 1);
            var dir = str.substring(len - 1);
            return this.parseDMSValue(coord + "00" + dir, defValue);
        }
        return defValue;
    };
    LocationService.prototype.isFloatANumber = function (str) {
        return !isNaN(parseFloat(str));
    };
    LocationService = __decorate([
        core_1.Injectable()
    ], LocationService);
    return LocationService;
}());
exports.LocationService = LocationService;
function parseSafeValue(str, defValue) {
    try {
        return parseFloat(str);
    }
    catch (e) {
        return defValue;
    }
}
function parseSafeDMValue(str, defValue) {
    try {
        var value = parseFloat(str);
        if (Math.abs(value) > 1000.0) {
            var sign = Math.sign(value);
            var absValue = Math.abs(value);
            var degMin = Math.trunc(absValue);
            var degrees = Math.trunc(degMin / 100.0);
            var minutes = Math.abs(degMin % 100);
            if (minutes > 59)
                return defValue;
            var seconds = Math.abs(absValue - degMin) * 60;
            return sign * (degrees + (minutes * 60.0 + seconds) / 3600.0);
        }
    }
    catch (e) {
    }
    return defValue;
}
//# sourceMappingURL=location.service.js.map