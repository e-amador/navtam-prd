"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotifyService = void 0;
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var NotifyService = /** @class */ (function () {
    function NotifyService() {
        this.categoryChangedSubject = new Subject_1.Subject();
    }
    NotifyService.prototype.subscribeCategorySubjectEvent = function () {
        return this.categoryChangedSubject.asObservable();
    };
    NotifyService.prototype.emitCategorySubjectEvent = function (categoryId) {
        this.categoryChangedSubject.next(categoryId);
    };
    NotifyService = __decorate([
        core_1.Injectable()
    ], NotifyService);
    return NotifyService;
}());
exports.NotifyService = NotifyService;
//# sourceMappingURL=notify.service.js.map