﻿import { Injectable } from '@angular/core';

export interface ILocation {
    latitude: number,
    longitude: number
}

@Injectable()
export class LocationService {

    parse(value: string): ILocation {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        const parts = this.splitLocation(value);

        if (parts.length !== 2)
            return null;

        return this.tryParseDMSCordinates(parts) || this.tryParseDMCordinates(parts) ||this.tryParseDecimalCordinates(parts) || this.tryParseSubfixedCordinates(parts);
    }

    splitDecimalLocation(value: string): ILocation {
        if (!value)
            return null;

        const parts = this.splitLocation(value);

        if (parts.length !== 2)
            return null;

        return { latitude: +parts[0], longitude: +parts[1] };
    }

    validDMSLocation(value: string): boolean {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        const parts = this.splitLocation(value);
        return parts.length === 2 && this.tryParseDMSCordinates(parts) !== null;
    }

    validDMLocation(value: string): boolean {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        const parts = this.splitLocation(value);
        return parts.length === 2 && this.tryParseDMCordinates(parts) !== null;
    }

    validLatitude(value: string) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        const lat = this.isFloatANumber(value) ? +value : 999;
        return lat >= -90 && lat <= 90;
    }

    validLongitude(value: string) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        const lng = this.isFloatANumber(value) ? +value : 999;
        return lng >= -180 && lng <= 180;
    }

    convertToDMS(latitude: number, longitude: number): string {
        const latDMS = this.decimalToDMS(latitude, latitude >= 0 ? "N" : "S");
        const lngDMS = this.decimalToDMS(longitude, longitude >= 0 ? "E" : "W");
        return `${latDMS} ${lngDMS}`;
    } 

    convertLatToDMS(latitude: number): string {
        const latDMS = this.decimalToDMS(latitude, latitude >= 0 ? "N" : "S");
        return `${latDMS}`;
    }

    convertLongToDMS(longitude: number): string {
        const lngDMS = this.decimalToDMS(longitude, longitude >= 0 ? "E" : "W");
        return `${lngDMS}`;
    } 

    nmToMeters(nm: number): number {
        return nm / 0.000539957;
    }

    feetToMeters(ft: number): number {
    return ft / 3.28084;
    }

    private decimalToDMS(value: number, dir: string): string {
        const abs = Math.abs(value);
        const deg = Math.trunc(abs);
        const min = Math.trunc((abs - deg) * 60.0);
        const sec = Math.round((abs - deg - min / 60.0) * 3600.0);
        return `${deg}${this.pad2Digits(min)}${this.pad2Digits(sec > 59 ? 59 : sec)}${dir}`;
    }

    private pad2Digits(n: number): string {
        const nStr = `${n}`;
        return nStr.length < 2 ? `0${nStr}` : nStr.substring(0, 2);
    }

    private splitLocation(value: string) {
        if (typeof (value) === 'string' && value !== null)
            value = value.toUpperCase();
        return (value || "").split(/\s+/).filter((s) => !!s);
    }

    private tryParseDMSCordinates(cords: string[]) {
        const latPat = /^([0-9]){6}[NS]$/;
        const lngPat = /^([0-9]){6,7}[WE]$/;
        return cords[0].match(latPat) && cords[1].match(lngPat) ? this.validateAndConvertCordinates(cords, this.parseDMSValue) : null;
    }

    private tryParseDMCordinates(cords: string[]) {
        const latPat = /^([0-9]){4}[NS]$/;
        const lngPat = /^([0-9]){4,5}[WE]$/;
        return cords[0].match(latPat) && cords[1].match(lngPat) ? this.validateAndConvertCordinates(cords, this.parseDMValue) : null;
    }

    private tryParseDecimalCordinates(cords: string[]) {
        const latPatD = /^[+-]?[0-9]{1,2}([.][0-9]+)?$/;
        const lngPatD = /^[+-]?[0-9]{1,3}([.][0-9]+)?$/;
        if (cords[0].match(latPatD) && cords[1].match(lngPatD)) 
            return this.validateAndConvertCordinates(cords, parseSafeValue);

        const latPatDM = /^[+-]?[0-9]{4,4}([.][0-9]+)?$/;
        const lngPatDM = /^[+-]?[0-9]{4,5}([.][0-9]+)?$/;
        if (cords[0].match(latPatDM) && cords[1].match(lngPatDM))
            return this.validateAndConvertCordinates(cords, parseSafeDMValue);

        return null;
    }

    private tryParseSubfixedCordinates(cords: string[]) {
        const latPatD = /^[0-9]{1,2}([.][0-9]+)?[NS]$/;
        const lngPatD = /^[0-9]{1,3}([.][0-9]+)?[WE]$/;
        if (cords[0].match(latPatD) && cords[1].match(lngPatD))
            return this.validateAndConvertCordinates(cords, this.parseSufixedValue);

        const latPatDM = /^[0-9]{4,4}([.][0-9]+)?[NS]$/;
        const lngPatDM = /^[0-9]{4,5}([.][0-9]+)?[WE]$/;
        if (cords[0].match(latPatDM) && cords[1].match(lngPatDM))
            return this.validateAndConvertCordinates(cords, this.parseSufixedDMValue);

        return null;
    }

    private validateAndConvertCordinates(cords: string[], parseFun: Function): ILocation {
        const lat = parseFun.apply(this, [cords[0], 360.0]);
        const lng = parseFun.apply(this, [cords[1], 360.0]);
        if (this.validateDecimalCoordinates(lat, lng)) {
            return {
                latitude: lat,
                longitude: lng
            }
        }
        return null;
    }

    private validateDecimalCoordinates(lat: number, lng: number) {
        return this.validateDegrees(lat, 90) && this.validateDegrees(lng, 180);
    }

    private validateDegrees(value: number, max: number) {
        return value >= -max && value <= max;
    }

    private parseSufixedValue(str: string, defValue: number) {
        const len = str.length;
        const sufix = str[len - 1];
        var sing = sufix === 'W' || sufix === 'S' ? -1 : 1;
        return sing * parseSafeValue(str.substring(0, len - 1), defValue);
    }

    private parseSufixedDMValue(str: string, defValue: number) {
        const len = str.length;
        const sufix = str[len - 1];
        var sing = sufix === 'W' || sufix === 'S' ? -1 : 1;
        return sing * parseSafeDMValue(str.substring(0, len - 1), defValue);
    }

    private parseDMSValue(str: string, defValue: number) {
        try {
            const len = str.length;
            const degrees = parseInt(len === 7 ? str.substring(0, 2) : str.substring(0, 3));
            const minutes = parseInt(len === 7 ? str.substring(2, 4) : str.substring(3, 5));
            const seconds = parseInt(len === 7 ? str.substring(4, 6) : str.substring(5, 7));

            if (+minutes > 59 || +seconds > 59)
                return defValue;

            const sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);
            var sing = sufx === 'W' || sufx === 'S' ? -1 : 1;

            return sing * (degrees + (minutes * 60.0 + seconds) / 3600.0);
        } catch (e) {
            return defValue;
        }
    }

    private parseDMValue(str: string, defValue: number) {
        const len = str.length;
        if (len > 1) {
            const coord = str.substring(0, len - 1);
            const dir = str.substring(len - 1);
            return this.parseDMSValue(`${coord}00${dir}`, defValue);
        }
        return defValue;
    }

    private isFloatANumber(str: string): boolean {
        return !isNaN(parseFloat(str));
    }
}

function parseSafeValue(str: string, defValue: number) {
    try {
        return parseFloat(str);
    } catch (e) {
        return defValue;
    }
}

function parseSafeDMValue(str: string, defValue: number) {
    try {
        const value = parseFloat(str);
        if (Math.abs(value) > 1000.0) {

            const sign = Math.sign(value);
            const absValue = Math.abs(value);
            const degMin = Math.trunc(absValue);
            
            const degrees = Math.trunc(degMin / 100.0);
            const minutes = Math.abs(degMin % 100);

            if (minutes > 59)
                return defValue;

            const seconds = Math.abs(absValue - degMin) * 60;

            return sign * (degrees + (minutes * 60.0 + seconds) / 3600.0);
        }
    } catch (e) {
    }
    return defValue;
}