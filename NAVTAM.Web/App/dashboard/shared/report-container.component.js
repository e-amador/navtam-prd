"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportContainerComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var windowRef_service_1 = require("../../common/windowRef.service");
var ReportContainerComponent = /** @class */ (function () {
    function ReportContainerComponent(toastr, translation, winRef) {
        this.toastr = toastr;
        this.translation = translation;
        this.winRef = winRef;
        this.currentReportType = 1;
        this.isNofUser = true;
        this.isNofUser = this.winRef.appConfig.isNof;
    }
    ReportContainerComponent.prototype.ngOnInit = function () {
    };
    ReportContainerComponent.prototype.checkIfNofUser = function () {
        return this.isNofUser;
    };
    ReportContainerComponent.prototype.onReportChange = function (reportType) {
        this.currentReportType = reportType;
    };
    ReportContainerComponent.prototype.exportTableToCSV = function ($table, filename) {
        var $headers = $table.find('tr:has(th)'), $rows = $table.find('tr:has(td)')
        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        , tmpColDelim = String.fromCharCode(11) // vertical tab character
        , tmpRowDelim = String.fromCharCode(0) // null character
        // actual delimiter characters for CSV format
        , colDelim = '","', rowDelim = '"\r\n"';
        // Grab text from table into CSV formatted string
        var csv = '"';
        csv += formatRows($headers.map(grabRow));
        csv += rowDelim;
        csv += formatRows($rows.map(grabRow)) + '"';
        // Data URI
        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
        // For IE (tested 10+)
        if (window.navigator.msSaveOrOpenBlob) {
            var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                type: "text/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, filename);
        }
        else {
            $(this)
                .attr({
                'download': filename,
                'href': csvData
                //,'target' : '_blank' //if you want it to open in a new window
            });
        }
        //------------------------------------------------------------
        // Helper Functions 
        //------------------------------------------------------------
        // Format the output so it has the appropriate delimiters
        function formatRows(rows) {
            return rows.get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim);
        }
        // Grab and format a row from the table
        function grabRow(i, row) {
            var $row = $(row);
            //for some reason $cols = $row.find('td') || $row.find('th') won't work...
            var $cols = $row.find('td');
            if (!$cols.length)
                $cols = $row.find('th');
            return $cols.map(grabCol)
                .get().join(tmpColDelim);
        }
        // Grab and format a column from the table 
        function grabCol(j, col) {
            var $col = $(col), $text = $col.text();
            return $text.replace('"', '""'); // escape double quotes
        }
    };
    ReportContainerComponent = __decorate([
        core_1.Component({
            selector: 'nof-report-container',
            templateUrl: '/app/dashboard/shared/report-container.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, angular_l10n_1.TranslationService, windowRef_service_1.WindowRef])
    ], ReportContainerComponent);
    return ReportContainerComponent;
}());
exports.ReportContainerComponent = ReportContainerComponent;
//# sourceMappingURL=report-container.component.js.map