﻿import { Component, OnInit, Inject, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from '../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'

import { Subject } from 'rxjs/Subject';

import { Select2OptionData } from 'ng2-select2';

import { TranslationService } from 'angular-l10n';

import { DataService } from '../shared/data.service'
import { NofQueues } from '../shared/data.model'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import {
} from '../shared/data.model'

import * as moment from 'moment';
declare var $: any;

@Component({
    selector: 'report-stats',
    styleUrls: ['app/dashboard/shared/report-stats.component.css'],
    templateUrl: '/app/dashboard/shared/report-stats.component.html'
})
export class ReportStatsComponent implements OnInit {
    @Input('exportFunc') exportFunc: any;

    stats: any;

    loadingData: boolean = true;

    start: any = null;
    end: any = null;
    periodAggregation: number = 0;
    periodOptions: any;
    currentPeriodOption: string = '0';

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        private translation: TranslationService) {

        this.periodOptions =
           [
            { id: '0', text: this.translation.translate('Reports.Yearly') },
            { id: '1', text: this.translation.translate('Reports.Monthly') },
            { id: '2', text: this.translation.translate('Reports.Weekly') },
            { id: '4', text: this.translation.translate('Reports.Daily') },
            { id: '3', text: this.translation.translate('Reports.Hourly') }
            ];

        this.stats = [];
    }

    ngOnInit() {
        this.setDataPickers("years");

        this.getStats();
    }

    ngAfterViewInit() {
        this.exportReport();
    }


    dateChanged(ev) {
        switch (ev.currentTarget.id) {
            case "rpt-stats-startdate":
                this.start = moment(new Date(ev.currentTarget.value));

                if (this.end && this.end.isValid()) {
                    let el = $('#rpt-stats-startdate');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
            case "rpt-stats-enddate":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    let el = $('#rpt-stats-enddate');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        let period = +this.currentPeriodOption;
                        if (period < 2) { //years or months
                            this.end = period === 0 ? moment(this.end).endOf('year') : moment(this.end).endOf('month');
                            el.bootstrapDatepicker('update', this.end.format('MM/DD/YYYY'));
                        }
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    }

    onPeriodOptionsChanged(e: any): void {
        this.currentPeriodOption = e.value;      

        switch (this.currentPeriodOption) {
            case '0': this.setDataPickers("years"); break;
            case '1': this.setDataPickers("months"); break;
            default : this.setDataPickers("days"); break;
        }

        //if (this.currentPeriodOption == '3') {
        $('#rpt-stats-enddate').bootstrapDatepicker('setDate', null);
        $('#rpt-stats-startdate').bootstrapDatepicker('setDate', null);
        //}
    }

    onRowSelected(rowId) {
    }

    isStartHasValue() {
        return $('#rpt-stats-startdate').bootstrapDatepicker('getDate') != null;
    }

    isEndHaveValue(): boolean {
        return $('#rpt-stats-enddate').bootstrapDatepicker('getDate') != null;
    }

    isRunReportButtonEnabled() {
        return this.currentPeriodOption !== '3' ? this.isStartHasValue() && this.isEndHaveValue() : this.isStartHasValue();
    }

    runReport() {
        this.getStats();
    }

    exportReport() {
        let self = this;
        $("#export").click(function (event) {
            let outputFile = window.prompt(self.translation.translate('Reports.PromptFilename'));;

            if (outputFile) {
                outputFile = outputFile.replace('.csv', '') + '.csv'

                // CSV
                self.exportFunc.apply(this, [$('#rpt-stats-data'), outputFile]);
            }
        });
    }

    private getStats() {
        try {
            this.loadingData = true;

            let start = this.start.toDate();
            let end = this.currentPeriodOption !== '3' ? this.end.toDate() : start;

            this.dataService.getNofReportStats(start, end, +this.currentPeriodOption)
                .subscribe((stats: any) => {
                    this.processStats(stats);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processStats(stats: any) {
        for (var iter = 0; iter < stats.length; iter++) {
            stats[iter].index = iter;
            stats[iter].totals = false;
            if (stats[iter].startPeriod === "" && stats[iter].endPeriod === "") {
                stats[iter].startPeriod = "Totals";
                stats[iter].totals = true;
            }
        }

        if (stats.length > 0) {
            this.stats = stats;
            let index = 0;

            //this.stats[0].isSelected = true;

            this.loadingData = false;
            //this.onRowSelected(0);
        } else {
            this.stats = [];
            //this.selectedNotam = null;
            this.loadingData = false;
        }
    }

    private setDataPickers( period ) {
        let self = this;

        $('.rpt-stats-datepicker').each(function () {
            $(this).bootstrapDatepicker().off('changeDate' );
        });

        $("#rpt-stats-startdate").bootstrapDatepicker("remove");
        $("#rpt-stats-enddate").bootstrapDatepicker("remove");

        $('.rpt-stats-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                viewMode: period,
                minViewMode: period,
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0, // 0: month view , 1: year view, 2: multiple year view
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "", // Disable 1 or various day. For monday and thursday: 1,3
                calendarWeeks: false, // Display week number 
                toggleActive: true, // Close other when open
                multidate: false, // Allow to select various days
            }).on('changeDate', function event_handler (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });
    }
}

