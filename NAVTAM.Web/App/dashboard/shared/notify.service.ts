﻿import {Injectable} from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class NotifyService {

    private categoryChangedSubject : Subject<number> = new Subject<number>();

    subscribeCategorySubjectEvent() : Observable<number> {
        return this.categoryChangedSubject.asObservable();
    }

    emitCategorySubjectEvent(categoryId: number) {
        this.categoryChangedSubject.next(categoryId);
    }
}