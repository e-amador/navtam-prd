"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CacheService = (function () {
    function CacheService() {
        this._cache = [];
    }
    CacheService.prototype.add = function (key, value) {
        var index = this._cache.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            this._cache[index].value = value;
        }
        else {
            this._cache.push({
                key: key,
                value: value
            });
        }
    };
    CacheService.prototype.get = function (key) {
        var index = this._cache.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            return this._cache[index].value;
        }
        return null;
    };
    return CacheService;
}());
CacheService = __decorate([
    core_1.Injectable()
], CacheService);
exports.CacheService = CacheService;
//# sourceMappingURL=cache.service.js.map