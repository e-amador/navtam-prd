"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataService = void 0;
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var http_1 = require("@angular/http");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_model_1 = require("./data.model");
var DataService = /** @class */ (function () {
    function DataService(http, winRef /*, private tokenService: BearerTokenService*/) {
        this.http = http;
        this.winRef = winRef;
        this.overrideBilingual = false;
        this.apiUrl = winRef.nativeWindow['app'].apiUrl;
        this.app = winRef.nativeWindow['app'];
        this.override = new Rx_1.BehaviorSubject(this.overrideBilingual);
    }
    DataService.prototype.getSdoExtendedName = function (sdo) {
        var extName = sdo.designator + ' ';
        if (sdo.servedCity.length > 0)
            extName += sdo.servedCity + '/';
        extName += sdo.name;
        return extName;
    };
    DataService.prototype.getSdoSubject = function (subId) {
        //this.setBearerToken();
        return this.http.get(this.app.apiUrl + ("sdosubjects/find/?subId=" + subId)).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getAerodromes = function () {
        //this.setBearerToken();
        return this.http.get(this.app.apiUrl + "sdosubjects/aerodromes").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getFirs = function () {
        return this.http.get(this.app.apiUrl + "sdosubjects/GetAllFIRAsync").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getMapHealth = function () {
        return this.http.get(this.apiUrl + "map/getmaphealth").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNsdCategories = function () {
        var _this = this;
        if (this.nsdCategories) {
            return Rx_1.Observable.of(this.nsdCategories);
        }
        //get series first time categories are loaded;
        this.getSeries().subscribe(function (series) { _this.series = series; });
        return this.http.get(this.apiUrl + "nsdcategories/activeorg").map(function (response) {
            _this.nsdCategories = response.json();
            return _this.nsdCategories;
        }).catch(this.handleError);
    };
    DataService.prototype.getSeries = function () {
        if (this.series) {
            return Rx_1.Observable.of(this.series);
        }
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "configvalues/series/")
            .map(function (response) {
            var series = [];
            response.json().forEach(function (serie) {
                series.push({ id: serie, text: serie });
            });
            return series;
        }).catch(this.handleError);
    };
    DataService.prototype.getItemD = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "itemd/" + proposalId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveItemD = function (itemD) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "itemd", itemD)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateItemD = function (freetext) {
        //this.setBearerToken();
        var viewmodel = { itemD: freetext };
        return this.http.post(this.apiUrl + "itemd/validate", viewmodel)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.loadItemD = function (proposalId) {
        return this.http.get(this.apiUrl + "itemd/" + proposalId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteItemD = function (proposalId) {
        //this.setBearerToken();
        return this.http.delete(this.apiUrl + "itemd/" + proposalId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalsByCategory = function (categoryId, queryOptions) {
        this.selectedCategoryId = categoryId;
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/" + categoryId, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getGroupedProposals = function (groupId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposals/grouped/" + groupId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.searchProposals = function (filterCriteria, queryOptions) {
        var data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions
        };
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposalhistory/filter", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.filterProposals = function (filterCriteria, queryOptions) {
        var data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions
        };
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/filter", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalStatus = function (ids, refreshRate, lastSyncStatusCheck) {
        var date;
        try {
            date = lastSyncStatusCheck.toISOString();
        }
        catch (err) {
            date = "";
        }
        return this.http.post(this.apiUrl + "proposals/status/?refreshRate=" + refreshRate + "&since=" + date, ids)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNotamIcaoFormat = function (notamId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/notams/icaoformat/" + notamId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.canNotamBeReplaced = function (notamId) {
        //this.setBearerToken();
        var notamIdStr = notamId.replace("/", "_");
        return this.http.get(this.apiUrl + "nof/notams/canbereplaced/" + notamIdStr).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.verifyObsoleteNotam = function (id) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/notams/verifyobsolete/" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //NDS Clients subscriptions
    DataService.prototype.getNdsClient = function (id) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/ndsclients/" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNdsClients = function (queryOptions) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/ndsclients/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNdsClientAddresses = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/ndsclients")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveNdsClient = function (client) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/ndsclients/", client)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateNdsClient = function (client) {
        //this.setBearerToken();
        return this.http.put(this.apiUrl + "nof/ndsclients/", client)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteNdsClient = function (clientId) {
        //this.setBearerToken();
        return this.http.delete(this.apiUrl + "nof/ndsclients/" + clientId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.sendNotamsToSubscriptions = function (clientIds) {
        var viewmodel = { subsClientIds: clientIds };
        return this.http.post(this.apiUrl + "transition/queuejob/", viewmodel)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.existsClientName = function (id, name) {
        return this.http.get(this.apiUrl + "nof/ndsclients/exists/?Id=" + id + "&name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //GeoRef functions
    DataService.prototype.saveGeoFile = function (data) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/ndsclients/uploadfile", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getGeoFeatures = function (region) {
        return this.http.post(this.apiUrl + "nof/ndsclients/GetGeoFeatures/", region)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateGeoLocation = function (loc) {
        return this.http.get(this.apiUrl + "nof/ndsclients/validatepoint/?value=" + loc)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateGeoPoints = function (points) {
        return this.http.get(this.apiUrl + "nof/ndsclients/validatepoints/?value=" + points)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //End GeoRef functions
    //Message Exchange Queues
    DataService.prototype.lockMessage = function (id, takeOwnership) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/lock/" + id + "/" + takeOwnership)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.isMessageLocked = function (id) {
        return this.http.get(this.apiUrl + "nof/messages/islocked/" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.unlockMessage = function (id) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/unlock/" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getMessage = function (id) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getIncomingMessages = function (queryOptions, status) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/incomingmessages/page/" + status, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getOutgoingMessages = function (queryOptions, status) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/outgoingmessages/page/" + status, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getParkedMessages = function (queryOptions, status) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/parkedmessages/page/" + status, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateMessage = function (message) {
        //this.setBearerToken();
        return this.http.put(this.apiUrl + "nof/incomingmessages", message)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.sendQueueMessage = function (message) {
        return this.http.post(this.apiUrl + "nof/outgoingmessages", message)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteIncomingMessage = function (messageId) {
        //this.setBearerToken();
        return this.http.delete(this.apiUrl + "nof/incomingmessages/" + messageId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.getMessageTemplates = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/templates")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getMessageTemplateNames = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/templates/names")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteMessageTemplate = function (templateId) {
        //this.setBearerToken();
        return this.http.delete(this.apiUrl + "nof/messages/templates/" + templateId)
            .map(function (response) {
            return response; //Non-content (204)
        }).catch(this.handleError);
    };
    DataService.prototype.getMessageTemplate = function (id) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/templates/" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveMessageTemplate = function (template) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/messages/templates/", template)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateMessageTemplate = function (template) {
        //this.setBearerToken();
        return this.http.put(this.apiUrl + "nof/messages/templates/", template)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getTotalParkedMessages = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/park/count").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getTotalUnreadMessages = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/messages/unread/count").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.filterNotams = function (filterCriteria, queryOptions, filterBy) {
        var data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions,
            filterBy: filterBy
        };
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/notams/filter", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.filterReportNotams = function (filterCriteria, queryOptions, startdate, enddate, filterBy) {
        var data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions,
            start: startdate,
            end: enddate,
            filterBy: filterBy
        };
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/notams/reportfilter", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.filterReportActiveNotams = function (filterCriteria, queryOptions, startdate, starttime, endtime) {
        var data = {
            Conditions: filterCriteria,
            queryOptions: queryOptions,
            start: startdate,
            startTime: starttime,
            endTime: endtime
        };
        return this.http.post(this.apiUrl + "nof/notams/reportactivefilter", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveQueryFilter = function (slotNumber, filterCriteria, filterType) {
        var data = {
            username: window['app'].usrName,
            filterName: 'F' + slotNumber,
            slotNumber: slotNumber,
            userFilterType: filterType,
            filterConditions: filterCriteria
        };
        var endpoint = this.app.isNof
            ? this.apiUrl + "nof/queryfilter"
            : this.apiUrl + "proposalhistory/queryfilter";
        //this.setBearerToken();
        return this.http.post(endpoint, data)
            .map(function (response) {
            return response.status;
        }).catch(this.handleError);
    };
    DataService.prototype.deleteQueryFilter = function (slotNumber, filterType) {
        var endpoint = this.app.isNof
            ? this.apiUrl + "nof/queryfilter/" + slotNumber + "/" + filterType
            : this.apiUrl + "proposalhistory/queryfilter/" + slotNumber;
        //this.setBearerToken();
        return this.http.delete(endpoint)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.getActiveQueryFilters = function (filterType) {
        var endpoint = this.app.isNof
            ? this.apiUrl + "nof/queryfilter/active/" + this.app.usrName + "/" + filterType
            : this.apiUrl + "proposalhistory/queryfilter/active/" + this.app.usrName;
        //this.setBearerToken();
        return this.http.get(endpoint).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getQueryFilterNames = function (username) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/queryfiltername?username=" + username).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveQueryFilterName = function (username, slotNumber, filterName) {
        //this.setBearerToken();
        var data = {
            username: username,
            slotNumber: slotNumber,
            filterName: filterName
        };
        return this.http.post(this.apiUrl + "nof/queryfiltername", data).map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.getQueryFilter = function (slotNumber, filterType) {
        var endpoint = this.app.isNof
            ? this.apiUrl + "nof/queryfilter/" + slotNumber + "/" + this.app.usrName + "/" + filterType
            : this.apiUrl + "proposalhistory/queryfilter/" + slotNumber + "/" + this.app.usrName;
        //this.setBearerToken();
        return this.http.get(endpoint).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalsByParentCategory = function (parentCategoryId, queryOptions) {
        this.selectedCategoryId = parentCategoryId;
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/children/" + parentCategoryId, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofTotalPending = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/pending/count").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofTotalParked = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/park/count").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getExpiringTotal = function () {
        return this.http.get(this.apiUrl + "proposals/expiringcount").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getExpiringProposals = function (queryOptions) {
        return this.http.post(this.apiUrl + "proposals/expiring", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofTotalReviewed = function () {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/review/count").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofProposalsByQueueName = function (categoryId, queueName, queryOptions) {
        var actionName = this.endpointActionName(queueName);
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/" + actionName + "/" + categoryId, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNOfProposalsByParentCategoryAndQueueName = function (parentCategoryId, queueName, queryOptions) {
        this.selectedCategoryId = parentCategoryId;
        var actionName = this.endpointActionName(queueName);
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/" + actionName + "/children/" + parentCategoryId, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofGroupedProposals = function (groupId, queueName) {
        //this.setBearerToken();
        var actionName = this.endpointActionName(queueName);
        return this.http.get(this.apiUrl + "nof/proposals/grouped/" + actionName + "/" + groupId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofNotams = function (queryOptions) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/notams", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofReportNotams = function (start, end, queryOptions, filterBy) {
        //this.setBearerToken();
        var data = {
            start: start,
            end: end,
            queryOptions: queryOptions,
            filterBy: filterBy
        };
        return this.http.post(this.apiUrl + "nof/reportnotams", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofReportActiveNotams = function (start, starttime, endtime, queryOptions) {
        var data = {
            start: start,
            queryOptions: queryOptions,
            startTime: starttime,
            endTime: endtime,
        };
        return this.http.post(this.apiUrl + "nof/reportactivenotams", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.exportNofReportNotams = function (payload) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/reportnotams/csv", payload)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.exportNofReportActiveNotams = function (payload) {
        return this.http.post(this.apiUrl + "nof/reportactivenotams/csv", payload)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofReportStats = function (start, end, type) {
        //this.setBearerToken();
        var data = {
            start: start,
            end: end,
            aggregatePeriod: type
        };
        return this.http.post(this.apiUrl + "nof/reportstats", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNofNotamTrackingList = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nof/notams/trackinglist/" + proposalId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalDetails = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "nsdcategories/").map(function (response) {
            return {};
        }).catch(this.handleError);
    };
    DataService.prototype.getIcaoSubjects = function (entityCode) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "icaosubjects/filter/?code=" + entityCode)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getDoaLocation = function (doaId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "doas/GeoJson/?doaId=" + doaId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getSerie = function (subjectId, qCode) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "series/getserie/?subjectId=" + subjectId + "&qCode=" + qCode)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getIcaoConditions = function (icaoSubjectId, cancelledOnly) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "icaoconditions/filter/?subId=" + icaoSubjectId + "&cancelledOnly=" + cancelledOnly)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getEmptyProposalModel = function (categoryId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposals/getemptymodel?catid=" + categoryId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalModel = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposals/getmodel/" + proposalId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalModelEnforced = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposals/getmodelenforced/" + proposalId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalReadOnlyModel = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposals/getreadonlymodel?id=" + proposalId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalHistoryModel = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposalhistory/getmodel?id=" + proposalId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getProposalHistory = function (proposalId) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposalhistory/getall?proposalId=" + proposalId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.generateIcao = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "icao/GenerateIcao", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveDraftProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/SaveDraft", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveDraftGroupProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/SaveDraftGrouped", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.parkProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/parkproposal", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.parkGroupedProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/ParkGroupedProposals", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.rejectProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/rejectproposal", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.disseminateProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/diseminateproposal", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.disseminateGroupedProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/diseminategroupedproposal", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.replaceProposal = function (proposalId) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/replaceproposal/" + proposalId, null)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.cancelProposal = function (proposalId) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/cancelproposal/" + proposalId, null)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.cloneProposal = function (cloneReq) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/cloneproposal", cloneReq)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.cloneNotam = function (id) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/clonenotam/" + id, null)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNotamProposalReadOnlyModel = function (id) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposals/getnotamreadonlymodel?id=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.acknowledgeProposal = function (proposalId) {
        return this.http.post(this.apiUrl + "proposals/acknowledge/" + proposalId, null)
            .map(function (response) {
            return response; //Non-content (204)
        }).catch(this.handleError);
    };
    DataService.prototype.discardProposal = function (proposalId) {
        //this.setBearerToken();
        return this.http.delete(this.apiUrl + "proposals/discard/" + proposalId)
            .map(function (response) {
            return response; //Non-content (204)
        }).catch(this.handleError);
    };
    DataService.prototype.discardNofProposal = function (proposalId) {
        //this.setBearerToken();
        return this.http.delete(this.apiUrl + "nof/discard/" + proposalId)
            .map(function (response) {
            return response; //Non-content (204)
        }).catch(this.handleError);
    };
    DataService.prototype.submitProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/SubmitProposal", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.submitGroupedProposal = function (model) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/SubmitGroupedProposal", model)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveProposalAttachments = function (data) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/SaveAttachments", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveProposalAnnotations = function (proposalId, annotations) {
        //this.setBearerToken();
        var data = {
            proposalId: proposalId,
            annotations: annotations
        };
        return this.http.post(this.apiUrl + "proposals/annotations", data)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.updateOriginatorContactInfo = function (proposalId, originator, email, phone) {
        //this.setBearerToken();
        var data = {
            proposalId: proposalId,
            originator: originator,
            email: email,
            phone: phone
        };
        return this.http.put(this.apiUrl + "proposals/contactinfo", data)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.loadProposalAttachment = function (id) {
        //this.setBearerToken();
        return this.http.get(this.apiUrl + "proposals/attachments/" + id)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.deleteProposalAttachment = function (data) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/DeleteAttachments", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.restoreProposalStatus = function (proposalId, statusUpdated) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "proposals/restorestatus/" + proposalId + "/" + statusUpdated, null)
            .map(function (response) {
            return response.status === 200;
        }).catch(this.handleError);
    };
    DataService.prototype.restoreNofProposalStatus = function (proposalId, statusUpdated) {
        //this.setBearerToken();
        return this.http.post(this.apiUrl + "nof/proposals/restorestatus/" + proposalId + "/" + statusUpdated, null)
            .map(function (response) {
            return response.status === 200;
        }).catch(this.handleError);
    };
    DataService.prototype.addOriginatorInfo = function (originator) {
        return this.http.post(this.apiUrl + "originatorinfo/save", originator)
            .map(function (response) {
            return response.status === 200;
        }).catch(this.handleError);
    };
    DataService.prototype.removeOriginatorInfo = function (originatorId) {
        return this.http.delete(this.apiUrl + "originatorinfo/" + originatorId)
            .map(function (response) {
            return response.status === 200;
        }).catch(this.handleError);
    };
    DataService.prototype.findMatchingOriginatorInfos = function (originatorName, orgId) {
        originatorName = encodeURIComponent(originatorName);
        return this.http.get(this.apiUrl + "originatorinfo/filter?query=" + originatorName + "&size=10&orgId=" + orgId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.handleError = function (error) {
        if (error.status === 401) {
            window.location.reload();
        }
        return Rx_1.Observable.throw(error.json());
    };
    DataService.prototype.getNsds = function () {
        return this.http.get(this.apiUrl + "dashboard/getnsds")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.endpointActionName = function (queueName) {
        switch (queueName) {
            case data_model_1.NofQueues.Pending: return "pending";
            case data_model_1.NofQueues.Park: return "park";
            case data_model_1.NofQueues.Review: return "review";
        }
    };
    DataService.prototype.getPrevNotamIcaoText = function (notamId, action) {
        //this.setBearerToken();
        notamId = notamId.replace('/', '_');
        return this.http.get(this.apiUrl + "notams/getPrevIcaoText?notamId=" + notamId + "&action=" + action).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.calculateGeoRef = function (data) {
        return this.http.post(this.apiUrl + "georef/calculate/", data).map(function (response) {
            return response.json();
        }); //.catch(this.handleError);
    };
    DataService.prototype.calculateGeoRefByPoint = function (data) {
        return this.http.post(this.apiUrl + "georef/pointradius/", data).map(function (response) {
            return response.json();
        }); //.catch(this.handleError);
    };
    DataService.prototype.validateLocation = function (location) {
        return this.http
            .get(this.app.apiUrl + "doas/indoa?location=" + location)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DataService.prototype.updateProposalItemX = function (id, itemX) {
        //this.setBearerToken();
        return this.http.put(this.apiUrl + "nof/itemX", { proposalId: id, value: itemX })
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.nextOverrideBilingual = function (value) {
        this.overrideBilingual = value;
        this.override.next(this.overrideBilingual);
    };
    DataService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef /*, private tokenService: BearerTokenService*/])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data.service.js.map