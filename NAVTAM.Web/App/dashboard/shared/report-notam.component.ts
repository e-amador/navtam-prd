﻿import { Component, OnInit, Input, AfterViewInit, Inject, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from '../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'

import { LocaleService, TranslationService } from 'angular-l10n';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import {
    INotam,
    IPaging,
    ITableHeaders,
    IQueryOptions,
    ProposalType
} from '../shared/data.model'

import * as moment from 'moment';
import * as _ from 'lodash';

import { ReportFilterComponent } from './report-filter.component';

declare var $: any;

@Component({
    selector: 'report-notam',
    styleUrls: ['app/dashboard/shared/report-notam.component.css'],
    templateUrl: '/app/dashboard/shared/report-notam.component.html',
})
export class ReportNotamComponent implements OnInit, AfterViewInit  {
    @ViewChild(ReportFilterComponent) filter: ReportFilterComponent;
    @Input('exportFunc') exportFunc: any;

    notams: INotam[]; 
    selectedNotam: INotam;

    queryOptions: IQueryOptions;

    tableHeaders: ITableHeaders;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;
    loadingData: boolean = true;
    isFilterApplied: boolean = false;
    filterPannelOpen: boolean = false;
    filterByActive: string = 'All';

    start: any = null;
    end: any = null;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        private translation: TranslationService) {

        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'notamId', direction: 0 },
                    { columnName: 'type', direction: 0 },
                    { columnName: 'referredNotamId', direction: 0 },
                    { columnName: 'series', direction: 0 },
                    { columnName: 'itemA', direction: 0 },
                    { columnName: 'siteId', direction: 0 },
                    { columnName: 'permanent', direction: 0 },
                    { columnName: 'estimated', direction: 0 },
                    { columnName: 'itemE', direction: 0 },
                    { columnName: 'originator', direction: 0 },
                    { columnName: 'modifiedByUsr', direction: 0 },
                    { columnName: 'startActivity', direction: 0 },
                    { columnName: 'endValidity', direction: 0 },
                    { columnName: 'received', direction: 0 },
                    { columnName: 'published', direction: 1 }
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '25', text: '25' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '1000', text: '1000' },
                ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[3].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_Published"
        }
    }

    ngOnInit() {
        let self = this;

        $('.rpt-notam-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0, // 0: month view , 1: year view, 2: multiple year view
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "", // Disable 1 or various day. For monday and thursday: 1,3
                calendarWeeks: false, // Display week number 
                toggleActive: true, // Close other when open
                multidate: false, // Allow to select various days
            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });

        this.isFilterApplied = this.memStorageService.get(this.memStorageService.REPORT_FILTER_QUERY_OPTIONS_KEY);
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        } else {
            let queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY);
            if (queryOptions !== null) {
                this.queryOptions = queryOptions;
            }
        }

        if (this.isFilterApplied) {
            this.filter.runReportFilter(false, this.start.toDate(), this.end.toDate(), this.queryOptions.page, this.queryOptions.pageSize, this.filterByActive);
        } else {
            this.getNotams(this.queryOptions.page);
        }
    }

    ngAfterViewInit() {
        this.filter.parent = this;
        this.memStorageService.save(this.memStorageService.REPORT_FILTER_FILTER_BY_KEY, status);
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, this.queryOptions.pageSize, "All");
        }
        else {
            this.getNotams(1);
        }
    }

    onFilterChanged(event: any) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        } else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processNotams(event.data);
            } else {
                this.queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY);
                this.getNotams(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    }

    onPageChange(page: number) {
        this.getNotams(page);
    }

    dateChanged(ev) {
        switch (ev.currentTarget.id) {
            case "rpt-notam-startdate":
                this.start = moment(new Date(ev.currentTarget.value));

                if (this.end && this.end.isValid()) {
                    let el = $('#rpt-notam-startdate');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
            case "rpt-notam-enddate":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    let el = $('#rpt-notam-enddate');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }

        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, this.queryOptions.pageSize, "All");
        } else {
            this.getNotams(1);
        }
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }


    onRowSelected(index) {
        let notam = this.notams[index];
        if (this.selectedNotam) {
            this.selectedNotam.isSelected = false;
        }
        this.selectedNotam = notam
        this.selectedNotam.isSelected = true;
    }

    isStartHasValue() {
        return $('#rpt-notam-startdate').bootstrapDatepicker('getDate') != null;
    }

    isEndHaveValue(): boolean {
        return $('#rpt-notam-enddate').bootstrapDatepicker('getDate') != null;
    }

    isRunReportButtonEnabled() {
        return this.isStartHasValue() && this.isEndHaveValue();
    }

    runReport() {
        this.getNotams(1);
    }

    exportReport() {
        let filename = window.prompt(this.translation.translate('Reports.PromptFilename'));
        if (filename) {
            filename = filename.replace('.csv', '') + '.csv';

            this.loadingData = true;

            const payload = {
                Conditions: this.isFilterApplied ? this.filter.filters : null,
                queryOptions: this.queryOptions,
                start: this.start.toDate(),
                end: this.end.toDate(),
                filterBy: this.filterByActive 
            };

            let self = this;
            this.dataService.exportNofReportNotams(payload)
                .subscribe((response: any) => {
                    // For IE (tested 10+)
                    if (window.navigator.msSaveOrOpenBlob) {
                        var blob = new Blob([decodeURIComponent(encodeURI(response))], {
                            type: "text/csv;charset=utf-8;"
                        });
                        navigator.msSaveBlob(blob, filename);
                    } else {
                        var csvData = new Blob([response], { type: 'text/csv;charset=utf-8;' });
                        var csvURL = window.URL.createObjectURL(csvData);
                        var tempLink = document.createElement('a');
                        tempLink.href = csvURL;
                        tempLink.setAttribute('download', filename);
                        tempLink.click();
                    }
                    this.loadingData = false;
                });
        }
    }

    filterByActives(status: string) {
        this.filterByActive = status;
        this.memStorageService.save(this.memStorageService.REPORT_FILTER_FILTER_BY_KEY, status);

        this.notams = [];
        this.totalPages = 0;
        this.paging.totalCount = 0;
    }

    notamList() {
        if (this.notams) {
            if (this.filterByActive == 'All') {
                return this.notams;
            }

            if (this.filterByActive == 'Active') {
                let activeNotams = this.notams.filter(function (item: INotam) {
                    return !(item.isObsolete);
                });

                return activeNotams;
            } 

            if (this.filterByActive == 'Inactive') {
                let activeNotams = this.notams.filter(function (item: INotam) {
                    return (item.isObsolete);
                });

                return activeNotams;
            } 
        }
    }

    private getNotams(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runReportFilter(false, this.start.toDate(), this.end.toDate(), page, this.queryOptions.pageSize, this.filterByActive);
            }
            else {
                this.memStorageService.save(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY, this.queryOptions);
                this.dataService.getNofReportNotams(this.start.toDate(), this.end.toDate(), this.queryOptions, this.filterByActive)
                    .subscribe((response: any) => {
                        this.processNotams(response);
                    });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processNotams(notams: any) {
        this.paging = notams.paging;
        this.totalPages = notams.paging.totalPages;

        for (var iter = 0; iter < notams.data.length; iter++) {
            notams.data[iter].index = iter;
            notams.data[iter].typeStr = this.toNotamTypeStr(notams.data[iter].notamType);
        }

        const lastNotamSelected = this.memStorageService.get(this.memStorageService.NOTAM_ID_KEY);
        if (notams.data.length > 0) {
            this.notams = notams.data;
            let index = 0;
            if (lastNotamSelected) {
                index = this.notams.findIndex(x => x.id === lastNotamSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.notams[index].isSelected = true;
            this.selectedNotam = this.notams[index];
            this.loadingData = false;
            //this.onRowSelected(0);
        } else {
            this.notams = [];
            this.selectedNotam = null;
            this.loadingData = false;
        }

    }

    private toNotamTypeStr(notamType) {
        switch (notamType) {
            case ProposalType.New: return 'N';
            case ProposalType.Replace: return 'R';
            case ProposalType.Cancel: return 'C';
            default: return '';
        }
    }
}

