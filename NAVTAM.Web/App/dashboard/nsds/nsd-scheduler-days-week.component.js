"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdSchedulerDaysOfWeekComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var moment = require("moment");
var NsdSchedulerDaysOfWeekComponent = /** @class */ (function () {
    function NsdSchedulerDaysOfWeekComponent(windowRef, cdRef) {
        this.windowRef = windowRef;
        this.cdRef = cdRef;
        this.parent = null;
        this.lastEventId = -1;
        this.currentEvent = null;
        this.displayStartDate = null;
        this.slootDuration = 15;
        this.scrollTime = '06:00:00';
        this.initialized = false;
        this.data = null;
        this.isFormValid = false;
        this.SR = "06:00";
        this.SS = "18:00";
        this.eventDetails = {
            starttime: '',
            endtime: '',
            s_sunrise: false,
            s_sunset: false,
            e_sunrise: false,
            e_sunset: false,
            s_setTime: false,
            e_setTime: false
        };
        this.defaultColor = '#3a87ad';
        this.selectedColor = '#a1c5f761';
        this.startEventUpdate = null;
        this.start = null;
        this.end = null;
        this.starttime = null;
        this.endtime = null;
        this.sheduleCleared = false;
        this.weekEventsDic = {};
        this.repeatToggle = false;
        this.dailyRow = {
            starttime: "",
            endtime: ""
        };
        this.arrayOfKeys = Object.keys(this.weekEventsDic);
    }
    NsdSchedulerDaysOfWeekComponent.prototype.ngOnInit = function () {
    };
    NsdSchedulerDaysOfWeekComponent.prototype.ngOnDestroy = function () {
    };
    NsdSchedulerDaysOfWeekComponent.prototype.ngAfterViewInit = function () {
        var self = this;
        this.calendar = $('#calendar-weekly');
        $('.w-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0,
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "",
                calendarWeeks: false,
                toggleActive: true,
                multidate: false,
            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });
        $('#w-starttime').mask('00:00', { onComplete: function (cep) { self.starttime = cep; } });
        $('#w-endtime').mask('00:00', { onComplete: function (cep) { self.endtime = cep; } });
        $('#w-modal-starttime').mask('00:00', { onComplete: function (cep) { self.eventDetails.starttime = cep; } });
        $('#w-modal-endtime').mask('00:00', { onComplete: function (cep) { self.eventDetails.endtime = cep; } });
    };
    NsdSchedulerDaysOfWeekComponent.prototype.initialize = function (data, start, end, slotDuration, events) {
        this.data = data;
        var currentDate = moment();
        this.initCalendar(this.calendar, currentDate);
        if (this.data) {
            if (this.data.length > 0) {
                start = moment(start);
                end = moment(end);
                this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
                this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));
            }
            for (var i = 0; i < this.data.length; i++) {
                var obj = JSON.parse(this.data[i].event);
                var event_1 = this.setEvent(obj);
                if (i === 0) {
                    this.displayStartDate = start;
                    this.scrollTime = this.starttime + ":00";
                    currentDate = event_1.start;
                }
                this.starttime = start.format("HH:mm");
                this.endtime = end.format("HH:mm");
                this.calendar.fullCalendar('renderEvent', event_1, true);
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        }
        else if (events) {
            for (var i = 0; i < events.length; i++) {
                var evt = this.setEvent(events[i]);
                if (i === 0) {
                    currentDate = evt.start;
                }
                this.calendar.fullCalendar('renderEvent', evt, true);
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        }
        else {
            //trick to enforce refreshing the calendar when modifications were made with calendar hidden 
            this.calendar.fullCalendar('prev');
            this.calendar.fullCalendar('next');
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.toggleShedule = function (event, clearItemD) {
        if (clearItemD === void 0) { clearItemD = false; }
        var isPannelOpenNow = this.isPanelOpen();
        if (event) {
            event.preventDefault();
            this.repeatToggle = true;
        }
        else {
            this.repeatToggle = false;
        }
        if (!this.initialized) {
            this.slideToggle();
        }
        this.showSchedule(this.parent.type === 'clone' ? false : clearItemD);
        var isPannelOpenThen = this.isPanelOpen();
        if (isPannelOpenNow === isPannelOpenThen) {
            if (!isPannelOpenNow || !this.repeatToggle) {
                this.slideToggle();
            }
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.deleteEvents = function (event, clearItemD) {
        if (clearItemD === void 0) { clearItemD = false; }
        if (event && clearItemD) {
            if (this.data && this.data.length > 0) {
                this.parent.resetSchedulersStatus();
            }
            event.preventDefault();
        }
        this.weekEventsDic = {};
        this.parent.hasScheduler = false;
        this.data = new Object();
        this.start = null;
        this.end = null;
        this.starttime = null;
        this.endtime = null;
        this.clearSchedule(clearItemD);
        if (clearItemD) {
            var enforceToggle = this.parent.type === 'clone' || this.parent.model.proposalId;
            this.parent.schedulerDaily.toggleScheduleAfterDeleteAction(enforceToggle, true);
            this.parent.schedulerDate.toggleScheduleAfterDeleteAction(enforceToggle, true);
        }
        this.parent.enableActivePeridDates();
        this.data = null;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.removeEvents = function () {
        if (this.hasEvents()) {
            this.calendar.fullCalendar('removeEvents');
            this.weekEventsDic = {};
            this.data = null;
            this.start = null;
            this.end = null;
            this.starttime = null;
            this.endtime = null;
            $('#w-start').bootstrapDatepicker('update', '');
            $('#w-end').bootstrapDatepicker('update', '');
            this.toggleScheduleAfterDeleteAction(true, true);
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.clearSchedule = function (clearItemD) {
        if (this.data) {
            this.toggleScheduleAfterDeleteAction(true, this.initialized);
            $('#w-start').bootstrapDatepicker('update', '');
            $('#w-end').bootstrapDatepicker('update', '');
            this.calendar.fullCalendar('destroy');
            var start = null;
            var end = null;
            if (this.start) {
                var starttime = moment(this.starttime, 'HH:mm');
                var endtime = moment(this.endtime, 'HH:mm');
                start = moment(this.start).set({
                    hour: starttime.get('hour'),
                    minute: starttime.get('minute'),
                });
                end = moment(this.end).set({
                    hour: endtime.get('hour'),
                    minute: endtime.get('minute'),
                });
            }
            this.initialize((this.parent.type === 'clone' || this.parent.model.proposalId) ? this.data : null, start, end);
            if (start && start.isValid()) {
                this.calendar.fullCalendar('gotoDate', start);
            }
            else {
                this.calendar.fullCalendar('gotoDate', moment());
            }
        }
        else {
            if (clearItemD) {
                if (this.parent.type === 'clone') {
                    this.toggleScheduleAfterDeleteAction(this.parent.model.proposalId ? true : false, this.initialized);
                }
                else {
                    this.toggleScheduleAfterDeleteAction(this.sheduleCleared, this.hasEvents());
                    this.sheduleCleared = false;
                }
            }
            this.initialize(null, this.start, this.end);
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.toggleScheduleAfterDeleteAction = function (enforce, initialized) {
        if (enforce) {
            this.slideToggle();
            this.initialized = initialized;
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.resetClearScheduleFlag = function () {
        this.sheduleCleared = true;
        this.repeatToggle = false;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.slideToggle = function () {
        $('#schedule-weekly-content').slideToggle();
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isPanelOpen = function () {
        return $('#schedule-weekly-content').css('display') !== 'none';
    };
    NsdSchedulerDaysOfWeekComponent.prototype.closePanelIfOpen = function () {
        if (this.isPanelOpen()) {
            this.slideToggle();
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.showSchedule = function (clearItemD) {
        this.clearSchedule(clearItemD);
        if (!this.displayStartDate) {
            this.displayStartDate = moment();
        }
        if (this.start) {
            this.start = moment.isMoment(this.start) ? this.start : moment(this.start);
            this.end = moment.isMoment(this.end) ? this.end : moment(this.end);
            $('#w-start').bootstrapDatepicker('update', this.start.toDate());
        }
        if (this.end) {
            $('#w-end').bootstrapDatepicker('update', this.end.toDate());
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.hasEvents = function () {
        return !$.isEmptyObject(this.weekEventsDic);
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isClearButtonDisabled = function () {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }
        if (this.hasEvents()) {
            return false;
        }
        if (this.calendar) {
            var events = this.calendar.fullCalendar('clientEvents');
            return events.length <= 0;
        }
        return true;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.checkboxChanged = function (control, e) {
        switch (control) {
            case 's_sunrise':
                this.eventDetails.s_sunrise = !this.eventDetails.s_sunrise;
                this.eventDetails.s_sunset = false;
                if (this.eventDetails.s_sunrise) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.starttime = this.SR;
                    this.starttime = this.SR;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            var hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 6) {
                                this.eventDetails.endtime = "07:00";
                            }
                        }
                        else {
                            this.eventDetails.endtime = "07:00";
                        }
                    }
                }
                break;
            case 's_sunset':
                this.eventDetails.s_sunset = !this.eventDetails.s_sunset;
                this.eventDetails.s_sunrise = false;
                if (this.eventDetails.s_sunset) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.e_sunset = false;
                    this.eventDetails.starttime = this.SS;
                    this.starttime = this.SS;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            var hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 18) {
                                this.eventDetails.endtime = "22:00";
                            }
                        }
                        else {
                            this.eventDetails.endtime = "22:00";
                        }
                    }
                }
                break;
            case 'e_sunrise':
                this.eventDetails.e_sunrise = !this.eventDetails.e_sunrise;
                this.eventDetails.e_sunset = false;
                if (this.eventDetails.e_sunrise) {
                    this.eventDetails.s_sunrise = false;
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.endtime = this.SR;
                    this.endtime = this.SR;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            var hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 6) {
                                this.eventDetails.starttime = "04:00";
                            }
                        }
                        else {
                            this.eventDetails.starttime = "04:00";
                        }
                    }
                }
                break;
            case 'e_sunset':
                this.eventDetails.e_sunset = !this.eventDetails.e_sunset;
                this.eventDetails.e_sunrise = false;
                if (this.eventDetails.e_sunset) {
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.endtime = this.SS;
                    this.endtime = this.SS;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            var hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 18) {
                                this.eventDetails.starttime = "13:00";
                            }
                        }
                        else {
                            this.eventDetails.starttime = "13:00";
                        }
                    }
                }
                break;
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.onSetTimeChanged = function (control, e) {
        var events = this.getCalendarEvents();
        for (var i = 0; i < events.length; i++) {
            if (events[i]._id !== this.currentEvent['_id']) {
                if (control === "s_setTime") {
                    events[i].s_setTime = false;
                }
                else { //set_endTime
                    events[i].e_setTime = false;
                }
            }
        }
        switch (control) {
            case 's_setTime':
                this.eventDetails.s_setTime = !this.eventDetails.s_setTime;
                this.currentEvent['s_setTime'] = this.eventDetails.s_setTime;
                this.starttime = this.eventDetails.s_setTime ? this.eventDetails.starttime : '';
                break;
            case 'e_setTime':
                this.eventDetails.e_setTime = !this.eventDetails.e_setTime;
                this.currentEvent['e_setTime'] = this.eventDetails.e_setTime;
                this.endtime = this.eventDetails.e_setTime ? this.eventDetails.endtime : '';
                break;
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isValid = function () {
        if (this.parent && this.parent.isReadOnly) {
            return false;
        }
        var hasStart = $('#w-start').bootstrapDatepicker('getDate') != null;
        var hasEnd = $('#w-end').bootstrapDatepicker('getDate') != null;
        var hasEvents = false;
        if (this.calendar) {
            var events = this.getCalendarEvents();
            hasEvents = events.length > 0;
        }
        return hasStart && this.starttime && hasEnd && this.endtime && hasEvents;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.zoomIn = function () {
        if (this.slootDuration > 0) {
            this.slootDuration -= 1;
            var events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');
            var starttime = moment(this.starttime, 'HH:mm');
            var endtime = moment(this.endtime, 'HH:mm');
            var start = moment(this.start).set({
                hour: starttime.get('hour'),
                minute: starttime.get('minute'),
            });
            var end = moment(this.end).set({
                hour: endtime.get('hour'),
                minute: endtime.get('minute'),
            });
            this.initialize(this.data, start, end, this.slootDuration, events);
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.zoomOut = function () {
        if (this.slootDuration < 15) {
            this.slootDuration += 1;
            var events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');
            var starttime = moment(this.starttime, 'HH:mm');
            var endtime = moment(this.endtime, 'HH:mm');
            var start = moment(this.start).set({
                hour: starttime.get('hour'),
                minute: starttime.get('minute'),
            });
            var end = moment(this.end).set({
                hour: endtime.get('hour'),
                minute: endtime.get('minute'),
            });
            this.initialize(this.data, start, end, this.slootDuration, events);
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.areConsecutiveDays = function (d1, d2) {
        switch (d1) {
            case "MON":
                return d2 === "TUE";
            case "TUE":
                return d2 === "WED";
            case "WED":
                return d2 === "THU";
            case "THU":
                return d2 === "FRI";
            case "FRI":
                return d2 === "SAT";
            case "SAT":
                return d2 === "SUN";
            case "SUN":
                return d2 === "MON";
            default:
                return false;
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.sameTimeRange = function (ev1, ev2) {
        return ev1.extendedHours === ev2.extendedHours && ev1.startTime === ev2.startTime && ev1.endTime === ev2.endTime;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.areMergeableEvents = function (ev1, ev2) {
        return this.areConsecutiveDays(ev1.lastDay, ev2.orgDayOfWeek) && this.sameTimeRange(ev1, ev2); //ev1.extendedHours === ev2.extendedHours && ev1.startTime === ev2.startTime && ev1.endTime === ev2.endTime;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.mergeEventDetails = function (details) {
        var merged = [];
        var last = null;
        for (var k = 0; k < details.length; k++) {
            var current = details[k];
            current.orgDayOfWeek = current.dayOfWeek;
            if (last && this.areMergeableEvents(last, current)) {
                last.dayOfWeek = last.orgDayOfWeek + "-" + current.dayOfWeek;
                last.lastDay = current.dayOfWeek;
            }
            else {
                current.lastDay = current.dayOfWeek;
                last = current;
                merged.push(current);
            }
        }
        // fix for merging SUN-MON
        var len = merged.length;
        if (len >= 2 && this.areMergeableEvents(merged[len - 1], merged[0])) {
            var last_1 = merged[len - 1];
            var current = merged[0];
            last_1.dayOfWeek = last_1.orgDayOfWeek + "-" + current.lastDay;
            merged[0] = last_1;
            merged.pop();
        }
        var normalized = [];
        for (var i = 0; i < merged.length; i++) {
            var current = merged[i];
            if (current._status === undefined) {
                current._status = 0;
                normalized.push(current);
                for (var j = i + 1; j < merged.length; j++) {
                    var next = merged[j];
                    if (next._status === undefined && this.sameTimeRange(current, next)) {
                        current.dayOfWeek = current.dayOfWeek + " " + next.dayOfWeek;
                        next._status = 1;
                    }
                }
            }
        }
        return normalized; // merged;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.formatTimeRange = function (value) {
        var start = (value.start || "").replace(":", "");
        var end = (value.end || "").replace(":", "");
        return start + "-" + end;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.pushTimeRange = function (ranges, key, range) {
        var last = ranges.length && ranges[ranges.length - 1];
        if (last && last.range === range) {
            last.key += " " + key;
        }
        else {
            ranges.push({ key: key, range: range });
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.parseEvents = function (toggle) {
        var _this = this;
        if (toggle === void 0) { toggle = false; }
        var events = this.calendar.fullCalendar('clientEvents');
        events = events.sort(this.sortEvents);
        if (this.data && this.data.length > 0) {
            var proposalId = this.data[0].proposalId;
            this.resetDataEvents(proposalId, events);
        }
        this.weekEventsDic = {};
        var details = this.mergeEventDetails(this.getDailyEvent(events));
        for (var k = 0; k < details.length; k++) {
            this.updateDataEvent(details[k]);
            if (details[k].extendedHours && details[k].startTime === "00:00") {
                continue;
            }
            var key = details[k].dayOfWeek;
            var value = {
                start: details[k].startTime,
                end: details[k].endTime,
            };
            if (this.weekEventsDic[key]) {
                this.weekEventsDic[key].push(value);
            }
            else {
                this.weekEventsDic[key] = [];
                this.weekEventsDic[key].push(value);
            }
        }
        var dayRanges = [];
        var keys = Object.keys(this.weekEventsDic);
        keys.forEach(function (key) {
            var time = _this.weekEventsDic[key];
            var timeRanges = time.map(_this.formatTimeRange).join(' ');
            _this.pushTimeRange(dayRanges, key, timeRanges);
        });
        var itemD = dayRanges.map(function (r) { return r.key + " " + r.range; }).join(', ');
        this.arrayOfKeys = Object.keys(this.weekEventsDic);
        this.parent.hasScheduler = true;
        if (this.start && this.end) {
            var startDate = moment(this.start);
            var endDate = moment(this.end);
            var sDay = startDate.format('DD');
            var sMonth = startDate.format('MM');
            var sYear = startDate.format('YYYY');
            var sTime = (this.starttime === "SR" ? this.SR : (this.starttime === "SS" ? this.SS : this.starttime));
            var eDay = endDate.format('DD');
            var eMonth = endDate.format('MM');
            var eYear = endDate.format('YYYY');
            var eTime = (this.endtime === "SR" ? this.SR : (this.endtime === "SS" ? this.SS : this.endtime));
            var start = moment.utc(sDay + "-" + sMonth + "-" + sYear + " " + sTime, 'DD-MM-YYYY HH:mm');
            var end = moment.utc(eDay + "-" + eMonth + "-" + eYear + " " + eTime, 'DD-MM-YYYY HH:mm');
            this.parent.disableActivePeridDates(start, end, itemD);
        }
        if (toggle) {
            this.slideToggle();
            //this.toggleShedule(null, true);
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.getCalendarEvents = function () {
        return this.calendar.fullCalendar('clientEvents');
    };
    NsdSchedulerDaysOfWeekComponent.prototype.slideScheduleClose = function () {
        this.slideToggle();
        this.initialized = true;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.dateChanged = function (ev) {
        switch (ev.currentTarget.id) {
            case "w-start":
                this.start = moment(new Date(ev.currentTarget.value));
                this.calendar.fullCalendar('removeEvents');
                this.calendar.fullCalendar('gotoDate', this.start);
                if (this.end && this.end.isValid()) {
                    var el = $('#w-start');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
            case "w-end":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    var el = $('#w-end');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    };
    NsdSchedulerDaysOfWeekComponent.prototype.sortEvents = function (event1, event2) {
        var start1 = new Date(event1.start).getTime();
        var start2 = new Date(event2.start).getTime();
        return start1 > start2 ? 1 : -1;
    };
    ;
    NsdSchedulerDaysOfWeekComponent.prototype.getDailyEvent = function (dailyEvents) {
        var events = [];
        for (var i = 0; i < dailyEvents.length; i++) {
            var startEvent = dailyEvents[i].start;
            var endEvent = dailyEvents[i].end;
            var dayOfWeek = startEvent.format('ddd').toUpperCase();
            var startHour = startEvent.format('HH');
            var startMinute = startEvent.format('mm');
            var endHour = endEvent.format('HH');
            var endMinute = endEvent.format('mm');
            var start = null;
            var end = null;
            if (dailyEvents[i].s_sunrise) {
                start = "SR";
            }
            else if (dailyEvents[i].s_sunset) {
                start = "SS";
            }
            if (dailyEvents[i].e_sunrise) {
                end = "SR";
            }
            else if (dailyEvents[i].e_sunset) {
                end = "SS";
            }
            start = start ? start : moment(startHour + ":" + startMinute, 'HH:mm').format('HH:mm');
            end = end ? end : moment(endHour + ":" + endMinute, 'HH:mm').format('HH:mm');
            events.push({
                id: dailyEvents[i]._id || dailyEvents[i].id,
                dayOfWeek: dayOfWeek,
                startTime: start,
                endTime: end,
                start: dailyEvents[i].start,
                end: dailyEvents[i].end,
                s_sunrise: dailyEvents[i].s_sunrise,
                e_sunrise: dailyEvents[i].e_sunrise,
                s_sunset: dailyEvents[i].s_sunset,
                e_sunset: dailyEvents[i].e_sunset,
                extendedHours: dailyEvents[i].extendedHours,
                extEndTime: dailyEvents[i].exEndtime,
                exStarttime: dailyEvents[i].exStarttime,
                exEndtime: dailyEvents[i].exEndtime,
            });
        }
        return events.length > 0 ? this.removeDuplicates(events) : events;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.removeDuplicates = function (myArr) {
        var props = Object.keys(myArr[0]);
        return myArr.filter(function (item, index, self) {
            return index === self.findIndex(function (t) { return (props.every(function (prop) {
                return t[prop] === item[prop];
            })); });
        });
    };
    NsdSchedulerDaysOfWeekComponent.prototype.initCalendar = function (calendar, currentDate) {
        var self = this;
        var duration = "00:" + this.windowRef.zeroPad(this.slootDuration, 2) + ":00";
        calendar.fullCalendar({
            defaultDate: moment(currentDate),
            defaultView: 'agendaWeek',
            contentHeight: 820,
            firstDay: 1,
            slotDuration: duration,
            allDaySlot: false,
            columnFormat: {
                week: 'ddd',
            },
            selectable: true,
            editable: true,
            displayEventTime: false,
            droppable: true,
            eventLimit: true,
            fixedWeekCount: false,
            eventOverlap: false,
            header: false,
            axisFormat: 'HH:mm',
            timeFormat: 'HH:mm{ - HH:mm}',
            //maxtime: "23:59:00",
            minTime: 0,
            eventDragStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            eventDataTransform: function (event) {
                return event;
            },
            eventDragStop: function (event, jsEvent, ui, view) {
            },
            eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                var isSameDay = event.start.isSame(event.end, 'day');
                if (!isSameDay) { // crossed midnight
                    revertFunc();
                }
                if (event.s_setTime) {
                    self.starttime = moment(event.start).format('HH:mm');
                }
                if (event.e_setTime) {
                    self.endtime = moment(event.end).format('HH:mm');
                }
                self.startEventUpdate = null;
            },
            eventResizeStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            select: function (start, end, event, view) {
                var eventData = {
                    start: start,
                    end: end,
                    s_sunrise: false,
                    s_sunset: false,
                    e_sunrise: false,
                    e_sunset: false,
                    extendedHours: false,
                    exStarttime: '',
                    exEndtime: '',
                    allDay: false,
                };
                if (self.IsValidEventStartEndDate(eventData)) {
                    if (!self.parent.isReadOnly && !self.isOverlapping({ start: start, end: end })) {
                        self.calendar.fullCalendar('renderEvent', eventData, true);
                        self.parent.hasScheduler = true;
                    }
                }
                self.calendar.fullCalendar('unselect');
            },
            eventResizeStop: function (event, jsEvent, ui, view) {
                var x = 1;
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                if (!self.IsValidEventStartEndDate(event)) {
                    revertFunc();
                }
                else {
                    var prevStartTime = self.startEventUpdate.start.format('hh:mm');
                    var events = calendar.fullCalendar('clientEvents');
                    for (var i = 0; i < events.length; i++) {
                        var startTime = events[i].start.format('hh:mm');
                        var endTime = events[i].end.format('hh:mm');
                        if (event._id !== events[i]._id) {
                            //if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId)/* && prevStartTime === startTime*/) {
                            //    events[i].end = events[i].end.add(delta._milliseconds, 'milliseconds');
                            //    calendar.fullCalendar('updateEvent', events[i]);
                            //}
                        }
                        else if (event._id === events[i]._id) {
                            calendar.fullCalendar('updateEvent', events[i]);
                        }
                    }
                    self.startEventUpdate = null;
                }
            },
            eventRender: function (event, element, view) {
                if (self.parent && self.parent.isReadOnly) {
                    event.editable = false;
                }
                var time = event.sunrise ? "SUNRISE - " : '{ ' + event.start.format('HH:mm') + ' - ';
                if (event.sunset) {
                    time = time + 'SUNSET';
                }
                else {
                    time = time + event.end.format('HH:mm') + ' }';
                }
                element.find('.fc-content').prepend('<span><i class="fa fa-clock-o m-r-5"></i>' + time + '</span>');
                element.css('background-color', event.eventColor);
                element.css('border-color', event.eventColor);
            },
            eventClick: function (event, jsEvent, view) {
                if (!self.model.readOnlyMode) {
                    var el = $('#calendar-weekly-modal');
                    self.eventDetails.starttime = event.start.format('HH:mm');
                    self.eventDetails.endtime = event.end.format('HH:mm');
                    self.currentEvent = event;
                    $('.time').mask('00:00');
                    el.modal();
                    self.eventDetails.s_sunrise = event.s_sunrise;
                    self.eventDetails.s_sunset = event.s_sunset;
                    self.eventDetails.e_sunrise = event.e_sunrise;
                    self.eventDetails.e_sunset = event.e_sunset;
                    self.eventDetails.s_setTime = event.s_setTime;
                    self.eventDetails.e_setTime = event.e_setTime;
                    el.on('shown.bs.modal', function (e) {
                        self.cdRef.detectChanges();
                    });
                    el.on('hidden.bs.modal', function () {
                        $(this).data('bs.modal', null);
                    });
                    el.on('hide.bs.modal', function (e) {
                        var pressedButton = $(document.activeElement).attr('id');
                        if (pressedButton === "weekly_btn-modal-yes") {
                            var _start = moment(self.eventDetails.starttime, "HH:mm").format("HH:mm");
                            var _end = moment(self.eventDetails.endtime, "HH:mm").format("HH:mm");
                            if (_start.length === 5) {
                                var eventStartTime = event.start.format('HH:mm');
                                var eventEndTime = event.end.format('HH:mm');
                                var events = calendar.fullCalendar('clientEvents');
                                var count = 0;
                                var lastParentId = events[0].parentId;
                                for (var i = 0; i < events.length; i++) {
                                    var startTime = events[i].start.format('HH:mm');
                                    var endTime = events[i].end.format('HH:mm');
                                    if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId) && eventStartTime === startTime && eventEndTime == endTime) {
                                        if (events[i].parentId !== lastParentId) {
                                            lastParentId = events[i].parentId;
                                            count = 0;
                                        }
                                        count += 1;
                                        var start = events[i].start.clone();
                                        var end = events[i].end.clone();
                                        events[i].start = events[i].start.set({
                                            hour: parseInt(_start.substring(0, 2)),
                                            minute: parseInt(_start.substring(3))
                                        });
                                        events[i].end = events[i].end.set({
                                            hour: parseInt(_end.substring(0, 2)),
                                            minute: parseInt(_end.substring(3))
                                        });
                                        if (events[i].s_setTime) {
                                            self.starttime = self.eventDetails.starttime;
                                        }
                                        if (events[i].e_setTime) {
                                            self.endtime = self.eventDetails.endtime;
                                        }
                                        if (self.isOverlapping(events[i])) {
                                            events[i].start = start;
                                            events[i].end = end;
                                        }
                                        else {
                                            events[i].s_sunrise = self.eventDetails.s_sunrise;
                                            events[i].s_sunset = self.eventDetails.s_sunset;
                                            events[i].e_sunrise = self.eventDetails.e_sunrise;
                                            events[i].e_sunset = self.eventDetails.e_sunset;
                                            var eventColor = events[i].eventColor;
                                            if (self.eventDetails.s_sunrise || self.eventDetails.s_sunset || self.eventDetails.e_sunrise || self.eventDetails.e_sunset) {
                                                events[i].previousColor = events[i].eventColor;
                                                events[i].eventColor = '#2b3c4e';
                                            }
                                            else {
                                                if (event.eventColor === '#2b3c4e') {
                                                    events[i].eventColor = events[i].previousColor;
                                                    events[i].previousColor = null;
                                                }
                                            }
                                            calendar.fullCalendar('updateEvent', events[i]);
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if (pressedButton === "weekly_btn-modal-delete") {
                                if (event.s_setTime) {
                                    self.starttime = '';
                                }
                                if (event.e_setTime) {
                                    self.endtime = '';
                                }
                                calendar.fullCalendar('removeEvents', event._id);
                                calendar.fullCalendar('renderEvents');
                            }
                        }
                        self.eventDetails.s_sunrise = false;
                        self.eventDetails.s_sunset = false;
                        $('#calendar-weekly-modal').off('hide.bs.modal');
                    });
                }
            },
        });
    };
    NsdSchedulerDaysOfWeekComponent.prototype.updateDataEvent = function (event) {
        if (!this.data) {
            return;
        }
        ;
        for (var i = 0; i < this.data.length; i++) {
            var obj = JSON.parse(this.data[i].event);
            var id = event._id || event.id;
            if (obj.id !== id) {
                continue;
            }
            ;
            obj.start = event.start;
            obj.end = event.end;
            obj.s_sunset = event.s_sunset;
            obj.s_sunrise = event.s_sunrise;
            obj.e_sunset = event.e_sunset;
            obj.e_sunrise = event.e_sunrise;
            obj.extendedHours = event.extendedHours;
            obj.exStarttime = event.exStarttime;
            obj.exEndtime = event.exEndtime;
            this.data[i].event = JSON.stringify(obj);
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.resetDataEvents = function (proposalId, cEvents) {
        this.data = [];
        for (var i = 0; i < cEvents.length; i++) {
            var cEvent = cEvents[i];
            var event_2 = {
                id: cEvent._id || cEvent.id,
                start: cEvent.start,
                end: cEvent.end,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                s_setTime: cEvent.s_setTime,
                e_setTime: cEvent.e_setTime
            };
            this.data.push({
                calendarType: 2,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event_2.id,
                itemD: this.model.itemD,
                event: JSON.stringify(event_2)
            });
        }
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isOverlapping = function (event) {
        var array = this.calendar.fullCalendar('clientEvents');
        for (var i in array) {
            if (array[i]._id != event._id) {
                if (!(array[i].start >= event.end || array[i].end <= event.start)) {
                    return true;
                }
            }
        }
        return false;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.IsValidEventStartEndDate = function (event) {
        return event.end.date() === event.start.date();
    };
    NsdSchedulerDaysOfWeekComponent.prototype.setEvent = function (obj) {
        var event = {
            id: obj.id,
            eventColor: obj.eventColor,
            previousColor: obj.previousColor,
            groupId: obj.groupId,
            linked: obj.linked,
            start: moment.utc(obj.start),
            end: moment.utc(obj.end),
            s_sunset: obj.s_sunset,
            s_sunrise: obj.s_sunrise,
            e_sunset: obj.e_sunset,
            e_sunrise: obj.e_sunrise,
            s_setTime: obj.s_setTime,
            e_setTime: obj.e_setTime,
            allDay: false,
            stick: true
        };
        return event;
    };
    // validators
    NsdSchedulerDaysOfWeekComponent.prototype.isStartHasValue = function () {
        return $('#w-start').bootstrapDatepicker('getDate') != null;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isStartValidDate = function () {
        if (this.isStartHasValue()) {
            return this.isValidDate();
        }
        return true;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isValidDate = function () {
        var start = moment($('#w-start').bootstrapDatepicker('getDate')).format("YYYY/MM/DD");
        var now = moment().format("YYYY/MM/DD");
        return moment(start).isSameOrAfter(now);
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isValidEndTime = function () {
        if (this.isEndHaveValue()) {
            var end = this.endtime;
            var start = this.starttime;
            var now = moment().format('hh:mm');
            return (end > now && end > start);
        }
        return true;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isEndHaveValue = function () {
        return $('#w-end').bootstrapDatepicker('getDate') != null;
    };
    NsdSchedulerDaysOfWeekComponent.prototype.isEndValidDate = function () {
        var start = moment($('#w-start').bootstrapDatepicker('getDate'));
        var end = moment($('#w-end').bootstrapDatepicker('getDate'));
        if (start.isValid() && end.isValid()) {
            return start.isSameOrBefore(end);
        }
        return true;
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], NsdSchedulerDaysOfWeekComponent.prototype, "form", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], NsdSchedulerDaysOfWeekComponent.prototype, "model", void 0);
    NsdSchedulerDaysOfWeekComponent = __decorate([
        core_1.Component({
            selector: 'scheduler-days-week',
            templateUrl: '/app/dashboard/nsds/nsd-scheduler-days-week.component.html'
        }),
        __metadata("design:paramtypes", [windowRef_service_1.WindowRef, core_1.ChangeDetectorRef])
    ], NsdSchedulerDaysOfWeekComponent);
    return NsdSchedulerDaysOfWeekComponent;
}());
exports.NsdSchedulerDaysOfWeekComponent = NsdSchedulerDaysOfWeekComponent;
//# sourceMappingURL=nsd-scheduler-days-week.component.js.map