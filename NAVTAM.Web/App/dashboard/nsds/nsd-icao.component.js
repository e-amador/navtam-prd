"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdIcaoComponent = void 0;
var core_1 = require("@angular/core");
var data_model_1 = require("../shared/data.model");
var angular_l10n_1 = require("angular-l10n");
var moment = require("moment");
var NsdIcaoComponent = /** @class */ (function () {
    function NsdIcaoComponent(translation) {
        this.translation = translation;
    }
    NsdIcaoComponent.prototype.ngOnInit = function () {
    };
    NsdIcaoComponent.prototype.ngOnChanges = function () {
        if (this.model.coordinates) {
            this.updateIcaoCoordinates(this.model.coordinates, this.model.radius);
        }
        this.startActivityText = this.formatStartActivityDate();
        this.endValidityText = this.formatEndValidityDate();
        this.lowerLimit = ("000" + this.model.lowerLimit).slice(-3);
        this.upperLimit = ("000" + this.model.upperLimit).slice(-3);
    };
    NsdIcaoComponent.prototype.updateIcaoCoordinates = function (coordinates, radius) {
        var coordinatesParts = coordinates.split(" ");
        if (coordinatesParts.length === 2) {
            radius = radius.pad(3);
            this.model.icaoCoordinates = coordinatesParts[0] + coordinatesParts[1] + radius;
        }
    };
    NsdIcaoComponent.prototype.formatStartActivityDate = function () {
        if (this.model.proposalType == data_model_1.ProposalType.Cancel) {
            return this.translation.translate('IcaoText.Immediate'); // "IMMEDIATE";
        }
        var startDate = moment.utc(this.model.startActivity);
        var diseminatedOrModified = this.model.status === data_model_1.ProposalStatus.Disseminated || this.model.status === data_model_1.ProposalStatus.DisseminatedModified;
        if (diseminatedOrModified) {
            return startDate.isValid() ? startDate.format("YYMMDDHHmm") + "   [" + startDate.format("MM/DD/YYYY, HH:mm") + "]" : "";
        }
        if (this.model.token.immediate) {
            return this.translation.translate('IcaoText.Immediate'); //"IMMEDIATE";
        }
        else {
            return startDate.isValid() ? startDate.format("YYMMDDHHmm") + "   [" + startDate.format("MM/DD/YYYY, HH:mm") + "]" : "";
        }
    };
    NsdIcaoComponent.prototype.formatEndValidityDate = function () {
        if (this.model.proposalType == data_model_1.ProposalType.Cancel) {
            return "";
        }
        var endDate = moment.utc(this.model.endValidity);
        if (this.model.token.permanent) {
            return "PERM";
        }
        else {
            return endDate.isValid() ? endDate.format("YYMMDDHHmm") + (this.model.token.estimated ? " EST" : "") + " [" + endDate.format("MM/DD/YYYY, HH:mm") + "]" : "";
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], NsdIcaoComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], NsdIcaoComponent.prototype, "loading", void 0);
    NsdIcaoComponent = __decorate([
        core_1.Component({
            selector: 'icao',
            templateUrl: '/app/dashboard/nsds/nsd-icao.component.html'
        }),
        __metadata("design:paramtypes", [angular_l10n_1.TranslationService])
    ], NsdIcaoComponent);
    return NsdIcaoComponent;
}());
exports.NsdIcaoComponent = NsdIcaoComponent;
//# sourceMappingURL=nsd-icao.component.js.map