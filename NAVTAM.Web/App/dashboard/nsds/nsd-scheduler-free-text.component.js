"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdSchedulerFreeTextComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../shared/data.service");
var moment = require("moment");
var NsdSchedulerFreeTextComponent = /** @class */ (function () {
    function NsdSchedulerFreeTextComponent(cdRef, dataService) {
        this.cdRef = cdRef;
        this.dataService = dataService;
        this.parent = null;
        this.isDisabled = false;
        this.itemD = "";
        this.start = null;
        this.end = null;
        this.starttime = null;
        this.endtime = null;
        this.validationResult = true;
        this.overridden = false;
        this.parserError = "";
    }
    NsdSchedulerFreeTextComponent.prototype.ngOnInit = function () {
    };
    NsdSchedulerFreeTextComponent.prototype.ngOnDestroy = function () {
    };
    NsdSchedulerFreeTextComponent.prototype.ngAfterViewInit = function () {
        var self = this;
        $('.ft-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: $(this).data('autoclose') ? $(this).data('autoclose') : false,
                todayHighlight: $(this).data('today-highlight') ? $(this).data('today-highlight') : true,
                orientation: $(this).data('orientation') ? $(this).data('orientation') : "top",
                startView: $(this).data('view') ? $(this).data('view') : 0,
                language: $(this).data('lang') ? $(this).data('lang') : "en",
                forceParse: $(this).data('parse') ? $(this).data('parse') : false,
                daysOfWeekDisabled: $(this).data('day-disabled') ? $(this).data('day-disabled') : "",
                calendarWeeks: $(this).data('calendar-week') ? $(this).data('calendar-week') : false,
                toggleActive: $(this).data('toggle-active') ? $(this).data('toggle-active') : true,
                multidate: $(this).data('multidate') ? $(this).data('multidate') : false,
            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });
        $('#ft-starttime').mask('00:00');
        $('#ft-endtime').mask('00:00');
    };
    NsdSchedulerFreeTextComponent.prototype.initialize = function (data, start, end) {
        if (data.length === 1) {
            var obj = JSON.parse(data[0].event);
            this.itemD = obj.itemD;
            start = moment(start);
            end = moment(end);
            this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
            this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));
            this.starttime = start.format("HH:mm");
            this.endtime = end.format("HH:mm");
            $('#ft-start').bootstrapDatepicker('update', this.start);
            $('#ft-end').bootstrapDatepicker('update', this.end);
            this.isDisabled = false;
        }
    };
    NsdSchedulerFreeTextComponent.prototype.setDate = function (start, end, starttime, endtime) {
        if (start) {
            start = typeof (start) === 'string' ? moment(start, "MM/DD/YYYY") : moment(start);
        }
        if (end) {
            end = typeof (end) === 'string' ? moment(end, "MM/DD/YYYY") : moment(end);
        }
        this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
        this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));
        this.starttime = starttime;
        this.endtime = endtime;
        var startElem = $('#ft-start');
        startElem.bootstrapDatepicker('update', this.start);
        startElem.prop('disabled', true);
        var endElem = $('#ft-end');
        endElem.bootstrapDatepicker('update', this.end);
        endElem.prop('disabled', true);
        this.isDisabled = true;
    };
    NsdSchedulerFreeTextComponent.prototype.dateChanged = function (ev) {
        switch (ev.currentTarget.id) {
            case "ft-start":
                this.start = moment.utc(ev.currentTarget.value, "MM/DD/YYYY");
                if (this.end && this.end.isValid()) {
                    var el = $('#ft-start');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
            case "ft-end":
                this.end = moment.utc(ev.currentTarget.value, "MM/DD/YYYY");
                if (this.start && this.start.isValid()) {
                    var el = $('#w-end');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    };
    NsdSchedulerFreeTextComponent.prototype.getFreeText = function () {
        return this.itemD;
    };
    NsdSchedulerFreeTextComponent.prototype.setFreeText = function (itemD) {
        this.itemD = itemD.toUpperCase();
    };
    NsdSchedulerFreeTextComponent.prototype.validate = function () {
        var _this = this;
        this.dataService.validateItemD(this.itemD)
            .subscribe(function (response) {
            _this.parserError = "";
            switch (response.case) {
                case "Success":
                    _this.validationResult = true;
                    _this.parent.updateFormControlValue("itemD", _this.itemD);
                    _this.parent.nsdForm.markAsDirty();
                    break;
                case "Failure":
                    _this.validationResult = false;
                    _this.parserError = response.errorMessage;
                    break;
            }
        }, function (error) {
            //todo
        });
    };
    NsdSchedulerFreeTextComponent.prototype.apply = function () {
        var start = this.setMomentDateTime(this.start, this.starttime.split(':'), 'DD/MM/YYYY HH:mm');
        var end = this.setMomentDateTime(this.end, this.endtime.split(':'), 'DD/MM/YYYY HH:mm');
        this.parent.disableActivePeridDates(start, end, this.itemD);
        this.parent.isScheduleOverridden = true;
    };
    NsdSchedulerFreeTextComponent.prototype.isValid = function () {
        return this.checkValidity();
    };
    NsdSchedulerFreeTextComponent.prototype.override = function () {
        this.overridden = true;
        if (this.parent.hasScheduler) {
            var self_1 = this;
            var itemD_1 = this.itemD;
            var m_start_1 = this.start;
            var m_end_1 = this.end;
            var start_1 = $('#ft-start').bootstrapDatepicker('getDate');
            var end_1 = $('#ft-end').bootstrapDatepicker('getDate');
            var starttime_1 = this.starttime;
            var endtime_1 = this.endtime;
            this.parent.clearSchedule(null, function (actionResult) {
                if (actionResult) {
                    self_1.itemD = itemD_1;
                    self_1.start = m_start_1;
                    self_1.end = m_end_1;
                    self_1.starttime = starttime_1;
                    self_1.endtime = endtime_1;
                    self_1.isDisabled = false;
                    self_1.parent.hasScheduler = false;
                    self_1.parent.isScheduleOverridden = true;
                    self_1.parent.updateFormControlValue("itemD", self_1.itemD);
                    $('#ft-start').bootstrapDatepicker('update', start_1);
                    $('#ft-end').bootstrapDatepicker('update', end_1);
                    $('#ft-start').prop('disabled', false);
                    $('#ft-end').prop('disabled', false);
                    self_1.parent.nsdForm.markAsDirty();
                }
            });
        }
        else {
            this.isDisabled = false;
            this.parent.hasScheduler = false;
            this.parent.updateFormControlValue("itemD", this.itemD);
            this.parent.nsdForm.markAsDirty();
            this.parent.isScheduleOverridden = true;
            $('#ft-start').prop('disabled', false);
            $('#ft-end').prop('disabled', false);
        }
    };
    NsdSchedulerFreeTextComponent.prototype.isClearButtonDisabled = function () {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }
        return !this.checkValidity();
    };
    NsdSchedulerFreeTextComponent.prototype.clear = function () {
        this.clearFormControls();
        this.parent.enableActivePeridDates();
        this.parent.resetSchedulersStatus();
        this.parent.schedulerDaily.removeEvents();
        this.parent.schedulerDayWeeks.removeEvents();
        this.parent.schedulerDate.removeEvents();
    };
    NsdSchedulerFreeTextComponent.prototype.clearFormControls = function () {
        this.start = null;
        this.end = null;
        this.starttime = "";
        this.itemD = "";
        this.endtime = "";
        var startElem = $('#ft-start');
        startElem.bootstrapDatepicker('update', null);
        startElem.prop('disabled', false);
        var endElem = $('#ft-end');
        endElem.bootstrapDatepicker('update', null);
        endElem.prop('disabled', false);
        this.isDisabled = false;
    };
    NsdSchedulerFreeTextComponent.prototype.setMomentDateTime = function (date, time, format) {
        var day = date.format('DD');
        var month = date.format('MM');
        var year = date.format('YYYY');
        var hour = time[0];
        var minute = time[1];
        return moment.utc(day + "-" + month + "-" + year + " " + hour + ":" + minute, 'DD-MM-YYYY HH:mm').format(format);
    };
    NsdSchedulerFreeTextComponent.prototype.isItemDControlDisabled = function () {
        if (this.parent && this.parent.hasScheduler && this.parent.schedulerTab !== 4) {
            return false;
        }
        var start = moment($('#ft-start').bootstrapDatepicker('getDate'));
        var end = moment($('#ft-end').bootstrapDatepicker('getDate'));
        this.start = start.isValid() ? start : null;
        this.end = end.isValid() ? end : null;
        if (this.start && this.starttime && this.end && this.endtime) {
            return false;
        }
        return true;
    };
    NsdSchedulerFreeTextComponent.prototype.checkValidity = function () {
        if (!this.isItemDControlDisabled()) {
            return this.itemD.length > 1;
        }
        return false;
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], NsdSchedulerFreeTextComponent.prototype, "form", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], NsdSchedulerFreeTextComponent.prototype, "model", void 0);
    NsdSchedulerFreeTextComponent = __decorate([
        core_1.Component({
            selector: 'scheduler-freetext',
            templateUrl: '/app/dashboard/nsds/nsd-scheduler-free-text.component.html'
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef, data_service_1.DataService])
    ], NsdSchedulerFreeTextComponent);
    return NsdSchedulerFreeTextComponent;
}());
exports.NsdSchedulerFreeTextComponent = NsdSchedulerFreeTextComponent;
//# sourceMappingURL=nsd-scheduler-free-text.component.js.map