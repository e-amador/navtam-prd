"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DynamicComponent = void 0;
var core_1 = require("@angular/core");
//Nsds Components
var catchall_draft_component_1 = require("./catchall/catchall-draft.component");
var catchall_cancel_component_1 = require("./catchall/catchall-cancel.component");
var obst_component_1 = require("./obst/obst.component");
var obst_cancel_component_1 = require("./obst/obst-cancel.component");
var obstlgus_component_1 = require("./obstlgus/obstlgus.component");
var obstlgus_cancel_component_1 = require("./obstlgus/obstlgus-cancel.component");
var mobst_component_1 = require("./mobst/mobst.component");
var mobst_cancel_component_1 = require("./mobst/mobst-cancel.component");
var mobstlgus_component_1 = require("./mobstlgus/mobstlgus.component");
var mobstlgus_cancel_component_1 = require("./mobstlgus/mobstlgus-cancel.component");
var rwyclsd_component_1 = require("./rwyclsd/rwyclsd.component");
var rwyclsd_cancel_component_1 = require("./rwyclsd/rwyclsd-cancel.component");
var twyclsd_component_1 = require("./twyclsd/twyclsd.component");
var twyclsd_cancel_component_1 = require("./twyclsd/twyclsd-cancel.component");
var carsclsd_component_1 = require("./carsclsd/carsclsd.component");
var carsclsd_cancel_component_1 = require("./carsclsd/carsclsd-cancel.component");
var DynamicComponent = /** @class */ (function () {
    function DynamicComponent(resolver) {
        this.resolver = resolver;
        this.currentComponent = null;
    }
    Object.defineProperty(DynamicComponent.prototype, "componentData", {
        // catId: Nsd categoryId used to resolve the component
        // ver: Nsd version (not used at this point but passed if future versioning is needed)
        // inputs: An object with key/value pairs mapped to input name/input value injected to the component
        set: function (data) {
            if (!data) {
                return;
            }
            // Inputs need to be in the following format to be resolved properly
            var inputProviders = Object.keys(data.inputs).map(function (inputName) {
                return { provide: inputName, useValue: data.inputs[inputName] };
            });
            var resolvedInputs = core_1.ReflectiveInjector.resolve(inputProviders);
            // We create an injector out of the data we want to pass down and this components injector
            var injector = core_1.ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.dynamicComponentContainer.parentInjector);
            var componentClass = this.resolveComponent(data.catId, data.version, data.inputs['type']);
            // We create a factory out of the component we want to create
            var factory = this.resolver.resolveComponentFactory(componentClass);
            // We create the component using the factory and the injector
            var component = factory.create(injector);
            // We insert the component into the dom container
            this.dynamicComponentContainer.insert(component.hostView);
            // We can destroy the old component is we like by calling destroy
            if (this.currentComponent) {
                this.currentComponent.destroy();
            }
            this.currentComponent = component;
        },
        enumerable: false,
        configurable: true
    });
    DynamicComponent.prototype.resolveComponent = function (catId, version, type) {
        //At this point there is not versioning. If in the future a new new version of the component is added, 
        //we can use the version param to resolve the correct instance (naming convention should be decided). 
        //Depending on the type of NSD it might be necesary to have different components for Cancel and Replace. 
        //In the case of Catch All all actions are the same so only one component is required.
        switch (catId) {
            case 2: //CATCH ALL
                switch (type) {
                    case "cancel":
                        return catchall_cancel_component_1.CatchAllCancelComponent;
                    default: return catchall_draft_component_1.CatchAllDraftComponent;
                }
            case 4: // OBST
                switch (type) {
                    case "cancel":
                        return obst_cancel_component_1.ObstCancelComponent;
                    default:
                        return obst_component_1.ObstComponent;
                }
            case 5: // OBST LG US
                switch (type) {
                    case "cancel":
                        return obstlgus_cancel_component_1.ObstLgUsCancelComponent;
                    default:
                        return obstlgus_component_1.ObstLgUsComponent;
                }
            case 7: //RWY CLSD
                switch (type) {
                    case "cancel":
                        return rwyclsd_cancel_component_1.RwyClsdCancelComponent;
                    default:
                        return rwyclsd_component_1.RwyClsdComponent;
                }
            case 11: //TWYCLSD
                switch (type) {
                    case "cancel":
                        return twyclsd_cancel_component_1.TwyClsdCancelComponent;
                    default:
                        return twyclsd_component_1.TwyClsdComponent;
                }
            case 13: //CARSCLSD
                switch (type) {
                    case "cancel":
                        return carsclsd_cancel_component_1.CarsClsdCancelComponent;
                    default:
                        return carsclsd_component_1.CarsClsdComponent;
                }
            case 14: //MULT OBST
                switch (type) {
                    case "cancel":
                        return mobst_cancel_component_1.MObstCancelComponent;
                    default:
                        return mobst_component_1.MObstComponent;
                }
            case 15: //MULT OBST LG US
                switch (type) {
                    case "cancel":
                        return mobstlgus_cancel_component_1.MObstLgUsCancelComponent;
                    default:
                        return mobstlgus_component_1.MObstLgUsComponent;
                }
            default:
                return catchall_draft_component_1.CatchAllDraftComponent;
        }
    };
    __decorate([
        core_1.ViewChild('dynamicComponentContainer', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], DynamicComponent.prototype, "dynamicComponentContainer", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], DynamicComponent.prototype, "componentData", null);
    DynamicComponent = __decorate([
        core_1.Component({
            selector: 'dynamic-component',
            entryComponents: [
                catchall_draft_component_1.CatchAllDraftComponent,
                catchall_cancel_component_1.CatchAllCancelComponent,
                obst_component_1.ObstComponent,
                obst_cancel_component_1.ObstCancelComponent,
                obstlgus_component_1.ObstLgUsComponent,
                obstlgus_cancel_component_1.ObstLgUsCancelComponent,
                mobst_component_1.MObstComponent,
                mobst_cancel_component_1.MObstCancelComponent,
                mobstlgus_component_1.MObstLgUsComponent,
                mobstlgus_cancel_component_1.MObstLgUsCancelComponent,
                rwyclsd_component_1.RwyClsdComponent,
                rwyclsd_cancel_component_1.RwyClsdCancelComponent,
                twyclsd_component_1.TwyClsdComponent,
                twyclsd_cancel_component_1.TwyClsdCancelComponent,
                carsclsd_component_1.CarsClsdComponent,
                carsclsd_cancel_component_1.CarsClsdCancelComponent
            ],
            template: "<div #dynamicComponentContainer></div>",
        }),
        __metadata("design:paramtypes", [core_1.ComponentFactoryResolver])
    ], DynamicComponent);
    return DynamicComponent;
}());
exports.DynamicComponent = DynamicComponent;
//# sourceMappingURL=dynamic.component.js.map