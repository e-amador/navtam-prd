﻿import { Component, OnInit, OnDestroy, Inject, Injector } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { DataService } from '../../shared/data.service'
import { LocationService } from '../../shared/location.service'
import { MObstService, IMObstType, IInDoaValidResult, ICenterRadius } from '../mobst/mobst.service'
import { ISelectOptions, MObstAreaType, IMObstViewModel } from '../../shared/data.model'

import { LocaleService, TranslationService } from 'angular-l10n';

@Component({
    templateUrl: '/app/dashboard/nsds/mobstlgus/mobstlgus-cancel.component.html'
})
export class MObstLgUsCancelComponent implements OnInit, OnDestroy {
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = false;
    obstacleTypes: ISelectOptions[] = [];
    leafletmap: any;
    isFormLoadingData: boolean;
    isLine: boolean = false;
    isOther: boolean = false;
    units: string = "(NM)";
    options = { onlySelf: true, emitEvent: false };

    upperLimitNM: number = 999;
    upperLimitFT: number = 6076;

    maxElevation: number = 99999;
    maxHeight: number = 9999;

    constructor(private injector: Injector,
        private fb: FormBuilder,
        public locationService: LocationService,
        private mobstService: MObstService,
        private dataService: DataService,
        public translation: TranslationService) {

        this.token = this.injector.get('token');
        this.isReadOnly = true;
        this.nsdForm = this.injector.get('form');
    }

    ngOnInit() {
        this.isFormLoadingData = !(!this.token.subjectId);
        const app = this.mobstService.app;

        this.leafletmap = app.leafletmap;
        if (this.token.radius === 0) {
            if (this.token.radiusUnit === 0) this.token.radius = 5;
            else this.token.radius = 1;
        }

        this.units = this.token.radiusUnit === 0 ? '(NM)' : '(FT)';
        this.isLine = (this.token.areaType === 1);
        this.isOther = (this.token.obstacleType === 'Other');

        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        } else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup 
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }

        this.loadMObstTypes();
        this.showDoaMapRegion(app.doaId);
    }

    ngOnDestroy() {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    objectTypeSelectOptions() {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ObjectTypePlaceholder')
            }
        }
    }

    loadMObstTypes() {
        this.mobstService
            .getMObstTypes()
            .subscribe((mobstTypes: IMObstType[]) => {
                let obstacles = mobstTypes.map((item: IMObstType) => {
                    return {
                        id: item.id,
                        text: item.name
                    }
                });
                // make the 'token.obstacleType' value the first on the list for "optionless convenience" or angular will throw an exception!
                this.makeFirstElement(obstacles, (obst) => obst.id === this.token.obstacleType, true);
                this.obstacleTypes = obstacles;
            });
    }

    setObjectType($event) {
        const mobstTypeCtrl = this.$ctrl('obstacleType');
        const newValue = $event.value;
        if (newValue !== "-1") {
            this.isOther = (newValue === 'Other');
            if (newValue !== mobstTypeCtrl.value) {
                mobstTypeCtrl.setValue(newValue);
                mobstTypeCtrl.markAsDirty();
            }
            this.refreshMap();
        }
    }

    private clearLocations() {
        const loc = this.$ctrl('location');
        const rad = this.$ctrl('radius');
        const uni = this.$ctrl('radiusUnit');
        const locFrom = this.$ctrl('fromPoint');
        const locTo = this.$ctrl('toPoint');

        loc.patchValue('', this.options);
        rad.patchValue(5, this.options);
        uni.patchValue(false, this.options);
        locFrom.patchValue('', this.options);
        locTo.patchValue('', this.options);

        loc.setErrors(null);
        rad.setErrors(null);
        uni.setErrors(null);
        locFrom.setErrors(null);
        locTo.setErrors(null);
    }

    public changeMode() {
        if (this.isFormLoadingData) return;
        var fc = this.$ctrl('areaType');
        this.clearLocations();
        this.isLine = fc.value;
    }

    public changeUnits() {
        var fc = this.$ctrl('radiusUnit');
        const rad = this.$ctrl('radius');
        if (fc.value === false) {
            this.units = "(NM)";
            rad.patchValue(5, this.options);
        }
        else {
            this.units = "(FT)"
            rad.patchValue(1, this.options);
        }
        rad.setErrors(null);
    }

    public validateRadiusKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key) && key !== '.') {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    decHeightValue() {
        const radCtrl = this.$ctrl('height');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incHeightValue() {
        const radCtrl = this.$ctrl('height');
        if (+radCtrl.value < this.maxHeight) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    decElevationValue() {
        const radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incElevationValue() {
        const radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value < this.maxElevation) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    validateLocation() {
        const fc = this.nsdFrmGroup.get('location');
        return !fc.errors || this.nsdForm.pristine;
    }

    validateObstacleName() {
        const fc = this.nsdFrmGroup.get('otherObstacleNameE');
        return !fc.errors || this.nsdForm.pristine;
    }

    validateDescriptionEng() {
        const fc = this.nsdFrmGroup.get('obstacleDescriptionE');
        return !fc.errors || this.nsdForm.pristine;
    }

    validateFromPoint() {
        const fc = this.nsdFrmGroup.get('fromPoint');
        return !fc.errors || this.nsdForm.pristine;
    }

    validateToPoint() {
        const fc = this.nsdFrmGroup.get('toPoint');
        return !fc.errors || this.nsdForm.pristine;
    }


    invalidMObstType(): boolean {
        if (this.nsdForm.pristine) return false;
        const ctrl = this.$ctrl('obstacleType');
        return !this.isReadOnly && this.$ctrl('obstacleType').invalid;
    }

    validateMObstTypeId() {
        const fc = this.nsdFrmGroup.get('obstacleType');
        return fc.value !== -1 || this.nsdForm.pristine;
    }

    validateRadius() {
        if (this.isLine || this.nsdForm.pristine)
            return true;

        const fc = this.$ctrl('radius');
        if (fc.errors)
            return false;

        return this.validateRadiusByUnits(+fc.value);
    }

    validateRadiusByUnits(radius: number) {
        return this.units === "(NM)" ? this.validateRadiusNM(radius) : this.validateRadiusFT(radius);
    }

    validateRadiusNM(radius: number) {
        return radius >= 1 && radius <= this.upperLimitNM;
    }

    validateRadiusFT(radius: number) {
        return radius >= 1 && radius <= this.upperLimitFT;
    }

    validateHeight() {
        if (this.nsdForm.pristine) return true;
        const fc = this.$ctrl('height');
        if (fc.errors) return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxHeight) return false;
            if (+fc.value < 0) return false;
        } else return false;
        return true;
    }

    validateElevation() {
        if (this.nsdForm.pristine) return true;
        const fc = this.$ctrl('elevation');
        if (fc.errors) return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxElevation) return false;
            if (+fc.value < 0) return false;
        } else return false;
        return true;
    }

    private locationChanged(value: any) {
        let tmp: string = this.$ctrl('location').value;
        if (tmp !== null && hasLowerCase(tmp)) this.$ctrl('location').patchValue(tmp.toUpperCase());
        if (value !== null && hasLowerCase(value)) value = value.toUpperCase();

        if (this.leafletmap === null) this.leafletmap = this.mobstService.app.leafletmap;
        const pair = this.locationService.parse(value);
        const geojson = pair ? this.leafletmap.latitudeLongitudeToGeoJson(pair.latitude, pair.longitude) : null;
        if (geojson) {
            this.mobstService
                .validateLocation(value)
                .subscribe((result: IInDoaValidResult) => {
                    var locCtrl = this.$ctrl('location');
                    var currentLocation = locCtrl.value;
                    if (currentLocation === value) {
                        if (result.inDoa) {
                            var rad = this.getRadiusInMeters();
                            this.leafletmap.addLocationAndRadius(pair.latitude, pair.longitude, +rad);

                            locCtrl.setErrors(null);
                            this.triggerFormChangeEvent(value);
                        } else {
                            locCtrl.setErrors({ invalidLocation: true });
                        }
                    }
                }, (error: any) => {
                    var locCtrl = this.$ctrl('location');
                    locCtrl.setErrors({ invalidLocation: true });
                });
        }
    }

    private changeRadius() {
        var tmp: string = this.$ctrl('location').value;
        this.locationChanged(tmp);
    }

    private getRadiusInMeters(): number {
        var rad = this.$ctrl('radius').value;
        var radU = this.$ctrl('radiusUnit').value;
        if (radU) rad = this.locationService.feetToMeters(+rad);
        else rad = this.locationService.nmToMeters(+rad);
        return +rad;
    }

    private locationToFromChanged(ctrlName: string, value: any) {
        var fc_from: string = this.$ctrl('fromPoint').value;
        var fc_to: string = this.$ctrl('toPoint').value;

        if (fc_from !== null && fc_to !== null) {
            if (this.validateFromPoint() && this.validateToPoint()) {
                this.mobstService.calcCenterLocation(fc_from, fc_to)
                    .subscribe((result: ICenterRadius) => {
                        if (result.valid) {
                            this.$ctrl('location').patchValue(result.center, this.options);
                            this.$ctrl('radius').patchValue(result.radius, this.options);
                            this.$ctrl('radiusUnit').patchValue(false, this.options);

                            if (this.leafletmap === null) this.leafletmap = this.mobstService.app.leafletmap;
                            const pair_ini = this.locationService.parse(fc_from);
                            const pair_end = this.locationService.parse(fc_to);
                            this.leafletmap.addPointToPointLine(pair_ini.latitude, pair_ini.longitude, pair_end.latitude, pair_end.longitude);

                            var locCtrl = this.$ctrl(ctrlName);
                            locCtrl.setErrors(null);
                            this.triggerFormChangeEvent(value);
                        }
                        else {
                            var locCtrl = this.$ctrl(ctrlName);
                            locCtrl.setErrors({ invalidLocation: true });
                        }
                    }, (error: any) => {
                        var locCtrl = this.$ctrl(ctrlName);
                        locCtrl.setErrors({ invalidLocation: true });
                    });
            }
        }
    }

    private otherObstacleNameChange(value: any): void {
        var fc_eng = this.$ctrl('otherObstacleNameE');
        var fc_fr = this.$ctrl('otherObstacleNameF');
        if (fc_fr.value && !fc_eng.value) {
            fc_eng.setErrors({ required: true });
        } else {
            fc_eng.setErrors(null);
            this.triggerFormChangeEvent(value);
        }
    }

    private obstacleDescriptionChange(value: any): void {
        var fc_eng = this.$ctrl('obstacleDescriptionE');
        var fc_fr = this.$ctrl('obstacleDescriptionF');
        if (fc_fr.value && !fc_eng.value) {
            fc_eng.setErrors({ required: true });
        } else {
            fc_eng.setErrors(null);
            this.triggerFormChangeEvent(value);
        }
    }

    private triggerFormChangeEvent(value) {
        this.$ctrl('refreshDummy').setValue(value);
        this.$ctrl('refreshDummy').markAsDirty();
    }

    private getSelectedObstacleName() {
        const obstId = this.$ctrl('obstacleType').value;

        if (!obstId || !this.obstacleTypes)
            return "MOBST";

        const obstacle = this.obstacleTypes.find(obst => obst.id === obstId);
        return obstacle ? obstacle.id : "MOBST";
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];

        this.nsdFrmGroup = this.fb.group({
            obstacleType: [{ value: this.token.obstacleType || -1, disabled: this.isReadOnly }, [mobstTypeValidator]],
            otherObstacleNameE: [{ value: this.token.otherObstacleNameE, disabled: this.isReadOnly }, []],
            otherObstacleNameF: [{ value: this.token.otherObstacleNameF, disabled: this.isReadOnly }, []],
            areaType: [{ value: this.token.areaType, disabled: this.isReadOnly }, []],
            location: [{ value: this.token.location, disabled: this.isReadOnly }, [validatePointLocation(this)]],
            radius: [{ value: this.token.radius, disabled: this.isReadOnly }, []],
            radiusUnit: [{ value: this.token.radiusUnit, disabled: this.isReadOnly }, []],
            fromPoint: [{ value: this.token.fromPoint, disabled: this.isReadOnly }, [validateLinePoint(this)]],
            toPoint: [{ value: this.token.toPoint, disabled: this.isReadOnly }, [validateLinePoint(this)]],
            obstacleDescriptionE: [{ value: this.token.obstacleDescriptionE, disabled: this.isReadOnly }, []],
            obstacleDescriptionF: [{ value: this.token.obstacleDescriptionF, disabled: this.isReadOnly }, []],
            elevation: [{ value: this.token.elevation, disabled: this.isReadOnly }, []],
            height: [{ value: this.token.height, disabled: this.isReadOnly }, []],
            refreshDummy: ""
        });

        this.$ctrl("elevation").setValidators(elevationValidatorFn(this.maxElevation));
        this.$ctrl("height").setValidators(elevationValidatorFn(this.maxHeight));

        this.$ctrl("areaType").valueChanges.debounceTime(1000).subscribe(() => this.changeMode());

        this.$ctrl("location").valueChanges.debounceTime(1000).subscribe((value) => this.locationChanged(value));
        this.$ctrl("radius").valueChanges.debounceTime(1000).subscribe(() => this.changeRadius());

        this.$ctrl("fromPoint").valueChanges.debounceTime(1000).subscribe((value) => this.locationToFromChanged('fromPoint', value));
        this.$ctrl("toPoint").valueChanges.debounceTime(1000).subscribe((value) => this.locationToFromChanged('toPoint', value));

        this.$ctrl("otherObstacleNameE").valueChanges.debounceTime(1000).subscribe((value) => this.otherObstacleNameChange(value));
        this.$ctrl("otherObstacleNameF").valueChanges.debounceTime(1000).subscribe((value) => this.otherObstacleNameChange(value));
        this.$ctrl("obstacleDescriptionE").valueChanges.debounceTime(1000).subscribe((value) => this.obstacleDescriptionChange(value));
        this.$ctrl("obstacleDescriptionF").valueChanges.debounceTime(1000).subscribe((value) => this.obstacleDescriptionChange(value));

        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("obstacleType", this.token.obstacleType);
        this.updateFormControlValue("otherObstacleNameE", this.token.otherObstacleNameE);
        this.updateFormControlValue("otherObstacleNameF", this.token.otherObstacleNameF);
        this.updateFormControlValue("areaType", this.token.areaType);
        this.updateFormControlValue("location", this.token.location);
        this.updateFormControlValue("radius", this.token.radius);
        this.updateFormControlValue("radiusUnit", this.token.radiusUnit);
        this.updateFormControlValue("fromPoint", this.token.fromPoint);
        this.updateFormControlValue("toPoint", this.token.toPoint);
        this.updateFormControlValue("obstacleDescriptionE", this.token.obstacleDescriptionE);
        this.updateFormControlValue("obstacleDescriptionF", this.token.obstacleDescriptionF);
        this.updateFormControlValue("elevation", this.token.elevation);
        this.updateFormControlValue("height", this.token.height);
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    private makeFirstElement(arr: any[], makeFirst: Function, insertUndefinedAtZero: boolean = true) {
        let len = arr.length;
        for (let i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
    }

    private swapArrayPositions(arr: any[], p1: number, p2: number) {
        const temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    }

    private showDoaMapRegion(doaId: number) {
        this.dataService
            .getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => this.renderMap(geoJsonStr));
    }

    private refreshMap() {
        if (this.isLine) {
            window.setTimeout(() => this.locationToFromChanged('toPoint', this.$ctrl('toPoint').value), 1000);
        }
        else {
            window.setTimeout(() => this.locationChanged(this.$ctrl('location').value), 1000);
        }
    }

    private renderMap(geoJsonStr: string) {
        const self = this;
        const interval = window.setInterval(function () {
            if (self.leafletmap && self.leafletmap.isReady()) {
                window.clearInterval(interval);
                self.leafletmap.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    }
}

function mobstTypeValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === -1) {
        return { 'required': true }
    }
    return null;
}

function validatePointLocation(controller: MObstLgUsCancelComponent): Function {
    return (ctrl: AbstractControl): { [key: string]: boolean } | null => {
        const location = ctrl.value;
        const isLineValue = controller.isLine;
        if (!isLineValue) {
            if (!location)
                return { required: true };
            if (controller.locationService.parse(location) === null)
                return { format: true };
        }
        return null;
    }
}

function validateLinePoint(controller: MObstLgUsCancelComponent): Function {
    return (ctrl: AbstractControl): { [key: string]: boolean } | null => {
        const location = ctrl.value;
        const isLineValue = controller.isLine;
        if (isLineValue) {
            if (!location)
                return { required: true };
            if (controller.locationService.parse(location) === null)
                return { format: true };
        }
        return null;
    }
}

function elevationValidatorFn(maxElevation: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 0 || +c.value > maxElevation) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxElevation) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function heightValidatorFn(maxHeight: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 0 || +c.value > maxHeight) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxHeight) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

function hasLowerCase(str: string): boolean {
    return (/[a-z]/.test(str));
}
