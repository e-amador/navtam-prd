"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdIcaoFormatComponent = void 0;
var core_1 = require("@angular/core");
//TODO: Move ICAO Generation Logic back to server
var NsdIcaoFormatComponent = /** @class */ (function () {
    function NsdIcaoFormatComponent() {
    }
    Object.defineProperty(NsdIcaoFormatComponent.prototype, "icaoNotamText", {
        //get icaoNotamText(): string {
        //    //return this.model && this.model.icaoText ? this.model.icaoText.aftnText : null;
        //    return this.model ? this.model.icaoText : null;
        //}
        get: function () {
            return this.icaoText;
        },
        enumerable: false,
        configurable: true
    });
    NsdIcaoFormatComponent.prototype.copyIcaoText = function () {
        this.copyToClipBoard(this.icaoText);
    };
    NsdIcaoFormatComponent.prototype.copyToClipBoard = function (text) {
        var el = document.createElement('textarea');
        el.value = text;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], NsdIcaoFormatComponent.prototype, "icaoText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], NsdIcaoFormatComponent.prototype, "loading", void 0);
    NsdIcaoFormatComponent = __decorate([
        core_1.Component({
            selector: 'icaoformat',
            templateUrl: '/app/dashboard/nsds/nsd-icaoformat.component.html'
        })
    ], NsdIcaoFormatComponent);
    return NsdIcaoFormatComponent;
}());
exports.NsdIcaoFormatComponent = NsdIcaoFormatComponent;
//# sourceMappingURL=nsd-icaoformat.component.js.map