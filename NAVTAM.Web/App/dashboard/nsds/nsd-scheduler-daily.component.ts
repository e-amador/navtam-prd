﻿import { Component, OnInit, OnDestroy, AfterViewInit, HostListener, Input, ChangeDetectorRef } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'
import { NsdFormComponent } from '../nsds/nsd-form.component';
import { WindowRef } from '../../common/windowRef.service';

import { ProposalStatus, ProposalType } from '../shared/data.model';

import * as moment from 'moment';

declare var $: any;

@Component({
    selector: 'scheduler-daily',
    templateUrl: '/app/dashboard/nsds/nsd-scheduler-daily.component.html'
})
export class NsdSchedulerDailyComponent implements OnInit, AfterViewInit, OnDestroy {

    public parent: NsdFormComponent = null;
    @Input('form') form: FormGroup;
    @Input('model') model: any;

    lastEventId: number = -1;
    currentEvent: any = null;
    displayStartDate: any = null;

    slootDuration = 15;
    scrollTime = '06:00:00';
    initialized: boolean = false;

    SR = "06:00";
    SS = "18:00";

    data: any = null;
    calendar: any;

    editingExtendedHours = false;

    eventDetails = {
        id: '',
        starttime: '',
        endtime: '',
        s_sunrise: false,
        s_sunset: false,
        e_sunrise: false,
        e_sunset: false,
        extendedHours: false,
        exStarttime: '',
        exEndtime: '',
        calendarType: ''
    }

    defaultColor = '#3a87ad';
    selectedColor = '#a1c5f761';

    startEventUpdate: any = null;
    start: any = null;
    end: any = null;
    starttime: string = null;
    endtime: string = null;
    hasExtendedHours: boolean = false;
    sheduleCleared: boolean = false;
    repeatToggle: boolean = false;   

    dpkStart: any = null;
    dpkEnd: any = null;

    dateEvents = [];

    public dailyRow: any = {
        starttime: "",
        endtime: ""
    }

    constructor(private windowRef: WindowRef, private cdRef: ChangeDetectorRef) {
    }


    ngOnInit() {
    }

    ngOnDestroy() {
    }


    ngAfterViewInit() {
        let self = this;
        this.calendar = $('#calendar');

        $('.d-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0, // 0: month view , 1: year view, 2: multiple year view
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "", // Disable 1 or various day. For monday and thursday: 1,3
                calendarWeeks: false, // Display week number 
                toggleActive: true, // Close other when open
                multidate: false, // Allow to select various days
            }).on('changeDate', function (e) {
                $(this).bootstrapDatepicker('hide');
                self.dateChanged(e)
            }).on('dp.show', function () {
            });
        });
    }

    initialize(data, start, end, slotDuration?, events?) {
        this.data = data;

        let currentDate = moment();

        this.initCalendar(this.calendar, currentDate);

        if (this.data) {

            if (this.data.length > 0) {
                start = moment(start);
                end = moment(end);

                this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
                this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));
            }

            for (let i = 0; i < this.data.length; i++) {
                let obj = JSON.parse(this.data[i].event);

                let event = this.setEvent(obj);

                if (i === 0) {
                    this.starttime = start.format("HH:mm");
                    this.endtime = end.format("HH:mm");
                    currentDate = event.start;
                    this.displayStartDate = start;
                    this.scrollTime = `${this.starttime}:00`;
                }
                this.calendar.fullCalendar('renderEvent', event, true);
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        } else if (events) {
            for (let i = 0; i < events.length; i++) {
                let evt = this.setEvent(events[i]);
                if (i === 0) {
                    currentDate = evt.start;
                }
                this.calendar.fullCalendar('renderEvent', evt, true);
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        } else { 
            //trick to enforce refreshing the calendar when modifications were made with calendar hidden 
            this.calendar.fullCalendar('prev'); 
            this.calendar.fullCalendar('next');
        }
    }

    toggleShedule(event, clearItemD: boolean = false) {
        let isPannelOpenNow = this.isPanelOpen();

        if (event) {
            event.preventDefault();
            this.repeatToggle = true;
        } else {
            this.repeatToggle = false;
        }

        if (!this.initialized) {
            this.slideToggle();
        }

        this.showSchedule(this.parent.type === 'clone' ? false : clearItemD);

        let isPannelOpenThen = this.isPanelOpen();

        if (isPannelOpenNow === isPannelOpenThen) {
            if (!isPannelOpenNow || !this.repeatToggle) {
                this.slideToggle();
            }
        }
    }


    deleteEvents(event, clearItemD: boolean = true) {
        if (event && clearItemD) {
            if (this.data && this.data.length > 0) {
                this.parent.resetSchedulersStatus();
            }
            event.preventDefault();
        }

        this.dateEvents = [];
        this.parent.hasScheduler = false;

        this.data = new Object();

        this.start = null;
        this.end = null;
        this.starttime = null;
        this.endtime = null;

        this.clearSchedule(clearItemD);

        if (clearItemD) {
            let enforceToggle = this.parent.type === 'clone' || this.parent.model.proposalId;
            this.parent.schedulerDayWeeks.toggleScheduleAfterDeleteAction(enforceToggle, true);
            this.parent.schedulerDate.toggleScheduleAfterDeleteAction(enforceToggle, true);
        }
        this.parent.enableActivePeridDates();
        this.data = null;
    }


    resetClearScheduleFlag() {
        this.sheduleCleared = true;
        this.repeatToggle = false;
    }


    slideToggle() {
        $('#schedule-daily-content').slideToggle();
    }

    isPanelOpen() {
        return $('#schedule-daily-content').css('display') !== 'none';
    }

    closePanelIfOpen() {
        if (this.isPanelOpen()) {
            this.slideToggle();
        }
    }

    removeEvents() {
        if (this.hasEvents()) {
            this.calendar.fullCalendar('removeEvents');
            this.dateEvents = [];
            this.data = null;

            this.start = null; 
            this.end = null; 

            $('#d-start').bootstrapDatepicker('update', '');
            $('#d-end').bootstrapDatepicker('update', '');


            this.toggleScheduleAfterDeleteAction(true, true);
        }
    } 

    private clearSchedule(clearItemD : boolean) {
        if (this.data) {
            this.toggleScheduleAfterDeleteAction(true, this.initialized);

            $('#d-start').bootstrapDatepicker('update', '');
            $('#d-end').bootstrapDatepicker('update', '');

            this.calendar.fullCalendar('destroy');

            let start = null;
            let end = null;
            if (this.start) {
                let starttime = moment(this.starttime, 'HH:mm');
                let endtime = moment(this.endtime, 'HH:mm');

                start = moment(this.start).set({
                    hour: starttime.get('hour'),
                    minute: starttime.get('minute'),
                })
                end = moment(this.end).set({
                    hour: endtime.get('hour'),
                    minute: endtime.get('minute'),
                })
            }

            this.initialize((this.parent.type === 'clone' || this.parent.model.proposalId) ? this.data : null, start, end);

            if (start && start.isValid()) {
                this.calendar.fullCalendar('gotoDate', start);
            } else {
                this.calendar.fullCalendar('gotoDate', moment());
            }
        } else {
            if (clearItemD) {
                if (this.parent.type === 'clone') {
                    this.toggleScheduleAfterDeleteAction(this.parent.model.proposalId ? true : false, this.initialized);
                } else {
                    this.toggleScheduleAfterDeleteAction(this.sheduleCleared, this.hasEvents());
                    this.sheduleCleared = false;
                }
            } 

            this.initialize(null, this.start, this.end);
        }

        $('#daily-midnight').mask('00:00');
    }

    toggleScheduleAfterDeleteAction(enforce: boolean, initialized : boolean) {
        if (enforce) {
            this.slideToggle();
            this.initialized = initialized;
        }
    }

    showSchedule(clearItemD: boolean) {
        this.clearSchedule(clearItemD);

        if (!this.displayStartDate) {
            this.displayStartDate = moment();
        }

        if (this.start) {
            this.start = moment.isMoment(this.start) ? this.start : moment(this.start);
            this.end = moment.isMoment(this.end) ? this.end : moment(this.end);

            $('#d-start').bootstrapDatepicker('update', this.start.toDate());
        }
        if (this.end) {
            $('#d-end').bootstrapDatepicker('update', this.end.toDate());
        }
    }

    zoomIn() {
        if (this.slootDuration > 0) {
            this.slootDuration -= 1;
            let events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');
            this.initialize(this.data, this.starttime, this.endtime, this.slootDuration, events);
        }
    }

    zoomOut() {
        if (this.slootDuration < 15) {
            this.slootDuration += 1;
            let events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');
            this.initialize(this.data, this.starttime, this.endtime, this.slootDuration, events);
        }
    }

    hasEvents() {
        return this.dateEvents.length > 0;
    }

    isClearButtonDisabled() {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }

        if (this.hasEvents()) {
            return false;
        }

        if (this.calendar) {
            var events = this.calendar.fullCalendar('clientEvents');
            return events.length <= 0;
        }

        return true;
    }

    checkboxChanged(control: string, e: any) {
        switch (control) {
            case 's_sunrise':
                this.eventDetails.s_sunrise = !this.eventDetails.s_sunrise;
                this.eventDetails.s_sunset = false;
                if (this.eventDetails.s_sunrise) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.starttime = this.SR;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            let hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 6) {
                                this.eventDetails.endtime = "07:00";
                            }
                        } else {
                            this.eventDetails.endtime = "07:00";
                        }
                    }
                }
                break;
            case 's_sunset':
                this.eventDetails.s_sunset = !this.eventDetails.s_sunset;
                this.eventDetails.s_sunrise = false;
                if (this.eventDetails.s_sunset) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.e_sunset = false;
                    this.eventDetails.starttime = this.SS;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            let hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 18) {
                                this.eventDetails.endtime = "22:00";
                            }
                        } else {
                            this.eventDetails.endtime = "22:00";
                        }
                    }
                }
                break;
            case 'e_sunrise':
                this.eventDetails.e_sunrise = !this.eventDetails.e_sunrise;
                this.eventDetails.e_sunset = false;
                if (this.eventDetails.e_sunrise) {
                    this.eventDetails.s_sunrise = false;
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.extendedHours = false;
                    this.eventDetails.exStarttime = "";
                    this.eventDetails.exEndtime = "";
                    this.eventDetails.endtime = this.SR;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            let hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 6) {
                                this.eventDetails.starttime = "04:00";
                            }
                        } else {
                            this.eventDetails.starttime = "04:00";
                        }
                    }
                }
                break;
            case 'e_sunset':
                this.eventDetails.e_sunset = !this.eventDetails.e_sunset;
                this.eventDetails.e_sunrise = false;
                if (this.eventDetails.e_sunset) {
                    this.eventDetails.extendedHours = false;
                    this.eventDetails.exStarttime = "";
                    this.eventDetails.exEndtime = "";
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.endtime = this.SS;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            let hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 18) {
                                this.eventDetails.starttime = "13:00";
                            }
                        } else {
                            this.eventDetails.starttime = "13:00";
                        }
                    }
                }
            break;
        }
    }

    isValid() {
        if (this.parent && this.parent.isReadOnly) {
         }

        let hasStart = $('#d-start').bootstrapDatepicker('getDate') != null;
        let hasEnd = $('#d-end').bootstrapDatepicker('getDate') != null;
        let hasEvents = false;
        if (this.calendar) {
            let events = this.getCalendarEvents();
            hasEvents = events.length > 0;
        }

        return hasStart && hasEnd && hasEvents;
    }

    private isStartHasValue() : boolean {
        return $('#d-start').bootstrapDatepicker('getDate') != null;
    }

    isStartValidDate(): boolean {
        if (this.isStartHasValue()) {
            let start = moment($('#d-start').bootstrapDatepicker('getDate')).format("YYYY/MM/DD");
            let now = moment().format("YYYY/MM/DD");

            return moment(start).isSameOrAfter(now);
        }

        return true;
    }

    isEndHaveValue(): boolean {
        return $('#d-end').bootstrapDatepicker('getDate') != null;
    }

    isEndValidDate(): boolean {
        let start = moment($('#d-start').bootstrapDatepicker('getDate'));
        let end = moment($('#d-end').bootstrapDatepicker('getDate'));
        
        if (start.isValid() && end.isValid()) {
            return (start <= end);
        }

        return true;
    }

    parseEvents(toggle: boolean = false) {
        this.dateEvents = [];

        let events = this.calendar.fullCalendar('clientEvents');
        events = events.sort(this.sortEvents);

        if (this.data && this.data.length > 0) {
            let proposalId = this.data[0].proposalId;
            this.resetDataEvents(proposalId, events);
        }

        let details = this.getDailyEvent(events);

        let itemD = "";
        for (let k = 0; k < details.length; k++) {
            this.updateDataEvent(details[k]);

            if (details[k].extendedHours && details[k].startTime === "00:00") {
                continue;
            }

            let row = {
                start: details[k].startTime,
                end: details[k].extendedHours ? details[k].extEndTime : details[k].endTime,
            }

            itemD = itemD + `${row.start.replace(':', '')}-${row.end.replace(':', '')} `;

            if (details[k].extendedHours) {
                row.end = row.end + ' [next day]'
            }
            this.dateEvents.push(row);


            //let rowStart = (details[k].startTime === "SR" ? this.SR : (details[k].startTime === "SS" ? this.SS : details[k].startTime))
            //let rowEnd = (details[k].endTime === "SR" ? this.SR : (details[k].endTime === "SS" ? this.SS : (details[k].extendedHours ? details[k].extEndTime : details[k].endTime)));

            //let _start = moment(this.start);
            //let _end = moment(this.end);

            //let momentRowStart = moment.utc(`${_start.year()}/${_start.month()}/${_start.day()} ${rowStart}`, 'yyyy/mm/dd HH:mm');
            //let momentRowEnd = moment.utc(`${_end.year()}/${_end.month()}/${_end.day()} ${rowEnd}`, 'yyyy/mm/dd HH:mm');

            //let row = {
            //    start: momentRowStart.format('HH:mm'),
            //    end: momentRowEnd.format('HH:mm')
            //}

            //itemD = itemD + `${row.start.replace(':', '')}-${row.end.replace(':', '')} `;

            //if (details[k].extendedHours) {
            //    row.end = row.end + ' [next day]'
            //}
            //this.dateEvents.push(row);
        }

        itemD = 'DAILY ' + itemD.trim();

        this.parent.hasScheduler = true;

        if (this.start && this.end) {
            let startDate = moment(this.start);
            let endDate = moment(this.end);
            if (details[details.length - 1].extendedHours) {
                //endDate = endDate.add("days", 1);
                this.end = moment(endDate);
            }

            let sDay = startDate.format('DD');
            let sMonth = startDate.format('MM');
            let sYear = startDate.format('YYYY');
            this.starttime = details[0].startTime;

            let sTime = (this.starttime === "SR"
                ? this.getSunriseTime(details[0].start)
                : (this.starttime === "SS" ? this.getSunsetTime(details[0].start) : this.starttime));

            let eDay = endDate.format('DD');
            let eMonth = endDate.format('MM');
            let eYear = endDate.format('YYYY');

            this.endtime = details[details.length - 1].extendedHours ? details[details.length - 1].exEndtime : details[details.length - 1].endTime;
            let eTime = details[details.length - 1].extendedHours
                ? details[details.length - 1].exEndtime
                : (this.endtime === "SR" ? this.getSunriseTime(details[details.length - 1].end) : (this.endtime === "SS" ? this.getSunsetTime(details[details.length - 1].end) : this.endtime));;

            let start = moment.utc(`${sDay}-${sMonth}-${sYear} ${sTime}`, 'DD-MM-YYYY HH:mm');
            let end = moment.utc(`${eDay}-${eMonth}-${eYear} ${eTime}`, 'DD-MM-YYYY HH:mm');

            this.parent.disableActivePeridDates(start, end, itemD);
        }

        if (toggle) {
            this.slideToggle();
        }
    }

    slideScheduleClose() {
        this.slideToggle();
        this.initialized = true;
    }

    getCalendarEvents() {
        return this.calendar.fullCalendar('clientEvents');
    }

    isUpdateButtonDisabled() {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }

        let _start = moment(this.eventDetails.starttime, "HH:mm");
        let _end = moment(this.eventDetails.endtime, "HH:mm");

        if ( !_start.isValid() || !_end.isValid() )
            return true;

        return false;
    } 

    private getSunriseTime(date: string) {
        let time = moment(date).format('HH:mm')

        return time !== this.SR ? time : this.SR;
    }

    private getSunsetTime(date: string) {
        let time = moment(date).format('HH:mm')

        return time !== this.SS ? time : this.SS;
    }

    private sortEvents(event1, event2) {
        var start1 = new Date(event1.start).getTime();
        var start2 = new Date(event2.start).getTime();
        return start1 > start2 ? 1 : -1;
    };

    private dateChanged(ev) {
        switch (ev.currentTarget.id) {
            case "d-start":
                this.start = moment(new Date(ev.currentTarget.value));
                this.calendar.fullCalendar('removeEvents');
                this.calendar.fullCalendar('gotoDate', this.start);

                if (this.end && this.end.isValid()) {
                    let el = $('#d-start');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
            case "d-end":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    let el = $('#d-end');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
        }
    }

    private getDailyEvent(dailyEvents) {
        let events = []
        for (let i = 0; i < dailyEvents.length; i++) {
            let startEvent = dailyEvents[i].start;
            let endEvent = dailyEvents[i].end;

            let startHour = startEvent.format('HH');
            let startMinute = startEvent.format('mm');

            let endHour = endEvent.format('HH');
            let endMinute = endEvent.format('mm');

            let start = null;
            let end = null;
            if (dailyEvents[i].s_sunrise) {
                start = "SR";
            }
            else if (dailyEvents[i].s_sunset) {
                start = "SS";
            }
            if (dailyEvents[i].e_sunrise) {
                end = "SR";
            }
            else if (dailyEvents[i].e_sunset) {
                end = "SS";
            }

            start = start ? start : moment(`${startHour}:${startMinute}`, 'HH:mm').format('HH:mm');
            end = end ? end : moment(`${endHour}:${endMinute}`, 'HH:mm').format('HH:mm');

            events.push({
                id: dailyEvents[i]._id || dailyEvents[i].id,
                startTime: start,
                endTime: end,
                start: dailyEvents[i].start,
                end: dailyEvents[i].end,
                s_sunrise: dailyEvents[i].s_sunrise,
                e_sunrise: dailyEvents[i].e_sunrise,
                s_sunset: dailyEvents[i].s_sunset,
                e_sunset: dailyEvents[i].e_sunset,
                extendedHours: dailyEvents[i].extendedHours,
                extEndTime: dailyEvents[i].exEndtime,
                exStarttime: dailyEvents[i].exStarttime,
                exEndtime: dailyEvents[i].exEndtime,
            });

        }

        return events.length > 0 ? this.removeDuplicates(events) : events;
    }

    private removeDuplicates(myArr) {
        var props = Object.keys(myArr[0])
        return myArr.filter((item, index, self) =>
            index === self.findIndex((t) => (
                props.every(prop => {
                    return t[prop] === item[prop]
                })
            ))
        )
    }

    private initCalendar(calendar: any, currentDate: any) {
        const self = this;

        let duration = `00:${this.windowRef.zeroPad(this.slootDuration, 2)}:00`
        calendar.fullCalendar({
            defaultDate: moment(currentDate),
            defaultView: 'agendaDay',
            contentHeight: 820,
            slotDuration: duration,
            allDaySlot: false,
            selectable: true,
            editable: true,
            displayEventTime: false,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            eventLimit: true, // allow "more" link when too many events, 
            fixedWeekCount: false,
            eventOverlap: false,
            //scrollTime: firstEventTime,
            columnFormat: {
                day: false,
            },
            header: false,
            axisFormat: 'HH:mm',
            timeFormat: 'HH:mm{ - HH:mm}',
            //maxTime: "23:59:00",
            nextDayThreshold: '00:00',
            minTime: 0,
            scrollTime: this.scrollTime,
            eventDragStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            eventDragStop: function (event, jsEvent, ui, view) {
            },
            viewRender: function (view, element) {
                element.find('.fc-day-header').html('');
            },
            eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                self.startEventUpdate = null;
                var isSameDay = event.start.isSame(event.end, 'day');
                if (!isSameDay) { // crossed midnight
                    revertFunc();
                }
            },
            eventResizeStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            select: function (start, end, event, view) {
                let eventData = {
                    start: start,
                    end: end,
                    s_sunrise: false,
                    s_sunset: false,
                    e_sunrise: false,
                    e_sunset: false,
                    allDay: false,
                    extendedHours: false,
                    exStarttime: '',
                    exEndtime: ''
                };

                if (!self.parent.isReadOnly && !self.isOverlapping({ start: start, end: end }) && self.isEventTimeAllowed(eventData)) {
                    self.calendar.fullCalendar('renderEvent', eventData, true);

                    self.parent.hasScheduler = true;
                }
                self.calendar.fullCalendar('unselect');
            },
            eventResizeStop: function (event, jsEvent, ui, view) {
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                self.startEventUpdate = null; // ??

                if (!self.isEventTimeAllowed(event)) {
                    revertFunc();
                }
            },
            eventRender: function (event, element, view) {
                if (self.parent && self.parent.isReadOnly) {
                    event.editable = false;
                }

                let time = event.sunrise ? "SUNRISE - " : '{ ' + event.start.format('HH:mm') + ' - ';
                if (event.sunset) {
                    time = time + 'SUNSET'
                } else {
                    time = time + event.end.format('HH:mm') + ' }';
                }

                element.find('.fc-content').prepend('<span><i class="fa fa-clock-o m-r-5"></i>' + time + '</span>');
                element.css('background-color', event.eventColor);
                element.css('border-color', event.eventColor);
            },
            eventAfterRender: function (event, element, view) {
                if (event.extendedHours) {
                    let title = `${element.text()} - { ${event.exStarttime} - ${event.exEndtime} }`; 
                    element.html(title);
                }
            },
            eventClick: function (event, jsEvent, view) {
                let el = $('#calendar-daily-modal');

                self.eventDetails.starttime = event.start.format('HH:mm');
                self.eventDetails.endtime = event.end.format('HH:mm');

                $('.time').mask('00:00');
                el.modal();

                self.eventDetails.id = event.id;
                self.eventDetails.s_sunrise = event.s_sunrise;
                self.eventDetails.s_sunset = event.s_sunset;
                self.eventDetails.e_sunrise = event.e_sunrise;
                self.eventDetails.e_sunset = event.e_sunset;
                self.eventDetails.extendedHours = event.extendedHours;
                self.eventDetails.exStarttime = event.exStarttime;
                self.eventDetails.exEndtime = event.exEndtime;

                self.hasExtendedHours = event.extendedHours;

                self.editingExtendedHours = event.extendedHours;

                el.on('shown.bs.modal', function (e) {
                    self.cdRef.detectChanges();
                });

                el.on('hidden.bs.modal', function () {
                    $(this).data('bs.modal', null);
                });

                el.on('hide.bs.modal', function (e) {
                    let pressedButton = $(document.activeElement).attr('id');
                    if (pressedButton === "daily_btn-modal-yes") {
                        let _start = moment(self.eventDetails.starttime, "HH:mm").format("HH:mm");
                        let _end = moment(self.eventDetails.endtime, "HH:mm").format("HH:mm");
                        if (_start.length === 5 && _end.length === 5) {
                            let eventStartTime = event.start.format('HH:mm');
                            let eventEndTime = event.end.format('HH:mm');

                            let events = calendar.fullCalendar('clientEvents');
                            let count = 0;
                            let lastParentId = events[0].parentId;
                            for (let i = 0; i < events.length; i++) {
                                let startTime = events[i].start.format('HH:mm');
                                let endTime = events[i].end.format('HH:mm');
                                if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId) && eventStartTime === startTime && eventEndTime == endTime) {
                                    if (events[i].parentId !== lastParentId) {
                                        lastParentId = events[i].parentId;
                                        count = 0;
                                    }
                                    count += 1;
                                    let start = events[i].start.clone();
                                    let end = events[i].end.clone();

                                    events[i].start = events[i].start.set({
                                        hour: parseInt(_start.substring(0, 2)),
                                        minute: parseInt(_start.substring(3))
                                    });
                                    events[i].end = events[i].end.set({
                                        hour: parseInt(_end.substring(0, 2)),
                                        minute: parseInt(_end.substring(3))
                                    });

                                    if (self.isOverlapping(events[i])) {
                                        events[i].start = start;
                                        events[i].end = end;
                                    } else {
                                        events[i].s_sunrise = self.eventDetails.s_sunrise;
                                        events[i].s_sunset = self.eventDetails.s_sunset;
                                        events[i].e_sunrise = self.eventDetails.e_sunrise;
                                        events[i].e_sunset = self.eventDetails.e_sunset;
                                        events[i].extendedHours = self.eventDetails.extendedHours;
                                        events[i].exStarttime = self.eventDetails.exStarttime;
                                        events[i].exEndtime = self.eventDetails.exEndtime;

                                        //let eventColor = events[i].eventColor;
                                        if (self.eventDetails.s_sunrise || self.eventDetails.s_sunset || self.eventDetails.e_sunrise || self.eventDetails.e_sunset) {
                                            events[i].previousColor = events[i].eventColor;
                                            events[i].eventColor = '#2b3c4e';
                                        } else {
                                            if (event.eventColor === '#2b3c4e') {
                                                events[i].eventColor = events[i].previousColor;
                                                events[i].previousColor = null;
                                            }
                                        }

                                        calendar.fullCalendar('updateEvent', events[i]);
                                    }
                                }
                            }
                        }
                    } else {
                        if (pressedButton === "daily_btn-modal-delete") {
                            calendar.fullCalendar('removeEvents', event._id);
                            calendar.fullCalendar('renderEvents');
                        }
                    }
                    self.eventDetails.s_sunrise = false;
                    self.eventDetails.s_sunset = false;

                    $('#calendar-daily-modal').off('hide.bs.modal');
                });

            },
        });
    }

    private updateDataEvent(event) {
        if (!this.data) {
            return
        };

        for (let i = 0; i < this.data.length; i++) {
            let obj = JSON.parse(this.data[i].event);

            let id = event._id || event.id;
            if (obj.id !== id) {
                continue
            };

            obj.start = event.start; 
            obj.end = event.end;
            obj.s_sunset = event.s_sunset;
            obj.s_sunrise = event.s_sunrise;
            obj.e_sunset = event.e_sunset;
            obj.e_sunrise = event.e_sunrise;
            obj.extendedHours = event.extendedHours;
            obj.exStarttime = event.exStarttime;
            obj.exEndtime = event.exEndtime;

            this.data[i].event = JSON.stringify(obj);
        }
    }

    private resetDataEvents(proposalId, cEvents) {
        this.data = [];
        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent._id || cEvent.id,
                start: cEvent.start,
                end: cEvent.end,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                exStarttime: cEvent.exStarttime,
                exEndtime: cEvent.exEndtime,
                extendedHours: cEvent.extendedHours
            };

            this.data.push({
                calendarType: 1,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event.id,
                itemD: this.model.itemD,
                event: JSON.stringify(event)
            });
        }
    }

    private isOverlapping(event) {
        let array = this.calendar.fullCalendar('clientEvents');
        for (let i in array) {
            if (array[i]._id != event._id) {
                if (!(array[i].start >= event.end || array[i].end <= event.start)) {
                    return true;
                }
            }
        }
        return false;
    }

    private onExtendedHours() {
        this.eventDetails.extendedHours = !this.eventDetails.extendedHours;
        if (!this.eventDetails.extendedHours) {
            this.eventDetails.exStarttime = "";
            this.eventDetails.exEndtime = "";
        } else {
            this.eventDetails.endtime = "23:59";
            this.eventDetails.e_sunrise = false;
            this.eventDetails.e_sunset = false;
            this.eventDetails.exStarttime = "00:00";
        }
    }

    private IsValidDate(selectedDate, calendarDate) {
        let dayOfMonth = parseInt(selectedDate.format('D'));
        return selectedDate.format('M') === calendarDate.format('M') && dayOfMonth >= parseInt(moment().format('D'));
    }

    private findEventById(events, eventId) {
        for (let i = 0; i < events.length; i++) {
            if (events[i].id == eventId) {
                return events[i].id;
            }
        }
        return null;
    }

    private isEventTimeAllowed(event) {
        //let result = event.start._d.getDay() === event.end._d.getDay();

        let minTime = moment('00:00', 'HH: mm');
        let maxTime = moment('00:05', 'HH: mm');

        let invalidTime = moment(event.start.format('HH:mm'), 'HH:mm').isBefore(minTime) || moment(event.end.format('HH:mm'), 'HH:mm').isSameOrBefore(maxTime);

        return !invalidTime;
    }

    private setEvent(obj) {
        let event = {
            id: obj.id,
            start: moment.utc(obj.start),
            end: moment.utc(obj.end),
            previousColor: obj.previousColor,
            s_sunset: obj.s_sunset,
            s_sunrise: obj.s_sunrise,
            e_sunset: obj.e_sunset,
            e_sunrise: obj.e_sunrise,
            extendedHours: obj.extendedHours,
            exStarttime: obj.exStarttime,
            exEndtime: obj.exEndtime,
            allDay: false,
            stick: true
        };

        return event;
    }
}


