"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdButtonBarComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//import { ProposalStatus, ProposalType } from '../shared/data.model';
var windowRef_service_1 = require("../../common/windowRef.service");
var nsd_georeftool_component_1 = require("../nsds/nsd-georeftool.component");
var data_service_1 = require("../shared/data.service");
var toastr_service_1 = require("../../common/toastr.service");
var NsdButtonBarComponent = /** @class */ (function () {
    function NsdButtonBarComponent(windowRef, activatedRoute, dataService, toastr) {
        this.windowRef = windowRef;
        this.activatedRoute = activatedRoute;
        this.dataService = dataService;
        this.toastr = toastr;
        this.isNOF = false;
        this.isReadOnly = false;
        this.parent = null;
        this.canDoCatchAll = false;
        this.isActionExecuted = false;
        this.isCancellation = false;
        this.canReject = true;
        this.canPark = true;
        this.actualOrgId = 0;
        this.source = '';
        this.points = [];
        this.canDoCatchAll = this.windowRef.appConfig.canDoCatchAll;
        this.actualOrgId = this.windowRef.appConfig.orgId;
        this.source = this.activatedRoute.snapshot.params["source"];
    }
    NsdButtonBarComponent.prototype.ngOnInit = function () {
        var notamId = this.model.notamId;
        if (notamId) {
            this.isCancellation = notamId[0] === "C";
        }
        this.canReject = this.model.canBeRejected;
        //this.canReject = true;
        if (this.source && this.canReject) {
            if (this.source === 'notam') {
                this.canReject = false;
            }
        }
        if (this.isNOF) {
            //if (this.model.proposalType === ProposalType.Replace || this.isCancellation) this.canPark = false;
        }
    };
    NsdButtonBarComponent.prototype.ngOnChanges = function () {
    };
    NsdButtonBarComponent.prototype.backToProposals = function () {
        if (this.parent) {
            if (this.isNOF) {
                this.parent.backToNofProposals();
            }
            else {
                this.parent.backToProposals();
            }
        }
    };
    NsdButtonBarComponent.prototype.backToReviewText = function () {
        if (this.parent) {
            return this.parent.txtBackToReviewList;
        }
        return "";
    };
    NsdButtonBarComponent.prototype.enableButtonActions = function () {
        this.isActionExecuted = false;
    };
    NsdButtonBarComponent.prototype.discardProposal = function () {
        var self = this;
        if (this.parent) {
            this.parent.confirmDiscardProposal(function (error) { return self.enableButtonActions(); });
        }
    };
    NsdButtonBarComponent.prototype.parkProposal = function () {
        var self = this;
        if (this.parent && !this.isActionExecuted) {
            this.isActionExecuted = true;
            this.parent.parkProposal(function (error) { return self.enableButtonActions(); });
        }
    };
    NsdButtonBarComponent.prototype.saveDraft = function () {
        if (this.parent) {
            this.parent.saveDraft();
        }
    };
    NsdButtonBarComponent.prototype.submitProposal = function () {
        var self = this;
        if (this.parent && !this.isActionExecuted) {
            this.isActionExecuted = true;
            this.parent.submitProposal(function (error) { return self.enableButtonActions(); });
        }
    };
    NsdButtonBarComponent.prototype.disseminateProposal = function () {
        var self = this;
        if (this.parent && !this.isActionExecuted) {
            this.isActionExecuted = true;
            this.parent.disseminateProposal(function (error) { return self.enableButtonActions(); });
        }
    };
    NsdButtonBarComponent.prototype.createProposalGroup = function () {
        if (this.parent) {
            this.parent.showGrouping();
        }
    };
    NsdButtonBarComponent.prototype.isCreateGroupButtonDisabled = function () {
        return this.parent && this.parent.isGroupingProposalDisable();
    };
    NsdButtonBarComponent.prototype.isDisseminateButtonDisabled = function () {
        return this.parent && this.parent.isSubmitProposalButtonDisabled(); //!this.parent.nsdForm.valid &&
    };
    NsdButtonBarComponent.prototype.isSaveButtonDisabled = function () {
        return this.parent && this.parent.isSaveDraftButtonDisabled();
    };
    NsdButtonBarComponent.prototype.isParkProposalButtonDisabled = function () {
        return this.parent && this.parent.isParkProposalDisabled();
    };
    NsdButtonBarComponent.prototype.openRejectProposalDialog = function () {
        if (this.parent) {
            this.parent.openRejectProposalDialog();
        }
    };
    NsdButtonBarComponent.prototype.isSubmitButtonDisabled = function () {
        return this.parent && this.parent.isSubmitProposalButtonDisabled();
    };
    NsdButtonBarComponent.prototype.isRejectProposalDisable = function () {
        return this.parent && this.parent.isRejectProposalDisable();
    };
    NsdButtonBarComponent.prototype.showActionButtons = function () {
        if (this.isReadOnly)
            return false;
        if (this.model.categoryId === 2) {
            return this.canDoCatchAll;
        }
        return true;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], NsdButtonBarComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('isNOF'),
        __metadata("design:type", Boolean)
    ], NsdButtonBarComponent.prototype, "isNOF", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], NsdButtonBarComponent.prototype, "isReadOnly", void 0);
    __decorate([
        core_1.ViewChild(nsd_georeftool_component_1.NsdGeoRefToolComponent),
        __metadata("design:type", nsd_georeftool_component_1.NsdGeoRefToolComponent)
    ], NsdButtonBarComponent.prototype, "geoRefTool", void 0);
    NsdButtonBarComponent = __decorate([
        core_1.Component({
            selector: 'nsd-buttonbar',
            templateUrl: '/app/dashboard/nsds/nsd-buttonbar.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [windowRef_service_1.WindowRef, router_1.ActivatedRoute, data_service_1.DataService, Object])
    ], NsdButtonBarComponent);
    return NsdButtonBarComponent;
}());
exports.NsdButtonBarComponent = NsdButtonBarComponent;
//# sourceMappingURL=nsd-buttonbar.component.js.map