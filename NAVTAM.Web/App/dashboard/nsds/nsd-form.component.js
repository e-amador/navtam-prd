"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdFormComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var nsd_buttonbar_component_1 = require("../nsds/nsd-buttonbar.component");
var nsd_scheduler_daily_component_1 = require("../nsds/nsd-scheduler-daily.component");
var nsd_scheduler_date_component_1 = require("../nsds/nsd-scheduler-date.component");
var nsd_scheduler_days_week_component_1 = require("../nsds/nsd-scheduler-days-week.component");
var nsd_scheduler_free_text_component_1 = require("../nsds/nsd-scheduler-free-text.component");
var nsd_georeftool_component_1 = require("../nsds/nsd-georeftool.component");
//import { Select2OptionData } from 'ng2-select2';
//import { DynamicComponent } from '../nsds/dynamic.component';
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var toastr_service_1 = require("./../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var moment = require("moment");
var NsdFormComponent = /** @class */ (function () {
    function NsdFormComponent(toastr, fb, dataService, memStorageService, router, activeRoute, windowRef, changeDetectionRef, translation, location) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activeRoute = activeRoute;
        this.windowRef = windowRef;
        this.changeDetectionRef = changeDetectionRef;
        this.translation = translation;
        this.location = location;
        this.isNOF = false;
        this.tabActive = 1;
        this.groupModel = [];
        this.endValidityText = "";
        this.startActivityText = "";
        this.selectedProposalAnnotationId = -1;
        this.isSaveButtonDisabled = true;
        this.isSubmitButtonDisabled = true;
        this.isGroupingEnabled = false;
        this.waitingForGeneratingIcao = false;
        this.loadingPrevIcao = false;
        this.isPrevIcaoTabVisible = false;
        this.prevIcaoText = "";
        this.isWaitingForResponse = false;
        this.openNotesToNof = false;
        this.seletedOriginatorId = "-1";
        this.originators = [];
        this.mapHealthy = false;
        this.waitForMapTick = true;
        this.schedulerPanelOpen = false;
        this.schedulerTab = 1;
        this.hasScheduler = false;
        this.isSchedulePanelOpen = false;
        this.isScheduleButtonActionVisible = true;
        this.isRemoveScheduleAlertShown = false;
        this.type = "";
        this.isScheduleOverridden = false;
        this.overrideBilingual = false;
        this.componentData = null;
        this.dateFormat = "YYMMDDHHmm";
        this.backToReviewListLabel = this.translation.translate('ActionButtons.BackToReviewList');
        this.isNewProposal = true;
    }
    NsdFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.location.subscribe(function (x) {
            _this.backToProposals();
        });
        this.dataService.override.subscribe(function (value) {
            _this.isSubmitButtonDisabled = true;
            _this.isSaveButtonDisabled = true;
            _this.overrideBilingual = value;
        });
        this.configureReactiveForm();
        this.isSaveButtonDisabled = this.isSaveButtonDisabled || this.isReadOnly;
        this.type = this.activeRoute.snapshot.data[0]['type'];
        if (this.windowRef.appConfig.isNof) {
            if (this.model.proposalType === data_model_1.ProposalType.Cancel && !this.isReadOnly) {
                this.type = "cancel"; //re-enforce cancellation for NOF when "Pick" action
            }
        }
        if (this.isReadOnly) {
            this.txtBackToReviewList = this.backToReviewListLabel;
        }
        else {
            var fromWidthdrawnAction = this.memStorageService.get(this.memStorageService.MODIFY_ACTION_KEY);
            this.txtBackToReviewList = fromWidthdrawnAction ? this.translation.translate('ActionButtons.CancelAnd') + this.backToReviewListLabel : this.backToReviewListLabel;
        }
        this.model.token.type = this.type;
        this.componentData = {
            catId: this.model.categoryId,
            version: this.model.version,
            inputs: {
                form: this.nsdForm,
                type: this.type,
                isReadOnly: this.isReadOnly,
                token: this.model.token,
                proposalType: this.model.proposalType
            }
        };
        this.getPrevIcaoText();
        this.loadMatchingOriginators(this.model.token.originator);
        this.openNotesToNof = this.model.token.annotations.length > 0;
        if (this.model.proposalId) { //when loading a saved proposal wait until form is ready
            if (this.model.immediate || this.model.permanent || this.model.estimated) {
                this.isScheduleButtonActionVisible = false;
            }
            else if (this.model.itemD) {
                this.loadScheduler(this.model.proposalId);
            }
            if (!this.isReadOnly) {
                if (this.type === "cancel" || this.type === "replace") {
                    this.isSaveButtonDisabled = false;
                }
                else {
                    if (!this.nsdForm.valid) {
                        this.nsdForm.markAsDirty();
                        //todo here
                    }
                    this.isSaveButtonDisabled = !this.nsdForm.valid;
                }
            }
        }
        else {
            if (this.type === "clone") {
                if (this.model.immediate || this.model.permanent || this.model.estimated) {
                    this.isScheduleButtonActionVisible = false;
                }
                this.isSaveButtonDisabled = false;
                //this.loadScheduler(this.activeRoute.snapshot.params['id']);
            }
        }
        if (this.model.status !== 0 || this.type === 'clone') {
            this.overrideBilingual = this.model.token.overrideBilingual;
        }
        else {
            this.overrideBilingual = false;
        }
        if (this.model.groupId && (this.isNOF || this.isReadOnly)) { // && type !== "cancel" && type !== "replace") {
            this.createGrouping(this.model.groupedProposals);
        }
        var originatorEmailField = this.nsdForm.get('originatorEmail');
        originatorEmailField.valueChanges
            .forEach(function (email) {
            if (!email) {
                originatorEmailField.clearValidators();
                if (originatorEmailField.errors) {
                    originatorEmailField.updateValueAndValidity();
                }
            }
            else {
                originatorEmailField.setValidators([forms_1.Validators.email, emailWidthDomainValidator]);
            }
        });
        var originatorPhoneField = this.nsdForm.get('originatorPhone');
        originatorPhoneField.valueChanges
            .forEach(function (phone) {
            if (!phone) {
                //originatorPhoneField.clearValidators();
                if (originatorPhoneField.errors) {
                    originatorPhoneField.updateValueAndValidity();
                }
            }
            else {
                //originatorPhoneField.setValidators([Validators.pattern('[0-9]+')]);
            }
        });
        this.initMap();
        this.mapHealthCheck();
    };
    NsdFormComponent.prototype.check48Hours = function () {
        if (this.isReadOnly) {
            return false;
        }
        else if (this.startActivityText) {
            return moment.duration(moment(this.startActivityText).diff(moment())).asHours() > 48;
        }
        return false;
    };
    NsdFormComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.buttonBars.forEach(function (child) {
            child.parent = _this;
        });
        this.schedulerDate.parent = this;
        this.schedulerDaily.parent = this;
        this.schedulerDayWeeks.parent = this;
        this.schedulerFreeText.parent = this;
        this.isSubmitButtonDisabled = !this.nsdForm.valid || !this.canSubmitFromStatus(this.model.status);
        if (this.type !== 'draft') {
            this.isSaveButtonDisabled = !this.nsdForm.valid || this.isReadOnly;
        }
        this.changeDetectionRef.detectChanges();
        if (!this.isReadOnly || this.isGroupingEnabled) {
            this.nsdForm.valueChanges
                .debounceTime(2000)
                .subscribe(function (data) {
                _this.generateIcao(data);
                if (_this.type !== 'draft')
                    _this.isNewProposal = false;
                else if (_this.model.proposalId !== null)
                    _this.isNewProposal = false;
                //else this.isNewProposal = true;
                if (_this.isGroupingEnabled) {
                    _this.updateCommonGroupItemsValues();
                }
            });
        }
        if (this.model.proposalType === data_model_1.ProposalType.Cancel) {
            window.setTimeout(function () { return _this.generateIcao(null); }, 2000);
        }
        //This is a hack on the select2 control that enable the current selection to be selected after searchig for it.
        $('body').on({
            mouseenter: function () {
                $($(this).context).attr('aria-selected', false);
            }
        }, '.select2-results__option.select2-results__option--highlighted');
        //Uncomment the html with modal-warning-date definition
        //let ctrl = this.nsdForm.get('grpDatePeriod');
        //if (ctrl.invalid && this.model.proposalId !== null) {
        //    $('#modal-warning-date').modal({});
        //}
    };
    Object.defineProperty(NsdFormComponent.prototype, "isBilingualRegion", {
        get: function () {
            return this.model && this.model.token && this.model.token.isBilingual;
        },
        enumerable: false,
        configurable: true
    });
    NsdFormComponent.prototype.activateTab = function (tab) {
        switch (tab) {
            case 'map':
                this.tabActive = 0;
                break;
            case 'icao-text':
                this.tabActive = 1;
                break;
            case 'icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 2;
                }
                break;
            case 'geoMap':
                this.tabActive = 3;
                break;
            case 'last-icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 4;
                }
                break;
            default:
                this.tabActive = 1;
        }
    };
    Object.defineProperty(NsdFormComponent.prototype, "mvpPeriodErrorMessage", {
        get: function () {
            var mvp = this.windowRef.appConfig.mvp || '30';
            var msg = this.translation.translate('ErrorMsgs.EndValidityFutureMins');
            return msg.replace("{mvp}", mvp);
        },
        enumerable: false,
        configurable: true
    });
    NsdFormComponent.prototype.backToProposals = function () {
        var _this = this;
        this.isSaveButtonDisabled = true;
        if (this.isGroupingEnabled && !this.model.proposalId) {
            this.model = this.groupModel[0];
        }
        if (this.model.proposalId) {
            this.dataService.restoreProposalStatus(this.model.proposalId, this.model.statusUpdated)
                .subscribe(function (response) {
                if (!response) {
                    _this.toastr.error(_this.translation.translate('ErrorMsgs.UnexpectedError'), "", { 'positionClass': 'toast-bottom-right' });
                }
                _this.disposeMap();
                _this.router.navigate(['/dashboard']);
            }, function (error) {
                _this.toastr.error(_this.translation.translate('ErrorMsgs.InternalServerError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                _this.disposeMap();
                _this.router.navigate(['/dashboard']);
            });
        }
        else {
            this.disposeMap();
            this.router.navigate(['/dashboard']);
        }
    };
    NsdFormComponent.prototype.confirmDiscardProposal = function (onError) {
        $('#modal-delete').modal({ onError: onError });
    };
    NsdFormComponent.prototype.discardProposal = function () {
        var _this = this;
        this.isSaveButtonDisabled = true;
        var onError = $('#modal-delete').data('bs.modal').options.onError;
        this.dataService.discardProposal(this.model.proposalId)
            .subscribe(function () {
            var msg = _this.translation.translate('Dashboard.MessageDiscardSuccess');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.disposeMap();
            _this.router.navigate(['/dashboard']);
        }, function (error) {
            _this.isSaveButtonDisabled = false;
            if (onError)
                onError(error);
            var msg = _this.translation.translate('Dashboard.MessageDiscardSuccess');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NsdFormComponent.prototype.backToNofProposals = function () {
        var _this = this;
        this.isSaveButtonDisabled = true;
        if (this.isGroupingEnabled && !this.model.proposalId) {
            this.model = this.groupModel[0];
        }
        if (this.model.proposalId) {
            this.dataService.restoreNofProposalStatus(this.model.proposalId, this.model.statusUpdated)
                .subscribe(function (response) {
                if (!response) {
                    _this.toastr.error(_this.translation.translate('ErrorMsgs.UnexpectedError'), "", { 'positionClass': 'toast-bottom-right' });
                }
                _this.disposeMap();
                _this.router.navigate(['/dashboard']);
            }, function (error) {
                _this.toastr.error(_this.translation.translate('ErrorMsgs.InternalServerError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                _this.disposeMap();
                _this.router.navigate(['/dashboard']);
            });
        }
        else {
            this.disposeMap();
            this.router.navigate(['/dashboard']);
        }
    };
    NsdFormComponent.prototype.showGrouping = function () {
        var copyModel = JSON.parse(JSON.stringify(this.model));
        copyModel.proposalId = 0;
        this.groupModel = [];
        this.model.isSelected = true;
        this.groupSelected = this.model;
        this.groupModel.push(this.model);
        this.groupModel.push(copyModel);
        this.isGroupingEnabled = true;
    };
    NsdFormComponent.prototype.createGrouping = function (groupedProposals) {
        var _this = this;
        this.groupModel = [];
        groupedProposals.forEach(function (p) {
            if (p.proposalId === _this.model.proposalId) {
                _this.model.isSelected = true;
                _this.groupSelected = _this.model;
                _this.groupModel.push(_this.model);
            }
            else {
                p.proposalId = p.proposalId || 0;
                p.isSelected = false;
                _this.groupModel.push(p);
            }
        });
        this.isGroupingEnabled = true;
    };
    NsdFormComponent.prototype.isGroupingActionButtonEnabled = function () {
        return this.nsdForm.valid && !this.isReadOnly && !this.waitingForGeneratingIcao;
    };
    NsdFormComponent.prototype.addToGroup = function () {
        this.groupModel.forEach(function (item) { item.isSelected = false; });
        var copyModel = JSON.parse(JSON.stringify(this.model));
        copyModel.proposalId = 0;
        this.groupModel.push(copyModel);
        this.onGroupItemSelected(this.groupModel.length - 1);
    };
    NsdFormComponent.prototype.onGroupItemSelected = function (index) {
        if (this.groupModel[index].isSelected) {
            return; //already selected
        }
        if (this.nsdForm.valid) {
            this.groupModel.forEach(function (item) { item.isSelected = false; });
            this.groupModel[index].isSelected = true;
            this.groupSelected = this.groupModel[index];
            this.waitingForGeneratingIcao = true;
            this.model = this.groupSelected;
            this.updateGroupItem();
        }
    };
    NsdFormComponent.prototype.removeFromGroup = function () {
        var index = this.groupModel.findIndex(function (x) { return x.isSelected === true; });
        if (index >= 2) {
            this.groupModel.splice(index, 1);
            if (this.groupModel.length > 0) {
                this.model = this.groupModel[0];
                this.model.isSelected = true;
                this.groupSelected = this.model;
                this.updateGroupItem();
            }
        }
    };
    NsdFormComponent.prototype.updateGroupItem = function () {
        this.updateFormControlValue("grpDatePeriod.immediate", this.model.token.immediate);
        this.updateFormControlValue("grpDatePeriod.permanent", this.model.token.permanent);
        this.updateFormControlValue("grpDatePeriod.estimated", this.model.token.estimated);
        this.updateFormControlValue("originator", this.model.token.originator);
        this.updateFormControlValue("originatorEmail", this.model.token.originatorEmail);
        this.updateFormControlValue("originatorPhone", this.model.token.originatorPhone);
        this.updateFormControlValue("itemD", this.model.token.itemD);
        this.updateFormControlValue("urgent", this.model.token.urgent);
        this.updateFormControlValue("amendPubMessage", this.model.token.amendPubMessage);
        this.updateFormControlValue("amendPubMessageFrench", this.model.token.amendPubMessageFrench);
        this.setStartActivityDate(this.model.token.startActivity, true);
        this.setEndValidityDate(this.model.token.endValidity, true);
        var type = this.componentData.inputs.type;
        this.componentData = {
            catId: this.model.categoryId,
            version: this.model.version,
            inputs: {
                form: this.nsdForm,
                type: type,
                isReadOnly: this.isReadOnly,
                token: this.model.token
            }
        };
        this.changeDetectionRef.detectChanges();
        this.nsdForm.markAsDirty();
    };
    NsdFormComponent.prototype.updateCommonGroupItemsValues = function () {
        var _this = this;
        this.groupModel.forEach(function (item) {
            if (!item.isSelected) {
                item.token.immediate = _this.model.token.immediate;
                item.token.permanent = _this.model.token.permanent;
                item.token.originator = _this.model.token.originator;
                item.token.originatorPhone = _this.model.token.originatorPhone;
                item.token.originatorEmail = _this.model.token.originatorEmail;
                item.token.itemD = _this.model.token.itemD;
                item.token.annotations = _this.model.token.annotations;
                item.token.estimated = _this.model.token.estimated;
                item.token.startActivity = _this.model.token.startActivity;
                item.token.endValidity = _this.model.token.endValidity;
                item.token.urgent = _this.model.token.urgent;
                item.token.amendPubMessage = _this.model.token.amendPubMessage;
                item.token.amendPubMessageFrench = _this.model.token.amendPubMessageFrench;
            }
        });
    };
    NsdFormComponent.prototype.clearSchedule = function (check, callbackFunc, deleteScheduleEventsEstimated) {
        if (deleteScheduleEventsEstimated === void 0) { deleteScheduleEventsEstimated = false; }
        if (check !== null) {
            this.isScheduleButtonActionVisible = !check;
        }
        if (this.hasScheduler) {
            var el = $('#modal-remove-schedule');
            var $modal = el.modal();
            el.data('funcAttr', callbackFunc);
            var self_1 = this;
            $modal.modal("show");
            if (!this.isRemoveScheduleAlertShown) {
                el.on('shown.bs.modal', function (e) {
                }).on('hide.bs.modal', function (e) {
                    var callback = $("#modal-remove-schedule").data("funcAttr");
                    var pressedButton = $(document.activeElement).attr('id');
                    if (pressedButton === "btn-remove") {
                        if (check) {
                            $('#schedule-panel').hide();
                        }
                        self_1.hasScheduler = check !== null ? false : true;
                        self_1.nsdForm.get('grpDatePeriod.startActivity').enable();
                        self_1.nsdForm.get('grpDatePeriod.endValidity').enable();
                        switch (self_1.schedulerTab) {
                            case 1:
                                self_1.schedulerDaily.deleteEvents(null, deleteScheduleEventsEstimated);
                                self_1.schedulerDaily.toggleShedule(null, deleteScheduleEventsEstimated);
                                self_1.hasScheduler = false;
                                break;
                            case 2:
                                self_1.schedulerDayWeeks.deleteEvents(null, deleteScheduleEventsEstimated);
                                self_1.schedulerDayWeeks.toggleShedule(null, deleteScheduleEventsEstimated);
                                self_1.hasScheduler = false;
                                break;
                            case 3:
                                self_1.schedulerDate.deleteEvents(null, deleteScheduleEventsEstimated);
                                self_1.schedulerDate.toggleShedule(null, deleteScheduleEventsEstimated);
                                self_1.hasScheduler = false;
                                break;
                            case 4:
                                if (self_1.schedulerDaily.dateEvents.length > 0) {
                                    self_1.schedulerDaily.deleteEvents(null, true);
                                    self_1.schedulerDaily.toggleShedule(null, true);
                                    self_1.schedulerDaily.slideScheduleClose();
                                    self_1.hasScheduler = false;
                                }
                                else if (self_1.schedulerDayWeeks.hasEvents()) {
                                    self_1.schedulerDayWeeks.deleteEvents(null, true);
                                    self_1.schedulerDayWeeks.toggleShedule(null, true);
                                    self_1.schedulerDayWeeks.slideScheduleClose();
                                    self_1.hasScheduler = false;
                                }
                                else if (self_1.schedulerDate.hasEvents()) {
                                    self_1.schedulerDate.deleteEvents(null, true);
                                    self_1.schedulerDate.toggleShedule(null, true);
                                    self_1.schedulerDate.slideScheduleClose();
                                    self_1.hasScheduler = false;
                                }
                                self_1.schedulerFreeText.clearFormControls();
                                self_1.updateFormControlValue('itemD', "");
                                self_1.resetSchedulersStatus();
                                break;
                        }
                        callback(true);
                    }
                    else {
                        if (callback) {
                            callback(false);
                        }
                    }
                });
            }
            this.isRemoveScheduleAlertShown = true;
        }
        else {
            if (check) {
                if (this.schedulerTab == 4) {
                    this.schedulerFreeText.clearFormControls();
                    this.updateFormControlValue('itemD', "");
                }
                $('#schedule-panel').hide();
            }
            if (callbackFunc) {
                callbackFunc(true);
            }
        }
    };
    NsdFormComponent.prototype.resetSchedulersStatus = function () {
        this.hasScheduler = false;
        this.schedulerDaily.resetClearScheduleFlag();
        this.schedulerDayWeeks.resetClearScheduleFlag();
        this.schedulerDate.resetClearScheduleFlag();
    };
    NsdFormComponent.prototype.isStartActivityCalendarActive = function () {
        if (this.nsdForm.get('grpDatePeriod.startActivity').disabled || this.model.itemD) {
            return false;
        }
        return this.model.proposalType !== data_model_1.ProposalType.Cancel && this.nsdForm.get('grpDatePeriod.immediate').value !== true;
    };
    NsdFormComponent.prototype.isEndValidityCalendarActive = function () {
        if (this.nsdForm.get('grpDatePeriod.endValidity').disabled || this.model.itemD) {
            return false;
        }
        return this.model.proposalType !== data_model_1.ProposalType.Cancel && this.nsdForm.get('grpDatePeriod.permanent').value !== true;
    };
    NsdFormComponent.prototype.setStartActivityDate = function (date, leaveEmpty) {
        var fc = this.nsdForm.get('grpDatePeriod.startActivity');
        if (date) {
            var momentDate = moment.utc(date);
            this.startActivityMoment = momentDate.format(this.dateFormat);
            if (fc.value !== this.startActivityMoment) {
                fc.setValue(this.startActivityMoment);
                fc.markAsDirty();
                this.startActivityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
            }
        }
        else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
            }
            else {
                this.setStartActivityDate(moment.utc());
            }
        }
        if (this.schedulerFreeText) {
            this.clearModelItemD();
        }
    };
    NsdFormComponent.prototype.onStartActivityDateInputChange = function (strDate) {
        if (strDate && strDate.length === 10) {
            var momentDate = moment.utc(strDate, "YYMMDDHHmm");
            if (momentDate.isValid()) {
                this.startActivityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var fc = this.nsdForm.get('grpDatePeriod.startActivity');
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    this.startActivityMoment = formatedDate;
                }
            }
            else {
                this.startActivityText = "";
            }
        }
        else {
            this.startActivityText = "";
        }
    };
    NsdFormComponent.prototype.setEndValidityDate = function (date, leaveEmpty) {
        var fc = this.nsdForm.get('grpDatePeriod.endValidity');
        if (date) {
            var momentDate = moment.utc(date);
            this.endValidityMoment = momentDate.format(this.dateFormat);
            if (fc.value !== this.endValidityMoment) {
                fc.setValue(this.endValidityMoment);
                fc.markAsDirty();
                this.endValidityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
            }
        }
        else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
            }
            else {
                this.setEndValidityDate(moment.utc());
            }
        }
        if (this.schedulerFreeText) {
            this.clearModelItemD();
        }
    };
    NsdFormComponent.prototype.onEndValidityInputChange = function (strDate) {
        if (strDate && strDate.length === 10) {
            var momentDate = moment.utc(strDate, "YYMMDDHHmm");
            if (momentDate.isValid()) {
                this.endValidityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var fc = this.nsdForm.get('grpDatePeriod.endValidity');
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    this.endValidityMoment = formatedDate;
                }
            }
            else {
                this.endValidityText = "";
            }
        }
        else {
            this.endValidityText = "";
        }
    };
    NsdFormComponent.prototype.immediateChanged = function (checked) {
        if (checked) {
            var self_2 = this;
            this.clearSchedule(checked, function (actionResult) {
                if (actionResult) {
                    var fc = self_2.nsdForm.get('grpDatePeriod.startActivity');
                    fc.setValue(null);
                    fc.markAsDirty();
                    fc.disable();
                    self_2.model.token.startActivity = null;
                    self_2.startActivityText = "";
                    self_2.setIcaoInfoTab.apply(self_2);
                }
                else {
                    var switchCtrl = self_2.nsdForm.get('grpDatePeriod.immediate');
                    switchCtrl.setValue(!checked);
                }
            });
        }
        else {
            if (this.checkScheduleButtonVisiblity()) {
                this.isScheduleButtonActionVisible = true;
                this.schedulerTab = 4;
                var endValidity = this.endValidityText;
                this.schedulerFreeText.clear();
                this.nsdForm.get('grpDatePeriod.endValidity').setValue(moment(moment(endValidity, 'YYYY/MM/DD, HH:mm')).format(this.dateFormat));
            }
            this.nsdForm.get("grpDatePeriod.startActivity").enable();
        }
    };
    NsdFormComponent.prototype.permanentChanged = function (checked) {
        var estimatedCtrl = this.nsdForm.get('grpDatePeriod.estimated');
        if (checked) {
            var self_3 = this;
            this.clearSchedule(checked, function (actionResult) {
                if (actionResult) {
                    var endValCtrl = self_3.nsdForm.get('grpDatePeriod.endValidity');
                    endValCtrl.setValue(null);
                    endValCtrl.markAsDirty();
                    endValCtrl.disable();
                    estimatedCtrl.disable();
                    if (estimatedCtrl.value) {
                        estimatedCtrl.setValue(false);
                        estimatedCtrl.markAsDirty();
                    }
                    self_3.endValidityText = "";
                    self_3.model.token.endValidity = null;
                    self_3.setIcaoInfoTab.apply(self_3);
                }
                else {
                    var permanentCtrl = self_3.nsdForm.get('grpDatePeriod.permanent');
                    permanentCtrl.setValue(!checked);
                }
            });
        }
        else {
            if (this.checkScheduleButtonVisiblity()) {
                this.isScheduleButtonActionVisible = true;
                this.schedulerTab = 4;
                var startActivity = this.startActivityText;
                this.schedulerFreeText.clear();
                this.nsdForm.get('grpDatePeriod.startActivity').setValue(moment(moment(startActivity, 'YYYY/MM/DD HH:mm')).format(this.dateFormat));
            }
            this.nsdForm.get("grpDatePeriod.endValidity").enable();
            estimatedCtrl.enable();
        }
    };
    NsdFormComponent.prototype.setIcaoInfoTab = function (enforce) {
        if (enforce === void 0) { enforce = false; }
        if (this.tabActive == 2 || enforce) {
            this.tabActive = 1;
        }
    };
    Object.defineProperty(NsdFormComponent.prototype, "isPermanent", {
        get: function () {
            var ctrl = this.nsdForm.get('grpDatePeriod.permanent');
            return ctrl && ctrl.value;
        },
        enumerable: false,
        configurable: true
    });
    NsdFormComponent.prototype.estimatedChanged = function (checked) {
        var permanentCtrl = this.nsdForm.get('grpDatePeriod.permanent');
        if (checked) {
            var self_4 = this;
            this.clearSchedule(checked, function (actionResult) {
                if (actionResult) {
                    permanentCtrl.disable();
                    permanentCtrl.setValue(false);
                    permanentCtrl.markAsDirty();
                    self_4.setIcaoInfoTab.apply(self_4);
                }
                else {
                    var estimatedCtrl = self_4.nsdForm.get('grpDatePeriod.estimated');
                    estimatedCtrl.setValue(!checked);
                }
            }, checked); //delete scheduler events because proposal is estimated
        }
        else {
            if (this.checkScheduleButtonVisiblity()) {
                this.isScheduleButtonActionVisible = true;
                this.schedulerTab = 4;
                var endValidity = this.endValidityText;
                var startActivity = this.startActivityText;
                this.schedulerFreeText.clear();
                this.nsdForm.get('grpDatePeriod.startActivity').setValue(moment(moment(startActivity, 'YYYY/MM/DD HH:mm')).format(this.dateFormat));
                this.nsdForm.get('grpDatePeriod.endValidity').setValue(moment(moment(endValidity, 'YYYY/MM/DD, HH:mm')).format(this.dateFormat));
            }
            permanentCtrl.enable();
        }
    };
    NsdFormComponent.prototype.validateOriginator = function () {
        var originatorCtrl = this.nsdForm.get("originator");
        if (this.model.proposalId) {
            return originatorCtrl.valid;
        }
        else {
            return originatorCtrl.valid || this.nsdForm.pristine;
        }
    };
    NsdFormComponent.prototype.updateOriginatorContactInfo = function () {
        var _this = this;
        var originatorCtrl = this.nsdForm.get("originator");
        var originatorEmailCtrl = this.nsdForm.get("originatorEmail");
        var originatorPhoneCtrl = this.nsdForm.get("originatorPhone");
        this.dataService.updateOriginatorContactInfo(this.model.proposalId, originatorCtrl.value, originatorEmailCtrl.value, originatorPhoneCtrl.value)
            .subscribe(function () {
            _this.model.originator = originatorCtrl.value;
            _this.model.originatorEmail = originatorEmailCtrl.value;
            _this.model.originatorPhone = originatorPhoneCtrl.value;
            _this.model.token.originator = originatorCtrl.value;
            _this.model.token.originatorEmail = originatorEmailCtrl.value;
            _this.model.token.originatorPhone = originatorPhoneCtrl.value;
            _this.setOriginatorInfoToPristime();
            _this.toastr.success(_this.translation.translate('Nsd.SaveOriginatorSuccess'), "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            _this.toastr.error(_this.translation.translate('ErrorMsgs.AddOriginatorError'), "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NsdFormComponent.prototype.validateStartActivity = function (errorName) {
        var hasErrors = this.nsdForm.get('grpDatePeriod').errors;
        if (hasErrors) {
            if (errorName) {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors[errorName];
            }
            else {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors["startActivityRequired"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateOnlyInFuture"] ||
                    this.nsdForm.get('grpDatePeriod').errors["invalidStartActivityTenDigitDate"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateOneDayInAdvance"];
            }
        }
        if (this.isNewProposal) {
            return !hasErrors || this.nsdForm.pristine;
        }
        else {
            return !hasErrors;
        }
    };
    NsdFormComponent.prototype.isStartApproaching = function () {
        if (this.isReadOnly)
            return false;
        var hasErrors = this.nsdForm.get('grpDatePeriod').errors;
        if (!hasErrors) {
            var immediateCtrl = this.nsdForm.get("grpDatePeriod.immediate");
            if (immediateCtrl) {
                if (!immediateCtrl.value) {
                    var startActivityCtrl = this.nsdForm.get("grpDatePeriod.startActivity");
                    var startDate = startActivityCtrl.value ? moment.utc(startActivityCtrl.value, 'YYMMDDHHmm') : null;
                    if (startDate && startDate.isValid()) {
                        var now = moment.utc(moment().utc().format("MM/DD/YYYY, H:mm"));
                        var minutes = moment.utc(startDate, "MM/DD/YYYY, H:mm").diff(now, 'minutes');
                        if (minutes < 30) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    };
    NsdFormComponent.prototype.validateEndValidity = function (errorName) {
        var hasErrors = this.nsdForm.get('grpDatePeriod').errors;
        if (hasErrors) {
            if (errorName) {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors[errorName];
            }
            else {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors["endValidityRequired"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateLaterThan30MinStartingDate"] ||
                    this.nsdForm.get('grpDatePeriod').errors["invalidEndValidityTenDigitDate"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateLaterThan92Days"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateLaterThanStartingDate"];
            }
        }
        if (this.isNewProposal) {
            return !hasErrors || this.nsdForm.pristine;
        }
        else {
            return !hasErrors;
        }
    };
    NsdFormComponent.prototype.ignoreUserInput = function (event) {
        event.preventDefault();
    };
    NsdFormComponent.prototype.saveDraft = function () {
        var _this = this;
        var payload = this.setPayload(false);
        function success(data) {
            this.isSaveButtonDisabled = true;
            this.txtBackToReviewList = this.backToReviewListLabel;
            this.updateModel(data);
            //this.updateReviewListFilter(data.proposalId);
            //This need to be done here, not on the updateModel function, because this is only happening when the proposal is saved
            //and the updateModel happen everytime that Icao is generated
            if (this.model.token.annotations) {
                //Reset the annotations control
                for (var i = this.annotations.length - 1; i >= 0; i--) {
                    this.annotations.removeAt(i);
                }
                this.selectedProposalAnnotationId = -1;
                //Rebuild the annotations control
                for (var i = 0; i < this.model.token.annotations.length; i++) {
                    this.annotations.push(this.buildAnnotation(this.model.token.annotations[i]));
                }
            }
            this.toastr.success(this.translation.translate('Dashboard.MessageSaveSuccess'), "", { 'positionClass': 'toast-bottom-right' });
        }
        function failure(error) {
            this.toastr.error(this.translation.translate('Dashboard.MessageSaveFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        }
        if (this.model.groupId) {
            this.dataService.saveDraftGroupProposal(payload)
                .subscribe(function (data) {
                success.call(_this, data);
            }, function (error) {
                failure.call(_this, error);
            });
        }
        else {
            this.dataService.saveDraftProposal(payload)
                .subscribe(function (data) {
                _this.saveScheduler(data);
                success.call(_this, data);
            }, function (error) {
                failure.call(_this, error);
            });
        }
    };
    NsdFormComponent.prototype.submitProposal = function (onError) {
        var _this = this;
        this.isSaveButtonDisabled = true;
        this.isWaitingForResponse = true;
        var payload = this.setPayload(false);
        function success(data) {
            this.router.navigate(['/dashboard']);
        }
        function failure(error) {
            this.isSaveButtonDisabled = false;
            this.isWaitingForResponse = false;
            if (onError)
                onError.call(error);
            this.toastr.error(this.translation.translate('Dashboard.MessageSubmitFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        }
        if (this.model.groupId) {
            this.dataService.submitGroupedProposal(payload)
                .subscribe(function (data) {
                success.call(_this, data);
            }, function (error) {
                failure.call(_this, error);
            });
        }
        else {
            this.dataService.submitProposal(payload)
                .subscribe(function (data) {
                _this.saveScheduler(data);
                success.call(_this, data);
            }, function (error) {
                failure.call(_this, error);
            });
        }
    };
    NsdFormComponent.prototype.parkProposal = function (onError) {
        var _this = this;
        this.isWaitingForResponse = true;
        this.isSaveButtonDisabled = true;
        if (this.isGroupingEnabled) {
            var payload = this.setGroupPayload(false);
            return this.dataService.parkGroupedProposal(payload)
                .subscribe(function (data) {
                _this.nsdForm.markAsPristine();
                _this.router.navigate(['/dashboard']);
            }, function (error) {
                _this.isWaitingForResponse = false;
                if (onError)
                    onError.call(error);
                _this.toastr.error(_this.translation.translate('Dashboard.MessageParkFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            var payload = this.setPayload(false);
            return this.dataService.parkProposal(payload)
                .subscribe(function (data) {
                _this.isSaveButtonDisabled = true;
                _this.updateModel(data);
                _this.saveScheduler(data);
                _this.nsdForm.markAsPristine();
                _this.router.navigate(['/dashboard']);
            }, function (error) {
                _this.isWaitingForResponse = false;
                _this.isSaveButtonDisabled = false;
                if (onError)
                    onError.call(error);
                _this.toastr.error(_this.translation.translate('Dashboard.MessageParkFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NsdFormComponent.prototype.openRejectProposalDialog = function () {
        var el = $('#modal-reject-reason');
        var $modal = el.modal();
        $modal.modal("show");
    };
    NsdFormComponent.prototype.rejectProposal = function () {
        var _this = this;
        this.isWaitingForResponse = true;
        this.isSaveButtonDisabled = true;
        var payload = this.setPayload(false);
        this.model.token.rejectedReason =
            this.dataService.rejectProposal(payload)
                .subscribe(function (data) {
                _this.model.token.rejectedReason = null;
                _this.nsdForm.markAsPristine();
                _this.router.navigate(['/dashboard']);
            }, function (error) {
                _this.isWaitingForResponse = false;
                _this.isSaveButtonDisabled = true;
                _this.toastr.error(_this.translation.translate('Dashboard.MessageRejectFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                _this.model.token.rejectedReason = null;
            });
    };
    NsdFormComponent.prototype.disseminateProposal = function (onError) {
        var _this = this;
        this.isWaitingForResponse = true;
        if (this.isGroupingEnabled) {
            var payload = this.setGroupPayload(false);
            this.dataService.disseminateGroupedProposal(payload)
                .subscribe(function (data) {
                //TODO implement scheduler grouping this.saveScheduler(data);
                _this.isSaveButtonDisabled = true;
                _this.nsdForm.markAsPristine();
                _this.router.navigate(['/dashboard']);
            }, function (error) {
                _this.isWaitingForResponse = false;
                if (onError)
                    onError.call(error);
                _this.toastr.error(_this.translation.translate('Dashboard.MessageDisseminateFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            var payload = this.setPayload(false);
            this.dataService.disseminateProposal(payload)
                .subscribe(function (data) {
                _this.saveScheduler(data);
                _this.isSaveButtonDisabled = true;
                _this.nsdForm.markAsPristine();
                _this.router.navigate(['/dashboard']);
            }, function (error) {
                _this.isWaitingForResponse = false;
                if (onError)
                    onError.call(error);
                _this.toastr.error(_this.translation.translate('Dashboard.MessageDisseminateFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NsdFormComponent.prototype.saveProposalAttchments = function () {
        var _this = this;
        var winObj = this.windowRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            var fileUpload = $("#attachments").get(0);
            var files = fileUpload.files;
            // Create FormData object  
            var fileData = new FormData();
            // Looping over all files and add it to FormData object  
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }
            fileData.append("proposalId", this.model.proposalId);
            fileData.append("userId", this.windowRef.appConfig.usrId);
            this.dataService.saveProposalAttachments(fileData)
                .subscribe(function (data) {
                _this.txtBackToReviewList = _this.backToReviewListLabel;
                _this.model.attachments = _this.model.attachments || [];
                for (var iter = 0; iter < data.length; iter++) {
                    _this.model.attachments.push({
                        id: data[iter].id,
                        userName: data[iter].userName,
                        fileName: data[iter].fileName,
                        readOnly: false
                    });
                }
                //const message = data.length > 1 
                //    ? `All ${data.length} attachments where already uploaded to the server.`
                //    : "The attachment was already uploaded to the server.";
                var message = data.length > 1
                    ? _this.translation.translate('Dashboard.MessageSaveAttachment1') + data.length + _this.translation.translate('Dashboard.MessageSaveAttachment2')
                    : _this.translation.translate('Dashboard.MessageSaveAttachment3');
                _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                $("#attachments").val('');
            }, function (error) {
                _this.toastr.error(_this.translation.translate('Dashboard.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            this.toastr.error(this.translation.translate('Dashboard.MessageSaveAttachmentBrowser1'), this.translation.translate('Dashboard.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    NsdFormComponent.prototype.downloadAttachment = function (id) {
        try {
            var url = window.location.protocol + "//" + window.location.host + "/api/proposals/attachments/" + id;
            var a = document.createElement('a');
            a.style.display = 'none';
            a.href = url;
            a.download = 'attachment';
            document.body.appendChild(a);
            a.click();
            window.URL.revokeObjectURL(url);
        }
        catch (e) {
            //td..
        }
    };
    NsdFormComponent.prototype.deleteProposalAttchment = function (attachment) {
        var _this = this;
        this.dataService.deleteProposalAttachment(attachment)
            .subscribe(function (data) {
            var index = _this.model.attachments.findIndex(function (x) { return x.id === attachment.id; });
            if (index >= 0) {
                _this.model.attachments.splice(index, 1);
            }
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Dashboard.MessageDeleteAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NsdFormComponent.prototype.validateProposalAnnotation = function (index) {
        var control = this.annotations.at(index);
        return control.valid || this.nsdForm.pristine;
    };
    NsdFormComponent.prototype.addProposalAnnotation = function () {
        if (this.annotations.length <= 0) {
            this.annotations.push(this.buildAnnotation({}));
        }
        else {
            var length_1 = this.annotations.length;
            if (this.annotations.value[length_1 - 1].note) {
                this.annotations.push(this.buildAnnotation({}));
            }
        }
        this.selectedProposalAnnotationId = this.annotations.length - 1;
    };
    NsdFormComponent.prototype.selectProposalAnnotation = function (index) {
        for (var i = 0; i < this.annotations.length; i++) {
            var control = this.annotations.at(i);
            if (i == index) {
                this.selectedProposalAnnotationId = control.value.id;
            }
            control.patchValue({ selected: i === index });
            control.markAsDirty();
        }
    };
    NsdFormComponent.prototype.isDateTimePeriodDisable = function () {
        return this.isReadOnly || this.model.proposalType === data_model_1.ProposalType.Cancel || this.model.itemD;
    };
    Object.defineProperty(NsdFormComponent.prototype, "annotations", {
        get: function () {
            return this.nsdForm.get('annotations');
        },
        enumerable: false,
        configurable: true
    });
    ;
    NsdFormComponent.prototype.deleteProposalAnnotation = function () {
        var index = this.annotations.value.findIndex(function (x) { return x.selected === true; });
        if (index >= 0) {
            this.annotations.removeAt(index);
            this.selectedProposalAnnotationId = this.annotations.length > 0 ? this.annotations.length - 1 : -1;
        }
    };
    NsdFormComponent.prototype.isCurrentAnnotationReadonly = function (annotation) {
        if (this.model.token.annotations && this.model.token.annotations.length > 0) {
            var currentAnnotationId_1 = annotation.controls.id.value;
            var pos = this.model.token.annotations.findIndex(function (x) { return x.id == currentAnnotationId_1; });
            return pos >= 0;
        }
        return false;
    };
    NsdFormComponent.prototype.isSaveAnnotationButtonDisabled = function () {
        if (!this.isReadOnly) {
            return true;
        }
        if (this.annotations.length <= 0) {
            return true;
        }
        else {
            if (this.annotations.value) {
                for (var i = 0; i < this.annotations.value.length; i++) {
                    if (!this.annotations.value[i].note) {
                        return true;
                    }
                }
            }
            if (this.model.token.annotations.length > 0) {
                var lastPositon = this.annotations.length - 1;
                if ((this.model.token.annotations.length - 1 >= lastPositon) && !this.model.token.annotations[lastPositon].note) {
                    return true;
                }
                else {
                    return !this.annotations.dirty || this.annotations.value.length === this.model.token.annotations.length;
                }
            }
        }
        return !this.annotations.dirty;
    };
    NsdFormComponent.prototype.saveProposalAnnotations = function () {
        var _this = this;
        if (this.annotations.length > 0 && this.model.proposalId >= 1) {
            this.dataService.saveProposalAnnotations(this.model.proposalId, this.annotations.value)
                .subscribe(function (data) {
                _this.annotations.markAsPristine();
                _this.toastr.success(_this.translation.translate('Nsd.SaveAnnotationSuccess'), "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                _this.toastr.error(_this.translation.translate('ErrorMsgs.SaveAnnotationError'), "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NsdFormComponent.prototype.isRejectProposalDisable = function () {
        return !this.model.proposalId || this.isWaitingForResponse; // || !this.nsdForm.valid; // || this.model.proposalType === ProposalType.Cancel || this.model.proposalType === ProposalType.Replace; 
    };
    NsdFormComponent.prototype.isParkProposalDisabled = function () {
        return !this.nsdForm.valid || this.isWaitingForResponse; // || this.model.proposalType !== ProposalType.New;
    };
    NsdFormComponent.prototype.isGroupingProposalDisable = function () {
        return !this.nsdForm.valid || this.isGroupingEnabled;
    };
    NsdFormComponent.prototype.toggleScheduler = function (event) {
        event.preventDefault();
    };
    NsdFormComponent.prototype.selectSchedulerTab = function (tab, e) {
        switch (tab) {
            case 1:
                if (this.schedulerDate.hasEvents() || this.schedulerDayWeeks.hasEvents()) {
                    e.preventDefault();
                    e.stopPropagation();
                    return 0;
                }
                this.schedulerDaily.closePanelIfOpen();
                break;
            case 2:
                if (this.schedulerDate.hasEvents() || this.schedulerDaily.hasEvents()) {
                    e.preventDefault();
                    e.stopPropagation();
                    return 0;
                }
                this.schedulerDayWeeks.closePanelIfOpen();
                break;
            case 3:
                if (this.schedulerDayWeeks.hasEvents() || this.schedulerDaily.hasEvents()) {
                    e.preventDefault();
                    e.stopPropagation();
                    return 0;
                }
                this.schedulerDate.closePanelIfOpen();
                break;
            case 4:
                if (this.hasScheduler) {
                    var start = null;
                    var end = null;
                    var starttime = null;
                    var endtime = null;
                    switch (this.schedulerTab) {
                        case 1:
                            start = this.schedulerDaily.start;
                            end = this.schedulerDaily.end;
                            starttime = this.schedulerDaily.starttime;
                            endtime = this.schedulerDaily.endtime;
                            break;
                        case 2:
                            start = this.schedulerDayWeeks.start;
                            end = this.schedulerDayWeeks.end;
                            starttime = this.schedulerDayWeeks.starttime;
                            endtime = this.schedulerDayWeeks.endtime;
                            break;
                        case 3:
                            //start = this.schedulerDate.start;
                            //end = this.schedulerDate.end;
                            start = moment.utc(this.nsdForm.get('grpDatePeriod.startActivity').value, 'YYMMDDHHmm');
                            end = moment.utc(this.nsdForm.get('grpDatePeriod.endValidity').value, 'YYMMDDHHmm');
                            starttime = this.schedulerDate.starttime;
                            endtime = this.schedulerDate.endtime;
                            break;
                    }
                    if (start && end && starttime && endtime) {
                        this.schedulerFreeText.setDate(start, end, starttime, endtime);
                    }
                }
                break;
        }
        if (tab !== this.schedulerTab) {
            this.schedulerTab = tab;
        }
    };
    NsdFormComponent.prototype.disableActivePeridDates = function (start, end, itemD) {
        this.hasScheduler = true;
        this.nsdForm.get('grpDatePeriod.startActivity').disable();
        this.nsdForm.get('grpDatePeriod.endValidity').disable();
        this.nsdForm.get('grpDatePeriod.startActivity').setValue(moment(moment(start, 'DD/MM/YYYY HH:mm')).format(this.dateFormat));
        this.nsdForm.get('grpDatePeriod.endValidity').setValue(moment(moment(end, 'DD/MM/YYYY HH:mm')).format(this.dateFormat));
        this.updateFormControlValue('itemD', itemD);
        this.schedulerFreeText.setFreeText(itemD);
        this.nsdForm.markAsDirty();
    };
    NsdFormComponent.prototype.enableActivePeridDates = function () {
        this.isScheduleOverridden = false;
        this.hasScheduler = false;
        var ctrlStartActivity = this.nsdForm.get('grpDatePeriod.startActivity');
        var ctrlEndValidity = this.nsdForm.get('grpDatePeriod.endValidity');
        ctrlStartActivity.setValue(null);
        ctrlEndValidity.setValue(null);
        ctrlStartActivity.enable();
        ctrlEndValidity.enable();
        this.clearModelItemD();
        this.schedulerFreeText.clearFormControls();
    };
    NsdFormComponent.prototype.select2Options = function () {
        var self = this;
        var app = this.windowRef.nativeWindow['app'];
        return {
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            overflow: scroll,
            minimumInputLength: 1,
            maximumInputLength: 148,
            width: "100%",
            tags: true,
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                };
            },
            placeholder: {
                id: '-1',
                text: 'FullName' //this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            //allowClear: true,
            ajax: {
                type: 'GET',
                url: app.apiUrl + "originatorinfo/filter",
                dataType: 'json',
                delay: 500,
                cache: false,
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        OrgId: app.orgId
                    };
                },
                processResults: function (data) {
                    var _loop_1 = function (iter) {
                        var originator = self.originators.find(function (e) { return e.text === data[iter].text; });
                        if (originator) {
                            originator.phone = data[iter].phone;
                            originator.email = data[iter].email;
                            self.seletedOriginatorId = "-1";
                        }
                        ;
                    };
                    for (var iter = 0; iter < data.length; iter++) {
                        _loop_1(iter);
                    }
                    return {
                        results: data
                    };
                },
            },
            templateResult: function (state) {
                self.clearOriginatorPanel();
                var fullName = state.text ? state.text : '';
                var email = state.email ? (' | ' + state.email) : '';
                var phone = state.phone ? (' | ' + state.phone) : '';
                var $state = $('<span>' + fullName + email + phone + '</span>');
                return $state;
            },
            templateSelection: function (selection, container) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                var modifiedOriginatorIdx = self.originators.findIndex(function (x) { return x.text == selection.text; });
                if (modifiedOriginatorIdx >= 0) {
                    selection.phone = self.originators[modifiedOriginatorIdx].phone;
                    selection.email = self.originators[modifiedOriginatorIdx].email;
                }
                var email;
                var phone;
                if (selection.newOption) {
                    var emailControl = self.nsdForm.get('originatorEmail');
                    var phoneControl = self.nsdForm.get('originatorPhone');
                    if (emailControl.value) {
                        email = emailControl.value instanceof Object ? emailControl.value.value : emailControl.value;
                    }
                    if (phoneControl.value) {
                        phone = phoneControl.value instanceof Object ? phoneControl.value.value : phoneControl.value;
                    }
                    self.updateOriginatorPanel("-1", selection.id, email ? email.trim() : email, phone);
                    return selection.text;
                }
                else {
                    email = selection.email === undefined ? (self.model.originatorEmail ? self.model.originatorEmail : null) : (selection.email === null ? null : selection.email);
                    phone = selection.phone === undefined ? (self.model.originatorPhone ? self.model.originatorPhone : null) : (selection.phone === null ? null : selection.phone);
                    self.updateOriginatorPanel(selection.id, selection.text, email ? email.trim() : email, phone);
                    return selection.text;
                }
            },
        };
    };
    NsdFormComponent.prototype.isSaveDraftButtonDisabled = function () {
        //if (!this.isScheduleValid) {
        //    return true; 
        //}
        return this.isWaitingForResponse || this.isSaveButtonDisabled;
    };
    NsdFormComponent.prototype.isSubmitProposalButtonDisabled = function () {
        //if (!this.isScheduleValid) {
        //    return true;
        //}
        return this.isWaitingForResponse || this.isSubmitButtonDisabled;
    };
    NsdFormComponent.prototype.isAnyFullCalendarActive = function () {
        if (this.schedulerDaily.dateEvents.length > 0) {
            return true;
        }
        if (this.schedulerDayWeeks.hasEvents()) {
            return true;
        }
        if (this.schedulerDate.hasEvents()) {
            return true;
        }
        return false;
    };
    NsdFormComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdForm.get(controlName);
        if (control && control.value !== value) {
            if (controlName === "itemD") {
                this.model.itemD = value;
                this.model.token.itemD = value;
            }
            control.setValue(value);
        }
    };
    NsdFormComponent.prototype.isAddOriginatorButtonDisabled = function () {
        var originatorCtrl = this.nsdForm.get("originator");
        var originatorEmailCtrl = this.nsdForm.get("originatorEmail");
        var originatorPhoneCtrl = this.nsdForm.get("originatorPhone");
        return !(originatorCtrl.valid && originatorEmailCtrl.valid && originatorPhoneCtrl.valid);
    };
    NsdFormComponent.prototype.isSaveContactInfoButtonDisabled = function () {
        if (!this.isReadOnly) {
            return true;
        }
        var originatorCtrl = this.nsdForm.get("originator");
        var originatorEmailCtrl = this.nsdForm.get("originatorEmail");
        var originatorPhoneCtrl = this.nsdForm.get("originatorPhone");
        var disabled = !(originatorCtrl.valid && originatorEmailCtrl.valid && originatorPhoneCtrl.valid);
        if (!disabled) {
            var savedOriginator = this.model.token.originator;
            if (savedOriginator) {
                if (savedOriginator !== originatorCtrl.value && !originatorEmailCtrl.value && !originatorPhoneCtrl.value) {
                    return false;
                }
            }
            disabled = originatorCtrl.pristine && originatorEmailCtrl.pristine && originatorPhoneCtrl.pristine;
        }
        return disabled;
        //return this.nsdForm.pristine || !(originatorCtrl.valid && originatorEmailCtrl.valid && originatorPhoneCtrl.valid);
    };
    NsdFormComponent.prototype.isRemoveOriginatorButtonDisabled = function () {
        var disabled = this.isReadOnly || this.seletedOriginatorId === "60000000" || this.seletedOriginatorId === "-1";
        if (disabled) {
            var control = this.nsdForm.get("originator");
            var content_1 = typeof (control.value) === "object" ? (control.value ? control.value.value : control.value) : control.value;
            if (content_1) {
                var originatorIdx = this.originators.findIndex(function (x) { return x.text === content_1; });
                disabled = originatorIdx < 0;
            }
        }
        return disabled;
    };
    NsdFormComponent.prototype.addOriginator = function () {
        var _this = this;
        var app = this.windowRef.nativeWindow['app'];
        var data = {
            organizationId: app.orgId,
            text: this.nsdForm.get('originator').value,
            email: this.nsdForm.get('originatorEmail').value,
            phone: this.nsdForm.get('originatorPhone').value
        };
        var modifiedOriginatorIdx = this.originators.findIndex(function (x) { return x.text === data.text; });
        if (modifiedOriginatorIdx >= 0) {
            this.originators[modifiedOriginatorIdx] = data;
        }
        else {
            this.originators.push(data);
        }
        this.dataService.addOriginatorInfo(data)
            .subscribe(function () {
            _this.setOriginatorInfoToPristime();
            _this.toastr.success(_this.translation.translate('Nsd.SaveOriginatorSuccess'), "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            _this.toastr.error(_this.translation.translate('ErrorMsgs.AddOriginatorError'), "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NsdFormComponent.prototype.setOriginatorInfoToPristime = function () {
        var originatorCtrl = this.nsdForm.get("originator");
        var originatorEmailCtrl = this.nsdForm.get("originatorEmail");
        var originatorPhoneCtrl = this.nsdForm.get("originatorPhone");
        originatorCtrl.markAsPristine();
        originatorEmailCtrl.markAsPristine();
        originatorPhoneCtrl.markAsPristine();
    };
    NsdFormComponent.prototype.removeOriginator = function () {
        var _this = this;
        var modifiedOriginatorIdx = this.originators.findIndex(function (x) { return x.id == _this.seletedOriginatorId; });
        if (modifiedOriginatorIdx >= 0) {
            this.originators.splice(modifiedOriginatorIdx, 1);
        }
        this.dataService.removeOriginatorInfo(+this.seletedOriginatorId)
            .subscribe(function (data) {
            _this.clearOriginatorPanel(true);
            _this.toastr.success(_this.translation.translate('Nsd.RemoveOriginatorSuccess'), "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            _this.toastr.error("ErrorMsgs.RemoveOriginatorError", "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NsdFormComponent.prototype.clearOriginatorPanel = function (removePhoneAndEmal) {
        if (removePhoneAndEmal === void 0) { removePhoneAndEmal = false; }
        this.seletedOriginatorId = "-1";
        this.nsdForm.get('originator').setValue(null);
        if (removePhoneAndEmal) {
            this.nsdForm.get('originatorEmail').setValue(null);
            this.nsdForm.get('originatorPhone').setValue(null);
        }
    };
    NsdFormComponent.prototype.updateOriginatorPanel = function (id, name, email, phone) {
        this.seletedOriginatorId = id;
        var originatorCtrl = this.nsdForm.get('originator');
        var origEmailCtrl = this.nsdForm.get('originatorEmail');
        var origPhoneCtrl = this.nsdForm.get('originatorPhone');
        originatorCtrl.setValue(name);
        origEmailCtrl.setValue(email);
        origPhoneCtrl.setValue(phone);
        if (id === "60000000" || id !== "-1") {
            var originatorToken = this.model.token.originator ? this.model.token.originator : null;
            var emailToken = this.model.token.originatorEmail ? this.model.token.originatorEmail : null;
            var phoneToken = this.model.token.originatorPhone ? this.model.token.originatorPhone : null;
            if (originatorToken === name) {
                originatorCtrl.markAsPristine();
            }
            else {
                originatorCtrl.markAsDirty();
            }
            if (emailToken === email) {
                origEmailCtrl.markAsPristine();
            }
            else {
                origEmailCtrl.markAsDirty();
            }
            if (phoneToken === phone) {
                origPhoneCtrl.markAsPristine();
            }
            else {
                origPhoneCtrl.markAsDirty();
            }
        }
        else {
            if (originatorCtrl.value === name) {
                originatorCtrl.markAsPristine();
            }
            else {
                originatorCtrl.markAsDirty();
            }
            if (origEmailCtrl.value === email) {
                origEmailCtrl.markAsPristine();
            }
            else {
                originatorCtrl.markAsDirty();
            }
            if (origPhoneCtrl.value === email) {
                origPhoneCtrl.markAsPristine();
            }
            else {
                origPhoneCtrl.markAsDirty();
            }
        }
    };
    NsdFormComponent.prototype.loadMatchingOriginators = function (originatorName) {
        var _this = this;
        if (originatorName !== null) {
            this.dataService.findMatchingOriginatorInfos(originatorName, this.windowRef.appConfig.orgId)
                .subscribe(function (data) {
                if (data.length > 0) {
                    _this.originators = data;
                    var index = _this.originators.findIndex((function (x) { return x.text === originatorName; }));
                    if (index >= 0) {
                        if (_this.model.originatorEmail) {
                            if (_this.model.originatorEmail != _this.originators[index].email) {
                                _this.originators[index].email = _this.model.originatorEmail;
                            }
                        }
                        else {
                            _this.originators[index].email = null;
                        }
                        if (_this.model.originatorPhone) {
                            if (_this.model.originatorPhone != _this.originators[index].phone) {
                                _this.originators[index].phone = _this.model.originatorPhone;
                            }
                        }
                        else {
                            _this.originators[index].phone = null;
                        }
                    }
                    else {
                        var originator = {
                            id: "60000000",
                            organization: null,
                            organizationId: +_this.windowRef.appConfig.orgId,
                            text: _this.model.token.originator,
                            email: _this.model.token.originatorEmail,
                            phone: _this.model.token.originatorPhone,
                        };
                        _this.originators.splice(0, 0, originator);
                    }
                    _this.seletedOriginatorId = index < 0 ? "60000000" : _this.originators[index].id + "";
                }
                else {
                    _this.setOriginator(originatorName);
                }
            }, function (error) {
                _this.toastr.error(_this.translation.translate('Dashboard.MessageIcaoError'), "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            this.setOriginator(originatorName);
        }
    };
    NsdFormComponent.prototype.getPrevIcaoText = function () {
        var _this = this;
        if (!this.model.notamId) {
            return;
        }
        var action = -1;
        if (this.model.status == data_model_1.ProposalStatus.Replaced ||
            this.model.status == data_model_1.ProposalStatus.Cancelled ||
            this.model.status == data_model_1.ProposalStatus.Submitted ||
            this.model.status == data_model_1.ProposalStatus.Withdrawn ||
            this.model.status == data_model_1.ProposalStatus.Rejected ||
            (this.model.status == data_model_1.ProposalStatus.Picked && (this.model.proposalType == data_model_1.ProposalType.Replace || this.model.proposalType == data_model_1.ProposalType.Cancel))) {
            action = 1;
        }
        this.loadingPrevIcao = true;
        this.dataService.getPrevNotamIcaoText(this.model.notamId, action)
            .subscribe(function (data) {
            if (data) {
                _this.isPrevIcaoTabVisible = true;
            }
            _this.prevIcaoText = data;
            _this.loadingPrevIcao = false;
        }, function (error) {
            _this.loadingPrevIcao = false;
        });
    };
    NsdFormComponent.prototype.clearModelItemD = function () {
        this.model.token.itemD = "";
        this.model.itemD = "";
        this.updateFormControlValue("itemD", this.model.token.itemD);
        this.schedulerFreeText.clearFormControls();
    };
    NsdFormComponent.prototype.setOriginator = function (originatorName) {
        this.originators = [{ id: "60000000", text: originatorName }];
        this.seletedOriginatorId = "60000000";
    };
    NsdFormComponent.prototype.generateIcao = function (data) {
        var _this = this;
        if (this.model.proposalType === data_model_1.ProposalType.Cancel || this.nsdForm.dirty && (this.isGroupingEnabled || !this.isReadOnly)) {
            this.isSaveButtonDisabled = !this.nsdForm.valid;
            this.isSubmitButtonDisabled = !this.nsdForm.valid;
            if (this.nsdForm.valid) {
                this.isSubmitButtonDisabled = this.model.status === data_model_1.ProposalStatus.Submitted || this.model.status === data_model_1.ProposalStatus.Terminated;
                this.resetIcaoModel();
                var payload = this.setPayload(true);
                this.waitingForGeneratingIcao = true;
                this.dataService.generateIcao(payload)
                    .subscribe(function (data) {
                    _this.model.token.overrideBilingual = _this.overrideBilingual;
                    _this.model.token.isBilingual = data.token.isBilingual; // get the isBilingual flag from the ICAO generation
                    data.token = _this.model.token; //don't override tokens with server response
                    data.groupId = _this.model.groupId;
                    data.rejectionReason = _this.model.rejectionReason;
                    _this.updateModel(data);
                    _this.waitingForGeneratingIcao = false;
                    //this.activateTab('icao');
                }, function (error) {
                    _this.waitingForGeneratingIcao = false;
                    _this.isSaveButtonDisabled = true;
                    _this.isSubmitButtonDisabled = true;
                    var errorMessage = error.message;
                    if (error.exceptionMessage) {
                        errorMessage += errorMessage + " " + error.exceptionMessage;
                    }
                    _this.toastr.error(_this.translation.translate('Dashboard.MessageIcaoError') + errorMessage, "", { 'positionClass': 'toast-bottom-right' });
                });
            }
            else {
                this.waitingForGeneratingIcao = false;
                this.model.itemE = "";
                this.model.itemEFrench = "";
            }
        }
        else {
            this.waitingForGeneratingIcao = false;
        }
    };
    NsdFormComponent.prototype.setPayload = function (toGenerateIcao) {
        var grpDatePeriod = this.nsdForm.controls["grpDatePeriod"];
        this.model.token.overrideBilingual = this.overrideBilingual;
        var immediate = grpDatePeriod.get('immediate');
        if (immediate) {
            this.model.token.immediate = immediate.value;
        }
        var permanent = grpDatePeriod.get('permanent');
        if (permanent) {
            this.model.token.permanent = permanent.value;
        }
        var estimated = this.nsdForm.get("grpDatePeriod.estimated");
        if (estimated) {
            this.model.token.estimated = estimated.value;
        }
        if (this.hasScheduler) {
            var itemD = this.schedulerFreeText.getFreeText();
            if (itemD) {
                if (estimated.value || permanent.value || immediate.value) {
                    this.model.itemD = "";
                    this.model.token.itemD = "";
                }
                else {
                    this.model.itemD = itemD;
                    this.model.token.itemD = itemD;
                }
            }
        }
        var originator = this.nsdForm.get("originator");
        if (originator) {
            this.model.token.originator = originator.value;
        }
        var originatorEmail = this.nsdForm.get("originatorEmail");
        if (originatorEmail) {
            this.model.token.originatorEmail = originatorEmail.value;
        }
        var originatorPhone = this.nsdForm.get("originatorPhone");
        if (originatorPhone) {
            this.model.token.originatorPhone = originatorPhone.value;
        }
        if (!toGenerateIcao) {
            this.model.token.annotations = this.model.token.annotations || [];
            var _loop_2 = function (i) {
                var annotation = this_1.annotations.value[i];
                if (annotation.id > 0) {
                    var tokenAnnotationIndex = this_1.model.token.annotations.findIndex(function (x) { return x.id === annotation.id; });
                    if (tokenAnnotationIndex >= 0) { //should always exist
                        this_1.model.token.annotations[tokenAnnotationIndex].note = annotation.note;
                    }
                }
                else { //add when new
                    this_1.model.token.annotations.push(annotation);
                }
            };
            var this_1 = this;
            for (var i = 0; i < this.annotations.value.length; i++) {
                _loop_2(i);
            }
        }
        var endValidity = grpDatePeriod.get('endValidity');
        if (endValidity && endValidity.value) {
            this.model.token.endValidity = moment.utc(endValidity.value, 'YYMMDDHHmm');
        }
        var startActivity = grpDatePeriod.get('startActivity');
        if (startActivity && startActivity.value) {
            this.model.token.startActivity = moment.utc(startActivity.value, 'YYMMDDHHmm');
        }
        var urgent = this.nsdForm.get("urgent");
        this.model.token.urgent = urgent && urgent.value === true;
        var amendPubMessage = this.nsdForm.get("amendPubMessage");
        if (amendPubMessage) {
            this.model.token.amendPubMessage = amendPubMessage.value;
        }
        var amendPubMessageFrench = this.nsdForm.get("amendPubMessageFrench");
        if (amendPubMessageFrench) {
            this.model.token.amendPubMessageFrench = amendPubMessageFrench.value;
        }
        this.setNsdPayload();
        var payload = {
            categoryId: this.model.categoryId,
            version: this.model.version,
            name: this.model.name,
            token: this.model.token,
            referredSeries: this.model.referredSeries,
            referredNumber: this.model.referredNumber,
            referredYear: this.model.referredYear,
            proposalType: this.model.proposalType,
            organizationId: this.model.organizationId,
            proposalId: this.model.proposalId,
            status: this.model.status,
            statusUpdated: this.model.statusUpdated,
            parentNotamId: this.model.parentNotamId,
            notamId: this.model.notamId,
            groupId: this.model.groupId,
            number: this.model.number,
            year: this.model.year,
            rowVersion: this.model.rowVersion
        };
        if (!toGenerateIcao) {
            //payload.organizationId = this.model.organizationId;
            //payload.proposalId = this.model.proposalId;
            //payload.status = this.model.status;
        }
        return payload;
    };
    NsdFormComponent.prototype.setGroupPayload = function (toGenerateIcao) {
        var payload = [];
        var data = this.setPayload(toGenerateIcao);
        for (var _i = 0, _a = this.groupModel; _i < _a.length; _i++) {
            var model = _a[_i];
            if (!model.isSelected) {
                payload.push(model);
            }
            else {
                //data.isSelected = true;
                data.groupId = model.groupId;
                data.isGroupMaster = model.isGroupMaster;
                data.grouped = model.grouped;
                payload.push(data);
            }
        }
        return payload;
    };
    NsdFormComponent.prototype.checkScheduleButtonVisiblity = function () {
        var immediateCtrl = this.nsdForm.get('grpDatePeriod.immediate');
        var permanentCtrl = this.nsdForm.get('grpDatePeriod.permanent');
        var estimatedCtrl = this.nsdForm.get('grpDatePeriod.estimated');
        if (!immediateCtrl.value && !permanentCtrl.value && !estimatedCtrl.value) {
            return true;
        }
        return false;
    };
    NsdFormComponent.prototype.loadScheduler = function (proposalId) {
        var _this = this;
        this.dataService.loadItemD(proposalId)
            .subscribe(function (data) {
            if (data.length > 0) {
                _this.hasScheduler = true;
                _this.isSchedulePanelOpen = true;
                _this.schedulerTab = data[0].calendarType;
                switch (_this.schedulerTab) {
                    case 1:
                        _this.schedulerDaily.initialize(data, _this.model.startActivity, _this.model.endValidity, null);
                        _this.schedulerDaily.parseEvents();
                        _this.schedulerDaily.slideScheduleClose();
                        break;
                    case 2:
                        _this.schedulerDayWeeks.initialize(data, _this.model.startActivity, _this.model.endValidity, null);
                        _this.schedulerDayWeeks.parseEvents();
                        _this.schedulerDayWeeks.slideScheduleClose();
                        break;
                    case 3:
                        _this.schedulerDate.initialize(data, _this.model.startActivity, _this.model.endValidity, null);
                        _this.schedulerDate.parseEvents();
                        _this.schedulerDate.slideScheduleClose();
                        break;
                    case 4:
                        _this.isScheduleOverridden = true;
                        _this.schedulerFreeText.initialize(data, _this.model.startActivity, _this.model.endValidity);
                        _this.schedulerDaily.slideScheduleClose();
                        _this.schedulerDayWeeks.slideScheduleClose();
                        _this.schedulerDate.slideScheduleClose();
                        break;
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    NsdFormComponent.prototype.getMasterScheduler = function (itemDs, calendarId, proposalId) {
        var cEvents = this.schedulerDate.getCalendarEvents(calendarId);
        for (var i = 0; i < cEvents.length; i++) {
            var cEvent = cEvents[i];
            var event_1 = {
                id: cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: cEvent.end ? moment.utc(cEvent.end).format() : null,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                extendedHours: cEvent.extendedHours,
                previousColor: cEvent.previousColor
            };
            var itemD = {
                calendarType: 3,
                proposalId: proposalId,
                calendarId: calendarId,
                event: JSON.stringify(event_1)
            };
            itemDs.push(itemD);
        }
        return itemDs;
    };
    NsdFormComponent.prototype.getDetailScheduler = function (itemDs, calendarId, proposalId) {
        var cEvents = this.schedulerDate.getCalendarEvents(calendarId);
        for (var i = 0; i < cEvents.length; i++) {
            var cEvent = cEvents[i];
            var event_2 = {
                id: cEvent._id || cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: moment.utc(cEvent.end).format(),
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunrise: cEvent.s_sunrise,
                s_sunset: cEvent.s_sunset,
                e_sunrise: cEvent.e_sunrise,
                e_sunset: cEvent.e_sunset,
                exStarttime: cEvent.exStarttime,
                exEndtime: cEvent.exEndtime,
                extendedHours: cEvent.extendedHours
            };
            var itemD = {
                calendarType: 3,
                proposalId: proposalId,
                calendarId: calendarId,
                eventId: event_2.id,
                event: JSON.stringify(event_2)
            };
            itemDs.push(itemD);
        }
        return itemDs;
    };
    NsdFormComponent.prototype.saveDailyScheduler = function (proposalId) {
        var itemDs = [];
        var cEvents = this.schedulerDaily.getCalendarEvents();
        for (var i = 0; i < cEvents.length; i++) {
            var cEvent = cEvents[i];
            var event_3 = {
                id: cEvent._id || cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: moment.utc(cEvent.end).format(),
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                exStarttime: cEvent.exStarttime,
                exEndtime: cEvent.exEndtime,
                extendedHours: cEvent.extendedHours
            };
            var itemD = {
                calendarType: 1,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event_3.id,
                itemD: this.model.itemD,
                event: JSON.stringify(event_3)
            };
            itemDs.push(itemD);
        }
        this.dataService.saveItemD(itemDs)
            .subscribe(function (data) {
        }, function (error) {
        });
    };
    NsdFormComponent.prototype.saveWeeklyScheduler = function (proposalId) {
        var cEvents = this.schedulerDayWeeks.getCalendarEvents();
        var itemDs = [];
        for (var i = 0; i < cEvents.length; i++) {
            var cEvent = cEvents[i];
            var event_4 = {
                id: cEvent._id || cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: moment.utc(cEvent.end).format(),
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                s_setTime: cEvent.s_setTime,
                e_setTime: cEvent.e_setTime
            };
            var itemD = {
                calendarType: 2,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event_4.id,
                itemD: this.model.itemD,
                event: JSON.stringify(event_4)
            };
            itemDs.push(itemD);
        }
        this.dataService.saveItemD(itemDs)
            .subscribe(function (data) {
        }, function (error) {
        });
    };
    NsdFormComponent.prototype.saveFreeTextScheduler = function (proposalId) {
        var freeText = this.schedulerFreeText.getFreeText();
        var itemDs = [];
        var event = {
            itemD: freeText
        };
        var itemD = {
            calendarType: 4,
            proposalId: proposalId,
            calendarId: -1,
            eventId: -1,
            itemD: this.model.itemD,
            event: JSON.stringify(event)
        };
        itemDs.push(itemD);
        this.dataService.saveItemD(itemDs)
            .subscribe(function (data) {
        }, function (error) {
        });
    };
    NsdFormComponent.prototype.updateReviewListFilter = function (proposalId) {
        //updating the filter to show and select the latest item saved in the review list;
        var queryOptions = this.memStorageService.get(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            queryOptions.sort = "Received";
            queryOptions.page = 1;
            this.memStorageService.save(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY, queryOptions);
        }
        this.memStorageService.save(this.memStorageService.PROPOSAL_ID_KEY, proposalId);
    };
    NsdFormComponent.prototype.setNsdPayload = function () {
        var frmArrControl = this.nsdForm.controls['frmArr'];
        var frmArrControls = frmArrControl.controls[0];
        for (var key in frmArrControls.controls) {
            //first we need to clean all the Array controls on the token
            if (this.model.token[key] instanceof Array) {
                this.model.token[key] = [];
            }
            if (frmArrControls.controls.hasOwnProperty(key)) {
                var control = frmArrControls.controls[key];
                if (control && control.value !== null) {
                    if (typeof (control.value) === "object") {
                        var grp = frmArrControls.controls[key];
                        var grpObject = grp.controls;
                        if (key in this.model.token) {
                            var content = typeof (control) === "object" ? control.value : control;
                            this.model.token[key] = content.value ? content.value : content;
                        }
                        else {
                            for (var grpKey in grpObject) {
                                if (grpObject.hasOwnProperty(grpKey)) {
                                    var grpControl = grpObject[grpKey];
                                    if (grpControl !== null) {
                                        if (control.value instanceof Array && key in this.model.token) {
                                            this.model.token[key].push(control.value[grpKey]);
                                        }
                                        else {
                                            if (grpKey in this.model.token) {
                                                var value = typeof (grpControl) === "object" ? grpControl.value : grpControl;
                                                this.model.token[grpKey] = value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if (key in this.model.token) {
                            var value = typeof (control) === "object" ? control.value : control;
                            this.model.token[key] = control.value;
                        }
                    }
                }
            }
        }
    };
    NsdFormComponent.prototype.resetIcaoModel = function () {
        this.model.code23 = "";
        this.model.code45 = "";
        this.model.radius = 0;
        this.model.lowerLimit = 0;
        this.model.upperLimit = 0;
        this.model.scope = "";
        this.model.series = "";
        this.model.startActivity = null;
        this.model.endValidity = null;
        this.model.fir = "";
        this.model.itemA = "";
        this.model.itemD = "";
        this.model.itemE = "";
        this.model.itemEFrench = "";
        this.model.itemF = 0;
        this.model.itemF = 0;
    };
    NsdFormComponent.prototype.updateModel = function (data) {
        data.organizationId = data.organizationId ? data.organizationId : this.model.organizationId;
        data.proposalId = data.proposalId ? data.proposalId : this.model.proposalId;
        var attachments = this.model.attachments;
        this.model = data;
        this.model.attachments = this.model.attachments || attachments;
    };
    NsdFormComponent.prototype.configureReactiveForm = function () {
        var self = this;
        if (this.model.proposalType === data_model_1.ProposalType.Cancel) {
            this.model.token.startActivity = null;
            this.model.token.immediate = true;
            this.model.token.endValidity = null;
            this.model.token.permanent = false;
            this.model.token.estimated = false;
            this.model.token.itemD = null;
        }
        else {
            if (this.model.token.startActivity)
                this.startActivityText = moment.utc(this.model.token.startActivity).format("YYYY/MM/DD, H:mm") + " UTC";
            if (this.model.token.endValidity)
                this.endValidityText = moment.utc(this.model.token.endValidity).format("YYYY/MM/DD, H:mm") + " UTC";
        }
        this.startActivityMoment = this.model.token.startActivity ? moment.utc(this.model.token.startActivity).format(this.dateFormat) : null;
        this.endValidityMoment = this.model.token.endValidity ? moment.utc(this.model.token.endValidity).format(this.dateFormat) : null;
        this.nsdForm = this.fb.group({
            originator: [{ value: this.model.token.originator }, [originatorValidator]],
            originatorEmail: [{ value: this.model.token.originatorEmail }],
            originatorPhone: [{ value: this.model.token.originatorPhone }],
            grpDatePeriod: this.fb.group({
                immediate: { value: this.model.token.immediate, disabled: this.isReadOnly || this.model.proposalType === data_model_1.ProposalType.Cancel },
                startActivity: { value: this.startActivityMoment, disabled: this.isReadOnly || this.model.proposalType === data_model_1.ProposalType.Cancel || this.model.token.immediate || this.model.itemD },
                permanent: { value: this.model.token.permanent, disabled: this.isReadOnly || this.model.proposalType === data_model_1.ProposalType.Cancel },
                endValidity: { value: this.endValidityMoment, disabled: this.isReadOnly || this.model.proposalType === data_model_1.ProposalType.Cancel || this.model.token.permanent || this.model.itemD },
                estimated: { value: this.model.token.estimated, disabled: this.isReadOnly || this.model.proposalType === data_model_1.ProposalType.Cancel },
            }, { validator: dateRangeValidator }),
            amendPubMessage: { value: this.model.token.amendPubMessage, disabled: this.isReadOnly },
            amendPubMessageFrench: { value: this.model.token.amendPubMessageFrench, disabled: this.isReadOnly },
            urgent: { value: this.model.token.urgent, disabled: this.isReadOnly },
            annotations: this.fb.array([]),
            //itemD: { value: this.model.token.itemD, disabled: this.isReadOnly || this.model.proposalType === ProposalType.Cancel},
            itemD: { value: this.model.token.itemD, disabled: false },
            overrideBilingual: [{ value: this.model.token.overrideBilingual, disabled: this.model.token.isBilingualRegion }],
            frmArr: this.fb.array([])
        });
        if (this.model.token.annotations) {
            for (var i = 0; i < this.model.token.annotations.length; i++) {
                this.annotations.push(this.buildAnnotation(this.model.token.annotations[i]));
            }
        }
    };
    NsdFormComponent.prototype.buildAnnotation = function (annotation) {
        return this.fb.group({
            id: annotation.id || 0,
            note: [{ value: annotation.note || "", disabled: false }, [forms_1.Validators.required]],
            userId: annotation.userId || this.windowRef.appConfig.usrName,
            created: moment.utc(annotation.received) || moment(),
            selected: false,
        });
    };
    NsdFormComponent.prototype.canSubmitFromStatus = function (status) {
        var result = (status === data_model_1.ProposalStatus.Draft ||
            status === data_model_1.ProposalStatus.Withdrawn ||
            status === data_model_1.ProposalStatus.Cancelled ||
            status === data_model_1.ProposalStatus.Picked ||
            status === data_model_1.ProposalStatus.ParkedPicked ||
            status === data_model_1.ProposalStatus.Taken);
        return result;
        //return status === ProposalStatus.Draft || status === ProposalStatus.Withdrawn || status === ProposalStatus.Cancelled || status === ProposalStatus.Picked || status === ProposalStatus.ParkedPicked;
    };
    NsdFormComponent.prototype.isANumber = function (str) {
        return !/\D/.test(str);
    };
    // this is nessecary because of how we load the map in a modal window
    NsdFormComponent.prototype.initMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.initMap();
        }
    };
    NsdFormComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.mapHealthy = true;
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        });
    };
    NsdFormComponent.prototype.mapAvailable = function () {
        return this.mapHealthy;
    };
    NsdFormComponent.prototype.validateMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.resizeMapForModal();
        }
    };
    NsdFormComponent.prototype.validateGeoMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletgeomap = winObj['app'].leafletgeomap;
        if (leafletgeomap) {
            leafletgeomap.resizeGeoMapForModal();
        }
    };
    NsdFormComponent.prototype.disposeMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletmap = winObj['app'].leafletmap;
        //        const leafletgeomap = winObj['app'].leafletgeomap;
        if (leafletmap) {
            leafletmap.dispose();
        }
        //if (leafletgeomap) {
        //    leafletgeomap.disposeGeoMap();
        //}
    };
    NsdFormComponent.prototype.getActiveSchedule = function () {
        if (this.schedulerDaily.hasEvents()) {
            return 1;
        }
        else if (this.schedulerDayWeeks.hasEvents()) {
            return 2;
        }
        else if (this.schedulerDate.hasEvents()) {
            return 3;
        }
        else {
            return this.schedulerTab; //this should be itemD text (4)
        }
    };
    NsdFormComponent.prototype.saveScheduler = function (data) {
        if (this.hasScheduler) {
            var activeSchedule = this.getActiveSchedule();
            switch (activeSchedule) {
                case 1:
                    this.saveDailyScheduler(data.proposalId);
                    break;
                case 2:
                    this.saveWeeklyScheduler(data.proposalId);
                    break;
                case 3:
                    var itemDs = [];
                    itemDs = this.getMasterScheduler(itemDs, 1, data.proposalId);
                    itemDs = this.getMasterScheduler(itemDs, 2, data.proposalId);
                    itemDs = this.getMasterScheduler(itemDs, 3, data.proposalId);
                    itemDs = this.getDetailScheduler(itemDs, 4, data.proposalId);
                    this.dataService.saveItemD(itemDs)
                        .subscribe(function (data) {
                    }, function (error) {
                    });
                    break;
                case 4:
                    this.saveFreeTextScheduler(data.proposalId);
                    break;
            }
        }
    };
    // Force Bilingual
    NsdFormComponent.prototype.displayBilingual = function () {
        var _this = this;
        if (this.model.status === 0) {
            this.dataService.override.subscribe(function (value) { return _this.overrideBilingual = value; });
        }
        var value = this.isBilingualRegion || this.overrideBilingual;
        return value;
    };
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], NsdFormComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], NsdFormComponent.prototype, "isReadOnly", void 0);
    __decorate([
        core_1.Input('isNOF'),
        __metadata("design:type", Boolean)
    ], NsdFormComponent.prototype, "isNOF", void 0);
    __decorate([
        core_1.ViewChildren(nsd_buttonbar_component_1.NsdButtonBarComponent),
        __metadata("design:type", core_1.QueryList)
    ], NsdFormComponent.prototype, "buttonBars", void 0);
    __decorate([
        core_1.ViewChild(nsd_scheduler_daily_component_1.NsdSchedulerDailyComponent),
        __metadata("design:type", nsd_scheduler_daily_component_1.NsdSchedulerDailyComponent)
    ], NsdFormComponent.prototype, "schedulerDaily", void 0);
    __decorate([
        core_1.ViewChild(nsd_scheduler_date_component_1.NsdSchedulerDateComponent),
        __metadata("design:type", nsd_scheduler_date_component_1.NsdSchedulerDateComponent)
    ], NsdFormComponent.prototype, "schedulerDate", void 0);
    __decorate([
        core_1.ViewChild(nsd_scheduler_days_week_component_1.NsdSchedulerDaysOfWeekComponent),
        __metadata("design:type", nsd_scheduler_days_week_component_1.NsdSchedulerDaysOfWeekComponent)
    ], NsdFormComponent.prototype, "schedulerDayWeeks", void 0);
    __decorate([
        core_1.ViewChild(nsd_scheduler_free_text_component_1.NsdSchedulerFreeTextComponent),
        __metadata("design:type", nsd_scheduler_free_text_component_1.NsdSchedulerFreeTextComponent)
    ], NsdFormComponent.prototype, "schedulerFreeText", void 0);
    __decorate([
        core_1.ViewChild(nsd_georeftool_component_1.NsdGeoRefToolComponent),
        __metadata("design:type", nsd_georeftool_component_1.NsdGeoRefToolComponent)
    ], NsdFormComponent.prototype, "geoRefTool", void 0);
    NsdFormComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/nsd-form.component.html',
            selector: 'proposal-form',
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            core_1.ChangeDetectorRef,
            angular_l10n_1.TranslationService,
            common_1.Location])
    ], NsdFormComponent);
    return NsdFormComponent;
}());
exports.NsdFormComponent = NsdFormComponent;
//Custom Validators
function dateRangeValidator(c) {
    var startActivityCtrl = c.get("startActivity");
    var endValidityCrl = c.get("endValidity");
    var immediateCtrl = c.get("immediate");
    var permanentCtrl = c.get("permanent");
    var startActivityErrors = null;
    var endValidityErrors = null;
    if (!immediateCtrl.value) {
        if (startActivityCtrl.value && startActivityCtrl.value.length > 0 && startActivityCtrl.value.length < 10) {
            startActivityErrors = {
                'invalidStartActivityTenDigitDate': true
            };
        }
        var startDate = startActivityCtrl.value ? moment.utc(startActivityCtrl.value, 'YYMMDDHHmm') : null;
        if (startDate && startDate.isValid()) {
            var now = moment.utc(moment().utc().format("MM/DD/YYYY, H:mm"));
            var minutes = moment.utc(startDate, "MM/DD/YYYY, H:mm").diff(now, 'minutes');
            if (minutes < 0) {
                if (startActivityErrors === null) {
                    startActivityErrors = {
                        'validateOnlyInFuture': true
                    };
                }
            }
        }
        else {
            if (startActivityErrors === null) {
                startActivityErrors = {
                    'invalidStartActivityTenDigitDate': true
                };
            }
        }
    }
    if (!permanentCtrl.value) {
        if (endValidityCrl.value && endValidityCrl.value.length > 0 && endValidityCrl.value.length < 10) {
            endValidityErrors = {
                'invalidEndValidityTenDigitDate': true
            };
        }
        var startDate = immediateCtrl.value
            ? moment.utc()
            : (startActivityCtrl.value ? moment.utc(startActivityCtrl.value, 'YYMMDDHHmm') : moment.utc());
        var endValidityDate = endValidityCrl.value ? moment.utc(endValidityCrl.value, 'YYMMDDHHmm') : null;
        if (endValidityDate && endValidityDate.isValid()) {
            var minutes = endValidityDate.diff(startDate, 'minutes');
            var days = endValidityDate.diff(startDate, 'days');
            if (days > 92) {
                endValidityErrors = {
                    'validateLaterThan92Days': true
                };
            }
            if (endValidityCrl.value) {
                var mvp = +window["app"].mvp || 30;
                if (minutes < mvp) {
                    if (endValidityErrors === null) {
                        endValidityErrors = {
                            'validateLaterThan30MinStartingDate': true
                        };
                    }
                }
            }
        }
        else {
            if (endValidityErrors === null) {
                endValidityErrors = {
                    'invalidEndValidityTenDigitDate': true
                };
            }
        }
    }
    if (startActivityErrors === null && endValidityErrors === null)
        return null;
    if (startActivityErrors !== null && endValidityErrors !== null) {
        return Object.assign(startActivityErrors, endValidityErrors);
    }
    else if (startActivityErrors !== null) {
        return startActivityErrors;
    }
    else {
        return endValidityErrors;
    }
}
Number.prototype["pad"] = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) {
        s = "0" + s;
    }
    return s;
};
function emailWidthDomainValidator(c) {
    var email = c.value;
    var re = /\S+@\S+\.\S+/;
    if (email && !re.test(email))
        return { email: true };
    return null;
}
function originatorValidator(c) {
    var originator = c;
    var value = null;
    if (originator) {
        value = originator.value instanceof Object ? originator.value.value : originator.value;
    }
    if (value && value !== "60000000") {
        return null;
    }
    return {
        'required': true
    };
}
//# sourceMappingURL=nsd-form.component.js.map