"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CatchAllCancelComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var catchall_service_1 = require("./catchall.service");
//import { ISdoSubject } from './catchall.model'
var toastr_service_1 = require("./../../../common/toastr.service");
var windowRef_service_1 = require("../../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var CatchAllCancelComponent = /** @class */ (function () {
    function CatchAllCancelComponent(toastr, injector, dataService, catchAllService, fb, windowRef, changeDetectionRef, translation) {
        this.toastr = toastr;
        this.injector = injector;
        this.dataService = dataService;
        this.catchAllService = catchAllService;
        this.fb = fb;
        this.windowRef = windowRef;
        this.changeDetectionRef = changeDetectionRef;
        this.translation = translation;
        this.token = null;
        this.isReadOnly = true;
        this.isBilingualRegion = false;
        this.overrideBilingual = false;
        this.subjectSelected = null;
        this.icaoSubjectSelected = null;
        this.icaoConditionSelected = null;
        this.icaoSubjectSelectOptions = {
            width: "100%"
        };
        this.icaoConditionSelectOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ICAOConditionPlaceholder')
            }
        };
        this.nonIcaoAerodrome = false;
        this.token = this.injector.get('token');
        this.nsdForm = this.injector.get('form');
    }
    CatchAllCancelComponent.prototype.ngOnInit = function () {
        var app = this.catchAllService.app;
        this.leafletmap = app.leafletmap;
        this.createUnitsListBox();
        this.createSeriesListBox();
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
        this.loadSubject(this.token.subjectId);
        if (this.token.type === "cancel") {
            var fc = this.nsdFrmGroup.get('itemEValue');
            this.prevItemE = fc.value;
            fc = this.nsdFrmGroup.get('itemEFrenchValue');
            this.prevItemEFrench = fc.value;
        }
        this.overrideBilingual = this.token.overrideBilingual;
        this.loadMapLocations(app.doaId);
    };
    CatchAllCancelComponent.prototype.ngOnDestroy = function () {
        this.leafletmap.dispose();
        this.leafletmap = null;
    };
    CatchAllCancelComponent.prototype.requireLowerUpperLimit = function () {
        return this.nsdFrmGroup.get('icaoConditionId').value != -1 &&
            (!this.nsdFrmGroup.get('requiresItemFG').value ||
                this.nsdFrmGroup.get('grpItemFG.itemFUnits').value === 2 ||
                this.nsdFrmGroup.get('grpItemFG.itemGUnits').value === 2);
    };
    CatchAllCancelComponent.prototype.requireItemFG = function () {
        return this.nsdFrmGroup.get('requiresItemFG').value;
    };
    CatchAllCancelComponent.prototype.icaoConditionChanged = function (e) {
        var conditionId = +e.value;
        var icaoConditionIdCtrl = this.nsdFrmGroup.get('icaoConditionId');
        if (conditionId !== +icaoConditionIdCtrl.value) {
            icaoConditionIdCtrl.setValue(conditionId);
        }
        icaoConditionIdCtrl.markAsDirty();
        if (this.icaoConditionSelected && this.icaoConditionSelected.id === conditionId) {
            return; //when multiples change events occurring react only to the first one
        }
        this.icaoConditionSelected = this.icaoConditions.filter(function (item) { return item.id === conditionId; })[0];
    };
    CatchAllCancelComponent.prototype.validateItemE = function () {
        var fc = this.nsdFrmGroup.get('itemEValue');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    CatchAllCancelComponent.prototype.validateItemEFrench = function () {
        var fc = this.nsdFrmGroup.get('itemEFrenchValue');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    CatchAllCancelComponent.prototype.toogleOverrideBilingual = function (e) {
        if (!this.isBilingualRegion) {
            this.overrideBilingual = e.target.checked;
            this.dataService.nextOverrideBilingual(this.overrideBilingual);
            if (this.overrideBilingual) {
                this.nsdFrmGroup.get('itemEFrenchValue').clearValidators();
                this.nsdFrmGroup.get('itemEFrenchValue').setValidators(forms_1.Validators.required);
                if (this.nsdFrmGroup.get('itemEFrenchValue').value) {
                    this.nsdFrmGroup.get('itemEFrenchValue').setValue(this.token.itemEFrenchValue);
                }
                else {
                    this.nsdFrmGroup.get('itemEFrenchValue').setValue(null);
                    this.nsdFrmGroup.setErrors({ 'invalid': true });
                }
                this.nsdFrmGroup.get('itemEFrenchValue').enable();
                this.nsdFrmGroup.markAsDirty();
            }
            else {
                if (this.nsdFrmGroup.get('itemEFrenchValue').value) {
                    this.nsdFrmGroup.get('itemEFrenchValue').setValue(null);
                }
                this.nsdFrmGroup.get('itemEFrenchValue').clearValidators();
                this.nsdFrmGroup.get('itemEFrenchValue').disable();
                this.nsdFrmGroup.markAsDirty();
            }
        }
    };
    CatchAllCancelComponent.prototype.validateIcaoConditionId = function () {
        var fc = this.nsdFrmGroup.get('icaoConditionId');
        return fc.value !== -1 || this.nsdFrmGroup.pristine;
    };
    CatchAllCancelComponent.prototype.validateAerodromeName = function () {
        var fc = this.nsdFrmGroup.get('aerodromeName');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllCancelComponent.prototype.select2Options = function () {
        var self = this;
        var app = this.catchAllService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            templateResult: function (data) {
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                self.nonIcaoAerodrome = self.designatorHasNumbers(selection.designator);
                self.createScopesListBox(selection.sdoEntityName);
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    CatchAllCancelComponent.prototype.loadSubject = function (subjectId) {
        var _this = this;
        this.dataService.getSdoSubject(subjectId)
            .subscribe(function (sdoSubjectResult) {
            _this.subjectSelected = sdoSubjectResult;
            _this.subjects = [];
            _this.subjects.push(sdoSubjectResult);
            _this.nonIcaoAerodrome = _this.designatorHasNumbers(sdoSubjectResult.designator);
            if (_this.nonIcaoAerodrome) {
                var aerodromeCtrl = _this.nsdFrmGroup.get('aerodromeName');
                if (!aerodromeCtrl.value) {
                    aerodromeCtrl.markAsDirty();
                }
                aerodromeCtrl.setValidators(forms_1.Validators.required);
            }
            _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
            _this.loadIcaoSubjects(sdoSubjectResult.sdoEntityName);
            _this.updateBilingualFromTokenLocation(_this.token.subjectLocation);
        });
    };
    CatchAllCancelComponent.prototype.updateBilingualFromTokenLocation = function (value) {
        var _this = this;
        this.dataService.validateLocation(value)
            .subscribe(function (result) { return _this.setBilingualRegion(result.inBilingualRegion); });
    };
    CatchAllCancelComponent.prototype.loadSeries = function (qCode) {
        var _this = this;
        this.dataService.getSeries()
            .subscribe(function (series) {
            _this.setFirstSelectItemInCombo(series, _this.token.series, false);
            _this.series = series;
        });
    };
    CatchAllCancelComponent.prototype.loadMapLocations = function (doaId) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leafletmap && self.leafletmap.isReady()) {
                    window.clearInterval(interval);
                    self.leafletmap.addDoa(geoJsonStr, 'DOA');
                    self.updateMap(self.subjectSelected);
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Nsd.DOAGeoError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    CatchAllCancelComponent.prototype.loadIcaoSubjects = function (sdoEntityName) {
        var _this = this;
        if (sdoEntityName) {
            this.dataService.getIcaoSubjects(sdoEntityName)
                .subscribe(function (icaoSubjects) {
                _this.loadIcaoConditions();
                _this.setFirstSelectItemInCombo(icaoSubjects, _this.token.icaoSubjectId);
                _this.icaoSubjects = icaoSubjects.map(function (item) {
                    if (item.code !== undefined) {
                        if (item.name.length > 90) {
                            item.text = item.code + ' - ' + item.name.substr(0, 80) + '...';
                        }
                        else {
                            item.text = item.code + ' - ' + item.name;
                        }
                    }
                    return item;
                });
            });
        }
    };
    CatchAllCancelComponent.prototype.loadIcaoConditions = function () {
        var _this = this;
        this.dataService.getIcaoConditions(this.token.icaoSubjectId, true)
            .subscribe(function (icaoConditions) {
            icaoConditions.unshift({
                id: -1,
                text: '',
            });
            _this.setFirstSelectItemInCombo(icaoConditions, _this.token.icaoConditionId);
            _this.icaoConditions = icaoConditions.map(function (item) {
                if (item.code !== undefined) {
                    if (item.description.length > 90) {
                        item.text = item.code + ' - ' + item.description.substr(0, 80) + '...';
                    }
                    else {
                        item.text = item.code + ' - ' + item.description;
                    }
                }
                return item;
            });
            _this.changeDetectionRef.detectChanges();
        });
    };
    CatchAllCancelComponent.prototype.updateMap = function (sdoSubject) {
        var _this = this;
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject && sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson && geojson.features.length === 1) {
                var markerGeojson_1 = this.getGeoJsonFromCurrentLocation();
                this.leafletmap.addMarker(geojson, sdoSubject.name, function () {
                    _this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, function () {
                        if (markerGeojson_1) {
                            _this.leafletmap.setNewMarkerLocation(markerGeojson_1);
                        }
                    });
                });
            }
        }
    };
    CatchAllCancelComponent.prototype.getGeoJsonFromCurrentLocation = function () {
        return this.token && this.token.latitude && this.token.longitude
            ? this.leafletmap.latitudeLongitudeToGeoJson(this.token.latitude, this.token.longitude)
            : null;
    };
    CatchAllCancelComponent.prototype.setFirstSelectItemInCombo = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var index = items.findIndex(function (x) { return x.id === itemId; });
        if (index > 0) { //move selected item to position 0
            items.splice(0, 0, items.splice(index, 1)[0]);
        }
    };
    CatchAllCancelComponent.prototype.designatorHasNumbers = function (designator) {
        for (var i = 0; i < designator.length; i++) {
            if (parseFloat(designator[i])) {
                return true;
            }
        }
        return false;
    };
    CatchAllCancelComponent.prototype.createUnitsListBox = function () {
        this.units = [
            { id: '0', text: 'FL' },
            { id: '1', text: 'AMSL' },
            { id: '2', text: 'AGL' }
        ];
    };
    CatchAllCancelComponent.prototype.createScopesListBox = function (entityName) {
        switch (entityName) {
            case "Ahp":
                this.scopes = [
                    { id: 'A', text: 'A' },
                    { id: 'AE', text: 'AE' }
                ];
                break;
            case "FIR":
                this.scopes = [
                    { id: 'E', text: 'E' },
                    { id: 'W', text: 'W' }
                ];
                break;
            default:
                this.scopes = [
                    { id: 'A', text: 'A' },
                    { id: 'E', text: 'E' },
                    { id: 'W', text: 'W' },
                    { id: 'AE', text: 'AE' },
                    { id: 'AW', text: 'AW' },
                    { id: 'K', text: 'K' }
                ];
                break;
        }
    };
    CatchAllCancelComponent.prototype.createSeriesListBox = function () {
        var _this = this;
        this.dataService.getSeries()
            .subscribe(function (series) {
            _this.series = series;
        });
    };
    CatchAllCancelComponent.prototype.setBilingualRegion = function (value) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        var itemEFCtrl = this.nsdFrmGroup.get('itemEFrenchValue');
        if (this.isBilingualRegion || this.overrideBilingual) {
            itemEFCtrl.setValidators([forms_1.Validators.required, itemEFrenchMustChangeValidator(this.token.itemEFrenchValue)]);
            //if (!this.isReadOnly) {
            //    itemEFCtrl.enable();
            //}
        }
        else {
            //itemEFCtrl.clearValidators();
            //if (!this.isReadOnly) {
            //    itemEFCtrl.disable();
            //}
        }
    };
    CatchAllCancelComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        var frmArrControls = this.nsdForm.controls['frmArr'];
        if (!this.windowRef.appConfig.isNof) {
            this.token.icaoConditionId = -1; //NOF User should see the cancellation entry from the user
        }
        if (this.token.itemEFrenchValue && this.token.itemEFrenchValue.length > 0) {
            this.isBilingualRegion = true;
        }
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, []],
            icaoSubjectId: [this.token.icaoSubjectId, []],
            icaoConditionId: [this.token.icaoConditionId || -1, [icaoConditionValidator]],
            subjectLocation: [{ value: this.token.subjectLocation, disabled: this.isReadOnly }, []],
            latitude: [{ value: this.token.latitude, disabled: this.isReadOnly }, []],
            longitude: [{ value: this.token.longitude, disabled: this.isReadOnly }, []],
            scope: this.token.scope,
            series: this.token.series,
            itemA: { value: this.token.itemA, disabled: this.isReadOnly },
            itemEValue: [{ value: this.token.itemEValue, disabled: false }, [forms_1.Validators.required, itemEMustChangeValidator(this.token.itemEValue)]],
            itemEFrenchValue: [{ value: this.token.itemEFrenchValue, disabled: false }],
            aerodromeName: { value: this.token.aerodromeName, disabled: this.isReadOnly },
            grpPurposes: this.fb.group({
                purposeB: this.token.purposeB,
                purposeM: this.token.purposeM,
                purposeN: this.token.purposeN,
                purposeO: this.token.purposeO,
            }),
            lowerLimit: { value: this.token.lowerLimit, disabled: this.isReadOnly },
            upperLimit: { value: this.token.upperLimit, disabled: this.isReadOnly },
            radius: { value: this.token.radius, disabled: this.isReadOnly },
            grpTraffic: this.fb.group({
                trafficI: this.token.trafficI,
                trafficV: this.token.trafficV,
            }),
            grpItemFG: this.fb.group({
                itemFValue: { value: this.token.itemFValue, disabled: this.isReadOnly },
                itemFSurface: { value: this.token.itemFSurface, disabled: this.isReadOnly },
                itemFUnits: this.token.itemFUnits,
                itemGValue: { value: this.token.itemGValue, disabled: this.isReadOnly },
                itemGUnlimited: { value: this.token.itemGUnlimited, disabled: this.isReadOnly },
                itemGUnits: this.token.itemGUnits,
            }),
            requiresItemA: this.token.itemA !== null,
            requiresSeries: this.token.series !== null,
            requiresScope: this.token.scope !== null,
            requiresPurpose: this.token.purposeB || this.token.purposeM || this.token.purposeN || this.token.purposeO,
            requiresItemFG: this.token.requiresItemFG // itemFValue || this.token.itemGValue
        });
        if (this.isBilingualRegion) {
            var control = this.nsdFrmGroup.get('itemEFrenchValue');
            control.setValidators([forms_1.Validators.required, itemEFrenchMustChangeValidator(this.token.itemEFrenchValue)]);
        }
        if (this.token.scope) {
            this.createScopesListBox(this.token.subjectId.substring(0, 3));
            var scope = this.scopes.filter(function (item) { return item.id === _this.token.scope; });
            if (scope.length === 1) {
                var fc = this.nsdFrmGroup.get('scope');
                //fc.setValue(scope[0].id);
            }
        }
        if (this.token.series && this.series) {
            var series = this.series.filter(function (item) { return item.id === _this.token.series; });
            if (series.length === 1) {
                var fc = this.nsdFrmGroup.get('series');
                fc.setValue(series[0].id);
            }
        }
        frmArrControls.push(this.nsdFrmGroup);
    };
    CatchAllCancelComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("icaoSubjectId", this.token.icaoSubjectId);
        this.updateFormControlValue("icaoConditionId", this.token.icaoConditionId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("latitude", this.token.latitude);
        this.updateFormControlValue("longitude", this.token.longitude);
        this.updateFormControlValue("scope", this.token.scope);
        this.updateFormControlValue("series", this.token.series);
        this.updateFormControlValue("itemA", this.token.itemA);
        this.updateFormControlValue("aerodromeName", this.token.aerodromeName);
        this.updateFormControlValue("itemEValue", this.token.itemEValue);
        this.updateFormControlValue("itemEFrenchValue", this.token.itemEFrenchValue);
        this.updateFormControlValue("lowerLimit", this.token.lowerLimit);
        this.updateFormControlValue("upperLimit", this.token.upperLimit);
        this.updateFormControlValue("radius", this.token.radius);
        this.updateFormControlValue("requiresItemA", this.token.requiresItemA);
        this.updateFormControlValue("requiresSeries", this.token.requiresSeries);
        this.updateFormControlValue("requiresPurpose", this.token.requiresPurpose);
        this.updateFormControlValue("requiresItemFG", this.token.requiresItemFG);
        this.updateFormControlValue("grpPurposes.purposeB", this.token.purposeB);
        this.updateFormControlValue("grpPurposes.purposeM", this.token.purposeM);
        this.updateFormControlValue("grpPurposes.purposeN", this.token.purposeN);
        this.updateFormControlValue("grpPurposes.purposeO", this.token.purposeO);
        this.updateFormControlValue("grpTraffic.trafficI", this.token.trafficI);
        this.updateFormControlValue("grpTraffic.trafficV", this.token.trafficV);
        this.updateFormControlValue("grpItemFG.itemFValue", this.token.itemFValue);
        this.updateFormControlValue("grpItemFG.itemFSurface", this.token.itemFSurface);
        this.updateFormControlValue("grpItemFG.itemFUnits", this.token.itemFUnits);
        this.updateFormControlValue("grpItemFG.itemGValue", this.token.itemGValue);
        this.updateFormControlValue("grpItemFG.itemGUnlimited", this.token.itemGUnlimited);
        this.updateFormControlValue("grpItemFG.itemGUnits", this.token.itemGUnits);
    };
    CatchAllCancelComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    CatchAllCancelComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/catchall/catchall-cancel.component.html',
            styleUrls: ['app/dashboard/nsds/catchall/catchall-draft.component.css']
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            data_service_1.DataService,
            catchall_service_1.CatchAllService,
            forms_1.FormBuilder,
            windowRef_service_1.WindowRef,
            core_1.ChangeDetectorRef,
            angular_l10n_1.TranslationService])
    ], CatchAllCancelComponent);
    return CatchAllCancelComponent;
}());
exports.CatchAllCancelComponent = CatchAllCancelComponent;
//Custom Validators
function icaoConditionValidator(c) {
    if (c.value === -1) {
        return { 'required': true };
    }
    return null;
}
function itemEMustChangeValidator(itemEValue) {
    return function (control) {
        if (window["app"].isNof) {
            return null;
        }
        var value = null;
        if (control) {
            value = control.value instanceof Object ? control.value.value : control.value;
        }
        return value === itemEValue ? { "mustchange": true } : null;
    };
}
function itemEFrenchMustChangeValidator(itemEFrenchValue) {
    return function (control) {
        if (window["app"].isNof) {
            return null;
        }
        var value = null;
        if (control) {
            value = control.value instanceof Object ? control.value.value : control.value;
        }
        return value === itemEFrenchValue ? { "mustchange": true } : null;
    };
}
//# sourceMappingURL=catchall-cancel.component.js.map