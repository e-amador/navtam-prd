﻿import {Injectable } from '@angular/core';
import {Subject, Observable} from 'rxjs/Rx';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import { WindowRef } from '../../../common/windowRef.service'
//import { ISdoSubject } from './catchall.model';

@Injectable()
export class CatchAllService {

    constructor(private http: Http, winRef: WindowRef) {
        this.app = winRef.nativeWindow['app'];
    }

    public app 

    //getSdoSubject(subId: string): Observable<ISdoSubject>{
    //    return this.http.get(this.app.apiUrl + `sdosubjects/find/?subId=${subId}`).map((response: Response) => {
    //        return response.json();
    //    }).catch(this.handleError);
    //}

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}