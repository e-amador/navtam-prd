﻿import { Component, OnInit, OnDestroy, Inject, Injector, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';

import { ISubject, ISdoSubject, IcaoSubject, IcaoCondition, IFIRSubject, ProposalType, IInDoaValidationResult } from '../../shared/data.model';
import { DataService } from '../../shared/data.service';
import { LocationService } from '../../shared/location.service';

import { CatchAllService } from './catchall.service';
import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';

import { LocaleService, TranslationService } from 'angular-l10n';

declare var $: any;

@Component({
    templateUrl: '/app/dashboard/nsds/catchall/catchall-draft.component.html',
    styleUrls: ['app/dashboard/nsds/catchall/catchall-draft.component.css']
})
export class CatchAllDraftComponent implements OnInit, OnDestroy {
    action: string = "";
    token: any = null;
    proposalType: ProposalType;
    isReadOnly: boolean = false;
    isBilingualRegion: boolean = false;
    overrideBilingual: boolean = false;
    locationInDoa: boolean = true;

    disableIcaoSubjectSelect: boolean = true;
    disableIcaoConditionSelect: boolean = true;

    subjectSelected: ISdoSubject = null;
    icaoSubjectSelected: IcaoSubject = null;
    icaoConditionSelected: IcaoCondition = null;

    subjects: ISdoSubject[];
    icaoSubjects: IcaoSubject[];
    icaoConditions: IcaoCondition[];

    icaoSubjectSelectOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.ICAOSubjectPlaceholder')
        }
    }
    icaoConditionSelectOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.ICAOConditionPlaceholder')
        }
    }

    units: Select2OptionData[];
    scopes: Select2OptionData[];
    series: Select2OptionData[];

    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    nonIcaoAerodrome: boolean = false;
    isFormLoadingData: boolean;
    isTrafficDisabled: boolean = true;
    leafletmap: any;

    lowerLimit: number;
    upperLimit: number;
    minimunRadius: number;
    lowerFLimit: number;
    upperFLimit: number;
    lowerGLimit: number;
    upperGLimit: number;

    firs: IFIRSubject[];

    itemFErrorMessage: string = '';
    itemGErrorMessage: string = '';

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        public dataService: DataService,
        public locationService: LocationService,
        private catchAllService: CatchAllService,
        private fb: FormBuilder,
        private changeDetectionRef: ChangeDetectorRef,
        public translation: TranslationService) {
        this.token = this.injector.get('token');
        this.proposalType = this.injector.get('proposalType');
        this.nsdForm = this.injector.get('form');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.action = this.injector.get('type');
        this.overrideBilingual = this.token.overrideBilingual;
    }

    ngOnInit() {
        const app = this.catchAllService.app;

        this.leafletmap = app.leafletmap;

        this.isFormLoadingData = !(!this.token.subjectId);

        this.dataService.getFirs()
            .subscribe((firs: IFIRSubject[]) => {
                this.firs = firs;
            });

        this.createUnitsListBox();
        this.createSeriesListBox();

        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        } else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }

        this.nsdFrmGroup.get('grpItemFG.itemFSurface')
            .valueChanges.subscribe(value => this.itemFSurfaceChanged(value));
        this.nsdFrmGroup.get('grpItemFG.itemGUnlimited')
            .valueChanges.subscribe(value => this.itemGUnlimitedChanged(value));

        this.nsdFrmGroup.get('subjectLocation')
            .valueChanges.subscribe(value => this.subjectLocationChanged(value));

        this.nsdFrmGroup.get('latitude')
            .valueChanges.subscribe(value => this.latitudeLongitudeChanged(value));
        this.nsdFrmGroup.get('longitude')
            .valueChanges.subscribe(value => this.latitudeLongitudeChanged(value));

        if (this.isFormLoadingData) {
            this.loadSubject(this.token.subjectId);
        }

        this.loadMapLocations(app.doaId, this.isFormLoadingData);
    }

    ngOnDestroy() {
        /* There is an issue when a grouped proposal change item A. Map was null because of following code  */
        //this.leafletmap.dispose();
        //this.leafletmap = null;
    }

    ngAfterViewInit() {
        this.DisableItemFG(this.nsdFrmGroup);
    }

    requireLowerUpperLimit(): boolean {
        return this.nsdFrmGroup.get('icaoConditionId').value != -1 &&
            (!this.nsdFrmGroup.get('requiresItemFG').value ||
             this.nsdFrmGroup.get('grpItemFG.itemFUnits').value === 2 ||
             this.nsdFrmGroup.get('grpItemFG.itemGUnits').value === 2);
    }

    requireItemFG(): boolean {
        return this.nsdFrmGroup.get('requiresItemFG').value;
    }

    DisableItemFG(grp: any) {
        if (grp.controls.grpItemFG.controls.itemGUnlimited.value) {
            this.nsdFrmGroup.get('grpItemFG.itemGValue').disable();
        }
        if (grp.controls.grpItemFG.controls.itemFSurface.value) {
            this.nsdFrmGroup.get('grpItemFG.itemFValue').disable();
        }
    }

    icaoSubjectChanged(e: any): void {
        if (e.value) {
            const subjectId = this.isFormLoadingData ? this.nsdFrmGroup.get('icaoSubjectId').value : +e.value;

            if (subjectId !== -1) {
                if (this.icaoSubjectSelected && this.icaoSubjectSelected.id === subjectId) {
                    return; //when multiples change events occurring react only to the first one
                }
                this.changeDetectionRef.detectChanges();

                if (!this.isFormLoadingData) {
                    this.nsdFrmGroup.patchValue({
                        icaoSubjectId: subjectId,
                        icaoConditionId: -1
                    });
                    this.nsdFrmGroup.get("icaoSubjectId").markAsDirty();
                    this.resetIcaoSubjectChildrenControls();
                }

                this.loadIcaoConditions(subjectId);
                this.disableIcaoConditionSelect = false;

                this.icaoSubjectSelected = this.icaoSubjects.filter(item => item.id === subjectId)[0];
                if (this.icaoSubjectSelected) {
                    if (this.icaoSubjectSelected.requiresItemA) {
                        this.nsdFrmGroup.get('itemA').setValidators(itemAValidator(this.subjectSelected.designator, this));
                    } else {
                        this.nsdFrmGroup.get('itemA').clearValidators();
                    }

                    if (!this.isFormLoadingData) {
                        this.nsdFrmGroup.patchValue({
                            requiresSeries: this.icaoSubjectSelected.requiresSeries,
                            requiresItemA: this.icaoSubjectSelected.requiresItemA,
                            requiresScope: this.icaoSubjectSelected.requiresScope,
                        });

                        this.nsdFrmGroup.patchValue({ itemA: this.formatDesignator(this.subjectSelected.designator) });

                        const fc = this.nsdFrmGroup.get('scope');
                        if (this.icaoSubjectSelected.requiresScope) {
                            this.nsdFrmGroup.patchValue({ scope: this.icaoSubjectSelected.scope });
                            if (this.scopes) {
                                const scope = this.scopes.filter(item => item.id === this.icaoSubjectSelected.scope);
                                if (scope.length === 1) {
                                    fc.setValue(scope[0].id);
                                    fc.markAsDirty();
                                }
                            }
                        }
                        else {
                            this.token.scope = null;
                            fc.setValue(null);
                            fc.markAsDirty();
                        }
                    }

                    const seriesCtrl = this.nsdFrmGroup.get('series');
                    if (this.icaoSubjectSelected.requiresSeries) {
                        this.loadSeries(this.icaoSubjectSelected.code);
                        if (!this.isFormLoadingData) {
                            this.calculateSeries(this.icaoSubjectSelected.code, seriesCtrl);
                        }
                    } else {
                        this.token.series = null;
                        seriesCtrl.setValue(null);
                        seriesCtrl.markAsDirty;
                    }
                }
            } else {
                this.disableIcaoConditionSelect = true;
                if (!this.isFormLoadingData) {
                    this.resetIcaoSubjectChildrenControls();
                    this.nsdFrmGroup.markAsDirty();
                }
            }
        }
    }

    icaoConditionChanged(e: any): void {
        if (e.value) {
            let conditionId = +e.value;

            const icaoConditionIdCtrl = this.nsdFrmGroup.get('icaoConditionId');
            if (this.isFormLoadingData) {
                conditionId = +icaoConditionIdCtrl.value;
            } else {
                if (conditionId !== icaoConditionIdCtrl.value) {
                    icaoConditionIdCtrl.setValue(conditionId);
                    icaoConditionIdCtrl.markAsDirty();
                }
            }

            if (conditionId === -1) {
                this.resetIcaoConditionChildrenControls();
            } else {
                if (this.icaoConditionSelected && this.icaoConditionSelected.id === conditionId) {
                    return; //when multiples change events occurring react only to the first one
                }

                this.icaoConditionSelected = this.icaoConditions.filter(item => item.id === conditionId)[0];
                if (this.icaoConditionSelected) {
                    this.changeDetectionRef.detectChanges();

                    const grpTrafficCtrl = this.nsdFrmGroup.get('grpTraffic');
                    grpTrafficCtrl.setValidators(trafficValidator);

                    const grpItemFGCtrl = this.nsdFrmGroup.get('grpItemFG');
                    if (this.icaoConditionSelected.requiresItemFG) {
                        grpItemFGCtrl.setValidators([itemFValidator(this), itemGValidator(this)]);
                    } else {
                        grpItemFGCtrl.clearValidators();

                        this.token.itemFValue = 0;
                        this.token.itemFUnits = 0;
                        this.token.itemFSurface = false;
                        this.token.itemGValue = 1;
                        this.token.itemGUnits = 0;
                        this.token.itemGUnlimited = false;
                    }

                    const grpPurposesCtrl = this.nsdFrmGroup.get('grpPurposes');
                    if (this.icaoConditionSelected.requiresPurpose) {
                        grpPurposesCtrl.setValidators(purposeAllowValidator);
                    } else {
                        grpPurposesCtrl.clearValidators();
                    }

                    this.isTrafficDisabled = !(this.icaoConditionSelected.i && this.icaoConditionSelected.v);

                    this.lowerLimit = this.icaoConditionSelected.lower;
                    this.upperLimit = this.icaoConditionSelected.upper;

                    this.lowerFLimit = this.icaoConditionSelected.lower;
                    this.upperFLimit = this.icaoConditionSelected.upper - 1;
                    if (this.upperFLimit > 998) this.upperFLimit = 998; 
                    this.lowerGLimit = this.icaoConditionSelected.lower + 1;
                    this.upperGLimit = this.icaoConditionSelected.upper;
                    if (this.upperGLimit > 999) this.upperGLimit = 999; 

                    this.minimunRadius = this.icaoConditionSelected.radius;

                    if (this.minimunRadius == 999) {
                        //TODO: Create a new column in the ICAOSubjectConditions to register the defaulkt value and the minimiun value
                        this.minimunRadius = 1
                    }

                    const radiusCtrl = this.nsdFrmGroup.get('radius');
                    radiusCtrl.clearValidators();
                    radiusCtrl.setValidators(radiusValidator(this.minimunRadius));

                    if (this.requireLowerUpperLimit()) {
                        let ctrl = this.nsdFrmGroup.get('lowerLimit');
                        ctrl.clearValidators();
                        ctrl.setErrors(null);
                        ctrl = this.nsdFrmGroup.get('upperLimit');
                        ctrl.clearValidators();
                        ctrl.setErrors(null);
                    } else {
                        this.nsdFrmGroup.get("lowerLimit").setValidators(lowerLimitValidator(this.nsdFrmGroup.get('upperLimit')));
                        this.nsdFrmGroup.get("upperLimit").setValidators(upperLimitValidator(this.nsdFrmGroup.get('lowerLimit')));
                    }

                    if (!this.isFormLoadingData) {
                        this.nsdFrmGroup.patchValue({
                            requiresItemFG: this.icaoConditionSelected.requiresItemFG,
                            requiresPurpose: this.icaoConditionSelected.requiresPurpose,
                            requiresTraffic: this.icaoConditionSelected.i || this.icaoConditionSelected.v,
                            grpItemFG: {
                                itemFValue: 0, //Limit is 0 - 998
                                itemFSurface: this.token.itemFSurface,
                                itemFUnits: this.token.itemFUnits,
                                itemGValue: (this.token.itemGUnlimited ? this.upperGLimit : 1), //Modified by Luis to put the lowest limit in ItemG as 1 (1 - 999)
                                itemGUnlimited: this.token.itemGUnlimited,
                                itemGUnits: this.token.itemGUnits,
                            },
                            grpPurposes: {
                                purposeB: this.icaoConditionSelected.b,
                                purposeM: this.icaoConditionSelected.m,
                                purposeN: this.icaoConditionSelected.n,
                                purposeO: this.icaoConditionSelected.o,
                            },
                            radius: this.icaoConditionSelected.radius,
                            lowerLimit: this.icaoConditionSelected.lower,
                            upperLimit: this.icaoConditionSelected.upper,
                            grpTraffic: {
                                trafficI: this.icaoConditionSelected.i,
                                trafficV: this.icaoConditionSelected.v
                            }
                        });
                    }
                    this.isFormLoadingData = false;
                }
            }
        } else {
        }
    }

    scopeChanged(e: any): void {
        if (!this.isFormLoadingData) {
            const fc = this.nsdFrmGroup.get('scope');
            if (fc.value !== e.value) {
                fc.setValue(e.value);
                fc.markAsDirty();
            }
        }
    }

    seriesChanged(e: any): void {
        if (!this.isFormLoadingData) {
            const fc = this.nsdFrmGroup.get('series');
            if (fc.value !== e.value) {
                fc.setValue(e.value);
                fc.markAsDirty();
            }
        }
    }

    checkboxChanged(controlName: string, checked: boolean) {
        const control = this.nsdFrmGroup.get(controlName);
        control.setValue(checked);
        control.markAsDirty();
    }

    decValue(fieldName) {
        if (this.isReadOnly) return;

        let value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                if (this.icaoConditionSelected) {
                    if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                        value = +this.nsdFrmGroup.get('lowerLimit').value;
                        if (value > 0) {
                            this.nsdFrmGroup.get('lowerLimit').setValue(value - 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('lowerLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'upperLimit':
                if (this.icaoConditionSelected) {
                    if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                        value = +this.nsdFrmGroup.get('upperLimit').value;
                        if (value > 0) {
                            this.nsdFrmGroup.get('upperLimit').setValue(value - 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'radius':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('radius').value;
                    if (value > this.minimunRadius) {
                        this.nsdFrmGroup.get('radius').setValue(value - 1);
                        this.nsdFrmGroup.get('radius').markAsDirty()
                    }
                }
                break;
            case 'itemF':
                if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemFValue').value;
                    const units = +this.nsdFrmGroup.get('grpItemFG.itemFUnits').value;
                    if (value > (units === 0 ? this.lowerFLimit : this.lowerFLimit * 100)) {
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').setValue(value - 1);
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').markAsDirty();
                    }
                }
                break;
            case 'itemG':
                if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemGValue').value;
                    const units = +this.nsdFrmGroup.get('grpItemFG.itemGUnits').value;
                    if (value > (units === 0 ? this.lowerGLimit : this.lowerGLimit * 100)) {
                        if (value > 2) {
                            this.nsdFrmGroup.get('grpItemFG.itemGValue').setValue(value - 1);
                            this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                            this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                            this.nsdFrmGroup.get('grpItemFG.itemGValue').markAsDirty();
                        }
                    }
                }
                break;
        }
    }

    incValue(fieldName) {
        if (this.isReadOnly) return;

        let value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('lowerLimit').value;
                    if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                        if (value < this.upperLimit) {
                            this.nsdFrmGroup.get('lowerLimit').setValue(value + 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('lowerLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'upperLimit':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('upperLimit').value;
                    if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                        if (value < this.upperLimit) {
                            this.nsdFrmGroup.get('upperLimit').setValue(value + 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'radius':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('radius').value;
                    if (value < 999) {
                        this.nsdFrmGroup.get('radius').setValue(value + 1);
                        this.nsdFrmGroup.get('radius').markAsDirty();
                    }
                }
                break;
            case 'itemF':
                if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemFValue').value;
                    const units = +this.nsdFrmGroup.get('grpItemFG.itemFUnits').value;
                    let limit = (units === 0 ? this.upperFLimit : (this.upperFLimit * 100) + 99);
                    if (value < limit) {
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').setValue(value + 1);
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').markAsDirty();
                    }
                }
                break;
            case 'itemG':
                if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemGValue').value;
                    const units = +this.nsdFrmGroup.get('grpItemFG.itemGUnits').value;
                    let limit = (units === 0 ? this.upperGLimit : (this.upperGLimit * 100) + 99);
                    if (value < limit) {
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').setValue(value + 1);
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').markAsDirty();
                    }
                }
                break;
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    validateSubjectId() {
        const fc = this.nsdFrmGroup.get('subjectId');

        return fc.value || this.nsdFrmGroup.pristine;
    }

    validateIcaoSubjectId() {
        const fc = this.nsdFrmGroup.get('icaoSubjectId');
        return fc.value !== -1 || this.nsdFrmGroup.pristine;
    }

    validateIcaoConditionId() {
        const fc = this.nsdFrmGroup.get('icaoConditionId');
        return fc.value !== -1 || this.nsdFrmGroup.pristine;
    }

    validateSubjectLocation() {
        const fc = this.nsdFrmGroup.get('subjectLocation');
        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    validateSubjectLatitude() {
        const fc = this.nsdFrmGroup.get('latitude');
        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    validateSubjectLongitude() {
        const fc = this.nsdFrmGroup.get('longitude');
        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    validateAerodromeName() {
        const fc = this.nsdFrmGroup.get('aerodromeName');

        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    validateRadius() {
        const fc = this.nsdFrmGroup.get('radius');
        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    validateLowerLimit() {
        const fc = this.nsdFrmGroup.get('lowerLimit');

        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    validateUpperLimit() {
        const fc = this.nsdFrmGroup.get('upperLimit');
        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    validateItemE() {
        const fc = this.nsdFrmGroup.get('itemEValue');

        return fc.value || this.nsdFrmGroup.pristine;
    }

    validateItemEFrench() {
        const fc = this.nsdFrmGroup.get('itemEFrenchValue');
        return fc.value || this.nsdFrmGroup.pristine;
    }

    select2Options(): any {
        const self = this;
        const app = this.catchAllService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/catchallfilter",
                dataType: 'json',
                delay: 500,
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }

                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }

    private loadSubject(subjectId: string) {
        this.dataService.getSdoSubject(subjectId)
            .subscribe((sdoSubjectResult: ISdoSubject) => {
                this.subjectSelected = sdoSubjectResult;
                this.subjects = [];
                this.subjects.push(sdoSubjectResult);

                this.nonIcaoAerodrome = this.designatorHasNumbers(sdoSubjectResult.designator);
                if (this.nonIcaoAerodrome) {
                    const aerodromeCtrl = this.nsdFrmGroup.get('aerodromeName');

                    if (!aerodromeCtrl.value) {
                        aerodromeCtrl.markAsDirty();
                    }
                    aerodromeCtrl.setValidators(Validators.required);
                }
                
                //this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                this.loadIcaoSubjects(sdoSubjectResult.sdoEntityName);
            });
    }

    private loadSeries(qCode: string): void {
        this.dataService.getSeries()
            .subscribe((series: Array<Select2OptionData>) => {
                this.setFirstSelectItemInCombo(series, this.token.series, false);
                this.series = series;
            });
    }

    private calculateSeries(qCode: string, fc: AbstractControl) {
        if (qCode && this.subjectSelected) {
            this.dataService.getSerie(this.subjectSelected.id, qCode)
                .subscribe((serie: string) => {
                    this.nsdFrmGroup.patchValue({ series: serie });
                    if (this.series) {
                        const series = this.series.filter(item => item.id === serie);
                        if (series.length === 1) {
                            if (fc.value !== series[0].id) {
                                fc.setValue(series[0].id);
                                fc.markAsDirty();
                            }
                        }
                    }
                });
        }
    }

    private loadMapLocations(doaId: number, loadingFromProposal: boolean) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leafletmap && self.leafletmap.isReady()) {
                        window.clearInterval(interval);
                        self.leafletmap.addDoa(geoJsonStr, 'DOA');
                        if (loadingFromProposal) {
                            self.updateMap(self.subjectSelected, false);
                        }
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error(this.translation.translate('Nsd.DOAGeoError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private updateSubjectSelected(sdoSubject: ISdoSubject) {
        this.createScopesListBox(sdoSubject.sdoEntityName);

        this.subjectSelected = sdoSubject;
        const subjectId = this.nsdFrmGroup.get('subjectId').value;
        if (subjectId === sdoSubject.id) {
            //for some weird reasons select2 templateSelection is called multiples
            //times after selection
            return;
        }

        if (!this.isReadOnly) {
            const itemACtrl = this.nsdFrmGroup.get('itemA');
            if (sdoSubject.sdoEntityName === "Ahp") {
                itemACtrl.disable(); //disable ItemA when Aerodrome
            } else {
                itemACtrl.enable();
            }
        }

        if (sdoSubject.id !== subjectId) {
            const subjectCtrl = this.nsdFrmGroup.get('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }

        this.icaoSubjects = null;
        this.icaoSubjectSelected = null;
        this.icaoConditionSelected = null;

        this.nonIcaoAerodrome = this.designatorHasNumbers(this.subjectSelected.designator);

        const aerodromeCtrl = this.nsdFrmGroup.get('aerodromeName');
        if (this.nonIcaoAerodrome) {
            aerodromeCtrl.setValidators(Validators.required);
        } else {
            aerodromeCtrl.clearValidators();
        }

        this.nsdFrmGroup.patchValue({
            icaoSubjectId: -1,
            icaoConditionId: -1,
            itemA: this.nonIcaoAerodrome ? "CXXX" : this.subjectSelected.designator,
            aerodromeName: this.nonIcaoAerodrome ? this.subjectSelected.name : null
        });

        this.disableIcaoSubjectSelect = true;
        this.disableIcaoConditionSelect = true;

        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe((sdoSubjectResult: ISdoSubject) => {
                    this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                    this.updateMap(sdoSubjectResult, true);
                });
        }
        this.loadIcaoSubjects(sdoSubject.sdoEntityName);
    }

    private loadIcaoSubjects(sdoEntityName: string) {
        if (sdoEntityName) {
            this.dataService.getIcaoSubjects(sdoEntityName)
                .subscribe((icaoSubjects: IcaoSubject[]) => {
                    this.disableIcaoSubjectSelect = false;

                    this.setFirstSelectItemInCombo(icaoSubjects, this.token.icaoSubjectId);

                    this.icaoSubjects = icaoSubjects.map(
                        function (item: IcaoSubject) {
                            if (item.code !== undefined) {
                                if (item.name.length > 90) {
                                    item.text = item.code + ' - ' + item.name.substr(0, 80) + '...';
                                } else {
                                    item.text = item.code + ' - ' + item.name;
                                }
                            }
                            return item;
                        })

                    if (!this.isFormLoadingData) {
                        this.resetIcaoSubjectChildrenControls();
                    }
                });
        }
    }

    private loadIcaoConditions(icaoSubjectId: number) {
        if (icaoSubjectId) {

            this.dataService.getIcaoConditions(icaoSubjectId, this.proposalType === ProposalType.Cancel)
                .subscribe((icaoConditions: IcaoCondition[]) => {
                    this.setFirstSelectItemInCombo(icaoConditions, this.token.icaoConditionId);
                    this.icaoConditions = icaoConditions.map(
                        function (item: IcaoCondition) {
                            if (item.code !== undefined) {
                                if (item.description.length > 90) {
                                    item.text = item.code + ' - ' + item.description.substr(0, 80) + '...';
                                } else {
                                    item.text = item.code + ' - ' + item.description;
                                }
                            }
                            return item;
                        });
                });
        }
    }

    private updateMap(sdoSubject: ISubject, acceptSubjectLocation: boolean) {
        if (acceptSubjectLocation && sdoSubject && sdoSubject.subjectGeoRefPoint) {
            const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
            const coordinates = geojson.features[0].geometry.coordinates;
            this.nsdFrmGroup.patchValue({
                subjectLocation: subLocation,
                latitude: coordinates[1],
                longitude: coordinates[0],
            }, { onlySelf: true, emitEvent: false });
            if (this.token) {
                this.token.subjectLocation = subLocation;
            }
            const location = this.locationService.parse(this.token.subjectLocation);
            this.locationInDoa = true;
            this.updateSubjectLocationInMap(location.latitude, location.longitude);
            this.setBilingualRegion(sdoSubject.isBilingualRegion);

        } else if (this.token && this.token.subjectLocation) {
            const location = this.locationService.parse(this.token.subjectLocation);
            this.dataService.validateLocation(this.token.subjectLocation)
                .subscribe((result: IInDoaValidationResult) => {
                    this.locationInDoa = result.inDoa;
                    if (this.locationInDoa) {
                        this.updateSubjectLocationInMap(location.latitude, location.longitude);
                        this.setBilingualRegion(result.inBilingualRegion);
                    }
                });

            //this.updateSubjectLocationInMap(this.token.latitude, this.token.longitude);
        } else {
            this.leafletmap.clearFeaturesOnLayers(['DOA']);
            if (sdoSubject && sdoSubject.subjectGeoRefPoint) {
                const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
                if (geojson.features.length === 1) {
                    const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                    const coordinates = geojson.features[0].geometry.coordinates;
                    const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                    if (acceptSubjectLocation) {
                        this.nsdFrmGroup.patchValue({
                            subjectLocation: subLocation,
                            latitude: coordinates[1],
                            longitude: coordinates[0],
                        }, { onlySelf: true, emitEvent: false });
                    }

                    this.dataService.validateLocation(subLocation)
                        .subscribe((result: IInDoaValidationResult) => {
                            this.locationInDoa = result.inDoa;
                            if (this.locationInDoa) {
                                this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                                });
                            }
                        });



                    //this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    //});
                }
            }
        }
    }

    private updateSubjectLocationInMap(latitude: number, longitude: number) {
        if (this.leafletmap) {
            const geojson = this.leafletmap.latitudeLongitudeToGeoJson(latitude, longitude);
            this.leafletmap.setNewMarkerLocation(geojson);
        }
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }

    private setFirstSelectItemInCombo(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        if (this.isFormLoadingData) {
            const index = items.findIndex(x => x.id === itemId);
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        } else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                });
            }
        }
    }

    private createUnitsListBox(): void {
        this.units = [
            { id: '0', text: 'FL' },
            { id: '1', text: 'AMSL' },
            { id: '2', text: 'AGL' },
        ];
    }

    private createScopesListBox(entityName: string): void {
        this.scopes = [
            { id: 'A', text: 'A' },
            { id: 'E', text: 'E' },
            { id: 'W', text: 'W' },
            { id: 'AE', text: 'AE' },
            { id: 'AW', text: 'AW' },
            { id: 'K', text: 'K' }];
    }

    private createSeriesListBox(): void {
        this.dataService.getSeries()
            .subscribe((series: Array<Select2OptionData>) => {
                this.series = series;
            });
    }

    private resetIcaoSubjectChildrenControls() {
        this.nsdFrmGroup.patchValue({
            series: null,
            scope: null,
            itemA: null,
            lowerLimit: 0,
            upperLimit: 0,
            radius: 0,
            grpTraffic: {
                trafficI: false,
                trafficV: false,
            },
        });
        this.isTrafficDisabled = true;
    }

    private resetIcaoConditionChildrenControls() {
        if (!this.isFormLoadingData) {
            this.nsdFrmGroup.patchValue({
                requiresItemFG: false,
                requiresPurpose: false,
                grpPurposes: {
                    purposeB: false,
                    purposeM: false,
                    purposeN: false,
                    purposeO: false,
                },
                grpItemFG: {
                    itemFValue: 0,
                    itemFSurface: false,
                    itemFUnits: 0,
                    itemGValue: 1,
                    itemGUnlimited: false,
                    itemGUnits: 0
                }
            });
        }
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];

        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, [Validators.required]],
            icaoSubjectId: [this.token.icaoSubjectId || -1, [Validators.required]],
            icaoConditionId: [this.token.icaoConditionId || -1, [icaoConditionValidator]],
            subjectLocation: [{ value: this.token.subjectLocation, disabled: this.isReadOnly }, [Validators.required, validateLocation(this.locationService)]],
            latitude: [{ value: this.token.latitude, disabled: this.isReadOnly }, [Validators.required, validateLatitude(this.locationService)]],
            longitude: [{ value: this.token.longitude, disabled: this.isReadOnly }, [Validators.required, validateLongitude(this.locationService)]],
            scope: this.token.scope,
            series: this.token.series,
            itemA: { value: this.token.itemA, disabled: this.isReadOnly },
            aerodromeName: { value: this.token.aerodromeName, disabled: this.isReadOnly },
            itemEValue: [{ value: this.token.itemEValue, disabled: this.isReadOnly }, [Validators.required]],
            itemEFrenchValue: [{ value: this.token.itemEFrenchValue, disabled: this.isReadOnly }],
            grpPurposes: this.fb.group({
                purposeB: this.token.purposeB,
                purposeM: this.token.purposeM,
                purposeN: this.token.purposeN,
                purposeO: this.token.purposeO,
            }),
            lowerLimit: [{ value: this.token.lowerLimit, disabled: this.isReadOnly }, []], 
            upperLimit: [{ value: this.token.upperLimit, disabled: this.isReadOnly }, []], 
            radius: [{ value: this.token.radius, disabled: this.isReadOnly }, []],
            grpTraffic: this.fb.group({
                trafficI: this.token.trafficI,
                trafficV: this.token.trafficV,
            }),
            grpItemFG: this.fb.group({
                itemFValue: { value: this.token.itemFValue || 0, disabled: this.isReadOnly },
                itemFSurface: { value: this.token.itemFSurface, disabled: this.isReadOnly },
                itemFUnits: this.token.itemFUnits,
                itemGValue: { value: this.token.itemGValue || 1, disabled: this.isReadOnly },
                itemGUnlimited: { value: this.token.itemGUnlimited, disabled: this.isReadOnly },
                itemGUnits: this.token.itemGUnits,
            }),
            requiresItemA: this.token.requiresItemA,
            requiresSeries: this.token.requiresSeries,
            requiresScope: this.token.requiresScope,
            requiresPurpose: this.token.requiresPurpose,
            requiresItemFG: this.token.requiresItemFG
        });

        this.nsdFrmGroup.get("lowerLimit").setValidators(lowerLimitValidator(this.nsdFrmGroup.get('upperLimit')));
        this.nsdFrmGroup.get("upperLimit").setValidators(upperLimitValidator(this.nsdFrmGroup.get('lowerLimit')));

        if (this.isFormLoadingData) {
            if (this.token.requiresScope && this.token.scope) {
                this.createScopesListBox(this.token.subjectId.substring(0, 3));
                const scope = this.scopes.filter(item => item.id === this.token.scope);
                if (this.scopes) {
                    if (scope.length === 1) {
                        const fc = this.nsdFrmGroup.get('scope');
                        fc.setValue(scope[0].id);
                    }
                }
            }
            if (this.token.requiresSeries && this.token.series) {
                if (this.series) {
                    const series = this.series.filter(item => item.id === this.token.series);
                    if (series.length === 1) {
                        const fc = this.nsdFrmGroup.get('series');
                        fc.setValue(series[0].id);
                    }
                }
            }
        }

        //this.$ctrl("aerodromeName").valueChanges.debounceTime(1000).subscribe(() => this.updateIcao());

        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];

        this.nsdFrmGroup.get("aerodromeName").clearValidators();
        this.nsdFrmGroup.get("itemA").clearValidators();
        this.nsdFrmGroup.get('grpTraffic').clearValidators();
        this.nsdFrmGroup.get("lowerLimit").clearValidators();
        this.nsdFrmGroup.get("upperLimit").clearValidators();
        this.nsdFrmGroup.get('grpPurposes').clearValidators();
        this.nsdFrmGroup.get('grpItemFG').clearValidators();

        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("icaoSubjectId", this.token.icaoSubjectId);
        this.updateFormControlValue("icaoConditionId", this.token.icaoConditionId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("latitude", this.token.latitude);
        this.updateFormControlValue("longitude", this.token.longitude);
        this.updateFormControlValue("scope", this.token.scope);
        this.updateFormControlValue("series", this.token.series);
        this.updateFormControlValue("itemA", this.token.itemA);
        this.updateFormControlValue("aerodromeName", this.token.aerodromeName);
        this.updateFormControlValue("itemEValue", this.token.itemEValue);
        this.updateFormControlValue("itemEFrenchValue", this.token.itemEFrenchValue);
        this.updateFormControlValue("lowerLimit", this.token.lowerLimit);
        this.updateFormControlValue("upperLimit", this.token.upperLimit);
        this.updateFormControlValue("radius", this.token.radius);
        this.updateFormControlValue("requiresItemA", this.token.requiresItemA);
        this.updateFormControlValue("requiresSeries", this.token.requiresSeries);
        this.updateFormControlValue("requiresPurpose", this.token.requiresPurpose);
        this.updateFormControlValue("requiresItemFG", this.token.requiresItemFG);
        this.updateFormControlValue("grpPurposes.purposeB", this.token.purposeB);
        this.updateFormControlValue("grpPurposes.purposeM", this.token.purposeM);
        this.updateFormControlValue("grpPurposes.purposeN", this.token.purposeN);
        this.updateFormControlValue("grpPurposes.purposeO", this.token.purposeO);
        this.updateFormControlValue("grpTraffic.trafficI", this.token.trafficI);
        this.updateFormControlValue("grpTraffic.trafficV", this.token.trafficV);
        this.updateFormControlValue("grpItemFG.itemFValue", this.token.itemFValue);
        this.updateFormControlValue("grpItemFG.itemFSurface", this.token.itemFSurface);
        this.updateFormControlValue("grpItemFG.itemFUnits", this.token.itemFUnits);
        this.updateFormControlValue("grpItemFG.itemGValue", this.token.itemGValue);
        this.updateFormControlValue("grpItemFG.itemGUnlimited", this.token.itemGUnlimited);
        this.updateFormControlValue("grpItemFG.itemGUnits", this.token.itemGUnits);
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    private formatDesignator(designator: string): string {
        let hasNumbers = false;
        for (let i = 0; i < designator.length; i++) {
            if (parseFloat(designator[i])) {
                hasNumbers = true;
            }
        }

        return hasNumbers ? "CXXX" : designator;
    }

    private designatorHasNumbers(designator: string) {
        for (let i = 0; i < designator.length; i++) {
            if (parseFloat(designator[i])) {
                return true;
            }
        }
        return false;
    }

    private setBilingualRegion(value: boolean) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        let itemEFCtrl = this.nsdFrmGroup.get('itemEFrenchValue');
        if (this.isBilingualRegion || this.overrideBilingual) {
            itemEFCtrl.setValidators(Validators.required);
            if (!this.isReadOnly) {
                itemEFCtrl.enable();
            }
        } else {
            itemEFCtrl.clearValidators();
            if (!this.isReadOnly) {
                itemEFCtrl.disable();
            }
        }
    }

    private isAGLUnitsInUse(): boolean {
        if (+this.nsdFrmGroup.get('grpItemFG.itemFUnits').value === 2) return true;
        if (+this.nsdFrmGroup.get('grpItemFG.itemGUnits').value === 2) return true;
        return false;
    }

    itemFUnitChanged(e: any): void {
        this.nsdFrmGroup.get('grpItemFG.itemFUnits').setValue(+e.value)
        if (this.isAGLUnitsInUse()) {
            this.nsdFrmGroup.get('lowerLimit').patchValue(this.lowerLimit);
            if (this.nsdFrmGroup.get('grpItemFG.itemFSurface').value === true)
                this.nsdFrmGroup.get('lowerLimit').disable();
            else this.nsdFrmGroup.get('lowerLimit').enable();
            if (this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value === true) { 
                this.nsdFrmGroup.get('upperLimit').patchValue(this.upperLimit);
                this.nsdFrmGroup.get('upperLimit').disable();
            }
            else this.nsdFrmGroup.get('upperLimit').enable();
        }
    }

    itemFSurfaceChanged(checked: boolean) {
        if (checked) {
            this.nsdFrmGroup.patchValue({
                grpItemFG: {
                    itemFUnits: +this.units[0].id,
                    itemFValue: 0
                }
            });
            this.nsdFrmGroup.get('grpItemFG.itemFValue').disable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('lowerLimit').patchValue(this.lowerLimit);
                this.nsdFrmGroup.get('lowerLimit').disable();
            }
        }
        else {
            this.nsdFrmGroup.get('grpItemFG.itemFValue').enable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('lowerLimit').enable();
            }
        }
    }

    public isLowerLimitDisabled(): boolean {
        if (this.nsdFrmGroup.get('lowerLimit').disabled) return true;
        if (this.isAGLUnitsInUse() && this.nsdFrmGroup.get('grpItemFG.itemFSurface').value === true) return true;
        return false;
    }

    itemGUnitChanged(e: any): void {
        this.nsdFrmGroup.get('grpItemFG.itemGUnits').setValue(+e.value);
        if (this.isAGLUnitsInUse()) {
            this.nsdFrmGroup.get('upperLimit').patchValue(this.upperLimit);
            if (this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value === true)
                this.nsdFrmGroup.get('upperLimit').disable();
            else this.nsdFrmGroup.get('upperLimit').enable();
            if (this.nsdFrmGroup.get('grpItemFG.itemFSurface').value === true) {
                this.nsdFrmGroup.get('lowerLimit').patchValue(this.lowerLimit);
                this.nsdFrmGroup.get('lowerLimit').disable();
            }
            else this.nsdFrmGroup.get('lowerLimit').enable();
        }

    }

    public isUpperLimitDisabled(): boolean {
        if (this.nsdFrmGroup.get('upperLimit').disabled) return true;
        if (this.isAGLUnitsInUse() && this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value === true) return true;
        return false;
    }

    private itemGUnlimitedChanged(checked: boolean) {
        if (checked) {
            this.nsdFrmGroup.patchValue({
                grpItemFG: {
                    itemGUnits: +this.units[0].id,
                    itemGValue: this.upperGLimit
                }
            });
            this.nsdFrmGroup.get('grpItemFG.itemGValue').disable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('upperLimit').patchValue(this.upperLimit);
                this.nsdFrmGroup.get('upperLimit').disable();
            }
        }
        else {
            this.nsdFrmGroup.get('grpItemFG.itemGValue').enable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('upperLimit').enable();
            }
        }
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    private subjectLocationChanged(value: string) {
        const location = this.locationService.parse(value);
        if (location) {
            this.dataService.validateLocation(value)
                .subscribe((result: IInDoaValidationResult) => {
                    this.locationInDoa = result.inDoa;
                    if (this.locationInDoa) {
                        const options = { onlySelf: true, emitEvent: false };
                        this.nsdFrmGroup.patchValue(location, options);
                        this.updateSubjectLocationInMap(location.latitude, location.longitude);
                        this.setBilingualRegion(result.inBilingualRegion);
                    }
                });
        }
    }

    private latitudeLongitudeChanged(value: string) {
        const latCtrl = this.$ctrl('latitude');
        const lngCtrl = this.$ctrl('longitude');
        if (!latCtrl.errors && !lngCtrl.errors) {
            const latitude = latCtrl.value;
            const longitude = lngCtrl.value;
            this.dataService.validateLocation(latitude + " " + longitude)
                .subscribe((result: IInDoaValidationResult) => {
                    this.locationInDoa = result.inDoa;
                    if (this.locationInDoa) {
                        const options = { onlySelf: true, emitEvent: false };
                        this.nsdFrmGroup.patchValue({ subjectLocation: this.locationService.convertToDMS(latitude, longitude) }, options);
                        this.updateSubjectLocationInMap(latitude, longitude);
                        this.setBilingualRegion(result.inBilingualRegion);
                    }
                });


            /*
            const latitude = latCtrl.value;
            const longitude = lngCtrl.value;
            const options = { onlySelf: true, emitEvent: false };
            this.nsdFrmGroup.patchValue({ subjectLocation: this.locationService.convertToDMS(latitude, longitude) }, options);
            this.updateSubjectLocationInMap(latitude, longitude);
            */
        }
    }

    public itemFErrors(): boolean {
        if (this.nsdFrmGroup.get('grpItemFG').errors) {
            if (this.nsdFrmGroup.get('grpItemFG').errors.ItemFMinValue) {
                this.itemFErrorMessage = this.translation.translate('ErrorMsgs.ItemFMinInvalid');
                return true
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.invalidFValue) {
                this.itemFErrorMessage = this.translation.translate('ErrorMsgs.ItemFInputInvalid');
                return true;
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.range) {
                this.itemFErrorMessage = this.translation.translate('ErrorMsgs.ItemFNotSmallerItemG');
                return true;
            }
        }
        this.itemFErrorMessage = '';
        return false;
    }

    public itemGErrors(): boolean {
        if (this.nsdFrmGroup.get('grpItemFG').errors) {
            if (this.nsdFrmGroup.get('grpItemFG').errors.ItemGMinValue) {
                this.itemGErrorMessage = this.translation.translate('ErrorMsgs.ItemGMinInvalid');
                return true;
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.invalidGValue) {
                this.itemGErrorMessage = this.translation.translate('ErrorMsgs.ItemGInputInvalid');
                return true;
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.range) {
                this.itemGErrorMessage = this.translation.translate('ErrorMsgs.ItemGNotSmallerItemF');
                return true;
            }
        }
        this.itemGErrorMessage = '';
        return false;
    }

    toogleOverrideBilingual(e) {
        if (!this.isBilingualRegion) {
            this.overrideBilingual = e.target.checked;
            this.dataService.nextOverrideBilingual(this.overrideBilingual);
            if (this.overrideBilingual) {
                this.nsdFrmGroup.get('itemEFrenchValue').clearValidators();
                this.nsdFrmGroup.get('itemEFrenchValue').setValidators(Validators.required);

                if (this.nsdFrmGroup.get('itemEFrenchValue').value) {
                    this.nsdFrmGroup.get('itemEFrenchValue').setValue(this.token.itemEFrenchValue);
                } else {
                    this.nsdFrmGroup.get('itemEFrenchValue').setValue(null);
                    this.nsdFrmGroup.setErrors({ 'invalid': true });
                }
                this.isReadOnly ? this.nsdFrmGroup.get('itemEFrenchValue').disable() : this.nsdFrmGroup.get('itemEFrenchValue').enable();
                this.nsdFrmGroup.markAsDirty();
            } else {
                if (this.nsdFrmGroup.get('itemEFrenchValue').value) {
                    this.nsdFrmGroup.get('itemEFrenchValue').setValue(null);
                }
                this.nsdFrmGroup.get('itemEFrenchValue').clearValidators();
                this.nsdFrmGroup.get('itemEFrenchValue').disable();
                this.nsdFrmGroup.markAsDirty();
            }
        }
    }
}

//Custom Validators
function validateLocation(locationService: LocationService): Function {
    return (ctrl: AbstractControl): { [key: string]: boolean } | null => {
        const location = ctrl.value;
        if (!location)
            return { required: true };

        if (locationService.validDMSLocation(location) === null)
            return { format: true };

        return null; //{ validatingLocation: true };
    }
}

function validateLatitude(locationService: LocationService): Function {
    return (ctrl: AbstractControl): { [key: string]: boolean } | null => {
        if (!locationService.validLatitude(ctrl.value))
            return { format: true };
        return null;
    }
}

function validateLongitude(locationService: LocationService): Function {
    return (ctrl: AbstractControl): { [key: string]: boolean } | null => {
        if (!locationService.validLongitude(ctrl.value))
            return { format: true };
        return null;
    }
}

function icaoConditionValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === -1) {
        return { 'required': true }
    }
    return null;
}

function radiusValidator(minRadius: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (control.value === "") {
            return { 'required': true }
        }
        var v: number = +control.value;
        return v < minRadius ? { "invalid": true } : null;
    };
}

function lowerLimitValidator(upperLimitCtrl: AbstractControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        var v1: number = +control.value;
        var v2: number = +upperLimitCtrl.value;

        if (v1 === 0 && v2 === 0) return null;

        return v1 >= v2 ? { "invalidLowerLimitRange": true } : null;
    };
}

function upperLimitValidator(lowerLimitCtrl: AbstractControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        var v1: number = +control.value;
        var v2: number = +lowerLimitCtrl.value;

        return v1 <= v2 ? { "invalidUpperLimitRange": true } : null;
    };
}

function itemFValidator(comp: CatchAllDraftComponent): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        const itemFSurface = comp.$ctrl("grpItemFG.itemFSurface").value;
        if (itemFSurface) return null;

        const unitsF = +comp.$ctrl('grpItemFG.itemFUnits').value;
        const unitsG = +comp.$ctrl('grpItemFG.itemGUnits').value;
        const itemFValue = comp.$ctrl("grpItemFG.itemFValue");
        const itemGValue = comp.$ctrl("grpItemFG.itemGValue");

        let itemF = 0;
        let itemG = 1;
        if (itemFValue.value === '') {
            return { 'invalidFValue': true }
        }
        if (isANumber(itemFValue.value)) {
            if (unitsF !== 2) {
                itemF = +itemFValue.value;
                const lowLimitF = (unitsF === 0 ? comp.lowerFLimit : comp.lowerFLimit * 100)
                const upperLimitF = (unitsF === 0 ? comp.upperFLimit : (comp.upperFLimit * 100) + 99);
                if (itemF < lowLimitF || itemF > upperLimitF) {
                    return { 'invalidFValue': true }
                }
            }
        } else {
            return { 'invalidFValue': true }
        }

        if (itemF !== 0 && unitsF !== 2) { // FL
            if (itemF < 1) {
                return { 'ItemFMinValue': true }
            }
        }

        if (isANumber(itemGValue.value) && unitsF !== 2 && unitsG !== 2) {
            itemF = (unitsF === 0 ? itemFValue.value * 100 : itemFValue.value);
            itemG = (unitsG === 0 ? itemGValue.value * 100 : itemGValue.value);
            if (+itemF >= +itemG) {
                return { 'range': true }
            }
        }
        return null;
    };
}

function itemGValidator(comp: CatchAllDraftComponent): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        const itemGUnlimited = comp.$ctrl("grpItemFG.itemGUnlimited").value;
        if (itemGUnlimited) return null;

        const unitsF = +comp.$ctrl('grpItemFG.itemFUnits').value;
        const unitsG = +comp.$ctrl('grpItemFG.itemGUnits').value;
        const itemFValue = comp.$ctrl("grpItemFG.itemFValue");
        const itemGValue = comp.$ctrl("grpItemFG.itemGValue");

        let itemF = 0;
        let itemG = 1;
        if (itemGValue.value === '') {
            return { 'invalidGValue': true }
        }
        if (isANumber(itemGValue.value)) {
            if (unitsG !== 2) {
                itemG = +itemGValue.value;
                const lowLimitG = (unitsG === 0 ? comp.lowerGLimit : comp.lowerGLimit * 100)
                const upperLimitG = (unitsG === 0 ? comp.upperGLimit : (comp.upperGLimit * 100) + 99);
                if (itemG < lowLimitG || itemG > upperLimitG) {
                    return { 'invalidGValue': true }
                }
            }
        } else {
            return { 'invalidGValue': true }
        }

        if (itemG !== 1 && unitsG !== 2) { // FL
            if (itemG < 2) {
                return { 'ItemGMinValue': true }
            }
        }

        if (isANumber(itemFValue.value) && unitsF !== 2 && unitsG !== 2) {
            itemF = (unitsF === 0 ? itemFValue.value * 100 : itemFValue.value);
            itemG = (unitsG === 0 ? itemGValue.value * 100 : itemGValue.value);
            if (+itemF >= +itemG) {
                return { 'range': true }
            }
        }
        return null;
    };
}

function purposeAllowValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const nCtrl = c.get("purposeN");
    const bCtrl = c.get("purposeB");
    const oCtrl = c.get("purposeO");
    const mCtrl = c.get("purposeM");

    const n = nCtrl.value;
    const b = bCtrl.value;
    const o = oCtrl.value;
    const m = mCtrl.value;

    if (n || b || o || m) {
        if (n && b && !o && !m) return null; //NB
        if (b && o && !m && !n) return null; //BO
        if (m && !o && !b && !n) return null; //M
        if (b && !o && !m && !n) return null; //B
        if (n && b && o && !m) return null; //NBO

        return { 'purposeInvalid': true }
    }
    else {
        return { 'purposeRequired': true }
    }
}

function trafficValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const i = c.get("trafficI");
    const v = c.get("trafficV");

    if (i.value || v.value) {
        return null
    }

    return { 'trafficRequired': true }
}

function itemAValidator(designator: string, comp: CatchAllDraftComponent): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (!c.value) {
            return { 'required': true };
        }

        if (c.value === "CXXX") {
            return null;
        }

        const itemA = c.value.toUpperCase().trim();
        const parts = itemA.replace(/\s+/g, '|').split('|');
        if (parts[0] !== designator) {
            return { 'invalid_fir': true };
        }

        //Valid FIRs and valid adjacent combinations
        if (parts.length > 1) {
            const validFirs: IFIRSubject[] = comp.firs;
            //Reset counter
            validFirs.forEach(x => x.count = 0);
            for (let i = 0; i < parts.length; i++) {
                const index = validFirs.findIndex(x => x.designator === parts[i]);
                if (index < 0) {
                    return { 'invalid_fir': true };
                } else {
                    validFirs[index].count = validFirs[index].count + 1;
                }
            }
            for (let j = 0; j < validFirs.length; j++) {
                if (validFirs[j].count > 1) return { 'repeated_fir': true };
            }
        }

        const trailingSpace = itemA.replace(/\s+/g, ' ') + " ";
        if (trailingSpace.match(/^([A-Z]{4} ){1,7} *$/)) {
            return null;
        }

        return { 'invalid': true };
    };
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}