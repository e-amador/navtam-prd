"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdGeoRefToolComponent = void 0;
var core_1 = require("@angular/core");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_service_1 = require("../shared/data.service");
var toastr_service_1 = require("../../common/toastr.service");
var location_service_1 = require("../shared/location.service");
var angular_l10n_1 = require("angular-l10n");
var NsdGeoRefToolComponent = /** @class */ (function () {
    function NsdGeoRefToolComponent(windowRef, dataService, locationService, translation, toastr) {
        this.windowRef = windowRef;
        this.dataService = dataService;
        this.locationService = locationService;
        this.translation = translation;
        this.toastr = toastr;
        this.mapHealthy = false;
        this.calcPoint = "";
        this.calcRadius = "";
        this.calcError = "";
        this.locationPoints = "";
        this.locationPoint = "";
        this.locationRadius = "";
        this.entryDataPoints = true;
    }
    NsdGeoRefToolComponent.prototype.ngOnInit = function () {
        this.initMap();
        this.mapHealthCheck();
        this.leafletgeomap.resetMapView();
    };
    NsdGeoRefToolComponent.prototype.ngAfterViewInit = function () {
        if (this.leafletgeomap) {
            this.leafletgeomap.resizeGeoMapForModal();
        }
    };
    NsdGeoRefToolComponent.prototype.ngOnDestroy = function () {
        this.disposeMap();
    };
    NsdGeoRefToolComponent.prototype.ngOnChanges = function () {
    };
    NsdGeoRefToolComponent.prototype.entryDataChange = function () {
        this.entryDataPoints = !this.entryDataPoints;
    };
    NsdGeoRefToolComponent.prototype.copyCalcPointText = function () {
        this.copyToClipBoard(this.calcPoint);
    };
    NsdGeoRefToolComponent.prototype.copyCalcRadiusText = function () {
        this.copyToClipBoard(this.calcRadius);
    };
    NsdGeoRefToolComponent.prototype.copyToClipBoard = function (text) {
        var el = document.createElement('textarea');
        el.value = text;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };
    NsdGeoRefToolComponent.prototype.calculateGeoRef = function () {
        if (this.entryDataPoints === true) {
            this.calculateGeo();
        }
        else {
            this.calculateGeoByPoint();
        }
    };
    NsdGeoRefToolComponent.prototype.calculateGeo = function () {
        var _this = this;
        if (this.locationPoints.length > 0) {
            this.calcError = "";
            this.calcPoint = "";
            this.calcRadius = "";
            var data = {
                points: this.convertEntryToPoints(this.locationPoints),
                calculatedPoint: "",
                calculatedRadius: 5,
                error: "",
                pointRadius: 0,
                internationalAerodromesDto: [],
            };
            this.dataService.calculateGeoRef(data)
                .subscribe(function (response) {
                _this.calcError = response.error;
                _this.leafletgeomap.resetMapView();
                if (_this.calcError.length === 0) {
                    _this.calcPoint = response.calculatedPoint;
                    _this.calcRadius = response.calculatedRadius.toString();
                    _this.renderMap(response.calculatedPoint, response.calculatedRadius);
                    _this.renderGeoPoints(response.points);
                    _this.renderAerodromes(response.internationalAerodromesDto);
                    _this.renderAerodromesPopup(response.internationalAerodromesDto);
                }
            }, function (error) {
            });
        }
    };
    NsdGeoRefToolComponent.prototype.calculateGeoByPoint = function () {
        var _this = this;
        if (this.locationPoint.trim().length > 0 && this.locationRadius.trim().length > 0) {
            this.calcError = "";
            this.calcPoint = "";
            this.calcRadius = "";
            var data = {
                points: this.convertEntryToPoints(this.locationPoint),
                calculatedPoint: "",
                calculatedRadius: 5,
                error: "",
                pointRadius: +this.locationRadius,
                internationalAerodromesDto: [],
            };
            if (data.points.length === 1) {
                this.dataService.calculateGeoRefByPoint(data)
                    .subscribe(function (response) {
                    _this.calcError = response.error;
                    _this.leafletgeomap.resetMapView();
                    if (_this.calcError.length === 0) {
                        _this.calcPoint = response.calculatedPoint;
                        _this.calcRadius = response.calculatedRadius.toString();
                        _this.renderMap(response.calculatedPoint, response.calculatedRadius);
                        _this.renderAerodromes(response.internationalAerodromesDto);
                        _this.renderAerodromesPopup(response.internationalAerodromesDto);
                    }
                }, function (error) {
                });
            }
            else {
                //Error
            }
        }
    };
    NsdGeoRefToolComponent.prototype.isCalcError = function () {
        if (this.calcError && this.calcError.length > 0)
            return true;
        return false;
    };
    NsdGeoRefToolComponent.prototype.convertEntryToPoints = function (loc) {
        var result = [];
        if (loc.length > 0) {
            var line = this.removeNewlines(loc); //loc.replace(/(\r\n|\n|\r)/gm, " ");
            var secPoints = line.split(",");
            secPoints.forEach(function (obj) {
                result.push(obj.trim().toUpperCase());
            });
        }
        return result;
    };
    NsdGeoRefToolComponent.prototype.removeNewlines = function (str) {
        //remove line breaks from str
        str = str.replace(/\t/g, ' ');
        str = str.toString().trim().replace(/(\r\n|\n|\r)/gm, " ");
        str = str.replace(/\s{2,}/g, ' ');
        return str;
    };
    NsdGeoRefToolComponent.prototype.isGeoPointInputEmpty = function () {
        if (this.entryDataPoints === true) {
            if (this.locationPoints.length > 0)
                return false;
            return true;
        }
        else {
            if (this.locationPoint.trim().length > 0 && this.locationRadius.trim().length > 0)
                return false;
            return true;
        }
    };
    //Map functions
    NsdGeoRefToolComponent.prototype.mapAvailable = function () {
        return this.mapHealthy;
    };
    NsdGeoRefToolComponent.prototype.initMap = function () {
        var winObj = this.windowRef.nativeWindow;
        this.leafletgeomap = winObj['app'].leafletgeomap;
        if (this.leafletgeomap) {
            this.leafletgeomap.initGeoMap();
        }
    };
    NsdGeoRefToolComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.mapHealthy = true;
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        });
    };
    NsdGeoRefToolComponent.prototype.renderMap = function (point, rad) {
        var pair = this.locationService.parse(point);
        if (pair) {
            var radP = this.locationService.nmToMeters(rad);
            this.leafletgeomap.addGeoLocationAndRadius(pair.latitude, pair.longitude, radP);
        }
    };
    NsdGeoRefToolComponent.prototype.renderGeoPoints = function (points) {
        this.leafletgeomap.addPolygonPoints(points);
    };
    NsdGeoRefToolComponent.prototype.renderAerodromes = function (aerodromesInfo) {
        this.leafletgeomap.clearAerodromeLayer();
        if (aerodromesInfo.length > 0) {
            this.leafletgeomap.addAerodromeLocationAndRadius(aerodromesInfo);
        }
    };
    NsdGeoRefToolComponent.prototype.renderAerodromesPopup = function (aerodromesInfo) {
        this.leafletgeomap.clearAerodromePopupLayer();
        if (aerodromesInfo.length > 0) {
            this.leafletgeomap.addAerodromesPopup(aerodromesInfo);
        }
    };
    NsdGeoRefToolComponent.prototype.renderCirclePoints = function (points, centerPoint) {
        var pair = this.locationService.parse(centerPoint);
        if (pair) {
            this.leafletgeomap.addCircleFromPoints(points, pair.latitude, pair.longitude);
        }
    };
    NsdGeoRefToolComponent.prototype.renderPoints = function (points, self) {
        points.forEach(function (point) {
            self.leafletgeomap.renderPoints(point.split(' ')[0], point.split(' ')[1]);
        });
    };
    NsdGeoRefToolComponent.prototype.disposeMap = function () {
        if (this.leafletgeomap) {
            this.leafletgeomap.disposeGeoMap();
        }
    };
    //End Map functions
    NsdGeoRefToolComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], NsdGeoRefToolComponent.prototype, "model", void 0);
    NsdGeoRefToolComponent = __decorate([
        core_1.Component({
            selector: 'nsd-georeftool',
            templateUrl: '/app/dashboard/nsds/nsd-georeftool.component.html'
        }),
        __param(4, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [windowRef_service_1.WindowRef,
            data_service_1.DataService,
            location_service_1.LocationService,
            angular_l10n_1.TranslationService, Object])
    ], NsdGeoRefToolComponent);
    return NsdGeoRefToolComponent;
}());
exports.NsdGeoRefToolComponent = NsdGeoRefToolComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=nsd-georeftool.component.js.map