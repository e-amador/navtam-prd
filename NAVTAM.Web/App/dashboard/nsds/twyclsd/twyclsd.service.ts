﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject, Observable } from 'rxjs/Rx';

import { WindowRef } from '../../../common/windowRef.service'

@Injectable()
export class TwyClsdService {
    constructor(private http: Http, winRef: WindowRef) {
        this.app = winRef.nativeWindow['app'];
    }

    public app: any;

    getSdoTwys(sdoId: string): Observable<any> {
        return this.http.get(`${this.app.apiUrl}taxiways/GetTwys/?sdoId=${sdoId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getTwyClsdReasons(): Observable<any> {
        return this.http.get(`${this.app.apiUrl}taxiways/GetTwyClsdReasons`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }

}