﻿import { Component, OnInit, OnDestroy, Inject, Injector } from '@angular/core'
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { DataService } from '../../shared/data.service'
import { LocationService } from '../../shared/location.service'
import { TwyClsdService } from './twyclsd.service'
import { ISdoSubject, ISelectOptions } from '../../shared/data.model'
import { ITwyClsdReason, ITaxiwaySubject } from './twyclsd.model'

import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';

import { LocaleService, TranslationService } from 'angular-l10n';

@Component({
    templateUrl: '/app/dashboard/nsds/twyclsd/twyclsd-cancel.component.html'
})
export class TwyClsdCancelComponent implements OnInit, OnDestroy {
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = true;
    leafletmap: any;
    isBilingualRegion: boolean = false;

    //isFormLoadingData: boolean;
    subjectSelected: ISdoSubject = null;
    subjectId: string = '';
    subjects: ISdoSubject[] = [];
    taxiwayId: string = '-1';

    taxiwaySelected: ITaxiwaySubject = null;
    taxiways: ITaxiwaySubject[] = [];
    taxiwaysSelected: ITaxiwaySubject[] = [];
    taxiwayComboList: ISelectOptions[] = [];

    closureReasonId: number = -1;
    closureReasonFreeText: string = '';
    closureReasonFreeTextFr: string = '';

    twyClsdReasons: ITwyClsdReason[] = [];

    closureStatus: string = '';
    allTaxiwaysSelected: boolean = false;

    disableTaxiwaySelector: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        private fb: FormBuilder,
        private locationService: LocationService,
        private twyClsdService: TwyClsdService,
        private dataService: DataService,
        public locale: LocaleService,
        public translation: TranslationService) {

        this.token = this.injector.get('token');
        this.nsdForm = this.injector.get('form');
    }

    ngOnInit() {
        //this.isFormLoadingData = !(!this.token.subjectId);
        const app = this.twyClsdService.app;
        this.leafletmap = app.leafletmap;

        this.subjectId = this.token.subjectId || '';

        this.closureReasonId = this.token.closureReasonId || -1;
        this.closureReasonFreeText = this.token.closureReasonFreeText || '';
        this.closureReasonFreeTextFr = this.token.closureReasonFreeTextFr || '';

        this.allTaxiwaysSelected = this.token.allTaxiwaysSelected || false;
        this.taxiways = this.token.allTaxiways || [];

        this.twyClsdService.getTwyClsdReasons()
            .subscribe((reasons: ITwyClsdReason[]) => {
                this.twyClsdReasons = reasons;
                this.setFirstSelectItemInCombo(this.twyClsdReasons, this.token.closureReasonId);
            });

        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.prepareTaxiwayComboList();
            this.taxiwaysSelected = [];
            var lastId: string = '';
            if (this.allTaxiwaysSelected) {
                this.disableTaxiwaySelector = true;
                var twyAll: ITaxiwaySubject = {
                    designator: 'TWYS',
                    end: '',
                    endFr: '',
                    full: true,
                    id: '0',
                    mid: '0',
                    sdoEntityName: 'ALL',
                    selected: true,
                    start: '',
                    startFr: '',
                    index: 0
                }
                this.taxiwaysSelected.push(twyAll);
                lastId = twyAll.id;
            } else {
                for (let i = 0; i < this.token.allTaxiways.length; i++) {
                    let modelTwy = this.token.allTaxiways[i];
                    if (modelTwy.selected) {
                        var twy = Object.assign({}, modelTwy);
                        this.taxiwaysSelected.push(twy);
                        lastId = twy.id;
                    }
                }
            }
            this.taxiwaySelected = this.taxiwaysSelected.find(x => x.id === lastId);

            this.dataService.getSdoSubject(this.token.subjectId)
                .subscribe((sdoSubjectResult: ISdoSubject) => {
                    this.subjectSelected = sdoSubjectResult;
                    this.subjects = [];
                    this.subjects.push(sdoSubjectResult);
                    this.isBilingualRegion = sdoSubjectResult.isBilingualRegion;
                    this.token.isBilingual = sdoSubjectResult.isBilingualRegion;
                    this.$ctrl('subjectId').setValue(sdoSubjectResult.id);
                    //this.setFirstSelectItemInCombo(this.subjects, this.subjectId);

                    this.loadMapLocations(app.doaId, true);
                    this.updateMap(sdoSubjectResult, true);
                    this.$ctrl('countSelectedTwys').setValue(this.taxiways.length);
                    this.pickTaxiway(this.taxiwaySelected.id);
                });

            this.configureReactiveFormLoading();
            this.$ctrl('countSelectedTwys').setValue(0);
            //if (lastId !== '') this.pickTaxiway(lastId);
        } else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup 
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }

    }

    ngOnDestroy() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
            this.leafletmap = null;
        }
    }

    public getTaxiwayName(twy: ITaxiwaySubject): string {
        return twy.sdoEntityName.toUpperCase() + ' ' + twy.designator.toUpperCase();
    }

    public getClosureText(twy: ITaxiwaySubject): string {
        if (twy.full) return this.translation.translate('TableHeaders.FullClosure');
        else return this.translation.translate('TableHeaders.PartialClosure');
    }

    public getBetweenText(twy: ITaxiwaySubject): string {
        if (twy.full) return "";
        else {
            if (this.isBilingualRegion) {
                return twy.start.toUpperCase() + " / " + twy.startFr.toUpperCase();
            } else {
                return twy.start.toUpperCase();
            }
        }
    }

    public getAndText(twy: ITaxiwaySubject): string {
        if (twy.full) return "";
        else {
            if (this.isBilingualRegion) {
                return twy.end.toUpperCase() + " / " + twy.endFr.toUpperCase();
            } else {
                return twy.end.toUpperCase();
            }
        }
    }

    //Enable functions
    public pickTaxiway(id: string) {
        var twySel = this.$ctrl('taxiwayId');
        if (twySel) {
            if (id === twySel.value) return;
            twySel.patchValue(id);
            this.taxiwayId = id;
            if (id === '-1') return;
            if (id === '0') {
                //All taxiways
                var field = this.$ctrl('startClosure');
                field.patchValue('');
                field = this.$ctrl('endClosure');
                field.patchValue('');
                field = this.$ctrl('startClosureFr');
                field.patchValue('');
                field = this.$ctrl('endClosureFr');
                field.patchValue('');
                field = this.$ctrl('fullClosure');
                field.patchValue(true);
                this.taxiwaySelected = null;

            } else {
                this.taxiwaySelected = this.taxiways.find(x => x.id === id);
                //Update the fields
                var field = this.$ctrl('fullClosure');
                field.patchValue(this.taxiwaySelected.full);

                field = this.$ctrl('startClosure');
                if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.start);
                else field.patchValue('');

                field = this.$ctrl('endClosure');
                if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.end);
                else field.patchValue('');

                field = this.$ctrl('startClosureFr');
                if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.startFr);
                else field.patchValue('');
                field = this.$ctrl('endClosureFr');
                if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.endFr);
                else field.patchValue('');

                //if (this.isBilingualRegion) {
                //    field = this.$ctrl('startClosureFr');
                //    if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.startFr);
                //    else field.patchValue('');
                //    field = this.$ctrl('endClosureFr');
                //    if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.endFr);
                //    else field.patchValue('');
                //} else {
                //    field = this.$ctrl('startClosureFr');
                //    field.patchValue('');
                //    field = this.$ctrl('endClosureFr');
                //    field.patchValue('');
                //}

                if (this.taxiwaySelected.full) this.closureStatus = this.translation.translate('TableHeaders.FullClosure');
                else this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
            }
        }
    }

    public rowColor(id: string) {
        if (!this.taxiwaySelected) {
            if (this.allTaxiwaysSelected) return 'bg-submitted';
            else return '';
        }
        if (this.taxiwaySelected.id === id) return 'bg-submitted';
        return '';
    }

    private configureReactiveFormLoading() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId || '', [Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            taxiwayId: [{ value: this.taxiwayId, disabled: false }],
            fullClosure: [{ value: this.taxiwaySelected.full, disabled: this.isReadOnly }],
            startClosure: [{ value: this.taxiwaySelected.start, disabled: true }],
            startClosureFr: [{ value: this.taxiwaySelected.startFr, disabled: true }],
            endClosure: [{ value: this.taxiwaySelected.end, disabled: true }],
            endClosureFr: [{ value: this.taxiwaySelected.endFr, disabled: true }],
            grpClosure: this.fb.group({
                closureReasonId: [{ value: this.token.closureReasonId, disabled: true }],
                closureReasonFreeText: [{ value: this.token.closureReasonFreeText, disabled: true }],
                closureReasonFreeTextFr: [{ value: this.token.closureReasonFreeTextFr, disabled: true }],
            }),
            allTaxiways: [{ value: this.taxiways, disabled: false }],
            countSelectedTwys: [{ value: 0 }, [countSelectedTwysValidator]],
            allTaxiwaysSelected: [{ value: this.token.allTaxiwaysSelected, disabled: false }],
        });
        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];

        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("taxiwayId", this.taxiwayId);
        this.updateFormControlValue("fullClosure", false);
        this.updateFormControlValue("startClosure", '');
        this.updateFormControlValue("startClosureFr", '');
        this.updateFormControlValue("endClosure", '');
        this.updateFormControlValue("endClosureFr", '');
        this.updateFormControlValue("grpClosure.closureReasonId", this.token.closureReasonId);
        this.updateFormControlValue("grpClosure.closureReasonFreeText", this.token.closureReasonFreeText);
        this.updateFormControlValue("grpClosure.closureReasonFreeTextFr", this.token.closureReasonFreeTextFr);
        this.updateFormControlValue("allTaxiways", this.taxiways);
        this.updateFormControlValue("countSelectedTwys", 0);
        this.updateFormControlValue("allTaxiwaysSelected", this.token.allTaxiwaysSelected);
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    private prepareTaxiwayComboList() {
        this.taxiwayComboList = [];
        this.taxiwayComboList = this.taxiways.map((item: ITaxiwaySubject) => {
            return {
                id: item.id,
                text: item.sdoEntityName.toUpperCase() + ' ' + item.designator.toUpperCase(),
            }
        });
        if (this.taxiways.length > 1) {
            //Add ALL TWYS
            this.taxiwayComboList.unshift({
                id: "0",
                text: this.translation.translate('Nsd.AllTaxiways'),
            });
        }

        //Add empty slot
        this.taxiwayComboList.unshift({
            id: "-1",
            text: "",
        });
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    public closureReasonOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.ClosurePlaceholder')
        }
    }

    public taxiwaysOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.TaxiwayPlaceholder')
        }
    }

    public sdoSubjectWithTwysOptions(): any {
        const self = this;
        const app = this.twyClsdService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "taxiways/filterwithtwy",
                dataType: 'json',
                delay: 500,
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }

    public get colBetweenCaption(): string {
        if (this.isBilingualRegion) return this.translation.translate('TableHeaders.BetweenBiligual');
        else return this.translation.translate('TableHeaders.Between');
    }

    public get colAndCaption(): string {
        if (this.isBilingualRegion) return this.translation.translate('TableHeaders.AndBiligual');
        else return this.translation.translate('TableHeaders.And');
    }

    //OnChange functions
    //Validation functions

    public isRequiredReasonFreeText(): boolean {
        const reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
        if (reasonIdCtrl.value === -1) return false;
        let elem = this.twyClsdReasons.find(x => x.id === reasonIdCtrl.value);
        if (elem !== undefined && elem !== null) {
            const reasonValueText = elem.text.toLowerCase();
            if ((reasonValueText === 'other') || (reasonValueText === 'autre')) return true;
        }
        return false;
    }

    public validateTaxiway(twy: ITaxiwaySubject): any {
        if (!twy.selected) return null;
        if (twy.full) {
            if (twy.start.length === 0 && twy.startFr.length === 0 && twy.end.length === 0 && twy.endFr.length === 0) return null;
            else return { fullError: { valid: false } };
        } else {
            if (twy.start.length === 0 || twy.end.length === 0) return { partialError: { valid: false } };
            else {
                if (this.isBilingualRegion) {
                    if (twy.startFr.length === 0 || twy.endFr.length === 0) return { partialError: { valid: false } };
                    else return null;
                } else return null;
            }
        }
    }

    public validateAllTaxiways() {
        if (!this.taxiways || !this.taxiwaysSelected) return { selectedTaxiways: { valid: false } }
        var error: any = null;
        if (this.taxiways.length === 0) return { allTaxiways: { valid: false } };
        if (this.taxiwaysSelected.length === 0) return { allTaxiways: { valid: false } };

        for (var i = 0; i < this.taxiways.length; i++) {
            error = this.validateTaxiway(this.taxiways[i]);
            if (error !== null) {
                i = this.taxiways.length + 1;
            }
        }
        return error;
    }

    private validateAndTrigger() {
        let error = this.validateAllTaxiways();
        if (error) {
            this.nsdForm.controls['frmArr'].setErrors(null);
            this.nsdForm.controls['frmArr'].setErrors(error);
            let ctrl = this.$ctrl('countSelectedTwys');
            ctrl.setValue(0);
        } else {
            this.nsdForm.controls['frmArr'].setErrors(null);
            let ctrl = this.$ctrl('countSelectedTwys');
            ctrl.setValue(this.taxiwaysSelected.length);
            ctrl = this.$ctrl('allTaxiways');
            ctrl.setValue(this.taxiways);

        }
    }

    // Standard general function

    private updateMap(sdoSubject: ISdoSubject, acceptSubjectLocation: boolean) {
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                const coordinates = geojson.features[0].geometry.coordinates;
                const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, () => {
                        if (markerGeojson) {
                            this.leafletmap.setNewMarkerLocation(markerGeojson);
                        }
                    });
                });
            }
        }

    }

    private setFirstSelectItemInCombo(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        const index = items.findIndex(x => x.id === itemId);
        if (index > 0) { //move selected item to position 0
            items.splice(0, 0, items.splice(index, 1)[0]);
        }
    }

    private setFirstSelectItemInTaxiways(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        const index = items.findIndex(x => x.id === itemId);
        if (index > 0) { //move selected item to position 0
            items.splice(0, 0, items.splice(index, 1)[0]);
        }
    }

    private loadMapLocations(doaId: number, loadingFromProposal: boolean) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leafletmap && self.leafletmap.isReady()) {
                        window.clearInterval(interval);
                        self.leafletmap.addDoa(geoJsonStr, 'DOA');
                        if (loadingFromProposal) {
                            self.updateMap(self.subjectSelected, false);
                        }
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error(this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }


}

function countSelectedTwysValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === 0) {
        return { 'required': true }
    }
    return null;
}
