"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ObstLgUsCancelComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var obstlgus_service_1 = require("./obstlgus.service");
var angular_l10n_1 = require("angular-l10n");
var ObstLgUsCancelComponent = /** @class */ (function () {
    function ObstLgUsCancelComponent(injector, fb, locationService, obstacleService, dataService, translation) {
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.obstacleService = obstacleService;
        this.dataService = dataService;
        this.translation = translation;
        this.token = null;
        this.obstacleTypes = [];
        this.token = this.injector.get('token');
        this.nsdForm = this.injector.get('form');
    }
    ObstLgUsCancelComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.obstacleService.app;
        this.leafletmap = app.leafletmap;
        //this.configureReactiveForm();
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
        this.loadObstacleTypes();
        this.nsdForm.markAsDirty();
        this.showDoaMapRegion(app.doaId);
    };
    ObstLgUsCancelComponent.prototype.ngOnDestroy = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
            this.leafletmap = null;
        }
    };
    ObstLgUsCancelComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    ObstLgUsCancelComponent.prototype.objectTypeSelectOptions = function () {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ObjectTypePlaceholder')
            }
        };
    };
    ObstLgUsCancelComponent.prototype.setCheckBoxValue = function (name) {
        this.$ctrl(name).value === true;
    };
    ObstLgUsCancelComponent.prototype.setObjectType = function ($event) {
        var obstTypeCtrl = this.$ctrl('obstacleType');
        var newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== obstTypeCtrl.value) {
                obstTypeCtrl.setValue(newValue);
                obstTypeCtrl.markAsDirty();
            }
            this.refreshMap();
        }
        else {
            //??
        }
    };
    ObstLgUsCancelComponent.prototype.loadObstacleTypes = function () {
        var _this = this;
        this.obstacleService
            .getObstacleTypes()
            .subscribe(function (obstacleTypes) {
            var obstacles = obstacleTypes.map(function (item) {
                return {
                    id: item.id,
                    text: item.name
                };
            });
            // make the 'token.obstacleType' value the first on the list for "optionless convenience" or angular will throw an exception!
            _this.makeFirstElement(obstacles, function (obst) { return obst.id === _this.token.obstacleType; }, true);
            _this.obstacleTypes = obstacles;
        });
    };
    ObstLgUsCancelComponent.prototype.makeFirstElement = function (arr, makeFirst, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var len = arr.length;
        for (var i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
    };
    ObstLgUsCancelComponent.prototype.swapArrayPositions = function (arr, p1, p2) {
        var temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    };
    ObstLgUsCancelComponent.prototype.radioButtonChanged = function (name) {
        this.$ctrl(name).markAsDirty();
    };
    ObstLgUsCancelComponent.prototype.hasLowerCase = function (str) {
        return (/[a-z]/.test(str));
    };
    ObstLgUsCancelComponent.prototype.locationChanged = function (value) {
        var _this = this;
        var tmp = this.$ctrl('location').value;
        if (tmp !== null && this.hasLowerCase(tmp))
            this.$ctrl('location').patchValue(tmp.toUpperCase());
        if (value !== null && this.hasLowerCase(value))
            value = value.toUpperCase();
        if (this.leafletmap === null)
            this.leafletmap = this.obstacleService.app.leafletmap;
        var pair = this.locationService.parse(value);
        var geojson = pair ? this.leafletmap.latitudeLongitudeToGeoJson(pair.latitude, pair.longitude) : null;
        if (geojson) {
            this.obstacleService
                .validateLocation(value)
                .subscribe(function (result) {
                var locCtrl = _this.$ctrl('location');
                var currentLocation = locCtrl.value;
                if (currentLocation === value) {
                    if (result.inDoa) {
                        _this.leafletmap.clearFeaturesOnLayers();
                        _this.leafletmap.addMarker(geojson, _this.getSelectedObstacleName());
                        _this.leafletmap.setNewMarkerLocation(geojson);
                        locCtrl.setErrors(null);
                        _this.triggerFormChangeEvent(value);
                    }
                    else {
                        locCtrl.setErrors({ invalidLocation: true });
                    }
                }
            });
        }
    };
    ObstLgUsCancelComponent.prototype.triggerFormChangeEvent = function (value) {
        this.$ctrl('refreshDummy').setValue(value);
    };
    ObstLgUsCancelComponent.prototype.refreshMap = function () {
        var _this = this;
        window.setTimeout(function () { return _this.locationChanged(_this.$ctrl('location').value); }, 1000);
    };
    ObstLgUsCancelComponent.prototype.getSelectedObstacleName = function () {
        var obstId = this.$ctrl('obstacleType').value;
        if (!obstId || !this.obstacleTypes)
            return "OBST";
        var obstacle = this.obstacleTypes.find(function (obst) { return obst.id === obstId; });
        return obstacle ? obstacle.text : "OBST";
    };
    ObstLgUsCancelComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            obstacleType: [{ value: this.token.obstacleType || -1, disabled: true }, []],
            location: [{ value: this.token.location, disabled: true }, []],
            height: [{ value: this.token.height, disabled: true }, []],
            elevation: [{ value: this.token.elevation, disabled: true }, []],
            refreshDummy: ""
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    ObstLgUsCancelComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("obstacleType", this.token.obstacleType);
        this.updateFormControlValue("location", this.token.location);
        this.updateFormControlValue("height", this.token.height);
        this.updateFormControlValue("elevation", this.token.elevation);
    };
    ObstLgUsCancelComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    ObstLgUsCancelComponent.prototype.showDoaMapRegion = function (doaId) {
        var _this = this;
        this.dataService
            .getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) { return _this.renderMap(geoJsonStr); });
    };
    ObstLgUsCancelComponent.prototype.renderMap = function (geoJsonStr) {
        var self = this;
        var interval = window.setInterval(function () {
            if (self.leafletmap && self.leafletmap.isReady()) {
                window.clearInterval(interval);
                self.leafletmap.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    };
    ObstLgUsCancelComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/obstlgus/obstlgus-cancel.component.html'
        }),
        __metadata("design:paramtypes", [core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            obstlgus_service_1.ObstacleLgUsService,
            data_service_1.DataService,
            angular_l10n_1.TranslationService])
    ], ObstLgUsCancelComponent);
    return ObstLgUsCancelComponent;
}());
exports.ObstLgUsCancelComponent = ObstLgUsCancelComponent;
//# sourceMappingURL=obstlgus-cancel.component.js.map