﻿import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { WindowRef } from '../../../common/windowRef.service'


export interface IClosureType {
    id: string,
    name: string
}
export interface IRwy {
    id: string,
    designator: string,    
}

export interface IAerodrome {
    id: string,
    designator: string,
    name: string
}

@Injectable()
export class RwyClsdService {
    constructor(private http: Http, winRef: WindowRef) {
        this.app = winRef.nativeWindow['app'];
    }

    public app: any;  

    getClosureReasons(): Observable<IClosureType[]> {
        return this.http
            .get(`${this.app.apiUrl}closures/get`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    checkIfBilingualRegion(subjectId: string) {
        return this.http
            .get(`${this.app.apiUrl}closures/isinbilingualregion/?subjectId=${subjectId}`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getDoaLocation(doaId: number): Observable<any> {
        return this.http
            .get(`${this.app.apiUrl}doas/GeoJson/?doaId=${doaId}`)
            .map((response: Response) => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getEffectedRunway(subject: any): Observable<any> {
       return this.http
            .get(`${this.app.apiUrl}sdosubjects/GetAllRwyByAhpIdAsync/?ahpId=${subject}`)
            .map((response: Response) => {
                return response.json();
            })
            .catch(this.handleError);
    }

    getGeoJsonAhp(subject: any): Observable<any> {
        return this.http
            .get(`${this.app.apiUrl}closures/getAhpGeo/?ahpId=${subject}`)
            .map((response: Response) => {
                return response.json();
            })
            .catch(this.handleError);
    }

    convertGeo(subject: any): Observable<any> {
        let x = 1;
        return this.http
            .get(`${this.app.apiUrl}closures/getGeo/?subjectId=${subject}`)
            .map((response: Response) => {
                return response.json();
            })
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}