"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RwyClsdComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var location_service_1 = require("../../shared/location.service");
var data_service_1 = require("../../shared/data.service");
var rwyclsd_service_1 = require("./rwyclsd.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var RwyClsdComponent = /** @class */ (function () {
    function RwyClsdComponent(toastr, injector, fb, locationService, closureService, changeDetectionRef, dataService, translation) {
        this.toastr = toastr;
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.closureService = closureService;
        this.changeDetectionRef = changeDetectionRef;
        this.dataService = dataService;
        this.translation = translation;
        this.action = "";
        this.token = null;
        this.isReadOnly = false;
        this.isBilingualRegion = false;
        this.closureReasons = [];
        this.rwySelections = [];
        this.addClosureReason = false;
        this.otherReason = false;
        this.availableAsTaxiway = false;
        this.effectedAerodromeName = "";
        this.acftWeightRestriction = 99999999;
        this.acftWingRestriction = 99999999;
        this.isReplacementNotam = false;
        this.subjectSelected = null;
        this.runwayEffected = null;
        this.disableIcaoSubjectSelect = true;
        this.disableIcaoConditionSelect = true;
        this.icaoSubjectSelectOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ItemASelect')
            }
        };
        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
        this.isReplacementNotam = this.token.type === "replace" ? true : false;
    }
    RwyClsdComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.closureService.app;
        this.leaflet = app.leafletmap;
        this.loadClosureTypes();
        var frmArr = this.nsdForm.controls['frmArr'];
        if (this.isFormLoadingData) {
            if (frmArr.length === 0) {
                this.configureReactiveForm();
                //This "dirty trick" is important to make the parent form not
                //to run the generateIcao until everything is loaded.
                this.$ctrl('refreshDummy').setValue(0);
                this.nsdForm.controls['frmArr'].setErrors({ 'required': true });
            }
            else {
                /*
               this is required because of the grouping funtionallity. When the NsdFormGroup
               was configured and any of items in the group has changed (new set of tokens values)
               the NsdForm Group needs to be updated with the new tokens values */
                this.updateReactiveForm(frmArr);
            }
            this.setAerodromeFromToken();
        }
        else {
            this.configureReactiveForm();
        }
        this.showDoaMapRegion(app.doaId);
    };
    RwyClsdComponent.prototype.ngOnDestroy = function () {
    };
    RwyClsdComponent.prototype.checkIfReviewDisabled = function () {
        var review = !(!this.token.subjectId);
        return review;
    };
    RwyClsdComponent.prototype.setAerodromeFromToken = function () {
        var _this = this;
        this.setDummyError(0);
        this.dataService.getSdoSubject(this.token.effectedAerodrome)
            .subscribe(function (sdoSubjectResult) {
            _this.subjectSelected = sdoSubjectResult;
            _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
            _this.getEffectedRunway(sdoSubjectResult.id);
            _this.subjects = [];
            _this.subjects.push(sdoSubjectResult);
            _this.loadMapLocations(_this.closureService.app.doaId, true);
            _this.updateMap(sdoSubjectResult, true);
            _this.setDummyError(1);
        });
    };
    RwyClsdComponent.prototype.loadMapLocations = function (doaId, loadingFromProposal) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leaflet && self.leaflet.isReady()) {
                    window.clearInterval(interval);
                    self.leaflet.addDoa(geoJsonStr, 'DOA');
                    if (loadingFromProposal) {
                        self.updateMap(self.subjectSelected, false);
                    }
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    RwyClsdComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    RwyClsdComponent.prototype.IsBilingualRegion = function () {
        return this.isBilingualRegion;
    };
    RwyClsdComponent.prototype.ItemASelectOptions = function () {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ItemASelect')
            }
        };
    };
    RwyClsdComponent.prototype.closureTypeSelectOptions = function () {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SelectClosure')
            }
        };
    };
    RwyClsdComponent.prototype.runwaySelectOptions = function () {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.RunwaySelect')
            }
        };
    };
    RwyClsdComponent.prototype.getEffectedAerodrome = function () {
        var self = this;
        var app = this.closureService.app;
        return {
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/rwyclosurefilter",
                dataType: 'json',
                delay: 500,
                data: function (params, page) {
                    return {
                        Query: params.term,
                        Size: 200,
                        doaId: app.doaId
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === '-1') {
                    return selection.text;
                }
                self.updateSubjectSelected(selection);
                self.effectedAerodromeName = self.dataService.getSdoExtendedName(selection);
                return self.effectedAerodromeName;
            },
        };
    };
    RwyClsdComponent.prototype.updateSubjectSelected = function (sdoSubject) {
        var _this = this;
        var subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            return;
        }
        if (sdoSubject.id !== subjectId) {
            var subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;
        this.dataService.getSdoSubject(sdoSubject.id)
            .subscribe(function (sdoSubjectResult) {
            _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
            if (!_this.isFormLoadingData) {
                _this.setEffectedAerodromeName(_this.effectedAerodromeName);
                _this.$ctrl('effectedRunway').setValue("");
                _this.$ctrl('effectedRunwayName').setValue("");
                _this.token.effectedRunwayName = "";
                _this.token.effectedRunway = "";
            }
            _this.updateMap(sdoSubjectResult, true);
        });
    };
    RwyClsdComponent.prototype.setEffectedAerodrome = function ($event) {
        var effectedAerodromeCtrl = this.$ctrl('effectedAerodrome');
        var subjectId = this.$ctrl('subjectId');
        var newValue = $event.value;
        if (newValue !== '-1' && effectedAerodromeCtrl.value.value != newValue) {
            effectedAerodromeCtrl.setValue(newValue);
            subjectId.patchValue(newValue);
            effectedAerodromeCtrl.markAsDirty();
            this.getEffectedRunway(newValue);
        }
    };
    RwyClsdComponent.prototype.checkForDisableItemAIfReplacement = function () {
        if (this.isReadOnly) {
            return this.isReadOnly; //same condiitons
        }
        return this.isReplacementNotam;
    };
    RwyClsdComponent.prototype.checkIfBilingualRegion = function (subjectId) {
        var _this = this;
        this.dataService.getSdoSubject(subjectId)
            .subscribe(function (sdoSubjectResult) {
            _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
        });
    };
    RwyClsdComponent.prototype.setBilingualRegion = function (value) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
            }
        }
        else {
            if (!this.isReadOnly) {
            }
        }
    };
    RwyClsdComponent.prototype.setEffectedAerodromeName = function (name) {
        var effectedAerodromeNameCtrl = this.$ctrl('effectedAerodromeName');
        effectedAerodromeNameCtrl.patchValue(name);
    };
    RwyClsdComponent.prototype.invalidEffectedAerodrome = function () {
        var ctrl = this.$ctrl('effectedAerodrome');
        return !this.isReadOnly && this.$ctrl('effectedAerodrome').invalid;
    };
    RwyClsdComponent.prototype.validateRunway = function () {
        var rwyName = this.$ctrl('effectedRunwayName');
        if (rwyName.value === "") {
            var error = { RunwayNeeded: { valid: false } };
            this.nsdFrmGroup.setErrors(error);
            return true;
        }
        return false;
    };
    RwyClsdComponent.prototype.getEffectedRunway = function (subjectAerodrome) {
        var _this = this;
        if (subjectAerodrome) {
            this.closureService
                .getEffectedRunway(subjectAerodrome)
                .subscribe(function (rwySelections) {
                var runways = rwySelections.map(function (item) {
                    return {
                        id: item.id,
                        text: item.designator
                    };
                });
                _this.rwySelections = null;
                _this.makeFirstElement(runways, function (rwy) { return rwy.text === _this.token.effectedRunwayName; }, !_this.isFormLoadingData);
                _this.rwySelections = runways;
                _this.isFormLoadingData = false;
            });
        }
    };
    RwyClsdComponent.prototype.setEffectedRunway = function ($event) {
        var _this = this;
        if ($event.value && $event.value !== '-1') {
            var effectedRwyCtrl = this.$ctrl('effectedRunway');
            var newValue_1 = $event.value;
            if (newValue_1 !== "") {
                var effectedRwyNameCtrl = this.$ctrl('effectedRunwayName');
                effectedRwyNameCtrl.setValue(this.rwySelections.find(function (x) { return x.id === newValue_1; }).text);
                effectedRwyCtrl.setValue(newValue_1);
                effectedRwyCtrl.markAsDirty();
                this.closureService.convertGeo($event.value)
                    .subscribe(function (sdoSubjectResult) {
                    _this.updateMap(sdoSubjectResult, true);
                });
            }
        }
        if (!this.isFormLoadingData) {
            this.setDummyError(1);
        }
    };
    RwyClsdComponent.prototype.loadClosureTypes = function () {
        var _this = this;
        this.closureService
            .getClosureReasons()
            .subscribe(function (closureReasons) {
            var closures = closureReasons.map(function (item) {
                return {
                    id: item.id,
                    text: item.name
                };
            });
            _this.makeFirstElement(closures, function (clsr) { return clsr.id === _this.token.closureReasonId; }, true);
            _this.closureReasons = closures;
        });
    };
    RwyClsdComponent.prototype.setClosureType = function ($event) {
        var closureTypeCtrl = this.$ctrl('closureReasonId');
        var newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== closureTypeCtrl.value) {
                closureTypeCtrl.setValue(newValue);
                closureTypeCtrl.markAsDirty();
            }
            if (!this.isFormLoadingData) {
                this.$ctrl('refreshDummy').setValue(0);
                this.validateOtherReason();
            }
        }
    };
    RwyClsdComponent.prototype.getOtherReason = function () {
        var closureQuestionValue = this.$ctrl('closureQuestion');
        var closureTypeCtrl = this.$ctrl('closureReasonId');
        if (closureQuestionValue.value && closureTypeCtrl.value === "Re9") {
            return true;
        }
        return false;
    };
    RwyClsdComponent.prototype.setDummyError = function (val) {
        if (val === 1 && this.$ctrl('refreshDummy').value !== 1) {
            this.$ctrl('refreshDummy').setValue(1);
            this.nsdForm.controls['frmArr'].setErrors(null);
        }
        else if (val === 0 && this.$ctrl('refreshDummy').value !== 0) {
            this.$ctrl('refreshDummy').setValue(0);
            this.nsdForm.controls['frmArr'].setErrors({ 'required': true });
        }
    };
    RwyClsdComponent.prototype.changeReasonFree = function () {
        if (this.isFormLoadingData)
            return;
        this.setDummyError(0);
        var closureFreeEn = this.$ctrl('closureReasonOtherFreeText');
        var closureFreeFr = this.$ctrl('closureReasonOtherFreeTextFr');
        if (this.isBilingualRegion) {
            if (closureFreeEn.value.length > 0 && closureFreeFr.value.length > 0) {
                this.setDummyError(1);
                return;
            }
        }
        else if (closureFreeEn.value.length > 0) {
            this.setDummyError(1);
            return;
        }
    };
    RwyClsdComponent.prototype.validateAerodromeRequired = function () {
        var ad = this.$ctrl('effectedAerodrome').value;
        if (ad.value === "") {
            var error = { aerodromeRequired: { valid: false } };
            this.nsdFrmGroup.setErrors(error);
            return false;
        }
        return true;
    };
    RwyClsdComponent.prototype.validateOtherReason = function () {
        if (this.getOtherReason()) {
            var closureFreeEn = this.$ctrl('closureReasonOtherFreeText').value;
            var closureFreeFr = this.$ctrl('closureReasonOtherFreeTextFr').value;
            var error = { closureReasonNeeded: { valid: false } };
            if (this.isBilingualRegion) {
                if (closureFreeEn.length > 0 && closureFreeFr.length > 0) {
                    this.setDummyError(1);
                    return false;
                }
            }
            else if (closureFreeEn.length > 0) {
                this.setDummyError(1);
                return false;
            }
            this.setDummyError(0);
            return true;
        }
        else {
            var closureTypeCtrl = this.$ctrl('closureReasonId');
            if (closureTypeCtrl.value !== "Re9") {
                this.cleanFreeTextClosureReason();
            }
        }
        if (this.$ctrl('refreshDummy').value === 0) {
            this.$ctrl('refreshDummy').setValue(1);
            this.nsdFrmGroup.setErrors(null);
        }
        return false;
    };
    RwyClsdComponent.prototype.cleanFreeTextClosureReason = function () {
        this.updateFormControlValue("closureReasonOtherFreeText", "");
        this.updateFormControlValue("closureReasonOtherFreeTextFr", "");
    };
    RwyClsdComponent.prototype.validateClosures = function () {
        if (!this.getOtherReason()) {
            var closureId = this.$ctrl('closureReasonId').value;
            var closureQuestion = this.$ctrl('closureQuestion').value;
            var error = { closureReasonNeeded: { valid: false } };
            if (closureQuestion) {
                if (closureId === -1 || closureId.toString() === "-1") {
                    this.nsdFrmGroup.setErrors(error);
                    return true;
                }
            }
        }
        return false;
    };
    RwyClsdComponent.prototype.validateTwyRestrictionsFreeText = function () {
        if (this.CheckIfTaxiwayRestrictions()) {
            if (this.isBilingualRegion) {
                if (this.$ctrl('freeTextTaxiwayRestriction').value === "" ||
                    this.$ctrl('freeTextTaxiwayRestrictionFr').value === "") {
                    var error = { TaxiwayRestrictionRequired: { valid: false } };
                    this.nsdFrmGroup.setErrors(error);
                    return true;
                }
            }
            if (this.$ctrl('freeTextTaxiwayRestriction').value === "") {
                var error = { TaxiwayRestrictionRequired: { valid: false } };
                this.nsdFrmGroup.setErrors(error);
                return true;
            }
        }
        return false;
    };
    RwyClsdComponent.prototype.validateAircraftRestrictionsValues = function () {
        if (this.CheckIfAircraftRestrictions) {
            var wingCtrlValue = this.$ctrl('maxWingSpanFt').value;
            var weightCtrlValue = this.$ctrl('maxWeightLbs').value;
            if (weightCtrlValue > 0 || wingCtrlValue > 0) {
                return false;
            }
            else {
                var error = { AircraftRestrictionRequired: { valid: false } };
                this.nsdFrmGroup.setErrors(error);
                return true;
            }
        }
        return false;
    };
    RwyClsdComponent.prototype.validateIsNaN = function () {
        var wing = this.$ctrl('maxWingSpanFt').value;
        var weight = this.$ctrl('maxWeightLbs').value;
        if (!isFinite(weight) || !isFinite(wing)) {
            var error = { validateIsNaN: { valid: false } };
            this.nsdFrmGroup.setErrors(error);
            return true;
        }
        return false;
    };
    RwyClsdComponent.prototype.validateBigInt = function () {
        var wing = this.$ctrl('maxWingSpanFt').value;
        var weight = this.$ctrl('maxWeightLbs').value;
        if (this.InvalidNumber(wing, weight)) {
            var error = { validateBigInt: { valid: false } };
            this.nsdFrmGroup.setErrors(error);
            return true;
        }
        return false;
    };
    RwyClsdComponent.prototype.InvalidNumber = function (wing, weight) {
        if ((wing !== null && wing.length > 20) || (weight !== null && weight.length > 20)) {
            return true;
        }
        return false;
    };
    RwyClsdComponent.prototype.CheckIfTaxiwayRestrictions = function () {
        var controlValue = this.$ctrl('txyRestrictions').value;
        return controlValue;
    };
    RwyClsdComponent.prototype.CheckIfAircraftRestrictions = function () {
        var value = this.$ctrl('acftRestrictions').value;
        if (this.getAvailAsTxyway() && value)
            return true;
        return false;
    };
    RwyClsdComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    RwyClsdComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectLocation: this.token.subjectLocation,
            subjectId: this.token.effectedAerodrome,
            closureReasonId: [{ value: this.token.closureReasonId || -1, disabled: this.isReadOnly }],
            closureQuestion: [{ value: this.token.closureQuestion, disabled: this.isReadOnly }, [closureTypeValidator(this.nsdFrmGroup)]],
            effectedAerodrome: [{ value: this.token.effectedAerodrome || "" }, [effectedAerodromeValidator]],
            effectedAerodromeName: [{ value: this.token.effectedAerodromeName || "" }],
            effectedRunway: [{ value: this.token.effectedRunway, disabled: this.isReadOnly }],
            effectedRunwayName: [{ value: this.token.effectedRunwayName || "" }],
            availableAsTaxiway: [{ value: this.token.availableAsTaxiway, disabled: this.isReadOnly }],
            txyRestrictions: [{ value: this.token.txyRestrictions, disabled: this.isReadOnly }, [txyRestrictions]],
            freeTextTaxiwayRestriction: [{ value: this.token.freeTextTaxiwayRestriction || "", disabled: this.isReadOnly }],
            freeTextTaxiwayRestrictionFr: [{ value: this.token.freeTextTaxiwayRestrictionFr || "", disabled: this.isReadOnly }],
            otherReason: [{ value: this.token.otherReason }],
            closureReasonOtherFreeText: [{ value: this.token.closureReasonOtherFreeText || "", disabled: this.isReadOnly }],
            closureReasonOtherFreeTextFr: [{ value: this.token.closureReasonOtherFreeTextFr || "", disabled: this.isReadOnly }],
            acftRestrictions: [{ value: this.token.acftRestrictions, disabled: this.isReadOnly }],
            maxWingSpanFt: [{ value: this.token.maxWingSpanFt, disabled: this.isReadOnly }, [aircraftRestrictionValidator(this.nsdFrmGroup)]],
            maxWeightLbs: [{ value: this.token.maxWeightLbs, disabled: this.isReadOnly }, [aircraftRestrictionValidator(this.nsdFrmGroup)]],
            refreshDummy: [{ value: 0 }, [refreshDummyValidator]],
        });
        this.subscribeRadioButtons();
        frmArrControls.push(this.nsdFrmGroup);
    };
    RwyClsdComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("closureReasonId", this.token.closureReasonId);
        this.updateFormControlValue("closureQuestion", this.token.closureQuestion);
        this.updateFormControlValue("effectedAerodrome", this.token.effectedAerodrome);
        this.updateFormControlValue("effectedAerodromeName", this.token.effectedAerodromeName);
        this.updateFormControlValue("effectedRunway", this.token.effectedRunway);
        this.updateFormControlValue("effectedRunwayName", this.token.effectedRunwayName);
        this.updateFormControlValue("availableAsTaxiway", this.token.availableAsTaxiway);
        this.updateFormControlValue("txyRestrictions", this.token.txyRestrictions);
        this.updateFormControlValue("freeTextTaxiwayRestriction", this.token.freeTextTaxiwayRestriction);
        this.updateFormControlValue("freeTextTaxiwayRestrictionFr", this.token.freeTextTaxiwayRestrictionFr);
        this.updateFormControlValue("otherReason", this.token.otherReason);
        this.updateFormControlValue("closureReasonOtherFreeText", this.token.closureReasonOtherFreeText);
        this.updateFormControlValue("closureReasonOtherFreeTextFr", this.token.closureReasonOtherFreeTextFr);
        this.updateFormControlValue("acftRestrictions", this.token.acftRestrictions);
        this.updateFormControlValue("maxWingSpanFt", this.token.maxWingSpanFt);
        this.updateFormControlValue("maxWeightLbs", this.token.maxWeightLbs);
        this.subscribeRadioButtons();
    };
    RwyClsdComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    RwyClsdComponent.prototype.subscribeRadioButtons = function () {
        var _this = this;
        this.$ctrl('availableAsTaxiway').valueChanges.subscribe(function (value) { return _this.radioButtonChanged('availableAsTaxiway'); });
        this.$ctrl('txyRestrictions').valueChanges.subscribe(function (value) { return _this.radioButtonChanged('txyRestrictions'); });
        this.$ctrl('closureQuestion').valueChanges.subscribe(function (value) { return _this.radioButtonChanged('closureQuestion'); });
        this.$ctrl('acftRestrictions').valueChanges.subscribe(function (value) { return _this.radioButtonChanged('acftRestrictions'); });
    };
    RwyClsdComponent.prototype.radioButtonChanged = function (name) {
        if (name === "closureQuestion") {
            this.resetClosureQuestionItemE(name);
        }
        if (name === "availableAsTaxiway") {
            this.resetTxyAvail();
        }
        if (name === "acftRestrictions") {
            this.resetAcftRestrictionsItemE(name);
        }
    };
    RwyClsdComponent.prototype.resetClosureQuestionItemE = function (name) {
        if (!this.$ctrl(name).value) {
            this.token.closureReasonId = -1;
            this.$ctrl('closureReasonId').patchValue(-1);
            if (!this.$ctrl('closureQuestion').value) {
                this.token.closureReason = null;
            }
            this.$ctrl('refreshDummy').setValue(1);
        }
        else {
            this.$ctrl('refreshDummy').setValue(0);
        }
    };
    RwyClsdComponent.prototype.resetAcftRestrictionsItemE = function (name) {
        if (!this.$ctrl(name).value) {
            this.token.acftRestrictions = false;
            this.token.maxWingSpanFt = null;
            this.token.maxWeightLbs = null;
            this.updateFormControlValue("maxWingSpanFt", null);
            this.updateFormControlValue("maxWeightLbs", null);
        }
        else {
            this.token.acftRestrictions = true;
        }
    };
    RwyClsdComponent.prototype.resetTxyAvail = function () {
        if (this.availableAsTaxiway) {
            this.token.availableAsTaxiway = true;
        }
        else {
            this.sanitizeTwyAvail();
        }
    };
    RwyClsdComponent.prototype.sanitizeTwyAvail = function () {
        this.$ctrl('freeTextTaxiwayRestriction').setValue("");
        this.$ctrl('freeTextTaxiwayRestrictionFr').setValue("");
        this.$ctrl('maxWingSpanFt').setValue("");
        this.$ctrl('maxWeightLbs').setValue("");
        //if not available as twy impossible to have restrictions -> sanitize control
        this.$ctrl('txyRestrictions').setValue(false);
        this.$ctrl('acftRestrictions').setValue(false);
    };
    RwyClsdComponent.prototype.showDoaMapRegion = function (doaId) {
        var _this = this;
        this.dataService
            .getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) { return _this.renderMap(geoJsonStr); });
    };
    RwyClsdComponent.prototype.renderMap = function (geoJsonStr) {
        var self = this;
        var interval = window.setInterval(function () {
            if (self.leaflet && self.leaflet.isReady()) {
                window.clearInterval(interval);
                self.leaflet.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    };
    RwyClsdComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        if (sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                this.leaflet.clearFeaturesOnLayers(['DOA']);
                var subLocation = this.leaflet.geojsonToTextCordinates(geojson);
                var coordinates = geojson.features[0].geometry.coordinates;
                var markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leaflet.addMarker(geojson, sdoSubject.designator);
            }
        }
    };
    RwyClsdComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leaflet.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    RwyClsdComponent.prototype.makeFirstElement = function (arr, makeFirst, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var len = arr.length;
        for (var i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
    };
    RwyClsdComponent.prototype.swapArrayPositions = function (arr, p1, p2) {
        var temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    };
    RwyClsdComponent.prototype.checkBothAircraftRestrictionsValidator = function () {
        var weight = this.$ctrl('acftWeightRestrictions').value;
        var wing = this.$ctrl('acftWingspanRestrictions').value;
        if (weight || wing) {
            return false;
        }
        return true;
    };
    RwyClsdComponent.prototype.checkBothTxyRestrictionsValidator = function () {
        var eng = this.$ctrl('freeTextTaxiwayRestriction').value;
        var fr = this.$ctrl('freeTextTaxiwayRestrictionFr').value;
        if (this.IsBilingualRegion()) {
            if (eng.length == 0 || fr.length === 0) {
                return false;
            }
        }
        if (eng.length === 0) {
            return false;
        }
        return true;
    };
    RwyClsdComponent.prototype.getAvailAsTxyway = function () {
        return this.$ctrl('availableAsTaxiway').value;
    };
    RwyClsdComponent.prototype.getTwyRestrictions = function () {
        return this.$ctrl('twyRestrictions').value;
    };
    RwyClsdComponent.prototype.setMaxWeight = function (resetCtrl) {
        var maxWeightCntrl = this.$ctrl('maxWeightLbs');
        this.token.maxWeightLbs = maxWeightCntrl.value;
        maxWeightCntrl.markAsDirty();
    };
    RwyClsdComponent.prototype.setMaxWingSpanFt = function () {
        var maxWingSpanFt = this.$ctrl('maxWingSpanFt');
        this.token.maxWingSpanFt = maxWingSpanFt.value;
        maxWingSpanFt.markAsDirty();
    };
    RwyClsdComponent.prototype.decWeight = function () {
        if (!this.isReadOnly) {
            var maxWeightCntrl = this.$ctrl("maxWeightLbs");
            if (+maxWeightCntrl.value > 0) {
                maxWeightCntrl.setValue(+maxWeightCntrl.value - 1);
                maxWeightCntrl.markAsDirty();
            }
        }
    };
    RwyClsdComponent.prototype.incWeight = function () {
        if (!this.isReadOnly) {
            var maxWeightCntrl = this.$ctrl("maxWeightLbs");
            if (+maxWeightCntrl.value < this.acftWeightRestriction) {
                maxWeightCntrl.setValue(+maxWeightCntrl.value + 1);
                maxWeightCntrl.markAsDirty();
            }
        }
    };
    RwyClsdComponent.prototype.decWingspan = function () {
        if (!this.isReadOnly) {
            var maxWingSpanFt = this.$ctrl("maxWingSpanFt");
            if (+maxWingSpanFt.value > 0) {
                maxWingSpanFt.setValue(+maxWingSpanFt.value - 1);
                maxWingSpanFt.markAsDirty();
            }
        }
    };
    RwyClsdComponent.prototype.incWingspan = function () {
        if (!this.isReadOnly) {
            var maxWingSpanFt = this.$ctrl("maxWingSpanFt");
            if (+maxWingSpanFt.value < this.acftWingRestriction) {
                maxWingSpanFt.setValue(+maxWingSpanFt.value + 1);
                maxWingSpanFt.markAsDirty();
            }
        }
    };
    RwyClsdComponent.prototype.validateReasonCtrls = function (ctrlName) {
        if (this.$ctrl(ctrlName).value === '-1') {
            return true;
        }
        return false;
    };
    RwyClsdComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/rwyclsd/rwyclsd.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            rwyclsd_service_1.RwyClsdService,
            core_1.ChangeDetectorRef,
            data_service_1.DataService,
            angular_l10n_1.TranslationService])
    ], RwyClsdComponent);
    return RwyClsdComponent;
}());
exports.RwyClsdComponent = RwyClsdComponent;
function closureTypeValidator(ctrl) {
    return function (c) {
        if (c.value === -1) {
            if (ctrl) {
                if (ctrl.closureQuestion) {
                    return { 'required': false };
                }
                return null;
            }
        }
        return null;
    };
}
function aircraftRestrictionValidator(ctrl) {
    return function (c) {
        if (ctrl) {
            if (ctrl.checkBothAircraftRestrictionsValidator()) {
                return null;
            }
            else {
                return { 'required': true };
            }
        }
        return null;
    };
}
function effectedAerodromeValidator(c) {
    if (c.value === -1) {
        return { 'required': true };
    }
    return null;
}
function txyRestrictions(ctrl) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!ctrl.checkBothTxyRestrictionsValidator()) {
                return null;
            }
            else {
                return { 'required': true };
            }
        }
        return null;
    };
}
function refreshDummyValidator(c) {
    if (c.value === 0) {
        return { 'required': true };
    }
    return null;
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=rwyclsd.component.js.map