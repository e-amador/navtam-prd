"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RwyClsdCancelComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var location_service_1 = require("../../shared/location.service");
var data_service_1 = require("../../shared/data.service");
var rwyclsd_service_1 = require("./rwyclsd.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var RwyClsdCancelComponent = /** @class */ (function () {
    function RwyClsdCancelComponent(toastr, injector, fb, locationService, closureService, changeDetectionRef, dataService) {
        this.toastr = toastr;
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.closureService = closureService;
        this.changeDetectionRef = changeDetectionRef;
        this.dataService = dataService;
        this.token = null;
        this.isReadOnly = false;
        this.closureReasons = [];
        this.closureReasonId = '';
        this.closureQuestion = false;
        this.isBilingualRegion = false;
        this.subjectSelected = null;
        this.disableIcaoSubjectSelect = true;
        this.disableIcaoConditionSelect = true;
        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
    }
    RwyClsdCancelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.closureService.app;
        this.leafletmap = app.leafletmap;
        this.configureReactiveForm();
        //This "dirty trick" is important to make the parent form not
        //to run the generateIcao until everything is loaded.
        this.$ctrl('refreshDummy').setValue(0);
        this.nsdForm.controls['frmArr'].setErrors({ 'required': true });
        this.loadClosureTypes();
        //this.showDoaMapRegion(app.doaId);
        this.setRadioButtonValues();
        //itemA
        this.dataService.getSdoSubject(this.token.effectedAerodrome)
            .subscribe(function (sdoSubjectResult) {
            _this.subjectSelected = sdoSubjectResult;
            _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
            _this.subjects = [];
            _this.subjects.push(sdoSubjectResult);
            _this.loadMapLocations(app.doaId, true);
            _this.updateMap(sdoSubjectResult, true);
            //rwy
            _this.dataService.getSdoSubject(_this.token.effectedRunway)
                .subscribe(function (sdoSubjectResult) {
                _this.rwySelections = [];
                _this.rwySelections.push(sdoSubjectResult);
                _this.$ctrl('refreshDummy').setValue(1);
            });
        });
        //this.isFormLoadingData = false;
        //this.triggerFormChangeEvent("refresh");
    };
    RwyClsdCancelComponent.prototype.ngOnDestroy = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
            this.leafletmap = null;
        }
    };
    RwyClsdCancelComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    RwyClsdCancelComponent.prototype.getEffectedAerodrome = function () {
        var self = this;
        var app = this.closureService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: 'Start typing to search...'
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/rwyclosurefilter",
                dataType: 'json',
                delay: 500,
                data: function (params, page) {
                    return {
                        Query: params.term,
                        Size: 200,
                        doaId: app.doaId
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === '-1') {
                    return selection.text;
                }
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    RwyClsdCancelComponent.prototype.getRunway = function () {
        var self = this;
        var app = this.closureService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: 'Getting effected runway...'
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/rwyclosurefilter",
                dataType: 'json',
                delay: 500,
                data: function (params, page) {
                    return {
                        Query: params.term,
                        Size: 200,
                        doaId: app.doaId
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return data.suffix + data.designator + ' ' + data.name;
            },
            templateSelection: function (selection) {
                if (selection && selection.id === '-1') {
                    return selection.text;
                }
                return selection.designator;
            },
        };
    };
    RwyClsdCancelComponent.prototype.getEffectedRunway = function () {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: 'Select a runway'
            }
        };
    };
    RwyClsdCancelComponent.prototype.IsBilingualRegion = function () {
        return this.isBilingualRegion;
    };
    RwyClsdCancelComponent.prototype.loadClosureTypes = function () {
        var _this = this;
        this.closureService
            .getClosureReasons()
            .subscribe(function (closureReasons) {
            var closures = closureReasons.map(function (item) {
                if (item.id == _this.token.closureReasonId) {
                    return {
                        id: item.id,
                        text: item.name
                    };
                }
            }).filter(Boolean);
            _this.closureReasons = closures;
            _this.isFormLoadingData = false;
        });
    };
    RwyClsdCancelComponent.prototype.setClosureType = function ($event) {
        var closureTypeCtrl = this.$ctrl('closureReasonId');
        var newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== closureTypeCtrl.value) {
                closureTypeCtrl.setValue(newValue);
                closureTypeCtrl.markAsDirty();
            }
        }
    };
    RwyClsdCancelComponent.prototype.checkClosureQuestion = function () {
        return this.$ctrl('closureQuestion').value;
    };
    RwyClsdCancelComponent.prototype.checkBothAircraftRestrictions = function () {
        var weight = this.$ctrl('acftWeightRestrictions').value;
        var wing = this.$ctrl('acftWingspanRestrictions').value;
        if (weight | wing) {
            return false;
        }
        return true;
    };
    RwyClsdCancelComponent.prototype.getOtherReason = function () {
        var closureQuestionValue = this.$ctrl('closureQuestion');
        var closureTypeCtrl = this.$ctrl('closureReasonId');
        if (closureQuestionValue.value && closureTypeCtrl.value === "Re9") { //if it's other reason'
            return true;
        }
        return false;
    };
    RwyClsdCancelComponent.prototype.getAvailAsTxyway = function () {
        return this.$ctrl('availableAsTaxiway').value;
    };
    RwyClsdCancelComponent.prototype.updateSubjectSelected = function (sdoSubject) {
        var _this = this;
        var subjectId = this.nsdFrmGroup.get('subjectId').value;
        if (subjectId === sdoSubject.id) {
            return;
        }
        if (sdoSubject.id !== subjectId) {
            var subjectCtrl = this.nsdFrmGroup.get('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;
        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe(function (sdoSubjectResult) {
                _this.updateMap(sdoSubjectResult, true);
            });
        }
    };
    RwyClsdCancelComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectLocation: this.token.subjectLocation || '',
            subjectId: this.token.effectedAerodome,
            closureReasonId: [{ value: this.token.closureReasonId, disabled: true }],
            addClosureReason: [{ value: this.token.addClosureReason, disabled: true }],
            closureQuestion: [{ value: this.token.closureQuestion, disabled: true }],
            effectedAerodrome: [{ value: this.token.effectedAerodrome, disabled: true }],
            effectedRunway: [{ value: this.token.effectedRunway, disabled: true }],
            availableAsTaxiway: [{ value: this.token.availableAsTaxiway, disabled: true }],
            txyRestrictions: [{ value: this.token.txyRestrictions, disabled: true }],
            freeTextTaxiwayRestriction: [{ value: this.token.freeTextTaxiwayRestriction, disabled: true }],
            otherReason: [{ value: this.token.otherReason, disabled: true }],
            closureReasonOtherFreeText: [{ value: this.token.closureReasonOtherFreeText, disabled: true }],
            closureReasonOtherFreeTextFr: [{ value: this.token.closureReasonOtherFreeTextFr, disabled: true }],
            acftRestrictions: [{ value: this.token.acftRestrictions, disabled: true }],
            maxWingSpanFt: [{ value: this.token.maxWingSpanFt, disabled: true }],
            maxWeightLbs: [{ value: this.token.maxWeightLbs, disabled: true }],
            refreshDummy: [{ value: 0 }, [refreshDummyValidator]],
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    RwyClsdCancelComponent.prototype.setBilingualRegion = function (value) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
            }
        }
        else {
            if (!this.isReadOnly) {
            }
        }
    };
    RwyClsdCancelComponent.prototype.setRadioButtonValues = function () {
        this.$ctrl('availableAsTaxiway').setValue(this.token.availableAsTaxiway);
        this.$ctrl('txyRestrictions').setValue(this.token.txyRestrictions);
        this.$ctrl('closureQuestion').setValue(this.token.closureQuestion);
        this.$ctrl('acftRestrictions').setValue(this.token.acftRestrictions);
    };
    RwyClsdCancelComponent.prototype.renderMap = function (geoJsonStr) {
        var self = this;
        var interval = window.setInterval(function () {
            if (self.leafletmap && self.leafletmap.isReady()) {
                window.clearInterval(interval);
                // self.leafletmap.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    };
    RwyClsdCancelComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        var _this = this;
        if (sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                this.leafletmap.clearFeaturesOnLayers(['DOA']);
                var subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                var coordinates = geojson.features[0].geometry.coordinates;
                var markerGeojson_1 = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.designator, function () {
                    _this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.designator, function () {
                        if (markerGeojson_1) {
                            _this.leafletmap.setNewMarkerLocation(markerGeojson_1);
                        }
                    });
                });
            }
        }
    };
    RwyClsdCancelComponent.prototype.loadMapLocations = function (doaId, loadingFromProposal) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leafletmap && self.leafletmap.isReady()) {
                    window.clearInterval(interval);
                    // self.leafletmap.addDoa(geoJsonStr, 'DOA');
                    if (loadingFromProposal) {
                        self.updateMap(self.subjectSelected, false);
                    }
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error("DOA Geographic Area Error: " + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    RwyClsdCancelComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    RwyClsdCancelComponent.prototype.makeFirstElement = function (arr, makeFirst, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var len = arr.length;
        for (var i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
    };
    RwyClsdCancelComponent.prototype.markFrmDirty = function () {
        this.nsdForm.controls['frmArr'].markAsDirty();
    };
    RwyClsdCancelComponent.prototype.swapArrayPositions = function (arr, p1, p2) {
        var temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    };
    RwyClsdCancelComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/rwyclsd/rwyclsd-cancel.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            rwyclsd_service_1.RwyClsdService,
            core_1.ChangeDetectorRef,
            data_service_1.DataService])
    ], RwyClsdCancelComponent);
    return RwyClsdCancelComponent;
}());
exports.RwyClsdCancelComponent = RwyClsdCancelComponent;
function refreshDummyValidator(c) {
    if (c.value === 0) {
        return { 'required': true };
    }
    return null;
}
//# sourceMappingURL=rwyclsd-cancel.component.js.map