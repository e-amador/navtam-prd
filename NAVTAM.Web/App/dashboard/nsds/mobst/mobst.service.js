"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MObstService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var windowRef_service_1 = require("../../../common/windowRef.service");
var MObstService = /** @class */ (function () {
    function MObstService(http, winRef) {
        this.http = http;
        this.app = winRef.nativeWindow['app'];
    }
    MObstService.prototype.getMObstTypes = function () {
        return this.http
            .get(this.app.apiUrl + "obstacles/complex")
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    MObstService.prototype.validateLocation = function (location) {
        return this.http
            .get(this.app.apiUrl + "doas/indoa?location=" + location)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    MObstService.prototype.calcCenterLocation = function (fromLocation, toLocation) {
        return this.http
            .get(this.app.apiUrl + "obstacles/calccenter?from=" + fromLocation + "&to=" + toLocation)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    MObstService.prototype.getDoaLocation = function (doaId) {
        return this.http.get(this.app.apiUrl + "doas/GeoJson/?doaId=" + doaId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    MObstService.prototype.handleError = function (error) {
        return Rx_1.Observable.throw(error.statusText);
    };
    MObstService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef])
    ], MObstService);
    return MObstService;
}());
exports.MObstService = MObstService;
//# sourceMappingURL=mobst.service.js.map