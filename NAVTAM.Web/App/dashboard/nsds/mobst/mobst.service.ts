﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject, Observable } from 'rxjs/Rx';

import { WindowRef } from '../../../common/windowRef.service'

export interface IMObstType {
    id: string,
    name: string
}

export interface IInDoaValidResult {
    location: string,
    inDoa: boolean
}

export interface ICenterRadius {
    center: string,
    radius: number,
    valid: boolean
}

@Injectable()
export class MObstService {
    constructor(private http: Http, winRef: WindowRef) {
        this.app = winRef.nativeWindow['app'];
    }

    public app: any;

    getMObstTypes(): Observable<IMObstType[]> {
        return this.http
            .get(`${this.app.apiUrl}obstacles/complex`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    validateLocation(location: string): Observable<IInDoaValidResult> {
        return this.http
            .get(`${this.app.apiUrl}doas/indoa?location=${location}`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    calcCenterLocation(fromLocation: string, toLocation: string): Observable<ICenterRadius> {
        return this.http
            .get(`${this.app.apiUrl}obstacles/calccenter?from=${fromLocation}&to=${toLocation}`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    } 

    getDoaLocation(doaId: number): Observable<any> {
        return this.http.get(`${this.app.apiUrl}doas/GeoJson/?doaId=${doaId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}