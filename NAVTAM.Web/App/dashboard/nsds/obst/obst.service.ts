﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject, Observable } from 'rxjs/Rx';

import { WindowRef } from '../../../common/windowRef.service'

export interface IObstacleType {
    id: string,
    name: string
}

export interface IInDoaValidationResult {
    location: string,
    inDoa: boolean,
    inBilingualRegion: boolean
}

@Injectable()
export class ObstacleService {
    constructor(private http: Http, winRef: WindowRef) {
        this.app = winRef.nativeWindow['app'];
    }

    public app: any;

    getObstacleTypes(): Observable<IObstacleType[]> {
        return this.http
            .get(`${this.app.apiUrl}obstacles/simple`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    validateLocation(location: string): Observable<IInDoaValidationResult> {
        return this.http
            .get(`${this.app.apiUrl}doas/indoa?location=${location}`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getDoaLocation(doaId: number): Observable<any> {
        return this.http.get(`${this.app.apiUrl}doas/GeoJson/?doaId=${doaId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}