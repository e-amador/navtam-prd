﻿import { Component, OnInit, OnDestroy, Inject, Injector } from '@angular/core'
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { DataService } from '../../shared/data.service'
import { LocationService } from '../../shared/location.service'
import { CarsClsdService } from './carsclsd.service'
import { ISdoSubject } from '../../shared/data.model'
import { ICarsClsdStatus } from './carsclsd.model'
import { WindowRef } from './../../../common/windowRef.service'

import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';

import { LocaleService, TranslationService } from 'angular-l10n';

@Component({
    templateUrl: '/app/dashboard/nsds/carsclsd/carsclsd-cancel.component.html'
})
export class CarsClsdCancelComponent implements OnInit, OnDestroy {
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = false;
    leafletmap: any;
    isBilingualRegion: boolean = false;
    isFormLoadingData: boolean;

    btnMondayClass: string = 'btn-white';

    subjectSelected: ISdoSubject = null;
    subjects: ISdoSubject[];

    carsStatus: ICarsClsdStatus[] = [];
    carsStatusId: number = -1;

    disabledDayCheckbox: boolean = true;
    addReason: boolean = false;
    selectedLanguage: string = '';

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        private fb: FormBuilder,
        private locationService: LocationService,
        private winRef: WindowRef,
        private carsClsdService: CarsClsdService,
        private dataService: DataService,
        public locale: LocaleService,
        public translation: TranslationService) {

        this.token = this.injector.get('token');
        this.nsdForm = this.injector.get('form');
    }

    ngOnInit() {
        this.isFormLoadingData = true;
        const app = this.carsClsdService.app;
        this.selectedLanguage = this.winRef.appConfig.cultureInfo;

        this.leafletmap = app.leafletmap;

        this.carsClsdService.getCarsClsdStatus(this.selectedLanguage)
            .subscribe((status: ICarsClsdStatus[]) => {
                this.carsStatus = status;
                this.carsStatusId = this.token.carsStatusId;
                this.setFirstSelectItemInCombo(this.carsStatus, this.token.carsStatusId);

                this.dataService.getSdoSubject(this.token.subjectId)
                    .subscribe((sdoSubjectResult: ISdoSubject) => {
                        this.subjectSelected = sdoSubjectResult;
                        this.subjects = [];
                        this.subjects.push(sdoSubjectResult);

                        this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);

                        this.loadMapLocations(app.doaId, true);
                        this.updateMap(sdoSubjectResult, true);
                        this.isFormLoadingData = false;

                        this.$ctrl('refreshIcao').setValue(1);
                    });
            });

        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveFormLoading();
            //This "dirty trick" is important to make the parent form not
            //to run the generateIcao until everything is loaded.
            this.$ctrl('refreshIcao').setValue(0);
            this.nsdForm.controls['frmArr'].setErrors({ 'required': true });
        } else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup 
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }


    }

    ngOnDestroy() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
            this.leafletmap = null;
        }
    }

    public get IsHrsOperationSelected(): boolean {
        return (this.$ctrl('carsStatusId').value === 1);
    }

    private configureReactiveFormLoading() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];
        if (this.token.reason && this.token.reason.length > 0) this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0) this.addReason = true;

        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, [Validators.required]],
            subjectLocation: this.token.subjectLocation,
            carsStatusId: [{ value: this.token.carsStatusId, disabled: true }],
            everyday: [{ value: this.token.everyday, disabled: true }],
            daysOfWeek: this.fb.group({
                monday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.monday : true, disabled: true }],
                tuesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true, disabled: true }],
                wednesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true, disabled: true }],
                thursday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.thursday : true, disabled: true }],
                friday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.friday : true, disabled: true }],
                saturday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.saturday : true, disabled: true }],
                sunday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.sunday : true, disabled: true }],
            }),
            reason: [{ value: this.token.reason || '', disabled: true }],
            reasonFr: [{ value: this.token.reasonFr || '', disabled: true }],
            startHour: [{ value: this.token.startHour || '', disabled: true }],
            endHour: [{ value: this.token.endHour || '', disabled: true }],
            daylightSavingTime: [{ value: this.token.daylightSavingTime, disabled: true }],
            refreshIcao: [{ value: 0 }, [refreshIcaoValidator]],
        });
        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];
        this.disabledDayCheckbox = (this.isReadOnly) ? true : (this.token.carsStatusId !== 1) ? true : false;
        if (this.token.reason && this.token.reason.length > 0) this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0) this.addReason = true;

        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("carsStatusId", this.token.carsStatusId);
        this.updateFormControlValue("addReason", this.addReason);
        this.updateFormControlValue("everyday", this.token.everyday);
        this.updateFormControlValue("daysOfWeek.monday", (!this.token.everyday) ? this.token.daysOfWeek.monday : true);
        this.updateFormControlValue("daysOfWeek.tuesday", (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true);
        this.updateFormControlValue("daysOfWeek.wednesday", (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true);
        this.updateFormControlValue("daysOfWeek.thursday", (!this.token.everyday) ? this.token.daysOfWeek.thursday : true);
        this.updateFormControlValue("daysOfWeek.friday", (!this.token.everyday) ? this.token.daysOfWeek.friday : true);
        this.updateFormControlValue("daysOfWeek.saturday", (!this.token.everyday) ? this.token.daysOfWeek.saturday : true);
        this.updateFormControlValue("daysOfWeek.sunday", (!this.token.everyday) ? this.token.daysOfWeek.sunday : true);
        this.updateFormControlValue("reason", this.token.reason || '');
        this.updateFormControlValue("reasonFr", this.token.reasonFr || '');
        this.updateFormControlValue("startHour", this.token.startHour || '');
        this.updateFormControlValue("endHour", this.token.endHour || '');
        this.updateFormControlValue("daylightSavingTime", this.token.daylightSavingTime);
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    //Validation functions
    public validateSubjectId() {
        const fc = this.$ctrl('subjectId');
        return fc.value || this.nsdFrmGroup.pristine;
    }

    public validateCarsStatusId() {
        const fc = this.$ctrl('carsStatusId');
        return (fc.value > -1) || this.nsdFrmGroup.pristine;
    }

    //Miscelaneas functions
    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    public carsStatusOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.CarsStatusPlaceholder')
        }
    }

    public sdoSubjectWithCarsOptions(): any {
        const self = this;
        const app = this.carsClsdService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "carsclsd/filterwithcars",
                dataType: 'json',
                delay: 500,
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }

    private updateMap(sdoSubject: ISdoSubject, acceptSubjectLocation: boolean) {
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                const coordinates = geojson.features[0].geometry.coordinates;
                const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, () => {
                        if (markerGeojson) {
                            this.leafletmap.setNewMarkerLocation(markerGeojson);
                        }
                    });
                });
            }
        }

    }

    private setFirstSelectItemInCombo(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        if (this.isFormLoadingData) {
            const index = items.findIndex(x => x.id === itemId);
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        } else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                    textFr: '',
                });
            }
        }
    }

    private loadMapLocations(doaId: number, loadingFromProposal: boolean) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leafletmap && self.leafletmap.isReady()) {
                        window.clearInterval(interval);
                        self.leafletmap.addDoa(geoJsonStr, 'DOA');
                        if (loadingFromProposal) {
                            self.updateMap(self.subjectSelected, false);
                        }
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error(this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private setBilingualRegion(value: boolean) {
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                //Do something
            }
        } else {
            if (!this.isReadOnly) {
                //Do something
            }
        }
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }

}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

function refreshIcaoValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === 0) {
        return { 'required': true }
    }
    return null;
}
