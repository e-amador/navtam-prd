"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CarsClsdCancelComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var carsclsd_service_1 = require("./carsclsd.service");
var windowRef_service_1 = require("./../../../common/windowRef.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var CarsClsdCancelComponent = /** @class */ (function () {
    function CarsClsdCancelComponent(toastr, injector, fb, locationService, winRef, carsClsdService, dataService, locale, translation) {
        this.toastr = toastr;
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.winRef = winRef;
        this.carsClsdService = carsClsdService;
        this.dataService = dataService;
        this.locale = locale;
        this.translation = translation;
        this.token = null;
        this.isReadOnly = false;
        this.isBilingualRegion = false;
        this.btnMondayClass = 'btn-white';
        this.subjectSelected = null;
        this.carsStatus = [];
        this.carsStatusId = -1;
        this.disabledDayCheckbox = true;
        this.addReason = false;
        this.selectedLanguage = '';
        this.carsStatusOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.CarsStatusPlaceholder')
            }
        };
        this.token = this.injector.get('token');
        this.nsdForm = this.injector.get('form');
    }
    CarsClsdCancelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isFormLoadingData = true;
        var app = this.carsClsdService.app;
        this.selectedLanguage = this.winRef.appConfig.cultureInfo;
        this.leafletmap = app.leafletmap;
        this.carsClsdService.getCarsClsdStatus(this.selectedLanguage)
            .subscribe(function (status) {
            _this.carsStatus = status;
            _this.carsStatusId = _this.token.carsStatusId;
            _this.setFirstSelectItemInCombo(_this.carsStatus, _this.token.carsStatusId);
            _this.dataService.getSdoSubject(_this.token.subjectId)
                .subscribe(function (sdoSubjectResult) {
                _this.subjectSelected = sdoSubjectResult;
                _this.subjects = [];
                _this.subjects.push(sdoSubjectResult);
                _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                _this.loadMapLocations(app.doaId, true);
                _this.updateMap(sdoSubjectResult, true);
                _this.isFormLoadingData = false;
                _this.$ctrl('refreshIcao').setValue(1);
            });
        });
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveFormLoading();
            //This "dirty trick" is important to make the parent form not
            //to run the generateIcao until everything is loaded.
            this.$ctrl('refreshIcao').setValue(0);
            this.nsdForm.controls['frmArr'].setErrors({ 'required': true });
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
    };
    CarsClsdCancelComponent.prototype.ngOnDestroy = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
            this.leafletmap = null;
        }
    };
    Object.defineProperty(CarsClsdCancelComponent.prototype, "IsHrsOperationSelected", {
        get: function () {
            return (this.$ctrl('carsStatusId').value === 1);
        },
        enumerable: false,
        configurable: true
    });
    CarsClsdCancelComponent.prototype.configureReactiveFormLoading = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        if (this.token.reason && this.token.reason.length > 0)
            this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0)
            this.addReason = true;
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, [forms_1.Validators.required]],
            subjectLocation: this.token.subjectLocation,
            carsStatusId: [{ value: this.token.carsStatusId, disabled: true }],
            everyday: [{ value: this.token.everyday, disabled: true }],
            daysOfWeek: this.fb.group({
                monday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.monday : true, disabled: true }],
                tuesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true, disabled: true }],
                wednesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true, disabled: true }],
                thursday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.thursday : true, disabled: true }],
                friday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.friday : true, disabled: true }],
                saturday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.saturday : true, disabled: true }],
                sunday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.sunday : true, disabled: true }],
            }),
            reason: [{ value: this.token.reason || '', disabled: true }],
            reasonFr: [{ value: this.token.reasonFr || '', disabled: true }],
            startHour: [{ value: this.token.startHour || '', disabled: true }],
            endHour: [{ value: this.token.endHour || '', disabled: true }],
            daylightSavingTime: [{ value: this.token.daylightSavingTime, disabled: true }],
            refreshIcao: [{ value: 0 }, [refreshIcaoValidator]],
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    CarsClsdCancelComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        this.disabledDayCheckbox = (this.isReadOnly) ? true : (this.token.carsStatusId !== 1) ? true : false;
        if (this.token.reason && this.token.reason.length > 0)
            this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0)
            this.addReason = true;
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("carsStatusId", this.token.carsStatusId);
        this.updateFormControlValue("addReason", this.addReason);
        this.updateFormControlValue("everyday", this.token.everyday);
        this.updateFormControlValue("daysOfWeek.monday", (!this.token.everyday) ? this.token.daysOfWeek.monday : true);
        this.updateFormControlValue("daysOfWeek.tuesday", (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true);
        this.updateFormControlValue("daysOfWeek.wednesday", (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true);
        this.updateFormControlValue("daysOfWeek.thursday", (!this.token.everyday) ? this.token.daysOfWeek.thursday : true);
        this.updateFormControlValue("daysOfWeek.friday", (!this.token.everyday) ? this.token.daysOfWeek.friday : true);
        this.updateFormControlValue("daysOfWeek.saturday", (!this.token.everyday) ? this.token.daysOfWeek.saturday : true);
        this.updateFormControlValue("daysOfWeek.sunday", (!this.token.everyday) ? this.token.daysOfWeek.sunday : true);
        this.updateFormControlValue("reason", this.token.reason || '');
        this.updateFormControlValue("reasonFr", this.token.reasonFr || '');
        this.updateFormControlValue("startHour", this.token.startHour || '');
        this.updateFormControlValue("endHour", this.token.endHour || '');
        this.updateFormControlValue("daylightSavingTime", this.token.daylightSavingTime);
    };
    CarsClsdCancelComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    //Validation functions
    CarsClsdCancelComponent.prototype.validateSubjectId = function () {
        var fc = this.$ctrl('subjectId');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    CarsClsdCancelComponent.prototype.validateCarsStatusId = function () {
        var fc = this.$ctrl('carsStatusId');
        return (fc.value > -1) || this.nsdFrmGroup.pristine;
    };
    //Miscelaneas functions
    CarsClsdCancelComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    CarsClsdCancelComponent.prototype.sdoSubjectWithCarsOptions = function () {
        var self = this;
        var app = this.carsClsdService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "carsclsd/filterwithcars",
                dataType: 'json',
                delay: 500,
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    CarsClsdCancelComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        var _this = this;
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                var subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                var coordinates = geojson.features[0].geometry.coordinates;
                var markerGeojson_1 = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, function () {
                    _this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, function () {
                        if (markerGeojson_1) {
                            _this.leafletmap.setNewMarkerLocation(markerGeojson_1);
                        }
                    });
                });
            }
        }
    };
    CarsClsdCancelComponent.prototype.setFirstSelectItemInCombo = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        if (this.isFormLoadingData) {
            var index = items.findIndex(function (x) { return x.id === itemId; });
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        }
        else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                    textFr: '',
                });
            }
        }
    };
    CarsClsdCancelComponent.prototype.loadMapLocations = function (doaId, loadingFromProposal) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leafletmap && self.leafletmap.isReady()) {
                    window.clearInterval(interval);
                    self.leafletmap.addDoa(geoJsonStr, 'DOA');
                    if (loadingFromProposal) {
                        self.updateMap(self.subjectSelected, false);
                    }
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    CarsClsdCancelComponent.prototype.setBilingualRegion = function (value) {
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                //Do something
            }
        }
        else {
            if (!this.isReadOnly) {
                //Do something
            }
        }
    };
    CarsClsdCancelComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    CarsClsdCancelComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/carsclsd/carsclsd-cancel.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            windowRef_service_1.WindowRef,
            carsclsd_service_1.CarsClsdService,
            data_service_1.DataService,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], CarsClsdCancelComponent);
    return CarsClsdCancelComponent;
}());
exports.CarsClsdCancelComponent = CarsClsdCancelComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
function refreshIcaoValidator(c) {
    if (c.value === 0) {
        return { 'required': true };
    }
    return null;
}
//# sourceMappingURL=carsclsd-cancel.component.js.map