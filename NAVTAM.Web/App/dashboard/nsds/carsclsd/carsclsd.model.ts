﻿import { ISdoSubject } from '../../shared/data.model'

export interface ICarsClsdStatus {
    id: number,
    text: string,
    //textFr: string,
}

export interface IDaysOfOperation {
    monday: boolean,
    tuesday: boolean,
    wednesday: boolean,
    thursday: boolean,
    friday: boolean,
    saturday: boolean,
    sunday: boolean
}

export interface ICarsClsdViewModel {
    carsStatusId: number,
    startHour: string,
    endHour: string,
    everyday: boolean,
    daysOfWeek: IDaysOfOperation,
    daylightSavingTime: boolean,
    reason: string,
    reasonFr: string,
    subjectLocation: string
}