﻿import { Component, OnInit, Input, OnChanges } from '@angular/core'
import { ProposalStatus, ProposalType } from '../shared/data.model';
import { LocaleService, TranslationService } from 'angular-l10n';

import * as moment from 'moment';

@Component({ 
    selector: 'icao',
    templateUrl: '/app/dashboard/nsds/nsd-icao.component.html'
})
export class NsdIcaoComponent implements OnInit, OnChanges {

    @Input() model : any;
    @Input() loading: boolean;

    startActivityText: string;
    endValidityText: string;
    lowerLimit: string;
    upperLimit: string;

    constructor(
        public translation: TranslationService) {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (this.model.coordinates) {
            this.updateIcaoCoordinates(this.model.coordinates, this.model.radius);
        }

        this.startActivityText = this.formatStartActivityDate();
        this.endValidityText = this.formatEndValidityDate();

        this.lowerLimit = ("000" + this.model.lowerLimit).slice(-3);
        this.upperLimit = ("000" + this.model.upperLimit).slice(-3);

    }

    private updateIcaoCoordinates(coordinates: string, radius: any) {
        const coordinatesParts = coordinates.split(" ");
        if (coordinatesParts.length === 2) {
            radius = radius.pad(3);
            this.model.icaoCoordinates = coordinatesParts[0] + coordinatesParts[1] + radius;
        }
    }

    private formatStartActivityDate(): string {
        if (this.model.proposalType == ProposalType.Cancel) {
            return this.translation.translate('IcaoText.Immediate'); // "IMMEDIATE";
        }

        const startDate = moment.utc(this.model.startActivity);

        const diseminatedOrModified = this.model.status === ProposalStatus.Disseminated || this.model.status === ProposalStatus.DisseminatedModified
        if (diseminatedOrModified) {
            return startDate.isValid() ? startDate.format("YYMMDDHHmm") + "   [" + startDate.format("MM/DD/YYYY, HH:mm") + "]" : "";
        }

        if (this.model.token.immediate) {
            return this.translation.translate('IcaoText.Immediate'); //"IMMEDIATE";
        } else {
            return startDate.isValid() ? startDate.format("YYMMDDHHmm") + "   [" + startDate.format("MM/DD/YYYY, HH:mm") + "]" : "";
        }
    }

    private formatEndValidityDate(): string {
        if (this.model.proposalType == ProposalType.Cancel) {
            return "";
        }

        const endDate = moment.utc(this.model.endValidity);

        if (this.model.token.permanent) {
            return "PERM";
        } else {
            return endDate.isValid() ? endDate.format("YYMMDDHHmm") + (this.model.token.estimated ? " EST":"") + " [" + endDate.format("MM/DD/YYYY, HH:mm") + "]": "";
        }
    }

}
