"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRoutes = void 0;
var user_list_component_1 = require("./user/user-list.component");
var user_create_component_1 = require("./user/user-create.component");
var user_edit_component_1 = require("./user/user-edit.component");
var user_view_component_1 = require("./user/user-view.component");
var user_viewhistory_component_1 = require("./user/user-viewhistory.component");
var report_container_component_1 = require("./shared/report-container.component");
var categories_resolver_1 = require("./resolvers/categories.resolver");
var proposal_model_resolver_1 = require("./resolvers/proposal-model.resolver");
var proposal_readonlymodel_resolver_1 = require("./resolvers/proposal-readonlymodel.resolver");
var proposal_createmodel_resolver_1 = require("./resolvers/proposal-createmodel.resolver");
var proposal_replacemodel_resolver_1 = require("./resolvers/proposal-replacemodel.resolver");
var proposal_cancelmodel_resolver_1 = require("./resolvers/proposal-cancelmodel.resolver");
var proposal_clonemodel_resolver_1 = require("./resolvers/proposal-clonemodel.resolver");
var proposal_historymodel_resolver_1 = require("./resolvers/proposal-historymodel.resolver");
exports.userRoutes = [
    { path: 'dashboard', component: user_list_component_1.UserListComponent,
        resolve: { categories: categories_resolver_1.CategoriesResolver }
    },
    {
        path: 'dashboard/create/:catid', component: user_create_component_1.UserCreateComponent,
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_createmodel_resolver_1.ProposalCreateModelResolver }
    },
    {
        path: 'dashboard/edit/:id', component: user_edit_component_1.UserEditComponent,
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_model_resolver_1.ProposalModelResolver }
    },
    {
        path: 'dashboard/clone/:id/:history', component: user_edit_component_1.UserEditComponent,
        data: [{ type: 'clone' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_clonemodel_resolver_1.ProposalCloneModelResolver }
    },
    {
        path: 'dashboard/view/:id', component: user_view_component_1.UserViewComponent,
        data: [{ type: 'draft' }],
        resolve: { model: proposal_readonlymodel_resolver_1.ProposalReadOnlyModelResolver }
    },
    {
        path: 'dashboard/replace/:id', component: user_edit_component_1.UserEditComponent,
        data: [{ type: 'replace' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_replacemodel_resolver_1.ProposalReplaceModelResolver }
    },
    {
        path: 'dashboard/reports', component: report_container_component_1.ReportContainerComponent,
    },
    {
        path: 'dashboard/cancel/:id', component: user_edit_component_1.UserEditComponent,
        data: [{ type: 'cancel' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_cancelmodel_resolver_1.ProposalCancelModelResolver }
    },
    {
        path: 'dashboard/viewhistory/:id', component: user_viewhistory_component_1.UserViewHistoryComponent,
        data: [{ type: 'draft' }],
        resolve: { model: proposal_historymodel_resolver_1.ProposalHistoryModelResolver }
    },
    {
        path: 'path/*', redirectTo: '/dashboard', pathMatch: 'full'
    }
];
//# sourceMappingURL=usr.routes.js.map