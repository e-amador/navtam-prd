"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.nofRoutes = void 0;
var nof_portal_component_1 = require("./nof/nof-portal.component");
var nof_pick_component_1 = require("./nof/nof-pick.component");
var nof_create_component_1 = require("./nof/nof-create.component");
var nof_view_component_1 = require("./nof/nof-view.component");
var nof_msg_compose_component_1 = require("./nof/nof-msg-compose.component");
var nof_msg_read_component_1 = require("./nof/nof-msg-read.component");
var nof_msg_template_list_component_1 = require("./nof/nof-msg-template-list.component");
var nof_msg_template_create_component_1 = require("./nof/nof-msg-template-create.component");
var nof_msg_template_edit_component_1 = require("./nof/nof-msg-template-edit.component");
var nof_viewhistory_component_1 = require("./nof/nof-viewhistory.component");
var categories_resolver_1 = require("./resolvers/categories.resolver");
var proposal_createmodel_resolver_1 = require("./resolvers/proposal-createmodel.resolver");
var proposal_model_resolver_1 = require("./resolvers/proposal-model.resolver");
var proposal_clonemodel_resolver_1 = require("./resolvers/proposal-clonemodel.resolver");
var notam_clonemodel_resolver_1 = require("./resolvers/notam-clonemodel.resolver");
var proposal_readonlymodel_resolver_1 = require("./resolvers/proposal-readonlymodel.resolver");
var notam_proposalmodel_resolver_1 = require("./resolvers/notam-proposalmodel.resolver");
var proposal_historymodel_resolver_1 = require("./resolvers/proposal-historymodel.resolver");
var proposal_replacemodel_resolver_1 = require("./resolvers/proposal-replacemodel.resolver");
var proposal_cancelmodel_resolver_1 = require("./resolvers/proposal-cancelmodel.resolver");
var nof_message_resolver_1 = require("./resolvers/nof-message.resolver");
var nof_msg_template_resolver_1 = require("./resolvers/nof-msg-template.resolver");
exports.nofRoutes = [
    { path: 'dashboard', component: nof_portal_component_1.NofPortalComponent,
        resolve: { categories: categories_resolver_1.CategoriesResolver }
    },
    {
        path: 'dashboard/create/:catid', component: nof_create_component_1.NofCreateComponent,
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_createmodel_resolver_1.ProposalCreateModelResolver }
    },
    {
        path: 'dashboard/pick/:id', component: nof_pick_component_1.NofPickComponent,
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_model_resolver_1.ProposalModelResolver }
    },
    {
        path: 'dashboard/clone/:id/:history', component: nof_pick_component_1.NofPickComponent,
        data: [{ type: 'clone' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_clonemodel_resolver_1.ProposalCloneModelResolver }
    },
    {
        path: 'dashboard/notamclone/:id', component: nof_pick_component_1.NofPickComponent,
        data: [{ type: 'clone' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: notam_clonemodel_resolver_1.NotamCloneModelResolver }
    },
    {
        path: 'dashboard/notamview/:id', component: nof_view_component_1.NofViewComponent,
        data: [{ type: 'draft' }],
        resolve: { model: notam_proposalmodel_resolver_1.NotamProposalReadOnlyModelResolver }
    },
    {
        path: 'dashboard/view/:id', component: nof_view_component_1.NofViewComponent,
        data: [{ type: 'draft' }],
        resolve: { model: proposal_readonlymodel_resolver_1.ProposalReadOnlyModelResolver }
    },
    {
        path: 'dashboard/replace/:id/:source', component: nof_pick_component_1.NofPickComponent,
        data: [{ type: 'replace' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_replacemodel_resolver_1.ProposalReplaceModelResolver }
    },
    {
        path: 'dashboard/cancel/:id/:source', component: nof_pick_component_1.NofPickComponent,
        data: [{ type: 'cancel' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: proposal_cancelmodel_resolver_1.ProposalCancelModelResolver }
    },
    {
        path: 'dashboard/viewhistory/:id', component: nof_viewhistory_component_1.NofViewHistoryComponent,
        data: [{ type: 'draft' }],
        resolve: { model: proposal_historymodel_resolver_1.ProposalHistoryModelResolver }
    },
    {
        path: 'dashboard/messages/templates', component: nof_msg_template_list_component_1.NofMsgTemplateListComponent,
    },
    {
        path: 'dashboard/messages/templates/create', component: nof_msg_template_create_component_1.NofMsgTemplateCreateComponent,
    },
    {
        path: 'dashboard/messages/templates/edit/:id', component: nof_msg_template_edit_component_1.NofMsgTemplateEditComponent,
        resolve: { model: nof_msg_template_resolver_1.NofMessageTemplateResolver }
    },
    {
        path: 'dashboard/messages/compose', component: nof_msg_compose_component_1.NofMsgComposeComponent,
    },
    {
        path: 'dashboard/messages/compose/:id', component: nof_msg_compose_component_1.NofMsgComposeComponent,
        resolve: { model: nof_message_resolver_1.NofMessageResolver }
    },
    {
        path: 'dashboard/messages/read/:id', component: nof_msg_read_component_1.NofMsgReadComponent,
        resolve: { model: nof_message_resolver_1.NofMessageResolver }
    },
    { path: 'path/*', redirectTo: '/dashboard', pathMatch: 'full' }
];
//# sourceMappingURL=nof.routes.js.map