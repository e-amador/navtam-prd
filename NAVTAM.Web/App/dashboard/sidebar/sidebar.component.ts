﻿import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core'

import { DataService } from '../shared/data.service'
import { NotifyService } from '../shared/notify.service'
import { INsdCategory } from '../shared/data.model'

@Component({
    selector: 'sidebar',
    templateUrl: '/app/dashboard/sidebar/sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    
    nsdCategories: INsdCategory[]
    selectedCategory : INsdCategory;

    constructor(private dataService : DataService, private notifyService: NotifyService ) {
        this.notifyService.subscribeCategorySubjectEvent()
            .subscribe((categoryId) => this.onCategoryChanged(categoryId) );
    }

    ngOnInit() {
    }

    onCategoryChanged(categoryId: number) {
        if( this.selectedCategory && this.selectedCategory.id === categoryId){
            return;
        }

        this.dataService.getNsdCategories()
            .subscribe(categories => {
                if (categories.length > 0) {
                    this.nsdCategories = categories;
                    categories[0].isSelected = true;    
                    this.getNotamsByCategory(categoryId);
                }
            });
    }

    getNotamsByCategory(categoryId) {
        if( this.selectedCategory ) {
            this.selectedCategory.isSelected = false;
        }

        let category = this.findCategory(categoryId);

        if( category ) {
            this.selectedCategory = category;
            this.selectedCategory.isSelected = true;

            this.notifyService.emitCategorySubjectEvent(categoryId);
        }
    }

    findCategory(categoryId) {
        let categories = 
            this.nsdCategories.filter(item=> item.id === categoryId);

        if(categories.length === 0) {
            for(let i=0; i < this.nsdCategories.length; i++) {
                var children = this.nsdCategories[i].children;
                if( children ) {
                    categories = children.filter(item=> item.id === categoryId);
                    if( categories.length > 0) {
                        break;
                    }
                }
            }
        }

        return categories.length === 1 ? categories[0] : null;
    }
}