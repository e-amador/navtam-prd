﻿import { Routes } from '@angular/router';
import { NofPortalComponent } from './nof/nof-portal.component';
import { NofPickComponent } from './nof/nof-pick.component';
import { NofCreateComponent } from './nof/nof-create.component';
import { NofViewComponent } from './nof/nof-view.component';
import { NofMsgComposeComponent } from './nof/nof-msg-compose.component';
import { NofMsgReadComponent } from './nof/nof-msg-read.component';
import { NofMsgTemplateListComponent } from './nof/nof-msg-template-list.component';
import { NofMsgTemplateCreateComponent } from './nof/nof-msg-template-create.component';
import { NofMsgTemplateEditComponent } from './nof/nof-msg-template-edit.component';
import { NofViewHistoryComponent } from './nof/nof-viewhistory.component';

import { CategoriesResolver } from './resolvers/categories.resolver';
import { ProposalCreateModelResolver } from './resolvers/proposal-createmodel.resolver';
import { ProposalModelResolver } from './resolvers/proposal-model.resolver';
import { ProposalCloneModelResolver } from './resolvers/proposal-clonemodel.resolver';
import { NotamCloneModelResolver } from './resolvers/notam-clonemodel.resolver';
import { ProposalReadOnlyModelResolver } from './resolvers/proposal-readonlymodel.resolver';
import { NotamProposalReadOnlyModelResolver } from './resolvers/notam-proposalmodel.resolver';
import { ProposalHistoryModelResolver } from './resolvers/proposal-historymodel.resolver';
import { ProposalReplaceModelResolver } from './resolvers/proposal-replacemodel.resolver';
import { ProposalCancelModelResolver } from './resolvers/proposal-cancelmodel.resolver';
import { NofMessageResolver } from './resolvers/nof-message.resolver';
import { NofMessageTemplateResolver } from './resolvers/nof-msg-template.resolver';


export const nofRoutes: Routes = [
    { path: 'dashboard', component: NofPortalComponent,
        resolve: { categories: CategoriesResolver }
    },
    {
        path: 'dashboard/create/:catid', component: NofCreateComponent,
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalCreateModelResolver }
    },
    {
        path: 'dashboard/pick/:id', component: NofPickComponent,
        data: [{ type: 'draft' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalModelResolver }
    },
    {
        path: 'dashboard/clone/:id/:history', component: NofPickComponent,
        data: [{ type: 'clone' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalCloneModelResolver }
    },
    {
        path: 'dashboard/notamclone/:id', component: NofPickComponent,
        data: [{ type: 'clone' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: NotamCloneModelResolver }
    },
    {
        path: 'dashboard/notamview/:id', component: NofViewComponent,
        data: [{ type: 'draft' }],
        resolve: { model: NotamProposalReadOnlyModelResolver }
    },
    {
        path: 'dashboard/view/:id', component: NofViewComponent,
        data: [{ type: 'draft' }],
        resolve: { model: ProposalReadOnlyModelResolver }
    },
    {
        path: 'dashboard/replace/:id/:source', component: NofPickComponent,
        data: [{ type: 'replace' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalReplaceModelResolver }
    },
    {
        path: 'dashboard/cancel/:id/:source', component: NofPickComponent,
        data: [{ type: 'cancel' }],
        canDeactivate: ['canDeactivateCreateEvent'],
        resolve: { model: ProposalCancelModelResolver }
    },
    {
        path: 'dashboard/viewhistory/:id', component: NofViewHistoryComponent,
        data: [{ type: 'draft' }],
        resolve: { model: ProposalHistoryModelResolver }
    },
    {
        path: 'dashboard/messages/templates', component: NofMsgTemplateListComponent,
    },
    {
        path: 'dashboard/messages/templates/create', component: NofMsgTemplateCreateComponent,
    },
    {
        path: 'dashboard/messages/templates/edit/:id', component: NofMsgTemplateEditComponent,
        resolve: { model: NofMessageTemplateResolver }
    },
    {
        path: 'dashboard/messages/compose', component: NofMsgComposeComponent,
    },
    {
        path: 'dashboard/messages/compose/:id', component: NofMsgComposeComponent,
        resolve: { model: NofMessageResolver }
    },
    {
        path: 'dashboard/messages/read/:id', component: NofMsgReadComponent,
        resolve: { model: NofMessageResolver }
    },
    { path: 'path/*', redirectTo: '/dashboard', pathMatch: 'full' }
]