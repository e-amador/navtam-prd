﻿import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { DataService } from '../shared/data.service'
import { ProposalStatus } from '../shared/data.model';

import { NsdFormComponent } from '../nsds/nsd-form.component'

@Component({ 
    templateUrl: '/app/dashboard/user/user-edit.component.html'
})
export class UserEditComponent implements OnInit, OnDestroy  {
    
    model : any = null;
    nsdForm: NsdFormComponent;
    title: string;
    faIcon: string;
    proposalOwner: string;

    @ViewChild(NsdFormComponent)
    set form(v: NsdFormComponent){
        this.nsdForm = v;
    }

    constructor(
        private dataService : DataService, 
        private activatedRoute: ActivatedRoute, 
        private router: Router) {

        switch (activatedRoute.snapshot.data[0]['type']) {
            case 'draft':
            case 'clone':
                this.title = "Edit";
                this.faIcon = "icon-plus";
                break;
            case 'replace':
                this.title = "Replace";
                this.faIcon = "fa fa-code-fork";
                break;
            case 'cancel':
                this.title = "Cancel";
                this.faIcon = "fa fa-hand-paper-o";
                break;
            default:
                this.title = "Edit";
                this.faIcon = "icon-plus";
                break;
        }  
    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];

        if (this.model.status == ProposalStatus.Taken) {
            this.proposalOwner = this.model.operator;

            const self = this;
            const el = $('#modal-proposaltaken');

            (<any>el).modal({});

            el.on('hide.bs.modal', function (e) {
                if (self.proposalOwner) {
                    self.router.navigate(['/dashboard']);
                }
            })
        }
    }


    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

    backToDashboard() {
        this.proposalOwner = null;
        this.router.navigate(['/dashboard']);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }
}