﻿import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { NsdFormComponent } from '../nsds/nsd-form.component'

@Component({ 
    templateUrl: '/app/dashboard/user/user-view.component.html'
})
export class UserViewComponent implements OnInit, OnDestroy  {
    
    model : any = null;
    nsdForm : NsdFormComponent;

    @ViewChild(NsdFormComponent)
    set form(v: NsdFormComponent){
        this.nsdForm = v;
    }

    constructor(private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    }

    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

}