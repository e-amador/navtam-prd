"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var notify_service_1 = require("../shared/notify.service");
var ProposalListComponent = (function () {
    function ProposalListComponent(toaster, dataService, notifyService, route) {
        this.toaster = toaster;
        this.dataService = dataService;
        this.notifyService = notifyService;
        this.route = route;
        this.loadingData = false;
        this.tableHeaders = {
            sorting: [
                { columnName: 'NotamId', direction: 1 },
                { columnName: 'CategoryId', direction: 0 },
                { columnName: 'ItemA', direction: 0 },
                { columnName: 'ItemE', direction: 0 },
                { columnName: 'StartActivity', direction: 0 },
                { columnName: 'EndValidity', direction: 0 },
                { columnName: 'Status', direction: 0 }
            ],
            itemsPerPage: [
                { id: '6', text: '6' },
                { id: '15', text: '15' }
            ]
        };
        this.itemsPerPageStartValue = this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            page: 1,
            pageSize: +this.itemsPerPageStartValue,
            sort: this.tableHeaders.sorting[0].columnName,
        };
    }
    ProposalListComponent.prototype.ngOnInit = function () {
        this.categoriesData = this.route.snapshot.data['categories'];
        this.categoryStartValue = this.categoriesData[0].id;
        this.currentCategoryId = +this.categoryStartValue;
        this.getProposalsByCategory();
    };
    ProposalListComponent.prototype.ngAfterViewInit = function () {
        setTimeout(function () {
            $('.loader-overlay').addClass('loaded');
            $('body > section').animate({
                opacity: 1,
            }, 200);
        }, 400);
    };
    ProposalListComponent.prototype.categoryChanged = function (e) {
        this.currentCategoryId = e.value;
        this.getProposalsByCategory();
    };
    ProposalListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getProposalsByCategory();
    };
    ProposalListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    ProposalListComponent.prototype.cssIcon = function (proposal) {
        switch (proposal.status) {
            case 1: return 'fa-save'; //draft
            case 5: return 'fa-upload'; //submitted
            case 19: return 'fa-wifi'; //diseminated
            case 4: return 'fa-close'; //rejected
            default: return 'fa-save';
        }
    };
    ProposalListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getProposalsByCategory();
    };
    ProposalListComponent.prototype.selectProposal = function (proposal) {
        if (this.selectedProposal) {
            this.selectedProposal.isSelected = false;
        }
        this.selectedProposal = proposal;
        this.selectedProposal.isSelected = true;
    };
    ProposalListComponent.prototype.getProposalsByCategory = function () {
        var _this = this;
        try {
            this.loadingData = true;
            this.dataService.getProposalsByCategory(this.currentCategoryId, this.queryOptions)
                .subscribe(function (proposals) {
                if (proposals.data.length > 0) {
                    _this.proposals = proposals.data;
                    _this.proposals[0].isSelected = true;
                    _this.selectedProposal = _this.proposals[0];
                    _this.loadingData = false;
                }
                else {
                    _this.proposals = [];
                    _this.selectedProposal = null;
                    _this.loadingData = false;
                }
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    return ProposalListComponent;
}());
ProposalListComponent = __decorate([
    core_1.Component({
        templateUrl: '/app/dashboard/proposals/proposal-list.component.html',
        styleUrls: ['app/dashboard/proposals/proposal-list.component.css']
    }),
    __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
    __metadata("design:paramtypes", [Object, data_service_1.DataService,
        notify_service_1.NotifyService,
        router_1.ActivatedRoute])
], ProposalListComponent);
exports.ProposalListComponent = ProposalListComponent;
//# sourceMappingURL=proposal-list.component.js.map