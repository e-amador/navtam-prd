"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEditComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var nsd_form_component_1 = require("../nsds/nsd-form.component");
var UserEditComponent = /** @class */ (function () {
    function UserEditComponent(dataService, activatedRoute, router) {
        this.dataService = dataService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.model = null;
        switch (activatedRoute.snapshot.data[0]['type']) {
            case 'draft':
            case 'clone':
                this.title = "Edit";
                this.faIcon = "icon-plus";
                break;
            case 'replace':
                this.title = "Replace";
                this.faIcon = "fa fa-code-fork";
                break;
            case 'cancel':
                this.title = "Cancel";
                this.faIcon = "fa fa-hand-paper-o";
                break;
            default:
                this.title = "Edit";
                this.faIcon = "icon-plus";
                break;
        }
    }
    Object.defineProperty(UserEditComponent.prototype, "form", {
        set: function (v) {
            this.nsdForm = v;
        },
        enumerable: false,
        configurable: true
    });
    UserEditComponent.prototype.ngOnInit = function () {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
        if (this.model.status == data_model_1.ProposalStatus.Taken) {
            this.proposalOwner = this.model.operator;
            var self_1 = this;
            var el = $('#modal-proposaltaken');
            el.modal({});
            el.on('hide.bs.modal', function (e) {
                if (self_1.proposalOwner) {
                    self_1.router.navigate(['/dashboard']);
                }
            });
        }
    };
    UserEditComponent.prototype.ngOnDestroy = function () {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    };
    UserEditComponent.prototype.backToDashboard = function () {
        this.proposalOwner = null;
        this.router.navigate(['/dashboard']);
    };
    Object.defineProperty(UserEditComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        core_1.ViewChild(nsd_form_component_1.NsdFormComponent),
        __metadata("design:type", nsd_form_component_1.NsdFormComponent),
        __metadata("design:paramtypes", [nsd_form_component_1.NsdFormComponent])
    ], UserEditComponent.prototype, "form", null);
    UserEditComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/user/user-edit.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], UserEditComponent);
    return UserEditComponent;
}());
exports.UserEditComponent = UserEditComponent;
//# sourceMappingURL=user-edit.component.js.map