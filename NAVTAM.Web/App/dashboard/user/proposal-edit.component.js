"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var proposal_form_component_1 = require("./proposal-form.component");
var ProposalEditComponent = (function () {
    function ProposalEditComponent(dataService, activatedRoute, router) {
        this.dataService = dataService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.model = null;
    }
    Object.defineProperty(ProposalEditComponent.prototype, "form", {
        set: function (v) {
            this.proposalForm = v;
        },
        enumerable: true,
        configurable: true
    });
    ProposalEditComponent.prototype.ngOnInit = function () {
        window['loadBingMapScript'].call(this, 'loadMapScenario');
        this.model = this.activatedRoute.snapshot.data['model'];
    };
    return ProposalEditComponent;
}());
__decorate([
    core_1.ViewChild(proposal_form_component_1.ProposalFormComponent),
    __metadata("design:type", proposal_form_component_1.ProposalFormComponent),
    __metadata("design:paramtypes", [proposal_form_component_1.ProposalFormComponent])
], ProposalEditComponent.prototype, "form", null);
ProposalEditComponent = __decorate([
    core_1.Component({
        templateUrl: '/app/dashboard/proposals/proposal-edit.component.html'
    }),
    __metadata("design:paramtypes", [data_service_1.DataService,
        router_1.ActivatedRoute,
        router_1.Router])
], ProposalEditComponent);
exports.ProposalEditComponent = ProposalEditComponent;
//# sourceMappingURL=proposal-edit.component.js.map