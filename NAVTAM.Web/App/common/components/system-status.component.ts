﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { WindowRef } from '../../common/windowRef.service';

@Component({
    selector: 'system-status',
    templateUrl: 'app/common/components/system-status.component.html'
})
export class SystemStatusComponent {
    apiUrl: string;
    statusTicker: any;
    systemStatus: any;
    expanded: boolean;

    constructor(private http: Http, private winRef: WindowRef) {
        this.apiUrl = winRef.nativeWindow['app'].apiUrl;
    }

    ngOnInit() {
        this.statusTicker = window.setInterval(this.checkStatus.bind(this), 30000);
    }

    ngDestroy() {
        if (this.statusTicker) {
            window.clearInterval(this.statusTicker);
            this.statusTicker = undefined;
        }
    }

    get systemDown(): boolean {
        return this.systemStatus && this.systemStatus.status;
    }

    get hubDown() {
        return this.systemStatus && this.systemStatus.status === 1;
    }

    get systemError() {
        return this.systemStatus && this.systemStatus.status === 2;
    }

    get lastErrorMessage() {
        return this.systemStatus && this.systemStatus.lastError;
    }

    private checkStatus() {
        this.http.post(`${this.apiUrl}system/status`, {})
            .map(res => res.json())
            .subscribe((status: any) => this.systemStatus = status);
    }

    private resetStatus() {
        this.http.post(`${this.apiUrl}system/status/reset`, {})
            .map(res => res.json())
            .subscribe((status: any) => this.systemStatus = 0);
    }

    private toggleExpand() {
        this.expanded = !this.expanded;
    }
}

