"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerPaginationComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var ServerPaginationComponent = /** @class */ (function () {
    function ServerPaginationComponent(locale, translation) {
        this.locale = locale;
        this.translation = translation;
        this.currentSlider = 0;
        this.pages = [];
        this.onPageChange = new core_1.EventEmitter();
    }
    ServerPaginationComponent.prototype.ngOnInit = function () {
        this.sliderSize = this.sliderSize || 3;
    };
    ServerPaginationComponent.prototype.ngOnChanges = function (changes) {
        if (changes && changes.currentPage && changes.currentPage.currentValue === 1) {
            if (this.currentSlider > 0) {
                this.currentSlider = 0;
            }
        }
        this.pages = [];
        this.sliderUpperBound = this.pageSize;
        var lowerBound = this.currentSlider * this.sliderSize;
        var upperBound = (this.currentSlider * this.sliderSize) + this.sliderSize;
        if (upperBound > this.totalPages) {
            upperBound = this.totalPages;
        }
        this.sliderLowerBound = lowerBound;
        this.sliderUpperBound = upperBound;
        this.sliderlastBound = (Math.ceil(this.totalPages / this.sliderSize));
        for (var i = lowerBound; i < upperBound; i++) {
            this.pages.push(i + 1);
        }
    };
    ServerPaginationComponent.prototype.prevSlider = function () {
        if (this.currentSlider > 0) {
            this.onPageChange.emit(this.currentSlider * this.sliderSize);
            this.currentSlider = this.currentSlider - 1;
        }
    };
    ServerPaginationComponent.prototype.nextSlider = function () {
        if (this.currentSlider < (this.totalPages / this.sliderSize)) {
            this.currentSlider = this.currentSlider + 1;
            this.onPageChange.emit((this.currentSlider * this.sliderSize) + 1);
        }
    };
    ServerPaginationComponent.prototype.prevPage = function () {
        if (this.currentPage === this.sliderLowerBound + 1) {
            this.prevSlider();
        }
        else if (this.currentPage > 1) {
            this.onPageChange.emit(this.currentPage - 1);
        }
    };
    ServerPaginationComponent.prototype.nextPage = function () {
        if (this.currentPage === this.sliderUpperBound) {
            this.nextSlider();
        }
        else if (this.currentPage < this.totalPages) {
            this.onPageChange.emit(this.currentPage + 1);
        }
    };
    ServerPaginationComponent.prototype.moveToPage = function (page) {
        if (page >= 1 && page <= this.totalPages && page !== this.currentPage) {
            this.onPageChange.emit(page);
        }
    };
    __decorate([
        core_1.Input('currentPage'),
        __metadata("design:type", Number)
    ], ServerPaginationComponent.prototype, "currentPage", void 0);
    __decorate([
        core_1.Input('pageSize'),
        __metadata("design:type", Number)
    ], ServerPaginationComponent.prototype, "pageSize", void 0);
    __decorate([
        core_1.Input('totalCount'),
        __metadata("design:type", Number)
    ], ServerPaginationComponent.prototype, "totalCount", void 0);
    __decorate([
        core_1.Input('totalPages'),
        __metadata("design:type", Number)
    ], ServerPaginationComponent.prototype, "totalPages", void 0);
    __decorate([
        core_1.Input('sliderSize'),
        __metadata("design:type", Number)
    ], ServerPaginationComponent.prototype, "sliderSize", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ServerPaginationComponent.prototype, "onPageChange", void 0);
    ServerPaginationComponent = __decorate([
        core_1.Component({
            selector: 'server-pagination',
            styles: ["\n        .btn-pagination:hover {\n            background-color:#37393a;\n\t        border-color:#37393a\n        }\n        .btn-pagination{\n            background-color:#707474;\n\t        border-color:#707474;\n        }\n        .btn-pagination[disabled] {\n            background-color:#d6d6d6;\n\t        border-color:#d6d6d6;\n        }\n        .btn-pagination.active {\n            background-color:#181a1d;\n\t        color:#ccc;\n        }\n    "],
            template: "\n        <div class=\"row\" *ngIf=\"totalPages > 1\">\n            <div class=\"col-md-6\">\n                <div class=\"dataTables_info\" role=\"status\" aria-live=\"polite\">\n                    <span>\n                        {{'Pagination.Showing' | translate:lang}}&nbsp;<span>{{((currentPage - 1) * pageSize) + 1}}</span>&nbsp;{{'Pagination.To' | translate:lang}}&nbsp;\n                        <span>{{((currentPage -1) * pageSize) + pageSize}}</span>&nbsp;{{'Pagination.Off' | translate:lang}}&nbsp;\n                        <span>{{totalCount}}</span>&nbsp;{{'Pagination.Entries' | translate:lang}}\n                    </span>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"dataTables_paginate pull-right\" id=\"table2_paginate\">\n                    <div class=\"btn-group\">\n<!--\n                        <button class=\"btn btn-xs btn-primary btn-pagination\" style=\"padding-right: 1px;\" type=\"button\"\n                                [disabled]=\"currentSlider === 0\"\n                                (click)=\"prevSlider()\">\n                            <i class=\"fa fa-angle-double-left\"></i>\n                        </button>\n-->\n                        <button class=\"btn btn-xs btn-primary btn-pagination\" style=\"padding-right: 3px;\" type=\"button\"\n                                [disabled]=\"currentPage <= 1\"\n                                (click)=\"prevPage()\">\n                            <i class=\"fa fa-angle-left\"></i>\n                        </button>\n                        <button *ngFor=\"let page of pages\" class=\"btn btn-xs btn-primary btn-pagination\"\n                                [class.active]=\"currentPage === page\"\n                                (click)=\"moveToPage(page)\">{{page}}</button>\n\n                        <button class=\"btn btn-xs btn-primary btn-pagination\" style=\"padding-right: 3px;\" type=\"button\"\n                                [disabled]=\"currentPage >= totalPages\"\n                                (click)=\"nextPage()\">\n                            <i class=\"fa fa-angle-right\"></i>\n                        </button>\n<!--\n                        <button class=\"btn btn-xs btn-primary btn-pagination\" style=\"padding-right: 1px;\" type=\"button\"\n                                [disabled]=\"currentSlider >= sliderlastBound - 1\"\n                                (click)=\"nextSlider()\">\n                            <i class=\"fa fa-angle-double-right\"></i>\n                        </button>\n-->\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\" *ngIf=\"totalPages <= 1\">\n            <div class=\"col-md-6\">\n                <div class=\"dataTables_info\" role=\"status\" aria-live=\"polite\">\n                    <span>\n                        {{'Pagination.Showing' | translate:lang}}&nbsp;<span>{{((currentPage - 1) * pageSize) + 1}}</span>&nbsp;{{'Pagination.To' | translate:lang}}&nbsp;\n                        <span>{{((currentPage -1) * pageSize) + pageSize}}</span>&nbsp;{{'Pagination.Off' | translate:lang}}&nbsp;\n                        <span>{{totalCount}}</span>&nbsp;{{'Pagination.Entries' | translate:lang}}\n                    </span>\n                </div>\n            </div>\n        </div>"
        }),
        __metadata("design:paramtypes", [angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], ServerPaginationComponent);
    return ServerPaginationComponent;
}());
exports.ServerPaginationComponent = ServerPaginationComponent;
//# sourceMappingURL=server-pagination.component.js.map