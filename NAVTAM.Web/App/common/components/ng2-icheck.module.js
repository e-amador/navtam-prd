"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ng2ICheckModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var ng2_icheck_component_1 = require("./ng2-icheck.component");
var ng2_iradio_component_1 = require("./ng2-iradio.component");
var ng2_icheck_component_2 = require("./ng2-icheck.component");
Object.defineProperty(exports, "ICheckComponent", { enumerable: true, get: function () { return ng2_icheck_component_2.ICheckComponent; } });
var ng2_iradio_component_2 = require("./ng2-iradio.component");
Object.defineProperty(exports, "IRadioComponent", { enumerable: true, get: function () { return ng2_iradio_component_2.IRadioComponent; } });
var Ng2ICheckModule = /** @class */ (function () {
    function Ng2ICheckModule() {
    }
    Ng2ICheckModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            declarations: [
                ng2_icheck_component_1.ICheckComponent,
                ng2_iradio_component_1.IRadioComponent
            ],
            providers: [],
            exports: [
                ng2_icheck_component_1.ICheckComponent,
                ng2_iradio_component_1.IRadioComponent
            ]
        })
    ], Ng2ICheckModule);
    return Ng2ICheckModule;
}());
exports.Ng2ICheckModule = Ng2ICheckModule;
//# sourceMappingURL=ng2-icheck.module.js.map