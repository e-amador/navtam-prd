"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ICheckComponent = void 0;
var core_1 = require("@angular/core");
var ICheckComponent = /** @class */ (function () {
    function ICheckComponent() {
        this.isChecked = false;
        this.disabled = false;
        this.change = new core_1.EventEmitter();
    }
    ICheckComponent.prototype.toggleCheck = function () {
        if (!this.disabled) {
            this.isChecked = !this.isChecked;
            this.change.emit(this.isChecked);
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ICheckComponent.prototype, "isChecked", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ICheckComponent.prototype, "disabled", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ICheckComponent.prototype, "change", void 0);
    ICheckComponent = __decorate([
        core_1.Component({
            selector: 'iCheck',
            styles: ["\n    :host {\n        font-family: \"Consolas\", \"Microsoft YaHei\", Arial, arial, sans-serif;\n        overflow: hidden;\n    }\n  ", "\n    :host > div > div {\n        width: 24px;\n        height: 24px;\n        display: inline-block;\n        vertical-align: middle;\n        background: url('content/css/icheck.skins/square/grey.png') no-repeat left;\n        background-position: 5 0;\n        cursor: pointer;\n    }\n  ", "\n    :host > div > div:hover {\n        background-position: -24px 0;\n    }\n  ", "\n    :host > div > div.disabled {\n        background-position: -72px 0;\n        cursor: default;\n    }    \n  ", "\n    :host > div > div.checked {\n        background-position: -48px 0;\n    }\n  ", "\n    :host > div > div.checked.disabled {\n        background-position: -96px 0;\n    }\n  ", "\n    :host .label {\n        display: inline-block;\n        vertical-align: middle;\n    }\n  "],
            template: "\n    <div (click)=\"toggleCheck()\" class=\"ng2-icheck\">\n        <div [class.checked]=\"isChecked\" [class.disabled]=\"disabled\"></div>\n        <ng-content class=\"label\"></ng-content>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [])
    ], ICheckComponent);
    return ICheckComponent;
}());
exports.ICheckComponent = ICheckComponent;
//# sourceMappingURL=ng2-icheck.component.js.map