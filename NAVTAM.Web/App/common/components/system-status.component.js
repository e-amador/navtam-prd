"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SystemStatusComponent = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var windowRef_service_1 = require("../../common/windowRef.service");
var SystemStatusComponent = /** @class */ (function () {
    function SystemStatusComponent(http, winRef) {
        this.http = http;
        this.winRef = winRef;
        this.apiUrl = winRef.nativeWindow['app'].apiUrl;
    }
    SystemStatusComponent.prototype.ngOnInit = function () {
        this.statusTicker = window.setInterval(this.checkStatus.bind(this), 30000);
    };
    SystemStatusComponent.prototype.ngDestroy = function () {
        if (this.statusTicker) {
            window.clearInterval(this.statusTicker);
            this.statusTicker = undefined;
        }
    };
    Object.defineProperty(SystemStatusComponent.prototype, "systemDown", {
        get: function () {
            return this.systemStatus && this.systemStatus.status;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SystemStatusComponent.prototype, "hubDown", {
        get: function () {
            return this.systemStatus && this.systemStatus.status === 1;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SystemStatusComponent.prototype, "systemError", {
        get: function () {
            return this.systemStatus && this.systemStatus.status === 2;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SystemStatusComponent.prototype, "lastErrorMessage", {
        get: function () {
            return this.systemStatus && this.systemStatus.lastError;
        },
        enumerable: false,
        configurable: true
    });
    SystemStatusComponent.prototype.checkStatus = function () {
        var _this = this;
        this.http.post(this.apiUrl + "system/status", {})
            .map(function (res) { return res.json(); })
            .subscribe(function (status) { return _this.systemStatus = status; });
    };
    SystemStatusComponent.prototype.resetStatus = function () {
        var _this = this;
        this.http.post(this.apiUrl + "system/status/reset", {})
            .map(function (res) { return res.json(); })
            .subscribe(function (status) { return _this.systemStatus = 0; });
    };
    SystemStatusComponent.prototype.toggleExpand = function () {
        this.expanded = !this.expanded;
    };
    SystemStatusComponent = __decorate([
        core_1.Component({
            selector: 'system-status',
            templateUrl: 'app/common/components/system-status.component.html'
        }),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef])
    ], SystemStatusComponent);
    return SystemStatusComponent;
}());
exports.SystemStatusComponent = SystemStatusComponent;
//# sourceMappingURL=system-status.component.js.map