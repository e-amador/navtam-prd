/**
 * modal.component
 */

import {
    Component, Output, OnInit, EventEmitter, trigger, transition, style, state, animate,
    ChangeDetectionStrategy
} from "@angular/core";

@Component({
    selector: 'picker-modal',
    template: `<section class="picker-modal"><section class="picker-modal-overlay" (click)="closeModal()"></section><section class="picker-modal-main" [@modalAnimation]="'in'"><ng-content></ng-content></section></section>`,
    styles: [`*,::after,::before{-moz-box-sizing:border-box;box-sizing:border-box}:host{z-index:9999999999}*,::after,::before{-moz-box-sizing:border-box;box-sizing:border-box}.picker-modal{position:fixed;top:0;left:0;z-index:99999999999;width:100%;height:100%;overflow-y:scroll}.picker-modal-overlay{position:fixed;top:0;left:0;z-index:11;display:block;width:100%;height:100%;background-color:rgba(0,0,0,.3)}.picker-modal-main{position:absolute;top:0;right:auto;bottom:auto;left:50%;z-index:111;height:100%;background-color:#fff;-webkit-transform:translate(-50%,0);-moz-transform:translate(-50%,0);-ms-transform:translate(-50%,0);transform:translate(-50%,0);-moz-box-shadow:0 5px 15px rgba(0,0,0,.3);box-shadow:0 5px 15px rgba(0,0,0,.3);-moz-border-radius:5px;border-radius:5px}@media only screen and (min-width:768px){.picker-modal-main{top:30px;height:auto}}`],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('modalAnimation', [
            state('*',
                style({
                    opacity: 1,
                    transform: 'translate(-50%, 0)',
                })
            ),
            transition(':enter', [
                style({
                    opacity: 0,
                    transform: 'translate(-50%, -100%)',
                }),
                animate('0.3s cubic-bezier(.13,.68,1,1.53)')
            ])
        ])
    ],
})

export class ModalComponent implements OnInit {

    @Output() onOverlayClick = new EventEmitter<boolean>();

    constructor() {
    }

    ngOnInit() {
    }

    closeModal() {
        this.onOverlayClick.emit(false);
    }
}
