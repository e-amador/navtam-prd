"use strict";
/**
 * index
 */
Object.defineProperty(exports, "__esModule", { value: true });
var picker_module_1 = require("./lib/picker.module");
Object.defineProperty(exports, "DateTimePickerModule", { enumerable: true, get: function () { return picker_module_1.DateTimePickerModule; } });
var picker_directive_1 = require("./lib/picker.directive");
Object.defineProperty(exports, "DateTimePickerDirective", { enumerable: true, get: function () { return picker_directive_1.DateTimePickerDirective; } });
//# sourceMappingURL=index.js.map