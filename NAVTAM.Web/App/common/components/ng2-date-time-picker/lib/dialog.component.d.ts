import { OnInit, ElementRef } from '@angular/core';
import { PickerService } from './picker.service';
export declare class DialogComponent implements OnInit {
    private el;
    private service;
    private show;
    private initialValue;
    private selectedMoment;
    private directiveInstance;
    private directiveElementRef;
    private now;
    private top;
    private left;
    private width;
    private height;
    private position;
    theme: string;
    hourTime: '12' | '24';
    positionOffset: string;
    mode: 'popup' | 'dropdown' | 'inline';
    returnObject: string;
    dialogType: DialogType;
    pickerType: 'both' | 'date' | 'time';
    private subId;
    constructor(el: ElementRef, service: PickerService);
    ngOnInit(): void;
    openDialog(moment: any): void;
    cancelDialog(): void;
    setInitialMoment(value: any): void;
    setDialog(instance: any, elementRef: ElementRef, initialValue: any, dtLocale: string, dtViewFormat: string, dtReturnObject: string, dtPositionOffset: string, dtMode: 'popup' | 'dropdown' | 'inline', dtHourTime: '12' | '24', dtTheme: string, dtPickerType: 'both' | 'date' | 'time'): void;
    confirm(close: boolean): void;
    toggleDialogType(type: DialogType): void;
    private setDialogPosition();
    private setInlineDialogPosition();
    private createBox(element, offset);
    private returnSelectedMoment();
    private onMouseDown(event);
}
export declare enum DialogType {
    Time = 0,
    Date = 1,
    Month = 2,
    Year = 3,
}
