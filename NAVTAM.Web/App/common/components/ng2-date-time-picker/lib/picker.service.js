"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var dialog_component_1 = require("./dialog.component");
var moment = require("moment/moment");
var Rx_1 = require("rxjs/Rx");
var PickerService = (function () {
    function PickerService() {
        this.eventSource = new Rx_1.Subject();
        this.events = this.eventSource.asObservable();
    }
    Object.defineProperty(PickerService.prototype, "dtLocale", {
        get: function () {
            return this._dtLocale;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtViewFormat", {
        get: function () {
            return this._dtViewFormat;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtReturnObject", {
        get: function () {
            return this._dtReturnObject;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtDialogType", {
        get: function () {
            return this._dtDialogType;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtPickerType", {
        get: function () {
            return this._dtPickerType;
        },
        set: function (value) {
            this._dtPickerType = value;
            if (value === 'both' || value === 'date') {
                this._dtDialogType = dialog_component_1.DialogType.Date;
            }
            else if (value === 'time') {
                this._dtDialogType = dialog_component_1.DialogType.Time;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtPositionOffset", {
        get: function () {
            return this._dtPositionOffset;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtMode", {
        get: function () {
            return this._dtMode;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtHourTime", {
        get: function () {
            return this._dtHourTime;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtTheme", {
        get: function () {
            return this._dtTheme;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "moment", {
        get: function () {
            return this._moment;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "selectedMoment", {
        get: function () {
            return this._selectedMoment;
        },
        set: function (value) {
            this._selectedMoment = value;
            this.eventSource.next(value);
        },
        enumerable: true,
        configurable: true
    });
    PickerService.prototype.setPickerOptions = function (dtLocale, dtViewFormat, dtReturnObject, dtPositionOffset, dtMode, dtHourTime, dtTheme, dtPickerType) {
        this._dtLocale = dtLocale;
        this._dtViewFormat = dtViewFormat;
        this._dtReturnObject = dtReturnObject;
        this._dtPositionOffset = dtPositionOffset;
        this._dtMode = dtMode;
        this._dtHourTime = dtHourTime;
        this._dtTheme = dtTheme;
        this.dtPickerType = dtPickerType;
    };
    PickerService.prototype.setMoment = function (value) {
        if (value && value.length === 10) {
            this._moment = this._dtReturnObject === 'string' ? moment(dt, this._dtViewFormat).utc() : moment.utc(value, 'YYMMDDHHmm');
            this.selectedMoment = this._moment.clone();
        }
        else {
            this._moment = moment().utc(); //ESTEBAN FIXED!!!!!!!
        }
    };
    PickerService.prototype.setDate = function (moment) {
        var m = this.selectedMoment ? this.selectedMoment.clone().utc() : this.moment.utc(); //ESTEBAN FIXED!!!!!!!
        var daysDifference = moment.clone().startOf('date').diff(m.clone().startOf('date'), 'days');
        this.selectedMoment = m.add(daysDifference, 'd');
    };
    PickerService.prototype.setTime = function (hour, minute, meridian) {
        var m = this.selectedMoment ? this.selectedMoment.clone().utc() : this.moment.clone().utc(); //ESTEBAN FIXED!!!!!!!
        if (this.dtHourTime === '12') {
            if (meridian === 'AM') {
                if (hour === 12) {
                    m.hours(0);
                }
                else {
                    m.hours(hour);
                }
            }
            else {
                if (hour === 12) {
                    m.hours(12);
                }
                else {
                    m.hours(hour + 12);
                }
            }
        }
        else if (this.dtHourTime === '24') {
            m.hours(hour);
        }
        m.minutes(minute);
        this.selectedMoment = m;
    };
    PickerService.prototype.parseToReturnObjectType = function () {
        switch (this.dtReturnObject) {
            case 'string':
                return this.selectedMoment.format(this.dtViewFormat);
            case 'moment':
                return this.selectedMoment;
            case 'json':
                return this.selectedMoment.toJSON();
            case 'array':
                return this.selectedMoment.toArray();
            case 'iso':
                return this.selectedMoment.toISOString();
            case 'object':
                return this.selectedMoment.toObject();
            case 'js':
            default:
                return this.selectedMoment.toDate();
        }
    };
    return PickerService;
}());
PickerService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], PickerService);
exports.PickerService = PickerService;
//# sourceMappingURL=picker.service.js.map