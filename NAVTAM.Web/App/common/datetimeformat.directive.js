"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateTimeFormatDirective = void 0;
var core_1 = require("@angular/core");
var moment = require("moment");
var DateTimeFormatDirective = /** @class */ (function () {
    function DateTimeFormatDirective(elementRef) {
        this.elementRef = elementRef;
        this.el = this.elementRef.nativeElement;
    }
    DateTimeFormatDirective.prototype.ngOnInit = function () {
        if (!this.isStringBlank(this.el.value)) {
            if (this.el.value.length < 10) {
                this.el.value = this.el.value;
            }
        }
        else {
            this.el.value = null;
        }
    };
    DateTimeFormatDirective.prototype.onBlur = function (value) {
        if (!this.isStringBlank(value)) {
            if (value.length !== 10) {
                this.el.value = null;
            }
            else {
                var momentDate = moment.utc(value, "YYMMDDHHmm");
                if (!momentDate.isValid()) {
                    this.el.value = null;
                }
            }
        }
    };
    DateTimeFormatDirective.prototype.onKeyUp = function (value) {
        if (value !== null) {
            if (value.length > 10) {
                this.el.value = value.substring(0, 10);
            }
            else {
                if (this.isANumber(value)) {
                    //switch (value.length) {
                    //    case 1://first year digit
                    //        if (+value[0] < 1 || +value[0] > 2) {
                    //            this.el.value = null;
                    //        }
                    //        break;
                    //    case 2: //second year digit
                    //        if (+value[0] === 1) {
                    //            if (+value[1] < 7) this.el.value = value.slice(0, 1);
                    //        } 
                    //        break; 
                    //    case 3: //first month digit
                    //        if (+value[2] > 1) this.el.value = value.slice(0, 2); break;
                    //    case 4: break; //second month digit;
                    //    case 5: //first day digit
                    //        if (+value[4] > 3) this.el.value = value.slice(0, 4); break;
                    //    case 6: //second day digit
                    //        if (+value[4] === 0) {
                    //            if( +value[5] < 1) this.el.value = value.slice(0, 5);
                    //        } else if (+value[4] === 3){
                    //            if (+value[5] > 1) this.el.value = value.slice(0, 5);
                    //        }
                    //        break;
                    //    case 7: //first hour digit
                    //        if (+value[6] > 2) this.el.value = value.slice(0, 6); break;
                    //    case 8: break; //second hour digit
                    //    case 9: //first minute digit
                    //        if (+value[8] > 5) this.el.value = value.slice(0, 7); break;
                    //    case 10://second minute digit
                    //        let momentDate = moment.utc(value, "YYMMDDHHmm");
                    //        if (!momentDate.isValid()) {
                    //            //this.el.value = null;
                    //        }
                    //        break;
                    //}
                }
                else {
                    var strDigits = "";
                    for (var i = 0; i < value.length; i++) {
                        if (this.isANumber(value[i])) {
                            strDigits += value[i];
                        }
                    }
                    this.el.value = strDigits;
                }
            }
        }
    };
    DateTimeFormatDirective.prototype.isANumber = function (str) {
        return !/\D/.test(str);
    };
    DateTimeFormatDirective.prototype.isStringBlank = function (str) {
        return (!str || /^\s*$/.test(str));
    };
    __decorate([
        core_1.HostListener("blur", ["$event.target.value"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], DateTimeFormatDirective.prototype, "onBlur", null);
    __decorate([
        core_1.HostListener("keyup", ["$event.target.value"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], DateTimeFormatDirective.prototype, "onKeyUp", null);
    DateTimeFormatDirective = __decorate([
        core_1.Directive({
            selector: '[datetimeformat]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], DateTimeFormatDirective);
    return DateTimeFormatDirective;
}());
exports.DateTimeFormatDirective = DateTimeFormatDirective;
//# sourceMappingURL=datetimeformat.directive.js.map