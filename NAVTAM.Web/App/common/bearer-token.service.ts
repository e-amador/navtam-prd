﻿import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

@Injectable()
export class BearerTokenService extends RequestOptions {

    constructor(private requestOptionArgs: RequestOptions) {
        super();
    }

    addHeader(headerName: string, headerValue: string) {
        (this.requestOptionArgs.headers as Headers).set(headerName, headerValue);
    }

    addAuthorization(accesstoken: string) {
        this.addHeader("Content-Type", "application/json");
        this.addHeader("Authorization", "Bearer " + accesstoken);
    }
}


