﻿var app = app || {};
app.bmap = new function () {
    var map;

    // 0: uninitialized
    // 1: initialized
    // 2: disconnected (internet connection error)

    var status = 0; 

    var initMap = function (callback) {
        try {
            map = new Microsoft.Maps.Map(document.getElementById('map'), {
                credentials: 'AnuhluTsAiyp-biSnICtTpyGBNrsn5VzOQ6ju9dEbmBjFGE-PIPxwEOYfhS5Cpps', //Esteban Credentials
                center: new Microsoft.Maps.Location(45.4215, -75.6973),
                mapTypeId: Microsoft.Maps.MapTypeId.road
            });
            status = 1;
            if (callback) {
                callback(status);
            }
        }
        catch (e) {
            status = 2; // error
            if (callback) {
                callback(status);
            }
            console.log(e);
        }
    };

    var isReady = function () {
        return status === 1;
    };

    var dispose = function () {
        map = null;
        status = 0;
    };

    var addDoa = function (geoLocation, featureName) {
        if (status === 1) {
            let layer = getLayerOnMap('DOA');
            if (!layer) layer = new Microsoft.Maps.Layer('DOA');
            const geojson = JSON.parse(geoLocation);
            if (geojson) {
                AddFeaturesToMap(layer, featureName, geojson, 'rgba(0, 255, 255, 0.2)');
            }
        }
    };
    var addMarker = function (geojson, featureName, onCompleted) {
        if (status === 1) {
            let layer = getLayerOnMap('PIN');
            if (!layer) layer = new Microsoft.Maps.Layer('PIN');
            AddFeaturesToMap(layer, featureName, geojson, 'blue', onCompleted);
        }
    };
    var addFeature = function (geoLocation, featureName, onCompleted) {
        if (status === 1) {
            let layer = getLayerOnMap('LOC');
            if (!layer) layer = new Microsoft.Maps.Layer('LOC');
            const geojson = JSON.parse(geoLocation);
            AddFeaturesToMap(layer, featureName, geojson, 'rgba(255, 0, 0, 0.4)', onCompleted);
        }
    };
    var setNewMarkerLocation = function (geojson) {
        if (status === 1) {
            if (geojson.features.length !== 1) return;

            const pinLayer = getLayerOnMap('PIN');
            if (pinLayer) {
                const pushpins = pinLayer.getPrimitives();
                if (pushpins.length === 1) {
                    //var currentLocation = pushpins[0].getLocation();
                    //latitudeDifference = Math.abs(currentLocation.latitude - geojson.features[0].geometry.coordinates[1]);
                    //longitudeDifference = Math.abs(currentLocation.longitude - geojson.features[0].geometry.coordinates[0]);
                    let updatedLayer = getLayerOnMap('PIN_UPDATED');
                    if (!updatedLayer) {
                        updatedLayer = new Microsoft.Maps.Layer('PIN_UPDATED');
                    } else {
                        updatedLayer.clear();
                    }
                    AddFeaturesToMap(updatedLayer, 'Location', geojson, 'red');
                }
            }
        }
    };
    var clearFeaturesOnLayers = function (excludingLayerIds) {
        if (status === 1) {
            excludingLayerIds = excludingLayerIds || [];
            for (let i = 0; i < map.layers.length; i++) {
                var layer = map.layers[i];
                if (excludingLayerIds.length > 0) {
                    for (let j = 0; j < excludingLayerIds.length; j++) {
                        if (layer.getId() !== excludingLayerIds[j]) {
                            layer.clear();
                        }
                    }
                }
                else {
                    layer.clear();
                }
            }
        }
    };
    var textCordinatesToGeojson = function (textCoordinates) {

        function convert(str) {
            try {
                const degrees = parseInt(str.length === 7 ? str.substring(0, 2) : str.substring(0, 3));
                const minutes = parseInt(str.length === 7 ? str.substring(2, 4) : str.substring(3, 5));
                const seconds = parseInt(str.length === 7 ? str.substring(4, 6) : str.substring(5, 7));
                const sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);

                var sing = sufx === 'W' || sufx === 'S' ? -1 : 1;
                return sing * (degrees + (minutes * 60.0 + seconds) / 3600.0);
            }
            catch (e) {
                return -1;
            }
        }

		const parts = (textCoordinates || "").split(' ');        
        if (parts.length !== 2)
            return null;

        if (parts[0].length > 8 || parts[0].length < 7) {
            return null;
        }
        if (parts[1].length > 8 || parts[1].length < 7) {
            return null;
        }

        let lng = -1;
        let lat = convert(parts[0]);
        if (lat !== -1) {
            lng = convert(parts[1]);
        }

        if (lat === -1 || lng === -1) {
            return null;
        }

        return latitudeLongitudeToGeoJson(lat, lng);
    };
    var geojsonToTextCordinates = function (geojson) {

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function convert(value, type) {
            try { 
                const sign = value < 0;

                var abs = Math.abs(value);
                var deg = Math.trunc(abs);
                var min = Math.trunc((abs - deg) * 60.0);
                var sec = Math.round((abs - deg - min / 60.0) * 3600.0);
                if (sec > 59) {
                    sec = 0;
                    min = min + 1;
                    if (min > 59) {
                        min = 0;
                        deg = deg + 1;
                    }
                }

                if (type === 'lat') {
                    return pad(deg, 2) + pad(min, 2) + pad(sec, 2) + (sign ? 'S' : 'N');
                }

                return pad(deg, 3) + pad(min, 2) + pad(sec, 2) + (sign ? 'W' : 'E');
            }
            catch (e) {
                return "";
            }
        }

        const coordinates = geojson.features[0].geometry.coordinates;

        const lat = convert(coordinates[1], 'lat');
        const lng = convert(coordinates[0], 'lng');

        return lat + ' ' + lng;
    };
    var latitudeLongitudeToGeoJson = function (lat, lng) {
        return {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Point",
                        "bbox": [lat, lng, lat, lng],
                        "coordinates": [lng, lat]
                    }
                }]
        };
    };
    
    function AddFeaturesToMap(layer, featureName, geojson, fillColor, onCompleted) {
        function getviewBoundaries(geojson) {
            let scaleBounds = 0;
            const bboxs = [];
            for (let i = 0; i < geojson.features.length; i++) {
                switch (layer.getId()) {
                    case 'LOC':
                        if (geojson.features[i].geometry.type === 'GeometryCollection') {
                            scaleBounds = geojson.features[i].geometry.geometries.length > 2 ? 0 : 10;
                        }
                        break;
                    case 'PIN_UPDATED': scaleBounds = 20; break;
                    case 'DOA': scaleBounds = 0; break;
                }

                if (geojson.features[i].geometry.bbox) {
                    let lat, lng;
                    for (let j = 0; j < geojson.features[i].geometry.bbox.length; j++) {
                        if (j % 2 !== 0) {
                            lng = geojson.features[i].geometry.bbox[j];
                            bboxs.push(new Microsoft.Maps.Location(lat, lng));
                        } else {
                            lat = geojson.features[i].geometry.bbox[j];
                        }
                    }
                }
                else if (geojson.features[i].geometry.type === "MultiPolygon") {
                    for (let j = 0; j < geojson.features[i].geometry.coordinates.length; j++) {
                        for (let k = 0; k < geojson.features[i].geometry.coordinates[j].length; k++) {
                            for (let z = 0; z < geojson.features[i].geometry.coordinates[j][k].length; z++) {
                                lat = geojson.features[i].geometry.coordinates[j][k][z][1];
                                lng = geojson.features[i].geometry.coordinates[j][k][z][0];
                                bboxs.push(new Microsoft.Maps.Location(lat, lng));
                            }
                        }
                    }
                }
            }
            const viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(bboxs);
            if (scaleBounds > 0) {
                viewBoundaries.buffer(scaleBounds);
            }
            return viewBoundaries;
        }
        Microsoft.Maps.loadModule('Microsoft.Maps.GeoJson', function () {
            const options = {
                polygonOptions:
                {
                    name: featureName,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: fillColor
                }
            };
            if (false && options.polygonOptions.name === 'DOA') {
                layer.add(geojson.features);
            } else {
                const featureCollection = Microsoft.Maps.GeoJson.read(geojson, options);
                for (let i = 0; i < featureCollection.length; i++) {

                    if (layer.getId() === 'PIN' || layer.getId() === 'PIN_UPDATED') {
                        if (featureCollection[i].geometryType === 1) {
                            const center = map.getCenter();
                            center.latitude = featureCollection[i].geometry.y;
                            center.longitude = featureCollection[i].geometry.x;
                            let pushpin;
                            if (layer.getId() === 'PIN_UPDATED') {
                                pushpin = new Microsoft.Maps.Pushpin(center, {
                                    icon: '/content/images/poi_custom.png',
                                    title: featureName,
                                    subTitle: '',//featureName,
                                    anchor: new Microsoft.Maps.Point(12, 39)
                                });
                                layer.add(pushpin);
                            } else {
                                pushpin = new Microsoft.Maps.Pushpin(center, {
                                    title: ' ',
                                    subTitle: featureName,
                                    anchor: new Microsoft.Maps.Point(12, 39)
                                });
                                layer.add(pushpin);
                            }
                        }
                    }
                    else layer.add(featureCollection[i]);
                }
            }

            if (!getLayerOnMap(layer.getId())) {
                map.layers.insert(layer);
            }

            const layerId = layer.getId();
            if (layerId) { // layerId !== 'PIN_UPDATED'
                try {
                    let zoomLevel;
                    switch (layerId) {
                        case "PIN": zoomLevel = 13; break;
                        case "PIN_UPDATED": zoomLevel = 13; break;
                        case "LOC": zoomLevel = 13; break;
                        case "DOA": zoomLevel = 5; break;
                        default: zoomLevel = 11; break;
                    }

                    map.setView({ bounds: getviewBoundaries(geojson), padding: 0 });
                    map.setView({ zoom: zoomLevel });

                    if (onCompleted) onCompleted();
                }
                catch (e) {
                    //note: avoiding data problems, in production this should never happend
                }
            }
        });
    }

    function getLayerOnMap(layerId) {
        if (map) {
            for (let i = 0; i < map.layers.length; i++) {
                if (map.layers[i].getId() === layerId) {
                    return map.layers[i];
                }
            }
        }
        return null;
    }

    return {
        initMap: initMap,
        isReady: isReady,
        dispose: dispose,

        addDoa: addDoa,
        addMarker: addMarker,
        addFeature: addFeature,

        clearFeaturesOnLayers: clearFeaturesOnLayers,
        setNewMarkerLocation: setNewMarkerLocation,

        textCordinatesToGeojson: textCordinatesToGeojson,
        geojsonToTextCordinates: geojsonToTextCordinates,
        latitudeLongitudeToGeoJson: latitudeLongitudeToGeoJson
    };
};



