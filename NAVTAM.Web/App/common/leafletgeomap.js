﻿app = app || {};

app.leafletgeomap = new function () {
    var geoMap;
    var dynamicGeoLocalizationLayer;
    var geoStatus = 0;
    var geoLocLayer;
    var geoPointsLayer;
    var geoCirclePointsLayer;
    var geoLocStyling;
    var geoCircleLayer;
    var geoAerodromeLayer;
    var geoAerodromePopupLayer;

    var getNestedObject = function getNestedObject(nestedObj, pathArr) {
        return pathArr.reduce(function (obj, key) {
            return obj && obj[key] !== 'undefined' ? obj[key] : undefined;
        }, nestedObj);
    };

    var initGeoMap = function () {
        try {
            var geoMapEndpoints = {
                darkMatter: window.app.mapServerHost + "/styles/dark-matter/{z}/{x}/{y}.png",
                klokantechBasic: window.app.mapServerHost + "/styles/klokantech-basic/{z}/{x}/{y}.png",
                osmBright: window.app.mapServerHost + "/styles/osm-bright/{z}/{x}/{y}.png",
                mapAttribution: "NOT FOR OPERATIONAL USE | PAS POUR UTILISATION OPÉRATIONNELLE",
                mapHealthApi: window.app.mapServerHost + "/health"
            };

            var geodark = new L.tileLayer(geoMapEndpoints.darkMatter, { attribution: geoMapEndpoints.mapAttribution }),
                geolight = new L.tileLayer(geoMapEndpoints.klokantechBasic, { attribution: geoMapEndpoints.mapAttribution }),
                geobright = new L.tileLayer(geoMapEndpoints.osmBright, { attribution: geoMapEndpoints.mapAttribution });

            var geoMapStyles = {
                "Dark": geodark,
                "Basic": geolight,
                "Bright": geobright
            };

            geoLocStyling = {
                "color": "#1e0c96",
                "weight": 1,
                "opacity": 0.50
            };

            var geoMapConfigurationSettings = {
                center: [52.9636976, -104.4676491],
                zoom: 3,
                layers: [geodark, geolight, geobright]
            };

            geoMap = new L.map('geoMap', geoMapConfigurationSettings);
            L.control.layers(geoMapStyles).addTo(geoMap);

            dynamicGeoLocalizationLayer = new L.featureGroup();
            dynamicGeoLocalizationLayer.addTo(geoMap);

            dynamicGeoLocalizationLayer.on("layeradd", function (layer) {
                var cord = getNestedObject(layer, ['layer', '_latlng']);
                if (typeof cord !== 'undefined') {
                    geoMap.setView({ lat: cord.lat, lng: cord.lng }, 13);
                }
            });

            geoStatus = 1;

        }
        catch (e) {
            geoStatus = 2;
        }

    };

    var resetMapView = function () {
        if (geoStatus === 1) {
            dynamicGeoLocalizationLayer.clearLayers();
            if (geoLocLayer) {
                geoMap.removeLayer(geoLocLayer);
                geoLocLayer = {};
            }
            if (geoPointsLayer) {
                geoMap.removeLayer(geoPointsLayer);
                geoPointsLayer = {};
            }
            if (geoCirclePointsLayer) {
                geoMap.removeLayer(geoCirclePointsLayer);
                geoCirclePointsLayer = {};
            }
            if (geoCircleLayer) {
                geoMap.removeLayer(geoCircleLayer);
                geoCircleLayer = {};
            }
            if (geoAerodromeLayer) {
                geoMap.removeLayer(geoAerodromeLayer);
                geoAerodromeLayer = {};
            }
            if (geoAerodromePopupLayer) {
                geoMap.removeLayer(geoAerodromePopupLayer);
                geoAerodromePopupLayer = {};
            }

            geoMap.setView(new L.latLng("52.9636976", "-104.4676491"), 3);
        }
    };

    var isReadyGeoMap = function () {
        return geoStatus === 1;
    };

    var disposeGeoMap = function () {
        geoMap = null;
        geoStatus = 0;
    };

    var addGeoLocationAndRadius = function (lat, long, rad) {
        if (geoStatus === 1) {

            if (geoLocLayer) {
                geoMap.removeLayer(geoLocLayer);
                geoLocLayer = {};
            }
            if (geoCircleLayer) {
                geoMap.removeLayer(geoCircleLayer);
                geoCircleLayer = {};
            }
            var geojson = latitudeLongitudeToGeoJson(lat,long);
            geoLocLayer = new L.geoJSON(geojson, { style: geoLocStyling });
            geoLocLayer.addTo(geoMap);

            geoCircleLayer = new L.circle([lat, long], { radius: rad });
            geoCircleLayer.addTo(geoMap);

            geoMap.fitBounds(geoCircleLayer.getBounds());
            //geoMap.setView([lat, long], 11);
        }
    };

    var clearAerodromeLayer = function () {
        if (geoAerodromeLayer) {
            geoMap.removeLayer(geoAerodromeLayer);
            geoAerodromeLayer = {};
        }
    };

    var addAerodromeLocationAndRadius = function (aerodromes) {
        if (geoStatus === 1) {

            if (aerodromes.length > 0) {
                geoAerodromeLayer = new L.featureGroup();
                geoAerodromeLayer.addTo(geoMap);
                // > make change here

                aerodromes.forEach(function (aero) {
                    var lat = aero.referencePoint.split(' ')[0];
                    var long = aero.referencePoint.split(' ')[1];
                    var radius = nmToMeters(aero.distance);
                    var rad;
                    if (radius > 0) {
                        rad = new L.circle([lat, long], {
                            color: 'green',
                            fillOpacity: 0.3,
                            radius: Math.abs(radius)
                        });
                    } else {
                        rad = new L.circle([lat, long], {
                            color: 'green',
                            fillOpacity: 0.3,
                            radius: Math.abs(5 * 1852)
                        });
                    }
                    
                    rad.addTo(geoAerodromeLayer);
                });

                geoAerodromeLayer.addTo(geoMap);
                geoMap.fitBounds(geoAerodromeLayer.getBounds());
            }
        }
    };

    var clearAerodromePopupLayer = function () {
        if (geoAerodromePopupLayer) {
            geoMap.removeLayer(geoAerodromePopupLayer);
            geoAerodromePopupLayer = {};
        }
    };

    var addAerodromesPopup = function (aerodromes) {
        if (geoStatus === 1) {
            if (aerodromes.length > 0) {
                geoAerodromePopupLayer = new L.featureGroup();
                geoAerodromePopupLayer.addTo(geoMap);

                aerodromes.forEach(function (aero) {
                    var lat = aero.referencePoint.split(' ')[0];
                    var long = aero.referencePoint.split(' ')[1];
                    var circle = new L.circle([lat, long], {
                        color: 'yellow',
                        fillColor: 'yellow',
                        fillOpacity: 0.3,
                        radius: 9
                    });

                    var rd = Math.trunc(Math.round(aero.distance * 10)) / 10;
                    circle.bindPopup(aero.codeId + " " + rd.toString() + "NM").openPopup();
                    circle.addTo(geoAerodromePopupLayer);
                });

                geoAerodromePopupLayer.addTo(geoMap);
            }
        }
    };

    var addPolygonPoints = function (points) {
        if (geoStatus === 1) {
            if (geoPointsLayer) {
                geoMap.removeLayer(geoPointsLayer);
                geoPointsLayer = {};
            }

            if (points.length > 0) {
                geoPointsLayer = new L.featureGroup();
                geoPointsLayer.addTo(geoMap);

                var lineCoord = [];
                points.forEach(function (p) {
                    var lat = p.split(' ')[0];
                    var long = p.split(' ')[1];
                    lineCoord.push([lat, long]);
                    var circle = new L.circle([lat, long], {
                        color: 'black',
                        fillColor: '#f02',
                        fillOpacity: 0.3,
                        radius: 2
                    });
                    circle.bindPopup(lat + " " + long);
                    circle.addTo(geoPointsLayer);
                });
                lineCoord.push([points[0].split(' ')[0], points[0].split(' ')[1]]);
                new L.Polygon(lineCoord, { color: 'black', weight: 1 }).addTo(geoPointsLayer);

                geoPointsLayer.addTo(geoMap);
            }
        }
    };

    var addCircleFromPoints = function (points, cLat, cLong) {
        if (geoStatus === 1) {
            if (geoCirclePointsLayer) {
                geoMap.removeLayer(geoCirclePointsLayer);
                geoCirclePointsLayer = {};
            }

            if (points.length > 0) {
                geoCirclePointsLayer = new L.featureGroup();
                geoCirclePointsLayer.addTo(geoMap);

                var lineCoord = [];
                points.forEach(function (p) {
                    var lat = p.split(' ')[0];
                    var long = p.split(' ')[1];
                    lineCoord.push([lat, long]);
                });
                lineCoord.push([points[0].split(' ')[0], points[0].split(' ')[1]]);
                new L.polyline(lineCoord, { color: 'navy', weight: 2 }).addTo(geoCirclePointsLayer);

                geoCirclePointsLayer.addTo(geoMap);

                geoMap.fitBounds(geoCirclePointsLayer.getBounds());
                geoMap.setView([cLat, cLong], 11);
            }
        }
    };

    function nmToMeters(nm) {
        return nm / 0.000539957;
    }

    function feetToMeters(ft) {
        return ft / 3.28084;
    }

    var disableGeoMapPanning = function () {
        this.geoMap.scrollWheelZoom.disable();
        this.geoMap.dragging.disable();
        this.geoMap.touchZoom.disable();
        this.geoMap.doubleClickZoom.disable();
        this.geoMap.boxZoom.disable();
        this.geoMap.keyboard.disable();

        if (this.geoMap.tap) {
            this.geoMap.tap.disable();
        }
    };
    var clearPoints = function () {
        // clear circle layer from mpa
    };

    var renderPoints = function (latitude, longitude) {
        // create circle layer
        // add to map
        new L.circle([latitude, longitude], {
            color: 'black',
            fillColor: '#f02',
            fillOpacity: 0.3,
            radius:2
        }).addTo(geoMap);
    };

    var textCordinatesToGeojson = function (textCoordinates) {
        function convert(str) {
            try {
                const degrees = parseInt(str.length === 7 ? str.substring(0, 2) : str.substring(0, 3));
                const minutes = parseInt(str.length === 7 ? str.substring(2, 4) : str.substring(3, 5));
                const seconds = parseInt(str.length === 7 ? str.substring(4, 6) : str.substring(5, 7));
                const sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);

                var sing = sufx === 'W' || sufx === 'S' ? -1 : 1;
                return sing * (degrees + (minutes * 60.0 + seconds) / 3600.0);
            }
            catch (e) {
                return -1;
            }
        }

        const parts = (textCoordinates || "").split(' ');
        if (parts.length !== 2)
            return null;

        if (parts[0].length > 8 || parts[0].length < 7) {
            return null;
        }
        if (parts[1].length > 8 || parts[1].length < 7) {
            return null;
        }

        var lng = -1;
        var lat = convert(parts[0]);
        if (lat !== -1) {
            lng = convert(parts[1]);
        }

        if (lat === -1 || lng === -1) {
            return null;
        }

        return latitudeLongitudeToGeoJson(lat, lng);
    };

    var geojsonToTextCordinates = function (geojson) {
        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function convert(value, type) {
            try {
                const sign = value < 0;

                var abs = Math.abs(value);
                var deg = Math.trunc(abs);
                var min = Math.trunc((abs - deg) * 60.0);
                var sec = Math.round((abs - deg - min / 60.0) * 3600.0);
                if (sec > 59) {
                    sec = 0;
                    min = min + 1;
                    if (min > 59) {
                        min = 0;
                        deg = deg + 1;
                    }
                }

                if (type === 'lat') {
                    return pad(deg, 2) + pad(min, 2) + pad(sec, 2) + (sign ? 'S' : 'N');
                }

                return pad(deg, 3) + pad(min, 2) + pad(sec, 2) + (sign ? 'W' : 'E');
            }
            catch (e) {
                return "";
            }
        }

        const coordinates = geojson.features[0].geometry.coordinates;

        const lat = convert(coordinates[1], 'lat');
        const lng = convert(coordinates[0], 'lng');

        return lat + ' ' + lng;
    };

    var latitudeLongitudeToGeoJson = function (lat, lng) {
        return {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Point",
                        "bbox": [lat, lng, lat, lng],
                        "coordinates": [lng, lat]
                    }
                }
            ]
        };
    };

    var validateGeoMapSize = function () {
        if (geoMap) {
            geoMap.invalidateSize();
        }
    };

    return {
        initGeoMap: initGeoMap,
        isReadyGeoMap: isReadyGeoMap,
        disposeGeoMap: disposeGeoMap,
        resizeGeoMapForModal: validateGeoMapSize,
        disableGeoMapPan: disableGeoMapPanning,
        resetMapView: resetMapView,
        textCordinatesToGeojson: textCordinatesToGeojson,
        geojsonToTextCordinates: geojsonToTextCordinates,
        latitudeLongitudeToGeoJson: latitudeLongitudeToGeoJson,
        addGeoLocationAndRadius: addGeoLocationAndRadius,
        addPolygonPoints: addPolygonPoints,
        addCircleFromPoints: addCircleFromPoints,
        renderPoints: renderPoints,
        clearPoints: clearPoints,
        nmToMeters: nmToMeters,
        feetToMeters: feetToMeters,
        clearAerodromeLayer: clearAerodromeLayer,
        addAerodromeLocationAndRadius: addAerodromeLocationAndRadius,
        clearAerodromePopupLayer: clearAerodromePopupLayer,
        addAerodromesPopup: addAerodromesPopup
    };
};
