﻿import { Component, OnInit, Inject } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'
import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'
import { LocaleService, TranslationService } from 'angular-l10n';
import { LocationService } from '../../dashboard/shared/location.service'

import {
    IADCQueryOptions,
    IPaging,
    IAerodromeDisseminationCategory,
    IAhpNoDC,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

@Component({
    templateUrl: '/app/admin/adc/adc-tab-list.component.html'
})
export class ADCTabListComponent implements OnInit {

    adcs: IAerodromeDisseminationCategory[];
    selectedADC: IAerodromeDisseminationCategory = null;

    ahpNoDcs: IAhpNoDC[];
    selectedAhp: IAhpNoDC = null;

    currentTab: string;
    withdcCount: number = 0;
    withoutdcCount: number = 0;
    runWithDCBellAnimation: boolean = false;
    runWithoutDCBellAnimation: boolean = false;
    tabsDisabled: boolean = false;


    loadingData: boolean = false;

    withQueryOptions: IADCQueryOptions;
    withTableHeaders: ITableHeaders;
    withItemsPerPageData: Array<any>;
    //withItemsPerPageStartValue: string;
    withPaging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    withTotalPages: number = 0;

    withoutQueryOptions: IADCQueryOptions;
    withoutTableHeaders: ITableHeaders;
    withoutItemsPerPageData: Array<any>;
    //withoutItemsPerPageStartValue: string;
    withoutPaging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    withoutTotalPages: number = 0;
    intervalHandler = null;

    canbeDeleted: boolean = true;
    adcListForm: FormGroup; 

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService,
        public geoLocation: LocationService) {

        if (this.winRef.appConfig.ccs) // || this.winRef.appConfig.nofAdmin
            this.router.navigate(['/administration']);

        this.withTableHeaders = {
            sorting:
                [
                    { columnName: 'ahpCodeId', direction: 0 },
                    { columnName: 'ahpName', direction: 0 },
                    { columnName: 'disseminationCategory', direction: 0 },
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '200', text: '200' },
                    { id: '500', text: '500' },
                    { id: '1000', text: '1000' },
                ]
        };

        this.withPaging.pageSize = +this.withTableHeaders.itemsPerPage[0].id;
        this.withQueryOptions = {
            entity: "adc",
            page: 1,
            pageSize: this.withPaging.pageSize,
            sort: this.withTableHeaders.sorting[0].columnName, //Default Ascending
            lastId: 0,
            filterValue: ""
        }

        this.withoutTableHeaders = {
            sorting:
                [
                    { columnName: 'designator', direction: 0 },
                    { columnName: 'name', direction: 0 },
                    { columnName: 'servedCity', direction: 0 },
                    { columnName: 'fir', direction: 0 },
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                ]
        };

        this.withoutPaging.pageSize = +this.withoutTableHeaders.itemsPerPage[0].id;
        this.withoutQueryOptions = {
            entity: "ahpNoDC",
            page: 1,
            pageSize: this.withoutPaging.pageSize,
            sort: this.withoutTableHeaders.sorting[0].columnName, //Default Ascending
            lastId: 0,
            filterValue: ""
        }


        this.currentTab = "withdc";
    };

    ngOnInit() {

        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        const tabSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (queryOptions) {
            if (queryOptions.entity) {
                if (queryOptions.entity === "ahpNoDC") {
                    this.currentTab = "withoutdc";
                    this.withoutQueryOptions = queryOptions;
                    this.getAhpNoDCInPage(this.withoutQueryOptions.page);
                } else if (queryOptions.entity === "adc") {
                    this.currentTab = "withdc";
                    this.withQueryOptions = queryOptions;
                    this.getADCInPage(this.withQueryOptions.page);
                } else {
                    this.currentTab = "withdc";
                    this.getADCInPage(this.withQueryOptions.page);
                }
            } else {
                this.currentTab = "withdc";
                this.getADCInPage(this.withQueryOptions.page);
            }
        } else {
            this.currentTab = "withdc";
            this.getADCInPage(this.withQueryOptions.page);
        }
        this.configureReactiveForm();

        this.getPendingQueueStats();
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    onTabClick(queueName: string) {
        this.currentTab = queueName;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.currentTab);
        this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, (this.currentTab === 'withdc') ? this.withQueryOptions : this.withoutQueryOptions);

        if (this.currentTab === 'withdc' && !this.adcs) {
            this.getADCInPage(this.withQueryOptions.page);
        } else if (this.currentTab === 'withoutdc' && !this.ahpNoDcs) {
            this.getAhpNoDCInPage(this.withoutQueryOptions.page);
        }
    }

    private configureReactiveForm() {
        this.adcListForm = this.fb.group({
            searchWithDC: [{ value: this.withQueryOptions.filterValue, disabled: false }, []],
            searchWithoutDC: [{ value: this.withoutQueryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchWithDC").valueChanges.debounceTime(1000).subscribe(() => this.updateWithList());
        this.$ctrl("searchWithoutDC").valueChanges.debounceTime(1000).subscribe(() => this.updateWithoutList());
    }

    updateWithList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchWithDC');
        this.withQueryOptions.filterValue = subject.value;
        this.getADCInPage(this.withQueryOptions.page);
    }

    updateWithoutList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchWithoutDC');
        this.withoutQueryOptions.filterValue = subject.value;
        this.getAhpNoDCInPage(this.withoutQueryOptions.page);
    }

    $ctrl(name: string): AbstractControl {
        return this.adcListForm.get(name);
    }

    selectADC(adc: IAerodromeDisseminationCategory) {
        if (this.selectedADC) {
            this.selectedADC.isSelected = false;
        }
        this.canbeDeleted = true;
        this.selectedADC = adc;
        this.selectedADC.isSelected = true;
        this.withQueryOptions.lastId = this.selectedADC.id;
        this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withQueryOptions);
    }

    selectAhp(sdo: IAhpNoDC) {
        if (this.selectedAhp) {
            this.selectedAhp.isSelected = false;
        }
        this.selectedAhp = sdo;
        this.selectedAhp.isSelected = true;
        this.withoutQueryOptions.lastId = +this.selectedAhp.id;
        this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withoutQueryOptions);
    }

    itemsPerPageChanged(e: any): void {
        if (this.currentTab === 'withdc') {
            this.withQueryOptions.pageSize = e.value;
            //this.withItemsPerPageStartValue = e.value;
            this.getADCInPage(1);
        } else {
            this.withoutQueryOptions.pageSize = e.value;
            //this.withoutItemsPerPageStartValue = e.value;
            this.getAhpNoDCInPage(1);
        }
    }

    onPageChange(page: number) {
        if (this.currentTab === 'withdc') {
            this.getADCInPage(page);
        } else {
            this.getAhpNoDCInPage(page);
        }
    }

    sort(index) {
        if (this.currentTab === 'withdc') {
            const dir = this.withTableHeaders.sorting[index].direction;

            for (let i = 0; i < this.withTableHeaders.sorting.length; i++) {
                this.withTableHeaders.sorting[i].direction = 0;
            }

            if (dir > 0) {
                this.withTableHeaders.sorting[index].direction = -1;
                this.withQueryOptions.sort = '_' + this.withTableHeaders.sorting[index].columnName;
            }
            else if (dir <= 0) {
                this.withTableHeaders.sorting[index].direction = 1;
                this.withQueryOptions.sort = this.withTableHeaders.sorting[index].columnName;
            }

            this.getADCInPage(this.withQueryOptions.page);
        } else {
            const dir = this.withoutTableHeaders.sorting[index].direction;

            for (let i = 0; i < this.withoutTableHeaders.sorting.length; i++) {
                this.withoutTableHeaders.sorting[i].direction = 0;
            }

            if (dir > 0) {
                this.withoutTableHeaders.sorting[index].direction = -1;
                this.withoutQueryOptions.sort = '_' + this.withoutTableHeaders.sorting[index].columnName;
            }
            else if (dir <= 0) {
                this.withoutTableHeaders.sorting[index].direction = 1;
                this.withoutQueryOptions.sort = this.withoutTableHeaders.sorting[index].columnName;
            }
            this.getAhpNoDCInPage(this.withoutQueryOptions.page);
        }
    }

    sortingClass(index) {
        if (this.currentTab === 'withdc') {
            if (this.withTableHeaders.sorting[index].direction > 0)
                return 'sorting sorting_asc';

            if (this.withTableHeaders.sorting[index].direction < 0)
                return 'sorting sorting_desc';
        } else {
            if (this.withoutTableHeaders.sorting[index].direction > 0)
                return 'sorting sorting_asc';

            if (this.withoutTableHeaders.sorting[index].direction < 0)
                return 'sorting sorting_desc';
        }
        return 'sorting';
    }

    private getADCInPage(page) {
        try {
            this.loadingData = true;
            this.withQueryOptions.page = page;
            //this.withQueryOptions.pageSize = +this.withItemsPerPageStartValue;
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.currentTab);
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withQueryOptions);

            this.dataService.getRegisteredADC(this.withQueryOptions)
                .subscribe((adcs: any) => {
                    this.processADCs(adcs);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processADCs(adcsData: any) {
        this.withPaging = adcsData.paging;
        this.withTotalPages = adcsData.paging.totalPages;

        for (var iter = 0; iter < adcsData.data.length; iter++) adcsData.data[iter].index = iter;

        const lastADCIdSelected = this.withQueryOptions.lastId;

        if (adcsData.data.length > 0) {
            this.adcs = adcsData.data;
            let index = 0;
            if (lastADCIdSelected) {
                index = this.adcs.findIndex(x => x.id === lastADCIdSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.adcs[index].isSelected = true;
            this.selectedADC = this.adcs[index];
            this.loadingData = false;
        } else {
            this.adcs = [];
            this.selectedADC = null;
            this.loadingData = false;
        }
    }

    private getAhpNoDCInPage(page) {
        try {
            this.loadingData = true;
            this.withoutQueryOptions.page = page;
            //this.withoutQueryOptions.pageSize = +this.withoutItemsPerPageStartValue;
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.currentTab);
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withoutQueryOptions);

            this.dataService.getRegisteredAhpNoDC(this.withoutQueryOptions)
                .subscribe((ahpNoDcs: any) => {
                    this.processNoDCADCs(ahpNoDcs);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processNoDCADCs(ahpNoDcsData: any) {
        this.withoutPaging = ahpNoDcsData.paging;
        this.withoutTotalPages = ahpNoDcsData.paging.totalPages;

        for (var iter = 0; iter < ahpNoDcsData.data.length; iter++) ahpNoDcsData.data[iter].index = iter;

        const lastADCIdSelected = this.withoutQueryOptions.lastId;
        if (ahpNoDcsData.data.length > 0) {
            this.ahpNoDcs = ahpNoDcsData.data;
            let index = 0;
            if (lastADCIdSelected) {
                index = this.ahpNoDcs.findIndex(x => x.id === lastADCIdSelected.toString());
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.ahpNoDcs[index].isSelected = true;
            this.selectedAhp = this.ahpNoDcs[index];
            this.loadingData = false;
        } else {
            this.ahpNoDcs = [];
            this.selectedAhp = null;
            this.loadingData = false;
        }
    }

    public getPendingQueueStats() {
        try {
            this.dataService.getAhpNoDCTotalPending()
                .subscribe((count: any) => {
                    if (count != this.withoutdcCount) {
                        this.withoutdcCount = count;
                    }
                });
        }
        catch (e) {
        }
    }

    getLatitude(lat: number): string {
        return this.geoLocation.convertLatToDMS(lat);
    }

    getLongitude(long: number): string {
        return this.geoLocation.convertLongToDMS(long);
    }
}
