﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { WindowRef } from '../../common/windowRef.service';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms'
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { DataService } from '../shared/data.service'
import { LocationService } from '../../dashboard/shared/location.service'
import { IAerodromeDisseminationCategory, ISelectOptions } from '../shared/data.model';

@Component({
    templateUrl: '/app/admin/adc/adc-add-sdo.component.html'
})
export class ADCAddSdoComponent implements OnInit {

    model: any = null;
    action: string;

    addSdoForm: FormGroup;

    disseminationCategories: ISelectOptions[] = [];
    selectedDisseminationCategory?: string = '-1';
    disseminationCategoriesOptions: Select2Options;

    isReadOnly: boolean = false;
    isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService,
        public geoLocation: LocationService) {

        if (this.winRef.appConfig.ccs) // || this.winRef.appConfig.nofAdmin
            this.router.navigate(['/administration']);
    }

    ngOnInit() {

        this.disseminationCategoriesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('ADCSession.SelectDissCat')
            },
            width: "100%"
        }

        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.isReadOnly = this.action === "view";

        this.loadDissCategory();

        this.configureReactiveForm();
    }

    backToADCList() {
        this.router.navigate(['/administration/adc-tab']);
    }

    disableSubmit(): boolean {
        if (this.addSdoForm.pristine) return true;
        if (!this.addSdoForm.valid) return true;
        if (this.isSubmitting) return true;
        return false;
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        var adcToSave = this.prepareADC();
        this.dataService.saveADC(adcToSave)
            .subscribe(data => {
                let msg = this.translation.translate('ADCSession.SuccessADCCreated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/adc-tab"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('ADCSession.FailureADCCreated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });

    }

    private getLatitude(lat: number): string {
        return this.geoLocation.convertLatToDMS(lat);
    }

    private getLongitude(long: number): string {
        return this.geoLocation.convertLongToDMS(long);
    }

    private configureReactiveForm() {
        this.addSdoForm = this.fb.group({
            designator: [{ value: this.model.designator, disabled: true }, []],
            name: [{ value: this.model.name, disabled: true }, []],
            disseminationCategory: [{ value: String(this.model.disseminationCategory), disabled: this.isReadOnly }, [validateDisseminationCategory]],
            disseminationCategoryName: [{ value: this.model.disseminationCategoryName, disabled: this.isReadOnly }, [Validators.required]],
        });
    }

    private prepareADC(): IAerodromeDisseminationCategory {
        const adc: IAerodromeDisseminationCategory = {
            id: 0,
            ahpCodeId: this.model.designator,
            ahpMid: this.model.mid,
            ahpName: this.model.name,
            latitude: this.model.latitude,
            longitude: this.model.longitude,
            disseminationCategory: this.addSdoForm.get('disseminationCategory').value,
            disseminationCategoryName: this.addSdoForm.get('disseminationCategoryName').value.toUpperCase(),
            servedCity: "",
        }
        return adc;
    }

    public validateDissemination() {
        const fc = this.addSdoForm.get('disseminationCategory');
        if (validateDisseminationCategory(fc) !== null) return false;
        return true;
    }

    public onDisseminationChanged(data: { value: string }) {
        if (data.value) {
            var diss = this.disseminationCategories.find(x => x.id === String(data.value));
            if (diss) {
                this.addSdoForm.get('disseminationCategory').setValue(diss.id);
                this.addSdoForm.get('disseminationCategoryName').setValue(diss.text);
            } else {
                this.addSdoForm.get('disseminationCategory').setValue('-1');
                this.addSdoForm.get('disseminationCategoryName').setValue('');
            }
        } else {
            this.addSdoForm.get('disseminationCategory').setValue('-1');
            this.addSdoForm.get('disseminationCategoryName').setValue('');
        }
        if (data.value !== '-1' && this.selectedDisseminationCategory !== this.addSdoForm.get('disseminationCategory').value) {
            this.addSdoForm.markAsDirty();
        }
    }

    private loadDissCategory(): void {
        this.disseminationCategories = [
            { id: '-1', text: '' },
            { id: '0', text: 'National' },
            { id: '1', text: 'US' },
            { id: '2', text: 'International' },
        ];
        this.selectedDisseminationCategory = String(this.model.disseminationCategory) || '-1';

    }
}

function validateDisseminationCategory(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

