"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ADCAddSdoComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var windowRef_service_1 = require("../../common/windowRef.service");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var location_service_1 = require("../../dashboard/shared/location.service");
var ADCAddSdoComponent = /** @class */ (function () {
    function ADCAddSdoComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation, geoLocation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.geoLocation = geoLocation;
        this.model = null;
        this.disseminationCategories = [];
        this.selectedDisseminationCategory = '-1';
        this.isReadOnly = false;
        this.isSubmitting = false;
        if (this.winRef.appConfig.ccs) // || this.winRef.appConfig.nofAdmin
            this.router.navigate(['/administration']);
    }
    ADCAddSdoComponent.prototype.ngOnInit = function () {
        this.disseminationCategoriesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('ADCSession.SelectDissCat')
            },
            width: "100%"
        };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.isReadOnly = this.action === "view";
        this.loadDissCategory();
        this.configureReactiveForm();
    };
    ADCAddSdoComponent.prototype.backToADCList = function () {
        this.router.navigate(['/administration/adc-tab']);
    };
    ADCAddSdoComponent.prototype.disableSubmit = function () {
        if (this.addSdoForm.pristine)
            return true;
        if (!this.addSdoForm.valid)
            return true;
        if (this.isSubmitting)
            return true;
        return false;
    };
    ADCAddSdoComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var adcToSave = this.prepareADC();
        this.dataService.saveADC(adcToSave)
            .subscribe(function (data) {
            var msg = _this.translation.translate('ADCSession.SuccessADCCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/adc-tab"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('ADCSession.FailureADCCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    ADCAddSdoComponent.prototype.getLatitude = function (lat) {
        return this.geoLocation.convertLatToDMS(lat);
    };
    ADCAddSdoComponent.prototype.getLongitude = function (long) {
        return this.geoLocation.convertLongToDMS(long);
    };
    ADCAddSdoComponent.prototype.configureReactiveForm = function () {
        this.addSdoForm = this.fb.group({
            designator: [{ value: this.model.designator, disabled: true }, []],
            name: [{ value: this.model.name, disabled: true }, []],
            disseminationCategory: [{ value: String(this.model.disseminationCategory), disabled: this.isReadOnly }, [validateDisseminationCategory]],
            disseminationCategoryName: [{ value: this.model.disseminationCategoryName, disabled: this.isReadOnly }, [forms_1.Validators.required]],
        });
    };
    ADCAddSdoComponent.prototype.prepareADC = function () {
        var adc = {
            id: 0,
            ahpCodeId: this.model.designator,
            ahpMid: this.model.mid,
            ahpName: this.model.name,
            latitude: this.model.latitude,
            longitude: this.model.longitude,
            disseminationCategory: this.addSdoForm.get('disseminationCategory').value,
            disseminationCategoryName: this.addSdoForm.get('disseminationCategoryName').value.toUpperCase(),
            servedCity: "",
        };
        return adc;
    };
    ADCAddSdoComponent.prototype.validateDissemination = function () {
        var fc = this.addSdoForm.get('disseminationCategory');
        if (validateDisseminationCategory(fc) !== null)
            return false;
        return true;
    };
    ADCAddSdoComponent.prototype.onDisseminationChanged = function (data) {
        if (data.value) {
            var diss = this.disseminationCategories.find(function (x) { return x.id === String(data.value); });
            if (diss) {
                this.addSdoForm.get('disseminationCategory').setValue(diss.id);
                this.addSdoForm.get('disseminationCategoryName').setValue(diss.text);
            }
            else {
                this.addSdoForm.get('disseminationCategory').setValue('-1');
                this.addSdoForm.get('disseminationCategoryName').setValue('');
            }
        }
        else {
            this.addSdoForm.get('disseminationCategory').setValue('-1');
            this.addSdoForm.get('disseminationCategoryName').setValue('');
        }
        if (data.value !== '-1' && this.selectedDisseminationCategory !== this.addSdoForm.get('disseminationCategory').value) {
            this.addSdoForm.markAsDirty();
        }
    };
    ADCAddSdoComponent.prototype.loadDissCategory = function () {
        this.disseminationCategories = [
            { id: '-1', text: '' },
            { id: '0', text: 'National' },
            { id: '1', text: 'US' },
            { id: '2', text: 'International' },
        ];
        this.selectedDisseminationCategory = String(this.model.disseminationCategory) || '-1';
    };
    ADCAddSdoComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/adc/adc-add-sdo.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService,
            location_service_1.LocationService])
    ], ADCAddSdoComponent);
    return ADCAddSdoComponent;
}());
exports.ADCAddSdoComponent = ADCAddSdoComponent;
function validateDisseminationCategory(c) {
    if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
//# sourceMappingURL=adc-add-sdo.component.js.map