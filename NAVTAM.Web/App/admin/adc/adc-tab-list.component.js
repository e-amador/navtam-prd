"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ADCTabListComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var location_service_1 = require("../../dashboard/shared/location.service");
var ADCTabListComponent = /** @class */ (function () {
    function ADCTabListComponent(toastr, fb, dataService, memStorageService, router, winRef, route, locale, translation, geoLocation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.geoLocation = geoLocation;
        this.selectedADC = null;
        this.selectedAhp = null;
        this.withdcCount = 0;
        this.withoutdcCount = 0;
        this.runWithDCBellAnimation = false;
        this.runWithoutDCBellAnimation = false;
        this.tabsDisabled = false;
        this.loadingData = false;
        //withItemsPerPageStartValue: string;
        this.withPaging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.withTotalPages = 0;
        //withoutItemsPerPageStartValue: string;
        this.withoutPaging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.withoutTotalPages = 0;
        this.intervalHandler = null;
        this.canbeDeleted = true;
        if (this.winRef.appConfig.ccs) // || this.winRef.appConfig.nofAdmin
            this.router.navigate(['/administration']);
        this.withTableHeaders = {
            sorting: [
                { columnName: 'ahpCodeId', direction: 0 },
                { columnName: 'ahpName', direction: 0 },
                { columnName: 'disseminationCategory', direction: 0 },
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
                { id: '500', text: '500' },
                { id: '1000', text: '1000' },
            ]
        };
        this.withPaging.pageSize = +this.withTableHeaders.itemsPerPage[0].id;
        this.withQueryOptions = {
            entity: "adc",
            page: 1,
            pageSize: this.withPaging.pageSize,
            sort: this.withTableHeaders.sorting[0].columnName,
            lastId: 0,
            filterValue: ""
        };
        this.withoutTableHeaders = {
            sorting: [
                { columnName: 'designator', direction: 0 },
                { columnName: 'name', direction: 0 },
                { columnName: 'servedCity', direction: 0 },
                { columnName: 'fir', direction: 0 },
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
            ]
        };
        this.withoutPaging.pageSize = +this.withoutTableHeaders.itemsPerPage[0].id;
        this.withoutQueryOptions = {
            entity: "ahpNoDC",
            page: 1,
            pageSize: this.withoutPaging.pageSize,
            sort: this.withoutTableHeaders.sorting[0].columnName,
            lastId: 0,
            filterValue: ""
        };
        this.currentTab = "withdc";
    }
    ;
    ADCTabListComponent.prototype.ngOnInit = function () {
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        var tabSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (queryOptions) {
            if (queryOptions.entity) {
                if (queryOptions.entity === "ahpNoDC") {
                    this.currentTab = "withoutdc";
                    this.withoutQueryOptions = queryOptions;
                    this.getAhpNoDCInPage(this.withoutQueryOptions.page);
                }
                else if (queryOptions.entity === "adc") {
                    this.currentTab = "withdc";
                    this.withQueryOptions = queryOptions;
                    this.getADCInPage(this.withQueryOptions.page);
                }
                else {
                    this.currentTab = "withdc";
                    this.getADCInPage(this.withQueryOptions.page);
                }
            }
            else {
                this.currentTab = "withdc";
                this.getADCInPage(this.withQueryOptions.page);
            }
        }
        else {
            this.currentTab = "withdc";
            this.getADCInPage(this.withQueryOptions.page);
        }
        this.configureReactiveForm();
        this.getPendingQueueStats();
    };
    Object.defineProperty(ADCTabListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    ADCTabListComponent.prototype.onTabClick = function (queueName) {
        this.currentTab = queueName;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.currentTab);
        this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, (this.currentTab === 'withdc') ? this.withQueryOptions : this.withoutQueryOptions);
        if (this.currentTab === 'withdc' && !this.adcs) {
            this.getADCInPage(this.withQueryOptions.page);
        }
        else if (this.currentTab === 'withoutdc' && !this.ahpNoDcs) {
            this.getAhpNoDCInPage(this.withoutQueryOptions.page);
        }
    };
    ADCTabListComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.adcListForm = this.fb.group({
            searchWithDC: [{ value: this.withQueryOptions.filterValue, disabled: false }, []],
            searchWithoutDC: [{ value: this.withoutQueryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchWithDC").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateWithList(); });
        this.$ctrl("searchWithoutDC").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateWithoutList(); });
    };
    ADCTabListComponent.prototype.updateWithList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchWithDC');
        this.withQueryOptions.filterValue = subject.value;
        this.getADCInPage(this.withQueryOptions.page);
    };
    ADCTabListComponent.prototype.updateWithoutList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchWithoutDC');
        this.withoutQueryOptions.filterValue = subject.value;
        this.getAhpNoDCInPage(this.withoutQueryOptions.page);
    };
    ADCTabListComponent.prototype.$ctrl = function (name) {
        return this.adcListForm.get(name);
    };
    ADCTabListComponent.prototype.selectADC = function (adc) {
        if (this.selectedADC) {
            this.selectedADC.isSelected = false;
        }
        this.canbeDeleted = true;
        this.selectedADC = adc;
        this.selectedADC.isSelected = true;
        this.withQueryOptions.lastId = this.selectedADC.id;
        this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withQueryOptions);
    };
    ADCTabListComponent.prototype.selectAhp = function (sdo) {
        if (this.selectedAhp) {
            this.selectedAhp.isSelected = false;
        }
        this.selectedAhp = sdo;
        this.selectedAhp.isSelected = true;
        this.withoutQueryOptions.lastId = +this.selectedAhp.id;
        this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withoutQueryOptions);
    };
    ADCTabListComponent.prototype.itemsPerPageChanged = function (e) {
        if (this.currentTab === 'withdc') {
            this.withQueryOptions.pageSize = e.value;
            //this.withItemsPerPageStartValue = e.value;
            this.getADCInPage(1);
        }
        else {
            this.withoutQueryOptions.pageSize = e.value;
            //this.withoutItemsPerPageStartValue = e.value;
            this.getAhpNoDCInPage(1);
        }
    };
    ADCTabListComponent.prototype.onPageChange = function (page) {
        if (this.currentTab === 'withdc') {
            this.getADCInPage(page);
        }
        else {
            this.getAhpNoDCInPage(page);
        }
    };
    ADCTabListComponent.prototype.sort = function (index) {
        if (this.currentTab === 'withdc') {
            var dir = this.withTableHeaders.sorting[index].direction;
            for (var i = 0; i < this.withTableHeaders.sorting.length; i++) {
                this.withTableHeaders.sorting[i].direction = 0;
            }
            if (dir > 0) {
                this.withTableHeaders.sorting[index].direction = -1;
                this.withQueryOptions.sort = '_' + this.withTableHeaders.sorting[index].columnName;
            }
            else if (dir <= 0) {
                this.withTableHeaders.sorting[index].direction = 1;
                this.withQueryOptions.sort = this.withTableHeaders.sorting[index].columnName;
            }
            this.getADCInPage(this.withQueryOptions.page);
        }
        else {
            var dir = this.withoutTableHeaders.sorting[index].direction;
            for (var i = 0; i < this.withoutTableHeaders.sorting.length; i++) {
                this.withoutTableHeaders.sorting[i].direction = 0;
            }
            if (dir > 0) {
                this.withoutTableHeaders.sorting[index].direction = -1;
                this.withoutQueryOptions.sort = '_' + this.withoutTableHeaders.sorting[index].columnName;
            }
            else if (dir <= 0) {
                this.withoutTableHeaders.sorting[index].direction = 1;
                this.withoutQueryOptions.sort = this.withoutTableHeaders.sorting[index].columnName;
            }
            this.getAhpNoDCInPage(this.withoutQueryOptions.page);
        }
    };
    ADCTabListComponent.prototype.sortingClass = function (index) {
        if (this.currentTab === 'withdc') {
            if (this.withTableHeaders.sorting[index].direction > 0)
                return 'sorting sorting_asc';
            if (this.withTableHeaders.sorting[index].direction < 0)
                return 'sorting sorting_desc';
        }
        else {
            if (this.withoutTableHeaders.sorting[index].direction > 0)
                return 'sorting sorting_asc';
            if (this.withoutTableHeaders.sorting[index].direction < 0)
                return 'sorting sorting_desc';
        }
        return 'sorting';
    };
    ADCTabListComponent.prototype.getADCInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.withQueryOptions.page = page;
            //this.withQueryOptions.pageSize = +this.withItemsPerPageStartValue;
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.currentTab);
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withQueryOptions);
            this.dataService.getRegisteredADC(this.withQueryOptions)
                .subscribe(function (adcs) {
                _this.processADCs(adcs);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    ADCTabListComponent.prototype.processADCs = function (adcsData) {
        this.withPaging = adcsData.paging;
        this.withTotalPages = adcsData.paging.totalPages;
        for (var iter = 0; iter < adcsData.data.length; iter++)
            adcsData.data[iter].index = iter;
        var lastADCIdSelected = this.withQueryOptions.lastId;
        if (adcsData.data.length > 0) {
            this.adcs = adcsData.data;
            var index = 0;
            if (lastADCIdSelected) {
                index = this.adcs.findIndex(function (x) { return x.id === lastADCIdSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.adcs[index].isSelected = true;
            this.selectedADC = this.adcs[index];
            this.loadingData = false;
        }
        else {
            this.adcs = [];
            this.selectedADC = null;
            this.loadingData = false;
        }
    };
    ADCTabListComponent.prototype.getAhpNoDCInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.withoutQueryOptions.page = page;
            //this.withoutQueryOptions.pageSize = +this.withoutItemsPerPageStartValue;
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.currentTab);
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.withoutQueryOptions);
            this.dataService.getRegisteredAhpNoDC(this.withoutQueryOptions)
                .subscribe(function (ahpNoDcs) {
                _this.processNoDCADCs(ahpNoDcs);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    ADCTabListComponent.prototype.processNoDCADCs = function (ahpNoDcsData) {
        this.withoutPaging = ahpNoDcsData.paging;
        this.withoutTotalPages = ahpNoDcsData.paging.totalPages;
        for (var iter = 0; iter < ahpNoDcsData.data.length; iter++)
            ahpNoDcsData.data[iter].index = iter;
        var lastADCIdSelected = this.withoutQueryOptions.lastId;
        if (ahpNoDcsData.data.length > 0) {
            this.ahpNoDcs = ahpNoDcsData.data;
            var index = 0;
            if (lastADCIdSelected) {
                index = this.ahpNoDcs.findIndex(function (x) { return x.id === lastADCIdSelected.toString(); });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.ahpNoDcs[index].isSelected = true;
            this.selectedAhp = this.ahpNoDcs[index];
            this.loadingData = false;
        }
        else {
            this.ahpNoDcs = [];
            this.selectedAhp = null;
            this.loadingData = false;
        }
    };
    ADCTabListComponent.prototype.getPendingQueueStats = function () {
        var _this = this;
        try {
            this.dataService.getAhpNoDCTotalPending()
                .subscribe(function (count) {
                if (count != _this.withoutdcCount) {
                    _this.withoutdcCount = count;
                }
            });
        }
        catch (e) {
        }
    };
    ADCTabListComponent.prototype.getLatitude = function (lat) {
        return this.geoLocation.convertLatToDMS(lat);
    };
    ADCTabListComponent.prototype.getLongitude = function (long) {
        return this.geoLocation.convertLongToDMS(long);
    };
    ADCTabListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/adc/adc-tab-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService,
            location_service_1.LocationService])
    ], ADCTabListComponent);
    return ADCTabListComponent;
}());
exports.ADCTabListComponent = ADCTabListComponent;
//# sourceMappingURL=adc-tab-list.component.js.map