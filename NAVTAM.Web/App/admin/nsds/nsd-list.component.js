"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdListComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var NsdListComponent = /** @class */ (function () {
    function NsdListComponent(toastr, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.selectedNsd = null;
        this.previousSelectedNsd = null;
        this.selectedOrganization = null;
        this.loadingData = false;
        this.canbeDeleted = false;
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
        this.tableHeaders = {
            sorting: [
                { columnName: 'Name', direction: 0 },
                { columnName: 'Category', direction: 0 },
                { columnName: 'FreeText', direction: 0 },
                { columnName: 'Operational', direction: 0 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
    }
    ;
    NsdListComponent.prototype.ngOnInit = function () {
        this.getNsds();
        this.loadingData = false;
    };
    Object.defineProperty(NsdListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    NsdListComponent.prototype.activateNsd = function (nsd) {
        if (nsd) {
            this.enableNsd(nsd);
        }
    };
    NsdListComponent.prototype.adjustNsdStatusModal = function () {
        if (this.selectedNsd) {
            $('#modal-adjust-operational-status').modal({});
        }
    };
    NsdListComponent.prototype.adjustNsdStatus = function () {
        if (this.selectedNsd) {
            this.selectedNsd.operational ? this.disableNsd(this.selectedNsd) : this.activateNsd(this.selectedNsd);
        }
        this.onComplete(this.selectedNsd, this.selectedOrganization);
    };
    NsdListComponent.prototype.isLoadingData = function () {
        return this.loadingData;
    };
    NsdListComponent.prototype.adjustOrganizationNsd = function () {
        if (this.selectedNsd && this.selectedOrganization) {
            $('#modal-remove-organizations-status').modal({});
        }
    };
    NsdListComponent.prototype.disableNsd = function (nsd) {
        if (nsd) {
            this.deactivateNsd(nsd);
        }
    };
    NsdListComponent.prototype.isOperational = function () {
        if (this.selectedNsd) {
            return this.selectedNsd.operational;
        }
        return false;
    };
    NsdListComponent.prototype.isOperationalListStatus = function (nsd) {
        return nsd.operational;
    };
    NsdListComponent.prototype.removeallOrganizations = function () {
        $('#modal-revoke-organization-status').modal({});
        $('#modal-remove-allorganizations').modal({});
    };
    NsdListComponent.prototype.revokeOrganizationAccess = function () {
        var _this = this;
        if (this.selectedNsd && this.selectedOrganization) {
            this.selectedNsd.orgId = this.selectedOrganization.id;
            this.dataService.removeNsdFromOrganization(this.selectedNsd).subscribe((function (x) { _this.getOrganizations(+_this.selectedNsd.id); }));
            this.selectedOrganization = null;
        }
    };
    NsdListComponent.prototype.isNsdSelected = function () {
        if (this.selectedNsd)
            return true;
        return false;
    };
    NsdListComponent.prototype.isOrganizationSelected = function () {
        if (this.selectedOrganization)
            return true;
        return false;
    };
    NsdListComponent.prototype.onNsdSelect = function () {
        this.getOrganizations(+this.selectedNsd.id);
    };
    NsdListComponent.prototype.onComplete = function (nsd, org) {
        if (nsd) {
            this.selectedNsd = nsd;
        }
        if (org) {
            this.selectedOrganization = org;
        }
    };
    NsdListComponent.prototype.selectNsd = function (nsd) {
        if (this.selectedNsd) {
            this.selectedNsd.isSelected = false;
        }
        this.selectedOrganization = null;
        this.selectedNsd = nsd;
        this.selectedNsd.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedNsd.id);
        this.onNsdSelect();
    };
    NsdListComponent.prototype.selectOrg = function (org) {
        if (this.selectedOrganization) {
            this.selectedOrganization.isSelected = false;
            this.selectedOrganization = null;
        }
        if (org.id > -1) {
            this.selectedOrganization = org;
            this.selectedOrganization.isSelected = true;
            this.selectedNsd.orgId = this.selectedOrganization.id;
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedOrganization.id);
        }
    };
    NsdListComponent.prototype.isOrgSelected = function () {
        if (this.selectedOrganization)
            return true;
        return false;
    };
    NsdListComponent.prototype.enableNsd = function (selectedNsd) {
        var _this = this;
        if (this.isNsdSelected()) {
            this.memStorageService.save("nsd", this.selectedNsd);
            this.dataService.activateNsd(selectedNsd).subscribe(function () { return _this.getNsds(); });
            this.getOrganizations(+this.selectedNsd.id);
            this.selectedNsd = this.memStorageService.get("nsd");
            this.memStorageService.remove("nsd");
        }
        return;
    };
    NsdListComponent.prototype.deactivateNsd = function (selectedNsd) {
        var _this = this;
        if (this.isNsdSelected()) {
            this.memStorageService.save("nsd", this.selectedNsd);
            this.dataService.disableNsd(selectedNsd).subscribe(function () { return _this.getNsds(); });
            this.getOrganizations(+this.selectedNsd.id);
            this.selectedNsd = this.memStorageService.get("nsd");
            this.memStorageService.remove("nsd");
        }
        return;
    };
    NsdListComponent.prototype.getNsds = function () {
        var _this = this;
        try {
            this.loadingData = true;
            this.dataService.getNsds().subscribe(function (nsds) { _this.processNsd(nsds); });
            this.loadingData = false;
        }
        catch (e) { } //TODO: Log client side actions?
    };
    NsdListComponent.prototype.getOrganizations = function (id) {
        var _this = this;
        this.dataService.getOrganizationsWithNsdId(id).subscribe(function (orgs) { _this.processOrgs(orgs); });
    };
    NsdListComponent.prototype.organizationIsSelected = function () {
        if (this.selectedOrganization) {
            return this.selectedOrganization.isSelected;
        }
        return false;
    };
    NsdListComponent.prototype.removeFromAllOrganizations = function (selectedNsd) {
        var _this = this;
        if (this.isNsdSelected()) {
            this.dataService.removeFromAllOrganizations(selectedNsd).subscribe(function () { return _this.getNsds(); });
            this.getOrganizations(+this.selectedNsd.id);
        }
        this.onComplete(this.selectedNsd, this.selectedOrganization);
        return;
    };
    NsdListComponent.prototype.processOrgs = function (orgs) {
        var orgArray = [];
        for (var _i = 0, orgs_1 = orgs; _i < orgs_1.length; _i++) {
            var org = orgs_1[_i];
            if (!org.isReadOnlyOrg) {
                var organization = { id: org.id, text: org.name };
                orgArray.push(organization);
            }
        }
        if (orgArray.length == 0) {
            var noOrgsExist = {
                id: -1,
                text: "No Organizations"
            };
            orgArray.push(noOrgsExist);
        }
        this.orgs = orgArray;
    };
    NsdListComponent.prototype.removeNsdFromOrganization = function () {
        var _this = this;
        if (this.selectedNsd && this.selectedOrganization.isSelected) {
            this.dataService.removeNsdFromOrganization(this.selectedNsd).subscribe(function () { return _this.getOrganizations(+_this.selectedNsd.id); });
            this.onComplete(this.selectedNsd, this.selectedOrganization);
        }
    };
    NsdListComponent.prototype.processNsd = function (nsdArray) {
        var _this = this;
        this.nsds = [];
        this.nsds = nsdArray;
        var index = 0;
        if (this.previousSelectedNsd) {
            index = this.nsds.findIndex(function (x) { return x.id === _this.selectedNsd.id; });
            if (index < 0)
                index = 0;
        }
        this.nsds[index].isSelected = true;
        this.selectedNsd = this.nsds[index];
        this.onNsdSelect();
    };
    NsdListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/nsds/nsd-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], NsdListComponent);
    return NsdListComponent;
}());
exports.NsdListComponent = NsdListComponent;
//# sourceMappingURL=nsd-list.component.js.map