"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adminRoutes = void 0;
var index_1 = require("./index");
exports.adminRoutes = [
    { path: 'administration', redirectTo: 'administration/users', pathMatch: 'full' },
    { path: 'path/*', redirectTo: 'administration/users', pathMatch: 'full' },
    // USERS
    { path: 'administration/users', component: index_1.UserListComponent, resolve: { roles: index_1.RolesResolver } },
    { path: 'administration/users/create', component: index_1.UserNewComponent },
    {
        path: 'administration/users/edit/:id', component: index_1.UserEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.UserModelResolver }
    },
    {
        path: 'administration/users/view/:id', component: index_1.UserEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.UserModelResolver }
    },
    { path: 'administration/users/resetpassword/:id', component: index_1.UserResetPasswordComponent, resolve: { model: index_1.UserModelResolver } },
    //ORG
    { path: 'administration/orgs', component: index_1.OrgListComponent },
    { path: 'administration/orgs/create', component: index_1.OrgNewComponent },
    {
        path: 'administration/orgs/edit/:id', component: index_1.OrgEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.OrgModelResolver }
    },
    {
        path: 'administration/orgs/view/:id', component: index_1.OrgEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.OrgModelResolver }
    },
    //ICAO Subjects & Conditions
    { path: 'administration/icaosubjects', component: index_1.IcaoSubjectsListComponent },
    { path: 'administration/icaosubjects/create', component: index_1.IcaoSubjectNewComponent },
    { path: 'administration/icaosubjects/createcondition/:subjectId', component: index_1.IcaoConditionNewComponent },
    {
        path: 'administration/icaosubjects/edit/:id', component: index_1.IcaoSubjectEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.IcaoSubjectModelResolver }
    },
    {
        path: 'administration/icaosubjects/view/:id', component: index_1.IcaoSubjectEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.IcaoSubjectModelResolver }
    },
    {
        path: 'administration/icaosubjects/editcondition/:id', component: index_1.IcaoConditionEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.IcaoConditionModelResolver }
    },
    {
        path: 'administration/icaosubjects/viewcondition/:id', component: index_1.IcaoConditionEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.IcaoConditionModelResolver }
    },
    //SERIES ALLOCATION
    { path: 'administration/series', component: index_1.SeriesListComponent },
    { path: 'administration/series/create', component: index_1.SeriesNewComponent },
    {
        path: 'administration/series/edit/:id', component: index_1.SeriesEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.SeriesModelResolver }
    },
    {
        path: 'administration/series/view/:id', component: index_1.SeriesEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.SeriesModelResolver }
    },
    //AERODROME DISSEMINATION CATEGORY
    { path: 'administration/adc-tab', component: index_1.ADCTabListComponent },
    {
        path: 'administration/adc/add/:id', component: index_1.ADCAddSdoComponent,
        data: [{ action: 'add' }],
        resolve: { model: index_1.AhpModelResolver }
    },
    {
        path: 'administration/adc/edit/:id', component: index_1.ADCEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.AdcModelResolver }
    },
    //DOMAIN OF AUTHORITY
    { path: 'administration/doas', component: index_1.DoasListComponent },
    {
        path: 'administration/doas/create', component: index_1.DoaNewComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/doas/view/:id', component: index_1.DoaEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.DoaModelResolver }
    },
    {
        path: 'administration/doas/edit-modify/:id', component: index_1.DoaEditModifyComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.DoaModelResolver }
    },
    {
        path: 'administration/doas/copy/:id', component: index_1.DoaEditModifyComponent,
        data: [{ action: 'copy' }],
        resolve: { model: index_1.DoaCopyModelResolver }
    },
    //NOTAM SCENARIO DEFINITIONS
    { path: 'administration/nsds', component: index_1.NsdListComponent },
    //Bilingual Region
    {
        path: 'administration/bregion/edit', component: index_1.BilingualRegionEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.BilingualRegionModelResolver }
    },
    //Northern Region
    {
        path: 'administration/nregion/edit', component: index_1.NorthernRegionEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.NorthernRegionModelResolver }
    },
    //GeoRegion
    { path: 'administration/georegion', component: index_1.GeoRegionListComponent },
    {
        path: 'administration/georegion/view/:id', component: index_1.GeoRegionShowComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.GeoRegionModelResolver }
    },
    {
        path: 'administration/georegion/create', component: index_1.GeoRegionNewComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/georegion/create/:id', component: index_1.GeoRegionNewComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.GeoRegionModelResolver }
    },
    //Subscription
    { path: 'administration/subscription', component: index_1.SubscComponent },
    {
        path: 'administration/subscription/view/:id', component: index_1.SubscEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: index_1.SubscriptionResolver }
    },
    {
        path: 'administration/subscription/create', component: index_1.SubscCreateComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/subscription/edit/:id', component: index_1.SubscEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.SubscriptionResolver }
    },
    //CONFIG
    { path: 'administration/config', component: index_1.ConfigListComponent },
    {
        path: 'administration/config/create', component: index_1.ConfigNewComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/config/edit/:category/:name', component: index_1.ConfigNewComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.ConfigModelResolver }
    },
    //Checklist
    {
        path: 'administration/checklist', component: index_1.ChecklistComponent,
        resolve: { model: index_1.ChecklistModelResolver }
    },
    {
        path: 'administration/checklist/create', component: index_1.SeriesChecklistNewComponent,
        data: [{ action: 'create' }]
    },
    {
        path: 'administration/checklist/edit/:series', component: index_1.SeriesChecklistNewComponent,
        data: [{ action: 'edit' }],
        resolve: { model: index_1.SeriesChecklistResolver }
    },
    //AeroRDS
    { path: 'administration/aerords', component: index_1.AerordsComponent },
];
//# sourceMappingURL=admin.routes.js.map