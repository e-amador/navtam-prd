﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { WindowRef } from '../../common/windowRef.service';
import { Select2OptionData } from 'ng2-select2';
import { LocaleService, TranslationService } from 'angular-l10n';
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { DataService } from '../shared/data.service'
import { IIcaoSubjectPartial } from '../shared/data.model';

@Component({ 
    templateUrl: '/app/admin/icaosubjects/icaosubjects-edit.component.html'
})
export class IcaoSubjectEditComponent implements OnInit  {
    
    model: any = null;
    action: string;

    icaoSubjectForm: FormGroup; 
    icaoNameLength: number = 150;
    isSubmitting: boolean = false;

    isReadOnly: boolean;

    public entityCodeOptions: Select2Options;
    public selectedEntityCodeId?: string;
    public entityCodes: Array<Select2OptionData>;

    public scopeOptions: Select2Options;
    public selectedScopeId?: string;
    public scopes: Array<Select2OptionData>;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.entityCodeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('IcaoSubjectSession.SelectEntityCode')
            },
            width: "100%"
        }

        this.scopeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('IcaoSubjectSession.SelectScope')
            },
            width: "100%"
        }

        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];

        this.isReadOnly = this.action === "view";

        this.configureReactiveForm();

        this.createEntityCodesAndScopes();
    }

    onSelectedEntityCodeChanged(data: { value: string }) {
        if (data.value !== "-1") {
            this.icaoSubjectForm.get('entityCode').setValue(data.value);
            this.icaoSubjectForm.markAsDirty();
        } else {
            this.icaoSubjectForm.get('entityCode').setValue("-1");
        }
    }

    onSelectedScopeChanged(data: { value: string }) {
        if (data.value !== "-1") {
            this.icaoSubjectForm.get('scope').setValue(data.value);
            this.icaoSubjectForm.markAsDirty();
        } else {
            this.icaoSubjectForm.get('scope').setValue("-1");
        }
    }

    backToIcaoSubjectList() {
        this.router.navigate(['/administration/icaosubjects']);
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        const icaoSubject = this.prepareUpdateIcaoSubject();
        this.dataService.updateIcaoSubject(icaoSubject)
            .subscribe(data => {
                let msg = this.translation.translate('IcaoSubjectSession.SuccessUpdated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.backToIcaoSubjectList();
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('IcaoSubjectSession.FailureUpdated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    validateEntityCode() {
        const fc = this.icaoSubjectForm.get("entityCode");
        return fc.value !== -1 || this.icaoSubjectForm.pristine;
    }

    validateCode23() {
        if (this.icaoSubjectForm.pristine) return true;
        const fc = this.icaoSubjectForm.get("code");
        if (fc.errors) return false;
        if (!fc.value) return false;
        if (!isALetter(fc.value)) return false;
        if (fc.value.length !== 2) return false;

        return true;
    }

    validateScope() {
        const fc = this.icaoSubjectForm.get("scope");
        return fc.value !== -1 || this.icaoSubjectForm.pristine;
    }

    validateName() {
        const nameCtrl = this.icaoSubjectForm.get("name");        
        return nameCtrl.valid || this.icaoSubjectForm.pristine;
    }

    validateNameLength(ctrlName: string) {
        const nameCtrl = this.icaoSubjectForm.get(ctrlName);
        let nameValue: string = nameCtrl.value;
        return nameValue.length > this.icaoNameLength;
    }

    validateNameFrench() {
        const nameFrenchCtrl = this.icaoSubjectForm.get("nameFrench");
        return nameFrenchCtrl.valid || this.icaoSubjectForm.pristine;
    }

    checkboxChanged(controlName: string, checked: boolean) {
        const control = this.icaoSubjectForm.get(controlName);
        control.setValue(checked);
        control.markAsDirty();
    }

    private createEntityCodesAndScopes(): void {
        this.entityCodes = [
            { id: "-1", text: this.translation.translate('IcaoSubjectSession.SelectEntityCode') },
            { id: "Ahp", text: "Aerodrome" },
            { id: "Fir", text: "FIR" },
            { id: "Obs", text: "Obstacle" },
            { id: "Rwy", text: "Runway" },
        ]
        this.selectedEntityCodeId = this.model.entityCode;

        this.scopes = [
            { id: "-1", text: this.translation.translate('IcaoSubjectSession.SelectEntityCode') },
            { id: 'A', text: 'A' },
            { id: 'E', text: 'E' },
            { id: 'W', text: 'W' },
            { id: 'AE', text: 'AE' },
            { id: 'AW', text: 'AW' },
            { id: 'K', text: 'K' }
        ]
        this.selectedScopeId = this.model.scope;
    }

    private configureReactiveForm() {
        this.icaoSubjectForm = this.fb.group({
            entityCode: [{ value: this.model.entityCode, disabled: this.isReadOnly }, [Validators.required]],
            code: [{ value: this.model.code, disabled: this.isReadOnly }, [Validators.required]],
            scope: [{ value: this.model.scope, disabled: this.isReadOnly }, [Validators.required]],
            name: [{ value: this.model.name, disabled: this.isReadOnly }, [Validators.required]],
            nameFrench: [{ value: this.model.nameFrench, disabled: this.isReadOnly }, [Validators.required]],
            requiresItemA: [{ value: this.model.requiresItemA, disabled: this.isReadOnly }],
            requiresSeries: [{ value: this.model.requiresSeries, disabled: this.isReadOnly }],
            requiresScope: [{ value: this.model.requiresScope, disabled: this.isReadOnly }],
        });
    }

    private prepareUpdateIcaoSubject(): IIcaoSubjectPartial {
        var code23 = this.icaoSubjectForm.get('code').value;
        code23 = code23.toString().toUpperCase();
        const icaoSubject: IIcaoSubjectPartial = {
            id: this.model.id,
            entityCode: this.icaoSubjectForm.get('entityCode').value,
            code: code23,
            scope: this.icaoSubjectForm.get('scope').value,
            name: this.icaoSubjectForm.get('name').value,
            nameFrench: this.icaoSubjectForm.get('nameFrench').value,
            requiresItemA: this.icaoSubjectForm.get('requiresItemA').value,
            requiresSeries: this.icaoSubjectForm.get('requiresSeries').value,
            requiresScope: this.icaoSubjectForm.get('requiresScope').value,
        }

        return icaoSubject;
    }

    public validateKeyPressLetter(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (key !== '\u0000' && !isALetter(key)) {
               evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

}

function isALetter(str: string): boolean {
    return !/[^a-z]/i.test(str);
}