"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IcaoSubjectNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var windowRef_service_1 = require("../../common/windowRef.service");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var IcaoSubjectNewComponent = /** @class */ (function () {
    function IcaoSubjectNewComponent(toastr, fb, dataService, router, activatedRoute, winRef, memStorageService, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.memStorageService = memStorageService;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isSubmitting = false;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }
    IcaoSubjectNewComponent.prototype.ngOnInit = function () {
        this.entityCodeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('IcaoSubjectSession.SelectEntityCode')
            },
            width: "100%"
        };
        this.codeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('IcaoSubjectSession.SelectCode')
            },
            width: "100%"
        };
        this.scopeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('IcaoSubjectSession.SelectScope')
            },
            width: "100%"
        };
        this.configureReactiveForm();
        this.createEntityCodesAndScopes();
    };
    IcaoSubjectNewComponent.prototype.onSelectedEntityCodeChanged = function (data) {
        if (data.value !== "-1") {
            this.icaoSubjectForm.get('entityCode').setValue(data.value);
            this.icaoSubjectForm.markAsDirty();
        }
        else {
            this.icaoSubjectForm.get('entityCode').setValue("-1");
        }
    };
    IcaoSubjectNewComponent.prototype.onSelectedScopeChanged = function (data) {
        if (data.value !== "-1") {
            this.icaoSubjectForm.get('scope').setValue(data.value);
            this.icaoSubjectForm.markAsDirty();
        }
        else {
            this.icaoSubjectForm.get('scope').setValue("-1");
        }
    };
    IcaoSubjectNewComponent.prototype.backToIcaoSubjectList = function () {
        this.router.navigate(['/administration/icaosubjects']);
    };
    IcaoSubjectNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var icaoSubject = this.prepareCreateIcaoSubject();
        this.dataService.createIcaoSubject(icaoSubject)
            .subscribe(function (data) {
            var queryOptions = _this.memStorageService.get(_this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
            if (queryOptions) {
                queryOptions.sort = "_Id"; //set order by Id so will be shown & selected after returning to reviewlist 
                queryOptions.page = 1;
                _this.memStorageService.save(_this.memStorageService.ITEM_QUERY_OPTIONS_KEY, queryOptions);
                _this.memStorageService.save(_this.memStorageService.ITEM_ID_KEY, -1);
            }
            var msg = _this.translation.translate('IcaoSubjectSession.SuccessCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.backToIcaoSubjectList();
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('IcaoSubjectSession.FailureCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    IcaoSubjectNewComponent.prototype.validateEntityCode = function () {
        var fc = this.icaoSubjectForm.get("entityCode");
        return fc.value !== "-1" || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectNewComponent.prototype.validateCode23 = function () {
        if (this.icaoSubjectForm.pristine)
            return true;
        var fc = this.icaoSubjectForm.get("code");
        if (fc.errors)
            return false;
        if (!fc.value)
            return false;
        if (!isALetter(fc.value))
            return false;
        if (fc.value.length !== 2)
            return false;
        return true;
    };
    IcaoSubjectNewComponent.prototype.validateScope = function () {
        var fc = this.icaoSubjectForm.get("scope");
        return fc.value !== "-1" || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectNewComponent.prototype.validateName = function () {
        var nameCtrl = this.icaoSubjectForm.get("name");
        return nameCtrl.valid || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectNewComponent.prototype.validateNameFrench = function () {
        var nameFrenchCtrl = this.icaoSubjectForm.get("nameFrench");
        return nameFrenchCtrl.valid || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectNewComponent.prototype.checkboxChanged = function (controlName, checked) {
        var control = this.icaoSubjectForm.get(controlName);
        control.setValue(checked);
        control.markAsDirty();
    };
    IcaoSubjectNewComponent.prototype.createEntityCodesAndScopes = function () {
        this.entityCodes = [
            { id: "-1", text: this.translation.translate('IcaoSubjectSession.SelectEntityCode') },
            { id: "Ahp", text: "Aerodrome" },
            { id: "Fir", text: "FIR" },
            { id: "Obs", text: "Obstacle" },
            { id: "Rwy", text: "Runway" },
        ];
        this.selectedEntityCodeId = this.entityCodes[0].id;
        this.scopes = [
            { id: "-1", text: this.translation.translate('IcaoSubjectSession.SelectEntityCode') },
            { id: 'A', text: 'A' },
            { id: 'E', text: 'E' },
            { id: 'W', text: 'W' },
            { id: 'AE', text: 'AE' },
            { id: 'AW', text: 'AW' },
            { id: 'K', text: 'K' }
        ];
        this.selectedScopeId = this.scopes[0].id;
    };
    IcaoSubjectNewComponent.prototype.configureReactiveForm = function () {
        this.icaoSubjectForm = this.fb.group({
            entityCode: [{ value: "-1", disabled: false }, [forms_1.Validators.required]],
            code: [{ value: "", disabled: false }, [forms_1.Validators.required]],
            scope: [{ value: "-1", disabled: false }, [forms_1.Validators.required]],
            name: [{ value: "", disabled: false }, [forms_1.Validators.required]],
            nameFrench: [{ value: "", disabled: false }, [forms_1.Validators.required]],
            requiresItemA: [{ value: false, disabled: false }],
            requiresSeries: [{ value: false, disabled: false }],
            requiresScope: [{ value: false, disabled: false }],
        });
    };
    IcaoSubjectNewComponent.prototype.prepareCreateIcaoSubject = function () {
        var code23 = this.icaoSubjectForm.get('code').value;
        code23 = code23.toString().toUpperCase();
        var icaoSubject = {
            id: -1,
            entityCode: this.icaoSubjectForm.get('entityCode').value,
            code: code23,
            scope: this.icaoSubjectForm.get('scope').value,
            name: this.icaoSubjectForm.get('name').value,
            nameFrench: this.icaoSubjectForm.get('nameFrench').value,
            requiresItemA: this.icaoSubjectForm.get('requiresItemA').value,
            requiresSeries: this.icaoSubjectForm.get('requiresSeries').value,
            requiresScope: this.icaoSubjectForm.get('requiresScope').value,
        };
        return icaoSubject;
    };
    IcaoSubjectNewComponent.prototype.validateKeyPressLetter = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (key !== '\u0000' && !isALetter(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    IcaoSubjectNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/icaosubjects/icaosubjects-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            mem_storage_service_1.MemoryStorageService,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], IcaoSubjectNewComponent);
    return IcaoSubjectNewComponent;
}());
exports.IcaoSubjectNewComponent = IcaoSubjectNewComponent;
function isALetter(str) {
    return !/[^a-z]/i.test(str);
}
//# sourceMappingURL=icaosubjects-new.component.js.map