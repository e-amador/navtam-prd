"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IcaoSubjectEditComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var IcaoSubjectEditComponent = /** @class */ (function () {
    function IcaoSubjectEditComponent(toastr, fb, dataService, router, activatedRoute, winRef, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.icaoNameLength = 150;
        this.isSubmitting = false;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }
    IcaoSubjectEditComponent.prototype.ngOnInit = function () {
        this.entityCodeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('IcaoSubjectSession.SelectEntityCode')
            },
            width: "100%"
        };
        this.scopeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('IcaoSubjectSession.SelectScope')
            },
            width: "100%"
        };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.isReadOnly = this.action === "view";
        this.configureReactiveForm();
        this.createEntityCodesAndScopes();
    };
    IcaoSubjectEditComponent.prototype.onSelectedEntityCodeChanged = function (data) {
        if (data.value !== "-1") {
            this.icaoSubjectForm.get('entityCode').setValue(data.value);
            this.icaoSubjectForm.markAsDirty();
        }
        else {
            this.icaoSubjectForm.get('entityCode').setValue("-1");
        }
    };
    IcaoSubjectEditComponent.prototype.onSelectedScopeChanged = function (data) {
        if (data.value !== "-1") {
            this.icaoSubjectForm.get('scope').setValue(data.value);
            this.icaoSubjectForm.markAsDirty();
        }
        else {
            this.icaoSubjectForm.get('scope').setValue("-1");
        }
    };
    IcaoSubjectEditComponent.prototype.backToIcaoSubjectList = function () {
        this.router.navigate(['/administration/icaosubjects']);
    };
    IcaoSubjectEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var icaoSubject = this.prepareUpdateIcaoSubject();
        this.dataService.updateIcaoSubject(icaoSubject)
            .subscribe(function (data) {
            var msg = _this.translation.translate('IcaoSubjectSession.SuccessUpdated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.backToIcaoSubjectList();
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('IcaoSubjectSession.FailureUpdated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    IcaoSubjectEditComponent.prototype.validateEntityCode = function () {
        var fc = this.icaoSubjectForm.get("entityCode");
        return fc.value !== -1 || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectEditComponent.prototype.validateCode23 = function () {
        if (this.icaoSubjectForm.pristine)
            return true;
        var fc = this.icaoSubjectForm.get("code");
        if (fc.errors)
            return false;
        if (!fc.value)
            return false;
        if (!isALetter(fc.value))
            return false;
        if (fc.value.length !== 2)
            return false;
        return true;
    };
    IcaoSubjectEditComponent.prototype.validateScope = function () {
        var fc = this.icaoSubjectForm.get("scope");
        return fc.value !== -1 || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectEditComponent.prototype.validateName = function () {
        var nameCtrl = this.icaoSubjectForm.get("name");
        return nameCtrl.valid || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectEditComponent.prototype.validateNameLength = function (ctrlName) {
        var nameCtrl = this.icaoSubjectForm.get(ctrlName);
        var nameValue = nameCtrl.value;
        return nameValue.length > this.icaoNameLength;
    };
    IcaoSubjectEditComponent.prototype.validateNameFrench = function () {
        var nameFrenchCtrl = this.icaoSubjectForm.get("nameFrench");
        return nameFrenchCtrl.valid || this.icaoSubjectForm.pristine;
    };
    IcaoSubjectEditComponent.prototype.checkboxChanged = function (controlName, checked) {
        var control = this.icaoSubjectForm.get(controlName);
        control.setValue(checked);
        control.markAsDirty();
    };
    IcaoSubjectEditComponent.prototype.createEntityCodesAndScopes = function () {
        this.entityCodes = [
            { id: "-1", text: this.translation.translate('IcaoSubjectSession.SelectEntityCode') },
            { id: "Ahp", text: "Aerodrome" },
            { id: "Fir", text: "FIR" },
            { id: "Obs", text: "Obstacle" },
            { id: "Rwy", text: "Runway" },
        ];
        this.selectedEntityCodeId = this.model.entityCode;
        this.scopes = [
            { id: "-1", text: this.translation.translate('IcaoSubjectSession.SelectEntityCode') },
            { id: 'A', text: 'A' },
            { id: 'E', text: 'E' },
            { id: 'W', text: 'W' },
            { id: 'AE', text: 'AE' },
            { id: 'AW', text: 'AW' },
            { id: 'K', text: 'K' }
        ];
        this.selectedScopeId = this.model.scope;
    };
    IcaoSubjectEditComponent.prototype.configureReactiveForm = function () {
        this.icaoSubjectForm = this.fb.group({
            entityCode: [{ value: this.model.entityCode, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            code: [{ value: this.model.code, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            scope: [{ value: this.model.scope, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            name: [{ value: this.model.name, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            nameFrench: [{ value: this.model.nameFrench, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            requiresItemA: [{ value: this.model.requiresItemA, disabled: this.isReadOnly }],
            requiresSeries: [{ value: this.model.requiresSeries, disabled: this.isReadOnly }],
            requiresScope: [{ value: this.model.requiresScope, disabled: this.isReadOnly }],
        });
    };
    IcaoSubjectEditComponent.prototype.prepareUpdateIcaoSubject = function () {
        var code23 = this.icaoSubjectForm.get('code').value;
        code23 = code23.toString().toUpperCase();
        var icaoSubject = {
            id: this.model.id,
            entityCode: this.icaoSubjectForm.get('entityCode').value,
            code: code23,
            scope: this.icaoSubjectForm.get('scope').value,
            name: this.icaoSubjectForm.get('name').value,
            nameFrench: this.icaoSubjectForm.get('nameFrench').value,
            requiresItemA: this.icaoSubjectForm.get('requiresItemA').value,
            requiresSeries: this.icaoSubjectForm.get('requiresSeries').value,
            requiresScope: this.icaoSubjectForm.get('requiresScope').value,
        };
        return icaoSubject;
    };
    IcaoSubjectEditComponent.prototype.validateKeyPressLetter = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (key !== '\u0000' && !isALetter(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    IcaoSubjectEditComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/icaosubjects/icaosubjects-edit.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], IcaoSubjectEditComponent);
    return IcaoSubjectEditComponent;
}());
exports.IcaoSubjectEditComponent = IcaoSubjectEditComponent;
function isALetter(str) {
    return !/[^a-z]/i.test(str);
}
//# sourceMappingURL=icaosubjects-edit.component.js.map