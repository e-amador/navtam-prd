"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var data_model_1 = require("../shared/data.model");
var NofSubscComponent = /** @class */ (function () {
    function NofSubscComponent(toastr, dataService, memStorageService, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterPaging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterTotalPages = 0;
        this.totalPages = 0;
        this.loadingData = false;
        this.selectedAll = false;
        this.tableHeaders = {
            sorting: [
                { columnName: 'Client', direction: -1 },
                { columnName: 'ClientType', direction: 0 },
                { columnName: 'Address', direction: 0 },
                { columnName: 'Series', direction: 0 },
                { columnName: 'ItemsA', direction: 0 },
                { columnName: 'Operator', direction: 0 },
                { columnName: 'French', direction: 0 },
                { columnName: 'AllowQueries', direction: 0 },
                { columnName: 'Updated', direction: 0 },
                { columnName: 'Synced', direction: 0 },
                { columnName: 'Active', direction: 0 }
            ],
            itemsPerPage: [
                { id: '15', text: '15' },
                { id: '30', text: '30' },
                { id: '75', text: '75' },
                { id: '100', text: '100' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName //Default ClientName
        };
    }
    NofSubscComponent.prototype.onKeyup = function ($event) {
        switch ($event.code) {
            case "ArrowUp": //NAVIGATE TABLE ROW UP
                if (this.selectedClient.index > 0) {
                    this.onRowSelected(this.selectedClient.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedClient.index < this.clients.length - 1) {
                    this.onRowSelected(this.selectedClient.index + 1);
                }
                break;
        }
    };
    NofSubscComponent.prototype.ngOnInit = function () {
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.NSD_CLIENTS_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            this.queryOptions = queryOptions;
        }
        this.getNdsClients(this.queryOptions.page);
    };
    NofSubscComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getNdsClients(this.queryOptions.page);
    };
    NofSubscComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    NofSubscComponent.prototype.confirmDelete = function () {
        $('#modal-delete').modal({});
    };
    NofSubscComponent.prototype.deleteClient = function () {
        var _this = this;
        if (this.selectedClient) {
            this.dataService.deleteNdsClient(this.selectedClient.id)
                .subscribe(function (result) {
                if (_this.clients.length === 1) {
                    _this.paging.currentPage = _this.paging.currentPage > 1 ? _this.paging.currentPage - 1 : 1;
                }
                _this.getNdsClients(_this.paging.currentPage);
                var msg = _this.translation.translate('NdsClient.SuccessClientDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('NdsClient.FailureClientDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NofSubscComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getNdsClients(1);
    };
    NofSubscComponent.prototype.onRowSelected = function (index) {
        this.selectNdsClient(this.clients[index]);
    };
    NofSubscComponent.prototype.onPageChange = function (page) {
        this.getNdsClients(page);
    };
    NofSubscComponent.prototype.selectNdsClient = function (subscription) {
        if (this.selectedClient) {
            this.selectedClient.isSelected = false;
        }
        this.selectedClient = subscription;
        this.selectedClient.isSelected = true;
        this.memStorageService.save(this.memStorageService.NSD_CLIENT_ID_KEY, this.selectedClient.id);
    };
    NofSubscComponent.prototype.getNdsClients = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.NSD_CLIENTS_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getNdsClients(this.queryOptions)
                .subscribe(function (clients) {
                _this.processNdsClients(clients);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    NofSubscComponent.prototype.processNdsClients = function (clients) {
        this.paging = clients.paging;
        this.totalPages = clients.paging.totalPages;
        for (var iter = 0; iter < clients.data.length; iter++) {
            clients.data[iter].index = iter;
            clients.data[iter].clientTypeStr = this.toClientTypeStr(clients.data[iter].clientType);
        }
        var lastClientSelected = this.memStorageService.get(this.memStorageService.NSD_CLIENT_ID_KEY);
        if (clients.data.length > 0) {
            this.clients = clients.data;
            var index = 0;
            if (lastClientSelected) {
                index = this.clients.findIndex(function (x) { return x.id === lastClientSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.clients[index].isSelected = true;
            this.selectedClient = this.clients[index];
            this.loadingData = false;
            this.onRowSelected(index);
        }
        else {
            this.clients = [];
            this.selectedClient = null;
            this.loadingData = false;
        }
    };
    NofSubscComponent.prototype.toClientTypeStr = function (clientType) {
        switch (clientType) {
            case data_model_1.ClientType.AFTN: return 'AFTN';
            case data_model_1.ClientType.SWIM: return 'SWIM';
            case data_model_1.ClientType.REST: return 'REST';
            default: return '';
        }
    };
    NofSubscComponent.prototype.selectAllSubscriptions = function () {
        this.selectedAll = !this.selectedAll;
        for (var iter = 0; iter < this.clients.length; iter++) {
            if (this.isSubscriptionEnabled(this.clients[iter])) {
                this.clients[iter].subscriptionSelected = this.selectedAll;
            }
        }
    };
    NofSubscComponent.prototype.selectSubscription = function (i) {
        var cl = this.clients[i];
        if (this.isSubscriptionEnabled(cl)) {
            cl.subscriptionSelected = !cl.subscriptionSelected;
            if (!cl.subscriptionSelected) {
                this.selectedAll = false;
            }
        }
    };
    NofSubscComponent.prototype.confirmSubmit = function () {
        $('#modal-sendnotams').modal({});
    };
    NofSubscComponent.prototype.isAnySubscriptionSelected = function () {
        var _this = this;
        var retV = false;
        if (this.clients) {
            retV = (this.clients.findIndex(function (c) { return _this.isSubscriptionEnabled(c) && c.subscriptionSelected === true; }) > -1) ? true : false;
        }
        return retV;
    };
    NofSubscComponent.prototype.isSubscriptionEnabled = function (client) {
        if (client.clientType === data_model_1.ClientType.AFTN || client.clientType === data_model_1.ClientType.SWIM)
            return true;
        return false;
    };
    NofSubscComponent.prototype.sendNotams = function () {
        var _this = this;
        if (this.isAnySubscriptionSelected()) {
            var clientIds = '';
            this.clients.forEach(function (x) {
                if (x.subscriptionSelected && _this.isSubscriptionEnabled(x)) {
                    clientIds = clientIds + x.id.toString() + '|';
                }
            });
            this.dataService.sendNotamsToSubscriptions(clientIds.trim())
                .subscribe(function (result) {
                var msg = _this.translation.translate('NdsClient.SendNotamsOk');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                _this.getNdsClients(_this.queryOptions.page);
            }, function (error) {
                var msg = _this.translation.translate('NdsClient.SendNotamsError');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    __decorate([
        core_1.ViewChild('notamFilter'),
        __metadata("design:type", Object)
    ], NofSubscComponent.prototype, "filter", void 0);
    __decorate([
        core_1.HostListener('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NofSubscComponent.prototype, "onKeyup", null);
    NofSubscComponent = __decorate([
        core_1.Component({
            selector: 'nof-subscription',
            templateUrl: '/app/dashboard/nof/nof-subsc.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            angular_l10n_1.TranslationService])
    ], NofSubscComponent);
    return NofSubscComponent;
}());
exports.NofSubscComponent = NofSubscComponent;
//# sourceMappingURL=nof-subsc.component.js.map