﻿import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { WindowRef } from '../../common/windowRef.service'
import { DataService } from '../shared/data.service'

import { INdsClient, ClientType, ISelectOptions, IRegionViewModel, RegionSource, IRegionMapRender, IRegionPointModel } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/subscription/subsc-edit.component.html'
})
export class SubscEditComponent implements OnInit, AfterViewInit, OnDestroy  {

    form: FormGroup;

    public clientTypesOptions: Select2Options;
    public selectedClientTypeId: string;
    public clientTypes: Array<Select2OptionData>;

    public seriesOptions: Select2Options;
    public selectedSeries: any;
    public series: Array<Select2OptionData>;
    public itemAOptions: Select2Options;
    public selectedItemsA: any;
    public itemsA: Array<Select2OptionData>;

    public bilingual: boolean = false;

    model: any = null;
    action: string;
    initialized: boolean = false;
    public isReadOnly: boolean;
    public formReady: boolean = false;

    isValidating: boolean = false;
    isValidLocation: boolean = true;
    isClientUniqueName: boolean = true;
    isSubmitting: boolean = false;

    geoSources: ISelectOptions[] = [];
    selectedSourceId?: number = 1;
    geoSourceOptions: Select2Options;
    region: IRegionViewModel;

    radiusLimit: number = 999;
    upperLimit: number = 999;
    lowerLimit: number = 998;

    leafletmap: any = null;
    mapHealthy: boolean = false;
    mapLayer: string = "GeoRegionModified";

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.clientTypesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('NdsClient.SelectClientType')
            },
            width: "100%"
        }
        this.seriesOptions = {
            maximumSelectionLength: 19,
            placeholder: {
                id: '-1',
                text: this.translation.translate('NdsClient.SelectSeries')
            },
            multiple: true,
            width: "100%"
        }
        this.itemAOptions = {
            maximumSelectionLength: 15,
            placeholder: {
                id: '-1',
                text: this.translation.translate('NdsClient.SelectItemA')
            },
            multiple: true,
            width: "100%"
        }

        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];

        this.isReadOnly = this.action === "view";

        //GeoRegion
        if (!this.model.region) {
            this.model.region = {
                fir: "",
                geography: "",
                location: "",
                points: "",
                radius: 5,
                source: RegionSource.None,
            };
        } else if (this.model.region.radius === 0) {
            //Set the standard value for radius, because from the server always is comming as a Geography Region with radius = 0
            this.model.region.radius = 5;
        }

        this.region = this.model.region;

        //Sources
        this.geoSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('NdsClient.SelectSource')
            },
            width: "100%"
        }
        this.loadGeoSources();

        //End GeoRegion

        this.configureReactiveForm();

        this.loadItemsA();
        this.loadClientTypes();
        this.loadSeries();
    }

    ngAfterViewInit() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                const winObj = this.winRef.nativeWindow;
                this.leafletmap = winObj['app'].leafletmap;

                if (this.leafletmap) {
                    this.leafletmap.initMap()
                }
                this.mapHealthy = true;
                if (this.model.isRegionSubscription) {
                    this.newGeoRegionRenderMap();
                    this.refreshMap();
                }
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        });
    }

    ngOnDestroy() {
        this.disposeMap();
    }

    //GeoRegion functions
    loadGeoSources() {
        this.geoSources = [
            { id: '1', text: this.translation.translate('NdsClient.GeoSourceGeography') },
            { id: '3', text: this.translation.translate('NdsClient.GeoSourcePoints') },
            { id: '4', text: this.translation.translate('NdsClient.GeoSourcePointRadius') }
        ];
        this.selectedSourceId = 1;
    }

    onSourceChanged(data: { value: string }) {
        if (data.value) {
            var diss = this.geoSources.find(x => x.id === String(data.value));
            if (diss) {
                this.region.source = +diss.id;
                this.selectedSourceId = +diss.id;
                if (this.region.source === RegionSource.PointAndRadius && this.region.radius === 0) {
                    this.region.radius = 5;
                    const radCtrl = this.form.get("geoRadius");
                    radCtrl.setValue(this.region.radius);
                }
                this.validateGeoRegion(this.region, true);
            }
            else {
                this.region.source = 1
                this.selectedSourceId = 1;
            }
        } else {
            this.region.source = 1;
            this.selectedSourceId = 1;
        }
    }

    public saveRegionFile() {
        const winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            const fileUpload = $("#attachments").get(0);
            const files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);

            this.dataService.saveGeoFile(fileData)
                .finally(() => {
                    $("#attachments").val('');
                    if (!/safari/i.test(navigator.userAgent)) {
                        $("#attachments").type = '';
                        $("#attachments").type = 'file';
                    }
                })
                .subscribe(data => {
                    this.region.geography = data;
                    this.region.source = RegionSource.Geography;
                    const message = this.translation.translate('NdsClient.FileSuccess');
                    this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                    this.isValidLocation = true;
                    this.newGeoRegionRenderMap();
                    this.form.markAsDirty();
                }, error => {
                    this.region.geography = "";
                    //this.region.source = RegionSource.None;
                    this.isValidLocation = false;
                    this.toastr.error(this.translation.translate('NdsClient.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                    this.leafletmap.resetDoaMapView();
                });
        } else {
            this.region.geography = "";
            //this.region.source = RegionSource.None;
            this.toastr.error(this.translation.translate('NdsClient.MessageSaveAttachmentBrowser1'), this.translation.translate('NdsClient.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
            this.leafletmap.resetDoaMapView();
        }
    }

    decValue(fieldName) {
        let value = 0;
        switch (fieldName) {
            case 'geoRadius':
                const radCtrl = this.form.get("geoRadius");
                if (+radCtrl.value > 1) {
                    radCtrl.setValue(+radCtrl.value - 1);
                    this.form.get('geoRadius').updateValueAndValidity();
                    radCtrl.markAsDirty();
                }
                break;
            case 'lowerLimit':
                const lowCtrl = this.form.get('lowerLimit');
                if (+lowCtrl.value > 0) {
                    lowCtrl.setValue(+lowCtrl.value - 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    lowCtrl.markAsDirty();
                }
                break;
            case 'upperLimit':
                const upCtrl = this.form.get('upperLimit');
                if (+upCtrl.value > 1) {
                    upCtrl.setValue(+upCtrl.value - 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    upCtrl.markAsDirty();
                }
                break;
        }
    }

    incValue(fieldName) {
        let value = 0;
        switch (fieldName) {
            case 'geoRadius':
                const radCtrl = this.form.get("geoRadius");
                if (+radCtrl.value < this.radiusLimit) {
                    radCtrl.setValue(+radCtrl.value + 1);
                    this.form.get('geoRadius').updateValueAndValidity();
                    radCtrl.markAsDirty();
                }
                break;
            case 'lowerLimit':
                const lowCtrl = this.form.get('lowerLimit');
                if (+lowCtrl.value < this.upperLimit - 1) {
                    lowCtrl.setValue(+lowCtrl.value + 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    lowCtrl.markAsDirty();
                }
                break;
            case 'upperLimit':
                const upCtrl = this.form.get('upperLimit');
                if (+upCtrl.value < this.upperLimit) {
                    upCtrl.setValue(+upCtrl.value + 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    upCtrl.markAsDirty();
                }
                break;
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    public newGeoRegionRenderMap() {
        let reg: IRegionMapRender = {
            region: this.region,
            changes: []
        };
        this.renderMap(reg);
    }

    public validateGeoRegion(region: IRegionViewModel, paint: boolean) {
        let reg: IRegionMapRender = {
            region: region,
            changes: []
        };
        if (reg.region.source === RegionSource.Geography && reg.region.geography === null) return;
        if (reg.region.source === RegionSource.Points && reg.region.points === null) return;
        if (reg.region.source === RegionSource.PointAndRadius && reg.region.location === null) return;

        this.dataService.getGeoFeatures(reg)
            .subscribe((resp: string) => {
                this.isValidLocation = true;
                if (paint) {
                    this.newGeoRegionRenderMap();
                    this.refreshMap();
                }
            }, error => {
                this.isValidLocation = false;
                if (paint) {
                    this.leafletmap.resetDoaMapView();
                    this.refreshMap();
                }
            });
    }

    public renderMap(reg: IRegionMapRender) {
        this.dataService.getGeoFeatures(reg)
            .subscribe((resp: string) => {
                let geoFeatures = resp;
                this.isValidLocation = true;
                this.renderRegionInMap(geoFeatures, this.mapLayer);
            }, error => {
                this.isValidLocation = false;
                //this.toastr.error(this.translation.translate('NdsClient.RenderMapErrorMsg'), this.translation.translate('NdsClient.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
            });
    }

    private renderRegionInMap(geoFeatures: string, name: string) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        } else {
            //this.toastr.error(this.translation.translate('NdsClient.RenderMapErrorMsg'), this.translation.translate('NdsClient.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    private initMap() {
        const winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;

        if (this.leafletmap) {
            this.leafletmap.initMap()
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.initMap();
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        });
    }

    private disposeMap() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    }

    private refreshMap() {
        setTimeout((function () {
            this.leafletmap.resizeMapForModal();
        }).bind(this), 500);
    }

    //End GeoRegion functions

    onClientTypeChanged(data: { value: string }) {

        this.selectedClientTypeId = data.value;

        if (data.value) {
            this.form.get('clientType').setValue(data.value);
        } else {
            this.form.get('clientTypes').setValue(this.clientTypes[0].id);
        }

        let addressCtrl = this.form.get('address');
        if (data.value !== this.clientTypes[0].id) {
            addressCtrl.clearValidators();
            addressCtrl.setValue("");
            addressCtrl.disable();
        }
        else {
            addressCtrl.setValue("");
            addressCtrl.setValidators([Validators.required, Validators.minLength(8), Validators.maxLength(8), Validators.pattern('^[a-zA-Z]+$')]);
            addressCtrl.enable();
        }

        if (this.formReady) {
            this.form.get('series').updateValueAndValidity();
            this.form.get('itemsA').updateValueAndValidity();
            this.form.markAsDirty();
        }
    }

    onSeriesChanged(data: any) {
        let ctrl = this.form.get('series');
        if (data.value) {
            ctrl.setValue(data.value);
        } else {
            ctrl.setValue("-1");
        }

        if (this.formReady) {
            this.form.get('itemsA').updateValueAndValidity();
            this.form.markAsDirty();
        }
    }

    onItemAChanged(data: { value: string }) {
        let ctrl = this.form.get('itemsA');
        if (data.value) {
            ctrl.setValue(data.value);
        } else {
            ctrl.setValue("-1");
        }

        let allowQueryctrl = this.form.get('allowQueries');
        if (!allowQueryctrl.value) {
            //this.addValidators();
        }

        if (this.formReady) {
            this.form.get('series').updateValueAndValidity();
            this.form.markAsDirty();
        }
    }

    allSeriesChanged(checked: boolean) {

        const seriesCtrl = this.form.get('series');
        const itemsACtrl = this.form.get('itemsA');

        if (checked) {
            this.selectedSeries = null;
            itemsACtrl.clearValidators();
            seriesCtrl.clearValidators();

            itemsACtrl.setErrors(null);
            seriesCtrl.setErrors(null);

            seriesCtrl.setValue(-1);
            itemsACtrl.setValue(-1);
        } else {
            //this.addValidators();
        }

        if (this.formReady) {
            seriesCtrl.updateValueAndValidity();
            itemsACtrl.updateValueAndValidity();
            this.form.markAsDirty();
        }
    }

    bilingualChanged(checked: boolean) {
        const fc = this.form.get('bilingual');
        fc.setValue(checked);
        fc.markAsDirty();
    }

    activeSubscriptionChanged(checked: boolean) {
        const fc = this.form.get('active');
        fc.setValue(checked);
        fc.markAsDirty();
    }

    allowQueriesChanged(checked: boolean) {
        const allowQueriesCtrl = this.form.get('allowQueries');
        const seriesCtrl = this.form.get('series');
        const itemsACtrl = this.form.get('itemsA');

        //allowQueriesCtrl.setValue(checked);
        if (checked) {
            seriesCtrl.clearValidators();
            itemsACtrl.clearValidators();
            seriesCtrl.setErrors(null);
            itemsACtrl.setErrors(null);
        } else {
            //this.addValidators();
        }

        //seriesCtrl.updateValueAndValidity();
        //itemsACtrl.updateValueAndValidity();
        allowQueriesCtrl.markAsDirty()
        //this.form.markAsDirty();
    }

    validateClient() {
        const ctrl = this.form.get("client");
        return ctrl.valid || this.form.pristine;
    }

    validateAddress() {
        if (this.selectedClientTypeId !== "AFTN") {
            return true;
        }

        const ctrl = this.form.get("address");
        return ctrl.valid || this.form.pristine;
    }

    validateClientType() {
        const fc = this.form.get('clientType');
        return fc.value !== "-1" || this.form.pristine;
    }

    //GeoRef validations

    public validateRegionLocation() {
        if (this.isValidating) return;
        const dLocation = this.form.get('geoLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateGeoLocation(dLocation.value)
                    .finally(() => {
                        this.isValidating = false;
                    })
                    .subscribe((resp: string) => {
                        this.isValidLocation = true;
                        this.region.location = resp.toUpperCase();
                        if (this.region.source === RegionSource.PointAndRadius && !this.form.get('geoRadius').errors) {
                            this.region.radius = +this.form.get('geoRadius').value;
                        }
                        this.isValidating = false;
                        this.newGeoRegionRenderMap();
                    }, error => {
                        this.isValidLocation = false;
                        this.region.location = '';
                    });
            } else this.isValidLocation = false;
        } else this.isValidLocation = false;
    }

    public validatePointsLocation() {
        if (this.isValidating) return;
        const dLocation = this.form.get('geoPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateGeoPoints(dLocation.value)
                    .finally(() => {
                        this.isValidating = false;
                    })
                    .subscribe((resp: string) => {
                        this.isValidLocation = true;
                        this.region.points = resp;
                        this.isValidating = false;
                        this.newGeoRegionRenderMap();
                    }, error => {
                        this.isValidLocation = false;
                        this.region.points = '';
                        this.isValidating = false;
                    });
            } else this.isValidLocation = false;
        } else this.isValidLocation = false;
    }

    validateRadius() {
        const fc = this.form.get('geoRadius');
        return !fc.errors || this.form.pristine;
    }

    validateLowerLimit() {
        const fc = this.form.get('lowerLimit');
        return !fc.errors || this.form.pristine;
    }

    validateUpperLimit() {
        const fc = this.form.get('upperLimit');
        return !fc.errors || this.form.pristine;
    }

    isLocationValid(): boolean {
        if (this.form.get('geoRef').value === false) return true;
        else return this.isValidLocation; //this.validGeoData();
    }
    //End GeoRef validations

    backToNdsClientList() {
        this.router.navigate(['/administration/subscription']);
    }

    saveDissabled() {
        return this.form.pristine || !this.form.valid || !this.isLocationValid() || !this.isClientUniqueName || this.isSubmitting;
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        const client = this.prepareNdsClient();
        if (client.isRegionSubscription) {
            this.dataService.validateRegionInCanada(client)
                .subscribe(data => {
                    if (data) {
                        this.dataService.updateNdsClient(client)
                            .subscribe(data => {
                                let msg = this.translation.translate('NdsClient.SuccessClientUpdated');
                                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                                this.backToNdsClientList();
                            }, error => {
                                this.isSubmitting = false;
                                let msg = this.translation.translate('NdsClient.FailureClientUpdated');
                                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                            });
                    } else {
                        //This message is correct in DoasAdminSession. Both share the same message
                        this.isSubmitting = false;
                        let msg = this.translation.translate('DoasAdminSession.RegionOutOfCanada');
                        this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
                    }
                }, error => {
                    //This message is correct in DoasAdminSession. Both share the same message
                    this.isSubmitting = false;
                    let msg = this.translation.translate('DoasAdminSession.ErrorRegionOutOfCanada');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            this.dataService.updateNdsClient(client)
                .subscribe(data => {
                    let msg = this.translation.translate('NdsClient.SuccessClientUpdated');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                    this.backToNdsClientList();
                }, error => {
                    this.isSubmitting = false;
                    let msg = this.translation.translate('NdsClient.FailureClientUpdated');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } 
    }

    private loadItemsA(): void {
        this.dataService.getAerodromes()
            .subscribe((aerodromes: [string, string]) => {
                this.itemsA = aerodromes.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.value,
                            text: r.value
                        };
                    });
                this.selectedItemsA = this.model.itemsA;

                let self = this;
                window.setTimeout(function () {
                    self.formReady = true;
                }, 500);
            });
    }

    private loadClientTypes(): void {
        this.clientTypes = [
            { id: "AFTN", text: "AFTN" },
            { id: "SWIM", text: "SWIM" },
            //{ id: "REST", text: "REST" }
        ];

        switch (this.model.clientType) {
            case ClientType.AFTN:
                this.selectedClientTypeId = this.clientTypes[0].id;
                const addressCtrl = this.form.get('address');
                addressCtrl.setValidators([Validators.required, Validators.minLength(8), Validators.maxLength(8), Validators.pattern('^[a-zA-Z]+$')]);
                break;
            case ClientType.SWIM:
                this.selectedClientTypeId = this.clientTypes[1].id;
                break;
            //case ClientType.REST: this.selectedClientTypeId = this.clientTypes[2].id; break;
            default: this.selectedClientTypeId = this.clientTypes[0].id;
        }
    }

    private loadSeries(): void {
        this.series = [
            { id: "A", text: "A" },
            { id: "B", text: "B" },
            { id: "C", text: "C" },
            { id: "D", text: "D" },
            { id: "E", text: "E" },
            { id: "F", text: "F" },
            { id: "G", text: "G" },
            { id: "H", text: "H" },
            { id: "I", text: "I" },
            { id: "J", text: "J" },
            { id: "K", text: "K" },
            { id: "L", text: "L" },
            { id: "M", text: "M" },
            { id: "N", text: "N" },
            { id: "O", text: "O" },
            { id: "P", text: "P" },
            { id: "Q", text: "Q" },
            { id: "R", text: "R" },
            { id: "S", text: "S" },
            { id: "T", text: "T" },
            { id: "U", text: "U" },
            { id: "V", text: "V" },
            { id: "W", text: "W" },
            { id: "X", text: "X" },
            { id: "Y", text: "Y" },
            { id: "Z", text: "Z" }];

        if (this.containsAllSeries(this.model.series)) {
            const allSeriesCtrl = this.form.get('allSeries');
            allSeriesCtrl.setValue(true);

            this.selectedSeries = "-1";

            const seriesCtrl = this.form.get('series');
            seriesCtrl.setValue("-1");
            //seriesCtrl.clearValidators();

        } else {
            this.selectedSeries = this.model.series;
        }
    }

    private configureReactiveForm() {

        this.form = this.fb.group({
            client: [{ value: this.model.clientName, disabled: this.isReadOnly }, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            address: [{ value: this.model.address, disabled: (this.model.clientType !== ClientType.AFTN || this.isReadOnly)  }],
            clientType: [{ value: this.model.clientType, disabled: true }],
            series: [{ value: this.model.series, disabled: this.isReadOnly }],
            itemsA: [{ value: this.model.itemsA, disabled: this.isReadOnly }],
            allSeries: [{ value: false, disabled: this.isReadOnly }],
            bilingual: [{ value: this.model.french, disabled: this.isReadOnly }],
            allowQueries: [{ value: this.model.allowQueries, disabled: this.isReadOnly }],
            operator: [{ value: this.model.operator, disabled: true }],
            active: [{ value: this.model.active, disabled: this.isReadOnly }],

            geoRef: [{ value: this.model.isRegionSubscription, disabled: this.isReadOnly }],
            geoLocation: [{ value: (this.region ? this.region.location : ""), disabled: this.isReadOnly }, []],
            geoRadius: [{ value: (this.region ? this.region.radius : 5), disabled: this.isReadOnly }, []],
            geoPoints: [{ value: (this.region ? this.region.points : ""), disabled: this.isReadOnly }, []],
            lowerLimit: [{ value: this.model.lowerLimit, disabled: this.isReadOnly }, []],
            upperLimit: [{ value: (this.model.upperLimit > 0 ? this.model.upperLimit : this.upperLimit), disabled: this.isReadOnly }, []],

        });

        this.form.get("client").valueChanges.debounceTime(1000).subscribe(() => this.validateClientNameUnique());

        this.form.get("geoRadius").setValidators(radiusValidatorFn(this.radiusLimit));

        this.form.get("geoLocation").valueChanges.debounceTime(1000).subscribe(() => this.validateRegionLocation());
        this.form.get("geoPoints").valueChanges.debounceTime(1000).subscribe(() => this.validatePointsLocation());
        this.form.get("geoRadius").valueChanges.debounceTime(1000).subscribe(() => this.validateRegionLocation());

        this.form.get("lowerLimit").setValidators(lowerLimitValidator(this.form.get('upperLimit'), this.lowerLimit));
        this.form.get("upperLimit").setValidators(upperLimitValidator(this.form.get('lowerLimit'), this.upperLimit));
    }

    geoRefChanged(value: boolean) {
        if (value) {
            this.form.get("geoRadius").setValidators(radiusValidatorFn(this.radiusLimit));
            this.form.get("lowerLimit").setValidators(lowerLimitValidator(this.form.get('upperLimit'), this.lowerLimit));
            this.form.get("upperLimit").setValidators(upperLimitValidator(this.form.get('lowerLimit'), this.upperLimit));
            this.validateGeoRegion(this.model.region, true);
        } else {
            this.form.get("geoRadius").clearValidators();
            this.form.get("lowerLimit").clearValidators();
            this.form.get("upperLimit").clearValidators();
        }
    }

    private prepareNdsClient(): INdsClient {
        let seriesValue = this.form.get('series').value;
        let itemsAValue = this.form.get('itemsA').value;

        if (typeof (seriesValue) === "object") {
            if (seriesValue.length === 1 && seriesValue[0] == "-1") {
                seriesValue = null;
            }
        } else {
            if (seriesValue == "-1") {
                seriesValue = null;
            }
        }

        if (typeof (itemsAValue) === "object") {
            if (itemsAValue.length === 1 && itemsAValue[0] == "-1") {
                itemsAValue = null;
            }
        } else {
            if (itemsAValue == "-1") {
                itemsAValue = null;
            }
        }

        const client: INdsClient = {
            id: this.model.id,
            client: this.form.get('client').value,
            clientType: this.form.get('clientType').value,
            address: this.form.get('address').value,
            series: this.form.get('allSeries').value ? this.getAllSeries() : seriesValue,
            itemsA: itemsAValue,
            allowQueries: this.form.get('allowQueries').value,
            french: this.form.get('bilingual').value,
            active: this.form.get('active').value,
            isRegionSubscription: this.form.get('geoRef').value,
            region: null,
            lowerLimit: 0,
            upperLimit: 0,
        }

        if (client.isRegionSubscription) {
            //Clean the extra info on the data
            var geoRegion: IRegionViewModel = { fir: '', geography: '', location: '', points: '', radius: 0, source: this.region.source };
            if (this.region.source === RegionSource.Geography) {
                geoRegion.geography = this.region.geography;
            } else if (this.region.source === RegionSource.FIR) {
                geoRegion.fir = this.region.fir;
            } else if (this.region.source === RegionSource.Points) {
                geoRegion.points = this.region.points;
            } else if (this.region.source === RegionSource.PointAndRadius) {
                geoRegion.location = this.region.location.toUpperCase();
                geoRegion.radius = this.region.radius;
            }
            this.region = geoRegion;
            client.region = this.region;
            client.lowerLimit = +this.form.get('lowerLimit').value;
            client.upperLimit = +this.form.get('upperLimit').value;
        }

        if (client.address) {
            client.address = client.address.toUpperCase();
        }

        return client;
    }

    public validateClientNameUnique() {
        if (this.isReadOnly) return true;
        if (this.isValidating) return;
        if (this.validateClient()) {
            const dName = this.form.get('client');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsClientName(this.model.id, dName.value)
                        .subscribe((resp: boolean) => {
                            this.isClientUniqueName = !resp;
                            this.isValidating = false;
                        });
                }
            }
        }
    }

    private getAllSeries(): string[] {
        return this.series.map(
            function (series: any) {
                return series.id
            });
    }

    private containsAllSeries(series): boolean {
        if (series === null) {
            return false;
        }

        if (this.series.length !== series.length) {
            return false;
        }

        let equal = true;
        let i = 0;

        while (equal && i < this.series.length) {
            if (this.series[i].id !== series[i]) {
                equal = false;
            }
            i++;
        }
        return equal;
    }

}

function dropDownEntryRequiredValidator(allowQueryCtrl: AbstractControl, seriesCtrl: AbstractControl, itemACtrl: AbstractControl, allSeriesCtrl: AbstractControl): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {

        if (!allowQueryCtrl.value) {

            if (allSeriesCtrl.value) {
                return null;
            }

            let serieCtrlValue = seriesCtrl.value === null ? "-1" : seriesCtrl.value;
            let itemACtrlValue = itemACtrl.value === null ? "-1" : itemACtrl.value;

            serieCtrlValue = typeof (serieCtrlValue) === "object" ? serieCtrlValue[0] : serieCtrlValue;
            itemACtrlValue = typeof (itemACtrlValue) === "object" ? itemACtrlValue[0] : itemACtrlValue;

            if ((!serieCtrlValue || serieCtrlValue == "-1") && (!itemACtrlValue || itemACtrlValue == "-1")) {
                return { 'required': true }
            }
        }

        return null;
    };
}

function radiusValidatorFn(maxRadius: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function lowerLimitValidator(upperLimitCtrl: AbstractControl, lowerMaxValue: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

        if (!isANumber(control.value)) {
            return { 'lowerLimitNonNumeric': true };
        }
        var v1: number = +control.value;

        if (v1 < 0) {
            return { 'lowerLimitLowerThanZero': true };
        }
        if (v1 > lowerMaxValue) {
            return { 'lowerLimitHigherThanMax': true };
        }
        if (!isANumber(upperLimitCtrl.value)) {
            return { 'upperLimitNonNumeric': true };
        }
        var v2: number = +upperLimitCtrl.value;
        if (v1 === 0 && v2 === 0) return null;
        return v1 >= v2 ? { "invalidLowerLimitRange": true } : null;
    };
}

function upperLimitValidator(lowerLimitCtrl: AbstractControl, upperMaxValue: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (!isANumber(control.value)) {
            return { 'upperLimitNonNumeric': true };
        }
        var v1: number = +control.value;
        if (v1 < 0) {
            return { 'upperLimitLowerThanZero': true };
        }
        if (v1 > upperMaxValue) {
            return { 'upperLimitHigherThanMax': true };
        }
        if (!isANumber(lowerLimitCtrl.value)) {
            return { 'lowerLimitNonNumeric': true };
        }
        var v2: number = +lowerLimitCtrl.value;
        return v1 <= v2 ? { "invalidUpperLimitRange": true } : null;
    };
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}
