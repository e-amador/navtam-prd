﻿import { Component, OnInit, Inject, HostListener, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { TranslationService } from 'angular-l10n';
import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'
import { Router } from '@angular/router'

import {
    INdsClientPartial,
    ClientType,
    IQueryOptions,
    IPaging,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

@Component({
    selector: 'subscription',
    templateUrl: '/app/admin/subscription/subsc.component.html'
})
export class SubscComponent implements OnInit {
    @ViewChild('notamFilter') filter;

    clients: INdsClientPartial[];
    queryOptions: IQueryOptions;
    selectedClient: INdsClientPartial;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterPaging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterTotalPages: number = 0;
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;

    frsPrevKeyCodeEvent: any;
    sndPrevKeyCodeEvent: any;
    selectedAll: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private winRef: WindowRef,
        private router: Router,
        private memStorageService: MemoryStorageService,
        private translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);

        this.tableHeaders = {
            sorting:
            [
                { columnName: 'Client', direction: -1 },
                { columnName: 'ClientType', direction: 0 },
                { columnName: 'Address', direction: 0 },
                { columnName: 'Series', direction: 0 },
                { columnName: 'ItemsA', direction: 0 },
                { columnName: 'Operator', direction: 0 },
                { columnName: 'French', direction: 0 },
                { columnName: 'AllowQueries', direction: 0 },
                { columnName: 'Updated', direction: 0 },
                { columnName: 'LastSynced', direction: 0 },
                { columnName: 'Active', direction: 0 }
            ],
            itemsPerPage:
            [
                { id: '15', text: '15' },
                { id: '30', text: '30' },
                { id: '75', text: '75' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName //Default ClientName
        }
    }

    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        switch ($event.code) {
            case "ArrowUp"://NAVIGATE TABLE ROW UP
                if (this.selectedClient.index > 0) {
                    this.onRowSelected(this.selectedClient.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedClient.index < this.clients.length - 1) {
                    this.onRowSelected(this.selectedClient.index + 1);
                }
                break;
        }
    }

    ngOnInit() {
        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.NSD_CLIENTS_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            this.queryOptions = queryOptions;
        }
        this.getNdsClients(this.queryOptions.page);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getNdsClients(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    confirmDelete() {
        $('#modal-delete').modal({});
    }

    deleteClient() {
        if (this.selectedClient) {
            this.dataService.deleteNdsClient(this.selectedClient.id)
                .subscribe(result => {
                    if (this.clients.length === 1 ) {
                        this.paging.currentPage = this.paging.currentPage > 1 ? this.paging.currentPage - 1 : 1 
                    }

                    this.getNdsClients(this.paging.currentPage);
                    let msg = this.translation.translate('NdsClient.SuccessClientDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('NdsClient.FailureClientDeleted');
                    this.toastr.error(msg + "&nbsp;" + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getNdsClients(1);
    }

    onRowSelected(index: number) {
        this.selectNdsClient(this.clients[index]);
    }

    onPageChange(page: number) {
        this.getNdsClients(page);
    }

    private selectNdsClient(subscription) {
        if (this.selectedClient) {
            this.selectedClient.isSelected = false;
        }
        this.selectedClient = subscription
        this.selectedClient.isSelected = true;
        this.memStorageService.save(this.memStorageService.NSD_CLIENT_ID_KEY, this.selectedClient.id);
    }

    private getNdsClients(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.NSD_CLIENTS_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getNdsClients(this.queryOptions)
                .subscribe((clients: any) => {
                    this.processNdsClients(clients);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processNdsClients(clients: any) {
        this.paging = clients.paging;
        this.totalPages = clients.paging.totalPages;

        for (var iter = 0; iter < clients.data.length; iter++) {
            clients.data[iter].index = iter;
            clients.data[iter].clientTypeStr = this.toClientTypeStr(clients.data[iter].clientType);
        }

        const lastClientSelected = this.memStorageService.get(this.memStorageService.NSD_CLIENT_ID_KEY);
        if (clients.data.length > 0) {
            this.clients = clients.data;
            let index = 0;
            if (lastClientSelected) {
                index = this.clients.findIndex(x => x.id === lastClientSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.clients[index].isSelected = true;
            this.selectedClient = this.clients[index];
            this.loadingData = false;
            this.onRowSelected(index);
        } else {
            this.clients = [];
            this.selectedClient = null;
            this.loadingData = false;
        }
    }

    toClientTypeStr(clientType) {
        switch (clientType) {
            case ClientType.AFTN: return 'AFTN';
            case ClientType.SWIM: return 'SWIM';
            case ClientType.REST: return 'REST';
            default: return '';
        }
    }

    selectAllSubscriptions(): void {
        this.selectedAll = !this.selectedAll;
        for (var iter = 0; iter < this.clients.length; iter++) {
            if (this.isSubscriptionEnabled(this.clients[iter])) {
                this.clients[iter].subscriptionSelected = this.selectedAll;
            }
        }
    }

    selectSubscription(i): void {
        var cl = this.clients[i];
        if (this.isSubscriptionEnabled(cl)) {
            cl.subscriptionSelected = !cl.subscriptionSelected;
            if (!cl.subscriptionSelected) {
                this.selectedAll = false;
            }
        }
    }

    confirmSubmit(): void {
        $('#modal-sendnotams').modal({});
    }

    isAnySubscriptionSelected(): boolean {
        var retV = false;
        if (this.clients) {
            retV = (this.clients.findIndex(c => this.isSubscriptionEnabled(c) && c.subscriptionSelected === true) > -1) ? true : false;
        }
        return retV;
    }

    isSubscriptionEnabled(client: INdsClientPartial): boolean {
        if (client.clientType === ClientType.AFTN || client.clientType === ClientType.SWIM) return true;
        return false;
    }

    sendNotams(): void {
        if (this.isAnySubscriptionSelected()) {
            var clientIds = '';
            this.clients.forEach(x => {
                if (x.subscriptionSelected && this.isSubscriptionEnabled(x)) { 
                    clientIds = clientIds + x.id.toString() + '|';
                }
            });
            this.dataService.sendNotamsToSubscriptions(clientIds.trim())
                .subscribe(result => {
                    let msg = this.translation.translate('NdsClient.SendNotamsOk');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                    this.getNdsClients(this.queryOptions.page);
                }, error => {
                    let msg = this.translation.translate('NdsClient.SendNotamsError');
                    this.toastr.error(msg + "&nbsp;" + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }
}

