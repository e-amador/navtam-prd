"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var AdminComponent = /** @class */ (function (_super) {
    __extends(AdminComponent, _super);
    function AdminComponent(locale, translation) {
        var _this = _super.call(this, locale, translation) || this;
        _this.locale = locale;
        _this.translation = translation;
        _this.locale.setDefaultLocale(window['app'].cultureInfo, "CA");
        _this.locale.defaultLocaleChanged.subscribe(function (item) { _this.onLanguageCodeChangedDataRecieved(item); });
        return _this;
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent.prototype.ChangeCulture = function (language, country, currency) {
        this.locale.setDefaultLocale(language, country);
        this.locale.setCurrentCurrency(currency);
    };
    AdminComponent.prototype.ChangeCurrency = function (currency) {
        this.locale.setCurrentCurrency(currency);
    };
    AdminComponent.prototype.onLanguageCodeChangedDataRecieved = function (item) {
        console.log('onLanguageCodeChangedDataRecieved App');
        console.log(item);
    };
    AdminComponent.prototype.ngAfterViewInit = function () {
        setTimeout(function () {
            $('.loader-overlay').addClass('loaded');
            $('body > section').animate({
                opacity: 1,
            }, 200);
        }, 400);
    };
    AdminComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            //template: `<router-outlet></router-outlet>`
            templateUrl: '/app/admin/admin.component.html'
        }),
        __metadata("design:paramtypes", [angular_l10n_1.LocaleService, angular_l10n_1.TranslationService])
    ], AdminComponent);
    return AdminComponent;
}(angular_l10n_1.Localization));
exports.AdminComponent = AdminComponent;
//# sourceMappingURL=admin.component.js.map