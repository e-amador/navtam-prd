﻿import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { DataService } from './../shared/data.service'

@Injectable()
export class ConfigModelResolver implements Resolve<any> {

    constructor(private dataService: DataService) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        const cat = route.params['category'];
        const name = route.params['name'];
        return this.dataService.getConfigRecAdmin(cat, name).map(model => model);
    }
}

