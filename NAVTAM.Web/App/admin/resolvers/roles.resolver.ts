﻿import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { IRole } from './../shared/data.model';
import { DataService } from './../shared/data.service'

@Injectable()
export class RolesResolver implements Resolve<IRole[]> {

    constructor(private dataService: DataService) {
    }
    resolve() {
        return this.dataService.getRoles().map(roles => roles);
    }
}