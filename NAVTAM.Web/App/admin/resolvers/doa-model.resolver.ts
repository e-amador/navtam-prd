﻿import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { DataService } from './../shared/data.service'

@Injectable()
export class DoaModelResolver implements Resolve<any> {

    constructor(private dataService: DataService) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        const id = route.params['id'];
        return this.dataService.getDoaAdmin(id).map(model => model);
    }
}

