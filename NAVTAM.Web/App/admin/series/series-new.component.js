"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeriesNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var SeriesNewComponent = /** @class */ (function () {
    function SeriesNewComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.subjects = [];
        this.geoRegions = [];
        this.disseminationCategories = [];
        this.selectedDisseminationCategory = '-1';
        this.codes = [];
        this.selectedCodeId = '-1';
        this.isReadOnly = false;
        this.readOnlyQCode = false;
        this.isSubmitting = false;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }
    SeriesNewComponent.prototype.ngOnInit = function () {
        this.model = this.prepareEmptySerie();
        this.loadSubjects();
        this.loadRegions();
        this.loadDissCategory();
        this.loadQCodes();
        this.subjectOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectRegion')
            },
            width: "100%"
        };
        this.geoRegionOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectRegion')
            },
            width: "100%"
        };
        this.disseminationCategoriesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectDissCat')
            },
            width: "100%"
        };
        this.codeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('SeriesAllocationSession.SelectQCode')
            },
            width: "100%"
        };
        this.configureReactiveForm();
    };
    SeriesNewComponent.prototype.backToSeriesList = function () {
        this.router.navigate(['/administration/series']);
    };
    SeriesNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var serieToSave = this.prepareSaveSerie();
        this.dataService.saveSeries(serieToSave)
            .subscribe(function (data) {
            var msg = _this.translation.translate('SeriesAllocationSession.SuccessSerieCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/series"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('SeriesAllocationSession.FailureSerieCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    SeriesNewComponent.prototype.configureReactiveForm = function () {
        this.seriesForm = this.fb.group({
            subject: [{ value: this.model.subject, disabled: false }, [validateSubject]],
            regionId: [{ value: this.model.regionId, disabled: false }, [validateRegionId]],
            series: [{ value: this.model.series, disabled: false }, [validateSeries]],
            disseminationCategory: [{ value: this.model.disseminationCategory, disabled: false }, [validateDisseminationCategory]],
            qCode: [{ value: this.model.qCode, disabled: this.readOnlyQCode }, [validateQCode]],
            disseminationCategoryName: [{ value: this.model.disseminationCategoryName, disabled: false }, [forms_1.Validators.required]],
            regionName: [{ value: this.model.regionName, disabled: false }, [forms_1.Validators.required]],
        });
    };
    SeriesNewComponent.prototype.prepareSaveSerie = function () {
        var serie = {
            subject: this.seriesForm.get('subject').value.toString(),
            regionName: this.seriesForm.get('regionName').value.toUpperCase(),
            regionId: this.seriesForm.get('regionId').value.toUpperCase(),
            series: this.seriesForm.get('series').value.toUpperCase(),
            disseminationCategory: this.seriesForm.get('disseminationCategory').value.toUpperCase(),
            disseminationCategoryName: this.seriesForm.get('disseminationCategoryName').value.toUpperCase(),
            qCode: this.seriesForm.get('qCode').value.toUpperCase(),
        };
        return serie;
    };
    SeriesNewComponent.prototype.prepareEmptySerie = function () {
        var serie = {
            id: null,
            subject: '0',
            regionName: '',
            regionId: '-1',
            series: '',
            disseminationCategory: '-1',
            disseminationCategoryName: '',
            qCode: '',
        };
        return serie;
    };
    SeriesNewComponent.prototype.validateSeriesName = function () {
        var fc = this.seriesForm.get("series");
        if (validateSeries(fc) !== null)
            return false;
        return true;
    };
    SeriesNewComponent.prototype.validateSubject = function () {
        var fc = this.seriesForm.get('subject');
        if (validateSubject(fc) !== null)
            return false;
        return true;
    };
    SeriesNewComponent.prototype.validateRegion = function () {
        var fc = this.seriesForm.get('regionId');
        if (validateRegionId(fc) !== null)
            return false;
        return true;
    };
    SeriesNewComponent.prototype.validateQCode = function () {
        var fc = this.seriesForm.get('qCode');
        if (validateQCode(fc) !== null)
            return false;
        return true;
    };
    SeriesNewComponent.prototype.validateDissemination = function () {
        var fc = this.seriesForm.get('disseminationCategory');
        if (validateDisseminationCategory(fc) !== null)
            return false;
        return true;
    };
    SeriesNewComponent.prototype.onDisseminationChanged = function (data) {
        if (data.value) {
            var diss = this.disseminationCategories.find(function (x) { return x.id === data.value; });
            if (diss) {
                this.seriesForm.get('disseminationCategory').setValue(diss.id);
                this.seriesForm.get('disseminationCategoryName').setValue(diss.text);
            }
            else {
                this.seriesForm.get('disseminationCategory').setValue('-1');
                this.seriesForm.get('disseminationCategoryName').setValue('');
            }
        }
        else {
            this.seriesForm.get('disseminationCategory').setValue('-1');
            this.seriesForm.get('disseminationCategoryName').setValue('');
        }
    };
    SeriesNewComponent.prototype.onSubjectChanged = function (data) {
        if (data.value) {
            this.seriesForm.get('subject').setValue(data.value);
            if (data.value === '2') {
                var code = this.codes.find(function (c) { return c.text === 'OB'; });
                if (code) {
                    this.onSelectedCodeChanged({ value: code.id });
                    this.readOnlyQCode = true;
                }
            }
            else {
                this.readOnlyQCode = false;
            }
        }
    };
    SeriesNewComponent.prototype.onRegionChanged = function (data) {
        if (data.value) {
            var reg = this.geoRegions.find(function (x) { return x.id === data.value; });
            if (reg) {
                this.seriesForm.get('regionId').setValue(reg.id);
                this.seriesForm.get('regionName').setValue(reg.text);
            }
            else {
                this.seriesForm.get('regionId').setValue('-1');
                this.seriesForm.get('regionName').setValue('');
            }
        }
        else {
            this.seriesForm.get('regionId').setValue('-1');
            this.seriesForm.get('regionName').setValue('');
        }
    };
    SeriesNewComponent.prototype.onSelectedCodeChanged = function (data) {
        if (data.value !== "-1") {
            this.selectedCodeId = data.value;
            this.seriesForm.get('qCode').setValue(data.value);
            this.seriesForm.markAsDirty();
        }
        else {
            this.seriesForm.get('qCode').setValue("-1");
        }
    };
    SeriesNewComponent.prototype.loadSubjects = function () {
        this.subjects = [
            {
                id: "0",
                text: "Ahp"
            },
            {
                id: "1",
                text: "FIR"
            },
            {
                id: "2",
                text: "FIR (MOB)"
            }
        ];
    };
    SeriesNewComponent.prototype.loadRegions = function () {
        var _this = this;
        this.dataService.getAllGeoRegions()
            .subscribe(function (geoR) {
            _this.geoRegions = geoR.map(function (r) {
                return {
                    id: r.id.toString(),
                    text: r.name
                };
            });
            _this.geoRegions.unshift({ id: "-1", text: "" });
            _this.selectedRegionId = _this.model.regionId || '-1';
        });
    };
    SeriesNewComponent.prototype.loadDissCategory = function () {
        this.disseminationCategories = [
            { id: '-1', text: '' },
            { id: '0', text: 'National' },
            { id: '1', text: 'US' },
            { id: '2', text: 'International' },
        ];
        this.selectedDisseminationCategory = this.model.disseminationCategory || '-1';
    };
    SeriesNewComponent.prototype.loadQCodes = function () {
        var _this = this;
        this.dataService.getAllIcaoSubjectCodes()
            .subscribe(function (codes) {
            _this.codes = codes.map(function (r) {
                return {
                    id: r,
                    text: r
                };
            });
            _this.codes.unshift({
                id: "-1",
                text: _this.translation.translate('SeriesAllocationSession.SelectQCode'),
            });
            _this.selectedCodeId = _this.codes[0].id;
        });
    };
    SeriesNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/series/series-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], SeriesNewComponent);
    return SeriesNewComponent;
}());
exports.SeriesNewComponent = SeriesNewComponent;
function validateSubject(c) {
    if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
function validateRegionId(c) {
    if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
function validateSeries(c) {
    if (c.value.length !== 1) {
        return { 'required': true };
    }
    if (!hasUpperCase(c.value)) {
        return { 'required': true };
    }
    return null;
}
function validateDisseminationCategory(c) {
    if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
function validateQCode(c) {
    if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
function hasUpperCase(str) {
    return (/[A-Za-z]/.test(str));
}
//# sourceMappingURL=series-new.component.js.map