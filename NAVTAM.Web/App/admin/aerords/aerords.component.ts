﻿import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { WindowRef } from '../../common/windowRef.service';

import { LocaleService, TranslationService } from 'angular-l10n';

import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'

declare var $: any;

@Component({
    templateUrl: '/app/admin/aerords/aerords.component.html'
})
export class AerordsComponent implements OnInit {

    status: IAeroRDSCacheStatus = {
        airacActiveDate: "?",
        airacEffectiveDate: "?",
        lastActiveDate: "?",
        lastActiveUpdated: "?",
        lastEffectiveDate: "?",
        lastEffectiveUpdated: "?"
    };

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.refreshCacheStatus();
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    refreshCacheStatus(event: any = null) {
        if (event)
            event.preventDefault();

        this.dataService.getAeroRDSCacheStatus()
            .subscribe(data => {
                this.status = data;
            }, error => {
                let msg = this.translation.translate('AerordsSession.FailureGlobal');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    get activeDateClass() {
        return this.status && this.status.airacActiveDate !== this.status.lastActiveDate ? "bg-sdocache-outdated" : "bg-sdocache-latest";
    }

    get effectiveDateClass() {
        return this.status && this.status.airacEffectiveDate === this.status.lastEffectiveDate ? "bg-sdocache-latest" : "";
    }


    globalSync() {
        $('#modal-global').modal({});
    }

    effectiveCacheSync() {
        $('#modal-effective-sync').modal({});
    }

    effectiveCacheLoad() {
        $('#modal-effective-load').modal({});
    }

    runGlobalSync() {
        this.dataService.pushAeroRDSGlobalSync()
            .subscribe(data => {
                let msg = this.translation.translate('AerordsSession.SuccessGlobal');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, error => {
                let msg = this.translation.translate('AerordsSession.FailureGlobal');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    runEffectiveCacheSync() {
        this.dataService.pushAeroRDSEffectiveCacheSync()
            .subscribe(data => {
                let msg = this.translation.translate('AerordsSession.SuccessPartial');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, error => {
                let msg = this.translation.translate('AerordsSession.FailurePartial');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    runEffectiveCacheLoad() {
        this.dataService.pushAeroRDSEffectiveCacheLoad()
            .subscribe(data => {
                let msg = this.translation.translate('AerordsSession.SuccessPartial');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, error => {
                let msg = this.translation.translate('AerordsSession.FailurePartial');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }
}

export interface IAeroRDSCacheStatus {
    id?: string,
    airacActiveDate?: any,
    airacEffectiveDate?: any,
    lastEffectiveDate?: any,
    lastActiveDate?: any,
    lastEffectiveUpdated?: any,
    lastActiveUpdated?: any
}