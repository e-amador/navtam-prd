"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DoaNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var DoaNewComponent = /** @class */ (function () {
    function DoaNewComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isReadOnly = false;
        this.isDoaUniqueName = true;
        this.isValidating = false;
        this.isValidLocation = true;
        this.isSubmitting = false;
        this.doaSources = [];
        this.selectedSourceId = 1;
        this.firSources = [];
        this.selectedFirId = '-1';
        this.radiusLimit = 999;
        this.mapHealthy = false;
        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    DoaNewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'create') {
            var region = {
                fir: "",
                geography: "",
                location: "",
                points: "",
                radius: 5,
                source: data_model_1.DoaRegionSource.None,
            };
            this.doa = {
                id: 0,
                description: "",
                name: "",
                region: region,
                doaType: data_model_1.RegionType.Doa,
                changes: null
            };
        }
        else if (this.action === 'copy') {
            this.doa = this.activatedRoute.snapshot.data['model'];
        }
        this.model = this.doa;
        //Sources
        this.doaSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectSource')
            },
            width: "100%"
        };
        this.loadDoaSources();
        if (this.action === 'copy') {
            this.selectedSourceId = +this.doaSources.find(function (x) { return x.id === String(_this.doa.region.source); }).id;
        }
        //FIRS
        this.firSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectFir')
            },
            width: "100%"
        };
        this.loadFirSources();
        this.configureReactiveForm();
    };
    DoaNewComponent.prototype.ngAfterViewInit = function () {
        this.mapHealthCheck();
    };
    DoaNewComponent.prototype.ngOnDestroy = function () {
        this.disposeMap();
    };
    DoaNewComponent.prototype.loadFirSources = function () {
        var _this = this;
        this.dataService.getAllFirs()
            .finally(function () {
            _this.selectedFirId = '-1';
        })
            .subscribe(function (firs) {
            _this.firSources = firs.map(function (r) {
                return {
                    id: r.id,
                    text: r.designator + " - " + r.name
                };
            });
            _this.firSources.unshift({ id: "-1", text: "" });
        });
    };
    DoaNewComponent.prototype.loadDoaSources = function () {
        this.doaSources = [
            //{ id: '0', text: this.translation.translate('DoasAdminSession.DoaSourceNone') },
            { id: '1', text: this.translation.translate('DoasAdminSession.DoaSourceGeography') },
            { id: '2', text: this.translation.translate('DoasAdminSession.DoaSourceFir') },
            { id: '3', text: this.translation.translate('DoasAdminSession.DoaSourcePoints') },
            { id: '4', text: this.translation.translate('DoasAdminSession.DoaSourcePointRadius') }
        ];
        this.selectedSourceId = 1;
    };
    DoaNewComponent.prototype.onFirChanged = function (data) {
        if (data.value) {
            this.doa.region.fir = data.value;
            this.newDoaRegionRenderMap();
        }
        else {
            this.doa.region.fir = '';
        }
    };
    DoaNewComponent.prototype.onSourceChanged = function (data) {
        if (data.value) {
            var diss = this.doaSources.find(function (x) { return x.id === String(data.value); });
            if (diss) {
                this.doa.region.source = +diss.id;
                this.selectedSourceId = +diss.id;
                if (this.doa.region.source === data_model_1.DoaRegionSource.PointAndRadius && this.doa.region.radius === 0) {
                    this.doa.region.radius = 5;
                    var radCtrl = this.$ctrl("doaRadius");
                    radCtrl.setValue(this.doa.region.radius);
                }
            }
            else {
                this.doa.region.source = 1;
                this.selectedSourceId = 1;
            }
        }
        else {
            this.doa.region.source = 1;
            this.selectedSourceId = 1;
        }
    };
    DoaNewComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.doasForm = this.fb.group({
            doaName: [{ value: this.model.name, disabled: false }, []],
            doaDescription: [{ value: this.model.description, disabled: false }, []],
            doaLocation: [{ value: this.model.region.location, disabled: false }, []],
            doaRadius: [{ value: this.model.region.radius, disabled: false }, []],
            doaPoints: [{ value: this.model.region.points, disabled: false }, []],
            doaGeo: [{ value: this.model.region.geography, disabled: true }, []],
        });
        this.$ctrl("doaRadius").setValidators(radiusValidatorFn(this.radiusLimit));
        this.$ctrl("doaName").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateDoaNameUnique(); });
        this.$ctrl("doaLocation").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateRegionLocation(); });
        this.$ctrl("doaPoints").valueChanges.debounceTime(1000).subscribe(function () { return _this.validatePointsLocation(); });
        this.$ctrl("doaRadius").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateRegionLocation(); });
    };
    DoaNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        this.prepareData();
        //Clean the extra info on the data
        var geoReg = { fir: '', geography: '', location: '', points: '', radius: 0, source: this.doa.region.source };
        if (this.doa.region.source === data_model_1.DoaRegionSource.Geography) {
            geoReg.geography = this.doa.region.geography;
        }
        else if (this.doa.region.source === data_model_1.DoaRegionSource.FIR) {
            geoReg.fir = this.doa.region.fir;
        }
        else if (this.doa.region.source === data_model_1.DoaRegionSource.Points) {
            geoReg.points = this.doa.region.points;
        }
        else if (this.doa.region.source === data_model_1.DoaRegionSource.PointAndRadius) {
            geoReg.location = this.doa.region.location.toUpperCase();
            geoReg.radius = this.doa.region.radius;
        }
        this.doa.region = geoReg;
        this.dataService.validateDoaInCanada(this.doa)
            .subscribe(function (data) {
            if (data) {
                _this.dataService.saveDoa(_this.doa)
                    .subscribe(function (data) {
                    var msg = _this.translation.translate('DoasAdminSession.SuccessDoaCreated');
                    _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                    _this.router.navigate(["/administration/doas"]);
                }, function (error) {
                    _this.isSubmitting = false;
                    var msg = _this.translation.translate('DoasAdminSession.FailureDoaCreated');
                    _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
            }
            else {
                _this.isSubmitting = false;
                var msg = _this.translation.translate('DoasAdminSession.RegionOutOfCanada');
                _this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
            }
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('DoasAdminSession.ErrorRegionOutOfCanada');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    DoaNewComponent.prototype.prepareData = function () {
        this.doa.id = 0;
        this.doa.name = this.doasForm.get('doaName').value;
        this.doa.description = this.doasForm.get('doaDescription').value;
        if (this.doa.region.source === data_model_1.DoaRegionSource.PointAndRadius) {
            var radCtrl = this.$ctrl("doaRadius");
            this.doa.region.radius = +radCtrl.value;
        }
        else if (this.doa.region.source === data_model_1.DoaRegionSource.Points) {
            var radCtrl = this.$ctrl("doaRadius");
            this.doa.region.radius = +radCtrl.value;
        }
        this.doa.doaType = data_model_1.RegionType.Doa;
    };
    DoaNewComponent.prototype.disableSubmit = function () {
        if (this.doasForm.pristine)
            return true;
        if (this.isValidating)
            return true;
        return !this.validateForm();
    };
    DoaNewComponent.prototype.saveDoaFile = function () {
        var _this = this;
        var winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            var fileUpload = $("#attachments").get(0);
            var files = fileUpload.files;
            // Create FormData object  
            var fileData = new FormData();
            fileData.append(files[0].name, files[0]);
            this.dataService.saveDoaFile(fileData)
                .finally(function () {
                $("#attachments").val('');
                if (!/safari/i.test(navigator.userAgent)) {
                    $("#attachments").type = '';
                    $("#attachments").type = 'file';
                }
            })
                .subscribe(function (data) {
                _this.doa.region.geography = data;
                _this.doa.region.source = data_model_1.DoaRegionSource.Geography;
                _this.$ctrl('doaGeo').setValue(_this.doa.region.geography);
                var message = _this.translation.translate('DoasAdminSession.FileSuccess');
                _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                _this.newDoaRegionRenderMap();
            }, function (error) {
                _this.doa.region.geography = "";
                _this.doa.region.source = data_model_1.DoaRegionSource.None;
                _this.$ctrl('doaGeo').setValue(_this.doa.region.geography);
                _this.toastr.error(_this.translation.translate('DoasAdminSession.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            this.doa.region.geography = "";
            this.doa.region.source = data_model_1.DoaRegionSource.None;
            this.toastr.error(this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser1'), this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    DoaNewComponent.prototype.backToDoasList = function () {
        this.router.navigate(['/administration/doas']);
    };
    DoaNewComponent.prototype.validDoaData = function () {
        if (this.doa.name.length > 3) { //&& (this.doa.description.length > 0) && (this.doa.region.source > DoaRegionSource.None)) {
            if (this.doa.region.source === data_model_1.DoaRegionSource.Geography && this.doa.region.geography.length > 0)
                return true;
            else if (this.doa.region.source === data_model_1.DoaRegionSource.FIR && this.doa.region.fir !== '-1' && this.doa.region.fir !== '')
                return true;
            else if (this.doa.region.source === data_model_1.DoaRegionSource.PointAndRadius && this.doa.region.location !== '' && isANumber(String(this.doa.region.radius)) && this.doa.region.radius > 0 && this.doa.region.radius <= this.radiusLimit)
                return this.isValidLocation;
            else if (this.doa.region.source === data_model_1.DoaRegionSource.Points && this.doa.region.points !== '')
                return this.isValidLocation;
        }
        return false;
    };
    DoaNewComponent.prototype.validDoaName = function () {
        var dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    DoaNewComponent.prototype.validDoaNameSize = function () {
        var dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 3)
                return true;
        }
        return false;
    };
    DoaNewComponent.prototype.validateDoaNameUnique = function () {
        var _this = this;
        if (this.isValidating)
            return;
        if (this.validDoaName() && this.validDoaNameSize()) {
            var dName = this.$ctrl('doaName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsDoaName(this.doa.id, dName.value)
                        .finally(function () {
                        _this.isValidating = false;
                    })
                        .subscribe(function (resp) {
                        _this.isDoaUniqueName = !resp;
                    });
                }
            }
        }
    };
    DoaNewComponent.prototype.validateRegionLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('doaLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaLocation(dLocation.value)
                    .finally(function () {
                    _this.isValidating = false;
                })
                    .subscribe(function (resp) {
                    _this.isValidLocation = true;
                    _this.doa.region.location = resp.toUpperCase();
                    _this.isValidating = false;
                    _this.validateForm();
                    _this.newDoaRegionRenderMap();
                }, function (error) {
                    _this.isValidLocation = false;
                    _this.doa.region.location = '';
                });
            }
            else
                this.isValidLocation = false;
        }
        else
            this.isValidLocation = false;
    };
    DoaNewComponent.prototype.validatePointsLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('doaPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaPoints(dLocation.value)
                    .finally(function () {
                    _this.isValidating = false;
                })
                    .subscribe(function (resp) {
                    _this.isValidLocation = true;
                    _this.doa.region.points = resp;
                    _this.isValidating = false;
                    _this.validateForm();
                    _this.newDoaRegionRenderMap();
                }, function (error) {
                    _this.isValidLocation = false;
                    _this.doa.region.points = '';
                    _this.isValidating = false;
                });
            }
            else
                this.isValidLocation = false;
        }
        else
            this.isValidLocation = false;
    };
    DoaNewComponent.prototype.validateRadius = function (ctrlName) {
        if (this.doasForm.pristine)
            return true;
        var fc = this.doasForm.get(ctrlName);
        if (fc.errors)
            return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit)
                return false;
            if (fc.value < 1)
                return false;
        }
        else
            return false;
        return true;
    };
    DoaNewComponent.prototype.validateForm = function () {
        if (this.isValidating)
            return false;
        if (!this.doasForm.pristine) {
            this.prepareData();
            if (this.validDoaData()) {
                return this.isDoaUniqueName;
            }
        }
        return false;
    };
    DoaNewComponent.prototype.$ctrl = function (name) {
        return this.doasForm.get(name);
    };
    DoaNewComponent.prototype.decRadius = function () {
        if (!this.isReadOnly) {
            var radCtrl = this.$ctrl("doaRadius");
            if (+radCtrl.value > 0) {
                radCtrl.setValue(+radCtrl.value - 1);
                radCtrl.markAsDirty();
            }
        }
    };
    DoaNewComponent.prototype.incRadius = function () {
        if (!this.isReadOnly) {
            var radCtrl = this.$ctrl("doaRadius");
            if (+radCtrl.value < this.radiusLimit) {
                radCtrl.setValue(+radCtrl.value + 1);
                radCtrl.markAsDirty();
            }
        }
    };
    DoaNewComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    //Map functions
    DoaNewComponent.prototype.newDoaRegionRenderMap = function () {
        var reg = {
            region: this.doa.region,
            changes: []
        };
        this.renderMap(reg);
    };
    DoaNewComponent.prototype.mapAvailable = function () {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    };
    DoaNewComponent.prototype.initMap = function () {
        var winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;
        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    };
    DoaNewComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.initMap();
                _this.mapHealthy = true;
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        });
    };
    DoaNewComponent.prototype.renderMap = function (reg) {
        var _this = this;
        this.dataService.getGeoFeatures(reg)
            .subscribe(function (resp) {
            var geoFeatures = resp;
            _this.renderRegionInMap(geoFeatures, "DoaModified");
        }, function (error) {
            _this.toastr.error(_this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), _this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        });
    };
    DoaNewComponent.prototype.renderRegionInMap = function (geoFeatures, name) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        }
        else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    DoaNewComponent.prototype.disposeMap = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    };
    DoaNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/doas/doa-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], DoaNewComponent);
    return DoaNewComponent;
}());
exports.DoaNewComponent = DoaNewComponent;
function radiusValidatorFn(maxRadius) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=doa-new.component.js.map