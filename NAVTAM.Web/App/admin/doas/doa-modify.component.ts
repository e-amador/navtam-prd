﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';
import { WindowRef } from '../../common/windowRef.service';

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'

import { DoaRegionSource, IDoaRegion, IDoaAdmin, IFIR, IDoaPointModel, ISelectOptions, RegionType } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/doas/doa-modify.component.html'
})
export class DoaModifyComponent implements OnInit {
    model: any = null;
    action: string;

    public doasForm: FormGroup;

    doa: IDoaAdmin;
    isDoaUniqueName: boolean = true;
    isValidating: boolean = false;
    isValidLocation: boolean = true;

    doaChangesList: IDoaPointModel[] = [];
    selectedDoaLocation: IDoaPointModel = null;

    activeLocationPoint: IDoaPointModel = null;

    doaSource: string = this.translation.translate('DoasAdminSession.DoaSourceGeography');

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private windowRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        this.activeLocationPoint = { source: '', location: '', inDoa: false, radius:0 };
        this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.doa = this.model;

        this.configureReactiveForm();
    }

    onSubmit() {
        var doaUpdate: IDoaAdmin = this.prepareData();
        if (doaUpdate) {
            this.dataService.saveDoa(doaUpdate)
                .subscribe(data => {
                    let msg = this.translation.translate('DoasAdminSession.SuccessDoaCreated');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                    this.router.navigate(["/administration/doas"]);
                }, error => {
                    let msg = this.translation.translate('DoasAdminSession.FailureDoaCreated');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            let msg = this.translation.translate('DoasAdminSession.ErrorDoaData');
            this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
        }
    }

    private configureReactiveForm() {
        this.doasForm = this.fb.group({
            doaName: [{ value: this.model.name, disabled: false }, []],
            doaDescription: [{ value: this.model.description, disabled: false }, []],
            doaSource: [{ value: this.doaSource, disabled: true }, []],
            doaGeo: [{ value: this.model.region.geography, disabled: true }, []],
            doaLocation: [{ value: this.selectedDoaLocation.location, disabled: false }, []],
            doaRadius: [{ value: this.selectedDoaLocation.radius, disabled: false }, [doaRadiusValidator]],
        });

        this.$ctrl("doaName").valueChanges.debounceTime(1000).subscribe(() => this.validateDoaNameUnique());
        this.$ctrl("doaLocation").valueChanges.debounceTime(1000).subscribe(() => this.validateRegionLocation());
    }

    public validateForm(): boolean {
        if (this.isValidating) return false;
        if (!this.doasForm.pristine) {
            if(this.prepareData())
                return this.isDoaUniqueName;
            return true;
        }
        return false;
    }

    public prepareData(): IDoaAdmin {
        if (!this.isDoaUniqueName) return null;

        const dName = this.$ctrl('doaName');
        const dDesc = this.$ctrl('doaDescription');

        var doaUpdate: IDoaAdmin = {
            id: (this.action === 'copy') ? 0 : this.doa.id,
            name: dName.value,
            description: dDesc.value,
            region: this.doa.region,
            doaType: RegionType.Doa,
            changes: this.doaChangesList
        };

        return doaUpdate;
    }

    public validateRegionLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('doaLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.isValidLocation = false;
                this.dataService.validateDoaGeoLocation(this.doa.id, dLocation.value)
                    .subscribe((resp: IDoaPointModel) => {
                        this.isValidLocation = true;
                        this.activeLocationPoint = resp;
                        this.isValidating = false;
                        this.validateForm();
                    }, error => {
                        this.activeLocationPoint.location = '';
                        this.isValidating = false;
                    });
            } 
        } 
    }

    public prepareLocationData() {
        if (this.isValidLocation) {
            const radiusCtrl = this.$ctrl('doaRadius');
            var loc: IDoaPointModel = { source: '', location: '', inDoa: false, radius: 5 };
            loc.source = this.activeLocationPoint.source.toUpperCase();
            loc.location = this.activeLocationPoint.location.toUpperCase();
            loc.radius = +radiusCtrl.value;
            loc.inDoa = this.activeLocationPoint.inDoa;

            var point = this.doaChangesList.find(x => x.source === loc.source && x.location === loc.location);
            if (point) {
                point.radius = loc.radius;
                this.selectedDoaLocation = point;
            } else {
                var index = this.doaChangesList.push(loc);
                this.selectedDoaLocation = this.doaChangesList[index - 1];
            }
        }
    }

    public includeLocationEnabled(): boolean {
        if (this.isValidLocation && !this.isValidating) {
            const radiusCtrl = this.$ctrl('doaRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '') {
                return !this.activeLocationPoint.inDoa;
            }
        } 
        return false;
    }

    public excludeLocationEnabled(): boolean {
        if (this.isValidLocation && !this.isValidating) {
            const radiusCtrl = this.$ctrl('doaRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '') return this.activeLocationPoint.inDoa;
        }
        return false;
    }

    disableSubmit(): boolean {
        if (this.doasForm.pristine) return true;
        if (this.isValidating) return true;
        return !this.validateForm();
    }

    public validDoaName(): boolean {
        const dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 0) return true;
        }
        return false;
    }

    public validDoaNameSize(): boolean {
        const dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 3) return true;
        }
        return false;
    }

    public validateDoaNameUnique() {
        if (this.isValidating) return;
        if (this.validDoaName() && this.validDoaNameSize()) {
            const dName = this.$ctrl('doaName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    var id = (this.action === 'copy') ? 0 : this.doa.id;
                    this.dataService.existsDoaName(id, dName.value)
                        .subscribe((resp: boolean) => {
                            this.isDoaUniqueName = !resp;
                            this.isValidating = false;
                        });
                }
            }
        }
    }

    public pickLocation(loc: IDoaPointModel) {
        if (this.doaChangesList) {
            var point = this.doaChangesList.find(x => (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa));
            if (point) {
                this.selectedDoaLocation = point;
                var ctrlLocation = this.$ctrl('doaLocation');
                var ctrlRadius = this.$ctrl('doaRadius');
                ctrlLocation.setValue(this.selectedDoaLocation.source);
                ctrlRadius.setValue(this.selectedDoaLocation.radius);
            }
        }
    }

    $ctrl(name: string): AbstractControl {
        return this.doasForm.get(name);
    }

    public rowColor(loc: IDoaPointModel) {
        if (!this.selectedDoaLocation) return '';
        if (this.selectedDoaLocation.source === loc.source && 
            this.selectedDoaLocation.location === loc.location &&
            this.selectedDoaLocation.inDoa === loc.inDoa) return 'bg-submitted';
        return '';
    }

    public deletePoint(loc: IDoaPointModel) {
        if (this.doaChangesList) {
            var index = this.doaChangesList.findIndex(x => (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa));
            if (index > -1) {
                this.doaChangesList.splice(index, 1);
                if (this.doaChangesList.length > 0) {
                    this.selectedDoaLocation = this.doaChangesList[0];
                    var ctrlLocation = this.$ctrl('doaLocation');
                    var ctrlRadius = this.$ctrl('doaRadius');
                    ctrlLocation.setValue(this.selectedDoaLocation.source);
                    ctrlRadius.setValue(this.selectedDoaLocation.radius);
                }
                else {
                    var ctrlLocation = this.$ctrl('doaLocation');
                    var ctrlRadius = this.$ctrl('doaRadius');
                    ctrlLocation.setValue('');
                    ctrlRadius.setValue(5);
                    this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
                    this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
                }
            }
        }
    }

    public getAction(inDoa: boolean) {
        if (inDoa) return this.translation.translate('DoasAdminSession.Exclude');
        else return this.translation.translate('DoasAdminSession.Include');
    }

    public validateRadius() {
        const fc = this.doasForm.get('doaRadius');
        return !fc.errors || this.doasForm.pristine;
    }

    public validateLocation(): boolean {
        return this.isValidLocation || this.doasForm.pristine;
    }

    decRadius() {
        const radCtrl = this.$ctrl("doaRadius");
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incRadius() {
        const radCtrl = this.$ctrl("doaRadius");
        if (+radCtrl.value < 9999) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    public backToDoasList() {
        this.router.navigate(['/administration/doas']);
    }

}

function doaRadiusValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (typeof (c.value) === "string") {
        if (!c.value) {
            return { 'required': true }
        }
    }

    if (isANumber(c.value)) {
        if (+c.value < 0 || +c.value > 999999) {
            return { 'invalid': true }
        }

        return null;
    }

    return { 'invalid': true }
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}
