﻿import { Component, OnInit, Inject } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { ActivatedRoute, Router } from '@angular/router'

import { Select2OptionData } from 'ng2-select2';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import { LocaleService, TranslationService } from 'angular-l10n';

import {
    IQueryOptions,
    IPaging,
    IDoaAdminPartial,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

@Component({
    templateUrl: '/app/admin/doas/doas-list.component.html'
})
export class DoasListComponent implements OnInit {
    queryOptions: IQueryOptions;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;
    canbeDeleted: boolean = false;

    doas: IDoaAdminPartial[];
    selectedDoa: IDoaAdminPartial = null;

    doaListForm: FormGroup; 

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);

        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'Name', direction: 0 },
                    { columnName: 'Description', direction: 0 },
                    { columnName: 'Organizations', direction: 0 }
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '200', text: '200' },
                ]
        };

        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "doas",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName, //Default Received Descending
            filterValue: ""
        }
    };

    ngOnInit() {
        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "doas") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getDoasInPage(this.queryOptions.page);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    private configureReactiveForm() {
        this.doaListForm = this.fb.group({
            searchDoa: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchDoa").valueChanges.debounceTime(1000).subscribe(() => this.updateDoasList());
    }

    updateDoasList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchDoa');
        this.queryOptions.filterValue = subject.value;
        this.getDoasInPage(this.queryOptions.page);
    }

    $ctrl(name: string): AbstractControl {
        return this.doaListForm.get(name);
    }

    selectDoaAllocation(doa: IDoaAdminPartial) {
        if (this.selectedDoa) {
            this.selectedDoa.isSelected = false;
        }

        this.canbeDeleted = (doa.orgCount == 0);
        this.selectedDoa = doa;
        this.selectedDoa.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedDoa.id);
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getDoasInPage(1);
    }

    onPageChange(page: number) {
        this.getDoasInPage(page);
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getDoasInPage(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    private getDoasInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredDoas(this.queryOptions)
                .subscribe((doasData: any) => {
                    this.processDoas(doasData);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processDoas(doasData: any) {
        this.paging = doasData.paging;
        this.totalPages = doasData.paging.totalPages;

        for (var iter = 0; iter < doasData.data.length; iter++) doasData.data[iter].index = iter;

        const lastDoaIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (doasData.data.length > 0) {
            this.doas = doasData.data;
            let index = 0;
            if (lastDoaIdSelected) {
                index = this.doas.findIndex(x => x.id === lastDoaIdSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.doas[index].isSelected = true;
            this.selectedDoa = this.doas[index];
            this.canbeDeleted = (this.selectedDoa.orgCount == 0);
            this.loadingData = false;
        } else {
            this.doas = [];
            this.selectedDoa = null;
            this.loadingData = false;
        }
    }

    confirmDeleteDoa() {
        $('#modal-delete').modal({});
    }

    deleteDoa() {
        if (this.selectedDoa) {
            this.dataService.deleteDoa(this.selectedDoa.id)
                .subscribe(result => {
                    this.getDoasInPage(this.paging.currentPage);
                    let msg = this.translation.translate('DoasAdminSession.SuccessDoaDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('DoasAdminSession.FailureDoaDeleted');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }


}