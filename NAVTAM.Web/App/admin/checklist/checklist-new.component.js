"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeriesChecklistNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var location_service_1 = require("../../dashboard/shared/location.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var SeriesChecklistNewComponent = /** @class */ (function () {
    function SeriesChecklistNewComponent(toastr, fb, dataService, router, activatedRoute, winRef, locale, locationService, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.locale = locale;
        this.locationService = locationService;
        this.translation = translation;
        this.model = null;
        this.isReadOnly = false;
        this.isSeriesUniqueName = true;
        this.isFirsValid = true;
        this.isValidCoordinatesValue = true;
        this.isValidating = false;
        this.isSubmitting = false;
        this.radiusLimit = 999;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }
    SeriesChecklistNewComponent.prototype.ngOnInit = function () {
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'create') {
            this.seriesRec = {
                series: "",
                firs: "",
                coordinates: "",
                radius: 999,
                isSelected: false
            };
        }
        else { //if (this.action === 'copy') {
            this.isReadOnly = true;
            this.seriesRec = this.activatedRoute.snapshot.data['model'];
        }
        this.model = this.seriesRec;
        this.configureReactiveForm();
    };
    SeriesChecklistNewComponent.prototype.ngOnDestroy = function () {
    };
    SeriesChecklistNewComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.seriesListForm = this.fb.group({
            series: [{ value: this.model.series, disabled: this.isReadOnly }, []],
            firs: [{ value: this.model.firs, disabled: false }, []],
            coordinates: [{ value: this.model.coordinates, disabled: false }, [forms_1.Validators.required]],
            radius: [{ value: this.model.radius, disabled: false }, []],
        });
        if (!this.isReadOnly) {
            this.$ctrl("series").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateSeriesUnique(); });
        }
        this.$ctrl("firs").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateFirsName(); });
        this.$ctrl("coordinates").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateCoordinatesValue(); });
    };
    SeriesChecklistNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        this.prepareData();
        this.dataService.saveSeriesChecklist(this.seriesRec)
            .subscribe(function (data) {
            var msg = _this.translation.translate('ChecklistSession.SuccessSeriesCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/checklist"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('ChecklistSession.FailureSeriesCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    SeriesChecklistNewComponent.prototype.prepareData = function () {
        this.seriesRec.series = this.$ctrl('series').value;
        this.seriesRec.firs = this.$ctrl('firs').value;
        this.seriesRec.coordinates = this.$ctrl('coordinates').value;
        this.seriesRec.radius = +this.$ctrl('radius').value;
        this.seriesRec.series = this.seriesRec.series.toUpperCase();
        this.seriesRec.firs = this.seriesRec.firs.toUpperCase();
        this.seriesRec.coordinates = this.seriesRec.coordinates.toUpperCase();
    };
    SeriesChecklistNewComponent.prototype.disableSubmit = function () {
        if (this.seriesListForm.pristine)
            return true;
        if (this.isValidating)
            return true;
        if (this.isSubmitting)
            return true;
        return !this.validateForm();
    };
    SeriesChecklistNewComponent.prototype.backToConfigList = function () {
        this.router.navigate(['/administration/checklist']);
    };
    SeriesChecklistNewComponent.prototype.validSeriesData = function () {
        if (this.seriesRec.series.length === 1 &&
            this.seriesRec.firs.length >= 4 &&
            this.seriesRec.coordinates.length > 0 &&
            this.seriesRec.radius > 0 && this.seriesRec.radius < 1000) {
            return true;
        }
        return false;
    };
    SeriesChecklistNewComponent.prototype.validSeriesSize = function () {
        if (this.seriesListForm.pristine)
            return true;
        var dName = this.$ctrl('series');
        if (dName.value) {
            if (dName.value.length === 1)
                return true;
        }
        return false;
    };
    SeriesChecklistNewComponent.prototype.validateSeriesUnique = function () {
        var _this = this;
        if (this.seriesListForm.pristine)
            return true;
        if (this.isValidating && this.action !== 'create')
            return;
        if (this.validSeriesSize()) {
            var dName = this.$ctrl('series').value;
            this.isValidating = true;
            this.dataService.verifySeriesChecklist(dName.toUpperCase())
                .finally(function () {
                _this.isValidating = false;
            })
                .subscribe(function (resp) {
                _this.isSeriesUniqueName = !resp;
            });
        }
    };
    SeriesChecklistNewComponent.prototype.validFirsSize = function () {
        if (this.seriesListForm.pristine)
            return true;
        var dName = this.$ctrl('firs');
        if (dName.value) {
            if (dName.value.length >= 4)
                return true;
        }
        return false;
    };
    SeriesChecklistNewComponent.prototype.validateFirsName = function () {
        var _this = this;
        if (this.seriesListForm.pristine)
            return true;
        if (this.isValidating)
            return;
        if (this.validFirsSize()) {
            var dName = this.$ctrl('firs').value;
            if (dName) {
                if (dName.length > 3) {
                    this.isValidating = true;
                    this.dataService.validateFirsNames(dName.toUpperCase())
                        .finally(function () {
                        _this.isValidating = false;
                    })
                        .subscribe(function (resp) {
                        _this.isFirsValid = resp;
                    }, function (error) {
                        _this.isFirsValid = false;
                        _this.toastr.error(_this.translation.translate('ChecklistSession.ErrorNotFIRData') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                    });
                }
            }
        }
    };
    SeriesChecklistNewComponent.prototype.validCoordinatesSize = function () {
        if (this.seriesListForm.pristine)
            return true;
        var dName = this.$ctrl('coordinates');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    SeriesChecklistNewComponent.prototype.validateCoordinatesValue = function () {
        if (this.seriesListForm.pristine)
            return true;
        if (this.isValidating)
            return;
        if (this.validCoordinatesSize()) {
            var dName = this.$ctrl('coordinates').value;
            if (dName) {
                this.isValidCoordinatesValue = this.locationService.validDMSLocation(dName);
            }
            else
                this.isValidCoordinatesValue = false;
        }
    };
    SeriesChecklistNewComponent.prototype.decRadius = function () {
        var radCtrl = this.$ctrl('radius');
        if (+radCtrl.value > 1) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    SeriesChecklistNewComponent.prototype.incRadius = function () {
        var radCtrl = this.$ctrl('radius');
        if (+radCtrl.value < this.radiusLimit) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    SeriesChecklistNewComponent.prototype.validateRadius = function () {
        if (this.seriesListForm.pristine)
            return true;
        var fc = this.$ctrl('radius');
        if (fc.errors)
            return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit)
                return false;
            if (fc.value < 1)
                return false;
        }
        else
            return false;
        return true;
    };
    SeriesChecklistNewComponent.prototype.validateForm = function () {
        if (this.isValidating)
            return false;
        if (!this.seriesListForm.pristine) {
            this.prepareData();
            if (this.validSeriesData()) {
                return (this.isFirsValid && this.isSeriesUniqueName && this.isValidCoordinatesValue);
            }
        }
        return false;
    };
    SeriesChecklistNewComponent.prototype.$ctrl = function (name) {
        return this.seriesListForm.get(name);
    };
    SeriesChecklistNewComponent.prototype.validateKeyPressLetter = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isALetter(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    SeriesChecklistNewComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    SeriesChecklistNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/checklist/checklist-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            angular_l10n_1.LocaleService,
            location_service_1.LocationService,
            angular_l10n_1.TranslationService])
    ], SeriesChecklistNewComponent);
    return SeriesChecklistNewComponent;
}());
exports.SeriesChecklistNewComponent = SeriesChecklistNewComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
function isALetter(str) {
    return !/[^a-z]/i.test(str);
}
//# sourceMappingURL=checklist-new.component.js.map