"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChecklistComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_service_1 = require("../shared/data.service");
var ChecklistComponent = /** @class */ (function () {
    function ChecklistComponent(toastr, fb, dataService, memStorageService, router, activatedRoute, winRef, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.locale = locale;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.selectedSeries = null;
        this.model = null;
        this.latestPubConfig = null;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
        this.tableHeaders = {
            sorting: [
                { columnName: 'Series', direction: 0 },
                { columnName: 'FIRS', direction: 0 },
                { columnName: 'Coordinates', direction: 0 },
                { columnName: 'Radius', direction: 0 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "seriesRec",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName,
            filterValue: ""
        };
    }
    ChecklistComponent.prototype.ngOnInit = function () {
        this.model = this.activatedRoute.snapshot.data['model'];
        this.latestPubConfig = this.model;
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "seriesRec") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getSeriesValuesInPage(this.queryOptions.page);
    };
    Object.defineProperty(ChecklistComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    ChecklistComponent.prototype.selectSeriesRecord = function (serie) {
        if (this.selectedSeries) {
            this.selectedSeries.isSelected = false;
        }
        this.selectedSeries = serie;
        this.selectedSeries.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedSeries.series);
    };
    ChecklistComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getSeriesValuesInPage(1);
    };
    ChecklistComponent.prototype.onPageChange = function (page) {
        this.getSeriesValuesInPage(page);
    };
    ChecklistComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getSeriesValuesInPage(this.queryOptions.page);
    };
    ChecklistComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    ChecklistComponent.prototype.getSeriesValuesInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredSeriesChecklistValues(this.queryOptions)
                .subscribe(function (configData) {
                _this.processSeriesValues(configData);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    ChecklistComponent.prototype.processSeriesValues = function (configData) {
        this.paging = configData.paging;
        this.totalPages = configData.paging.totalPages;
        for (var iter = 0; iter < configData.data.length; iter++)
            configData.data[iter].index = iter;
        var stored = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (configData.data.length > 0) {
            this.seriesRecs = configData.data;
            var index = 0;
            if (stored) {
                index = this.seriesRecs.findIndex(function (x) { return x.series === stored; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.seriesRecs[index].isSelected = true;
            this.selectedSeries = this.seriesRecs[index];
            this.loadingData = false;
        }
        else {
            this.seriesRecs = [];
            this.selectedSeries = null;
            this.loadingData = false;
        }
    };
    ChecklistComponent.prototype.onSubmit = function () {
        $('#modal-publish').modal({});
    };
    ChecklistComponent.prototype.publishChecklist = function () {
        var _this = this;
        this.dataService.pushChecklist()
            .subscribe(function (data) {
            var msg = _this.translation.translate('ChecklistSession.SuccessPush');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            var msg = _this.translation.translate('ChecklistSession.FailurePush');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    ChecklistComponent.prototype.configureReactiveForm = function () {
        this.checklistForm = this.fb.group({
            latestPublication: [{ value: this.latestPubConfig.value, disabled: false }, []],
        });
    };
    ChecklistComponent.prototype.saveData = function () {
        var _this = this;
        this.latestPubConfig.value = this.$ctrl('latestPublication').value;
        this.latestPubConfig.value = this.latestPubConfig.value.toUpperCase();
        this.dataService.saveChecklistConfig(this.latestPubConfig)
            .subscribe(function (data) {
            var msg = _this.translation.translate('ConfigurationSession.SuccessConfigCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            var msg = _this.translation.translate('ConfigurationSession.FailureConfigCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    ChecklistComponent.prototype.disableSave = function () {
        var val = this.$ctrl('latestPublication').value;
        if (val.trim().length === 0)
            return true;
        else if (this.latestPubConfig.value.localeCompare(val.toUpperCase()) === 0)
            return true;
        return false;
    };
    ChecklistComponent.prototype.disableReset = function () {
        var val = this.$ctrl('latestPublication').value;
        if (this.latestPubConfig.value.localeCompare(val.toUpperCase()) === 0)
            return true;
        else
            return false;
    };
    ChecklistComponent.prototype.reloadData = function () {
        var _this = this;
        this.dataService.getChecklistValue()
            .finally(function () {
            _this.$ctrl('latestPublication').setValue(_this.latestPubConfig.value);
        })
            .subscribe(function (resp) {
            _this.latestPubConfig = resp;
        }, function (error) {
            _this.latestPubConfig = {
                name: "",
                description: "",
                value: "",
                category: "NOTAM",
                action: ""
            };
        });
    };
    ChecklistComponent.prototype.$ctrl = function (name) {
        return this.checklistForm.get(name);
    };
    ChecklistComponent.prototype.confirmDeleteSeries = function () {
        $('#modal-delete').modal({});
    };
    ChecklistComponent.prototype.deleteSeries = function () {
        var _this = this;
        if (this.selectedSeries) {
            this.dataService.deleteSeriesChecklist(this.selectedSeries.series)
                .subscribe(function (result) {
                _this.getSeriesValuesInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('ChecklistSession.SuccessSerieChecklistDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('ChecklistSession.FailureSerieChecklistDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    ChecklistComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/checklist/checklist.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], ChecklistComponent);
    return ChecklistComponent;
}());
exports.ChecklistComponent = ChecklistComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=checklist.component.js.map