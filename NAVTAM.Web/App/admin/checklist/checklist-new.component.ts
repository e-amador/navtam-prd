﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms'
import { LocaleService, TranslationService } from 'angular-l10n';
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { DataService } from '../shared/data.service'
import { LocationService } from '../../dashboard/shared/location.service'
import { WindowRef } from '../../common/windowRef.service';
import { ISeriesChecklist } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/checklist/checklist-new.component.html'
})
export class SeriesChecklistNewComponent implements OnInit {

    model: any = null;
    action: string;

    public seriesListForm: FormGroup;

    isReadOnly: boolean = false;

    seriesRec: ISeriesChecklist;
    isSeriesUniqueName: boolean = true;
    isFirsValid: boolean = true;
    isValidCoordinatesValue: boolean = true;
    isValidating: boolean = false;
    isSubmitting: boolean = false;

    radiusLimit: number = 999;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        public locale: LocaleService,
        public locationService: LocationService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'create') {
            this.seriesRec = {
                series: "",
                firs: "",
                coordinates: "",
                radius: 999,
                isSelected: false
            };
        } else {//if (this.action === 'copy') {
            this.isReadOnly = true;
            this.seriesRec = this.activatedRoute.snapshot.data['model'];
        }
        this.model = this.seriesRec;
        this.configureReactiveForm();
    }

    ngOnDestroy() {
    }

    private configureReactiveForm() {
        this.seriesListForm = this.fb.group({
            series: [{ value: this.model.series, disabled: this.isReadOnly }, []],
            firs: [{ value: this.model.firs, disabled: false }, []],
            coordinates: [{ value: this.model.coordinates, disabled: false }, [Validators.required]],
            radius: [{ value: this.model.radius, disabled: false }, []],
        });

        if (!this.isReadOnly) {
            this.$ctrl("series").valueChanges.debounceTime(1000).subscribe(() => this.validateSeriesUnique());
        }
        this.$ctrl("firs").valueChanges.debounceTime(1000).subscribe(() => this.validateFirsName());
        this.$ctrl("coordinates").valueChanges.debounceTime(1000).subscribe(() => this.validateCoordinatesValue());
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        this.prepareData();
        this.dataService.saveSeriesChecklist(this.seriesRec)
            .subscribe(data => {
                let msg = this.translation.translate('ChecklistSession.SuccessSeriesCreated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/checklist"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('ChecklistSession.FailureSeriesCreated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private prepareData() {
        this.seriesRec.series = this.$ctrl('series').value;
        this.seriesRec.firs = this.$ctrl('firs').value;
        this.seriesRec.coordinates = this.$ctrl('coordinates').value;
        this.seriesRec.radius = +this.$ctrl('radius').value;

        this.seriesRec.series = this.seriesRec.series.toUpperCase();
        this.seriesRec.firs = this.seriesRec.firs.toUpperCase();
        this.seriesRec.coordinates = this.seriesRec.coordinates.toUpperCase();
    }

    disableSubmit(): boolean {
        if (this.seriesListForm.pristine) return true;
        if (this.isValidating) return true;
        if (this.isSubmitting) return true;
        return !this.validateForm()
    }

    public backToConfigList() {
        this.router.navigate(['/administration/checklist']);
    }

    private validSeriesData(): boolean {
        if (this.seriesRec.series.length === 1 &&
            this.seriesRec.firs.length >= 4 &&
            this.seriesRec.coordinates.length > 0 &&
            this.seriesRec.radius > 0 && this.seriesRec.radius < 1000) {
            return true;
        }
        return false;
    }

    public validSeriesSize(): boolean {
        if (this.seriesListForm.pristine) return true;
        const dName = this.$ctrl('series');
        if (dName.value) {
            if (dName.value.length === 1) return true;
        }
        return false;
    }

    public validateSeriesUnique() {
        if (this.seriesListForm.pristine) return true;
        if (this.isValidating && this.action !== 'create') return;
        if (this.validSeriesSize()) {
            const dName: string = this.$ctrl('series').value;
            this.isValidating = true;
            this.dataService.verifySeriesChecklist(dName.toUpperCase())
                .finally(() => {
                    this.isValidating = false;
                })
                .subscribe((resp: boolean) => {
                    this.isSeriesUniqueName = !resp;
                });
        }
    }

    public validFirsSize(): boolean {
        if (this.seriesListForm.pristine) return true;
        const dName = this.$ctrl('firs');
        if (dName.value) {
            if (dName.value.length >= 4) return true;
        }
        return false;
    }

    public validateFirsName() {
        if (this.seriesListForm.pristine) return true;
        if (this.isValidating ) return;
        if (this.validFirsSize()) {
            const dName: string = this.$ctrl('firs').value;
            if (dName) {
                if (dName.length > 3) {
                    this.isValidating = true;
                    this.dataService.validateFirsNames(dName.toUpperCase())
                        .finally(() => {
                            this.isValidating = false;
                        })
                        .subscribe((resp: boolean) => {
                            this.isFirsValid = resp;
                        }, (error: any) => {
                            this.isFirsValid = false;
                            this.toastr.error(this.translation.translate('ChecklistSession.ErrorNotFIRData') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                        });
                }
            }
        }
    }

    public validCoordinatesSize(): boolean {
        if (this.seriesListForm.pristine) return true;
        const dName = this.$ctrl('coordinates');
        if (dName.value) {
            if (dName.value.length > 0) return true;
        }
        return false;
    }

    public validateCoordinatesValue() {
        if (this.seriesListForm.pristine) return true;
        if (this.isValidating) return;
        if (this.validCoordinatesSize()) {
            const dName: string = this.$ctrl('coordinates').value;
            if (dName) {
                this.isValidCoordinatesValue = this.locationService.validDMSLocation(dName);
            } else this.isValidCoordinatesValue = false;
        }
    }

    decRadius() {
        const radCtrl = this.$ctrl('radius');
        if (+radCtrl.value > 1) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incRadius() {
        const radCtrl = this.$ctrl('radius');
        if (+radCtrl.value < this.radiusLimit) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    public validateRadius() {
        if (this.seriesListForm.pristine) return true;
        const fc = this.$ctrl('radius');
        if (fc.errors) return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit) return false;
            if (fc.value < 1) return false;
        } else return false;
        return true;
    }

    public validateForm(): boolean {
        if (this.isValidating) return false;
        if (!this.seriesListForm.pristine) {
            this.prepareData();
            if (this.validSeriesData()) {
                return (this.isFirsValid && this.isSeriesUniqueName && this.isValidCoordinatesValue);
            }
        }
        return false;
    }

    $ctrl(name: string): AbstractControl {
        return this.seriesListForm.get(name);
    }

    public validateKeyPressLetter(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isALetter(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

function isALetter(str: string): boolean {
    return !/[^a-z]/i.test(str);
}
