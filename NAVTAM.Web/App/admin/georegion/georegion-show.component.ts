﻿import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms'
import { WindowRef } from '../../common/windowRef.service';
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { LocaleService, TranslationService } from 'angular-l10n';
import { DataService } from '../shared/data.service'
import { IGeoRegion, IRegionMapRender } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/georegion/georegion-show.component.html'
})
export class GeoRegionShowComponent implements OnInit, AfterViewInit {
    model: any = null;
    action: string;

    public geoShowForm: FormGroup;

    geoRegion: IGeoRegion;

    leafletmap: any;
    mapHealthy: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.geoRegion = this.model;

        this.configureReactiveForm();
    }

    ngAfterViewInit() {
        this.mapHealthCheck();        
    }

    ngOnDestroy() {
        this.disposeMap();
    }
    private configureReactiveForm() {
        this.geoShowForm = this.fb.group({
            geoName: [{ value: this.model.name, disabled: true }, []],
            geoRegionGeo: [{ value: this.model.region.geography, disabled: true }, []],
        });
    }

    public backToGeoRegionList() {
        this.router.navigate(['/administration/georegion']);
    }

    $ctrl(name: string): AbstractControl {
        return this.geoShowForm.get(name);
    }

    //Map functions
    public geoRegionRenderMap() {
        let reg: IRegionMapRender = {
            region: this.geoRegion.region,
            changes: []
        };
        this.renderMap(reg);
    }

    private initMap() {
        const winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;

        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.initMap();
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        }, () => this.renderRegionInMap(this.model.region.geoFeatures, "Doa")
        );
    }

    public mapAvailable() {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    }

    public renderMap(reg: IRegionMapRender) {
        this.dataService.getGeoFeatures(reg)
            .subscribe((resp: string) => {
                let geoFeatures = resp;
                this.renderRegionInMap(geoFeatures, "DoaModified");
            }, error => {
                this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
            });
    }

    private renderRegionInMap(geoFeatures: string, name: string) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        } else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    private disposeMap() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    }
    //End Map functions
}

