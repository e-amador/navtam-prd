"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeoRegionNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var GeoRegionNewComponent = /** @class */ (function () {
    function GeoRegionNewComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isEdit = false;
        this.isGeoRegionUniqueName = true;
        this.isValidating = false;
        this.isValidLocation = true;
        this.isSubmitting = false;
        this.geoRegionSources = [];
        this.selectedSourceId = 1;
        this.firSources = [];
        this.selectedFirId = '-1';
        this.radiusLimit = 999;
        this.mapHealthy = false;
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    GeoRegionNewComponent.prototype.ngOnInit = function () {
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'edit')
            this.isEdit = true;
        else
            this.isEdit = false;
        if (!this.isEdit) {
            var region = {
                fir: "",
                geography: "",
                location: "",
                points: "",
                radius: 5,
                source: data_model_1.DoaRegionSource.None,
            };
            this.geoRegion = {
                id: 0,
                name: "",
                region: region,
                changes: null
            };
        }
        else {
            this.geoRegion = this.activatedRoute.snapshot.data['model'];
        }
        this.model = this.geoRegion;
        //Sources
        this.geoRegionSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('GeoRegionSession.SelectSource')
            },
            width: "100%"
        };
        this.loadGeoRegionSources();
        //FIRS
        this.firSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('GeoRegionSession.SelectFir')
            },
            width: "100%"
        };
        this.loadFirSources();
        this.configureReactiveForm();
    };
    GeoRegionNewComponent.prototype.ngAfterViewInit = function () {
        this.mapHealthCheck();
    };
    GeoRegionNewComponent.prototype.ngOnDestroy = function () {
        this.disposeMap();
    };
    GeoRegionNewComponent.prototype.clearRegion = function () {
        var region = {
            fir: "",
            geography: "",
            location: "",
            points: "",
            radius: 5,
            source: data_model_1.DoaRegionSource.None,
        };
        return region;
    };
    GeoRegionNewComponent.prototype.loadFirSources = function () {
        var _this = this;
        this.dataService.getAllFirs()
            .finally(function () {
            _this.selectedFirId = '-1';
        })
            .subscribe(function (firs) {
            _this.firSources = firs.map(function (r) {
                return {
                    id: r.id,
                    text: r.designator + " - " + r.name
                };
            });
            _this.firSources.unshift({ id: "-1", text: "" });
        });
    };
    GeoRegionNewComponent.prototype.loadGeoRegionSources = function () {
        this.geoRegionSources = [
            //{ id: '0', text: this.translation.translate('GeoRegionSession.GeoRegionSourceNone') },
            { id: '1', text: this.translation.translate('GeoRegionSession.GeoRegionSourceGeography') },
            { id: '2', text: this.translation.translate('GeoRegionSession.GeoRegionSourceFir') },
            { id: '3', text: this.translation.translate('GeoRegionSession.GeoRegionSourcePoints') },
            { id: '4', text: this.translation.translate('GeoRegionSession.GeoRegionSourcePointRadius') }
        ];
        if (this.isEdit)
            this.selectedSourceId = +this.geoRegion.region.source;
        else
            this.selectedSourceId = 1;
    };
    GeoRegionNewComponent.prototype.onFirChanged = function (data) {
        if (data.value) {
            this.geoRegion.region.fir = data.value;
            this.geoRegionRenderMap();
        }
        else {
            this.geoRegion.region.fir = '';
        }
        this.$ctrl("geoRegionArea").markAsDirty();
    };
    GeoRegionNewComponent.prototype.onSourceChanged = function (data) {
        if (data.value) {
            //this.geoRegion.region = this.clearRegion();
            var diss = this.geoRegionSources.find(function (x) { return x.id === String(data.value); });
            if (diss) {
                this.geoRegion.region.source = +diss.id;
                this.selectedSourceId = +diss.id;
                if (this.geoRegion.region.source === data_model_1.DoaRegionSource.PointAndRadius && this.geoRegion.region.radius === 0) {
                    this.geoRegion.region.radius = 5;
                    var radCtrl = this.$ctrl("geoRegionRadius");
                    radCtrl.setValue(this.geoRegion.region.radius);
                }
            }
            else {
                this.geoRegion.region.source = 1;
                this.selectedSourceId = 1;
            }
        }
        else {
            this.geoRegion.region.source = 1;
            this.selectedSourceId = 1;
        }
        this.$ctrl("geoRegionArea").markAsDirty();
    };
    GeoRegionNewComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.geoRegionForm = this.fb.group({
            geoRegionName: [{ value: this.model.name, disabled: false }, []],
            geoRegionLocation: [{ value: this.model.region.location, disabled: false }, []],
            geoRegionRadius: [{ value: this.model.region.radius, disabled: false }, []],
            geoRegionPoints: [{ value: this.model.region.points, disabled: false }, []],
            geoRegionArea: [{ value: this.model.region.geography, disabled: true }, []],
        });
        this.$ctrl("geoRegionRadius").setValidators(radiusValidatorFn(this.radiusLimit));
        this.$ctrl("geoRegionName").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateGeoRegionNameUnique(); });
        this.$ctrl("geoRegionLocation").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateRegionLocation(); });
        this.$ctrl("geoRegionPoints").valueChanges.debounceTime(1000).subscribe(function () { return _this.validatePointsLocation(); });
        this.$ctrl("geoRegionRadius").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateRegionLocation(); });
    };
    GeoRegionNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        this.prepareData();
        //Clean the extra info on the data
        var geoReg = { fir: '', geography: '', location: '', points: '', radius: 0, source: this.geoRegion.region.source };
        if (this.geoRegion.region.source === data_model_1.DoaRegionSource.Geography) {
            geoReg.geography = this.geoRegion.region.geography;
        }
        else if (this.geoRegion.region.source === data_model_1.DoaRegionSource.FIR) {
            geoReg.fir = this.geoRegion.region.fir;
        }
        else if (this.geoRegion.region.source === data_model_1.DoaRegionSource.Points) {
            geoReg.points = this.geoRegion.region.points;
        }
        else if (this.geoRegion.region.source === data_model_1.DoaRegionSource.PointAndRadius) {
            geoReg.location = this.geoRegion.region.location.toUpperCase();
            geoReg.radius = this.geoRegion.region.radius;
        }
        this.geoRegion.region = geoReg;
        //
        this.dataService.saveGeoRegion(this.geoRegion)
            .subscribe(function (data) {
            var msg = _this.translation.translate('GeoRegionSession.SuccessGeoRegionCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/georegion"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('GeoRegionSession.FailureGeoRegionCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    GeoRegionNewComponent.prototype.prepareData = function () {
        if (!this.isEdit)
            this.geoRegion.id = 0;
        this.geoRegion.name = this.geoRegionForm.get('geoRegionName').value;
        if (this.geoRegion.region.source === data_model_1.DoaRegionSource.PointAndRadius) {
            var radCtrl = this.$ctrl("geoRegionRadius");
            this.geoRegion.region.radius = +radCtrl.value;
        }
        else if (this.geoRegion.region.source === data_model_1.DoaRegionSource.Points) {
            var radCtrl = this.$ctrl("geoRegionRadius");
            this.geoRegion.region.radius = +radCtrl.value;
        }
    };
    GeoRegionNewComponent.prototype.disableSubmit = function () {
        if (this.geoRegionForm.pristine)
            return true;
        if (this.isValidating)
            return true;
        if (this.isSubmitting)
            return true;
        return !this.validateForm();
    };
    GeoRegionNewComponent.prototype.saveGeoRegionFile = function () {
        var _this = this;
        var winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            var fileUpload = $("#attachments").get(0);
            var files = fileUpload.files;
            // Create FormData object  
            var fileData = new FormData();
            fileData.append(files[0].name, files[0]);
            this.dataService.saveDoaFile(fileData)
                .finally(function () {
                $("#attachments").val('');
                if (!/safari/i.test(navigator.userAgent)) {
                    $("#attachments").type = '';
                    $("#attachments").type = 'file';
                }
            })
                .subscribe(function (data) {
                _this.geoRegion.region.geography = data;
                _this.geoRegion.region.source = data_model_1.DoaRegionSource.Geography;
                _this.$ctrl('geoRegionArea').setValue(_this.geoRegion.region.geography);
                var message = _this.translation.translate('GeoRegionSession.FileSuccess');
                _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                _this.geoRegionRenderMap();
            }, function (error) {
                _this.geoRegion.region.geography = "";
                _this.geoRegion.region.source = data_model_1.DoaRegionSource.None;
                _this.$ctrl('geoRegionArea').setValue(_this.geoRegion.region.geography);
                _this.toastr.error(_this.translation.translate('GeoRegionSession.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            this.geoRegion.region.geography = "";
            this.geoRegion.region.source = data_model_1.DoaRegionSource.None;
            this.toastr.error(this.translation.translate('GeoRegionSession.MessageSaveAttachmentBrowser1'), this.translation.translate('GeoRegionSession.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    GeoRegionNewComponent.prototype.backToGeoRegionList = function () {
        this.router.navigate(['/administration/georegion']);
    };
    GeoRegionNewComponent.prototype.validGeoRegionData = function () {
        if ((this.geoRegion.name.length > 3) &&
            (this.geoRegion.region.source > data_model_1.DoaRegionSource.None)) {
            if (this.geoRegion.region.source === data_model_1.DoaRegionSource.Geography && this.geoRegion.region.geography.length > 0)
                return true;
            else if (this.geoRegion.region.source === data_model_1.DoaRegionSource.FIR && this.geoRegion.region.fir !== '-1' && this.geoRegion.region.fir !== '')
                return true;
            else if (this.geoRegion.region.source === data_model_1.DoaRegionSource.PointAndRadius && this.geoRegion.region.location !== '' && isANumber(String(this.geoRegion.region.radius)) && this.geoRegion.region.radius > 0 && this.geoRegion.region.radius <= this.radiusLimit)
                return this.isValidLocation;
            else if (this.geoRegion.region.source === data_model_1.DoaRegionSource.Points && this.geoRegion.region.points !== '')
                return this.isValidLocation;
        }
        return false;
    };
    GeoRegionNewComponent.prototype.validGeoRegionName = function () {
        var dName = this.$ctrl('geoRegionName');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    GeoRegionNewComponent.prototype.validGeoRegionNameSize = function () {
        var dName = this.$ctrl('geoRegionName');
        if (dName.value) {
            if (dName.value.length > 3)
                return true;
        }
        return false;
    };
    GeoRegionNewComponent.prototype.validateGeoRegionNameUnique = function () {
        var _this = this;
        if (this.isValidating)
            return;
        if (this.validGeoRegionName() && this.validGeoRegionNameSize()) {
            var dName = this.$ctrl('geoRegionName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsGeoRegionName(this.geoRegion.id, dName.value)
                        .finally(function () {
                        _this.isValidating = false;
                    })
                        .subscribe(function (resp) {
                        _this.isGeoRegionUniqueName = !resp;
                    });
                }
            }
        }
    };
    GeoRegionNewComponent.prototype.validateRegionLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('geoRegionLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaLocation(dLocation.value)
                    .finally(function () {
                    _this.isValidating = false;
                })
                    .subscribe(function (resp) {
                    _this.isValidLocation = true;
                    _this.geoRegion.region.location = resp.toUpperCase();
                    _this.isValidating = false;
                    _this.validateForm();
                    _this.geoRegionRenderMap();
                }, function (error) {
                    _this.isValidLocation = false;
                    _this.geoRegion.region.location = '';
                });
            }
            else
                this.isValidLocation = false;
        }
        else
            this.isValidLocation = false;
    };
    GeoRegionNewComponent.prototype.validatePointsLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('geoRegionPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaPoints(dLocation.value)
                    .finally(function () {
                    _this.isValidating = false;
                })
                    .subscribe(function (resp) {
                    _this.isValidLocation = true;
                    _this.geoRegion.region.points = resp;
                    _this.isValidating = false;
                    _this.validateForm();
                    _this.geoRegionRenderMap();
                }, function (error) {
                    _this.isValidLocation = false;
                    _this.geoRegion.region.points = '';
                    _this.isValidating = false;
                });
            }
            else
                this.isValidLocation = false;
        }
        else
            this.isValidLocation = false;
    };
    GeoRegionNewComponent.prototype.validateRadius = function (ctrlName) {
        if (this.geoRegionForm.pristine)
            return true;
        var fc = this.geoRegionForm.get(ctrlName);
        if (fc.errors)
            return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit)
                return false;
            if (fc.value < 1)
                return false;
        }
        else
            return false;
        return true;
    };
    GeoRegionNewComponent.prototype.validateForm = function () {
        if (this.isValidating)
            return false;
        if (!this.geoRegionForm.pristine) {
            this.prepareData();
            if (this.validGeoRegionData()) {
                return this.isGeoRegionUniqueName;
            }
        }
        return false;
    };
    GeoRegionNewComponent.prototype.$ctrl = function (name) {
        return this.geoRegionForm.get(name);
    };
    GeoRegionNewComponent.prototype.decRadius = function () {
        var radCtrl = this.$ctrl("geoRegionRadius");
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    GeoRegionNewComponent.prototype.incRadius = function () {
        var radCtrl = this.$ctrl("geoRegionRadius");
        if (+radCtrl.value < this.radiusLimit) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    GeoRegionNewComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    //Map functions
    GeoRegionNewComponent.prototype.geoRegionRenderMap = function () {
        var reg = {
            region: this.geoRegion.region,
            changes: []
        };
        this.renderMap(reg);
    };
    GeoRegionNewComponent.prototype.initMap = function () {
        var winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;
        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    };
    GeoRegionNewComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.initMap();
                _this.mapHealthy = true;
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        }, function () { return _this.renderRegionInMap(_this.model.region.geoFeatures, "Doa"); });
    };
    GeoRegionNewComponent.prototype.mapAvailable = function () {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    };
    GeoRegionNewComponent.prototype.renderMap = function (reg) {
        var _this = this;
        this.dataService.getGeoFeatures(reg)
            .subscribe(function (resp) {
            var geoFeatures = resp;
            _this.renderRegionInMap(geoFeatures, "DoaModified");
        }, function (error) {
            _this.toastr.error(_this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), _this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        });
    };
    GeoRegionNewComponent.prototype.renderRegionInMap = function (geoFeatures, name) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        }
        else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    GeoRegionNewComponent.prototype.disposeMap = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    };
    GeoRegionNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/georegion/georegion-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], GeoRegionNewComponent);
    return GeoRegionNewComponent;
}());
exports.GeoRegionNewComponent = GeoRegionNewComponent;
function radiusValidatorFn(maxRadius) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=georegion-new.component.js.map