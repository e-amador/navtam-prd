﻿import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';
import { WindowRef } from '../../common/windowRef.service';

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'

import { DoaRegionSource, IGeoRegion, IDoaRegion, IFIR, ISelectOptions, IRegionLocationModel, IRegionMapRender } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/georegion/georegion-new.component.html'
})
export class GeoRegionNewComponent implements OnInit, AfterViewInit, OnDestroy {
    model: any = null;
    action: string;

    public geoRegionForm: FormGroup;

    isEdit: boolean = false;

    geoRegion: IGeoRegion;
    isGeoRegionUniqueName: boolean = true;
    isValidating: boolean = false;
    isValidLocation: boolean = true;
    isSubmitting: boolean = false;

    geoRegionSources: ISelectOptions[] = [];
    selectedSourceId?: number = 1;
    geoRegionSourceOptions: Select2Options;

    firSources: ISelectOptions[] = [];
    selectedFirId?: string = '-1';
    firSourceOptions: Select2Options;

    radiusLimit: number = 999;

    mapHealthy: boolean = false;
    leafletmap: any;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'edit') this.isEdit = true;
        else this.isEdit = false;
        if (!this.isEdit) {
            var region: IDoaRegion = {
                fir: "",
                geography: "",
                location: "",
                points: "",
                radius: 5,
                source: DoaRegionSource.None,
            };

            this.geoRegion = {
                id: 0,
                name: "",
                region: region,
                changes: null
            };
        } else {
            this.geoRegion = this.activatedRoute.snapshot.data['model'];
        }

        this.model = this.geoRegion;

        //Sources
        this.geoRegionSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('GeoRegionSession.SelectSource')
            },
            width: "100%"
        }
        this.loadGeoRegionSources();

        //FIRS
        this.firSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('GeoRegionSession.SelectFir')
            },
            width: "100%"
        }
        this.loadFirSources();

        this.configureReactiveForm();
    }

    ngAfterViewInit() {
        this.mapHealthCheck();        
    }

    ngOnDestroy() {
        this.disposeMap();
    }

    private clearRegion(): IDoaRegion {
        var region: IDoaRegion = {
            fir: "",
            geography: "",
            location: "",
            points: "",
            radius: 5,
            source: DoaRegionSource.None,
        };
        return region;
    }

    loadFirSources() {
        this.dataService.getAllFirs()
            .finally(() => {
                this.selectedFirId = '-1';
            })
            .subscribe((firs: IFIR[]) => {
                this.firSources = firs.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: `${r.designator} - ${r.name}`
                        };
                    });
                this.firSources.unshift({ id: "-1", text: "" });
            });
    }

    loadGeoRegionSources() {
        this.geoRegionSources = [
            //{ id: '0', text: this.translation.translate('GeoRegionSession.GeoRegionSourceNone') },
            { id: '1', text: this.translation.translate('GeoRegionSession.GeoRegionSourceGeography') },
            { id: '2', text: this.translation.translate('GeoRegionSession.GeoRegionSourceFir') },
            { id: '3', text: this.translation.translate('GeoRegionSession.GeoRegionSourcePoints') },
            { id: '4', text: this.translation.translate('GeoRegionSession.GeoRegionSourcePointRadius') }
        ];
        if (this.isEdit) this.selectedSourceId = +this.geoRegion.region.source;
        else this.selectedSourceId = 1;
    }

    public onFirChanged(data: { value: string }) {
        if (data.value) {
            this.geoRegion.region.fir = data.value;
            this.geoRegionRenderMap();
        } else {
            this.geoRegion.region.fir = '';
        }
        this.$ctrl("geoRegionArea").markAsDirty();
    }

    public onSourceChanged(data: { value: string }) {
        if (data.value) {
            //this.geoRegion.region = this.clearRegion();
            var diss = this.geoRegionSources.find(x => x.id === String(data.value));
            if (diss) {
                this.geoRegion.region.source = +diss.id;
                this.selectedSourceId = +diss.id;
                if (this.geoRegion.region.source === DoaRegionSource.PointAndRadius && this.geoRegion.region.radius === 0) {
                    this.geoRegion.region.radius = 5;
                    const radCtrl = this.$ctrl("geoRegionRadius");
                    radCtrl.setValue(this.geoRegion.region.radius);
                }
            }
            else {
                this.geoRegion.region.source = 1;
                this.selectedSourceId = 1;
            }

        } else {
            this.geoRegion.region.source = 1;
            this.selectedSourceId = 1;
        }
        this.$ctrl("geoRegionArea").markAsDirty();
    }

    private configureReactiveForm() {
        this.geoRegionForm = this.fb.group({
            geoRegionName: [{ value: this.model.name, disabled: false }, []],
            geoRegionLocation: [{ value: this.model.region.location, disabled: false }, []],
            geoRegionRadius: [{ value: this.model.region.radius, disabled: false }, []],
            geoRegionPoints: [{ value: this.model.region.points, disabled: false }, []],
            geoRegionArea: [{ value: this.model.region.geography, disabled: true }, []],
        });
        this.$ctrl("geoRegionRadius").setValidators(radiusValidatorFn(this.radiusLimit));

        this.$ctrl("geoRegionName").valueChanges.debounceTime(1000).subscribe(() => this.validateGeoRegionNameUnique());
        this.$ctrl("geoRegionLocation").valueChanges.debounceTime(1000).subscribe(() => this.validateRegionLocation());
        this.$ctrl("geoRegionPoints").valueChanges.debounceTime(1000).subscribe(() => this.validatePointsLocation());

        this.$ctrl("geoRegionRadius").valueChanges.debounceTime(1000).subscribe(() => this.validateRegionLocation());
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        this.prepareData();
        //Clean the extra info on the data
        var geoReg: IDoaRegion = { fir: '', geography: '', location: '', points: '', radius: 0, source: this.geoRegion.region.source };
        if (this.geoRegion.region.source === DoaRegionSource.Geography) {
            geoReg.geography = this.geoRegion.region.geography;
        } else if (this.geoRegion.region.source === DoaRegionSource.FIR) {
            geoReg.fir = this.geoRegion.region.fir;
        } else if (this.geoRegion.region.source === DoaRegionSource.Points) {
            geoReg.points = this.geoRegion.region.points;
        } else if (this.geoRegion.region.source === DoaRegionSource.PointAndRadius) {
            geoReg.location = this.geoRegion.region.location.toUpperCase();
            geoReg.radius = this.geoRegion.region.radius;
        }
        this.geoRegion.region = geoReg;
        //

        this.dataService.saveGeoRegion(this.geoRegion)
            .subscribe(data => {
                let msg = this.translation.translate('GeoRegionSession.SuccessGeoRegionCreated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/georegion"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('GeoRegionSession.FailureGeoRegionCreated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private prepareData() {
        if (!this.isEdit) this.geoRegion.id = 0;
        this.geoRegion.name = this.geoRegionForm.get('geoRegionName').value;
        if (this.geoRegion.region.source === DoaRegionSource.PointAndRadius) {
            const radCtrl = this.$ctrl("geoRegionRadius");
            this.geoRegion.region.radius = +radCtrl.value;
        } else if (this.geoRegion.region.source === DoaRegionSource.Points) {
            const radCtrl = this.$ctrl("geoRegionRadius");
            this.geoRegion.region.radius = +radCtrl.value;
        }
    }

    disableSubmit(): boolean {
        if (this.geoRegionForm.pristine) return true;
        if (this.isValidating) return true;
        if (this.isSubmitting) return true;
        return !this.validateForm()
    }

    public saveGeoRegionFile() {
        const winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            const fileUpload = $("#attachments").get(0);
            const files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);

            this.dataService.saveDoaFile(fileData)
                .finally(() => {
                    $("#attachments").val('');
                    if (!/safari/i.test(navigator.userAgent)) {
                        $("#attachments").type = '';
                        $("#attachments").type = 'file';
                    }
                })
                .subscribe(data => {
                    this.geoRegion.region.geography = data;
                    this.geoRegion.region.source = DoaRegionSource.Geography;
                    this.$ctrl('geoRegionArea').setValue(this.geoRegion.region.geography);

                    const message = this.translation.translate('GeoRegionSession.FileSuccess');
                    this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                    this.geoRegionRenderMap();
                }, error => {
                    this.geoRegion.region.geography = "";
                    this.geoRegion.region.source = DoaRegionSource.None;
                    this.$ctrl('geoRegionArea').setValue(this.geoRegion.region.geography);
                    this.toastr.error(this.translation.translate('GeoRegionSession.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            this.geoRegion.region.geography = "";
            this.geoRegion.region.source = DoaRegionSource.None;
            this.toastr.error(this.translation.translate('GeoRegionSession.MessageSaveAttachmentBrowser1'), this.translation.translate('GeoRegionSession.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    public backToGeoRegionList() {
        this.router.navigate(['/administration/georegion']);
    }

    private validGeoRegionData(): boolean {
        if ((this.geoRegion.name.length > 3) &&
            (this.geoRegion.region.source > DoaRegionSource.None)) {
            if (this.geoRegion.region.source === DoaRegionSource.Geography && this.geoRegion.region.geography.length > 0)
                return true;
            else if (this.geoRegion.region.source === DoaRegionSource.FIR && this.geoRegion.region.fir !== '-1' && this.geoRegion.region.fir !== '')
                return true;
            else if (this.geoRegion.region.source === DoaRegionSource.PointAndRadius && this.geoRegion.region.location !== '' && isANumber(String(this.geoRegion.region.radius)) && this.geoRegion.region.radius > 0 && this.geoRegion.region.radius <= this.radiusLimit)
                return this.isValidLocation;
            else if (this.geoRegion.region.source === DoaRegionSource.Points && this.geoRegion.region.points !== '')
                return this.isValidLocation;
        }
        return false;
    }

    public validGeoRegionName(): boolean {
        const dName = this.$ctrl('geoRegionName');
        if (dName.value) {
            if (dName.value.length > 0) return true;
        }
        return false;
    }

    public validGeoRegionNameSize(): boolean {
        const dName = this.$ctrl('geoRegionName');
        if (dName.value) {
            if (dName.value.length > 3) return true;
        }
        return false;
    }

    public validateGeoRegionNameUnique() {
        if (this.isValidating) return;
        if (this.validGeoRegionName() && this.validGeoRegionNameSize()) {
            const dName = this.$ctrl('geoRegionName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsGeoRegionName(this.geoRegion.id, dName.value)
                        .finally(() => {
                            this.isValidating = false;
                        })
                        .subscribe((resp: boolean) => {
                            this.isGeoRegionUniqueName = !resp;
                        });
                }
            }
        }
    }

    public validateRegionLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('geoRegionLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaLocation(dLocation.value)
                    .finally(() => {
                        this.isValidating = false;
                    })
                    .subscribe((resp: string) => {
                        this.isValidLocation = true;
                        this.geoRegion.region.location = resp.toUpperCase();
                        this.isValidating = false;
                        this.validateForm();
                        this.geoRegionRenderMap();
                    }, error => {
                        this.isValidLocation = false;
                        this.geoRegion.region.location = '';
                    });
            } else this.isValidLocation = false;
        } else this.isValidLocation = false;
    }

    public validatePointsLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('geoRegionPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaPoints(dLocation.value)
                    .finally(() => {
                        this.isValidating = false;
                    })
                    .subscribe((resp: string) => {
                        this.isValidLocation = true;
                        this.geoRegion.region.points = resp;
                        this.isValidating = false;
                        this.validateForm();
                        this.geoRegionRenderMap();
                    }, error => {
                        this.isValidLocation = false;
                        this.geoRegion.region.points = '';
                        this.isValidating = false;
                    });
            } else this.isValidLocation = false;
        } else this.isValidLocation = false;
    }

    public validateRadius(ctrlName: string) {
        if (this.geoRegionForm.pristine) return true;
        const fc = this.geoRegionForm.get(ctrlName);
        if (fc.errors) return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit) return false;
            if (fc.value < 1) return false;
        } else return false;
        return true;
    }

    public validateForm(): boolean {
        if (this.isValidating) return false;
        if (!this.geoRegionForm.pristine) {
            this.prepareData();
            if (this.validGeoRegionData()) {
                return this.isGeoRegionUniqueName;
            }
        }
        return false;
    }

    $ctrl(name: string): AbstractControl {
        return this.geoRegionForm.get(name);
    }

    decRadius() {
        const radCtrl = this.$ctrl("geoRegionRadius");
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incRadius() {
        const radCtrl = this.$ctrl("geoRegionRadius");
        if (+radCtrl.value < this.radiusLimit) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    //Map functions
    public geoRegionRenderMap() {
        let reg: IRegionMapRender = {
            region: this.geoRegion.region,
            changes: []
        };
        this.renderMap(reg);
    }

    private initMap() {
        const winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;

        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.initMap();
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        }, () => this.renderRegionInMap(this.model.region.geoFeatures, "Doa")
        );
    }

    public mapAvailable() {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    }

    public renderMap(reg: IRegionMapRender) {
        this.dataService.getGeoFeatures(reg)
            .subscribe((resp: string) => {
                let geoFeatures = resp;
                this.renderRegionInMap(geoFeatures, "DoaModified");
            }, error => {
                this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
            });
    }

    private renderRegionInMap(geoFeatures: string, name: string) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        } else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    private disposeMap() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    }
    //End Map functions
}

function radiusValidatorFn(maxRadius: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}
