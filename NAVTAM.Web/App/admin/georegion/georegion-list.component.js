"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeoRegionListComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var GeoRegionListComponent = /** @class */ (function () {
    function GeoRegionListComponent(toastr, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.canbeDeleted = false;
        this.selectedGeoRegion = null;
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
        this.tableHeaders = {
            sorting: [
                { columnName: 'Name', direction: 0 },
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "georegions",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName //Default Received Descending
        };
    }
    ;
    GeoRegionListComponent.prototype.ngOnInit = function () {
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "georegions") {
            this.queryOptions = queryOptions;
        }
        this.getGeoRegionsInPage(this.queryOptions.page);
    };
    Object.defineProperty(GeoRegionListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    GeoRegionListComponent.prototype.selectRegionAllocation = function (region) {
        if (this.selectedGeoRegion) {
            this.selectedGeoRegion.isSelected = false;
        }
        this.canbeDeleted = true;
        this.selectedGeoRegion = region;
        this.selectedGeoRegion.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedGeoRegion.id);
    };
    GeoRegionListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getGeoRegionsInPage(1);
    };
    GeoRegionListComponent.prototype.onPageChange = function (page) {
        this.getGeoRegionsInPage(page);
    };
    GeoRegionListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getGeoRegionsInPage(this.queryOptions.page);
    };
    GeoRegionListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    GeoRegionListComponent.prototype.getGeoRegionsInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredGeoRegions(this.queryOptions)
                .subscribe(function (geosData) {
                _this.processGeoRegions(geosData);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    GeoRegionListComponent.prototype.processGeoRegions = function (geosData) {
        this.paging = geosData.paging;
        this.totalPages = geosData.paging.totalPages;
        for (var iter = 0; iter < geosData.data.length; iter++)
            geosData.data[iter].index = iter;
        var lastGeoRegionIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (geosData.data.length > 0) {
            this.geoRegions = geosData.data;
            var index = 0;
            if (lastGeoRegionIdSelected) {
                index = this.geoRegions.findIndex(function (x) { return x.id === lastGeoRegionIdSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.geoRegions[index].isSelected = true;
            this.selectedGeoRegion = this.geoRegions[index];
            this.canbeDeleted = true;
            this.loadingData = false;
        }
        else {
            this.geoRegions = [];
            this.selectedGeoRegion = null;
            this.loadingData = false;
        }
    };
    GeoRegionListComponent.prototype.confirmDeleteGeo = function () {
        $('#modal-delete').modal({});
    };
    GeoRegionListComponent.prototype.deleteGeoRegion = function () {
        var _this = this;
        if (this.selectedGeoRegion) {
            this.dataService.deleteGeoRegion(this.selectedGeoRegion.id)
                .subscribe(function (result) {
                _this.getGeoRegionsInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('GeoRegionSession.SuccessGeoDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('GeoRegionSession.FailureGeoDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    GeoRegionListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/georegion/georegion-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], GeoRegionListComponent);
    return GeoRegionListComponent;
}());
exports.GeoRegionListComponent = GeoRegionListComponent;
//# sourceMappingURL=georegion-list.component.js.map