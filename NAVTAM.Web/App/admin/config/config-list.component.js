"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigListComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var ConfigListComponent = /** @class */ (function () {
    function ConfigListComponent(toastr, fb, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.canbeDeleted = false;
        this.selectedConfig = null;
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
        this.tableHeaders = {
            sorting: [
                { columnName: 'Name', direction: 0 },
                { columnName: 'Description', direction: 0 },
                { columnName: 'Value', direction: 0 },
                { columnName: 'Category', direction: 0 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "configRec",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName,
            filterValue: ""
        };
    }
    ;
    ConfigListComponent.prototype.ngOnInit = function () {
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "configRec") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getConfigValuesInPage(this.queryOptions.page);
    };
    ConfigListComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.configListForm = this.fb.group({
            searchConfig: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchConfig").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateConfigList(); });
    };
    Object.defineProperty(ConfigListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    ConfigListComponent.prototype.updateConfigList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchConfig');
        this.queryOptions.filterValue = subject.value;
        this.getConfigValuesInPage(this.queryOptions.page);
    };
    ConfigListComponent.prototype.clearSearch = function () {
        if (this.loadingData)
            return;
        this.$ctrl('searchConfig').setValue('');
    };
    ConfigListComponent.prototype.$ctrl = function (name) {
        return this.configListForm.get(name);
    };
    ConfigListComponent.prototype.selectConfigRecord = function (config) {
        if (this.selectedConfig) {
            this.selectedConfig.isSelected = false;
        }
        this.canbeDeleted = true;
        this.selectedConfig = config;
        this.selectedConfig.isSelected = true;
        var pair = {
            category: this.selectedConfig.category,
            name: this.selectedConfig.name
        };
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, pair);
    };
    ConfigListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getConfigValuesInPage(1);
    };
    ConfigListComponent.prototype.onPageChange = function (page) {
        this.getConfigValuesInPage(page);
    };
    ConfigListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getConfigValuesInPage(this.queryOptions.page);
    };
    ConfigListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    ConfigListComponent.prototype.getConfigValuesInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredConfigValues(this.queryOptions)
                .subscribe(function (configData) {
                _this.processConfigValues(configData);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    ConfigListComponent.prototype.processConfigValues = function (configData) {
        this.paging = configData.paging;
        this.totalPages = configData.paging.totalPages;
        for (var iter = 0; iter < configData.data.length; iter++)
            configData.data[iter].index = iter;
        var pair = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (configData.data.length > 0) {
            this.configRecs = configData.data;
            var index = 0;
            if (pair) {
                index = this.configRecs.findIndex(function (x) { return x.category === pair.category && x.name === pair.name; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.configRecs[index].isSelected = true;
            this.selectedConfig = this.configRecs[index];
            this.canbeDeleted = true;
            this.loadingData = false;
        }
        else {
            this.configRecs = [];
            this.selectedConfig = null;
            this.loadingData = false;
        }
    };
    ConfigListComponent.prototype.confirmDeleteConfig = function () {
        $('#modal-delete').modal({});
    };
    ConfigListComponent.prototype.deleteConfigRec = function () {
        var _this = this;
        if (this.selectedConfig) {
            this.dataService.deleteConfig(this.selectedConfig.category, this.selectedConfig.name)
                .subscribe(function (result) {
                _this.getConfigValuesInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('ConfigurationSession.SuccessConfigDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('ConfigurationSession.FailureConfigDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    ConfigListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/config/config-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], ConfigListComponent);
    return ConfigListComponent;
}());
exports.ConfigListComponent = ConfigListComponent;
//# sourceMappingURL=config-list.component.js.map