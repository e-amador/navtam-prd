﻿import { Component, OnInit, Inject } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { ActivatedRoute, Router } from '@angular/router'

import { Select2OptionData } from 'ng2-select2';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import { LocaleService, TranslationService } from 'angular-l10n';

import {
    IQueryOptions,
    IPaging,
    IConfigValuePartial,
    IConfigValuePair,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

@Component({
    templateUrl: '/app/admin/config/config-list.component.html'
})
export class ConfigListComponent implements OnInit {
    queryOptions: IQueryOptions;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;
    canbeDeleted: boolean = false;

    configRecs: IConfigValuePartial[];
    selectedConfig: IConfigValuePartial = null;

    configListForm: FormGroup;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);

        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'Name', direction: 0 },
                    { columnName: 'Description', direction: 0 },
                    { columnName: 'Value', direction: 0 },
                    { columnName: 'Category', direction: 0 }
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '200', text: '200' },
                ]
        };

        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "configRec",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName, //Default Received Descending
            filterValue: ""
        }
    };

    ngOnInit() {
        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "configRec") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getConfigValuesInPage(this.queryOptions.page);
    }

    private configureReactiveForm() {
        this.configListForm = this.fb.group({
            searchConfig: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchConfig").valueChanges.debounceTime(1000).subscribe(() => this.updateConfigList());
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    updateConfigList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchConfig');
        this.queryOptions.filterValue = subject.value;
        this.getConfigValuesInPage(this.queryOptions.page);
    }

    clearSearch(): void {
        if (this.loadingData) return;
        this.$ctrl('searchConfig').setValue('');
    }

    $ctrl(name: string): AbstractControl {
        return this.configListForm.get(name);
    }

    selectConfigRecord(config: IConfigValuePartial) {
        if (this.selectedConfig) {
            this.selectedConfig.isSelected = false;
        }

        this.canbeDeleted = true;
        this.selectedConfig = config;
        this.selectedConfig.isSelected = true;
        var pair : IConfigValuePair = {
            category: this.selectedConfig.category,
            name: this.selectedConfig.name
        }
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, pair);
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getConfigValuesInPage(1);
    }

    onPageChange(page: number) {
        this.getConfigValuesInPage(page);
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getConfigValuesInPage(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    private getConfigValuesInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredConfigValues(this.queryOptions)
                .subscribe((configData: any) => {
                    this.processConfigValues(configData);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processConfigValues(configData: any) {
        this.paging = configData.paging;
        this.totalPages = configData.paging.totalPages;

        for (var iter = 0; iter < configData.data.length; iter++) configData.data[iter].index = iter;

        const pair : IConfigValuePair = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (configData.data.length > 0) {
            this.configRecs = configData.data;
            let index = 0;
            if (pair) {
                index = this.configRecs.findIndex(x => x.category === pair.category && x.name === pair.name);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.configRecs[index].isSelected = true;
            this.selectedConfig = this.configRecs[index];
            this.canbeDeleted = true;
            this.loadingData = false;
        } else {
            this.configRecs = [];
            this.selectedConfig = null;
            this.loadingData = false;
        }
    }

    confirmDeleteConfig() {
        $('#modal-delete').modal({});
    }

    deleteConfigRec() {
        if (this.selectedConfig) {
            this.dataService.deleteConfig(this.selectedConfig.category, this.selectedConfig.name)
                .subscribe(result => {
                    this.getConfigValuesInPage(this.paging.currentPage);
                    let msg = this.translation.translate('ConfigurationSession.SuccessConfigDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('ConfigurationSession.FailureConfigDeleted');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }


}