﻿import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';
import { WindowRef } from '../../common/windowRef.service';

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'

import {
    IConfigValuePartial,
    IConfigValuePair,
 } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/config/config-new.component.html'
})
export class ConfigNewComponent implements OnInit {

    model: any = null;
    action: string;

    public configListForm: FormGroup;

    isReadOnly: boolean = false;

    configRec: IConfigValuePartial;
    isConfigUniqueName: boolean = true;
    isValidating: boolean = false;
    isValidLocation: boolean = true;
    isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'create') {
            this.configRec = {
                name: "",
                description: "",
                value: "",
                category: "NOTAM",
                action: ""
            };
        } else {//if (this.action === 'copy') {
            this.isReadOnly = true;
            this.configRec = this.activatedRoute.snapshot.data['model'];
        }
        this.configRec.action = this.action;
        this.model = this.configRec;
        this.configureReactiveForm();
    }

    ngOnDestroy() {
    }

    private configureReactiveForm() {
        this.configListForm = this.fb.group({
            configName: [{ value: this.model.name, disabled: this.isReadOnly }, []],
            configDescription: [{ value: this.model.description, disabled: false }, []],
            configValue: [{ value: this.model.value, disabled: false }, [Validators.required]],
            configCategory: [{ value: this.model.category, disabled: false }, []],
        });

        if (!this.isReadOnly) {
            this.$ctrl("configName").valueChanges.debounceTime(1000).subscribe(() => this.validateConfigNameUnique());
        }
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        this.prepareData();
        this.dataService.saveConfig(this.configRec)
            .subscribe(data => {
                let msg = this.translation.translate('ConfigurationSession.SuccessConfigCreated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/config"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('ConfigurationSession.FailureConfigCreated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private prepareData() {
        this.configRec.name = this.$ctrl('configName').value;
        this.configRec.description = this.$ctrl('configDescription').value;
        this.configRec.category = this.$ctrl('configCategory').value;
        this.configRec.value = this.$ctrl('configValue').value;
    }

    disableSubmit(): boolean {
        if (this.configListForm.pristine) return true;
        if (this.isValidating) return true;
        if (this.isSubmitting) return true;
        return !this.validateForm()
    }

    public backToConfigList() {
        this.router.navigate(['/administration/config']);
    }

    private validConfigData(): boolean {
        if (this.configRec.name.length > 3 &&
            this.configRec.description.length > 3 &&
            this.configRec.value.length > 0 &&
            this.configRec.category.length > 2) {
            return true;
        }
        return false;
    }

    public validConfigName(): boolean {
        if (this.configListForm.pristine) return true;
        const dName = this.$ctrl('configName');
        if (dName.value) {
            if (dName.value.length > 0) return true;
        }
        return false;
    }

    public validConfigDescription(): boolean {
        if (this.configListForm.pristine) return true;
        const dName = this.$ctrl('configDescription');
        if (dName.value) {
            if (dName.value.length > 0) return true;
        }
        return false;
    }

    public validConfigValue(): boolean {
        if (this.configListForm.pristine) return true;
        const dName = this.$ctrl('configValue');
        if (dName.value.length > 0) return true;
        return false;
    }

    public validConfigAftnValue(): boolean {
        if (this.configListForm.pristine) return true;
        const dName = this.$ctrl('configValue');
        const configName = this.$ctrl('configName');
        if (configName.value === 'AFTN_DisseminationDelay') {
            if (Number.isInteger(+dName.value)) {
                let value = parseInt(dName.value);
                return value >= 1000;
            } else {
                return false;
            }
        }
        return true;
    }

    public validConfigSwimValue(): boolean {
        if (this.configListForm.pristine) return true;
        const dName = this.$ctrl('configValue');
        const configName = this.$ctrl('configName');
        if (configName.value === 'SWIM_DisseminationDelay') {
            if (Number.isInteger(+dName.value)) {
                let value = parseInt(dName.value);
                return value >= 100;
            } else {
                return false;
            }
        }
        return true;
    }

    public validConfigCategory(): boolean {
        if (this.configListForm.pristine) return true;
        const dName = this.$ctrl('configCategory');
        if (dName.value) {
            if (dName.value.length > 0) return true;
        }
        return false;
    }

    public validConfigNameSize(): boolean {
        if (this.configListForm.pristine) return true;
        if (!this.validConfigName()) return true;
        const dName = this.$ctrl('configName');
        if (dName.value) {
            if (dName.value.length > 3) return true;
        }
        return false;
    }

    public validConfigDescritionSize(): boolean {
        if (this.configListForm.pristine) return true;
        if (!this.validConfigDescription()) return true;
        const dName = this.$ctrl('configDescription');
        if (dName.value) {
            if (dName.value.length > 3) return true;
        }
        return false;
    }

    public validConfigCategorySize(): boolean {
        if (this.configListForm.pristine) return true;
        if (!this.validConfigCategory()) return true;
        const dName = this.$ctrl('configCategory');
        if (dName.value) {
            if (dName.value.length > 2) return true;
        }
        return false;
    }

    public validateConfigNameUnique() {
        if (this.configListForm.pristine) return true;
        if (this.isValidating && this.action !== 'create') return;
        if (this.validConfigName() && this.validConfigNameSize()) {
            const dName = this.$ctrl('configName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsConfigName(dName.value)
                        .finally(() => {
                            this.isValidating = false;
                        })
                        .subscribe((resp: boolean) => {
                            this.isConfigUniqueName = !resp;
                        });
                }
            }
        }
    }

    public validateForm(): boolean {
        if (this.isValidating) return false;
        if (!this.validConfigAftnValue()) {
            return false;
        }
        if (!this.validConfigSwimValue()) {
            return false;
        }
        if (!this.configListForm.pristine) {
            this.prepareData();
            if (this.validConfigData()) {
                return this.isConfigUniqueName;
            }
        }
        return false;
    }

    $ctrl(name: string): AbstractControl {
        return this.configListForm.get(name);
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}
