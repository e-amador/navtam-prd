"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ng2_select2_1 = require("ng2-select2");
var ng2_date_time_picker_1 = require("../common/components/ng2-date-time-picker");
var startup_service_1 = require("./startup.service");
var ng2_signalr_1 = require("ng2-signalr");
var angular_l10n_1 = require("angular-l10n");
var index_1 = require("../common/index");
var main_component_1 = require("./main.component");
var index_2 = require("./index");
var routes_1 = require("./routes");
//declare let $: Object;
function startupServiceFactory(startupService) {
    return function () { return startupService.load(); };
}
exports.startupServiceFactory = startupServiceFactory;
function createConfig() {
    var c = new ng2_signalr_1.SignalRConfiguration();
    c.hubName = 'SignalRHub';
    //c.qs = { user: 'user.subscribed' };
    c.url = window.location.origin;
    c.logging = true;
    return c;
}
exports.createConfig = createConfig;
var LocalizationConfig = (function () {
    function LocalizationConfig(locale, translation) {
        this.locale = locale;
        this.translation = translation;
    }
    LocalizationConfig.prototype.load = function () {
        var _this = this;
        this.locale.addConfiguration()
            .addLanguage('en', 'ltr')
            .addLanguage('fr', 'ltr')
            .defineLanguage(window['app'].cultureInfo)
            .defineCurrency("CAD")
            .defineDefaultLocale('en', 'CA');
        this.locale.init();
        this.translation.addConfiguration()
            .addProvider('./app/resources/locale-');
        var promise = new Promise(function (resolve) {
            _this.translation.translationChanged.subscribe(function () {
                resolve(true);
            });
        });
        this.translation.init();
        return promise;
    };
    return LocalizationConfig;
}());
LocalizationConfig = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [angular_l10n_1.LocaleService, angular_l10n_1.TranslationService])
], LocalizationConfig);
exports.LocalizationConfig = LocalizationConfig;
// AoT compilation requires a reference to an exported function.
function initLocalization(localizationConfig) {
    return function () { return localizationConfig.load(); };
}
exports.initLocalization = initLocalization;
var UserModule = (function () {
    function UserModule() {
    }
    return UserModule;
}());
UserModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            router_1.RouterModule.forRoot(routes_1.routes),
            ng2_select2_1.Select2Module,
            ng2_date_time_picker_1.DateTimePickerModule,
            index_1.Ng2ICheckModule,
            angular_l10n_1.LocalizationModule.forRoot() // New instance of TranslationService
        ],
        declarations: [
            main_component_1.MainComponent,
            //common
            index_1.DatexPipe,
            index_1.DateTimeFormatDirective,
            index_1.ModalTriggerDirective,
            index_1.SimpleModalComponent,
            index_1.ServerPaginationComponent,
            //User,
            index_2.UserListComponent,
        ],
        providers: [
            index_2.DataService,
            startup_service_1.StartupService,
            LocalizationConfig,
            {
                provide: core_1.APP_INITIALIZER,
                useFactory: initLocalization,
                deps: [LocalizationConfig],
                multi: true
            },
            //common
            {
                // Provider for APP_INITIALIZER
                provide: core_1.APP_INITIALIZER,
                useFactory: startupServiceFactory,
                deps: [startup_service_1.StartupService],
                multi: true
            },
            { provide: index_1.TOASTR_TOKEN, useValue: toastr },
            //{ provide: JQUERY_TOKEN, useValue: $ }, 
            { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
            index_1.BearerTokenService,
            index_1.WindowRef
        ],
        bootstrap: [main_component_1.MainComponent]
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=module.js.map