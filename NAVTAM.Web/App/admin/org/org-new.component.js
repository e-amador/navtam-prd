"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrgNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var phone_validator_1 = require("../shared/phone.validator");
var data_service_1 = require("../shared/data.service");
var OrgNewComponent = /** @class */ (function () {
    function OrgNewComponent(toastr, fb, dataService, memStorageService, router, activatedRoute, winRef, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isOrgNameUnique = true;
        this.isValidatingOrg = false;
        this.isAdminNameUnique = true;
        this.isValidatingAdmin = false;
        this.isSubmitting = false;
        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    OrgNewComponent.prototype.ngOnInit = function () {
        this.orgTypeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: false,
            width: "100%"
        };
        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: true,
            width: "100%"
        };
        this.nsdOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectNsds')
            },
            multiple: true,
            width: "100%"
        };
        this.orgTypes = [
            {
                id: '0',
                text: this.translation.translate('OrgSession.OrgTypeExternal')
            },
            {
                id: '1',
                text: this.translation.translate('OrgSession.OrgTypeFic')
            },
            {
                id: '2',
                text: this.translation.translate('OrgSession.OrgTypeNof')
            },
            {
                id: '3',
                text: this.translation.translate('OrgSession.OrgTypeAdministration')
            }
        ];
        this.model = this.prepareEmptyOrg();
        this.configureReactiveForm();
        this.loadDoas();
        this.loadNsds();
    };
    OrgNewComponent.prototype.onOrgTypeChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('orgType').setValue(data.value);
        }
        else {
            this.orgForm.get('orgType').setValue("");
        }
    };
    OrgNewComponent.prototype.onDoaChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('doas').setValue(data.value.join(','));
        }
        else {
            this.orgForm.get('doas').setValue("");
        }
    };
    OrgNewComponent.prototype.onNsdChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('nsds').setValue(data.value.join(','));
        }
        else {
            this.orgForm.get('nsds').setValue("");
        }
    };
    OrgNewComponent.prototype.backToOrgList = function () {
        this.router.navigate(['/administration/orgs']);
    };
    OrgNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var org = this.prepareSaveOrg();
        this.dataService.saveOrg(org)
            .subscribe(function (data) {
            var msg = _this.translation.translate('OrgSession.SuccessOrgSaved');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/orgs"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('OrgSession.FailureOrgSaved');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    OrgNewComponent.prototype.validateName = function () {
        var usernameCtrl = this.orgForm.get("name");
        return usernameCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateEmailAddress = function () {
        var emailCtrl = this.orgForm.get("emailAddress");
        return emailCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateAddress = function () {
        var emailCtrl = this.orgForm.get("address");
        return emailCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateTelephone = function () {
        var emailCtrl = this.orgForm.get("telephone");
        return emailCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateTypeOfOperation = function () {
        var typeOfOperationCtrl = this.orgForm.get("typeOfOperation");
        return typeOfOperationCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateDoas = function () {
        var fc = this.orgForm.get('doas');
        return fc.value || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateNsds = function () {
        var fc = this.orgForm.get('nsds');
        return fc.value || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateUsername = function () {
        var usernameCtrl = this.orgForm.get("username");
        return usernameCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateFirstName = function () {
        var firstNameCtrl = this.orgForm.get("firstName");
        return firstNameCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.validateLastName = function () {
        var lastNameCtrl = this.orgForm.get("lastName");
        return lastNameCtrl.valid || this.orgForm.pristine;
    };
    OrgNewComponent.prototype.disableSubmit = function () {
        if (this.orgForm.pristine || !this.orgForm.valid)
            return true;
        if (this.isValidatingOrg)
            return true;
        if (this.isSubmitting)
            return true;
        return !this.isOrgNameUnique || !this.isAdminNameUnique;
    };
    OrgNewComponent.prototype.loadDoas = function () {
        var _this = this;
        this.dataService.getDoas()
            .subscribe(function (doa) {
            _this.doas = doa.map(function (r) {
                return {
                    id: r.id,
                    text: r.name
                };
            });
            _this.selectedDoaId = _this.model.assignedDoaId || -1;
        });
    };
    OrgNewComponent.prototype.loadNsds = function () {
        var _this = this;
        this.dataService.getNsdLeaves()
            .subscribe(function (nsds) {
            _this.nsds = nsds.map(function (r) {
                return {
                    id: r.id,
                    text: r.name
                };
            });
            _this.selectedDoaId = -1;
        });
    };
    OrgNewComponent.prototype.validateOrgNameUnique = function () {
        var _this = this;
        if (this.isValidatingOrg)
            return;
        if (this.validateName()) {
            var orgName = this.$ctrl('name');
            if (orgName.value) {
                if (orgName.value.length >= 3) {
                    this.isValidatingOrg = true;
                    this.dataService.isOrgNameUnique(0, orgName.value)
                        .finally(function () {
                        _this.isValidatingOrg = false;
                    })
                        .subscribe(function (resp) {
                        _this.isOrgNameUnique = resp;
                    });
                }
            }
        }
    };
    OrgNewComponent.prototype.validateOrgAdminNameUnique = function () {
        var _this = this;
        if (this.isValidatingAdmin)
            return;
        if (this.validateUsername()) {
            var adminName = this.$ctrl('username');
            if (adminName.value) {
                this.isValidatingAdmin = true;
                this.dataService.isOrgAdminNameUnique(adminName.value)
                    .finally(function () {
                    _this.isValidatingAdmin = false;
                })
                    .subscribe(function (resp) {
                    _this.isAdminNameUnique = resp;
                });
            }
        }
    };
    OrgNewComponent.prototype.$ctrl = function (name) {
        return this.orgForm.get(name);
    };
    OrgNewComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.orgForm = this.fb.group({
            // organization
            name: [{ value: this.model.name, disabled: false }, [forms_1.Validators.required, forms_1.Validators.minLength(3), forms_1.Validators.maxLength(100), forms_1.Validators.pattern('[a-zA-Z].*')]],
            address: [{ value: this.model.address, disabled: false }, [forms_1.Validators.required]],
            emailAddress: [{ value: this.model.emailAddress, disabled: false }, [forms_1.Validators.required, forms_1.Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
            headOffice: [{ value: this.model.headOffice, disabled: false }, []],
            typeOfOperation: [{ value: this.model.typeOfOperation, disabled: false }, [forms_1.Validators.required]],
            emailDistribution: [{ value: this.model.emailDistribution, disabled: false }, []],
            doas: [{ value: this.model.assignedDoaIds || -1, disabled: false }, [doasValidator]],
            nsds: [{ value: this.model.assignedNsdIds || -1, disabled: false }, [nsdsValidator]],
            telephone: [{ value: this.model.telephone, disabled: false }, [phoneNumberValidator]],
            orgType: { value: this.model.type, disabled: false },
            // administrator
            username: [{ value: this.model.username, disabled: false }, [forms_1.Validators.required, forms_1.Validators.minLength(3), forms_1.Validators.maxLength(16), forms_1.Validators.pattern('[a-zA-Z].*')]],
            firstName: [{ value: this.model.firstName, disabled: false }, [forms_1.Validators.required]],
            lastName: [{ value: this.model.lastName, disabled: false }, [forms_1.Validators.required]],
        });
        this.$ctrl("name").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateOrgNameUnique(); });
        this.$ctrl("username").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateOrgAdminNameUnique(); });
    };
    OrgNewComponent.prototype.phoneNumberFocus = function () {
        var phoneCtrl = this.$ctrl("telephone");
        var value = phone_validator_1.PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    OrgNewComponent.prototype.phoneNumberBlur = function () {
        var phoneCtrl = this.$ctrl("telephone");
        var value = phone_validator_1.PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    OrgNewComponent.prototype.prepareSaveOrg = function () {
        var org = {
            name: this.orgForm.get('name').value,
            address: this.orgForm.get('address').value,
            emailAddress: this.orgForm.get('emailAddress').value,
            emailDistribution: this.orgForm.get('emailDistribution').value,
            headOffice: this.orgForm.get('headOffice').value,
            telephone: this.orgForm.get('telephone').value,
            typeOfOperation: this.orgForm.get('typeOfOperation').value,
            type: this.parseOrgType(this.orgForm.get('orgType').value),
            assignedDoaIds: this.parseIds(this.orgForm.get('doas').value),
            assignedNsdIds: this.parseIds(this.orgForm.get('nsds').value),
            adminUserName: this.orgForm.get('username').value,
            adminFirstName: this.orgForm.get('firstName').value,
            adminLastName: this.orgForm.get('lastName').value,
        };
        return org;
    };
    OrgNewComponent.prototype.parseOrgType = function (value) {
        return +(value || 0);
    };
    OrgNewComponent.prototype.parseIds = function (value) {
        return (value || "").split(",").map(function (v) { return +v; });
    };
    OrgNewComponent.prototype.prepareEmptyOrg = function () {
        var org = {
            id: null,
            name: "",
            address: "",
            emailAddress: "",
            emailDistribution: "",
            telephone: "",
            typeOfOperation: "",
            isReadOnlyOrg: false,
            type: 0,
            headOffice: ""
        };
        return org;
    };
    OrgNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/org/org-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], OrgNewComponent);
    return OrgNewComponent;
}());
exports.OrgNewComponent = OrgNewComponent;
function doasValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    return null;
}
function nsdsValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    return null;
}
function phoneNumberValidator(c) {
    var value = c.value;
    if (!value)
        return { 'required': true };
    if (!phone_validator_1.PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true };
    }
    return null;
}
//# sourceMappingURL=org-new.component.js.map