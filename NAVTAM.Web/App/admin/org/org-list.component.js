"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrgListComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var phone_validator_1 = require("../shared/phone.validator");
var OrgListComponent = /** @class */ (function () {
    function OrgListComponent(toastr, fb, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.selectedOrg = null;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.canbeDeleted = false;
        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
        this.tableHeaders = {
            sorting: [
                { columnName: 'Name', direction: 0 },
                { columnName: 'Address', direction: 0 },
                { columnName: 'EmailAddress', direction: 0 },
                { columnName: 'Telephone', direction: 0 },
                { columnName: 'TypeOfOperation', direction: 0 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "org",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName,
            filterValue: ""
        };
    }
    ;
    OrgListComponent.prototype.ngOnInit = function () {
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "org") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getOrgsInPage(this.queryOptions.page);
    };
    Object.defineProperty(OrgListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    OrgListComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.orgListForm = this.fb.group({
            searchOrg: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchOrg").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateOrgList(); });
    };
    OrgListComponent.prototype.updateOrgList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchOrg');
        this.queryOptions.filterValue = subject.value;
        this.getOrgsInPage(this.queryOptions.page);
    };
    OrgListComponent.prototype.$ctrl = function (name) {
        return this.orgListForm.get(name);
    };
    OrgListComponent.prototype.selectOrg = function (org) {
        var _this = this;
        this.canbeDeleted = false;
        if (this.selectedOrg) {
            this.selectedOrg.isSelected = false;
        }
        this.selectedOrg = org;
        this.selectedOrg.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedOrg.id);
        this.dataService.getOrgUserCount(this.selectedOrg.id)
            .subscribe(function (count) {
            _this.canbeDeleted = count === 0;
        });
    };
    OrgListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getOrgsInPage(1);
    };
    OrgListComponent.prototype.onPageChange = function (page) {
        this.getOrgsInPage(page);
    };
    OrgListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getOrgsInPage(this.queryOptions.page);
    };
    OrgListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    OrgListComponent.prototype.confirmDeleteOrg = function () {
        $('#modal-delete').modal({});
    };
    OrgListComponent.prototype.deleteOrg = function () {
        var _this = this;
        if (this.selectedOrg) {
            this.dataService.deleteOrg(this.selectedOrg.id)
                .subscribe(function (result) {
                _this.getOrgsInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('OrgSession.SuccessOrgDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('OrgSession.FailureOrgDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    OrgListComponent.prototype.getOrgsInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredOrgs(this.queryOptions)
                .subscribe(function (orgs) {
                _this.processOrgs(orgs);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    OrgListComponent.prototype.processOrgs = function (orgs) {
        this.paging = orgs.paging;
        this.totalPages = orgs.paging.totalPages;
        for (var iter = 0; iter < orgs.data.length; iter++) {
            var org = orgs.data[iter];
            org.index = iter;
            org.telephone = phone_validator_1.PhoneValidator.formatTenDigits(org.telephone);
        }
        var lastOrgIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (orgs.data.length > 0) {
            this.orgs = orgs.data;
            var index = 0;
            if (lastOrgIdSelected) {
                index = this.orgs.findIndex(function (x) { return x.id === lastOrgIdSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.orgs[index].isSelected = true;
            this.selectedOrg = this.orgs[index];
            this.loadingData = false;
        }
        else {
            this.orgs = [];
            this.selectedOrg = null;
            this.loadingData = false;
        }
    };
    OrgListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/org/org-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], OrgListComponent);
    return OrgListComponent;
}());
exports.OrgListComponent = OrgListComponent;
//# sourceMappingURL=org-list.component.js.map