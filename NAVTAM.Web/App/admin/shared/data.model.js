"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProposalStatus = exports.RegionType = exports.DoaRegionSource = exports.ClientType = exports.RegionSource = void 0;
//End Geographic Region
var RegionSource = /** @class */ (function () {
    function RegionSource() {
    }
    RegionSource.None = 0;
    RegionSource.Geography = 1;
    RegionSource.FIR = 2;
    RegionSource.Points = 3;
    RegionSource.PointAndRadius = 4;
    return RegionSource;
}());
exports.RegionSource = RegionSource;
var ClientType = /** @class */ (function () {
    function ClientType() {
    }
    ClientType.AFTN = 0;
    ClientType.SWIM = 1;
    ClientType.REST = 2;
    return ClientType;
}());
exports.ClientType = ClientType;
var DoaRegionSource = /** @class */ (function () {
    function DoaRegionSource() {
    }
    DoaRegionSource.None = 0;
    DoaRegionSource.Geography = 1;
    DoaRegionSource.FIR = 2;
    DoaRegionSource.Points = 3;
    DoaRegionSource.PointAndRadius = 4;
    return DoaRegionSource;
}());
exports.DoaRegionSource = DoaRegionSource;
var RegionType = /** @class */ (function () {
    function RegionType() {
    }
    RegionType.Doa = 0;
    RegionType.Bilingual = 1;
    RegionType.Northern = 2;
    return RegionType;
}());
exports.RegionType = RegionType;
var ProposalStatus = /** @class */ (function () {
    function ProposalStatus() {
    }
    ProposalStatus.Undefined = 0;
    // User Proposal status
    ProposalStatus.Draft = 1;
    ProposalStatus.Submitted = 2;
    ProposalStatus.Withdrawn = 3;
    // NOF Proposal status
    ProposalStatus.Picked = 10;
    ProposalStatus.Parked = 11;
    ProposalStatus.Disseminated = 12;
    ProposalStatus.Rejected = 13;
    ProposalStatus.SoontoExpire = 14;
    ProposalStatus.Expired = 15;
    ProposalStatus.ParkedDraft = 16;
    ProposalStatus.Terminated = 17;
    ProposalStatus.DisseminatedModified = 18;
    ProposalStatus.ParkedPicked = 19;
    //Everybody
    ProposalStatus.Cancelled = 20;
    ProposalStatus.Replaced = 21;
    ProposalStatus.Deleted = 22;
    ProposalStatus.Discarded = 23;
    ProposalStatus.Taken = 255;
    return ProposalStatus;
}());
exports.ProposalStatus = ProposalStatus;
//# sourceMappingURL=data.model.js.map