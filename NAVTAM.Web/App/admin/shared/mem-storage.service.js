"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemoryStorageService = void 0;
var core_1 = require("@angular/core");
var MemoryStorageService = /** @class */ (function () {
    function MemoryStorageService() {
        this.ITEM_QUERY_OPTIONS_KEY = "ITEM_QUERY_OPTIONS_KEY";
        this.ITEM_ID_KEY = "ITEM_ID_KEY";
        this.NSD_CLIENTS_QUERY_OPTIONS_KEY = "NSD_CLIENTS_QUERY_OPTIONS_KEY";
        this.NSD_CLIENT_ID_KEY = "NSD_CLIENT_ID_KEY";
        this._memoryStorage = [];
    }
    MemoryStorageService.prototype.save = function (key, value) {
        var index = this._memoryStorage.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            this._memoryStorage[index].value = value;
        }
        else {
            this._memoryStorage.push({
                key: key,
                value: value
            });
        }
    };
    MemoryStorageService.prototype.get = function (key) {
        var index = this._memoryStorage.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            return this._memoryStorage[index].value;
        }
        return null;
    };
    MemoryStorageService.prototype.remove = function (key) {
        var index = this._memoryStorage.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            this._memoryStorage.splice(index, 1);
        }
    };
    MemoryStorageService = __decorate([
        core_1.Injectable()
    ], MemoryStorageService);
    return MemoryStorageService;
}());
exports.MemoryStorageService = MemoryStorageService;
//# sourceMappingURL=mem-storage.service.js.map