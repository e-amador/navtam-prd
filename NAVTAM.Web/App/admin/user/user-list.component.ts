﻿import { Component, OnInit, Inject } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { ActivatedRoute, Router } from '@angular/router'

import { Select2OptionData } from 'ng2-select2';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import { LocaleService, TranslationService } from 'angular-l10n';

import {
    IUserQueryOptions,
    IPaging,
    IUserPartial,
    IUsersDelete,
    ITableHeaders
} from '../shared/data.model'

declare var $: any; 

@Component({ 
    templateUrl: '/app/admin/user/user-list.component.html'
})
export class UserListComponent implements OnInit {

    users: IUserPartial[];
    queryOptions: IUserQueryOptions;
    selectedUser: IUserPartial = null;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;

    includeDisabled: boolean = false;
    includeDeleted: boolean = false;
    userListForm: FormGroup;
    deleteUserWarning: string = "";

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService, 
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        this.tableHeaders = {
            sorting:
            [
                { columnName: 'UserName', direction: 0 },
                { columnName: 'Organization', direction: 0 },
                { columnName: 'Roles', direction: 0 },
                { columnName: 'FirstName', direction: 0 },
                { columnName: 'LastName', direction: 0 },
                { columnName: 'Disabled', direction: 0 },
                { columnName: 'Deleted', direction: 0 }
            ],
            itemsPerPage:
            [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };

        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "user",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName, //Default Received Descending
            includeDisabled: this.includeDisabled,
            includeDeleted: this.includeDeleted,
            filterValue:  ""
        }
    };

    ngOnInit() {
        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "user") {
            this.queryOptions = queryOptions;
            this.includeDisabled = this.queryOptions.includeDisabled;
            this.includeDeleted = this.queryOptions.includeDeleted;
        }

        this.configureReactiveForm();

        this.getUsersInPage(this.queryOptions.page);
        this.deleteUserWarning = this.translation.translate('UserSession.DeleteUserConfirmation')
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    private configureReactiveForm() {
        this.userListForm = this.fb.group({
            searchUser: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchUser").valueChanges.debounceTime(1000).subscribe(() => this.updateUserList());
    }

    updateUserList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchUser');
        this.queryOptions.filterValue = subject.value;
        this.getUsersInPage(this.queryOptions.page);
    }

    $ctrl(name: string): AbstractControl {
        return this.userListForm.get(name);
    }

    private getSelectedUsers(): IUserPartial[] {
        if (!this.users) return [];
        var selected = this.users.filter(u => u.isSelected);
        return selected;
    }

    private clearSelectedUsers(): void {
        this.users.forEach(u => u.isSelected = false);
    }

    selectUser(user, event) {
        if (!event.ctrlKey && !event.shiftKey) {
            this.clearSelectedUsers();
        }
        if (event.shiftKey) {
            this.clearSelectedUsers();
            this.selectedUser.isSelected = true;
            var i1 = this.users.findIndex(u => u.id === this.selectedUser.id);
            var i2 = this.users.findIndex(u => u.id === user.id);
            if (i1 < i2) {
                for (var i = i1; i <= i2; i++) {
                    this.users[i].isSelected = true;
                }
            } else {
                for (var i = i2; i <= i1; i++) {
                    this.users[i].isSelected = true;
                }
            }
        } else {
            user.isSelected = !user.isSelected; 
        }
        if (user.isSelected) {
            this.selectedUser = user;
        } else {
            var sel = this.getSelectedUsers();
            if (sel.length > 0) this.selectedUser = sel[0];
            else this.selectedUser = null;
        }
        if (this.selectedUser) {
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedUser.username);
        }
        if (this.getSelectedUsers().length > 1) {
            this.deleteUserWarning = this.translation.translate('UserSession.DeleteUsersConfirmation');
        } else {
            this.deleteUserWarning = this.translation.translate('UserSession.DeleteUserConfirmation');
        }
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getUsersInPage(1);
    }

    onPageChange(page: number) {
        this.getUsersInPage(page);
    }

    public rowColor(dis: boolean) {
        if (dis) return 'bg-submitted';
        return '';
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getUsersInPage(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    confirmDeleteUser() {
        $('#modal-delete').modal({});
    }

    isShowDisabled(): boolean {
        if (this.selectedUser === null) return true;
        if (this.getSelectedUsers().length > 1) return true;
        return false;
    }

    isEditDisabled(): boolean {
        if (this.selectedUser === null) return true;
        if (this.getSelectedUsers().length > 1) return true;
        return false;
    }

    disableDeleteSelected() {
        if (this.getSelectedUsers().findIndex(u => u.id === this.winRef.appConfig.usrId) > -1) return true;
        return !this.selectedUser ||this.selectedUser.deleted;
    }

    disableResetPassword() {
        if (this.selectedUser === null) return true;
        if (this.getSelectedUsers().length > 1) return true;
        return this.selectedUser.deleted || this.selectedUser.disabled;
    }

    deleteUser() {
        if (this.selectedUser) {
            var names: IUsersDelete = {
                usernames: []
            };

            var selected = this.getSelectedUsers();
            if (selected.length > 0) {
                selected.forEach(u => names.usernames.push(u.username));
            }
            this.dataService.deleteMultiUsers(names)
                .subscribe(result => {
                    this.getUsersInPage(this.paging.currentPage);
                    let msg = this.translation.translate('UserSession.SuccessUserDeleted')
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('UserSession.FailureUserDeleted')
                    this.toastr.error("Delete error: " + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });

        }
    }

    private getUsersInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.queryOptions.includeDisabled = this.includeDisabled;
            this.queryOptions.includeDeleted = this.includeDeleted;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredUsers(this.queryOptions)
                .subscribe((users: any) => {
                    this.processUsers(users);
                });
        }
        catch (e) {
            this.loadingData = false;

        }
    }

    public switchDisabled() {
        this.includeDisabled = !this.includeDisabled;
        this.getUsersInPage(this.queryOptions.page);
    }

    public switchDeleted() {
        this.includeDeleted = !this.includeDeleted;
        this.getUsersInPage(this.queryOptions.page);
    }

    private processUsers(users: any) {
        this.paging = users.paging;
        this.totalPages = users.paging.totalPages;

        for (var iter = 0; iter < users.data.length; iter++) users.data[iter].index = iter;

        const lastUserSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (users.data.length > 0) {
            this.users = users.data;
            let index = 0;
            if (lastUserSelected) {
                index = this.users.findIndex(x => x.username === lastUserSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.users[index].isSelected = true;
            this.selectedUser = this.users[index];
            this.loadingData = false;
        } else {
            this.users = [];
            this.selectedUser = null;
            this.loadingData = false;
        }
    }


}
