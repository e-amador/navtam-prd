﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'

import { LocaleService, TranslationService } from 'angular-l10n';

import { Select2OptionData } from 'ng2-select2';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { WindowRef } from '../../common/windowRef.service'

import { DataService } from '../shared/data.service'
import { ProposalStatus } from '../shared/data.model';
import { PhoneValidator } from '../shared/phone.validator'

import { IRole, IUser, IOrganization, IDoaPartial } from '../shared/data.model';

@Component({ 
    templateUrl: '/app/admin/user/user-edit.component.html'
})
export class UserEditComponent implements OnInit  {
    
    model: any = null;
    action: string;
    currentPageTitle: string; 

    userEditForm: FormGroup;

    isReadOnly: boolean;

    public roleOptions: Select2Options;
    public orgOptions: Select2Options;
    public doaOptions: Select2Options;

    public orgs: Select2OptionData[] = [];
    public roles: Select2OptionData[] = [];
    public doas: Select2OptionData[] = [];

    public selectedRoleId: string = '-1';
    public selectedOrgId: string = '-1';
    public selectedDoasIds: string[] = [];

    public loadedRole: boolean = false;
    public loadedOrg: boolean = false;
    public loadedDoas: boolean = false;

    enableDelete: boolean = false;
    isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        if (this.action === "view") {
            this.isReadOnly = true;
            this.enableDelete = false;
            this.currentPageTitle = this.translation.translate('Common.View');
        } else {
            this.isReadOnly = false;
            this.enableDelete = this.model.deleted;
            this.currentPageTitle = this.translation.translate('Common.Edit');
        }

        this.roleOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectRole')
            },
            width: "100%"
        }
        this.orgOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectUserOrg')
            },
        }
        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectMultiDoas')
            },
            multiple: true,
            width: "100%"
        }

        this.model.phoneNumber = PhoneValidator.formatTenDigits(this.model.phoneNumber);
        this.model.fax = PhoneValidator.formatTenDigits(this.model.fax);

        this.configureReactiveForm();

        this.dataService.getOrganizations()
            .subscribe((org: IOrganization[]) => {
                this.orgs = org.map(
                    function (o: IOrganization) {
                        return <Select2OptionData>{
                            id: o.id,
                            text: o.name
                        };
                    });
                this.selectedOrgId = this.model.organizationId;
                this.loadedOrg = true;

                this.dataService.getAvailableRolesByOrg(this.model.organizationId)
                    .subscribe((roles: IRole[]) => {
                        this.roles = roles.map(
                            function (r: IRole) {
                                return <Select2OptionData>{
                                    id: r.roleId,
                                    text: r.roleName
                                };
                            });
                        this.roles.unshift({ id: "-1", text: "" });
                        if (this.roles.findIndex(x => x.id === this.model.roleId) > -1) {
                            this.selectedRoleId = this.model.roleId;
                        } else {
                            this.selectedRoleId = "-1";
                            this.$ctrl('roleId').setValue("-1", { onlySelf: false, emitEvent: true });
                            this.model.roleId = "-1";
                        }
                        this.loadedRole = true;

                        this.dataService.getDoasInOrg(this.model.organizationId)
                            .subscribe((doas: IDoaPartial[]) => {
                                this.doas = doas.map(
                                    function (d: IDoaPartial) {
                                        return <Select2OptionData>{
                                            id: d.id.toString(),
                                            text: d.name
                                        };
                                    });
                                this.doas.unshift({ id: "-1", text: "" });
                                this.selectedDoasIds = this.model.doaIds.split(',');
                                this.loadedDoas = true;
                                this.userEditForm.markAsPristine();
                            });

                    });
            });
    }

    private finishLoading(): boolean {
        return (this.loadedOrg && this.loadedDoas && this.loadedRole);
    }

    private loadRoles(orgId: string): void {
        if (orgId !== '-1') {
            this.loadedRole = false;
            this.dataService.getAvailableRolesByOrg(orgId)
                .subscribe((roles: IRole[]) => {
                    this.roles = roles.map(
                        function (r: IRole) {
                            return <Select2OptionData>{
                                id: r.roleId,
                                text: r.roleName
                            };
                        });
                    this.roles.unshift({ id: "-1", text: "" });
                    this.selectedRoleId = "-1";
                    this.$ctrl('roleId').setValue("-1", { onlySelf: false, emitEvent: true });
                    this.loadedRole = true;
                });
        }
    }

    private loadDoas(orgId: string): void {
        if (orgId !== '-1') {
            this.loadedDoas = false;
            this.dataService.getDoasInOrg(orgId)
                .subscribe((doas: IDoaPartial[]) => {
                    this.doas = doas.map(
                        function (d: IDoaPartial) {
                            return <Select2OptionData>{
                                id: d.id.toString(),
                                text: d.name
                            };
                        });
                    this.doas.unshift({ id: "-1", text: "" });
                    this.selectedDoasIds = [];
                    this.$ctrl('doas').setValue("", { onlySelf: false, emitEvent: false });
                    this.loadedDoas = true;
                });
        }
    }

    onOrgChanged(data: { value: string }) {
        if (!this.finishLoading()) return;
        if (data.value) {
            var ctrl = this.$ctrl('org');
            if (ctrl.value !== data.value && data.value !== '-1') {
                this.$ctrl('org').setValue(data.value);
                this.$ctrl('org').markAsDirty();
                this.loadRoles(data.value);
                this.loadDoas(data.value);
            }
        }
    }

    onRoleChanged(data: { value: string }) {
        if (!this.finishLoading()) return;
        if (data.value) { //&& data.value !== '-1'
            var ctrl = this.$ctrl('roleId');
            if (ctrl.value !== data.value) {
                this.$ctrl('roleId').setValue(data.value);
                this.selectedRoleId = data.value;
                this.$ctrl('roleId').markAsDirty();
            }
        }
    }

    onDoaChanged(data: { value: string[] }) {
        if (!this.finishLoading())
            return;

        var ctrl = this.$ctrl('doas');
        const newDoas = data.value && data.value.length ? data.value.join(',') : "";

        if (ctrl.value !== newDoas) {
            this.$ctrl('doas').setValue(newDoas);
            if (newDoas === this.model.doaIds) {
                this.$ctrl('doas').markAsPristine();
            } else {
                this.$ctrl('doas').markAsDirty();
            }
        }
    }

    onApproveChanged(checked: boolean) {
        if (this.isReadOnly) return true;
        const control = this.$ctrl('approved');
        control.setValue(checked);
        control.markAsDirty();
    }

    onDisableChanged(e: any) {
        if (this.isReadOnly) return true;
        const control = this.$ctrl('disabled');
        const val = control.value;
        control.setValue(!val);
        control.markAsDirty();
    }

    onDeletedChanged(e: any) {
        if (this.isReadOnly) return true;
        const control = this.$ctrl('deleted');
        const val = control.value;
        control.setValue(!val);
        control.markAsDirty();
    }

    backToUserList() {
        this.router.navigate(['/administration']);
    }

    resendVerificationEmail() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        this.dataService.sendVerificationEmail(this.model.id)
            .subscribe(r => {
                let msg = this.translation.translate('UserSession.ConfirmationEmailUser');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('UserSession.ConfirmationEmailUserError');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    resetPasswordVerificationCode() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        this.dataService.resetPasswordVerificationCode(this.model.id)
            .subscribe(r => {
                let msg = this.translation.translate('UserSession.ConfirmationPassword');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('UserSession.ConfirmationPasswordError');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    phoneNumberFocus(ctrName: string) {
        const phoneCtrl = this.$ctrl(ctrName);
        const value = PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    phoneNumberBlur(ctrName: string) {
        const phoneCtrl = this.$ctrl(ctrName);
        const value = PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    validateFirstName() {
        if (this.isReadOnly) return true;
        const firstNameCtrl = this.$ctrl("firstName");
        return firstNameCtrl.valid || this.userEditForm.pristine;
    }

    validateLastName() {
        if (this.isReadOnly) return true;
        const lastNameCtrl = this.$ctrl("lastName");
        return lastNameCtrl.valid || this.userEditForm.pristine;
    }

    validateEmailAddress() {
        if (this.isReadOnly) return true;
        const emailCtrl = this.$ctrl("email");
        return emailCtrl.valid || this.userEditForm.pristine;
    }

    validateAddress() {
        if (this.isReadOnly) return true;
        const emailCtrl = this.$ctrl("address");
        return emailCtrl.valid || this.userEditForm.pristine;
    }

    validatePhoneNumber() {
        if (this.isReadOnly) return true;
        const emailCtrl = this.$ctrl("phoneNumber");
        return emailCtrl.valid || this.userEditForm.pristine;
    }

    validateFax() {
        if (this.isReadOnly) return true;
        const emailCtrl = this.$ctrl("fax");
        return emailCtrl.valid || this.userEditForm.pristine;
    }

    validateRoles() {
        const fc = this.$ctrl('roleId');
        this.$ctrl('roleId').setErrors(null);
        if (fc.value === "" || fc.value === "-1") {
            let error = { roleIdNeeded: { valid: false } };
            this.$ctrl('roleId').setErrors(error);
            return false;
        }
        //if (this.userEditForm.pristine) return true;
        return true;
    }

    validateOrganization() {
        if (this.isReadOnly) return true;
        const fc = this.$ctrl('org');
        return fc.value !== -1 || this.userEditForm.pristine;
    }

    validateDoas() {
        if (this.isReadOnly) return true;
        const fc = this.$ctrl('doas');
        this.$ctrl('doas').setErrors(null);
        if (this.userEditForm.pristine) return true;
        if (fc.value !== "" && fc.value && fc.value.length > 0) return true;
        let error = { doasNeeded: { valid: false } };
        this.$ctrl('doas').setErrors(error);
        return false;
        //return fc.value !== "" || this.userEditForm.pristine;
    }

    public canSave(): boolean {
        if (this.isSubmitting) return false;
        return this.isValidForm() && !this.userEditForm.pristine;
    } 

    public isValidForm(): boolean {
        if (this.isReadOnly) return true;
        if (this.userEditForm.invalid) return false;
        if (this.userEditForm.pristine) return false;
        this.$ctrl('roleId').setErrors(null);
        if (this.validateRoles()) {
            this.$ctrl('deleted').setErrors(null);
            if (this.$ctrl('deleted').value) {
                let error = { deletedUser: { valid: false } };
                this.$ctrl('deleted').setErrors(error);
                return false;
            }
            return true;
        }
        return false;
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        const user = this.prepareUpdateUser();
        this.dataService.updateUser(user)
            .subscribe(data => {
                //this.isSaveButtonDisabled = true;
                let msg = this.translation.translate('UserSession.SuccessUserUpdated')
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('UserSession.FailureUserUpdated')
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    isAdminItself() {
        return this.model.id && this.winRef.appConfig.usrId && this.model.id === this.winRef.appConfig.usrId;
    }

    doaDisabled() {
        return this.isReadOnly || this.isAdminItself();
    }

    orgDisabled() {
        return this.isReadOnly || this.isAdminItself() || !this.winRef.appConfig.sysAdmin;
    }

    rolesDisabled() {
        return this.isReadOnly || this.isAdminItself();
    }

    disabledFieldDisabled() {
        return this.isReadOnly || this.isAdminItself();
    }

    private configureReactiveForm() {
        this.userEditForm = this.fb.group({
            username: { value: this.model.username, disabled: true },
            firstName: [{ value: this.model.firstName, disabled: this.isReadOnly }, [Validators.required, Validators.pattern('[a-zA-Z0-9\ -_.]+$')]],
            lastName: [{ value: this.model.lastName, disabled: this.isReadOnly }, [Validators.required, Validators.pattern('[a-zA-Z0-9\ -_.]+$')]],
            position: { value: this.model.position, disabled: this.isReadOnly },
            email: [{ value: this.model.email, disabled: this.isReadOnly }, [Validators.required, Validators.pattern("[a-zA-Z0-9._+-]{1,}@[a-zA-Z0-9._-]{2,}[.]{1}[a-zA-Z]{2,3}")]], 
            org: [{ value: this.model.organizationId, disabled: this.isReadOnly }, [orgValidator]],
            roleId: [{ value: this.model.roleId, disabled: this.isReadOnly }, [roleValidator]],
            doas: [{ value: this.model.doaIds, disabled: this.isReadOnly }, [doaValidator]], //.split(',')
            address: [{ value: this.model.address, disabled: this.isReadOnly }, [Validators.required]],
            //approved: { value: this.model.approved, disabled: this.isReadOnly },
            disabled: { value: this.model.disabled, disabled: this.isReadOnly },
            deleted: { value: this.model.deleted, disabled: this.isReadOnly },
            phoneNumber: [{ value: this.model.phoneNumber, disabled: this.isReadOnly }, [Validators.required, phoneNumberValidator]],
            fax: [{ value: this.model.fax, disabled: this.isReadOnly }, [phoneNumberValidator]]
        });
    }

    private $ctrl(name: string): AbstractControl {
        return this.userEditForm.get(name);
    }

    private prepareUpdateUser(): IUser {
        const user: IUser = {
            username: this.$ctrl('username').value,
            position: this.$ctrl('position').value,
            address: this.$ctrl('address').value,
            fax: PhoneValidator.formatTenDigits(this.$ctrl('fax').value),
            phoneNumber: PhoneValidator.formatTenDigits(this.$ctrl('phoneNumber').value),
            firstName: this.$ctrl('firstName').value,
            lastName: this.$ctrl('lastName').value,
            email: this.$ctrl('email').value,
            approved: true,
            disabled: this.$ctrl('disabled').value,
            roleId: this.$ctrl('roleId').value,
            organizationId: this.$ctrl('org').value,
            doaIds: this.$ctrl('doas').value,
            deleted: this.$ctrl('deleted').value
        }
        return user;
    }

    private matchIds(newValues: string[], oldValues: string[]) {
        const newJoined = newValues ? newValues.join(' ') : "";
        const oldJoined = oldValues ? oldValues.join(' ') : "";
        return newJoined === oldJoined;
    }

}

function roleValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    }
    return null;
}

function doaValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    }
    return null;
}

function orgValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === -1) {
        return { 'required': true }
    }
    return null;
}

function phoneNumberValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const value = c.value;
    if (value && !PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true }
    }
    return null;
}