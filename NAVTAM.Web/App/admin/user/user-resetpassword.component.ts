﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'
import { ProposalStatus } from '../shared/data.model';

import { PasswordValidator } from '../shared/password.validator';

import { IRole, IUser, IOrganization } from '../shared/data.model';

@Component({ 
    templateUrl: '/app/admin/user/user-resetpassword.component.html'
})
export class UserResetPasswordComponent implements OnInit  {
    
    model: any = null;
    userForm: FormGroup;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        this.model = this.activatedRoute.snapshot.data['model'];
        this.configureReactiveForm();
    }

    backToUserList() {
        this.router.navigate(['/administration']);
    }

    validatePassword() {
        const passwordCtrl = this.userForm.get('grpPassword.password');
        return passwordCtrl.valid || this.userForm.pristine;
    }

    validateConfirmPassword() {
        const confirmPasswordCtrl = this.userForm.get('grpPassword.confirmPassword');
        return confirmPasswordCtrl.valid || this.userForm.pristine;
    }

    onSubmit() {
        const data = this.prepareUserResetPassword();
        this.dataService.resetUserPassword(data)
            .subscribe(data => {
                let msg = this.translation.translate('UserSession.SuccessResetPassword')
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration"]);
            }, error => {
                let msg = this.translation.translate('UserSession.FailureResetPassword')
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private configureReactiveForm() {
        this.userForm = this.fb.group({
            username: { value: this.model.username, disabled: true },
            firstName: { value: this.model.firstName, disabled: true },
            lastName: { value: this.model.lastName, disabled: true },
            grpPassword: this.fb.group({
                password: [{ value: "", disabled: false }, [Validators.required, Validators.pattern("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,20})")]],
                confirmPassword: [{ value: "", disabled: false }, [Validators.required]],
            }, { validator: PasswordValidator.MatchPassword })
        });
    }

    private prepareUserResetPassword(): any {
        const data = {
            username: this.userForm.get('username').value,
            password: this.userForm.get('grpPassword.password').value,
            confirmPassword: this.userForm.get('grpPassword.confirmPassword').value
        }

        return data;
    }

}