"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var UserListComponent = /** @class */ (function () {
    function UserListComponent(toastr, fb, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.selectedUser = null;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.includeDisabled = false;
        this.includeDeleted = false;
        this.deleteUserWarning = "";
        this.tableHeaders = {
            sorting: [
                { columnName: 'UserName', direction: 0 },
                { columnName: 'Organization', direction: 0 },
                { columnName: 'Roles', direction: 0 },
                { columnName: 'FirstName', direction: 0 },
                { columnName: 'LastName', direction: 0 },
                { columnName: 'Disabled', direction: 0 },
                { columnName: 'Deleted', direction: 0 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "user",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName,
            includeDisabled: this.includeDisabled,
            includeDeleted: this.includeDeleted,
            filterValue: ""
        };
    }
    ;
    UserListComponent.prototype.ngOnInit = function () {
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "user") {
            this.queryOptions = queryOptions;
            this.includeDisabled = this.queryOptions.includeDisabled;
            this.includeDeleted = this.queryOptions.includeDeleted;
        }
        this.configureReactiveForm();
        this.getUsersInPage(this.queryOptions.page);
        this.deleteUserWarning = this.translation.translate('UserSession.DeleteUserConfirmation');
    };
    Object.defineProperty(UserListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    UserListComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.userListForm = this.fb.group({
            searchUser: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchUser").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateUserList(); });
    };
    UserListComponent.prototype.updateUserList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchUser');
        this.queryOptions.filterValue = subject.value;
        this.getUsersInPage(this.queryOptions.page);
    };
    UserListComponent.prototype.$ctrl = function (name) {
        return this.userListForm.get(name);
    };
    UserListComponent.prototype.getSelectedUsers = function () {
        if (!this.users)
            return [];
        var selected = this.users.filter(function (u) { return u.isSelected; });
        return selected;
    };
    UserListComponent.prototype.clearSelectedUsers = function () {
        this.users.forEach(function (u) { return u.isSelected = false; });
    };
    UserListComponent.prototype.selectUser = function (user, event) {
        var _this = this;
        if (!event.ctrlKey && !event.shiftKey) {
            this.clearSelectedUsers();
        }
        if (event.shiftKey) {
            this.clearSelectedUsers();
            this.selectedUser.isSelected = true;
            var i1 = this.users.findIndex(function (u) { return u.id === _this.selectedUser.id; });
            var i2 = this.users.findIndex(function (u) { return u.id === user.id; });
            if (i1 < i2) {
                for (var i = i1; i <= i2; i++) {
                    this.users[i].isSelected = true;
                }
            }
            else {
                for (var i = i2; i <= i1; i++) {
                    this.users[i].isSelected = true;
                }
            }
        }
        else {
            user.isSelected = !user.isSelected;
        }
        if (user.isSelected) {
            this.selectedUser = user;
        }
        else {
            var sel = this.getSelectedUsers();
            if (sel.length > 0)
                this.selectedUser = sel[0];
            else
                this.selectedUser = null;
        }
        if (this.selectedUser) {
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedUser.username);
        }
        if (this.getSelectedUsers().length > 1) {
            this.deleteUserWarning = this.translation.translate('UserSession.DeleteUsersConfirmation');
        }
        else {
            this.deleteUserWarning = this.translation.translate('UserSession.DeleteUserConfirmation');
        }
    };
    UserListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getUsersInPage(1);
    };
    UserListComponent.prototype.onPageChange = function (page) {
        this.getUsersInPage(page);
    };
    UserListComponent.prototype.rowColor = function (dis) {
        if (dis)
            return 'bg-submitted';
        return '';
    };
    UserListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getUsersInPage(this.queryOptions.page);
    };
    UserListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    UserListComponent.prototype.confirmDeleteUser = function () {
        $('#modal-delete').modal({});
    };
    UserListComponent.prototype.isShowDisabled = function () {
        if (this.selectedUser === null)
            return true;
        if (this.getSelectedUsers().length > 1)
            return true;
        return false;
    };
    UserListComponent.prototype.isEditDisabled = function () {
        if (this.selectedUser === null)
            return true;
        if (this.getSelectedUsers().length > 1)
            return true;
        return false;
    };
    UserListComponent.prototype.disableDeleteSelected = function () {
        var _this = this;
        if (this.getSelectedUsers().findIndex(function (u) { return u.id === _this.winRef.appConfig.usrId; }) > -1)
            return true;
        return !this.selectedUser || this.selectedUser.deleted;
    };
    UserListComponent.prototype.disableResetPassword = function () {
        if (this.selectedUser === null)
            return true;
        if (this.getSelectedUsers().length > 1)
            return true;
        return this.selectedUser.deleted || this.selectedUser.disabled;
    };
    UserListComponent.prototype.deleteUser = function () {
        var _this = this;
        if (this.selectedUser) {
            var names = {
                usernames: []
            };
            var selected = this.getSelectedUsers();
            if (selected.length > 0) {
                selected.forEach(function (u) { return names.usernames.push(u.username); });
            }
            this.dataService.deleteMultiUsers(names)
                .subscribe(function (result) {
                _this.getUsersInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('UserSession.SuccessUserDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('UserSession.FailureUserDeleted');
                _this.toastr.error("Delete error: " + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    UserListComponent.prototype.getUsersInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.queryOptions.includeDisabled = this.includeDisabled;
            this.queryOptions.includeDeleted = this.includeDeleted;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredUsers(this.queryOptions)
                .subscribe(function (users) {
                _this.processUsers(users);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    UserListComponent.prototype.switchDisabled = function () {
        this.includeDisabled = !this.includeDisabled;
        this.getUsersInPage(this.queryOptions.page);
    };
    UserListComponent.prototype.switchDeleted = function () {
        this.includeDeleted = !this.includeDeleted;
        this.getUsersInPage(this.queryOptions.page);
    };
    UserListComponent.prototype.processUsers = function (users) {
        this.paging = users.paging;
        this.totalPages = users.paging.totalPages;
        for (var iter = 0; iter < users.data.length; iter++)
            users.data[iter].index = iter;
        var lastUserSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (users.data.length > 0) {
            this.users = users.data;
            var index = 0;
            if (lastUserSelected) {
                index = this.users.findIndex(function (x) { return x.username === lastUserSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.users[index].isSelected = true;
            this.selectedUser = this.users[index];
            this.loadingData = false;
        }
        else {
            this.users = [];
            this.selectedUser = null;
            this.loadingData = false;
        }
    };
    UserListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/user/user-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], UserListComponent);
    return UserListComponent;
}());
exports.UserListComponent = UserListComponent;
//# sourceMappingURL=user-list.component.js.map