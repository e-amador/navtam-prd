"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEditComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_service_1 = require("../shared/data.service");
var phone_validator_1 = require("../shared/phone.validator");
var UserEditComponent = /** @class */ (function () {
    function UserEditComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.orgs = [];
        this.roles = [];
        this.doas = [];
        this.selectedRoleId = '-1';
        this.selectedOrgId = '-1';
        this.selectedDoasIds = [];
        this.loadedRole = false;
        this.loadedOrg = false;
        this.loadedDoas = false;
        this.enableDelete = false;
        this.isSubmitting = false;
    }
    UserEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        if (this.action === "view") {
            this.isReadOnly = true;
            this.enableDelete = false;
            this.currentPageTitle = this.translation.translate('Common.View');
        }
        else {
            this.isReadOnly = false;
            this.enableDelete = this.model.deleted;
            this.currentPageTitle = this.translation.translate('Common.Edit');
        }
        this.roleOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectRole')
            },
            width: "100%"
        };
        this.orgOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectUserOrg')
            },
        };
        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectMultiDoas')
            },
            multiple: true,
            width: "100%"
        };
        this.model.phoneNumber = phone_validator_1.PhoneValidator.formatTenDigits(this.model.phoneNumber);
        this.model.fax = phone_validator_1.PhoneValidator.formatTenDigits(this.model.fax);
        this.configureReactiveForm();
        this.dataService.getOrganizations()
            .subscribe(function (org) {
            _this.orgs = org.map(function (o) {
                return {
                    id: o.id,
                    text: o.name
                };
            });
            _this.selectedOrgId = _this.model.organizationId;
            _this.loadedOrg = true;
            _this.dataService.getAvailableRolesByOrg(_this.model.organizationId)
                .subscribe(function (roles) {
                _this.roles = roles.map(function (r) {
                    return {
                        id: r.roleId,
                        text: r.roleName
                    };
                });
                _this.roles.unshift({ id: "-1", text: "" });
                if (_this.roles.findIndex(function (x) { return x.id === _this.model.roleId; }) > -1) {
                    _this.selectedRoleId = _this.model.roleId;
                }
                else {
                    _this.selectedRoleId = "-1";
                    _this.$ctrl('roleId').setValue("-1", { onlySelf: false, emitEvent: true });
                    _this.model.roleId = "-1";
                }
                _this.loadedRole = true;
                _this.dataService.getDoasInOrg(_this.model.organizationId)
                    .subscribe(function (doas) {
                    _this.doas = doas.map(function (d) {
                        return {
                            id: d.id.toString(),
                            text: d.name
                        };
                    });
                    _this.doas.unshift({ id: "-1", text: "" });
                    _this.selectedDoasIds = _this.model.doaIds.split(',');
                    _this.loadedDoas = true;
                    _this.userEditForm.markAsPristine();
                });
            });
        });
    };
    UserEditComponent.prototype.finishLoading = function () {
        return (this.loadedOrg && this.loadedDoas && this.loadedRole);
    };
    UserEditComponent.prototype.loadRoles = function (orgId) {
        var _this = this;
        if (orgId !== '-1') {
            this.loadedRole = false;
            this.dataService.getAvailableRolesByOrg(orgId)
                .subscribe(function (roles) {
                _this.roles = roles.map(function (r) {
                    return {
                        id: r.roleId,
                        text: r.roleName
                    };
                });
                _this.roles.unshift({ id: "-1", text: "" });
                _this.selectedRoleId = "-1";
                _this.$ctrl('roleId').setValue("-1", { onlySelf: false, emitEvent: true });
                _this.loadedRole = true;
            });
        }
    };
    UserEditComponent.prototype.loadDoas = function (orgId) {
        var _this = this;
        if (orgId !== '-1') {
            this.loadedDoas = false;
            this.dataService.getDoasInOrg(orgId)
                .subscribe(function (doas) {
                _this.doas = doas.map(function (d) {
                    return {
                        id: d.id.toString(),
                        text: d.name
                    };
                });
                _this.doas.unshift({ id: "-1", text: "" });
                _this.selectedDoasIds = [];
                _this.$ctrl('doas').setValue("", { onlySelf: false, emitEvent: false });
                _this.loadedDoas = true;
            });
        }
    };
    UserEditComponent.prototype.onOrgChanged = function (data) {
        if (!this.finishLoading())
            return;
        if (data.value) {
            var ctrl = this.$ctrl('org');
            if (ctrl.value !== data.value && data.value !== '-1') {
                this.$ctrl('org').setValue(data.value);
                this.$ctrl('org').markAsDirty();
                this.loadRoles(data.value);
                this.loadDoas(data.value);
            }
        }
    };
    UserEditComponent.prototype.onRoleChanged = function (data) {
        if (!this.finishLoading())
            return;
        if (data.value) { //&& data.value !== '-1'
            var ctrl = this.$ctrl('roleId');
            if (ctrl.value !== data.value) {
                this.$ctrl('roleId').setValue(data.value);
                this.selectedRoleId = data.value;
                this.$ctrl('roleId').markAsDirty();
            }
        }
    };
    UserEditComponent.prototype.onDoaChanged = function (data) {
        if (!this.finishLoading())
            return;
        var ctrl = this.$ctrl('doas');
        var newDoas = data.value && data.value.length ? data.value.join(',') : "";
        if (ctrl.value !== newDoas) {
            this.$ctrl('doas').setValue(newDoas);
            if (newDoas === this.model.doaIds) {
                this.$ctrl('doas').markAsPristine();
            }
            else {
                this.$ctrl('doas').markAsDirty();
            }
        }
    };
    UserEditComponent.prototype.onApproveChanged = function (checked) {
        if (this.isReadOnly)
            return true;
        var control = this.$ctrl('approved');
        control.setValue(checked);
        control.markAsDirty();
    };
    UserEditComponent.prototype.onDisableChanged = function (e) {
        if (this.isReadOnly)
            return true;
        var control = this.$ctrl('disabled');
        var val = control.value;
        control.setValue(!val);
        control.markAsDirty();
    };
    UserEditComponent.prototype.onDeletedChanged = function (e) {
        if (this.isReadOnly)
            return true;
        var control = this.$ctrl('deleted');
        var val = control.value;
        control.setValue(!val);
        control.markAsDirty();
    };
    UserEditComponent.prototype.backToUserList = function () {
        this.router.navigate(['/administration']);
    };
    UserEditComponent.prototype.resendVerificationEmail = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        this.dataService.sendVerificationEmail(this.model.id)
            .subscribe(function (r) {
            var msg = _this.translation.translate('UserSession.ConfirmationEmailUser');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('UserSession.ConfirmationEmailUserError');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    UserEditComponent.prototype.resetPasswordVerificationCode = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        this.dataService.resetPasswordVerificationCode(this.model.id)
            .subscribe(function (r) {
            var msg = _this.translation.translate('UserSession.ConfirmationPassword');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('UserSession.ConfirmationPasswordError');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    UserEditComponent.prototype.phoneNumberFocus = function (ctrName) {
        var phoneCtrl = this.$ctrl(ctrName);
        var value = phone_validator_1.PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    UserEditComponent.prototype.phoneNumberBlur = function (ctrName) {
        var phoneCtrl = this.$ctrl(ctrName);
        var value = phone_validator_1.PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    UserEditComponent.prototype.validateFirstName = function () {
        if (this.isReadOnly)
            return true;
        var firstNameCtrl = this.$ctrl("firstName");
        return firstNameCtrl.valid || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.validateLastName = function () {
        if (this.isReadOnly)
            return true;
        var lastNameCtrl = this.$ctrl("lastName");
        return lastNameCtrl.valid || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.validateEmailAddress = function () {
        if (this.isReadOnly)
            return true;
        var emailCtrl = this.$ctrl("email");
        return emailCtrl.valid || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.validateAddress = function () {
        if (this.isReadOnly)
            return true;
        var emailCtrl = this.$ctrl("address");
        return emailCtrl.valid || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.validatePhoneNumber = function () {
        if (this.isReadOnly)
            return true;
        var emailCtrl = this.$ctrl("phoneNumber");
        return emailCtrl.valid || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.validateFax = function () {
        if (this.isReadOnly)
            return true;
        var emailCtrl = this.$ctrl("fax");
        return emailCtrl.valid || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.validateRoles = function () {
        var fc = this.$ctrl('roleId');
        this.$ctrl('roleId').setErrors(null);
        if (fc.value === "" || fc.value === "-1") {
            var error = { roleIdNeeded: { valid: false } };
            this.$ctrl('roleId').setErrors(error);
            return false;
        }
        //if (this.userEditForm.pristine) return true;
        return true;
    };
    UserEditComponent.prototype.validateOrganization = function () {
        if (this.isReadOnly)
            return true;
        var fc = this.$ctrl('org');
        return fc.value !== -1 || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.validateDoas = function () {
        if (this.isReadOnly)
            return true;
        var fc = this.$ctrl('doas');
        this.$ctrl('doas').setErrors(null);
        if (this.userEditForm.pristine)
            return true;
        if (fc.value !== "" && fc.value && fc.value.length > 0)
            return true;
        var error = { doasNeeded: { valid: false } };
        this.$ctrl('doas').setErrors(error);
        return false;
        //return fc.value !== "" || this.userEditForm.pristine;
    };
    UserEditComponent.prototype.canSave = function () {
        if (this.isSubmitting)
            return false;
        return this.isValidForm() && !this.userEditForm.pristine;
    };
    UserEditComponent.prototype.isValidForm = function () {
        if (this.isReadOnly)
            return true;
        if (this.userEditForm.invalid)
            return false;
        if (this.userEditForm.pristine)
            return false;
        this.$ctrl('roleId').setErrors(null);
        if (this.validateRoles()) {
            this.$ctrl('deleted').setErrors(null);
            if (this.$ctrl('deleted').value) {
                var error = { deletedUser: { valid: false } };
                this.$ctrl('deleted').setErrors(error);
                return false;
            }
            return true;
        }
        return false;
    };
    UserEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var user = this.prepareUpdateUser();
        this.dataService.updateUser(user)
            .subscribe(function (data) {
            //this.isSaveButtonDisabled = true;
            var msg = _this.translation.translate('UserSession.SuccessUserUpdated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('UserSession.FailureUserUpdated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    UserEditComponent.prototype.isAdminItself = function () {
        return this.model.id && this.winRef.appConfig.usrId && this.model.id === this.winRef.appConfig.usrId;
    };
    UserEditComponent.prototype.doaDisabled = function () {
        return this.isReadOnly || this.isAdminItself();
    };
    UserEditComponent.prototype.orgDisabled = function () {
        return this.isReadOnly || this.isAdminItself() || !this.winRef.appConfig.sysAdmin;
    };
    UserEditComponent.prototype.rolesDisabled = function () {
        return this.isReadOnly || this.isAdminItself();
    };
    UserEditComponent.prototype.disabledFieldDisabled = function () {
        return this.isReadOnly || this.isAdminItself();
    };
    UserEditComponent.prototype.configureReactiveForm = function () {
        this.userEditForm = this.fb.group({
            username: { value: this.model.username, disabled: true },
            firstName: [{ value: this.model.firstName, disabled: this.isReadOnly }, [forms_1.Validators.required, forms_1.Validators.pattern('[a-zA-Z0-9\ -_.]+$')]],
            lastName: [{ value: this.model.lastName, disabled: this.isReadOnly }, [forms_1.Validators.required, forms_1.Validators.pattern('[a-zA-Z0-9\ -_.]+$')]],
            position: { value: this.model.position, disabled: this.isReadOnly },
            email: [{ value: this.model.email, disabled: this.isReadOnly }, [forms_1.Validators.required, forms_1.Validators.pattern("[a-zA-Z0-9._+-]{1,}@[a-zA-Z0-9._-]{2,}[.]{1}[a-zA-Z]{2,3}")]],
            org: [{ value: this.model.organizationId, disabled: this.isReadOnly }, [orgValidator]],
            roleId: [{ value: this.model.roleId, disabled: this.isReadOnly }, [roleValidator]],
            doas: [{ value: this.model.doaIds, disabled: this.isReadOnly }, [doaValidator]],
            address: [{ value: this.model.address, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            //approved: { value: this.model.approved, disabled: this.isReadOnly },
            disabled: { value: this.model.disabled, disabled: this.isReadOnly },
            deleted: { value: this.model.deleted, disabled: this.isReadOnly },
            phoneNumber: [{ value: this.model.phoneNumber, disabled: this.isReadOnly }, [forms_1.Validators.required, phoneNumberValidator]],
            fax: [{ value: this.model.fax, disabled: this.isReadOnly }, [phoneNumberValidator]]
        });
    };
    UserEditComponent.prototype.$ctrl = function (name) {
        return this.userEditForm.get(name);
    };
    UserEditComponent.prototype.prepareUpdateUser = function () {
        var user = {
            username: this.$ctrl('username').value,
            position: this.$ctrl('position').value,
            address: this.$ctrl('address').value,
            fax: phone_validator_1.PhoneValidator.formatTenDigits(this.$ctrl('fax').value),
            phoneNumber: phone_validator_1.PhoneValidator.formatTenDigits(this.$ctrl('phoneNumber').value),
            firstName: this.$ctrl('firstName').value,
            lastName: this.$ctrl('lastName').value,
            email: this.$ctrl('email').value,
            approved: true,
            disabled: this.$ctrl('disabled').value,
            roleId: this.$ctrl('roleId').value,
            organizationId: this.$ctrl('org').value,
            doaIds: this.$ctrl('doas').value,
            deleted: this.$ctrl('deleted').value
        };
        return user;
    };
    UserEditComponent.prototype.matchIds = function (newValues, oldValues) {
        var newJoined = newValues ? newValues.join(' ') : "";
        var oldJoined = oldValues ? oldValues.join(' ') : "";
        return newJoined === oldJoined;
    };
    UserEditComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/user/user-edit.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], UserEditComponent);
    return UserEditComponent;
}());
exports.UserEditComponent = UserEditComponent;
function roleValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    return null;
}
function doaValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    return null;
}
function orgValidator(c) {
    if (c.value === -1) {
        return { 'required': true };
    }
    return null;
}
function phoneNumberValidator(c) {
    var value = c.value;
    if (value && !phone_validator_1.PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true };
    }
    return null;
}
//# sourceMappingURL=user-edit.component.js.map