"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserResetPasswordComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var password_validator_1 = require("../shared/password.validator");
var UserResetPasswordComponent = /** @class */ (function () {
    function UserResetPasswordComponent(toastr, fb, dataService, memStorageService, router, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
    }
    UserResetPasswordComponent.prototype.ngOnInit = function () {
        this.model = this.activatedRoute.snapshot.data['model'];
        this.configureReactiveForm();
    };
    UserResetPasswordComponent.prototype.backToUserList = function () {
        this.router.navigate(['/administration']);
    };
    UserResetPasswordComponent.prototype.validatePassword = function () {
        var passwordCtrl = this.userForm.get('grpPassword.password');
        return passwordCtrl.valid || this.userForm.pristine;
    };
    UserResetPasswordComponent.prototype.validateConfirmPassword = function () {
        var confirmPasswordCtrl = this.userForm.get('grpPassword.confirmPassword');
        return confirmPasswordCtrl.valid || this.userForm.pristine;
    };
    UserResetPasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        var data = this.prepareUserResetPassword();
        this.dataService.resetUserPassword(data)
            .subscribe(function (data) {
            var msg = _this.translation.translate('UserSession.SuccessResetPassword');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration"]);
        }, function (error) {
            var msg = _this.translation.translate('UserSession.FailureResetPassword');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    UserResetPasswordComponent.prototype.configureReactiveForm = function () {
        this.userForm = this.fb.group({
            username: { value: this.model.username, disabled: true },
            firstName: { value: this.model.firstName, disabled: true },
            lastName: { value: this.model.lastName, disabled: true },
            grpPassword: this.fb.group({
                password: [{ value: "", disabled: false }, [forms_1.Validators.required, forms_1.Validators.pattern("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,20})")]],
                confirmPassword: [{ value: "", disabled: false }, [forms_1.Validators.required]],
            }, { validator: password_validator_1.PasswordValidator.MatchPassword })
        });
    };
    UserResetPasswordComponent.prototype.prepareUserResetPassword = function () {
        var data = {
            username: this.userForm.get('username').value,
            password: this.userForm.get('grpPassword.password').value,
            confirmPassword: this.userForm.get('grpPassword.confirmPassword').value
        };
        return data;
    };
    UserResetPasswordComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/user/user-resetpassword.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], UserResetPasswordComponent);
    return UserResetPasswordComponent;
}());
exports.UserResetPasswordComponent = UserResetPasswordComponent;
//# sourceMappingURL=user-resetpassword.component.js.map