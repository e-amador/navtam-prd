﻿import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AdminModule } from './admin.module';

const platform = platformBrowserDynamic();
enableProdMode();
platform.bootstrapModule(AdminModule);