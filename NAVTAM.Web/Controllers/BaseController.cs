﻿using Microsoft.AspNet.Identity.Owin;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NAVTAM.App_Start;
using NAVTAM.Helpers;
using System;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace NAVTAM.Controllers
{
    public class BaseController : Controller
    {
        public BaseController(IUow uow, ILogger logger)
        {
            Uow = uow;
            Logger = logger;
        }

        public T LogAction<T>(Func<T> lambda, string method)
        {
            var threadId = GetThreadId();          
            try
            {
                Logger.LogVerbose($"{threadId}) Begin {method}.");
                var result = lambda.Invoke();
                Logger.LogVerbose($"{threadId}) End {method}.");
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, GetType(), $"Error in {method}");
                throw;
            }
        }

        public IApplicationContext ApplicationContext
        {
            get
            {
                return _applicationContext ?? (_applicationContext = new WebApplicationContext(HttpContext, UserManager));
            }
            set
            {
                _applicationContext = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
            }
            set
            {
                _userManager = value;
            }
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = GetCulture();
            if (cultureName != null)
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
            return base.BeginExecuteCore(callback, state);
        }

        protected string GetCulture()
        {
            var cultureCookie = Request.Cookies["_culture"];
            return cultureCookie != null ? cultureCookie.Value : null;
        }

        protected void SetLanguage(string culture)
        {
            SetCultureCookie(culture);
        }

        private void SetCultureCookie(string cultureName)
        {
            HttpCookie cultureCookie = new HttpCookie("_culture")
            {
                Value = cultureName,
                Expires = DateTime.Now.AddYears(10)
            };
            Response.Cookies.Add(cultureCookie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Uow != null)
                {
                    Uow.Dispose();
                    Uow = null;
                }
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }
            }
            base.Dispose(disposing);
        }

        static int GetThreadId() => Thread.CurrentThread.ManagedThreadId;

        protected IUow Uow;
        protected ILogger Logger;

        private ApplicationUserManager _userManager;
        private IApplicationContext _applicationContext;
    }
}