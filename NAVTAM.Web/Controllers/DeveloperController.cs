﻿using System.Web.Mvc;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NAVTAM.ViewModels;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Threading.Tasks;
using NavCanada.Core.Common.Contracts;

namespace NAVTAM.Controllers
{
    [Authorize(Roles = "Developer")]
    public class DeveloperController : BaseController
    {
        public DeveloperController(IUow uow, ILogger logger) : base(uow, logger)
        {
        }

        // GET: Feedback
        public ActionResult Index()
        {
            return View();
        }
    }
}