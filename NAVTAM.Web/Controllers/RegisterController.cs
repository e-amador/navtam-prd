﻿using Microsoft.AspNet.Identity;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Mto;
using NAVTAM.App_Start;
using NAVTAM.ViewModels;
using System;
using System.Configuration;
using System.Data.Entity.Validation;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace NAVTAM.Controllers
{
    public class RegisterController : BaseController
    {
        public RegisterController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IUow uow)
            : base(userManager, signInManager, uow)
        {
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.RecaptchaKey = ConfigurationManager.AppSettings["RecaptchaPublicKey"];
            return View("Register");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            ViewBag.RecaptchaKey = ConfigurationManager.AppSettings["RecaptchaPublicKey"];

            if (ModelState.IsValid)
            {
                if (!ValidRecaptcha())
                {
                    ViewBag.errorMessage = "Invalid recapcha, you must prove you are a human.";
                    return View(model);
                }
                //Verify Terms and Conditions

                try
                {
                    var user = new UserProfile
                    {
                        UserName = model.UserName,
                        FirstName = model.FisrtName,
                        LastName = model.LastName,
                        Email = model.Email,
                        PhoneNumber = model.Telephone,
                        Address = !string.IsNullOrEmpty(model.Address) ? model.Address : model.OrgAddress,
                        Fax = model.Fax,
                        Position = model.Position,
                        LastPasswordChangeDate = DateTime.UtcNow
                    };

                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var registration = await CreateRegistrationObjectAsync(model);
                        var userProfile = await UserManager.FindByNameAsync(model.UserName);
                        await UserManager.AddToRoleAsync(user.Id, CommonDefinitions.UserRole);

                        var tokenConfirmation = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                        Uow.BeginTransaction();
                        try
                        {
                            userProfile.RegistrationId = registration.Id;
                            Uow.UserProfileRepo.Update(userProfile);
                            await Uow.SaveChangesAsync();

                            var mto = CreateMtoForUserRegistration(MessageTransferObjectId.RegistrationNotificationEmail, 
                                null,
                                registration.Id,
                                RegistrationStatus.ConfirmEmail,
                                Uri.EscapeDataString(tokenConfirmation));

                            PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                            Uow.Commit();
                        }
                        catch (Exception)
                        {
                            Uow.Rollback();
                            throw;
                        }
                        return RedirectToAction("Index", "Home");
                    }
                    AddErrors(result);
                }
                catch (DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            raise = new InvalidOperationException("Fail to register user - " + message, raise);
                        }
                    }
                    throw raise;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: {0}", e.Message);
                    throw;
                }
                // If we got this far, something failed, redisplay form
                return View(model);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private async Task<Registration> CreateRegistrationObjectAsync(RegisterViewModel model)
        {
            var registration = await Uow.RegistrationRepo.GetByNameAsync(model.OrgName);
            if (registration != null)
                return registration;

            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
                throw new NullReferenceException("The registration user can't be found in the database");

            registration = new Registration
            {
                Name = model.OrgName,
                Address = model.OrgAddress,
                EmailAddress = model.OrgEmailAddress,
                Telephone = model.OrgTelephone,
                TypeOfOperation = model.TypeOfOperation,
                HeadOffice = model.HeadOffice,
                Status = (int)RegistrationStatus.Initial,
                StatusTime = DateTime.Now,
                RegisteredUserId = user.Id,
                RegisteredUserName = model.UserName,
                RegisteredUserFirstName = model.FisrtName,
                RegisteredUserLastName = model.LastName,
                RegisteredUserEmail = model.Email
            };

            Uow.RegistrationRepo.Add(registration);
            await Uow.SaveChangesAsync();
            return registration;
        }

        private MessageTransferObject CreateMtoForUserRegistration(MessageTransferObjectId messageId, string userId, long registrationId, RegistrationStatus regStatus, string tokenConfirmation)
        {
            var mto = new MessageTransferObject();

            //add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            //registration status
            mto.Add(MessageDefs.RegistrationStatusKey, (int)regStatus, DataType.Int32);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)messageId, DataType.Int32);

            //add unique identifier
            if (regStatus == RegistrationStatus.Completed)
            {
                mto.Add(MessageDefs.EntityIdentifierKey, userId, DataType.String);
            }
            else
            {
                mto.Add(MessageDefs.EntityIdentifierKey, registrationId, DataType.String);
            }

            //Time when message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            //Email Token Confirmation
            if (regStatus == RegistrationStatus.ConfirmEmail && !string.IsNullOrEmpty(tokenConfirmation))
                mto.Add(MessageDefs.EmailTokenConfirmationKey, tokenConfirmation, DataType.String);

            return mto;
        }

        private void PublishMtoToTargetQueue(MessageTransferObject mto, SqlServiceBrokerQueueSettings targetQueueName)
        {
            try
            {
                var targetQueue = new SqlServiceBrokerQueue(Uow.DbContext, targetQueueName);
                targetQueue.Publish(mto);
            }
            catch (Exception ex)
            {
                var message = string.Format("Error publishing Mto to targetQueue [{0}]. Details: ", targetQueueName.ReceiveQueueName);
                Console.WriteLine(ex.Message);
                //_logger.Error(message, ex);
            }
        }

        private bool ValidRecaptcha()
        {
            var response = Request["g-recaptcha-response"];

            var urlTemplate = ConfigurationManager.AppSettings["RecaptchaUrlTemplate"];

            // Note: Remove for release! This is a temp feature for the Testers.
            if (string.IsNullOrEmpty(urlTemplate))
                return true;

            var secretKey = ConfigurationManager.AppSettings["RecaptchaPrivateKey"];
            if (string.IsNullOrEmpty(urlTemplate) || string.IsNullOrEmpty(secretKey))
                throw new Exception("Invalid Recaptcha configuration.");

            var url = string.Format(urlTemplate, secretKey, response);
            var req = WebRequest.Create(url) as HttpWebRequest;
            if (req == null) return false;
            try
            {
                using (var webResponse = req.GetResponse())
                {
                    var responseStream = webResponse.GetResponseStream();
                    if (responseStream == null) return false;
                    using (var reader = new StreamReader(responseStream))
                    {
                        var jsonResponse = reader.ReadToEnd();
                        var js = new JavaScriptSerializer();
                        var data = js.Deserialize<ReCpatchaResponse>(jsonResponse);
                        return Convert.ToBoolean(data.success);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        protected override void Dispose(bool disposing)
        {
            Uow.Dispose();
            base.Dispose(disposing);
        }

        class ReCpatchaResponse
        {
            public string success { get; set; }
        }

    }
}