﻿using Microsoft.AspNet.Identity;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NAVTAM.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IUow uow, ILogger logger) : base(uow, logger)
        {
            _logger = logger;
        }

        [Authorize]
        public async Task<ActionResult> Index()
        {
            return await LogAction(async () =>
            {           
                var userId = User.Identity.GetUserId();
               
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    var roles = await UserManager.GetRolesAsync(userId);

                    var adminAreaAccess = new[] { CommonDefinitions.AdminRole, CommonDefinitions.CcsRole };
                    if (adminAreaAccess.Any(i => roles.Contains(i)))
                        return RedirectToAction("Index", "Administration");

                    var dashboardAccess = new[] { CommonDefinitions.UserRole, CommonDefinitions.OrgAdminRole, CommonDefinitions.NofUserRole, CommonDefinitions.NofAdminRole, CommonDefinitions.FicUserRole };
                    if (dashboardAccess.Any(i => roles.Contains(i)))
                        return RedirectToAction("Index", "Dashboard");

                    var developerAccess = new[] { CommonDefinitions.DeveloperRole };
                    if (developerAccess.Any(i => roles.Contains(i)))
                        return RedirectToAction("Index", "Developer");
                }

                return RedirectToAction("Login", "Account");

            }, "home/index");
        }

        /// <summary>
        /// Home/HealthCheck
        /// </summary>
        /// <returns></returns>
        public ActionResult HealthCheck()
        {
            return LogAction(() => Content("OK"), "HealthCheck");
        }

        protected readonly ILogger _logger;
    }
}