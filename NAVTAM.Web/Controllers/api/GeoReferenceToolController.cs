﻿using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Device.Location;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api")]
    public class GeoReferenceToolController : ApiBaseController
    {
        public GeoReferenceToolController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        { }

        [Route("georef/pointradius")]
        [HttpPost]
        public async Task<IHttpActionResult> PointAndRadius([FromBody] PointViewModel pvm)
        {
            if(pvm.Points.Length == 1 && pvm.PointRadius > 0)
            {
                GeoCoordinate geo = new GeoCoordinate();
                for (int i = 0; i < pvm.Points.Length; i++)
                {
                    string point = pvm.Points[i];

                    if (DMSLocation.IsKnownFormat(point))
                    {
                        var dmsPoint = DMSLocation.FromKnownFormat(point).Round();

                        geo = dmsPoint.ToGeoCoordinate();
                        pvm.CalculatedPoint = dmsPoint.ToDM();
                        pvm.CalculatedRadius = pvm.PointRadius;
                        pvm.InternationalAerodromesDto = await FindClosestAeronauticalAerdromes(geo, GeoDataService.NMToMeters(pvm.PointRadius.Value), true);
                    }
                    else
                    {
                        pvm.Error = $"{Resources.InvalidCoordinate}: {point}";
                    }
                }
            }
            else
            {
                pvm.Error = $"{Resources.PointRadiusReq}";
            }
            return Ok(pvm);
        }

        [Route("georef/calculate")]
        [HttpPost]
        public async Task<IHttpActionResult> CalculateCircle([FromBody] PointViewModel pvm)
        {
            List<DMSLocation> dmsPoints;
            if (pvm.Points.Length > 1)
            {
                #region parsing of the input points

                dmsPoints = new List<DMSLocation>();
                foreach (var point in pvm.Points)
                {
                    if (DMSLocation.IsKnownFormat(point))
                    {
                        var convertedCoordinate = DMSLocation.FromKnownFormat(point);
                        dmsPoints.Add(convertedCoordinate);
                    }
                    else
                    {
                        pvm.Error = $"{Resources.InvalidCoordinate}: {point}";
                        return Ok(pvm);
                    }
                }

                #endregion

                // convert the DMS coordinates to GeoCoordinates
                var geoCoordinates = dmsPoints.Select(p => new GeoCoordinate(p.LatitudeDecimal, p.LongitudeDecimal)).ToList();

                // find the best center
                var bestCenterPoint = GeoCoordinateExt.FindBestCenter(geoCoordinates);

                // convert to DMS and round it
                var dmsRoundedCenter = bestCenterPoint.ConvertToDMSLocation().Round();

                // convert rounded center back to GeoCoordinates
                bestCenterPoint = dmsRoundedCenter.ConvertToGeoCoordinate();

                // recalculate the radius (in meters) based on the rounded center
                var radiusInMeters = geoCoordinates.Max(c => c.DistanceToEx(bestCenterPoint));

                // convert to nautical miles (rounded to the upper integer number)
                var avgRadiusNM = Math.Ceiling(GeoDataService.MetersToNM(radiusInMeters));

                // create circle to display point-to-point
                var avgRadiusRounded = GeoDataService.NMToMeters(avgRadiusNM);

                // update the result of the caculation
                pvm.CalculatedPoint = dmsRoundedCenter.ToDM();
                pvm.CalculatedRadius = avgRadiusNM;

                // return the points as decimal for the client side map rendering
                pvm.Points = dmsPoints.Select(p => ConvertToDecimal(p)).ToArray();

                pvm.InternationalAerodromesDto = await FindClosestAeronauticalAerdromes(bestCenterPoint, avgRadiusRounded); //radiusInMeters

                return Ok(pvm);
            }
            else
            {
                pvm.Error = $"{Resources.RequireMoreThenOneCoordinate}";
                return Ok(pvm);
            }
        }

        private async Task<List<InternationalAerodromeDto>> FindClosestAeronauticalAerdromes(GeoCoordinate point, double radiusInMeters, bool isPointRadiusQuery = false)
        {
            
            var aerodromes = 
                await Uow.SdoCacheRepo.GetNearbyInternationalAerodromesAsync(DbGeography.PointFromText($"POINT ({point.Longitude} {point.Latitude})", CommonDefinitions.Srid),
                CommonDefinitions.NAUTICAL_MILES_IN_METERS_25 + radiusInMeters);
            var internationalAirports = new List<InternationalAerodromeDto>();
            if (aerodromes.Count > 0)
            {
                internationalAirports = new List<InternationalAerodromeDto>();
                foreach (var aerodrome in aerodromes)
                {
                    var intAero = new InternationalAerodromeDto
                    {
                        CodeId = aerodrome.CodeId,
                        Distance = GeoDataService.MetersToNM(aerodrome.Distance - radiusInMeters),
                        ReferencePoint = $"{aerodrome.SdoCache.RefPoint.Latitude} {aerodrome.SdoCache.RefPoint.Longitude}",
                        IsPointRadiusQuery = isPointRadiusQuery                     
                    };

                    internationalAirports.Add(intAero);
                }
            }

            return internationalAirports;
        }

        static string ConvertToDecimal(DMSLocation p) => $"{p.LatitudeDecimal.ToString()} {p.LongitudeDecimal.ToString()}";
    }
}