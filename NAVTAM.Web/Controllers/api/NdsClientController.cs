﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/nof")]
    [Authorize] //[Authorize(Roles = "NofAdministrator")]
    public class NdsClientController : GeographyBaseController
    {
        public NdsClientController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("ndsclients/{id}")]
        public async Task<IHttpActionResult> GetNdsClient(int id)
        {
            return await LogAction(async () =>
            {
                var client = await Uow.NdsClientRepo.GetByIdAsync(id);

                if (client == null)
                    return NotFound();

                var series = from serie in client.Series select serie.Series;
                var itemsA = from serie in client.ItemsA select serie.ItemA;

                var result = new
                {
                    client.Id,
                    ClientName = client.Client,
                    client.ClientType,
                    client.Address,
                    itemsA = itemsA.ToList(),
                    Series = series.ToList(),
                    client.AllowQueries,
                    client.French,
                    client.Operator,
                    client.Active,
                    IsRegionSubscription = client.Region != null,
                    Region = GetRegionViewModel(client.Region),
                    client.LowerLimit,
                    client.UpperLimit
                };

                return Ok(result);

            }, $"api/nof/ndsclients/{id}");
        }

        [HttpGet]
        [Route("ndsclients")]
        public async Task<IHttpActionResult> GetAllNdsClients()
        {
            return await LogAction(async () => Ok(await Uow.NdsClientRepo.GetAllSubscriptionsAsync()), "GET: api/nof/ndsclients");
        }

        [HttpPost]
        [Route("ndsclients")]
        public async Task<IHttpActionResult> SaveNdsClient(NdsClientViewModel model)
        {
            return await LogTransaction(async () =>
            {
                if (!ModelState.IsValid)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                FlattenSubscriptionModel(model);
                var id = (model.Id.HasValue) ? model.Id.Value : -1;
                if (await ClientNameExists(id, model.Client))
                    return ApiStepResult.Fail(BadRequest(String.Format(Resources.ClientNameExists, model.Client)));

                var getRegionStep = GetGeoRegion(model, Uow);
                if (getRegionStep.Failed)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                if (model.IsRegionSubscription) {
                    //Verify if the Region is inside Canada 
                    var nofDoa = await Uow.DoaRepo.GetNofDoaAsync();
                    if (nofDoa != null && !nofDoa.DoaGeoArea.Intersects(getRegionStep.Result))
                        return ApiStepResult.Fail(BadRequest(Resources.RegionOutOfCanada));

                    // Verify the Region is counter clockwise
                    var result = IsAreaLargerThanCanada(getRegionStep.Result);
                    if (result)
                        return ApiStepResult.Fail(BadRequest(Resources.ErrorGeoFileClockwise));
                }

                //Uow.BeginTransaction();
                try
                {
                    var client = new NdsClient
                    {
                        Client = model.Client,
                        ClientType = model.ClientType,
                        Address = model.Address,
                        French = model.French,
                        Operator = User.Identity.Name,
                        AllowQueries = model.AllowQueries,
                        Updated = DateTime.UtcNow,
                        Active = model.Active,
                        Region = getRegionStep.Result,
                        LowerLimit = model.LowerLimit,
                        UpperLimit = model.UpperLimit,
                    };

                    AddClientNewItemAs(model, client);
                    AddClientNewSeries(model, client);

                    Uow.NdsClientRepo.Add(client);
                    await Uow.SaveChangesAsync();

                    //Uow.Commit();

                    model.Id = client.Id;

                    Logger.LogInfo(GetType(), $"Client Subscription '{model.Client}' created  by '{ApplicationContext.GetUserName()}'.");

                    return ApiStepResult.Succeed(Ok(model));
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }
            }, "POST: api/nof/ndsclients");
        }

        [HttpPost]
        [Route("validateregionincanada")]
        public async Task<IHttpActionResult> ValidateRegionInCanada([FromBody] NdsClientViewModel model)
        {
            return await LogAction(async () =>
            {
                var result = await IsRegionInCanada(model, Uow);
                return Ok(result);
            }, "api/nof/validateregionincanada");
        }

        [HttpPut]
        [Route("ndsclients")]
        public async Task<IHttpActionResult> UpdateNdsClient(NdsClientViewModel model)
        {
            return await LogTransaction(async () =>
            {
                if (!ModelState.IsValid)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                if (!model.Id.HasValue)
                    return ApiStepResult.Fail(BadRequest("Id is invalid"));

                if (await ClientNameExists(model.Id.Value, model.Client))
                    return ApiStepResult.Fail(BadRequest(String.Format(Resources.ClientNameExists, model.Client)));


                var client = await Uow.NdsClientRepo.GetByIdAsync(model.Id.Value);
                if (client == null)
                    return ApiStepResult.Fail(NotFound());

                FlattenSubscriptionModel(model);

                var getRegionStep = GetGeoRegion(model, Uow);
                if (getRegionStep.Failed)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                if (model.IsRegionSubscription)
                {
                    //Verify if the Region is inside Canada 
                    var nofDoa = await Uow.DoaRepo.GetNofDoaAsync();
                    if (nofDoa != null && !nofDoa.DoaGeoArea.Intersects(getRegionStep.Result))
                        return ApiStepResult.Fail(BadRequest(Resources.RegionOutOfCanada));

                    // Verify the Region is counter clockwise
                    var result = IsAreaLargerThanCanada(getRegionStep.Result);
                    if (result)
                        return ApiStepResult.Fail(BadRequest(Resources.ErrorGeoFileClockwise));
                }

                //Uow.BeginTransaction();
                try
                {
                    await RemoveClientItemAs(model);
                    await RemoveClientSeries(model);

                    AddClientNewItemAs(model, client);
                    AddClientNewSeries(model, client);

                    client.Client = model.Client;
                    client.ClientType = model.ClientType;
                    client.Address = model.Address;
                    client.French = model.French;
                    client.Operator = User.Identity.Name;
                    client.AllowQueries = model.AllowQueries;
                    client.Updated = DateTime.UtcNow;
                    client.Active = model.Active;

                    client.Region = getRegionStep.Result;
                    client.LowerLimit = model.LowerLimit;
                    client.UpperLimit = model.UpperLimit;

                    await Uow.SaveChangesAsync();

                    //Uow.Commit();

                    Logger.LogInfo(GetType(), $"Client Subscription '{model.Client}' updated by '{ApplicationContext.GetUserName()}'.");

                    return ApiStepResult.Succeed(Ok(model));
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }
            }, "PUT: api/nof/ndsclients");
        }

        [HttpDelete]
        [Route("ndsclients/{id}")]
        public async Task<IHttpActionResult> DeleteNdsClient(int id)
        {
            return await LogTransaction(async () =>
            {
                var client = await Uow.NdsClientRepo.GetByIdAsync(id);
                if (client == null)
                    return ApiStepResult.Fail(NotFound());

                //Uow.BeginTransaction();
                try
                {
                    var itemsA = await Uow.NdsClientItemARepo.GetByClientIdAsync(id);
                    if (itemsA != null)
                    {
                        foreach (var itemA in itemsA)
                        {
                            Uow.NdsClientItemARepo.Delete(itemA);
                        }
                        await Uow.SaveChangesAsync();
                    }

                    var series = await Uow.NdsClientSeriesRepo.GetByClientIdAsync(id);
                    if (series != null)
                    {
                        foreach (var serie in series)
                        {
                            Uow.NdsClientSeriesRepo.Delete(serie);
                        }
                        await Uow.SaveChangesAsync();
                    }

                    Uow.NdsClientRepo.Delete(client);
                    await Uow.SaveChangesAsync();

                    //Uow.Commit();

                    return ApiStepResult.Succeed(Ok());
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }
            }, $"DELETE: api/nof/ndsclients{id}");
        }

        [HttpPost, Route("ndsclients/page", Name = "RegisteredNdsClientPage")]
        public async Task<IHttpActionResult> GetNsdClientsInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > 1000)
                    queryOptions.PageSize = 1000;

                var totalCount = await Uow.NdsClientRepo.GetAllCountAsync();
                var clients = await Uow.NdsClientRepo.GetAllAsync(queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredNdsClientPage", clients, totalCount));

            }, "api/nof/ndsclients/page");
        }

        [HttpPost]
        [Route("ndsclients/uploadfile")]
        public IHttpActionResult UploadDoaGeographyFile()
        {
            return LogAction(() =>
            {
                var validateResult = ValidateUploadedGeographyFile();
                if (validateResult.Succeeded)
                    return Ok(validateResult.Result);

                return BadRequest(validateResult.ErrorMessage);

            }, "api/nof/ndsclients/uploadfile");
        }

        [HttpGet]
        [Route("ndsclients/validatepoint")]
        public async Task<IHttpActionResult> ValidatePoint(string value)
        {
            return await LogAction(async () =>
            {
                var convertResult = await ValidateAndConvertToPoint(value);

                if (convertResult.Succeeded)
                    return Ok(convertResult.Result);

                return BadRequest();

            }, "api/nof/ndsclients/validatepoint");
        }

        [HttpGet]
        [Route("ndsclients/validatepoints")]
        public IHttpActionResult ValidatePoints(string value)
        {
            return LogAction(() =>
            {
                var convertResult = ValidateAndConvertToPolygon(value);
                if (convertResult.Succeeded)
                    return Ok(convertResult.Result);

                return BadRequest();

            }, "api/nof/ndsclients/validatepoints");
        }

        [HttpPost]
        [Route("ndsclients/getgeofeatures")]
        public IHttpActionResult GetGeoFeatures([FromBody] RegionMapRenderViewModel model)
        {
            return LogAction(() => ConvertToGeoFeatures(model), "api/nof/ndsclients/getgeofeatures");
        }

        [HttpGet]
        [Route("ndsclients/exists")]
        public async Task<IHttpActionResult> Exists(int Id, string name)
        {
            return await LogAction(async () => Ok(await ClientNameExists(Id, name)), "api/nof/ndsclients/exists");
        }

        private Task<bool> ClientNameExists(int Id, string name)
        {
            var trimmed = (name ?? "").Trim();
            return Id > 0 ? Uow.NdsClientRepo.Exists(Id, trimmed) : Uow.NdsClientRepo.Exists(trimmed);
        }

        private StepResult<DbGeography> GetGeoRegion(NdsClientViewModel model, IUow uow)
        {
            return model.IsRegionSubscription ? CreateRegionGeography(model.Region, uow) : StepResult<DbGeography>.Succeed(null);
        }

        private RegionViewModel GetRegionViewModel(DbGeography geo)
        {
            if (geo != null)
            {
                return new RegionViewModel()
                {
                    Source = RegionSource.Geography,
                    Geography = geo.ToString(),
                    GeoFeatures = ConvertToGeoJson(geo)
                };
            }
            return null;
        }

        private void FlattenSubscriptionModel(NdsClientViewModel model)
        {
            if (model.IsRegionSubscription)
            {
                model.ItemsA = new string[0];
                model.Series = new string[0];
            }
            else
            {
                model.Region = null;
                model.LowerLimit = 0;
                model.UpperLimit = 0;
            }
        }

        private async Task RemoveClientSeries(NdsClientViewModel model)
        {
            var series = await Uow.NdsClientSeriesRepo.GetByClientIdAsync(model.Id.Value);
            if (series != null && series.Count > 0)
            {
                foreach (var serie in series)
                {
                    Uow.NdsClientSeriesRepo.Delete(serie);
                }
                await Uow.SaveChangesAsync();
            }
        }

        private async Task RemoveClientItemAs(NdsClientViewModel model)
        {
            var itemsA = await Uow.NdsClientItemARepo.GetByClientIdAsync(model.Id.Value);
            if (itemsA != null && itemsA.Count > 0)
            {
                foreach (var itemA in itemsA)
                {
                    Uow.NdsClientItemARepo.Delete(itemA);
                }
                await Uow.SaveChangesAsync();
            }
        }

        static void AddClientNewSeries(NdsClientViewModel model, NdsClient client)
        {
            if (client.Series == null)
            {
                client.Series = new List<NdsClientSeries>();
            }
            else
            {
                client.Series.Clear();
            }
            if (model.Series != null)
            {
                foreach (var serie in model.Series)
                {
                    client.Series.Add(new NdsClientSeries
                    {
                        Series = serie,
                        NdsClient = client
                    });
                }
            }
        }

        static void AddClientNewItemAs(NdsClientViewModel model, NdsClient client)
        {
            if (client.ItemsA == null)
            {
                client.ItemsA = new List<NdsClientItemA>();
            }
            else
            {
                client.ItemsA.Clear();
            }
            if (model.ItemsA != null)
            {
                foreach (var itemA in model.ItemsA)
                {
                    client.ItemsA.Add(new NdsClientItemA
                    {
                        ItemA = itemA,
                        NdsClient = client
                    });
                }
            }
        }

        static async Task<bool> IsRegionInCanada(NdsClientViewModel model, IUow uow)
        {
            var retValue = false;
            var nofDoa = await uow.DoaRepo.GetNofDoaAsync();
            if (nofDoa != null)
            {
                // try to create the geography
                var geoResult = CreateRegionGeography(model.Region, uow);
                if (geoResult.Succeeded)
                {
                    retValue = nofDoa.DoaGeoArea.Intersects(geoResult.Result);
                }
            }
            else
            {
                //There is a problem with the NOF DOA, we return true
                retValue = true;
            }
            return retValue;
        }

    }
}