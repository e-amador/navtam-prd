﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/configvalues")]
    [Authorize]
    public class ConfigurationValuesController : ApiBaseController
    {
        public ConfigurationValuesController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("series")]
        public IHttpActionResult Series()
        {
            return LogAction(() =>
            {
                var lSeries = Uow.SeriesChecklistRepo.GetAvailableSeries();
                return Ok(lSeries);
            }, "api/configvalues/series");
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost, Route("admin/page", Name = "RegisteredConfigPage")]
        public async Task<IHttpActionResult> GetConfigValuesInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.ConfigurationValueRepo.GetAllCountAsync(queryOptions);
                var configValues = await Uow.ConfigurationValueRepo.GetConfigValuesPartials(queryOptions);

                if (configValues.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    configValues = await Uow.ConfigurationValueRepo.GetConfigValuesPartials(queryOptions);
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredConfigPage", configValues, totalCount));

            }, "api/configvalues/admin/page");
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete]
        [Route("admin/delete")]
        public async Task<IHttpActionResult> Delete(string cat, string name)
        {
            return await LogAction(async () =>
            {
                var config = await Uow.ConfigurationValueRepo.GetByCategoryandNameAsync(cat, name);
                if (config == null)
                    return NotFound();

                Uow.ConfigurationValueRepo.Delete(config);
                Uow.SaveChanges();

                Logger.LogInfo(GetType(), $"DELETE: Configuration(Category: '{cat}', Name: '{name}') deleted  by '{ApplicationContext.GetUserName()}'.");

                return Ok();

            }, "api/configvalues/admin/delete");
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        [Route("admin/find")]
        public async Task<IHttpActionResult> Find(string cat, string name)
        {
            return await LogAction(async () =>
            {
                var config = await Uow.ConfigurationValueRepo.GetByCategoryandNameAsync(cat, name);
                if (config == null) return NotFound();

                return Ok(config);

            }, "api/configvalues/admin/find");
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        [Route("admin/exists")]
        public async Task<IHttpActionResult> Exists(string name)
        {
            return await LogAction(async () => Ok(await ConfigNameExists(name)), "api/configvalues/admin/exists");
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [Route("admin/save")]
        public async Task<IHttpActionResult> SaveConfig([FromBody] ConfigurationValueViewModel model)
        {
            return await LogAction(async () =>
            {
                // validate model
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                // validate the name is not empty
                if (string.IsNullOrWhiteSpace(model.Name))
                    return BadRequest(Resources.InvalidConfigValueName);

                var confRec = new ConfigurationValue()
                {
                    Category = model.Category,
                    Description = model.Description,
                    Name = model.Name,
                    Value = model.Value
                };

                // validate name doesn't exists
                if (model.Action == "create")
                {
                    if (await ConfigNameExists(model.Name))
                        return BadRequest(String.Format(Resources.ConfigValueNameExists, model.Name));
                    Uow.ConfigurationValueRepo.Add(confRec);
                }
                else
                {
                    Uow.ConfigurationValueRepo.Update(confRec);
                }
                await Uow.SaveChangesAsync();

                Logger.LogInfo(GetType(), $"SET: Configuration(Category: '{confRec.Category}', Name: '{confRec.Name}', Value: '{confRec.Value}') modified  by '{ApplicationContext.GetUserName()}'.");

                return Ok(model);

            }, "api/configvalues/admin/save");
        }

        private Task<bool> ConfigNameExists(string name)
        {
            var name_trimmed = (name ?? "").Trim();
            return Uow.ConfigurationValueRepo.Exists(name_trimmed);
        }
    }
}