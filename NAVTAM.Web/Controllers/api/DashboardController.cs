﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/Dashboard")]
    [Authorize]
    public class DashboardController : ApiBaseController
    {
        public DashboardController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [Route("getnsds")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNsdsForOrganization()
        {
            return await LogAction(async () =>
            {
                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                if (orgId == 0)
                    return BadRequest(Resources.InvalidOrganizationId);

                var query = await Uow.NsdCategoryRepo.GetActiveNsdsByOrganizationId(orgId);

                var nsds = query.Select(nsd => new NsdCategory
                {
                    Id = nsd.Id,
                    Name = nsd.Name,
                    ParentCategory = nsd.ParentCategory
                });

                return Ok(nsds);

            }, "api/dashboard/getnsds");
        }
    }
}