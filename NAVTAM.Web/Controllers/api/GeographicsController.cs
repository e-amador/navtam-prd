﻿using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/geo")]
    //[Authorize(Roles = "Administrator, CCS, NOF_ADMINISTRATOR")]
    [Authorize(Roles = "Administrator")]
    public class GeographicsController : ApiBaseController
    {
        public GeographicsController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        #region FIRS

        [HttpGet]
        [Route("FixFIRs")]
        public IHttpActionResult FixFirs()
        {
            return LogAction(() => Ok(FixAndUpdateFIRs()), "api/geo/FixFIRs");
        }

        [HttpGet]
        [Route("GetFIRGeo")]
        public async Task<IHttpActionResult> GetFIRGeo(string id)
        {
            return await LogAction(async () =>
            {
                var fir = await Uow.SdoCacheRepo.GetFirByDesignatorAsync(id);
                return Ok(fir != null ? fir.Geography?.AsText() : $"FIR with designator {id} was not found!");
            }, "api/geo/GetFIRGeo");
        }

        private string FixAndUpdateFIRs()
        {
            Uow.BeginTransaction();
            var firs = Uow.SdoCacheRepo.GetFirs();
            try
            {
                foreach (var fir in firs)
                {
                    var fixedGeo = fir.Geography.Normalize();
                    if (fir.Geography != fixedGeo)
                    {
                        fir.Geography = fixedGeo;
                        Uow.SdoCacheRepo.Update(fir);
                        Uow.SaveChanges();
                    }
                }
                Uow.Commit();
                return "Fixing FIRs completed succesfully";
            }
            catch (Exception ex)
            {
                Uow.Rollback();
                return $"Fixing FIRs failed with error: {ex.Message}";
            }
        }

        #endregion

        #region GeoRegions

        [HttpGet]
        [Route("FixGeoRegions")]
        public IHttpActionResult FixGeoRegions()
        {
            return LogAction(() => Ok(FixAndUpdateRegions()), "api/geo/FixGeoRegions");
        }

        [HttpGet]
        [Route("GetGeoRegion")]
        public IHttpActionResult GetGeoRegion(string name)
        {
            return LogAction(() =>
            {
                var region = Uow.GeoRegionRepo.GetAll().FirstOrDefault(r => r.Name == name);
                return Ok(region != null ? region.Geo.AsText() : $"Region named '{name}' was not found!");
            }, "api/geo/GetGeoRegion");
        }

        [HttpGet]
        [Route("GetGeoRegions")]
        public async Task<IHttpActionResult> GetGeoRegions()
        {
            return await LogAction(async () =>
            {
                var regions = await Uow.GeoRegionRepo.GetAllGeoRegionsAsync();
                return regions != null ? Ok(regions) as IHttpActionResult : BadRequest();
            }, "api/geo/GetGeoRegions");
        }

        [HttpGet]
        [HttpPost, Route("admin/page", Name = "RegisteredGeoRegionPage")]
        public async Task<IHttpActionResult> GetGeoRegionsInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.GeoRegionRepo.GetAllCountAsync();
                var regions = await Uow.GeoRegionRepo.GetGeoRegionPartials(queryOptions);

                if (regions.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    regions = await Uow.GeoRegionRepo.GetGeoRegionPartials(queryOptions);
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredGeoRegionPage", regions, totalCount));

            }, "api/geo/admin/page");
        }

        [HttpDelete]
        [Route("admin/delete")]
        public async Task<IHttpActionResult> Delete(int geoId)
        {
            return await LogAction(async () =>
            {
                var region = await Uow.GeoRegionRepo.GetByIdAsync(geoId);
                if (region == null)
                    return NotFound();

                Uow.GeoRegionRepo.Delete(region);
                Uow.SaveChanges();

                Logger.LogInfo(GetType(), $"DELETE: GeoRegion('{region.Name}') deleted  by '{ApplicationContext.GetUserName()}'.");

                return Ok();

            }, "api/geo/admin/delete");
        }

        [HttpGet]
        [Route("admin/find")]
        public async Task<IHttpActionResult> Find(int geoId)
        {
            return await LogAction(async () =>
            {
                var region = await Uow.GeoRegionRepo.GetByIdAsync(geoId);
                if (region == null)
                    return NotFound();

                // todo: add features to render in map (when we put the map).
                var featureGeo = ConvertToGeoJson(region.Geo);

                var geoViewModel = new GeoRegionViewModel
                {
                    Id = region.Id,
                    Name = region.Name,
                    Region = new RegionViewModel
                    {
                        Source = RegionSource.Geography,
                        Geography = region.Geo.ToString(),
                        GeoFeatures = featureGeo,
                        Fir = "",
                        Location = "",
                        Points = "",
                        Radius = 5
                    }
                };

                return Ok(geoViewModel);

            }, "api/geo/admin/find");
        }

        [HttpPost]
        [Route("admin/save")]
        public async Task<IHttpActionResult> SaveGeoRegion([FromBody] GeoRegionViewModel model)
        {
            return await LogAction(async () =>
            {
                // validate model
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                // validate the name is not empty
                if (string.IsNullOrWhiteSpace(model.Name))
                    return BadRequest(Resources.InvalidDoaName);

                // validate name doesn't exists
                if (await GeoRegionNameExists(model.Id, model.Name))
                    return BadRequest(String.Format(Resources.GeoRegionNameExists, model.Name));

                // try to create the geography
                var geoResult = CreateGeoRegionGeography(model.Region, Uow);
                if (geoResult.Failed)
                    return BadRequest(geoResult.ErrorMessage);

                // try to add/update the Geographic region to the repository
                var geoRegion = new GeoRegion()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Geo = ModifyDoaGeography(geoResult.Result, model.Changes),
                };

                // add or update
                if (model.Id == 0)
                {
                    Uow.GeoRegionRepo.Add(geoRegion);
                }
                else
                {
                    Uow.GeoRegionRepo.Update(geoRegion);
                }

                await Uow.SaveChangesAsync();

                Logger.LogInfo(GetType(), $"MODIFY: GeoRegion('{geoRegion.Name}') modified by '{ApplicationContext.GetUserName()}'.");

                return Ok(geoRegion.Id);

            }, "api/geo/admin/save");
        }

        [HttpGet]
        [Route("admin/exists")]
        public async Task<IHttpActionResult> Exists(int geoId, string name)
        {
            return await LogAction(async () => Ok(await GeoRegionNameExists(geoId, name)), "api/geo/admin/exists");
        }

        private string FixAndUpdateRegions()
        {
            Uow.BeginTransaction();
            var regions = Uow.GeoRegionRepo.GetAll().ToList();
            try
            {
                foreach (var region in regions)
                {
                    var fixedGeo = region.Geo.Normalize();
                    if (region.Geo != fixedGeo)
                    {
                        region.Geo = fixedGeo;
                        Uow.GeoRegionRepo.Update(region);
                        Uow.SaveChanges();
                    }
                }
                Uow.Commit();
                return "Fixing regions completed succesfully";
            }
            catch (Exception ex)
            {
                Uow.Rollback();
                return $"Fixing regions failed with error: {ex.Message}";
            }
        }

        private Task<bool> GeoRegionNameExists(int geoId, string name)
        {
            var trimmed = (name ?? "").Trim();
            return Uow.GeoRegionRepo.Exists(geoId, trimmed);
        }

        static StepResult<DbGeography> CreateGeoRegionGeography(RegionViewModel region, IUow uow)
        {
            try
            {
                switch (region.Source)
                {
                    case RegionSource.Geography:
                        return StepResult<DbGeography>.Succeed(ParseGeography(region.Geography));
                    case RegionSource.FIR:
                        return StepResult<DbGeography>.Succeed(GetFirGeography(region.Fir, uow));
                    case RegionSource.Points:
                        return StepResult<DbGeography>.Succeed(GetPolygonGeography(region.Points));
                    case RegionSource.PointAndRadius:
                        return StepResult<DbGeography>.Succeed(GetPointAndRadiusGeography(region.Location, region.Radius));
                    default:
                        return StepResult<DbGeography>.Fail(Resources.DoaGeographyNotImplemented);
                }
            }
            catch (Exception ex)
            {
                return StepResult<DbGeography>.Fail(ex.Message);
            }
        }

        static DbGeography ParseGeography(string text)
        {
            var geoText = ConvertToGeoText(text);
            return DbGeography.FromText(geoText.Text, geoText.Srid);
        }

        static DbGeography GetFirGeography(string firId, IUow uow)
        {
            var fir = uow.SdoCacheRepo.GetById(firId);

            if (fir == null || fir.Type != SdoEntityType.Fir)
                throw new Exception(String.Format(Resources.FIRNotFound, firId));

            return fir.Geography;
        }

        static DbGeography GetPolygonGeography(string points)
        {
            // parse location list from points
            var locations = (points ?? "").Split(',').Select(s => s.Trim()).ToList();

            // validate number of points
            if (locations.Count < 3)
                throw new Exception(String.Format(Resources.DoaGeoMore3Points, points));

            if (locations.Exists(l => !DMSLocation.IsValidDecimal(l)))
                throw new Exception(String.Format(Resources.DoaGeoInvalidPoint, points));

            // close the polygon if not closed
            if (locations.First() != locations.Last())
                locations.Add(locations[0]);

            // make points counterclockwise
            if (CalcSignedArea(locations.Select(l => new DecimalLocation(l)).ToList()) > 0)
                locations.Reverse();

            // assemble the points again
            var poly = string.Join(",", locations.Select(l => DMSLocation.FlipCoordinates(l)));

            return DbGeography.PolygonFromText($"POLYGON(({poly}))", DbGeography.DefaultCoordinateSystemId);
        }

        static DbGeography GetPointAndRadiusGeography(string location, int radius)
        {
            if (!DMSLocation.IsKnownFormat(location))
                throw new Exception(String.Format(Resources.DoaGeoInvalidPoint, location));

            var geoPoint = GetGeoPointFromLocation(DMSLocation.ToDecimalLatitudeLongitude(location));
            var nmRadius = GeoDataService.NMToMeters(radius);

            return geoPoint.Buffer(nmRadius);
        }

        static GeoText ConvertToGeoText(string text)
        {
            if ((text ?? "").StartsWith("SRID="))
            {
                var geoPos = text.IndexOf(';') + 1;
                var srid = ParseSrid(text.Substring(0, geoPos - 1));
                return new GeoText() { Srid = srid, Text = text.Substring(geoPos) };
            }
            return new GeoText() { Srid = DbGeography.DefaultCoordinateSystemId, Text = text };
        }

        static double CalcSignedArea(List<DecimalLocation> points)
        {
            var signedArea = 0.0;
            for (var i = 0; i < points.Count - 1; i++)
            {
                var current = points[i];
                var next = points[i + 1];
                signedArea += current.Latitude * next.Longitude - next.Latitude * current.Longitude;
            }
            return signedArea;
        }

        static DbGeography ModifyDoaGeography(DbGeography geo, List<DoaRegionChange> changes)
        {
            if (changes != null)
            {
                foreach (var p in changes.Where(c => c.ChangeType == DoaRegionChangeType.Add))
                {
                    var area = GetPointAndRadiusGeography(p.Location, p.Radius);
                    geo = geo.Union(area);
                }
                foreach (var p in changes.Where(c => c.ChangeType == DoaRegionChangeType.Remove))
                {
                    var area = GetPointAndRadiusGeography(p.Location, p.Radius);
                    geo = geo.Difference(area);
                }
            }
            return geo;
        }

        static DbGeography GetGeoPointFromLocation(string location)
        {
            return DbGeography.PointFromText($"POINT({DMSLocation.FlipCoordinates(location)})", DbGeography.DefaultCoordinateSystemId);
        }

        static int ParseSrid(string text)
        {
            var sridText = text.Substring("SRID=".Length);
            int srid;
            return int.TryParse(sridText, out srid) ? srid : DbGeography.DefaultCoordinateSystemId;
        }

        struct GeoText
        {
            public int Srid;
            public string Text;
        }

        #endregion

        #region Bilingual Region

        [HttpGet]
        [Route("FixBilingualRegion")]
        public IHttpActionResult FixBilingualRegion()
        {
            return LogAction(() => Ok(FixAndUpdateBilingualRegion()), "api/geo/FixBilingualRegion");
        }

        [HttpGet]
        [Route("GetBilingualRegion")]
        public IHttpActionResult GetBilingualRegion()
        {
            return LogAction(() =>
            {
                var region = Uow.DoaRepo.GetBilingualRegion()?.DoaGeoArea;
                if (region == null)
                    return NotFound();

                return Ok(region.AsText());

            }, "api/geo/GetBilingualRegion");
        }

        private string FixAndUpdateBilingualRegion()
        {
            try
            {
                var bilingualRegionDoa = Uow.DoaRepo.GetBilingualRegion();
                if (bilingualRegionDoa != null)
                {
                    var fixedGeo = bilingualRegionDoa.DoaGeoArea.Normalize();
                    if (fixedGeo != bilingualRegionDoa.DoaGeoArea)
                    {
                        bilingualRegionDoa.DoaGeoArea = fixedGeo;
                        Uow.DoaRepo.Update(bilingualRegionDoa);
                        Uow.SaveChanges();
                    }
                }
                return "Fixing bilingual regions completed succesfully";
            }
            catch (Exception ex)
            {
                return $"Fixing bilingual regions failed with error: {ex.Message}";
            }
        }

        #endregion

        #region DOAs

        [HttpGet]
        [Route("FixDoas")]
        public IHttpActionResult FixDoas()
        {
            return LogAction(() => Ok(FixAndUpdateDoas()), "api/geo/FixDoas");
        }

        private string FixAndUpdateDoas()
        {
            Uow.BeginTransaction();
            var doas = Uow.DoaRepo.GetAll().ToList();
            try
            {
                foreach (var doa in doas)
                {
                    var fixedGeo = doa.DoaGeoArea.Normalize();
                    if (doa.DoaGeoArea != fixedGeo)
                    {
                        doa.DoaGeoArea = fixedGeo;
                        Uow.DoaRepo.Update(doa);
                        Uow.SaveChanges();
                    }
                }
                Uow.Commit();
                return "Fixing DOAs completed succesfully";
            }
            catch (Exception ex)
            {
                Uow.Rollback();
                return $"Fixing DAOs failed with error: {ex.Message}";
            }
        }

        #endregion
    }
}
