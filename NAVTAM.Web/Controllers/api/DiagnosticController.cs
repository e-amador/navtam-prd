﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Common.Security;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.TaskEngine.Mto;
using NAVTAM.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace NAVTAM.Controllers.api
{
    [Authorize(Roles = "Developer")]
    [RoutePrefix("api/diagnostic")]
    public class DiagnosticController : ApiBaseController
    {
        readonly INotamQueuePublisher _queuePublisher;

        public DiagnosticController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, INotamQueuePublisher queuePublisher, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _queuePublisher = queuePublisher;
        }

        #region TASK ENGINE
        [HttpGet]
        [Route("te")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var configValue = await Uow.ConfigurationValueRepo.GetByNameAsync("Health-TE");
                if (configValue != null)
                {
                    if (configValue.Value == "1")
                    {
                        configValue.Value = "0";
                        configValue.Description = "";
                        Uow.ConfigurationValueRepo.Update(configValue);
                    }
                }
                else
                {
                    Uow.ConfigurationValueRepo.Add(new ConfigurationValue { Category = "Diagnostic", Name = "Health-TE", Value = "0", Description = "" });
                }

                await Uow.SaveChangesAsync();

                PublishMessageQueue(MessageTransferObjectId.Diagnostic);

                //We should wait a second to give TE the time to process the message
                await Task.Delay(3000);

                DetachEntity();
                configValue = await Uow.ConfigurationValueRepo.GetByNameAsync("Health-TE");

                var result = new
                {
                    Health = configValue.Value,
                    Instance = configValue.Description
                };

                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        #endregion

        #region HUB
        [HttpGet]
        [Route("hub/test")]
        public async Task<IHttpActionResult> TestHubConnection()
        {
            try
            {
                PublishMessageQueue(MessageTransferObjectId.HubConnectionTestMsg);

                //We should wait a second to give TE the time to process the message
                await Task.Delay(10000);

                var status = Uow.SystemStatusRepo.GetStatus();

                var result = new
                {
                    status.Status,
                    Description = $"0 is normal function",
                    status.LastError,
                    status.LastUpdated
                };

                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("hub/in")]
        public IHttpActionResult NdsIncomingMessages()
        {
            try
            {
                List<NdsIncomingRequest> incomingMessages = Uow.NdsIncomingRequestRepo.GetLatestMessages();
                JObject messages = new JObject(
                    new JObject(
                            new JProperty($"NdsIncomingRequests",
                                new JArray(
                                    from m in incomingMessages
                                    orderby m.RecievedDate descending
                                    select new JObject(
                                        new JProperty("MsgId:", m.MessageId),
                                        new JProperty("Msg type:", m.MessageType),
                                        new JProperty("ProcessedDate:", m.ProcessedDate),
                                        new JProperty("RecievedDate:", m.RecievedDate),
                                        new JProperty("MetaData:", m.Metadata)
                                    )
                                )
                            )
                        )
                );

                return Ok(messages);
            }
            catch (Exception e)
            {
                return Ok(e);
            }
        }


        [HttpGet]
        [Route("hub/msgs")]
        public IHttpActionResult NdsCount()
        {
            try
            {
                int incMsg = Uow.NdsIncomingRequestRepo.GetCount();
                int outMsg = Uow.NdsOutgoingMessageRepo.GetCount();

                return Ok($"Inc: {incMsg} Out:{outMsg}");
            }
            catch(Exception e)
            {
                return Ok(e);
            }
        }
              
        [HttpGet]
        [Route("hub/out")]
        public IHttpActionResult NdsOutgoingMessages()
        {
            try
            {
                List<NdsOutgoingMessage> outGoing =  Uow.NdsOutgoingMessageRepo.GetLatestMessages();
                JObject messages = new JObject(
                    new JObject(
                            new JProperty($"NdsOutGoingMessages",
                                new JArray(
                                    from m in outGoing
                                    orderby m.GroupId, m.SubmitDate descending
                                    select new JObject(
                                        new JProperty("Destination:", m.Destination),
                                        new JProperty("Format:", m.Format),
                                        new JProperty("Content:", m.Content),
                                        new JProperty("Status == | 0 Pending | 1 Sent | 2 Confirmed | 3 Failed | :", m.Status),
                                        new JProperty("SubmitDate:", m.SubmitDate)
                                    )
                                )
                            )
                        )
                );

                return Ok(messages);
            }
            catch (Exception e)
            {
                return Ok(e);
            }
        }
        #endregion

        #region LOGS
        [HttpGet]
        [Route("logs/from/{date}/{time}/{count:int?}")]
        public async Task<IHttpActionResult> GetFrom(string date, string time, int count = 1000)
        {
            try
            {
                // expected date format yyyy-MM-dd HHmmss 2019-05-23 180000
                var dateFormat = "yyyy-MM-dd HHmmss";
                DateTime fromDate;
                if (!DateTime.TryParseExact($"{date} {time}", dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out fromDate))
                {
                    return Ok($"Invalid date format found '{date}/{time}'. Use 'yyyy-MM-dd/HHmmss' E.g.: '2019-05-23/184500'");
                }

                using (var ctx = new AppDbContext(LogConnectionString))
                {
                    var logs = await ctx.Logs.Where(e => e.TimeStamp <= fromDate).OrderByDescending(e => e.TimeStamp).Take(count).ToListAsync();

                    if (logs.Count == 0)
                        return Ok($"Nothing logged before {fromDate}!");

                    return ObjectToCsv(logs.Reverse<Log>(), $"logs-{fromDate.ToString()}-{count}.log", '\t');
                }
            }
            catch(Exception e)
            {
                return Ok(e);
            }
        }

        [HttpGet]
        [Route("logs/last")]
        public async Task<IHttpActionResult> Last1000()
        {
            try
            {
                // expected date format yyyy-MM-dd HHmmss 2019-05-23 180000
                using (var ctx = new AppDbContext(LogConnectionString))
                {
                    var logs = await ctx.Logs.OrderByDescending(e => e.TimeStamp).Take(1000).ToListAsync();

                    if (logs.Count == 0)
                        return Ok($"Nothing logged!");

                    return ObjectToCsv(logs.Reverse<Log>(), $"logs.log", '\t');
                }
            }
            catch(Exception e)
            {
                return Ok(e);
            }
          
        }          

        [HttpGet]
        [Route("logs/status")]
        public IHttpActionResult CheckLogStatus()
        {
            try
            {
                if (!Logger.CheckLogStatus())
                {
                    return Ok("Log is currently down. Contact administrator.");
                }

                return Ok("Log is functioning normally");
            }
            catch(Exception e)
            {
                return Ok(e);
            }
            
        }

        [HttpGet]
        [Route("logs/level")]
        public IHttpActionResult CheckLogLevel()
        {
            //if (!Logger.CheckLogStatus())
            //{
            //    return Ok("Log is currently down. Contact administrator.");
            //}

            return Ok($"Level is: {Logger.CheckLogLevel()}");
        }

        #endregion

    

        #region SYSTEM STATUS
        [HttpGet]
        [Route("system/status")]
        public IHttpActionResult CheckStatus() => LogAction(() => Ok(GetStatus(Uow)), "DEVELOPER INITIATED QUERY: CHECK SYSTEM STATUS");
        #endregion

        #region PRIVATE METHODS
        private IHttpActionResult ObjectToCsv(IEnumerable<Log> logs, string fileName, char sep = ',')
        {
            var sb = new StringBuilder();

            // headers
            sb.Append($"Id{sep}Timestamp{sep}Level{sep}Message{sep}Exception").AppendLine();

            // rows
            foreach (var log in logs)
            {
                sb.Append($"{log.Id}").Append(sep)
                  .Append(log.TimeStamp).Append(sep)
                  .Append(log.Level).Append(sep)
                  .Append(log.Message?.Replace(sep, ' ')).Append(sep)
                  .Append(log.Exception?.Replace(sep, ' '))
                  .AppendLine();
            }

            var bytes = Encoding.Unicode.GetBytes(sb.ToString());
            sb = null;

            MemoryStream stream = new MemoryStream(bytes);

            HttpResponseMessage httpResponse = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = new StreamContent(stream)
            };
            httpResponse.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName //$"logs-{start}-{end}.csv"
            };
            httpResponse.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/csv");

            ResponseMessageResult responseMsgResult = ResponseMessage(httpResponse);

            stream = null;

            return responseMsgResult;
        }

        private void PublishMessageQueue(MessageTransferObjectId messageId)
        {
            var mto = MtoHelper.CreateMessage((int)messageId, m =>
            {
                m.Add(MessageDefs.DiagnosticType, MessageDefs.TaskEngineHealthCheck, DataType.String);
            });
            _queuePublisher.Publish(Uow, mto);
        }

        private void DetachEntity()
        {
            var changedEntriesCopy = Uow.DbContext.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Unchanged ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
            {
                entry.State = EntityState.Detached;
            }
        }

        static string LogConnectionString =>
            CipherUtils.DecryptConnectionString((ConfigurationManager.ConnectionStrings["LogConnection"] ?? ConfigurationManager.ConnectionStrings["DefaultConnection"])?.ConnectionString);
        static SystemStatus GetStatus(IUow uow) => uow.SystemStatusRepo.GetStatus();
    }
    #endregion
}