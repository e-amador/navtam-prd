﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/originatorinfo")]
    [Authorize]
    public class OriginatorController : ApiBaseController
    {
        public OriginatorController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("filter")]
        public async Task<IHttpActionResult> MatchAllFilter([FromUri] ResourceQuery res)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();


                int orgId;
                if( !int.TryParse(res.OrgId, out orgId))
                    return BadRequest("Invalid organization");

                var originators = await Uow.OriginatorInfoRepo.FilterAsync(res.Query, res.Size, orgId);

                return Ok(originators);

            }, "api/originatorinfo/filter");
        }

        [HttpPost]
        [Route("save")]
        public async Task<IHttpActionResult> Save(OriginatorInfoDto model)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var originator = await Uow.OriginatorInfoRepo.FindAsync(model.Text, model.OrganizationId);
                if(originator == null)
                {
                    originator = new OriginatorInfo
                    {
                        Fullname = model.Text,
                        OrganizationId = model.OrganizationId,
                        Email = model.Email,
                        Phone = model.Phone
                    };

                    Uow.OriginatorInfoRepo.Add(originator);
                }
                else
                {
                    originator.Email = model.Email;
                    originator.Phone = model.Phone;

                    Uow.OriginatorInfoRepo.Update(originator);
                }
                await Uow.SaveChangesAsync();

                return Ok();

            }, "api/originatorinfo");
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var originator = await Uow.OriginatorInfoRepo.GetByIdAsync(id);
                if (originator != null)
                {
                    Uow.OriginatorInfoRepo.Delete(originator);
                    await Uow.SaveChangesAsync();

                    return Ok();
                }

                return NotFound();


            }, "api/originatorinfo");
        }



    }
}