﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.App_Start;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/admin")]
    [Authorize(Roles = "Administrator, Organization Administrator, CCS, NOF_ADMINISTRATOR")]
    public class UserController : ApiBaseController
    {
        public UserController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ApplicationUserManager userManager, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _userManager = userManager;
        }

        [HttpGet, Route("user/roles")]
        public async Task<IHttpActionResult> UserRoles()
        {
            return await LogAction(async () => Ok(await Uow.UserProfileRepo.GetRolesAsync()), "api/admin/user/roles");
        }

        [HttpGet, Route("user/availableroles")]
        public Task<IHttpActionResult> AvailableUserRolesByOrg(int id)
        {
            return LogAction(() => GetAvailableUserRolesByOrg(id), "api/admin/user/availableroles");
        }

        public async Task<IHttpActionResult> GetAvailableUserRolesByOrg(int id)
        {
            var userRoles = new List<UserRoleDto>();
            var org = await Uow.OrganizationRepo.GetByIdAsync(id);
            if (org != null)
            {
                var roles = await Uow.ApplicationRoleRepo.GetAllAsync();
                if (org.Type == OrganizationType.Nof)
                {
                    var role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.NofAdminRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                    role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.NofUserRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                }
                else if (org.Type == OrganizationType.Fic)
                {
                    var role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.OrgAdminRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                    role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.FicUserRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                }
                else if (org.Type == OrganizationType.External)
                {
                    var role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.OrgAdminRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                    role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.UserRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                }
                else
                {
                    var role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.OrgAdminRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                    role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.AdminRole, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                    role = roles.Find(r => String.Compare(r.Name, CommonDefinitions.CcsUser, true) == 0);
                    userRoles.Add(new UserRoleDto
                    {
                        RoleId = role.Id,
                        RoleName = role.Name
                    });
                }
            }
            return Ok(userRoles);
        }

        [HttpGet, Route("users")]
        public async Task<IHttpActionResult> Get(string username)
        {
            return await LogAction(async () =>
            {
                var orgId = await GetScopeOrganizationId();
                var user = await Uow.UserProfileRepo.GetUserInfoAsync(username);

                if (user == null)
                    return NotFound();

                if (orgId.HasValue && orgId.Value != user.OrganizationId)
                    return NotFound();

                // check the administrator has access to this user
                var doas = await Uow.UserDoaRepo.GetDoasByUserAsync(user.Id);
                user.DoaIds = string.Join(",", doas.Select(d => d.DoaId));

                return Ok(user);

            }, "api/admin/users");
        }

        [HttpPost, Route("users/page", Name = "RegisteredUsersPage")]
        public async Task<IHttpActionResult> GetUsersInPage(bool includeDisabled, bool includeDeleted, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                queryOptions.PageSize = Math.Min(queryOptions.PageSize, MaxRecordsToReturn);

                var orgId = await GetScopeOrganizationId();
                var totalCount = await (orgId.HasValue ?
                            Uow.UserProfileRepo.GetUserCountInOrganization(orgId.Value, includeDisabled, includeDeleted, queryOptions) :
                            Uow.UserProfileRepo.GetAllCountAsync(includeDisabled, includeDeleted, queryOptions));

                var users = await Uow.UserProfileRepo.GetAllAsync(queryOptions, orgId, includeDisabled, includeDeleted);

                if (users.Count == 0 && queryOptions.Page > 1) {
                    queryOptions.Page = queryOptions.Page - 1;
                    users = await Uow.UserProfileRepo.GetAllAsync(queryOptions, orgId, includeDisabled, includeDeleted);
                }
                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredUsersPage", users, totalCount));

            }, "api/admin/users/page");
        }

        [HttpPost, Route("users")]
        public Task<IHttpActionResult> Create([FromBody]UserViewModel userVm)
        {
            return LogTransaction(() => ExecuteCreate(userVm), "POST: api/admin/users");
        }

        private async Task<ApiStepResult> ExecuteCreate([FromBody]UserViewModel userVm)
        {
            if (!ModelState.IsValid)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

            var isOrgAmin = await IsOrgAdmin();
            if (isOrgAmin)
            {
                var orgId = GetOrganizationId();
                if (orgId != userVm.OrganizationId)
                {
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.InvalidOrganizationId));
                }
            }

            userVm.Password = "^Zx0!" + Guid.NewGuid().ToString("n").Substring(0, 8);

            if (userVm.Deleted)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ErrorUserCantBeDeleted));

            UserProfile user = null;
            try
            {
                user = new UserProfile
                {
                    UserName = userVm.Username.Trim(),
                    Email = userVm.Email,
                    FirstName = userVm.FirstName,
                    LastName = userVm.LastName,
                    Address = userVm.Address,
                    IsApproved = true, //Always is approved.
                    Disabled = userVm.Disabled,
                    Fax = userVm.Fax,
                    PhoneNumber = userVm.PhoneNumber,
                    Position = userVm.Position,
                    OrganizationId = userVm.OrganizationId,
                    EmailConfirmed = false,
                    LockoutEnabled = true,
                    LastPasswordChangeDate = DateTime.UtcNow,
                };

                var result = await UserManager.CreateAsync(user, userVm.Password);
                if (result.Succeeded)
                {
                    result = await UserManager.SetLockoutEnabledAsync(user.Id, userVm.Disabled);
                    if (result.Succeeded)
                    {
                        //Role
                        var roleIds = userVm.RoleId.Split(',').Select(e => e).ToList();
                        if (roleIds.Any())
                        {
                            var appRoles = await Uow.ApplicationRoleRepo.GetByRoleIdsAsync(roleIds);
                            foreach (var appRole in appRoles)
                            {
                                if (result.Succeeded)
                                {
                                    result = await UserManager.AddToRoleAsync(user.Id, appRole.Name);
                                }
                                else
                                    throw new InvalidOperationException(Resources.ErrorCantAddRolesToUser);
                            }
                        }

                        UserRegistration userRegistration = null;
                        var onDisasterRecovery = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";

                        if (onDisasterRecovery)
                        {
                            userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(user.Id);
                        }

                        //string tokenConfirmation = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var tokenConfirmation = await UserManager.GenerateUserTokenAsync("EmailConfirmation", user.Id);

                        try
                        {
                            var doaIds = userVm.DoaIds.Split(',').Select(e => e).ToList();
                            if (doaIds.Any())
                            {
                                var doas = await Uow.DoaRepo.GetByDoaIdsAsync(doaIds);
                                foreach (var doa in doas)
                                {
                                    var ud = new UserDoa
                                    {
                                        UserId = user.Id,
                                        DoaId = doa.Id,
                                        OrganizationId = (user.OrganizationId.HasValue) ? user.OrganizationId.Value : -1
                                    };
                                    Uow.UserDoaRepo.Add(ud);
                                }

                                if (onDisasterRecovery)
                                {
                                    if (userRegistration == null)
                                    {
                                        userRegistration = new UserRegistration
                                        {
                                            UserProfileId = user.Id,
                                            VerificationCode = tokenConfirmation
                                        };
                                        Uow.UserRegistrationRepo.Add(userRegistration);
                                    }
                                    else
                                    {
                                        userRegistration.VerificationCode = tokenConfirmation;
                                        Uow.UserRegistrationRepo.Update(userRegistration);
                                    }
                                }

                                await Uow.SaveChangesAsync();

                                if (!onDisasterRecovery)
                                {
                                    try
                                    {
                                        await SendVerificationEmail(user.Id, tokenConfirmation);
                                    }
                                    catch (Exception)
                                    {
                                        //Ignore exception if not message was sent;
                                        //return ApiStepResult.Fail(Content(HttpStatusCode.RequestTimeout, "The SMTP mail server is not responding."));
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                    else
                        throw new InvalidOperationException(Resources.ErrorLockoutEnable);
                }
                else
                {
                    throw new FieldAccessException(result.Errors.FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                if (!string.IsNullOrEmpty(user.Id))
                {
                    var saveUser = await UserManager.FindByIdAsync(user.Id);
                    if (saveUser != null)
                    {
                        //Delete user and roles if user was created and something failed down the path.
                        var roles = await UserManager.GetRolesAsync(user.Id);
                        if (roles.Any())
                        {
                            foreach (var r in roles)
                            {
                                await UserManager.RemoveFromRoleAsync(user.Id, r);
                            }
                        }
                        await UserManager.DeleteAsync(user);
                    }
                }

                if (e is FieldAccessException)
                {
                    return ApiStepResult.Fail(BadRequest(e.Message));
                }

                return ApiStepResult.Fail(InternalServerError(e));
            }

            Logger.LogInfo(GetType(), $"User '{userVm.Username}' created  by '{ApplicationContext.GetUserName()}'.");

            userVm.Password = null;
            userVm.ConfirmPassword = null;
            return ApiStepResult.Succeed(Ok(userVm));
        }

        [HttpPost, Route("Users/ResendVerificationEmail")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResendVerificationEmail(string userId)
        {
            try
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user == null)
                    return NotFound();

                var onDisasterRecovery = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";

                //string tokenConfirmation = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
                var tokenConfirmation = await UserManager.GenerateUserTokenAsync("EmailConfirmation", userId);
                if (onDisasterRecovery)
                {
                    var userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(user.Id);
                    if (userRegistration == null)
                    {
                        userRegistration = new UserRegistration
                        {
                            UserProfileId = user.Id,
                            VerificationCode = tokenConfirmation
                        };
                        Uow.UserRegistrationRepo.Add(userRegistration);
                    }
                    else
                    {
                        userRegistration.VerificationCode = tokenConfirmation;
                        Uow.UserRegistrationRepo.Update(userRegistration);
                    }

                    await Uow.SaveChangesAsync();
                }

                if (!onDisasterRecovery)
                {
                    await SendVerificationEmail(userId, tokenConfirmation);
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("Users/ResetPasswordVerificationCode")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResetPasswordVerificationCode(string userId)
        {
            try
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user == null)
                    return NotFound();

                var onDisasterRecovery = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";

                string verificationCode = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                if (onDisasterRecovery)
                {
                    var userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(user.Id);
                    if (userRegistration == null)
                    {
                        userRegistration = new UserRegistration
                        {
                            UserProfileId = user.Id,
                            VerificationCode = verificationCode
                        };
                        Uow.UserRegistrationRepo.Add(userRegistration);
                    }
                    else
                    {
                        userRegistration.VerificationCode = verificationCode;
                        Uow.UserRegistrationRepo.Update(userRegistration);
                    }
                }

                await Uow.SaveChangesAsync();

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPut, Route("users")]
        public Task<IHttpActionResult> Update([FromBody]UserViewModel userVm)
        {
            return LogTransaction(() => ExecuteUpdate(userVm), "PUT: api/admin/users");
        }

        private async Task<ApiStepResult> ExecuteUpdate([FromBody]UserViewModel userVm)
        {
            if (!ModelState.IsValid)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

            if (userVm.Deleted)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ErrorUserCantBeDeleted));

            var isOrgAmin = await IsOrgAdmin();
            if (isOrgAmin)
            {
                var orgId = GetOrganizationId();
                if (orgId != userVm.OrganizationId)
                {
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.InvalidOrganizationId));
                }
            }

            var user = await UserManager.FindByNameAsync(userVm.Username);

            var isRecovered = false;
            if ((user.Disabled && !userVm.Disabled) || (user.IsDeleted && !userVm.Deleted)) isRecovered = true;

            if (user != null)
            {
                try
                {
                    user.Email = userVm.Email;
                    user.FirstName = userVm.FirstName;
                    user.LastName = userVm.LastName;
                    user.Address = userVm.Address;
                    user.IsApproved = true;
                    user.Disabled = userVm.Disabled;
                    user.Fax = userVm.Fax;
                    user.PhoneNumber = userVm.PhoneNumber;
                    user.Position = userVm.Position;
                    user.OrganizationId = userVm.OrganizationId;
                    user.EmailConfirmed = true;
                    user.LastPasswordChangeDate = DateTime.UtcNow;
                    user.IsDeleted = false;
                    if (isRecovered)
                    {
                        //This is to "destroy" the old password
                        user.PasswordHash = "^Zx0!" + user.PasswordHash;
                    }

                    var result = await UserManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        result = await UserManager.SetLockoutEnabledAsync(user.Id, user.LockoutEnabled);
                        if (result.Succeeded)
                        {

                            // Roles
                            var roles = user.Roles.Select(c => c.RoleId).ToList();
                            string[] userRoles = Uow.ApplicationRoleRepo.GetAll().ToList().Where(a => roles.Contains(a.Id)).Select(r => r.Name).ToArray();
                            result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles);
                            if (result.Succeeded)
                            {
                                var roleIds = userVm.RoleId.Split(',').Select(e => e).ToList();
                                if (roleIds.Any())
                                {
                                    var appRoles = await Uow.ApplicationRoleRepo.GetByRoleIdsAsync(roleIds);
                                    foreach (var appRole in appRoles)
                                    {
                                        if (result.Succeeded)
                                        {
                                            result = await UserManager.AddToRoleAsync(user.Id, appRole.Name);
                                        }
                                        else
                                            throw new InvalidOperationException("Can't add Roles to User");
                                    }
                                }
                            }
                            else
                                throw new InvalidOperationException(Resources.ErrorCantAddRolesToUser);

                            UserRegistration userRegistration = null;
                            var onDisasterRecovery = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";
                            string tokenConfirmation = "";
                            if (isRecovered)
                            {
                                if (onDisasterRecovery)
                                {
                                    userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(user.Id);
                                }
                                //tokenConfirmation = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                                tokenConfirmation = await UserManager.GenerateUserTokenAsync("EmailConfirmation", user.Id);
                            }

                            //Doas
                            //Uow.BeginTransaction();
                            try
                            {
                                //Delete the old DOAS for the user
                                var actualDoas = await Uow.UserDoaRepo.GetDoasByUserAsync(user.Id);
                                if (actualDoas.Any())
                                {
                                    foreach (var doa in actualDoas)
                                    {
                                        Uow.UserDoaRepo.Delete(doa);
                                    }
                                }

                                var doaIds = userVm.DoaIds.Split(',').Select(e => e).ToList();
                                if (doaIds.Any())
                                {
                                    var doas = await Uow.DoaRepo.GetByDoaIdsAsync(doaIds);
                                    foreach (var doa in doas)
                                    {
                                        var ud = new UserDoa
                                        {
                                            UserId = user.Id,
                                            DoaId = doa.Id,
                                            OrganizationId = (user.OrganizationId.HasValue) ? user.OrganizationId.Value : -1
                                        };
                                        Uow.UserDoaRepo.Add(ud);
                                    }
                                }

                                if (isRecovered)
                                {
                                    if (onDisasterRecovery)
                                    {
                                        if (userRegistration == null)
                                        {
                                            userRegistration = new UserRegistration
                                            {
                                                UserProfileId = user.Id,
                                                VerificationCode = tokenConfirmation
                                            };
                                            Uow.UserRegistrationRepo.Add(userRegistration);
                                        }
                                        else
                                        {
                                            userRegistration.VerificationCode = tokenConfirmation;
                                            Uow.UserRegistrationRepo.Update(userRegistration);
                                        }
                                    }

                                }

                                await Uow.SaveChangesAsync();
                                //Uow.Commit();

                                if (isRecovered && !onDisasterRecovery)
                                {
                                    await SendVerificationEmail(user.Id, tokenConfirmation);
                                }

                            }
                            catch (Exception)
                            {
                                //Uow.Rollback();
                                throw;
                            }

                        }
                        else
                            throw new InvalidOperationException(Resources.ErrorLockoutEnable);
                    }
                    else
                        throw new InvalidOperationException(Resources.ErrorUpdatingUserInfo);
                }
                catch (Exception e)
                {
                    return ApiStepResult.Fail(InternalServerError(e));
                }
            }

            Logger.LogInfo(GetType(), $"User '{userVm.Username}' updated  by '{ApplicationContext.GetUserName()}'.");

            return ApiStepResult.Succeed(Ok(userVm));
        }

        [HttpPut, Route("users/resetpassword")]
        public async Task<IHttpActionResult> ResetPassword([FromBody]UserResetPasswordViewModel userVm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var user = await UserManager.FindByNameAsync(userVm.Username);
                if (user == null)
                    return NotFound();

                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var result = await UserManager.ResetPasswordAsync(user.Id, code, userVm.Password);

                return result.Succeeded ? Ok(userVm) : BadRequest() as IHttpActionResult;

            }, "api/admin/users/resetpassword");
        }

        ///TODO: Implement Logging here?
        [HttpPut, Route("multiusers")]
        public async Task<IHttpActionResult> DeleteMultiUsers([FromBody] UsersDeleteViewModel users)
        {
            return await LogTransaction(async () =>
            {
                foreach (var username in users.Usernames)
                {
                    if (!await CanDeleteUser(username))
                    {
                        return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.InvalidOrganizationId));
                    }
                }

                foreach (var username in users.Usernames)
                {
                    var result = await DeleteUser(username);
                    if (result == MultiUserDeleteResult.AUTODELETE) { }
                    //Logger.Warning(Resources.MultiUserDeleteError1);
                    else if (result == MultiUserDeleteResult.INVALID_ORG_ID) { }
                    else if (result == MultiUserDeleteResult.DELETE_FAIL) { }
                    //Logger.Warning(string.Format(Resources.MultiUserDeleteError2, username));
                    else if (result == MultiUserDeleteResult.DOAS_FAIL) { }
                    //Logger.Warning(string.Format(Resources.MultiUserDeleteError3, username));
                }
                return ApiStepResult.Succeed(Ok());
            }, $"DELETE: api/admin/multiusers");
        }

        private async Task<MultiUserDeleteResult> DeleteUser(string username)
        {
            if (!await CanDeleteUser(username))
            {
                return MultiUserDeleteResult.INVALID_ORG_ID;
            }

            var user = await UserManager.FindByNameAsync(username);

            // NOTE: an admin cannot delete himself
            if (user == null || SameUserId(user.Id, ApplicationContext.GetUserId()))
                return MultiUserDeleteResult.AUTODELETE;

            user.IsDeleted = true;
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
                return MultiUserDeleteResult.DELETE_FAIL;

            //Uow.BeginTransaction();
            try
            {
                // delete the DOAs for the user
                var actualDoas = await Uow.UserDoaRepo.GetDoasByUserAsync(user.Id);
                if (actualDoas.Any())
                {
                    foreach (var doa in actualDoas)
                        Uow.UserDoaRepo.Delete(doa);
                }

                await Uow.SaveChangesAsync();
                //Uow.Commit();

                Logger.LogInfo(GetType(), $"User '{user.UserName}' removed  by '{ApplicationContext.GetUserName()}'.");

                return MultiUserDeleteResult.SUCCESS;
            }
            catch (Exception)
            {
                //Uow.Rollback();
                return MultiUserDeleteResult.DOAS_FAIL;
            }
        }

        private async Task<bool> CanDeleteUser(string username)
        {
            if (await IsAdmin())
            {
                return true;
            }

            //User is a NofAdmin or OrgAdmin 
            var user = await UserManager.FindByNameAsync(username);
            if (user == null)
            {
                return false;
            }

            var orgId = GetOrganizationId();
            return orgId == user.OrganizationId;
        }

        [HttpDelete, Route("users/{username}")]
        public async Task<IHttpActionResult> Delete(string username)
        {
            return await LogTransaction(async () =>
            {
                var result = await DeleteUser(username);
                if (result == MultiUserDeleteResult.AUTODELETE)
                {
                    //Logger.Warning(Resources.MultiUserDeleteError1);
                    return ApiStepResult.Fail(NotFound());
                }
                else if (result == MultiUserDeleteResult.DELETE_FAIL)
                {
                    //Logger.Warning(string.Format(Resources.MultiUserDeleteError2, username));
                    return ApiStepResult.Fail(BadRequest());
                }
                else if (result == MultiUserDeleteResult.DOAS_FAIL)
                {
                    //Logger.Warning(string.Format(Resources.MultiUserDeleteError3, username));
                    return ApiStepResult.Fail(InternalServerError());
                }
                else if (result == MultiUserDeleteResult.INVALID_ORG_ID)
                {
                    //Logger.Warning(string.Format(Resources.MultiUserDeleteError3, username));
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.InvalidOrganizationId));
                }
                return ApiStepResult.Succeed(Ok());
            }, $"DELETE: api/admin/users/{username}");
        }

        static bool SameUserId(string id1, string id2) => string.Compare(id1, id2, true) == 0;

        public enum MultiUserDeleteResult
        {
            SUCCESS = 0,
            AUTODELETE,
            DELETE_FAIL,
            INVALID_ORG_ID,
            DOAS_FAIL
        }

        private ApplicationUserManager _userManager;
    }
}
