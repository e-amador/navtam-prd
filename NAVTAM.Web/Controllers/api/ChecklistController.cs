﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/checklist")]
    [Authorize(Roles = "Administrator, NOF_ADMINISTRATOR")]
    public class ChecklistController : ApiBaseController
    {
        public ChecklistController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, INotamQueuePublisher queuePublisher, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _queuePublisher = queuePublisher;
        }

        [HttpPost, Route("admin/page", Name = "RegisteredSeriesChecklistPage")]
        public async Task<IHttpActionResult> GetConfigValuesInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.SeriesChecklistRepo.GetAllCountAsync(queryOptions);
                var seriesValues = await Uow.SeriesChecklistRepo.GetAllAsync(queryOptions);

                if (seriesValues.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    seriesValues = await Uow.SeriesChecklistRepo.GetAllAsync(queryOptions);
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredSeriesChecklistPage", seriesValues, totalCount));

            }, "api/checklist/admin/page");
        }

        [HttpDelete]
        [Route("admin/delete")]
        public async Task<IHttpActionResult> Delete(string serie)
        {
            return await LogAction(async () =>
            {
                var serieck = await Uow.SeriesChecklistRepo.GetByIdAsync(serie);
                if (serieck == null)
                    return NotFound();

                if (await Uow.NotamRepo.FindNotamWithSeriesAsync(serieck.Series))
                {
                    return BadRequest(Resources.ErrorSerieInUse);
                }

                Uow.SeriesChecklistRepo.Delete(serieck);
                Uow.SaveChanges();

                Logger.LogInfo(GetType(), $"DELETE: SeriesChecklist('{serie}') deleted  by '{ApplicationContext.GetUserName()}'.");

                return Ok();

            }, "api/checklist/admin/delete");
        }

        [HttpPost]
        [Route("push")]
        public IHttpActionResult PushCheckList()
        {
            return LogAction(() =>
            {
                var mto = MtoHelper.CreateMessage((int)MessageTransferObjectId.DisseminateChecklistsMsg, m => { });

                _queuePublisher.Publish(Uow, mto);

                Uow.SaveChanges();

                Logger.LogInfo(GetType(), $"CHECKLIST PUSHED by '{ApplicationContext.GetUserName()}'.");

                return Ok("");

            }, "api/checklist/push");
        }

        [HttpGet]
        [Route("admin/findvalue")]
        public async Task<IHttpActionResult> FindValue()
        {
            return await LogAction(async () =>
            {
                var name = CommonDefinitions.CheckListPubMessage;
                var config = await Uow.ConfigurationValueRepo.GetByNameAsync(name);
                if (config == null)
                {
                    config = new ConfigurationValue()
                    {
                        Name = name,
                        Category = "NOTAM",
                        Description = "",
                        Value = ""
                    };
                }
                return Ok(config);

            }, "api/checklist/admin/findvalue");
        }

        [HttpGet]
        [Route("admin/findseries")]
        public async Task<IHttpActionResult> FindSeries(string series)
        {
            return await LogAction(async () =>
            {
                var serie = await Uow.SeriesChecklistRepo.GetByIdAsync(series);
                if (serie == null)
                {
                    return BadRequest(Resources.InvalidSeriesChecklistValue);
                }
                return Ok(serie);

            }, "api/checklist/admin/findseries");
        }

        [HttpGet]
        [Route("admin/verifyseries")]
        public async Task<IHttpActionResult> VerifySeries(string series)
        {
            return await LogAction(async () =>
           {
               return Ok(await Uow.SeriesChecklistRepo.Exists(series));
           }, "api/checklist/admin/findseries");
        }

        [HttpGet]
        [Route("admin/validatefirs")]
        public async Task<IHttpActionResult> ValidateFirs(string names)
        {
            return await LogAction(async () =>
            {
                var valid = true;
                var firs = await Uow.SdoCacheRepo.GetFirsAsync();
                if (firs == null || firs.Count == 0)
                    return BadRequest(Resources.ErrorNotFIRData);

                var firsNames = names.ToUpper().Split(' ');
                if (firsNames.Length == 0) valid = false;
                else
                {
                    foreach (var firName in firsNames)
                    {
                        if (!firs.Exists(f => f.Designator == firName.Trim()))
                            valid = false;
                    }
                }

                return Ok(valid);

            }, "api/checklist/admin/validatefirs");
        }

        [HttpPost]
        [Route("admin/saveseries")]
        public async Task<IHttpActionResult> SaveChecklistConfig([FromBody] SeriesChecklist model)
        {
            return await LogAction(async () =>
            {
                // validate model
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                if (await SeriesChecklistExists(model.Series))
                {
                    Uow.SeriesChecklistRepo.Update(model);
                }
                else
                {
                    Uow.SeriesChecklistRepo.Add(model);
                }
                await Uow.SaveChangesAsync();

                Logger.LogInfo(GetType(), $"SET: SeriesChecklist(Series: '{model.Series}', Firs: '{model.Firs}', Coordinates: '{model.Coordinates}', Radius: {model.Radius}) modified by '{ApplicationContext.GetUserName()}'.");

                return Ok(model);

            }, "api/checklist/admin/saveseries");
        }


        [HttpPost]
        [Route("admin/save")]
        public async Task<IHttpActionResult> SaveChecklistConfig([FromBody] ConfigurationValueViewModel model)
        {
            return await LogAction(async () =>
            {
                // validate model
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                if (!AftnHelper.TryValidateAftnChars(model.Value))
                    return BadRequest(Resources.BadRequest + " " + Resources.InvalidAftnChar);

                var forbiddenWordsConfig = await Uow.ConfigurationValueRepo.GetByNameAsync(CommonDefinitions.ForbiddenWordsConfigName);
                if (forbiddenWordsConfig != null)
                {
                    if (AftnHelper.ContainsForbiddenWord(model.Value, forbiddenWordsConfig.Value))
                    {
                        return BadRequest(Resources.BadRequest + " " + Resources.InvalidAftnChar);
                    }
                }

                model.Name = CommonDefinitions.CheckListPubMessage;

                var confRec = new ConfigurationValue()
                {
                    Category = model.Category,
                    Description = model.Description,
                    Name = model.Name,
                    Value = model.Value
                };

                if (await ConfigNameExists(model.Name))
                {
                    Uow.ConfigurationValueRepo.Update(confRec);
                }
                else
                {
                    Uow.ConfigurationValueRepo.Add(confRec);
                }
                await Uow.SaveChangesAsync();

                Logger.LogInfo(GetType(), $"SET: CHECKLIST PUB TEXT to '{model.Value}' by '{ApplicationContext.GetUserName()}'.");

                return Ok(model);

            }, "api/checklist/admin/save");
        }

        private Task<bool> ConfigNameExists(string name)
        {
            var name_trimmed = (name ?? "").Trim();
            return Uow.ConfigurationValueRepo.Exists(name_trimmed);
        }

        private Task<bool> SeriesChecklistExists(string name)
        {
            var name_trimmed = (name ?? "").Trim();
            return Uow.SeriesChecklistRepo.Exists(name_trimmed);
        }

        readonly INotamQueuePublisher _queuePublisher;
    }
}
