﻿using alatas.GeoJSON4EntityFramework;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.RWYCLSD;
using System;
using System.Linq;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/closures")]
    [Authorize]
    public class ClosureController : ApiBaseController
    {
        public ClosureController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            return LogAction(() =>
            {
                var results = RwyClsdReasons.Get(GetEFCulture()).Select(pair => new { Id = pair.Key, Name = pair.Value.Name, EFieldName = pair.Value.EFieldName });
                return Ok(results);
            }, "api/closures/get");
        }

        [HttpGet]
        [Route("getGeo")]
        public IHttpActionResult getGeo(string subjectId)
        {
            return LogAction(() =>
            {
                if (string.IsNullOrWhiteSpace(subjectId))
                    return BadRequest();

                var obj = Uow.SdoCacheRepo.GetById(subjectId);
                if (obj == null)
                    return NotFound();

                var response = new
                {
                    Id = obj.Id,
                    Name = obj.Name,
                    Designator = obj.Designator,
                    SubjectGeoRefPoint = new FeatureCollection(obj.RefPoint).Serialize(false),
                };

                return Ok(response);
            }, "api/closures/getGeo");
        }

        private string GetEFCulture()
        {
            var culture = ApplicationContext.GetCookie("_culture", "en");
            return "fr".Equals(culture, StringComparison.OrdinalIgnoreCase) ? "fr" : "en";
        }
    }
}


