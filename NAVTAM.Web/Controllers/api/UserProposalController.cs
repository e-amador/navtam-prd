﻿using Business.Common;
using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using NAVTAM.SignalrHub;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
//using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/proposals")]
    public class UserProposalController : ProposalController
    {
        public UserProposalController(IDashboardContext context, IEventHubContext eventHubContext, IGeoLocationCache geoLocationCache, ILogger logger)
            : base(context, eventHubContext, geoLocationCache, logger)
        {
        }

        [Route("GetEmptyModel")]
        [HttpGet]
        public Task<IHttpActionResult> GetEmptyModel(int catid)
        {
            return LogAction(() => GetProposalEmptyModel(catid), $"api/proposal/GetEmptyModel/{catid}");
        }

        [Route("GetReadOnlyModel")]
        [HttpGet]
        public Task<IHttpActionResult> GetReadOnlyModel(int id)
        {
            return LogAction(() => GetProposalReadOnlyModel(id), $"api/proposal/GetReadOnlyModel/{id}");
        }

        [Route("GetNotamReadOnlyModel")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNotamReadOnlyModel(Guid id)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                var notam = await Uow.NotamRepo.GetByIdAsync(id);
                if (notam == null)
                    return BadRequest();

                return await GetNotamProposalReadOnlyModel(notam.NotamId, notam.ProposalId);
            }, $"api/proposal/GetNotamReadOnlyModel/{id}");
        }

        [Route("GetModel/{id}")]
        [HttpGet]
        public Task<IHttpActionResult> GetModel(int id)
        {
            return LogTransaction(() => GetProposalModel(id, false), $"api/proposal/GetModel/{id}");
        }

        [Route("GetModelEnforced/{id}")]
        [HttpGet]
        public Task<IHttpActionResult> GetModelEnforced(int id)
        {
            return LogTransaction(() => GetProposalModel(id, true), $"api/proposal/GetModelEnforced/{id}");
        }

        [Route("restorestatus/{id}/{updateStatus}")]
        [HttpPost]
        public async Task<IHttpActionResult> RestoreStatus(int id, bool updateStatus)
        {
            return await LogTransaction(async () =>
            {
                if (!updateStatus) return ApiStepResult.Succeed(Ok());

                var proposal = await Uow.ProposalRepo.GetByIdAsync(id);
                if (proposal == null) return ApiStepResult.Fail(BadRequest());

                if (proposal.Status == NotamProposalStatusCode.Rejected) return ApiStepResult.Succeed(Ok()); //Do nothing, it stay on Rejected status

                if (proposal.UserId != ApplicationContext.GetUserId()) return ApiStepResult.Succeed(Ok()); //Do nothing, other user took the Proposal

                return proposal.GroupId.HasValue
                    ? await RestoreGroupLastValidStatus(proposal)
                    : await RestoreLastValidStatus(proposal);
            }, $"api/proposal/restorestatus/{id}/{updateStatus}");
        }

        [Route("status")]
        [HttpPost]
        public async Task<IHttpActionResult> GetStatus(int refreshRate, DateTime? since, [FromBody] int[] ids)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (!since.HasValue)
                    since = DateTime.Now;

                var org = await GetUserOrganization();
                if (org == null)
                    return BadRequest();

                var proposalStatus = await Uow.ProposalRepo.GetStatusAsync(ids, since.Value, org.Id, org.Type);

                return Ok(new { lastSyncTime = since.Value.AddSeconds(refreshRate), proposalStatus = proposalStatus });
            }, "api/proposal/status");
        }

        [Route("{cat}", Name = "GetByCategories")]
        [HttpPost]
        public async Task<IHttpActionResult> GetProposalsInPage(int cat, [FromBody]QueryOptions queryOptions)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var org = await GetUserOrganization();
                if (org == null)
                    return BadRequest();

                var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
<<<<<<< HEAD
                var totalCount = await Uow.ProposalRepo.GetUserProposalsInRegionCountAsync(userDoa, org.Id, org.Type, cat, queryOptions);
                var proposals = await Uow.ProposalRepo.GetUserProposalsInRegionAsync(userDoa, org.Id, org.Type, cat, queryOptions);
=======

                try
                {
                    var totalCount = await Uow.ProposalRepo.GetUserProposalsInRegionCountAsync(userDoa, org.Id, org.Type, cat, queryOptions);
                    var proposals = await Uow.ProposalRepo.GetUserProposalsInRegionAsync(userDoa, org.Id, org.Type, cat, queryOptions);
                    return Ok(ResponsewithPagingEnvelope(queryOptions, "GetByCategories", proposals, totalCount));
                }
                catch (Exception e)
                {
                    throw e;
                }
>>>>>>> f4e06108050342a2ce25992ce3872adeae07d194

            }, $"POST: api/proposal/{cat}");
        }

        [Route("expiringcount")]
        [HttpGet]
        public async Task<IHttpActionResult> GetExpiringTotal()
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var org = await GetUserOrganization();
                if (org == null)
                    return BadRequest();

                var minutes = await GetExpiringValueMinutes();

                var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var totalCount = await Uow.ProposalRepo.GetUserProposalsExpiringSoonCountAsync(userDoa, org.Id, org.Type, minutes);

                return Ok(totalCount);

            }, "api/proposals/expiringcount");
        }

        [Route("expiring", Name = "GetExpiringProposals")]
        [HttpPost]
        public async Task<IHttpActionResult> GetExpiringProposalsInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var org = await GetUserOrganization();
                if (org == null)
                    return BadRequest();

                var minutes = await GetExpiringValueMinutes();
                var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());

                var totalCount = await Uow.ProposalRepo.GetUserProposalsExpiringSoonCountAsync(userDoa, org.Id, org.Type, minutes);
                var proposals = await Uow.ProposalRepo.GetUserProposalsExpiringSoonAsync(userDoa, org.Id, org.Type, queryOptions, minutes);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetByCategories", proposals, totalCount));
            }, $"POST: api/proposal/expiring");
        }

        [Route("children/{cat}", Name = "GetChildrenByCategories")]
        [HttpPost]
        public async Task<IHttpActionResult> GetChildrenProposalsInPage(int cat, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var org = await GetUserOrganization();
                if (org == null) return BadRequest();

                var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var orgId = org.Type == OrganizationType.External ? (Nullable<int>)(org.Id) : null;

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var childCategories = await Uow.NsdCategoryRepo.GetChildCategoriesAsync(cat);

                int[] childCategoryIds = null;
                if (childCategories.Any())
                    childCategoryIds = childCategories.Select(x => x.Id).ToArray();

                int totalCount = await Uow.ProposalRepo.GetCountByChildrenCategoryIdsAsync(userDoa, childCategoryIds, orgId, queryOptions);
                var proposals = await Uow.ProposalRepo.GetByChildrenCategoryIdsAsync(userDoa, childCategoryIds, orgId, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetChildrenByCategories", proposals, totalCount));
            }, $"api/proposal/children/{cat}");
        }

        [Route("submitted/{cat}", Name = "GetSubmittedByCategories")]
        [HttpPost]
        public async Task<IHttpActionResult> GetSubmitted(int cat, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest()) return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn) queryOptions.PageSize = MaxRecordsToReturn;

                int totalCount = 0;
                totalCount = await Uow.ProposalRepo.GetSubmittedCountByCategoryAsync(cat, queryOptions);
                var proposals = await Uow.ProposalRepo.GetSubmittedByCategoryAsync(cat, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetSubmittedByCategories", proposals, totalCount));
            }, $"api/proposal/submitted/{cat}");
        }

        [Route("grouped/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProposalByGroupId(Guid id)
        {
            return await LogAction(async () =>
            {
                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                if (orgId == 0) return BadRequest();

                var groupedProposals = await Uow.ProposalRepo.GetProposalsGroupedAsync(orgId, id);
                return Ok(groupedProposals);
            }, $"api/proposal/grouped/{id}");
        }

        [Route("dashboard/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetFromDashboardSearch(int id)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest()) return Unauthorized();

                var response = await Uow.ProposalRepo.GetProposalDto(id);

                return Ok(response);
            }, $"api/proposal/dashboard/{id}");
        }

        [Route("SaveDraft")]
        [HttpPost]
        public Task<IHttpActionResult> SaveDraft(ProposalViewModel model)
        {
            return LogTransaction(async () =>
            {
                var validateStatus = await ValidateModelVsProposal(model);
                if (validateStatus.Succeeded)
                {
                    if (model.Status == NotamProposalStatusCode.Undefined)
                        model.Status = NotamProposalStatusCode.Draft;

                    if (model.Status == NotamProposalStatusCode.Withdrawn)
                        model.Status = NotamProposalStatusCode.Draft;

                    model.NotamId = BuildNotamId(model);
                    return await SaveNotamProposal(model, model.Status);
                }
                return validateStatus;

            }, "api/proposal/grouped/savedraft");
        }

        [Route("contactinfo")]
        [HttpPut]
        public Task<IHttpActionResult> UpdateOriginatorContactInfo(OriginatorViewModel model)
        {
            return LogTransaction(async () =>
            {
                if (!ModelState.IsValid)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                var proposal = await Uow.ProposalRepo.GetByIdAsync(model.ProposalId);
                if (proposal == null)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                var nsdManager = _managerResolver.GetNsdManager($"{proposal.CategoryId}-{proposal.Version}");

                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                if (orgId == 0)
                    return ApiStepResult.Fail(BadRequest(Resources.InvalidOrganizationId));

                var originator = await Uow.OriginatorInfoRepo.FindAsync(model.Originator, orgId);
                if(originator != null )
                {
                    if(originator.Email != model.Email || originator.Phone != model.Phone)
                    {
                        originator.Email = model.Email;
                        originator.Phone = model.Phone;

                        Uow.OriginatorInfoRepo.Update(originator);
                    }
                }
                else{
                    var originatorInfo = new OriginatorInfo
                    {
                        Fullname = model.Originator,
                        OrganizationId = orgId,
                        Email = model.Email,
                        Phone = model.Phone
                    };

                    Uow.OriginatorInfoRepo.Add(originatorInfo);
                }

                proposal.Originator = model.Originator;
                proposal.OriginatorEmail = model.Email;
                proposal.OriginatorPhone = model.Phone;

                var tokenViewModel = nsdManager.DeserializeToken(proposal.Tokens);

                tokenViewModel.Originator = model.Originator;
                tokenViewModel.OriginatorEmail = model.Email;
                tokenViewModel.OriginatorPhone = model.Phone;

                proposal.Tokens = nsdManager.SerializeObject(tokenViewModel);

                Uow.ProposalRepo.Update(proposal);
                await Uow.SaveChangesAsync();

                var proposalHistory = await Uow.ProposalHistoryRepo.GetLatestByProposalIdAsync(proposal.Id);
                if(proposalHistory != null )
                {
                    proposalHistory.Originator = proposal.Originator;
                    proposalHistory.OriginatorEmail = proposal.OriginatorEmail;
                    proposalHistory.OriginatorPhone = proposal.OriginatorPhone;
                    proposalHistory.Tokens = proposal.Tokens;
                }

                Uow.ProposalHistoryRepo.Update(proposalHistory);
                await Uow.SaveChangesAsync();


                return ApiStepResult.Succeed(Ok());

            }, "api/proposal/contactinfo");
        }

        [Route("SaveDraftGrouped")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveDraftGrouped(List<ProposalViewModel> model)
        {
            // >> refactor!!
            return await LogTransaction(async () =>
            {
                if (model.Count == 0)
                    return ApiStepResult.Fail(BadRequest("Invalid Proposal Group"));

                foreach (var proposal in model)
                {
                    if (proposal.Status == NotamProposalStatusCode.Undefined) proposal.Status = NotamProposalStatusCode.Draft;
                    proposal.NotamId = BuildNotamId(proposal);
                }

                return await SaveNotamGroupedProposal(model, model[0].Status);
            }, "api/proposal/SaveDraftGrouped");
        }

        [Route("SubmitProposal")]
        [HttpPost]
        public Task<IHttpActionResult> SubmitProposal(ProposalViewModel model)
        {
            return LogTransaction(() => ExecuteSubmitProposal(model), "api/proposal/SubmitProposal");
        }

        private async Task<ApiStepResult> ExecuteSubmitProposal(ProposalViewModel model)
        {
            if (!ModelState.IsValid)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

            if (await IsValidUserAction(model.CategoryId) == false)
                return ApiStepResult.Fail(Unauthorized());

            model.NotamId = BuildNotamId(model);

            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            if (orgId == 0)
                return ApiStepResult.Fail(BadRequest(Resources.InvalidOrganizationId));

            var nsdManager = _managerResolver.GetNsdManager($"{model.CategoryId}-{model.Version}");
            if (nsdManager == null) return ApiStepResult.Fail(BadRequest(Resources.ErrorCreatingManager));


            var validateStatus = await ValidateModelVsProposal(model);
            if (validateStatus.Failed)
                return validateStatus;

            Proposal savedProposal = null;
            if (model.ProposalId.HasValue && model.ProposalId.Value > 0)
            {
                savedProposal = await Uow.ProposalRepo.GetByIdAsync(model.ProposalId.Value);
            }

            var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
            var context = await GetProposalContextAsync(model, userDoa);

            await nsdManager.UpdateContext(model, context);

            var validateResult = await nsdManager.Validate(model, context);
            if (validateResult.HasErrors)
                return ApiStepResult.Fail(BadRequest(validateResult.ErrorMessages.Count > 0 ? validateResult.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation));

            var icaoResult = await nsdManager.GenerateIcao(model, context);
            if (!icaoResult)
                return ApiStepResult.Fail(BadRequest(Resources.ErrorGeneratingICAO));

            var proposal = MapModelToProposal(nsdManager, savedProposal, model, orgId, NotamProposalStatusCode.Submitted);
            if (proposal == null) return ApiStepResult.Fail(BadRequest());

<<<<<<< HEAD
=======
            proposal.EffectArea = CalculateEfectiveArea(proposal);

            //Uow.BeginTransaction();
>>>>>>> f4e06108050342a2ce25992ce3872adeae07d194
            try
            {
                if (proposal.Id > 0) Uow.ProposalRepo.Update(proposal);
                else Uow.ProposalRepo.Add(proposal);

                await Uow.SaveChangesAsync();

                //Insert a new history into ProposalHistory table
                Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                await Uow.SaveChangesAsync();

                model.ProposalId = proposal.Id;
                //BroadcastStatusChangeEvent(proposal);

                SetIcaoText(model);

                Logger.LogInfo(GetType(), $"Proposal: '{proposal.Id}' submitted by '{ApplicationContext.GetUserName()}'.");

                //Thread.Sleep(60000);

                return ApiStepResult.Succeed(Ok(model));
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("SubmitGroupedProposal")]
        [HttpPost]
        public Task<IHttpActionResult> SubmitGroupedProposal(ProposalViewModel model)
        {
            return LogTransaction(() => ExecuteSubmitGroupedProposal(model), "api/proposal/SubmitGroupedProposal");
        }

        private async Task<ApiStepResult> ExecuteSubmitGroupedProposal(ProposalViewModel model)
        {
            if (!model.GroupId.HasValue) return ApiStepResult.Fail(BadRequest("This proposal is not part of a group"));

            if (!ModelState.IsValid) return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

            if (await IsValidUserAction(model.CategoryId) == false) return ApiStepResult.Fail(Unauthorized());

            ProposalViewModel masterModel = new ProposalViewModel();
            model.CopyPropertiesTo(masterModel);

            try
            {
                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                if (orgId == 0) return ApiStepResult.Fail(BadRequest(Resources.InvalidOrganizationId));
                var doaId = int.Parse(ApplicationContext.GetCookie("doa_id", "0"));
                if (doaId == 0) return ApiStepResult.Fail(BadRequest(Resources.InvalidDOAId));

                //ApplyCurrentCulture();
                var nsdManager = _managerResolver.GetNsdManager($"{model.CategoryId}-{model.Version}");
                if (nsdManager == null) return ApiStepResult.Fail(BadRequest(Resources.ErrorCreatingManager));

                var proposalGroup = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(model.GroupId.Value);

                string errorMessage = null;
                foreach (var savedProposal in proposalGroup)
                {
                    model.NotamId = BuildNotamProposalId(savedProposal);
                    model.IsGroupMaster = savedProposal.IsGroupMaster;
                    if (model.IsGroupMaster) //check only for the master proposal
                    {
                        if (savedProposal.Status == model.Status)
                        {
                            if (savedProposal.UserId != ApplicationContext.GetUserId())
                            {
                                errorMessage = Resources.OtherUserWorkingProposal;
                                break;
                            }
                        }
                        else if (savedProposal.Status == NotamProposalStatusCode.Submitted)
                        {
                            errorMessage = Resources.OtherUserWorkingProposal;
                            break;
                        }
                    }
                    else
                    {
                        model = nsdManager.CreateViewModelFromProposal(savedProposal);

                        model.Token.EndValidity = masterModel.Token.EndValidity;
                        model.Token.StartActivity = masterModel.Token.StartActivity;
                        model.Token.Immediate = masterModel.Token.Immediate;
                        model.Token.Permanent = masterModel.Token.Permanent;
                        model.Token.Estimated = masterModel.Token.Estimated;
                        model.ProposalType = masterModel.ProposalType;

                        model.ReferredSeries = savedProposal.Series;
                        model.ReferredNumber = savedProposal.Number;
                        model.ReferredYear = savedProposal.Year;
                        model.ParentNotamId = savedProposal.NotamId;

                        if (masterModel.ProposalType == NotamType.C)
                            model.Token = nsdManager.CopyTokenForGroupCancellation(masterModel.Token, model.Token); // model.Token.UpdateTokenForCancellation(masterModel.Token);
                    }

                    var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());

                    var context = await GetProposalContextAsync(model, userDoa);
                    await nsdManager.UpdateContext(model, context);

                    var validateResult = await nsdManager.Validate(model, context);
                    if (validateResult.HasErrors)
                    {
                        errorMessage = validateResult.ErrorMessages.Count > 0 ? validateResult.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation;
                        break;
                    }

                    var icaoResult = await nsdManager.GenerateIcao(model, context);
                    if (!icaoResult)
                    {
                        errorMessage = Resources.ErrorGeneratingICAO;
                        break;
                    }

                    var proposal = MapModelToProposal(nsdManager, savedProposal, model, orgId, NotamProposalStatusCode.Submitted);
                    if (proposal == null)
                    {
                        errorMessage = "Error creating Internal Proposal";
                        break;
                    }

                    if (proposal.Id > 0) Uow.ProposalRepo.Update(proposal);
                    else Uow.ProposalRepo.Add(proposal);

                    await Uow.SaveChangesAsync();

                    //Insert a new history into ProposalHistory table
                    Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                    await Uow.SaveChangesAsync();

                    if (model.IsGroupMaster)
                    {
                        model.ProposalId = proposal.Id;
                        SetIcaoText(model);
                    }
                }

                if (string.IsNullOrEmpty(errorMessage))
                {
                    return ApiStepResult.Succeed(Ok(model));
                }

                return ApiStepResult.Fail(BadRequest(errorMessage));
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("SaveAttachments")]
        [HttpPost]
        public Task<IHttpActionResult> SaveAttachments()
        {
            return LogTransaction(() => ExecuteSaveAttachments(), "api/proposal/SaveAttachments");
        }

        [Route("Attachments/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> DownloadAttachment(Guid id)
        {
            try
            {
                var attachment = await Uow.ProposalAttachmentRepo.GetByIdAsync(id);

                var memoryStream = new MemoryStream(attachment.Attachment);
                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(memoryStream)
                };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentLength = attachment.Attachment.Length;
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = attachment.FileName,
                    Size = attachment.Attachment.Length
                };

                return result;
            }
            catch (Exception e)
            {
                var responseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                responseMessage.Content = new StringContent(e.Message);
                throw new HttpResponseException(responseMessage); 
            }
        }

        [Route("Annotations")]
        [HttpPost]
        public Task<IHttpActionResult> ExecuteSaveAnnotations(UpdateAnnotationViewModel model)
        {
            return LogTransaction(() => SaveAnnotations(model), "api/proposal/SaveAnnotations");
        }

        private async Task<ApiStepResult> ExecuteSaveAttachments()
        {
            var request = HttpContext.Current.Request;

            if (request.Files.Count == 0)
                return ApiStepResult.Fail(BadRequest(Resources.AttachNoFilesSelected));

            int proposalId;
            if (!int.TryParse(request.Form["proposalId"], out proposalId))
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " - " + Resources.AttachMissingProposalId)); ;

            var proposal = await Uow.ProposalRepo.GetByIdAsync(proposalId);
            if (proposal == null)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " - " + Resources.AttachProposalNotExists));

            var userId = request.Form["userId"];
            if (string.IsNullOrEmpty(userId) || userId != ApplicationContext.GetUserId())
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " - " + Resources.AttachBadUserRequest));

            var files = request.Files;
            var proposalAttachments = await Uow.ProposalAttachmentRepo.GetByProposalIdAsync(proposalId);
            if (proposalAttachments.Count + files.Count > 25)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " - " + Resources.AttachMaxFilesCount));

            for (int i = 0; i < files.Count; i++)
            {
                var file = files[i];
                if (file.ContentLength > 52428800)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + Resources.AttachFile + " " + file.FileName + " " + Resources.AttachFileSize));
            }

            //Validation of the attachments
            //If a GUID is comming, it is invalid
            var attachments = new List<ProposalAttachmentViewModel>();
            for (int i = 0; i < files.Count; i++)
            {
                var file = files[i];
                var target = new MemoryStream();
                file.InputStream.CopyTo(target);

                string fname;
                // Checking for Internet Explorer
                if (request.Browser.Browser.ToUpper() == "IE" || request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else fname = file.FileName;

                var attachment = new ProposalAttachment
                {
                    Id = Guid.NewGuid(),
                    FileName = fname,
                    Attachment = target.ToArray(),
                    ProposalId = proposalId,
                    CreationDate = DateTime.Now.ToUniversalTime(),
                    UserId = ApplicationContext.GetUserId()
                };

                attachments.Add(FromAttachment(attachment, ApplicationContext.GetUserName(), false));
                Uow.ProposalAttachmentRepo.Add(attachment);
            }
            await Uow.SaveChangesAsync();

            return ApiStepResult.Succeed(Ok(attachments));
        }

        private async Task<ApiStepResult> SaveAnnotations(UpdateAnnotationViewModel model)
        {
            var proposal = await Uow.ProposalRepo.GetByIdAsync(model.ProposalId);
            if(proposal == null )
                return ApiStepResult.Fail(BadRequest());

            var nsdManager = _managerResolver.GetNsdManager($"{proposal.CategoryId}-{proposal.Version}");

            var tokenViewModel = nsdManager.DeserializeToken(proposal.Tokens);

            //We need to re-order the annotations
            model.Annotations = SortAnnotationsByAscendingOrder(model.Annotations);

            //After the sort I'm going back to prepare the NoteToNof field in chronological order
            var ntonof = "";
            for (var i = 0; i < model.Annotations.Count; i++)
            {
                var savedAnnotation = tokenViewModel.Annotations.Find(x => x.Id == model.Annotations[i].Id);
                if(savedAnnotation != null)
                {
                    if(model.Annotations[i].Note != savedAnnotation.Note)
                    {
                        model.Annotations[i].Received = DateTime.UtcNow;
                    }
                    else
                    {
                        model.Annotations[i].Received = savedAnnotation.Received;
                        model.Annotations[i].UserId = savedAnnotation.UserId;
                    }
                }
                else
                {
                    model.Annotations[i].Received = DateTime.UtcNow;
                }

                ntonof += model.Annotations[i].Note;
                if (i < model.Annotations.Count - 1) ntonof += " || ";
            }

            tokenViewModel.NoteToNof = ntonof;
            tokenViewModel.Annotations = model.Annotations;

            proposal.Tokens = nsdManager.SerializeObject(tokenViewModel);

            Uow.ProposalRepo.Update(proposal);
            await Uow.SaveChangesAsync();


            var proposalHistory = await Uow.ProposalHistoryRepo.GetLatestByProposalIdAsync(proposal.Id);
            if (proposalHistory != null)
            {
                proposalHistory.Tokens = proposal.Tokens;
                proposalHistory.RowVersion = proposal.RowVersion;
            }

            Uow.ProposalHistoryRepo.Update(proposalHistory);
            await Uow.SaveChangesAsync();

            return ApiStepResult.Succeed(Ok());
        }

        [Route("DeleteAttachments")]
        [HttpPost]
        public async Task<IHttpActionResult> DeleteAttachments(ProposalAttachmentViewModel model)
        {
            return await LogTransaction(async () =>
            {
                if (!ModelState.IsValid)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                var attachment = await Uow.ProposalAttachmentRepo.GetByIdAsync(model.Id);
                if (attachment != null)
                {
                    Uow.ProposalAttachmentRepo.Delete(attachment);
                    await Uow.SaveChangesAsync();

                    return ApiStepResult.Succeed(StatusCode(HttpStatusCode.NoContent));
                }
                return ApiStepResult.Fail(StatusCode(HttpStatusCode.NotFound));
            }, "api/proposal/DeleteAttachments");
        }

        [Route("acknowledge/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> AcknowledgeProposal(int id)
        {
            return await LogTransaction(async () =>
            {
                var proposal = await Uow.ProposalRepo.GetByIdAsync(id);
                if (proposal == null)
                    return ApiStepResult.Fail(BadRequest());

                var userIsNOF = await IsNOFUser(); 
                if ( !userIsNOF)
                {
                    if(!proposal.PendingReview )
                    {
                        return ApiStepResult.Fail(BadRequest("ACKNOWLEDGED"));
                    }
                    if( proposal.Status == NotamProposalStatusCode.Disseminated || proposal.Status == NotamProposalStatusCode.DisseminatedModified)
                    {
                        var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));

                        var acknowledgements = await Uow.ProposalAcknowledgementRepo.GetByProposalIdAsync(proposal.Id);
                        var acknowledgement = acknowledgements.SingleOrDefault(e => e.OrganizationId == orgId && !e.Acknowledge);
                        if( acknowledgement != null)
                        {
                            acknowledgement.Acknowledge = true;
                            Uow.ProposalAcknowledgementRepo.Update(acknowledgement);
                            await Uow.SaveChangesAsync();
                        }
                        else
                        {
                            var savedAcknowledgement = acknowledgements.Find(e => e.OrganizationId == orgId);
                            if (savedAcknowledgement != null)
                            {
                                return ApiStepResult.Fail(BadRequest("ACKNOWLEDGED"));
                            }
                            else
                            {
                                Uow.ProposalAcknowledgementRepo.Add(
                                new ProposalAcknowledgement
                                {
                                    OrganizationId = orgId,
                                    ProposalId = proposal.Id,
                                    Acknowledge = true
                                });
                            }
                        }

                        var allChecked = acknowledgements.Count(e => e.Acknowledge) == acknowledgements.Count;
                        if (allChecked)
                        {
                            proposal.PendingReview = false;
                            Uow.ProposalRepo.Update(proposal);
                            await Uow.SaveChangesAsync();

                            var proposalHistory = await Uow.ProposalHistoryRepo.GetLatestByProposalIdAsync(proposal.Id);
                            if (proposalHistory != null)
                                proposalHistory.RowVersion = proposal.RowVersion;

                            await Uow.SaveChangesAsync();
                        }
                        return ApiStepResult.Succeed(Ok());
                    }
                }

                return ApiStepResult.Fail(BadRequest());

            }, $"api/proposal/Discard/{id}");
        }

        [Route("Discard/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Discard(int id)
        {
            return await LogTransaction(async () =>
            {
                var proposal = await Uow.ProposalRepo.GetByIdAsync(id);
                if (proposal == null)
                    return ApiStepResult.Fail(BadRequest());

                if (await IsValidUserAction(proposal.CategoryId) == false)
                    return ApiStepResult.Fail(Unauthorized());

                if (proposal.UserId != ApplicationContext.GetUserId())
                {
                    if (proposal.Status == NotamProposalStatusCode.Rejected)
                    {
                        var nof = (await ApplicationContext.IsUserInRole(proposal.UserId, CommonDefinitions.NofUserRole) || await ApplicationContext.IsUserInRole(proposal.UserId, CommonDefinitions.NofAdminRole));
                        if (nof)
                            return await DiscardUserProposal(proposal);
                        else
                            return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                    }
                    else
                    {
                        return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                    }
                }
                return await DiscardUserProposal(proposal);
            }, $"api/proposal/Discard/{id}");
        }

        [Route("CancelProposal/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> CancelProposal(int id)
        {
            return await LogAction(async () =>
            {
                var result = await CloneInternalProposal(id);
                if (result == null)
                    return BadRequest();

                if (result.Status == NotamProposalStatusCode.Expired)
                    return BadRequest();

                //if(result.Status != NotamProposalStatusCode.Rejected)
                {
                    if ( result.ProposalType == NotamType.C)
                        return BadRequest();
                }

                if (!result.Estimated && result.EndValidity.HasValue && result.EndValidity.Value < DateTime.UtcNow)
                    return BadRequest();

                result.ReferredSeries = result.Series;
                result.ReferredNumber = result.Number;
                result.ReferredYear = result.Year;

                result.ParentNotamId = result.NotamId;

                result.Status = NotamProposalStatusCode.Cancelled;
                result.ProposalType = NotamType.C;
                result.Immediate = true;
                result.StartActivity = null;
                result.Permanent = false;
                result.EndValidity = null;
                result.NotamId = BuildNotamId(result);

                //This "fix" is very important, because the icao generator will
                //trigger as soon as the nsd is loaded, even before the all the data
                //is loaded on the form and it trigger an error. With this "fix", it will wait
                //until all the data is loaded on the form to generate the icao for cancellation
                result.ItemE = "";
                result.ItemEFrench = "";

                return Ok(result);
            }, $"api/proposal/CancelProposal/{id}");
        }

        [Route("ReplaceProposal/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> ReplaceProposal(int id)
        {
            return await LogAction(async () =>
            {
                var result = await CloneInternalProposal(id);
                if (result == null)
                    return BadRequest();

                result.ReferredSeries = result.Series;
                result.ReferredNumber = result.Number;
                result.ReferredYear = result.Year;

                result.ParentNotamId = result.NotamId;

                result.Status = NotamProposalStatusCode.Replaced;
                result.ProposalType = NotamType.R;

                if (result.GroupedProposals != null) // change status of grouped proposals to replaced
                    ChangeStatusOfGroupedProposals(result, NotamProposalStatusCode.Replaced, NotamType.R);

                result.NotamId = BuildNotamId(result);

                if (result.GroupId.HasValue)
                {
                    foreach (var proposal in result.GroupedProposals)
                    {
                        proposal.ReferredSeries = proposal.Series;
                        proposal.ReferredNumber = proposal.Number;
                        proposal.ReferredYear = proposal.Year;
                        proposal.ParentNotamId = result.NotamId; /// ?????
                        proposal.IcaoText = ICAOFormatUtils.Generate(proposal, true);
                    }
                }

                result.IcaoText = ICAOFormatUtils.Generate(result, true); 

                return Ok(result);
            }, $"api/proposal/ReplaceProposal/{id}");
        }

        private static void ChangeStatusOfGroupedProposals(ProposalViewModel result, NotamProposalStatusCode statusCode, NotamType notamType)
        {
            foreach (var proposal in result.GroupedProposals)
            {
                proposal.Status = statusCode;
                proposal.ProposalType = notamType;
            }
        }

        [Route("CloneProposal")]
        [HttpPost]
        public async Task<IHttpActionResult> CloneProposal([FromBody] CloneRequest cloneRequest)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                var result = await CloneInternalProposal(cloneRequest.Id, cloneRequest.History);
                if (result == null)
                    return BadRequest();

                result = PrepareClonedModel(result);

                return Ok(result);

            }, $"api/proposal/CloneProposal");
        }

        [Route("CloneNotam/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> CloneNotam(Guid id)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                var notam = await Uow.NotamRepo.GetByIdAsync(id);
                if (notam == null)
                    return BadRequest();

                var result = await CloneInternalNotam(notam.NotamId, notam.ProposalId);
                if (result == null)
                    return BadRequest();

                result = PrepareClonedModel(result);

                return Ok(result);
            }, $"api/proposals/CloneNotam/{id}");
        }

        [Route("filter", Name = "FilterProposals")]
        [HttpPost]
        public async Task<IHttpActionResult> FilterProposals([FromBody]FilterCriteria filterCriteria)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                var org = await Uow.OrganizationRepo.GetByIdAsync(orgId);
                if (org == null)
                    return Unauthorized();

                var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var filterQuery = Uow.ProposalRepo.PrepareQueryFilter(filterCriteria);
                var totalCount = await Uow.ProposalRepo.FilterCountAsync(filterCriteria, org.Id, org.Type, userDoa, filterQuery);
                var proposals = await Uow.ProposalRepo.FilterAsync(filterCriteria, org.Id, org.Type, userDoa, filterQuery);

                return Ok(ResponsewithPagingEnvelope(filterCriteria.QueryOptions, "FilterProposals", proposals, totalCount));

            }, "api/proposals/filter");
        }

        private ProposalViewModel PrepareClonedModel(ProposalViewModel model)
        {
            model.Status = NotamProposalStatusCode.Undefined;
            model.ProposalId = 0;
            model.NotamId = "";
            //model.ParentNotamId = "";

            model.ReferredSeries = "";
            model.ReferredNumber = 0;
            model.ReferredYear = 0;
            model.Series = "";
            model.Number = 0;
            model.Year = 0;
            model.Grouped = false;
            model.IsGroupMaster = false;
            model.GroupId = null;
            model.ProposalType = NotamType.N;
            model.Attachments = null;
            model.NoteToNof = null;
            model.Originator = null;
            model.OriginatorEmail = null;
            model.OriginatorPhone = null;
            model.PendingReview = false;


            //Lastest Requirement (when clone clear start, end and itemD)
            model.StartActivity = null;
            model.EndValidity = null;
            model.ItemD = null;

            model.Token.StartActivity = null;
            model.Token.EndValidity = null;
            model.Token.ItemD = null;
            model.Token.Annotations = new List<ProposalAnnotationViewModel>();
            model.Token.NoteToNof = null;
            model.Token.Originator = null;
            model.Token.OriginatorEmail = null;
            model.Token.OriginatorPhone = null;

            //var tokens = nsdManager.DeserializeToken(model.Tokens);
            //tokens.Annotations = null;
            //tokens.NoteToNof = null;
            //model.Tokens = nsdManager.SerializeObject(tokens);


            return model;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Validate(ProposalViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var nsdManager = _managerResolver.GetNsdManager($"{model.CategoryId}-{model.Version}");

                var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var context = await GetProposalContextAsync(model, userDoa);

                await nsdManager.UpdateContext(model, context);

                var validateResult = await nsdManager.Validate(model, context);
                if (validateResult.HasErrors)
                    return BadRequest(validateResult.ErrorMessages.Count > 0 ? validateResult.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation);

                //SetIcaoText(model);

                return Ok(model);
            }, "POST: api/proposal/");
        }

        private ProposalAttachmentViewModel FromAttachment(ProposalAttachment attachment, string userName, bool readOnly = true)
        {
            return (attachment == null) ?
                null :
                new ProposalAttachmentViewModel
                {
                    Id = attachment.Id,
                    FileName = attachment.FileName,
                    UserName = userName,
                    ReadOnly = readOnly
                };
        }

        private Task<Organization> GetUserOrganization()
        {
            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            return Uow.OrganizationRepo.GetByIdAsync(orgId);
        }
    }
}