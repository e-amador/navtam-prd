﻿using Business.Common;
using Core.Common.Geography;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/notams")]
    [Authorize]
    public class NotamsController : ApiBaseController
    {
        public NotamsController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        /// <summary>
        /// Gets a list of Notams given the filtering parameters.
        /// </summary>
        /// <param name="location">Location could be a coordinate or a polygon depending on the number of points provided.</param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetWithStartAndEndDate(string location, string start, string end)
        {
            return await LogAction(async () =>
            {
                if (!ValidateLocation(location))
                    return BadRequest("Invalid location");

                var startDate = ParseDate(start);
                if (!startDate.HasValue)
                    return BadRequest("Invalid start date");

                var endDate = ParseDate(end);
                if (!endDate.HasValue)
                    return BadRequest("Invalid end date");

                if (startDate > endDate)
                    return BadRequest("Invalid date range");

                return await ExecuteSearch(() => Uow.NotamRepo.GetByLocationBetweenDatesAsync(location, startDate.Value, endDate.Value));

            }, "api/notams/location,start,end");
        }

        /// <summary>
        /// Gets a list of Notams given the filtering parameters.
        /// </summary>
        /// <param name="location">Location could be a coordinate or a polygon depending on the number of points provided.</param>
        /// <param name="start"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetWithStartDate(string location, string start)
        {
            return await LogAction(async () =>
            {
                if (!ValidateLocation(location))
                    return BadRequest("Invalid location");

                var startDate = ParseDate(start);
                if (!startDate.HasValue)
                    return BadRequest("Invalid start date");

                return await ExecuteSearch(() => Uow.NotamRepo.GetByLocationAfterDateAsync(location, startDate.Value));

            }, "api/notams/location,start");
        }

        /// <summary>
        /// Gets a list of Notams given the filtering parameters.
        /// </summary>
        /// <param name="location">Location could be a coordinate or a polygon depending on the number of points provided.</param>
        /// <param name="end"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetWithEndDate(string location, string end)
        {
            return await LogAction(async () =>
            {
                if (!ValidateLocation(location))
                    return BadRequest("Invalid location");

                var endDate = ParseDate(end);
                if (!endDate.HasValue)
                    return BadRequest("Invalid end date");

                return await ExecuteSearch(() => Uow.NotamRepo.GetByLocationBeforeDateAsync(location, endDate.Value));

            }, "api/notams/location,end");
        }

        /// <summary>
        /// Gets a list of Notams given the filtering parameters.
        /// </summary>
        /// <param name="location">Location could be a coordinate or a polygon depending on the number of points provided.</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetFromLocationAlone(string location)
        {
            return await LogAction(async () =>
            {
                if (!ValidateLocation(location))
                    return BadRequest("Invalid location");

                return await ExecuteSearch(() => Uow.NotamRepo.GetByLocationAsync(location));

            }, "api/notams/location");
        }

        private async Task<IHttpActionResult> ExecuteSearch(Func<Task<List<Notam>>> searchFunc)
        {
            try
            {
                var notams = await searchFunc.Invoke();
                return Ok(notams.Select(n => MapToOutputFormat(n)));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetPrevIcaoText(string notamId, int action )
        {
            try
            {
                if(notamId.Length == 10 && notamId.IndexOf(":") == 1 )
                {
                    notamId = notamId.Remove(0, 2);
                }
                notamId = notamId.Replace('_', '/');


                if( action < 0)
                {
                    var notam = await Uow.NotamRepo.GetByNotamIdAsync(notamId);
                    if (notam != null)
                    {
                        var proposal = await Uow.ProposalRepo.GetByIdAsync(notam.ProposalId);
                        if (proposal != null && (proposal.Status == NotamProposalStatusCode.Parked || proposal.Status == NotamProposalStatusCode.ParkedPicked))
                        {
                            return Ok(notam.IcaoText);
                        } else
                        {
                            var prevIcao = await Uow.NotamRepo.GetPrevIcaoText(notamId);
                            return Ok(prevIcao);
                        }
                    }
                }

                var currentIcao = await Uow.NotamRepo.GetCurrentIcaoText(notamId);

                return Ok(currentIcao);
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }

        private bool ValidateLocation(string location)
        {
            var points = (location ?? "").Split(',');

            var count = points.Length;
            if (count != 2 && count != 4)
                return false;

            for (var i = 0; i < count; i++)
                if (!ValidPointValue(points[i], i))
                    return false;

            return true;
        }

        private bool ValidPointValue(string point, int index)
        {
            double bound = index % 2 == 0 ? 180 : 90;
            double value;
            return double.TryParse(point, out value) && (value >= -bound) && (value <= bound);
        }

        private DateTime? ParseDate(string dateString)
        {
            DateTime value;

            if (string.IsNullOrEmpty(dateString))
                return null;

            if (DateTime.TryParseExact(dateString, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out value))
                return value;

            return null;
        }

        // todo: move to Business.Common module & add unit-tests
        private IcaoNotam MapToOutputFormat(Notam notam)
        {
            return new IcaoNotam
            {
                id = ICAOTextUtils.FormatNotamId(notam.Series, notam.Number, notam.Year),
                he = ICAOTextUtils.FormatNotamNumber(notam),
                q = ICAOTextUtils.FormatQLine(notam),
                a = NullifyString(notam.Fir),
                ge = BuildNotamGeoCoordinates(notam.Coordinates),
                r = notam.Radius,
                b = ICAOTextUtils.FormatStartActivity(false, notam.StartActivity),
                c = ICAOTextUtils.FormatEndValidity(notam.Permanent, notam.Estimated, notam.EndValidity),
                d = NullifyString(notam.ItemD),
                st = FormatDate(notam.StartActivity, "yyyy-MM-dd HHmm"),
                en = BuildENLine(notam),
                ee = NullifyString(notam.ItemE),
                ef = NullifyString(notam.ItemEFrench),
                f = NullifyString(notam.ItemF),
                g = NullifyString(notam.ItemG)
            };
        }

        private string NullifyString(string input)
        {
            return string.IsNullOrEmpty(input) ? null : input;
        }

        private string BuildENLine(Notam notam)
        {
            return notam.Permanent ? "PERM" : FormatDate(notam.EndValidity, "yyyy-MM-dd HHmm");
        }

        private string FormatDate(DateTime? date, string format)
        {
            return date?.ToString(format);
        }

        private IcaNotamGeo BuildNotamGeoCoordinates(string coordinates)
        {
            var location = DMSLocation.FromDMS(coordinates);

            if (location == null)
                throw new Exception($"Invalid cordinates '{coordinates}' passed to 'BuildNotamGeoCoordinates'.");

            return new IcaNotamGeo
            {
                type = "Point",
                coordinate = $"[{location.Longitude.AsDecimal},{location.Latitude.AsDecimal}]"
            };
        }
    }
}