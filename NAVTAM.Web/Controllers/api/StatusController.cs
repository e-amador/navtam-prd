﻿using System;
using System.Web.Http;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/system")]    
    public class StatusController : ApiBaseController
    {
        public StatusController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpPost]
        [Route("status")]
        public IHttpActionResult CheckStatus() => LogAction(() => Ok(GetStatus(Uow)), "POST: api/system/status");

        [HttpPost]
        [Route("status/reset")]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult ResetStatus()
        {
            return LogAction(() => {
                SetSystemStatus(Uow, SystemStatusEnum.Ok);
                return Ok(true);
            }, "POST: api/system/status");
        }

        [HttpGet]
        [Route("status/{value?}")]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult Status(string value = "")
        {
            if ("up".Equals(value, StringComparison.OrdinalIgnoreCase))
            {
                SetSystemStatus(Uow, SystemStatusEnum.Ok);
                return Ok($"System will be set to '{SystemStatusEnum.Ok}'");
            }

            if ("nohub".Equals(value, StringComparison.OrdinalIgnoreCase))
            {
                SetSystemStatus(Uow, SystemStatusEnum.NDSDown);
                return Ok($"System will be set to '{SystemStatusEnum.NDSDown}'");
            }

            if ("error".Equals(value, StringComparison.OrdinalIgnoreCase))
            {
                SetSystemStatus(Uow, SystemStatusEnum.CriticalError);
                return Ok($"System will be set to '{SystemStatusEnum.CriticalError}'");
            }

            if (string.IsNullOrEmpty(value))
            {
                var status = GetStatus(Uow);

                if (status.Status == SystemStatusEnum.Ok)
                    return Ok("System is running OK!");

                if (status.Status == SystemStatusEnum.NDSDown)
                    return Ok("System is running, but there is no connection to the NAVCanHub!");

                return Ok(new { Status = "Error", status.LastError });
            }

            return Ok("URL must end with: /up or /error or /nohub");
        }

        static SystemStatus GetStatus(IUow uow) => uow.SystemStatusRepo.GetStatus();

        private void SetSystemStatus(IUow uow, SystemStatusEnum value)
        {
            var status = uow.SystemStatusRepo.GetStatus();
            if (status != null)
            {
                var before = status.Status;

                status.Status = value;
                status.LastUpdated = DateTime.UtcNow;
                status.LastError = string.Empty;
                uow.SystemStatusRepo.Update(status);

                uow.SaveChanges();

                Logger.LogInfo(GetType(), $"CHANGED SystemStatus from '{before}' to '{value}' done by '{ApplicationContext.GetUserName()}'.");
            }
        }
    }
}