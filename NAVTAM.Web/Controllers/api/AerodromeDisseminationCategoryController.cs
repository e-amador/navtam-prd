﻿using System.Web.Http;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.Common.Common;
using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NAVTAM.Helpers;
using NavCanada.Core.Common.Contracts;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/adc")]
    [Authorize(Roles = "Administrator, NOF_ADMINISTRATOR")]
    public class AerodromeDisseminationCategoryController : ApiBaseController
    {
        public AerodromeDisseminationCategoryController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpPost, Route("page", Name = "RegisteredADCPage")]
        public async Task<IHttpActionResult> GetAdcInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > 1000)
                    queryOptions.PageSize = 1000;

                var totalCount = await Uow.AerodromeDisseminationCategoryRepo.GetAllCountAsync(queryOptions);
                var adcs = await Uow.AerodromeDisseminationCategoryRepo.GetAllAsync(queryOptions);

                if (adcs.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    adcs = await Uow.AerodromeDisseminationCategoryRepo.GetAllAsync(queryOptions); 
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredADCPage", adcs, totalCount));

            }, "api/adc/page");
        }

        [HttpPost, Route("sdo/page", Name = "RegisteredSDOPage")]
        public async Task<IHttpActionResult> GetSdoInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > 1000)
                    queryOptions.PageSize = 1000;

                var totalCount = await Uow.SdoCacheRepo.GetTotalAerodromesWithouDisseminationCategoryAsync(queryOptions);
                var sdos = await Uow.SdoCacheRepo.FilterAerodromesWithouDisseminationCategoryAsync(queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredSDOPage", sdos, totalCount));

            }, "api/adc/sdo/page");
        }

        [HttpPost, Route("create")]
        public async Task<IHttpActionResult> Create([FromBody]AerodromeDisseminationCategoryDto adcVm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var sdo = await Uow.SdoCacheRepo.GetByIdAsync(adcVm.AhpMid);
                if (sdo == null)
                    return BadRequest(Resources.BadRequest + " " + Resources.InvalidSubjectId);

                AerodromeDisseminationCategory adc = new AerodromeDisseminationCategory
                {
                    AhpCodeId = adcVm.AhpCodeId,
                    AhpMid = adcVm.AhpMid,
                    AhpName = adcVm.AhpName,
                    Location = sdo.RefPoint,
                    DisseminationCategory = adcVm.DisseminationCategory,
                };

                Uow.AerodromeDisseminationCategoryRepo.Add(adc);
                await Uow.SaveChangesAsync();

                Logger.LogInfo(GetType(), $"NEW: {SumarizeADC(adc)} created  by '{ApplicationContext.GetUserName()}'.");

                return Ok(adcVm) as IHttpActionResult;

            }, "api/adc/create");
        }

        [HttpGet, Route("getsdo")]
        public async Task<IHttpActionResult> GetSdo(string id)
        {
            return await LogAction(async () =>
            {
                var sdo = await Uow.SdoCacheRepo.GetByIdAsync(id);
                if (sdo == null)
                    return BadRequest(Resources.BadRequest + " " + Resources.InvalidSubjectId);

                var sdoDto = new AerodromeWithoutDisseminationCategoryDto
                {
                    Id = sdo.Id,
                    Mid = sdo.Id,
                    Designator = sdo.Designator,
                    Name = sdo.Name,
                    Fir = sdo.Fir,
                    Latitude = sdo.RefPoint.Latitude ?? 0.0,
                    Longitude = sdo.RefPoint.Longitude ?? 0.0
                };

                return Ok(sdoDto) as IHttpActionResult;

            }, "api/adc/getsdo");
        }

        [HttpGet, Route("getadc")]
        public async Task<IHttpActionResult> GetAdc(int id)
        {
            return await LogAction(async () =>
            {
                var adc = await Uow.AerodromeDisseminationCategoryRepo.GetByIdAsync(id);
                if (adc == null) return BadRequest(Resources.BadRequest + " " + Resources.InvalidSubjectId);
                var adcDto = new AerodromeDisseminationCategoryDto
                {
                    Id = adc.Id,
                    AhpCodeId = adc.AhpCodeId,
                    AhpMid = adc.AhpMid,
                    AhpName = adc.AhpName,
                    DisseminationCategory = adc.DisseminationCategory,
                    DisseminationCategoryName = adc.DisseminationCategory.ToString(),
                    Latitude = (adc.Location.Latitude.HasValue ? adc.Location.Latitude.Value : 0),
                    Longitude = (adc.Location.Longitude.HasValue ? adc.Location.Longitude.Value : 0),
                };
                return Ok(adcDto) as IHttpActionResult;

            }, "api/adc/getadc");
        }

        [HttpPost, Route("update")]
        public async Task<IHttpActionResult> Update([FromBody]AerodromeDisseminationCategoryDto adcVm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var adc = await Uow.AerodromeDisseminationCategoryRepo.GetByIdAsync(adcVm.Id);
                if (adc == null)
                    return BadRequest(Resources.BadRequest + " " + Resources.InvalidSubjectId);

                var beforeSummary = SumarizeADC(adc);

                var sdo = await Uow.SdoCacheRepo.GetByIdAsync(adcVm.AhpMid);
                if (sdo != null)
                {
                    adc.AhpName = sdo.Name;
                    adc.AhpCodeId = sdo.Designator;
                    adc.Location = sdo.RefPoint;
                }
                adc.DisseminationCategory = adcVm.DisseminationCategory;

                Uow.AerodromeDisseminationCategoryRepo.Update(adc);
                await Uow.SaveChangesAsync();

                var afterSummary = SumarizeADC(adc);

                Logger.LogInfo(GetType(), $"UPDATE: {beforeSummary} updated to {afterSummary} by '{ApplicationContext.GetUserName()}'.");

                return Ok(adcVm);

            }, "api/adc/update");
        }
        
        [Route("pending/count")]
        [HttpGet]
        public async Task<IHttpActionResult> GetWithoutDCTotal()
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                return Ok(await Uow.SdoCacheRepo.GetTotalAerodromesWithouDisseminationCategoryAsync());

            }, "api/adc/pending/count");
        }

        static string SumarizeADC(AerodromeDisseminationCategory adc)
        {
            return $"ADC(Code: '{adc.AhpCodeId}', Category: '{adc.DisseminationCategory}')";
        }
    }
}