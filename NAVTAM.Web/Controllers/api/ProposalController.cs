﻿using Business.Common;
using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using NAVTAM.SignalrHub;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/proposals")]
    public class ProposalController : ApiBaseController
    {
        public ProposalController(IDashboardContext context, IEventHubContext eventHubContext, IGeoLocationCache geoLocationCache, ILogger logger) : base(context.Uow, context.GetAeroRdsProxy(), geoLocationCache, logger)
        {
            _dashboardContext = context;
            _eventHubContext = eventHubContext;
            _managerResolver = context.GetNsdManagerResolver();
        }

        public async Task<IHttpActionResult> GetProposalReadOnlyModel(int id)
        {
            var proposal = await Uow.ProposalRepo.GetByIdAsync(id);
            if (proposal == null) return NotFound();
            return await GetInternalProposalReadOnlyModel(proposal);
        }

        public async Task<IHttpActionResult> GetNotamProposalReadOnlyModel(string NotamId, int ProposalId)
        {
            var hstry = await Uow.ProposalHistoryRepo.GetByProposalIdAndNotamIdAsync(NotamId, ProposalId);
            if (hstry == null) return NotFound();
            var proposal = ProposalFromHistory(hstry);
            if (proposal == null) return NotFound();
            return await GetInternalProposalReadOnlyModel(proposal);
        }

        public async Task<IHttpActionResult> GetInternalProposalReadOnlyModel(Proposal proposal)
        {
            List<Proposal> proposalsGrouped = new List<Proposal>();
            if (proposal.GroupId.HasValue)
                proposalsGrouped = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(proposal.GroupId.Value);

            //ApplyCurrentCulture();
            var nsdManager = _managerResolver.GetNsdManager($"{proposal.CategoryId}-{proposal.Version}");

            var parentNotamId = await RetrieveParentNotamId(proposal);
            List<ProposalViewModel> modelGroup = null;
            if (proposal.GroupId.HasValue)
            {
                modelGroup = new List<ProposalViewModel>();
                foreach (var proposalGrouped in proposalsGrouped)
                {
                    var groupModel = nsdManager.CreateViewModelFromProposal(proposalGrouped);
                    groupModel.Attachments = await GetAllProposalAttachments(proposal);
                    groupModel.Token.Annotations = SortAnnotationsByAscendingOrder(groupModel.Token.Annotations);
                    groupModel.ParentNotamId = parentNotamId;
                    groupModel.ReadOnlyMode = true;
                    modelGroup.Add(groupModel);
                }
            }

            var model = nsdManager.CreateViewModelFromProposal(proposal);
            model.Attachments = await GetAllProposalAttachments(proposal);
            model.Token.Annotations = SortAnnotationsByAscendingOrder(model.Token.Annotations);
            model.ParentNotamId = await RetrieveParentNotamId(proposal);
            model.GroupedProposals = modelGroup;
            model.ReadOnlyMode = true;
            model.PendingReview = proposal != null && proposal.PendingReview;

            SetIcaoText(model);

            return Ok(model);
        }

        public async Task<IHttpActionResult> GetProposalEmptyModel(int catid)
        {
            if (await IsValidUserAction(catid) == false) return Unauthorized();

            //ApplyCurrentCulture();
            var manager = _managerResolver.GetNsdManager($"{catid}-");
            var model = manager.CreateViewModel();
            return Ok(model);
        }

        protected async Task<ApiStepResult> SaveNotamProposal(ProposalViewModel model, NotamProposalStatusCode status)
        {
            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            if (orgId == 0) return ApiStepResult.Fail(BadRequest(Resources.InvalidOrganizationId));
            var doaId = int.Parse(ApplicationContext.GetCookie("doa_id", "0"));
            if (doaId == 0) return ApiStepResult.Fail(BadRequest(Resources.InvalidDOAId));

            var nsdManager = _managerResolver.GetNsdManager($"{model.CategoryId}-{model.Version}");

            Proposal savedProposal = null;
            if (model.ProposalId.HasValue && model.ProposalId.Value > 0)
            {
                savedProposal = await Uow.ProposalRepo.GetByIdAsync(model.ProposalId.Value);
                if (savedProposal == null) return ApiStepResult.Fail(BadRequest());
                if(model.Status != NotamProposalStatusCode.Parked)
                {
                    if (model.Status != NotamProposalStatusCode.Replaced && model.Status != NotamProposalStatusCode.Cancelled)
                    {
                        if ((savedProposal.UserId != ApplicationContext.GetUserId())) return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                    }
                    else if (savedProposal.Status != NotamProposalStatusCode.Disseminated && savedProposal.Status != NotamProposalStatusCode.DisseminatedModified)
                    {
                        if ((savedProposal.UserId != ApplicationContext.GetUserId())) return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                    }
                }
            }

            var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
            var context = await GetProposalContextAsync(model, userDoa);

            await nsdManager.UpdateContext(model, context);

            var validateResult = await nsdManager.Validate(model, context);
            if (validateResult.HasErrors)
                return ApiStepResult.Fail(BadRequest(validateResult.ErrorMessages.Count > 0 ? validateResult.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation));

            var icaoResult = await nsdManager.GenerateIcao(model, context);
            if (!icaoResult)
                return ApiStepResult.Fail(BadRequest(Resources.ErrorGeneratingICAO));

            var proposal = MapModelToProposal(nsdManager, savedProposal, model, orgId, status);

            //Calculate the Effected Area
            proposal.EffectArea = CalculateEfectiveArea(proposal);

            try
            {
                if (proposal.Id > 0) Uow.ProposalRepo.Update(proposal);
                else Uow.ProposalRepo.Add(proposal);

                await Uow.SaveChangesAsync();

                Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                await Uow.SaveChangesAsync();

                model.ProposalId = proposal.Id;
                model.RowVersion = proposal.RowVersion;
                model.StatusUpdated = false; // Not need to restore previous status if Back button pressed after saving

                SetIcaoText(model);

                return ApiStepResult.Succeed(Ok(model));
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected async Task<ApiStepResult> SaveNotamGroupedProposal(List<ProposalViewModel> proposalsViewModel, NotamProposalStatusCode status)
        {
            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            if (orgId == 0) return ApiStepResult.Fail(BadRequest(Resources.InvalidOrganizationId));
            var doaId = int.Parse(ApplicationContext.GetCookie("doa_id", "0"));
            if (doaId == 0) return ApiStepResult.Fail(BadRequest(Resources.InvalidDOAId));

            //Uow.BeginTransaction();
            try
            {
                var masterGroupId = proposalsViewModel[0].GroupId.HasValue ? proposalsViewModel[0].GroupId.Value : Guid.Empty;
                var nsdManager = _managerResolver.GetNsdManager($"{proposalsViewModel[0].CategoryId}-{proposalsViewModel[0].Version}");

                await RemoveProposalsFromGroupIfUserDelete(Uow, proposalsViewModel, masterGroupId);

                foreach (var proposalVm in proposalsViewModel)
                {
                    Proposal savedProposal = null;
                    if (proposalVm.ProposalId.HasValue && proposalVm.ProposalId.Value > 0)
                    {
                        savedProposal = await Uow.ProposalRepo.GetByIdAsync(proposalVm.ProposalId.Value);
                        if (savedProposal == null) throw new Exception("Proposal Not Found");

                        if (proposalVm.Status != NotamProposalStatusCode.Replaced && proposalVm.Status != NotamProposalStatusCode.Cancelled)
                        {
                            if ((savedProposal.UserId != ApplicationContext.GetUserId())) throw new Exception(Resources.OtherUserWorkingProposal);
                        }
                        else if (savedProposal.Status != NotamProposalStatusCode.Disseminated && savedProposal.Status != NotamProposalStatusCode.DisseminatedModified)
                        {
                            if ((savedProposal.UserId != ApplicationContext.GetUserId())) throw new Exception(Resources.OtherUserWorkingProposal);
                        }
                    }

                    var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                    var context = await GetProposalContextAsync(proposalVm, userDoa);
                    await nsdManager.UpdateContext(proposalVm, context);

                    var validateResult = await nsdManager.Validate(proposalVm, context);
                    if (validateResult.HasErrors)
                        throw new Exception(validateResult.ErrorMessages.Count > 0 ? validateResult.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation);

                    var icaoResult = await nsdManager.GenerateIcao(proposalVm, context);
                    if (!icaoResult)
                        throw new Exception(Resources.ErrorGeneratingICAO);

                    var proposal = MapModelToProposal(nsdManager, savedProposal, proposalVm, orgId, status);

                    if (proposal.Id > 0)
                    {
                        if (masterGroupId == Guid.Empty)
                        {
                            masterGroupId = Guid.NewGuid();
                            proposal.IsGroupMaster = proposal.Id == proposalVm.ProposalId; //master is first of the group when proposal exist and group created.
                        }
                        else
                            proposal.IsGroupMaster = proposal.IsGroupMaster; //group exist so use the actual master.

                        proposal.Grouped = proposal.Grouped;
                        proposal.GroupId = masterGroupId;

                        Uow.ProposalRepo.Update(proposal);
                    }
                    else
                    {
                        if (masterGroupId == Guid.Empty)
                        {
                            masterGroupId = Guid.NewGuid();
                            proposal.IsGroupMaster = true;
                            proposal.Grouped = false;
                            proposal.GroupId = masterGroupId;
                        }
                        else
                        {
                            proposal.IsGroupMaster = false;
                            proposal.Grouped = true;
                            proposal.GroupId = masterGroupId;
                        }

                        Uow.ProposalRepo.Add(proposal);
                    }
                    await Uow.SaveChangesAsync();

                    Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                    await Uow.SaveChangesAsync();

                    proposalVm.ProposalId = proposal.Id;
                    proposalVm.StatusUpdated = false; // Not need to restore previous status if Back button pressed after saving

                    SetIcaoText(proposalVm);
                }

                //Uow.Commit();

                return ApiStepResult.Fail(Ok(proposalsViewModel[0]));
            }
            catch (Exception e)
            {
                //Uow.Rollback();
                return ApiStepResult.Fail(BadRequest(e.Message));
            }
        }

        protected async Task RemoveProposalsFromGroupIfUserDelete(IUow uow, List<ProposalViewModel> proposalsViewModel, Guid masterGroupId)
        {
            //This method will be called inside of a transaction
            var groupProposalsSaved = await uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(masterGroupId);
            var proposalsRemoved = groupProposalsSaved.Where(item1 => !proposalsViewModel.Any(item2 => item1.Id == item2.ProposalId)).ToList();

            foreach (var pr in proposalsRemoved)
            {
                uow.ProposalRepo.Delete(pr);
                var proposalHistories = await uow.ProposalHistoryRepo.GetByProposalIdAsync(pr.Id);
                foreach (var ph in proposalHistories)
                    uow.ProposalHistoryRepo.Delete(ph);
                await uow.SaveChangesAsync();
            }
        }

        protected async Task<ApiStepResult> GetProposalModel(int id, bool enforce)
        {
            var proposal = await Uow.ProposalRepo.GetByIdAsync(id);

            if (proposal == null)
                return ApiStepResult.Fail(NotFound());

            // detach from the DB entity
            Uow.ProposalRepo.Detach(proposal);

            if (await IsValidUserAction(proposal.CategoryId) == false)
                return ApiStepResult.Fail(Unauthorized());

            var OriginalOrg = proposal.ModifiedByOrg;
            var OriginalUser = proposal.ModifiedByUsr;

            List<Proposal> proposalsGrouped = new List<Proposal>();
            if (proposal.GroupId.HasValue)
                proposalsGrouped = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(proposal.GroupId.Value);

            var nsdManager = _managerResolver.GetNsdManager($"{proposal.CategoryId}-{proposal.Version}");
            var nofUser = await IsNOFUser();
            var parentNotamId = await RetrieveParentNotamId(proposal);
            var needToUpdate = true;
            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            if (await IsEditableStatus(proposal.Status))
            {
                if (nofUser)
                {
                    if (proposal.Status == NotamProposalStatusCode.Withdrawn)
                    {
                        needToUpdate = false;
                        proposal.Status = NotamProposalStatusCode.Taken;
                    }
                    else if (proposal.Status == NotamProposalStatusCode.Picked || proposal.Status == NotamProposalStatusCode.ParkedPicked)
                    {
                        if (proposal.UserId == ApplicationContext.GetUserId())
                        {
                            needToUpdate = false;
                        }
                        else if (!enforce)
                        {
                            needToUpdate = false;
                            proposal.Status = NotamProposalStatusCode.Taken;
                        }
                    }
                    else
                    {
                        if (proposal.Status == NotamProposalStatusCode.Parked || proposal.Status == NotamProposalStatusCode.ParkedDraft)
                        {
                            proposal.Status = NotamProposalStatusCode.ParkedPicked;
                        }
                        else
                        {
                            proposal.Status = NotamProposalStatusCode.Picked;
                        }
                    }

                    if (needToUpdate)
                    {
                        var organization = await Uow.OrganizationRepo.GetByIdAsync(orgId);
                        var r = proposal.GroupId.HasValue && proposalsGrouped.Count > 0
                            ? await UpdateInternalProposalGroup(proposalsGrouped, proposal.Status, organization?.Name)
                            : await UpdateInternalProposal(proposal, organization?.Name);

                        if (r.HasErrors)
                        {
                            var errorMessage = r.ErrorMessages.Count > 0 ? r.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation;
                            return ApiStepResult.Fail(BadRequest(errorMessage));
                        }
                    }
                }
                else
                {
                    if (proposal.Status == NotamProposalStatusCode.ModifiedAndSubmitted ||
                        proposal.Status == NotamProposalStatusCode.Submitted ||
                        proposal.Status == NotamProposalStatusCode.Rejected)
                    {
                        needToUpdate = true;
                        proposal.Status = NotamProposalStatusCode.Withdrawn;
                        var organization = await Uow.OrganizationRepo.GetByIdAsync(orgId);

                        var r = proposal.GroupId.HasValue && proposalsGrouped.Count > 0
                            ? await UpdateInternalProposalGroup(proposalsGrouped, proposal.Status, organization?.Name)
                            : await UpdateInternalProposal(proposal, organization?.Name);

                        if (r.HasErrors)
                        {
                            var errorMessage = r.ErrorMessages.Count > 0 ? r.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation;
                            return ApiStepResult.Fail(BadRequest(errorMessage));
                        }
                    }
                    else
                    {
                        needToUpdate = false;
                        if (proposal.UserId != ApplicationContext.GetUserId())
                        {
                            var organization = await Uow.OrganizationRepo.GetByIdAsync(orgId);
                            needToUpdate = true;
                            var r = proposal.GroupId.HasValue && proposalsGrouped.Count > 0
                                ? await UpdateUserProposalGroup(proposalsGrouped, organization?.Name)
                                : await UpdateUserProposal(proposal, organization?.Name);

                            if (r.HasErrors)
                            {
                                var errorMessage = r.ErrorMessages.Count > 0 ? r.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation;
                                return ApiStepResult.Fail(BadRequest(errorMessage));
                            }
                        }
                    }
                }
            }
            else
            {
                // check when a nof tries to pick a withdrawn proposal
                if (nofUser && proposal.Status == NotamProposalStatusCode.Withdrawn)
                    return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalChangeStatusWithdrawn));

                // check when a user tries to withdraw a picked, parked or parkpicked proposal
                if (!nofUser && (proposal.Status == NotamProposalStatusCode.Picked || proposal.Status == NotamProposalStatusCode.Parked || proposal.Status == NotamProposalStatusCode.ParkedPicked))
                    return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalChangeStatusPicked));

                // >> this will update the proposal on the outside Uow.Commit()
                proposal.Status = NotamProposalStatusCode.Taken;
            }

            List<ProposalViewModel> modelGroup = null;
            if (proposal.GroupId.HasValue)
            {
                modelGroup = new List<ProposalViewModel>();
                foreach (var proposalGrouped in proposalsGrouped)
                {
                    var groupeModel = nsdManager.CreateViewModelFromProposal(proposalGrouped);
                    groupeModel.Attachments = await GetAllProposalAttachments(proposal);
                    groupeModel.Token.Annotations = SortAnnotationsByAscendingOrder(groupeModel.Token.Annotations);
                    groupeModel.StatusUpdated = needToUpdate;
                    groupeModel.ParentNotamId = parentNotamId;
                    modelGroup.Add(groupeModel);
                }
            }

            var model = nsdManager.CreateViewModelFromProposal(proposal);
            model.Attachments = await GetAllProposalAttachments(proposal);
            model.Token.Annotations = SortAnnotationsByAscendingOrder(model.Token.Annotations);
            model.StatusUpdated = needToUpdate;
            model.ParentNotamId = parentNotamId;
            model.GroupedProposals = modelGroup;
            model.CanBeRejected = false;
            if (nofUser)
            {
                if (proposal.OrganizationType != OrganizationType.Nof) model.CanBeRejected = true;
                else if (OriginalOrg != proposal.ModifiedByOrg) model.CanBeRejected = true;
                else if (OriginalUser != ApplicationContext.GetUserName()) model.CanBeRejected = true;
            }
            SetIcaoText(model);

            return ApiStepResult.Succeed(Ok(model));
        }

        protected async Task<ApiStepResult> DiscardUserProposal(Proposal proposal)
        {
            if (proposal.Status == NotamProposalStatusCode.Draft)
            {
                // For the new change that the user can save a Widthdraw Proposal, to do the delete,
                //we need to verify if there is not a submitted status on the History
                var allowDelete = true;
                var histories = await Uow.ProposalHistoryRepo.GetByProposalIdAsync(proposal.Id);
                allowDelete = histories?.Exists(h => h.Status == NotamProposalStatusCode.Submitted) != true;
                if (allowDelete) return await DeleteProposal(proposal); //Draft proposals cannot be grouped by a user (only by NOF)
            }

            if (proposal.GroupId.HasValue)
            {
                var groupProposals = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(proposal.GroupId.Value);
                return await DiscardGroupedProposals(groupProposals);
            }

            return await DiscardProposal(proposal);
        }

        protected async Task<ApiStepResult> DiscardNofProposal(Proposal proposal)
        {
            if (proposal.Status == NotamProposalStatusCode.ParkedDraft)
                return await DeleteProposal(proposal);
            else if (proposal.Status == NotamProposalStatusCode.Picked || 
                    proposal.Status == NotamProposalStatusCode.Parked || 
                    proposal.Status == NotamProposalStatusCode.ParkedPicked)
                return await DiscardProposal(proposal);
            else return ApiStepResult.Fail(BadRequest());
        }

        protected async Task<ApiStepResult> DiscardNofGroupProposal(Proposal proposalInGroup)
        {
            var proposalsGrouped = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(proposalInGroup.GroupId.Value);

            if (proposalInGroup.Status == NotamProposalStatusCode.ParkedDraft)
                return await DeleteGroupedProposals(proposalsGrouped);
            else if (proposalInGroup.Status == NotamProposalStatusCode.Picked || 
                proposalInGroup.Status == NotamProposalStatusCode.Parked || 
                proposalInGroup.Status == NotamProposalStatusCode.ParkedPicked)
                return await DiscardGroupedProposals(proposalsGrouped);
            else return ApiStepResult.Fail(BadRequest());
        }

        protected async Task<ApiStepResult> RestoreLastValidStatus(Proposal proposal)
        {
            var histories = await Uow.ProposalHistoryRepo.GetByProposalIdDescAsync(proposal.Id);
            var userIsNOF = await IsNOFUser();

            var status = proposal.Status;
            ProposalHistory history;
            if (userIsNOF)
            {
                history = histories.FirstOrDefault(h => h.Status != NotamProposalStatusCode.Picked && 
                                                        h.Status != NotamProposalStatusCode.ParkedPicked &&
                                                        h.Status != NotamProposalStatusCode.Taken);
            }
            else
            {
                history = histories.FirstOrDefault(h => h.Status != proposal.Status);
            }

            if (history == null) return ApiStepResult.Fail(BadRequest());

            try
            {
                var rowVersion = proposal.RowVersion;
                proposal = ProposalFromHistory(history, proposal);
                proposal.RowVersion = rowVersion;
                Uow.ProposalRepo.Update(proposal);
                await Uow.SaveChangesAsync();

                var newHistory = new ProposalHistory();
                history.CopyPropertiesTo(newHistory);
                Uow.ProposalHistoryRepo.Add(newHistory);

                await Uow.SaveChangesAsync();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return ApiStepResult.Fail(BadRequest(Resources.ErrorRecordLocked));
            }
            catch (Exception ex)
            {
                return ApiStepResult.Fail(BadRequest(ex.Message));
            }
            return ApiStepResult.Succeed(Ok());
        }

        protected async Task<ApiStepResult> RestoreGroupLastValidStatus(Proposal proposalInGroup)
        {
            var proposalsGrouped = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(proposalInGroup.GroupId.Value);

            try
            {
                foreach (var proposal in proposalsGrouped)
                {
                    var histories = await Uow.ProposalHistoryRepo.GetByProposalIdDescAsync(proposal.Id);
                    var userIsNOF = await IsNOFUser();

                    var status = proposal.Status;
                    if (userIsNOF) status = NotamProposalStatusCode.Picked;

                    var history = histories.FirstOrDefault(h => h.Status != status);
                    if (history == null)
                    {
                        return ApiStepResult.Fail(BadRequest());
                    }

                    var proposalFromHistory = ProposalFromHistory(history, proposal);
                    
                    Uow.ProposalRepo.Update(proposalFromHistory);
                    await Uow.SaveChangesAsync();

                    var newHistory = new ProposalHistory();
                    history.CopyPropertiesTo(newHistory);
                    
                    Uow.ProposalHistoryRepo.Add(newHistory);
                    await Uow.SaveChangesAsync();
                }

                return ApiStepResult.Succeed(Ok());
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return ApiStepResult.Fail(BadRequest(Resources.ErrorRecordLocked));
            }
            catch (Exception ex)
            {
                return ApiStepResult.Fail(BadRequest(ex.Message));
            }
        }

        protected async Task<ValidationResult> UpdateInternalProposal(Proposal proposal, string orgName)
        {
            var ErrorList = new List<string>();
            try
            {
                proposal.UserId = ApplicationContext.GetUserId();

                //Change by request of Esteban - 20181206
                proposal.ModifiedByOrg = orgName;
                proposal.ModifiedByUsr = ApplicationContext.GetUserName();

                proposal.Received = DateTime.UtcNow.RemoveSeconds();
                
                Uow.ProposalRepo.Update(proposal);
                await Uow.SaveChangesAsync();

                Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                await Uow.SaveChangesAsync();

                return ValidationResult.Succeed();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                ErrorList.Add(Resources.ErrorRecordLocked);
                return ValidationResult.Fail(ErrorList);
            }
            catch (Exception ex)
            {
                ErrorList.Add(ex.Message);
                return ValidationResult.Fail(ErrorList);
            }
        }

        protected async Task<ValidationResult> UpdateInternalProposalGroup(List<Proposal> proposals, NotamProposalStatusCode status, string orgName)
        {
            var ErrorList = new List<string>();
            //Uow.BeginTransaction();
            try
            {
                foreach (var proposal in proposals)
                {
                    proposal.UserId = ApplicationContext.GetUserId();
                    proposal.Status = status;
                    //The Operator is the creator of the Proposal, it never change
                    //proposal.Operator = ApplicationContext.GetUserName();

                    //Change by request of Esteban - 20181206
                    proposal.ModifiedByOrg = orgName;
                    proposal.ModifiedByUsr = ApplicationContext.GetUserName();

                    proposal.Received = DateTime.UtcNow.RemoveSeconds();
                    
                    Uow.ProposalRepo.Update(proposal);
                    await Uow.SaveChangesAsync();

                    Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                    await Uow.SaveChangesAsync();
                }

                //Uow.Commit();
                return ValidationResult.Succeed();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                //Uow.Rollback();
                ErrorList.Add(Resources.ErrorRecordLocked);
                return ValidationResult.Fail(ErrorList);
            }
            catch (Exception ex)
            {
                //Uow.Rollback();
                ErrorList.Add(ex.Message);
                return ValidationResult.Fail(ErrorList);
            }
        }

        protected async Task<ProposalViewModel> CloneInternalNotam(string NotamId, int ProposalId )
        {
            Proposal proposal = null;
            var hstry = await Uow.ProposalHistoryRepo.GetByProposalIdAndNotamIdAsync(NotamId, ProposalId);
            proposal = ProposalFromHistory(hstry);
            if (proposal == null) return null;

            return await CloneProposalGeneral(proposal);
        }

        protected async Task<ProposalViewModel> CloneInternalProposal(int id, bool history = false)
        {
            Proposal proposal;
            if (!history)
            {
                proposal = await Uow.ProposalRepo.GetByIdAsync(id);
            }
            else
            {
                var hstry  = await Uow.ProposalHistoryRepo.GetByIdAsync(id);
                proposal = ProposalFromHistory(hstry);
            }
            if (proposal == null) return null;

            return await CloneProposalGeneral(proposal);
        }

        private async Task<ProposalViewModel> CloneProposalGeneral(Proposal proposal)
        {
            if (await IsValidUserAction(proposal.CategoryId) == false) return null;

            var clonedProposal = new Proposal();
            proposal.CopyPropertiesTo(clonedProposal);

            clonedProposal.UserId = ApplicationContext.GetUserId();
            clonedProposal.Operator = ApplicationContext.GetUserName();
            clonedProposal.Received = DateTime.UtcNow.RemoveSeconds();

            var nsdManager = _managerResolver.GetNsdManager($"{clonedProposal.CategoryId}-{clonedProposal.Version}");
            var model = nsdManager.CreateViewModelFromProposal(clonedProposal);
            model.Attachments = await GetAllProposalAttachments(clonedProposal);
            model.Token.Annotations = SortAnnotationsByAscendingOrder(model.Token.Annotations);

            if (proposal.GroupId.HasValue)
            {
                var vmModelGroup = new List<ProposalViewModel>();
                var proposalsGrouped = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(proposal.GroupId.Value);

                foreach (var proposalGrouped in proposalsGrouped)
                {
                    var groupModel = nsdManager.CreateViewModelFromProposal(proposalGrouped);
                    groupModel.Attachments = await GetAllProposalAttachments(proposal);
                    groupModel.Token.Annotations = SortAnnotationsByAscendingOrder(groupModel.Token.Annotations);
                    groupModel.ParentNotamId = model.ParentNotamId;

                    vmModelGroup.Add(groupModel);
                }

                model.GroupedProposals = vmModelGroup;
            }

            return model;
        }

        protected void SetIcaoText(ProposalViewModel model)
        {
            model.IcaoText = ICAOFormatUtils.Generate(model, true);
        }

        protected async Task<ApiStepResult> ValidateModelVsProposal(ProposalViewModel model)
        {
            var proposalId = model.ProposalId;
            var modelStatus = model.Status;
            if (proposalId.HasValue && proposalId.Value > 0)
            {
                var proposal = await Uow.ProposalRepo.GetByIdAsync(proposalId.Value);
                if (proposal == null)
                    return ApiStepResult.Fail(BadRequest());

                // compare row versions
                if (!CompareBytes(model.RowVersion, proposal.RowVersion))
                    return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalStatus));

                if (modelStatus == NotamProposalStatusCode.Replaced || modelStatus == NotamProposalStatusCode.Cancelled)
                {
                    // the numbers must match or someone already disseminated a new NOTAM
                    if (model.Number != proposal.Number)
                        return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalStatus));

                        // the proposal must be Disseminated or DisseminatedModified
                    if (proposal.Status != NotamProposalStatusCode.Disseminated && proposal.Status != NotamProposalStatusCode.DisseminatedModified)
                        return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalStatus));
                }
                else
                {
                    // status must match and user must match or someone else is working on this proposal
                    if (modelStatus != proposal.Status || proposal.UserId != ApplicationContext.GetUserId())
                        return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                }
            }
            return ApiStepResult.Succeed(Ok());
        }

        protected static bool CompareBytes(byte[] a, byte[] b)
        {
            var aStr = a != null ? string.Join("", a) : null;
            var bStr = b != null ? string.Join("", b) : null;
            return aStr == bStr;
        }

        protected DbGeography CalculateEfectiveArea(Proposal proposal)
        {
            return IsFirNotam(proposal)
                ? CalculateFirsEffectArea(proposal.ItemA)
                : CalculateCircleEffectArea(proposal.Coordinates, proposal.Radius);

        }

        private async Task<bool> IsEditableStatus(NotamProposalStatusCode status)
        {
            var editable = true;
            if (await IsNOFUser())
            {
                if (status != NotamProposalStatusCode.Parked &&
                    status != NotamProposalStatusCode.Picked &&
                    status != NotamProposalStatusCode.ParkedDraft &&
                    status != NotamProposalStatusCode.ParkedPicked &&
                    status != NotamProposalStatusCode.Submitted &&
                    status != NotamProposalStatusCode.Taken &&
                    status != NotamProposalStatusCode.Rejected) editable = false;
            }
            else
            {
                if (status == NotamProposalStatusCode.Disseminated) editable = false;
                else if (status == NotamProposalStatusCode.Picked) editable = false;
                else if (status == NotamProposalStatusCode.ParkedPicked) editable = false;
                else if (status == NotamProposalStatusCode.Parked) editable = false;
                else if (status == NotamProposalStatusCode.ParkedDraft) editable = false;
                else if (status == NotamProposalStatusCode.Taken) editable = false;
            }
            return editable;
        }

        private async Task<ApiStepResult> DiscardProposal(Proposal proposal)
        {
            var histories = await Uow.ProposalHistoryRepo.GetByProposalIdDescAsync(proposal.Id);
            if (histories == null) return ApiStepResult.Fail(BadRequest("Error retrieving the Proposal history"));

            var userIsNOF = await IsNOFUser();

            if (!userIsNOF)
            {
                if (proposal.ProposalType == NotamType.C || proposal.ProposalType == NotamType.R)
                {
                    var history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Disseminated || h.Status == NotamProposalStatusCode.DisseminatedModified);
                    if (history == null) return ApiStepResult.Fail(BadRequest());
                    return await DiscardInternalProposal(proposal, history);
                }
                else
                {   
                    var history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Disseminated || h.Status == NotamProposalStatusCode.DisseminatedModified);
                    //if we found any history that have status Disseminated or DisseminatedModified, it is an error
                    if (history != null) return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalDisseminated));
                    proposal.Status = NotamProposalStatusCode.Deleted;
                    proposal.Delete = true;
                    var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                    var organization = await Uow.OrganizationRepo.GetByIdAsync(orgId);

                    var r = await UpdateInternalProposal(proposal, organization?.Name);
                    if (r.HasErrors)
                    {
                        var errorMessage = r.ErrorMessages.Count > 0 ? r.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation;
                        return ApiStepResult.Fail(BadRequest(errorMessage));
                    }
                    return ApiStepResult.Succeed(Ok());
                }
            }
            else // NOF
            {
                var history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Submitted);
                if(history == null )
                {
                    if (proposal.ProposalType == NotamType.C || proposal.ProposalType == NotamType.R)
                    {
                        history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Disseminated || h.Status == NotamProposalStatusCode.DisseminatedModified);
                        if (history == null) return ApiStepResult.Fail(BadRequest());
                        return await DiscardInternalProposal(proposal, history);
                    }
                    return await DeleteProposal(proposal);
                }
                return await DiscardInternalProposal(proposal, history);
                
                //var history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Submitted);
                //if (history == null) return await DeleteProposal(proposal);
                //return await DiscardInternalProposal(proposal, history);
            }
        }

        private async Task<ApiStepResult> DiscardGroupedProposals(List<Proposal> proposals)
        {
            //Uow.BeginTransaction();
            try
            {
                var userIsNOF = await IsNOFUser();
                string errorMessage = null;
                foreach (var proposal in proposals)
                {
                    if (!string.IsNullOrEmpty(errorMessage))
                        break;

                    var histories = await Uow.ProposalHistoryRepo.GetByProposalIdDescAsync(proposal.Id);
                    if (histories == null)
                    {
                        errorMessage = "Error retrieving the Proposal history";
                        break;
                    }
                    if (!userIsNOF)
                    {
                        if (proposal.ProposalType == NotamType.C || proposal.ProposalType == NotamType.R)
                        {
                            var history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Disseminated || h.Status == NotamProposalStatusCode.DisseminatedModified);
                            if (history == null)
                            {
                                errorMessage = "Non History Found";
                                break;
                            }
                            await DiscardInternaGroupedlProposal(proposal, history);
                        }
                        else
                        {   // Never been Disseminated
                            var history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Disseminated || h.Status == NotamProposalStatusCode.DisseminatedModified);
                            //if we found any history that have status Disseminated or DisseminatedModified, it is an error
                            if (history != null)
                            {
                                errorMessage = Resources.ErrorProposalDisseminated;
                                break;
                            }
                            proposal.Status = NotamProposalStatusCode.Deleted;
                            proposal.Delete = true;
                            proposal.UserId = ApplicationContext.GetUserId();
                            //proposal.Operator = ApplicationContext.GetUserName();
                            proposal.Received = DateTime.UtcNow.RemoveSeconds();
                            
                            Uow.ProposalRepo.Update(proposal);
                            await Uow.SaveChangesAsync();

                            Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                            await Uow.SaveChangesAsync();
                        }
                    }
                    else // NOF
                    {
                        var history = histories.FirstOrDefault(h => h.Status == NotamProposalStatusCode.Submitted);
                        if (history == null)
                        {
                            await DeleteGroupedProposal(proposal);
                        }
                        else
                        {
                            await DiscardInternaGroupedlProposal(proposal, history);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    //Uow.Rollback();
                    return ApiStepResult.Fail(BadRequest(errorMessage));
                }

                //Uow.Commit();
                return ApiStepResult.Succeed(Ok());
            }
            catch (Exception e)
            {
                //Uow.Rollback();
                return ApiStepResult.Fail(InternalServerError(e));
            }
        }

        private async Task<ApiStepResult> DiscardInternalProposal(Proposal proposal, ProposalHistory history)
        {
            proposal.Status = NotamProposalStatusCode.Discarded;
            //Uow.BeginTransaction();
            try
            {
                proposal.UserId = ApplicationContext.GetUserId();
                proposal.Received = DateTime.UtcNow.RemoveSeconds();

                Uow.ProposalRepo.Update(proposal);
                await Uow.SaveChangesAsync();

                Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                await Uow.SaveChangesAsync();

                var proposalID = proposal.Id;
                var rowVersion = proposal.RowVersion;
                var pendingReview = proposal.PendingReview;
                history.CopyPropertiesTo(proposal);
                proposal.Id = proposalID;
                proposal.RowVersion = rowVersion;
                proposal.PendingReview = pendingReview;

                proposal.Received = DateTime.UtcNow;
                
                Uow.ProposalRepo.Update(proposal);
                await Uow.SaveChangesAsync();

                Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
                await Uow.SaveChangesAsync();

                ////Uow.Commit();
                return ApiStepResult.Succeed(Ok());
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                //Uow.Rollback();
                return ApiStepResult.Fail(BadRequest(Resources.ErrorRecordLocked));
            }
            catch (Exception ex)
            {
                //Uow.Rollback();
                return ApiStepResult.Fail(BadRequest(ex.Message));
            }
        }

        private async Task DiscardInternaGroupedlProposal(Proposal proposal, ProposalHistory history)
        {
            proposal.Status = NotamProposalStatusCode.Discarded;
            proposal.UserId = ApplicationContext.GetUserId();
            //proposal.Operator = ApplicationContext.GetUserName();
            proposal.Received = DateTime.UtcNow.RemoveSeconds();
            proposal.GroupId = null;
            proposal.Grouped = false;
            proposal.IsGroupMaster = false;

            Uow.ProposalRepo.Update(proposal);
            await Uow.SaveChangesAsync();

            Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
            await Uow.SaveChangesAsync();

            var proposalID = proposal.Id;
            var rowVersion = proposal.RowVersion;
            history.CopyPropertiesTo(proposal);
            proposal.Id = proposalID;
            proposal.RowVersion = rowVersion;

            proposal.Received = DateTime.UtcNow;
            
            Uow.ProposalRepo.Update(proposal);
            await Uow.SaveChangesAsync();

            Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));
            await Uow.SaveChangesAsync();
        }

        private async Task<ValidationResult> UpdateUserProposal(Proposal proposal, string orgName)
        {
            var ErrorList = new List<string>();
            try
            {
                var lastHistory = await Uow.ProposalHistoryRepo.GetLatestByProposalIdAsync(proposal.Id);
                if (lastHistory == null)
                {
                    ErrorList.Add(Resources.ErrorOnDB);
                    return ValidationResult.Fail(ErrorList);
                }
                proposal.UserId = ApplicationContext.GetUserId();
                proposal.Received = DateTime.UtcNow.RemoveSeconds();

                //Change by request of Esteban - 20181206
                proposal.ModifiedByOrg = orgName;
                proposal.ModifiedByUsr = ApplicationContext.GetUserName();

                lastHistory.UserId = ApplicationContext.GetUserId();
                lastHistory.Received = proposal.Received;

                //Change by request of Esteban - 20181206
                lastHistory.ModifiedByOrg = orgName;
                lastHistory.ModifiedByUsr = ApplicationContext.GetUserName();

                Uow.ProposalRepo.Update(proposal);
                await Uow.SaveChangesAsync();

                var newHistory = new ProposalHistory();
                lastHistory.CopyPropertiesTo(newHistory);

                Uow.ProposalHistoryRepo.Add(newHistory);
                await Uow.SaveChangesAsync();

                return ValidationResult.Succeed();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                ErrorList.Add(Resources.ErrorRecordLocked);
                return ValidationResult.Fail(ErrorList);
            }
            catch (Exception ex)
            {
                ErrorList.Add(ex.Message);
                return ValidationResult.Fail(ErrorList);
            }
        }

        private async Task<ValidationResult> UpdateUserProposalGroup(List<Proposal> proposals, string orgName)
        {
            var ErrorList = new List<string>();
            //Uow.BeginTransaction();
            try
            {
                foreach (var proposal in proposals)
                {
                    var lastHistory = await Uow.ProposalHistoryRepo.GetLatestByProposalIdAsync(proposal.Id);
                    if (lastHistory == null)
                    {
                        //Uow.Rollback();
                        ErrorList.Add(Resources.ErrorOnDB);
                        return ValidationResult.Fail(ErrorList);
                    }
                    proposal.UserId = ApplicationContext.GetUserId();
                    //proposal.Operator = ApplicationContext.GetUserName();
                    proposal.Received = DateTime.UtcNow.RemoveSeconds();

                    //Change by request of Esteban - 20181206
                    proposal.ModifiedByOrg = orgName;
                    proposal.ModifiedByUsr = ApplicationContext.GetUserName();

                    lastHistory.UserId = ApplicationContext.GetUserId();
                    //lastHistory.Operator = ApplicationContext.GetUserName();
                    lastHistory.Received = proposal.Received;

                    //Change by request of Esteban - 20181206
                    lastHistory.ModifiedByOrg = orgName;
                    lastHistory.ModifiedByUsr = ApplicationContext.GetUserName();

                    Uow.ProposalRepo.Update(proposal);
                    await Uow.SaveChangesAsync();

                    var newHistory = new ProposalHistory();
                    lastHistory.CopyPropertiesTo(newHistory);
                    
                    Uow.ProposalHistoryRepo.Add(newHistory);
                    await Uow.SaveChangesAsync();
                }

                //Uow.Commit();
                return ValidationResult.Succeed();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                //Uow.Rollback();
                ErrorList.Add(Resources.ErrorRecordLocked);
                return ValidationResult.Fail(ErrorList);
            }
            catch (Exception ex)
            {
                //Uow.Rollback();
                ErrorList.Add(ex.Message);
                return ValidationResult.Fail(ErrorList);
            }
        }

        private async Task<ApiStepResult> DeleteProposal(Proposal proposal)
        {
            if (await IsValidUserAction(proposal.CategoryId) == false) return ApiStepResult.Fail(Unauthorized());
            try
            {
                try
                {
                    var attachments = await Uow.ProposalAttachmentRepo.GetByProposalIdAsync(proposal.Id);
                    if(attachments != null)
                    {
                        foreach (var attach in attachments)
                        {
                            Uow.ProposalAttachmentRepo.Delete(attach);
                        }
                    }

                    var history = await Uow.ProposalHistoryRepo.GetByProposalIdAsync(proposal.Id);
                    if (history != null)
                    {
                        foreach (ProposalHistory propH in history)
                        {
                            Uow.ProposalHistoryRepo.Delete(propH.Id);
                        }
                    }

                    var itemD = await Uow.ItemDRepo.GetByProposalId(proposal.Id);
                    if(itemD != null)
                    {
                        foreach (var item in itemD)
                        {
                            Uow.ItemDRepo.Delete(item.Id);
                        }
                    }

                    Uow.ProposalRepo.Delete(proposal);
                    await Uow.SaveChangesAsync();

                    return ApiStepResult.Succeed(Ok());
                }
                catch (Exception e)
                {
                    return ApiStepResult.Fail(BadRequest(e.Message));
                }
            }
            catch (Exception e)
            {
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " - " + e.Message));
            }
        }

        private async Task DeleteGroupedProposal(Proposal proposal)
        {
            var history = await Uow.ProposalHistoryRepo.GetByProposalIdAsync(proposal.Id);
            if (history != null)
            {
                foreach (ProposalHistory propH in history)
                {
                    Uow.ProposalHistoryRepo.Delete(propH.Id);
                }
            }
            Uow.ProposalRepo.Delete(proposal);
            await Uow.SaveChangesAsync();
        }

        private async Task<ApiStepResult> DeleteGroupedProposals(List<Proposal> proposals)
        {
            if (await IsValidUserAction(proposals[0].CategoryId) == false)
                return ApiStepResult.Fail(Unauthorized());

            try
            {
                //Uow.BeginTransaction();
                foreach (var proposal in proposals)
                {
                    var history = await Uow.ProposalHistoryRepo.GetByProposalIdAsync(proposal.Id);
                    if (history != null)
                    {
                        foreach (ProposalHistory propH in history)
                        {
                            Uow.ProposalHistoryRepo.Delete(propH.Id);
                        }
                    }
                    Uow.ProposalRepo.Delete(proposal);
                    await Uow.SaveChangesAsync();
                }
                //Uow.Commit();
                return ApiStepResult.Succeed(Ok());
            }
            catch (Exception e)
            {
                //Uow.Rollback();
                return ApiStepResult.Fail(BadRequest(e.Message));
            }
        }

        private const int FirRadius = 999;
        private bool IsFirNotam(INotam notam) => notam.Radius == FirRadius && SplitFirs(notam.ItemA).All(f => Uow.SdoCacheRepo.FirExists(f));
        private static string[] SplitFirs(string itemA) => (itemA ?? "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        /// <summary>
        /// Calculates the Effect Area based on center and radius
        /// </summary>
        private DbGeography CalculateCircleEffectArea(string coordinates, int radius)
        {
            var dmsLocation = DMSLocation.FromDMS(coordinates);
            if (dmsLocation == null)
                throw new Exception($"Invalid DMS coordinates ({coordinates}) found.");

            var latitude = dmsLocation.Latitude.AsDecimal;
            var longitude = dmsLocation.Longitude.AsDecimal;
            var point = DbGeography.PointFromText($"POINT({longitude} {latitude})", CommonDefinitions.Srid); // POINT(long, lat)

            return point.Buffer(radius * 1852.0);
        }

        /// <summary>
        /// Calculates the Effect Area based on firs
        /// </summary>
        private DbGeography CalculateFirsEffectArea(string itemA)
        {
            DbGeography geo = null;
            foreach (var d in SplitFirs(itemA))
            {
                var firGeo = Uow.SdoCacheRepo.GetFirGeography(d);
                geo = geo != null ? geo.Union(firGeo) : firGeo;
            }
            return geo;
        }

        protected readonly IDashboardContext _dashboardContext;
        protected INsdManagerResolver _managerResolver;
        private IEventHubContext _eventHubContext;
    }
}