﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize(Roles = "Administrator, Organization Administrator, CCS, NOF_ADMINISTRATOR")]
    [RoutePrefix("api/organizationnsdcategory")]
    public class OrganizationNsdCategoryController : ApiBaseController
    {
        //TODO: Add resources
        public OrganizationNsdCategoryController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger)
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [Route("removensdsinorganizationbynsdid")]
        [HttpPost]
        public async Task<IHttpActionResult> RemoveNsdFromOrganization([FromBody]NsdManagementDto nsddto)
        {
            return await LogTransaction(async () =>
            {
                if (!ModelState.IsValid) return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                if (!nsddto.IsSelected) return ApiStepResult.Fail(BadRequest(Resources.BadRequest + ": NSD was not selected"));

                //Uow.BeginTransaction();
                try
                {
                    var success = await Uow.OrganizationNsdRepo.RemoveNsdInOrganizationByNsdId(nsddto.Id, nsddto.OrgId);

                    if (success)
                    {
                        await Uow.SaveChangesAsync();
                        //Uow.Commit();

                        Logger.LogInfo(GetType(), $"RemoveNsdFromOrganization(NSD: '{nsddto.Name}', orgId: {nsddto.OrgId}) triggered by '{ApplicationContext.GetUserName()}'.");

                        return ApiStepResult.Succeed(Ok(nsddto.Name));
                    }

                    //Uow.Rollback();
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest));
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }
            }, "removensdsinorganizationbynsdid");
        }

        [Route("removensdfromallorganizations")]
        [HttpPost]
        public async Task<IHttpActionResult> RemoveNsdFromAllOrganizations([FromBody]NsdManagementDto nsddto)
        {
            return await LogTransaction(async () =>
             {
                 if (!ModelState.IsValid) return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                 if (!nsddto.IsSelected) return ApiStepResult.Fail(BadRequest(Resources.BadRequest + ": NSD was not selected"));

                 //Uow.BeginTransaction();
                 try
                 {
                     var success = await Uow.OrganizationNsdRepo.RemoveNsdFromAllOrganizationsByNsdId(nsddto.Id);

                     if (success)
                     {
                         await Uow.SaveChangesAsync();
                         //Uow.Commit();

                         Logger.LogInfo(GetType(), $"RemoveNsdFromAllOrganizations(NSD: '{nsddto.Name}') triggered by '{ApplicationContext.GetUserName()}'.");

                         return ApiStepResult.Succeed(Ok(nsddto.Name));
                     }

                     //Uow.Rollback();
                     return ApiStepResult.Fail(BadRequest(Resources.BadRequest));
                 }
                 catch (Exception e)
                 {
                     //Uow.Rollback();
                     return ApiStepResult.Fail(BadRequest(Resources.BadRequest + $": {e}"));
                 }
             }, "removensdfromallorganizations");
        }

        [Route("addnsdtoorganization")]
        [HttpPost]
        public async Task<IHttpActionResult> AddNsdToOrganization([FromBody]NsdManagementDto nsddto)
        {
            return await LogTransaction(async () =>
             {
                 if (!ModelState.IsValid) return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

                 if (!nsddto.IsSelected) return ApiStepResult.Fail(BadRequest(Resources.BadRequest + ": NSD was not selected"));

                 //Uow.BeginTransaction();
                 try
                 {
                     bool success = await Uow.OrganizationNsdRepo.AddNsdToOrganization(nsddto.Id, nsddto.OrgId);

                     if (success)
                     {
                         await Uow.SaveChangesAsync();
                         //Uow.Commit();

                         Logger.LogInfo(GetType(), $"AddNsdToOrganization(NSD: '{nsddto.Name}', orgId: {nsddto.OrgId}) triggered by '{ApplicationContext.GetUserName()}'.");

                         return ApiStepResult.Succeed(Ok(nsddto.Name));
                     }

                     //Uow.Rollback();

                     return ApiStepResult.Fail(BadRequest(Resources.BadRequest));
                 }
                 catch (Exception)
                 {
                     //Uow.Rollback();
                     throw;
                 }
             }, "addnsdtoorganization");
        }
    }
}