﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.TaskEngine.Mto;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using NDS.Relay;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/nof")]
    public class MessageQueueController : ApiBaseController
    {
        public MessageQueueController(IUow uow, IAeroRdsProxy aeroRdsProxy, INotamQueuePublisher queuePublisher, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _queuePublisher = queuePublisher;
        }

        [HttpGet]
        [Route("messages/{id}")]
        public async Task<IHttpActionResult> GetMessage(Guid id)
        {
            return await LogAction(async () =>
            {
                var userName = User.Identity.Name;
                var message = await Uow.MessageExchangeQueueRepo.GetByIdAsync(id);

                if (!message.Read)
                {
                    message.Read = true;
                    Uow.MessageExchangeQueueRepo.Update(message);
                    await Uow.SaveChangesAsync();
                }

                if (!message.Locked && message.LastOwner != userName)
                {
                    return BadRequest(); // Should be picked by the same user
                }

                return Ok(message);
            }, "api/nof/messages/{id}");
        }

        [HttpGet]
        [Route("messages/lock/{id}/{takeOwnership}")]
        public async Task<IHttpActionResult> LockMessage(Guid id, Boolean takeOwnership)
        {
            return await LogAction(async () =>
            {
                var userName = User.Identity.Name;
                var message = await Uow.MessageExchangeQueueRepo.GetByIdAsync(id);
                if (!takeOwnership)
                {
                    if (message.Locked)
                    {
                        if (message.LastOwner != userName)
                        {
                            return Ok(new { Success = false, Owner = message.LastOwner });
                        }
                    }
                }

                message.Locked = true;
                message.LastOwner = userName;
                message.LastUpdated = DateTime.UtcNow;

                await Uow.SaveChangesAsync();

                return Ok(new { Success = true });

            }, "api/nof/messages/lock/{id}/{takeOwnership}");
        }

        [HttpGet]
        [Route("messages/islocked/{id}")]
        public async Task<IHttpActionResult> IsMessageLocked(Guid id)
        {
            return await LogAction(async () =>
            {
                var userName = User.Identity.Name;
                var message = await Uow.MessageExchangeQueueRepo.GetByIdAsync(id);
                if (message.Locked)
                {
                    if (message.LastOwner != userName)
                    {
                        return Ok(new { Locked = true, Owner = message.LastOwner });
                    }
                }
                return Ok(new { Locked = false });

            }, "api/nof/messages/lock/{id}/{takeOwnership}");
        }

        [HttpGet]
        [Route("messages/unlock/{id}")]
        public async Task<IHttpActionResult> UnlockMessage(Guid id)
        {
            return await LogAction(async () =>
            {
                var message = await Uow.MessageExchangeQueueRepo.GetByIdAsync(id);
                if (!message.Locked)
                {
                    //if (message.LastOwner != User.Identity.Name)
                    {
                        return Ok(false);
                    }
                }

                message.Locked = false;
                message.LastOwner = "";
                message.LastUpdated = DateTime.UtcNow;

                await Uow.SaveChangesAsync();

                return Ok(true);

            }, "api/nof/messages/unlock/{id}");
        }

        [HttpPost]
        [Route("messages")]
        public async Task<IHttpActionResult> SaveMessage(MessageQueueViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                Uow.BeginTransaction();
                try
                {
                    var message = new MessageExchangeQueue
                    {
                        Recipients = model.Recipients.ToUpper(),
                        Status = model.Status,
                        ClientType = model.ClientType,
                        Body = model.Body.ToUpper(),
                        Created = DateTime.UtcNow,
                        LastUpdated = DateTime.UtcNow,
                        ClientName = model.ClientName,
                        MessageType = model.MessageType,
                        Locked = model.Locked,
                        Read = false
                    };

                    Uow.MessageExchangeQueueRepo.Add(message);
                    await Uow.SaveChangesAsync();

                    Uow.Commit();

                    model.Id = message.Id;

                    return Ok(model);
                }
                catch (Exception)
                {
                    Uow.Rollback();
                    throw;
                }

            }, "api/nof/messages");
        }

        [HttpPut]
        [Route("incomingmessages")]
        public async Task<IHttpActionResult> UpdateMessage(MessageQueueViewModel model)
        {
            return await LogAction(async () =>
            {
                var message = await Uow.MessageExchangeQueueRepo.GetByIdAsync(model.Id);
                if (message == null)
                    return NotFound();

                if (string.IsNullOrEmpty(message.LastOwner) && model.LastOwner != User.Identity.Name)
                {
                    model.AnyError = true;
                    model.Status = MessageExchangeStatus.AlreadyTaken;
                    return Ok(model);
                }

                if (message.Status != model.PrevStatus)
                {
                    model.AnyError = true;
                    model.LastOwner = message.LastOwner;
                    model.LastUpdated = message.LastUpdated;
                    return Ok(model);
                }

                message.Status = model.Status;
                message.LastUpdated = DateTime.UtcNow;
                message.LastOwner = User.Identity.Name;
                message.Locked = false;
                message.Read = model.Read;

                Uow.MessageExchangeQueueRepo.Update(message);
                await Uow.SaveChangesAsync();

                return Ok(model);

            }, "api/nof/incomingmessages");
        }

        [HttpDelete]
        [Route("messages/{id}")]
        public async Task<IHttpActionResult> DeleteMessage(Guid id)
        {
            return await LogAction(async () =>
            {
                var message = await Uow.MessageExchangeQueueRepo.GetByIdAsync(id);
                if (message == null)
                    return NotFound();

                message.Status = MessageExchangeStatus.Deleted;
                message.LastUpdated = DateTime.UtcNow;

                Uow.MessageExchangeQueueRepo.Update(message);
                await Uow.SaveChangesAsync();

                return Ok();

            }, "api/nof/messages/{id}");
        }

        [HttpPost, Route("incomingmessages/page/{status}", Name = "RegisteredIncomingMessagesPage")]
        public async Task<IHttpActionResult> GetIncomingMessagesInPage(MessageExchangeStatus status, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.MessageExchangeQueueRepo.GetAllIncomingCountAsync(status);
                var clients = await Uow.MessageExchangeQueueRepo.GetAllIncomingAsync(status, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredIncomingMessagesPage", clients, totalCount));

            }, "api/nof/incomingmessages/page/{status}");
        }

        [HttpPost, Route("outgoingmessages/page/{status}", Name = "RegisteredOutgoingMessagesPage")]
        public async Task<IHttpActionResult> GetOutgoingMessagesInPage(MessageExchangeStatus status, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.MessageExchangeQueueRepo.GetAllOutgoingCountAsync(status);
                var clients = await Uow.MessageExchangeQueueRepo.GetAllOutgoingAsync(status, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredOutgoingMessagesPage", clients, totalCount));

            }, "api/nof/outgoingmessages/page/{status}");
        }

        [HttpPost, Route("parkedmessages/page/{status}", Name = "RegisteredParkedMessagesPage")]
        public async Task<IHttpActionResult> GetParkedMessagesInPage(MessageExchangeStatus status, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.MessageExchangeQueueRepo.GetAllParkedCountAsync();
                var clients = await Uow.MessageExchangeQueueRepo.GetAllParkedMessagesAsync(queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredParkedMessagesPage", clients, totalCount));

            }, "api/nof/parkedmessages/page/{status}");
        }

        [HttpPost, Route("outgoingmessages")]
        public async Task<IHttpActionResult> SaveOutgoingMessagesInPage(MessageQueueViewModel model)
        {
            return await LogTransaction(async () =>
            {
                if (model.ReplyToId.HasValue && model.ClientName != User.Identity.Name)
                    return ApiStepResult.Fail(BadRequest("Another user has taken message"));

                // convert message body to uppercase
                var upperMessageBody = (model.Body ?? "").ToUpper();

                try
                {
                    AftnHelper.ValidateAftnChars(upperMessageBody, "Message contains invalid char '{0}'");
                    AftnHelper.ValidateAftnAddresses(model.Recipients?.ToUpper());
                    AftnHelper.ValidateAftnAddresses(model.ClientAddress?.ToUpper());
                }
                catch (Exception ex)
                {
                    return ApiStepResult.Fail(BadRequest(ex.Message));
                }

                var now = DateTime.UtcNow;

                var clientAddress = model.ClientAddress ?? AftnConsts.NOFAddress;

                var msg = model.ReplyToId.HasValue ? await Uow.MessageExchangeQueueRepo.GetByIdAsync(model.ReplyToId.Value) : null;

                //Uow.BeginTransaction();
                try
                {
                    // update replied message
                    if (msg != null)
                    {
                        msg.Locked = false;
                        msg.Status = MessageExchangeStatus.Reply;
                        msg.LastOwner = null;
                        Uow.MessageExchangeQueueRepo.Update(msg);

                        await Uow.SaveChangesAsync();
                    }

                    // save new message
                    var messageExchangeQueue = new MessageExchangeQueue
                    {
                        Id = Guid.NewGuid(),
                        Recipients = model.Recipients.ToUpper(),
                        ClientName = model.ClientName,
                        ClientAddress = clientAddress.ToUpper(),
                        Body = upperMessageBody.ToUpper(),
                        Inbound = false,
                        ClientType = model.ClientType,
                        MessageType = model.MessageType,
                        PriorityCode = model.PriorityCode,
                        Read = true,
                        Status = MessageExchangeStatus.None,
                        Locked = false,
                        Created = now,
                        LastUpdated = now
                    };

                    Uow.MessageExchangeQueueRepo.Add(messageExchangeQueue);

                    await Uow.SaveChangesAsync();

                    model.Id = messageExchangeQueue.Id;

                    PublishMessageTask(messageExchangeQueue.Id);

                    //Uow.Commit();
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }

                return ApiStepResult.Succeed(Ok(model));

            }, "api/nof/outgoingmessages");
        }

        [HttpGet]
        [Route("messages/templates")]
        public async Task<IHttpActionResult> GetTemplates()
        {
            return await LogAction(async () =>
                Ok(await Uow.MessageExchangeTemplateRepo.GetAllPartialsAsync()), $"api/nof/messages/templates");         
        }

        [HttpGet]
        [Route("messages/templates/names")]
        public async Task<IHttpActionResult> GetTemplateNames()
        {
            return await LogAction(async () =>
                Ok(await Uow.MessageExchangeTemplateRepo.GetAllTemplateKeyParValuesAsync()), $"api/nof/messages/templates/names");
        }


        [HttpGet]
        [Route("messages/templates/{id}")]
        public async Task<IHttpActionResult> GetTemplate(Guid id)
        {
            return await LogAction(async () =>
                Ok(await Uow.MessageExchangeTemplateRepo.GetByIdAsync(id)), $"api/nof/messages/templates/{id}");
        }

        [HttpDelete]
        [Route("messages/templates/{id}")]
        public async Task<IHttpActionResult> DeleteTemplate(Guid id)
        {
            return await LogAction(async () =>
            {
                var template = await Uow.MessageExchangeTemplateRepo.GetByIdAsync(id);
                if (template == null)
                    return NotFound();

                Uow.MessageExchangeTemplateRepo.Delete(template);
                await Uow.SaveChangesAsync();

                return Ok();

            }, "api/nof/messages/templates/{id}");
        }


        [HttpGet]
        [Route("messages/users/templates/{username}")]
        public async Task<IHttpActionResult> GetUserTemplate(string username)
        {
            return await LogAction(async () =>
                Ok(await Uow.NofTemplateMessageRecordRepo.GetAllByUsernameAsync(username)), $"api/nof/messages/templates/{username}");
        }

        [HttpGet]
        [Route("messages/park/count")]
        public async Task<IHttpActionResult> GetParkMessagesCount()
        {
            return await LogAction(async () =>
                Ok(await Uow.MessageExchangeQueueRepo.GetAllParkedCountAsync()), "api/nof/messages/park/count");
        }

        [HttpGet]
        [Route("messages/unread/count")]
        public async Task<IHttpActionResult> GetUnreadMessagesCount()
        {
            return await LogAction(async () =>
                Ok(await Uow.MessageExchangeQueueRepo.GetAllUnreadCountAsync()), "api/nof/messages/unread/count");
        }

        [HttpPost]
        [Route("messages/templates")]
        public async Task<IHttpActionResult> SaveMessageTemplate(MessageTemplateViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var template = await Uow.MessageExchangeTemplateRepo.GetByNameAsync(model.Name);
                if (template != null)
                    return BadRequest("The name of the template has been taken.");

                template = new MessageExchangeTemplate
                {
                    Id = Guid.NewGuid(),
                    Name = model.Name,
                    Addresses = model.Addresses,
                    Body = model.Body.ToUpper(),
                    Created = DateTime.UtcNow
                };

                Uow.MessageExchangeTemplateRepo.Add(template);
                await Uow.SaveChangesAsync();

                model.Id = template.Id;

                return Ok(model);

            }, "api/nof/messages/templates");
        }


        [HttpPut]
        [Route("messages/templates/")]
        public async Task<IHttpActionResult> UpdateMessageTemplate(MessageTemplateViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var template = await Uow.MessageExchangeTemplateRepo.GetByIdAsync(model.Id);
                if (template == null)
                    return NotFound();

                var count = await Uow.MessageExchangeTemplateRepo.GetCountByName(model.Name);
                if ( count > 1 || ((count == 1 && template.Name != model.Name)) )
                    return BadRequest("The name of the template has been taken.");

                template.Name = model.Name;
                template.Addresses = model.Addresses;
                template.Body = model.Body.ToUpper();

                Uow.MessageExchangeTemplateRepo.Update(template);
                await Uow.SaveChangesAsync();

                return Ok(model);

            }, "api/nof/messages/templates");
        }

        private void PublishMessageTask(Guid messageId)
        {
            var mto = MtoHelper.CreateMessage((int)MessageTransferObjectId.ProccessExchangeMsg, m => m.Add(MessageDefs.EntityIdentifierKey, messageId, DataType.String));
            _queuePublisher.Publish(Uow, mto);
        }

        readonly INotamQueuePublisher _queuePublisher;
    }
}