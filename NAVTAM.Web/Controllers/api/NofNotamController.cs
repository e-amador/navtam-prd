﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.SignalrHub;
using NAVTAM.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.UI.WebControls;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/nof")]
    public class NofNotamController : ApiBaseController
    {
        public NofNotamController(IUow uow, IAeroRdsProxy aeroRdsProxy, IEventHubContext eventHubContext, IGeoLocationCache geoLocationCache, ILogger logger)
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _eventHubContext = eventHubContext;
        }

        [Route("notams", Name = "GetNofDisseminatedNotam")]
        [HttpPost]
        public async Task<IHttpActionResult> GetNofDisseminatedNotam([FromBody]QueryOptions queryOptions)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.NotamRepo.GetAllCountAsync(queryOptions);
                var notams = await Uow.NotamRepo.GetAllAsync(queryOptions);
                if (notams.Count > 0)
                {
                    for (var i = 0; i < notams.Count; i++)
                    {
                        if (notams[i].EffectArea != null)
                        {
                            notams[i].EffectAreaGeoJson = ConvertToGeoJson(notams[i].EffectArea);
                            notams[i].EffectArea = null;
                        }
                    }
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetNofDisseminatedNotam", notams, totalCount));

            }, "api/nof/notmas");
        }

        [Route("reportnotams", Name = "GetNofReportNotam")]
        [HttpPost]
        public async Task<IHttpActionResult> GetReportNofDisseminatedNotam([FromBody]NofReportViewModel model)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                model.Start = new DateTime(model.Start.Year, model.Start.Month, model.Start.Day);
                model.End = new DateTime(model.End.Year, model.End.Month, model.End.Day, 23, 59, 59);

                if (model.filterBy == "Active" || model.filterBy == "Inactive")
                {
                    model.QueryOptions.FilterByActiveInactiveOrAll = model.filterBy == "Active" ? 1 : 2;
                }
                var notams = await Uow.NotamRepo.ReportAsync(model.Start, model.End, model.QueryOptions);
                var totalCount = await Uow.NotamRepo.ReportCountAsync(model.Start, model.End, model.QueryOptions);

                var response = GenerateNotamReportDto(notams, model.End, false, false);

                return Ok(ResponsewithPagingEnvelope(model.QueryOptions, "GetNofReportNotam", response, totalCount));

            }, "api/nof/reportnotmas");
        }

        [Route("reportnotams/csv")]
        [HttpPost]
        public async Task<IHttpActionResult> CsvReportNotams([FromBody]ReportFilterCriteria rptFlterCriteria)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var startDate = new DateTime(rptFlterCriteria.Start.Year, rptFlterCriteria.Start.Month, rptFlterCriteria.Start.Day);
                var endDate = new DateTime(rptFlterCriteria.End.Year, rptFlterCriteria.End.Month, rptFlterCriteria.End.Day, 23, 59, 59);

                List<Notam> notams = null;
                if (rptFlterCriteria.Conditions != null)
                {
                    var filterCriteria = new FilterCriteria
                    {
                        Conditions = rptFlterCriteria.Conditions,
                        QueryOptions = rptFlterCriteria.QueryOptions
                    };
                    var filterQuery = Uow.NotamRepo.PrepareQueryFilter(filterCriteria);
                    notams = await Uow.NotamRepo.ReportFilterCsvAsync(startDate, endDate, rptFlterCriteria.filterBy, filterQuery);
                }
                else
                {
                    notams = await Uow.NotamRepo.ReportCsvAsync(startDate, endDate, rptFlterCriteria.filterBy);
                }

                var data = GenerateNotamReportDto(notams, endDate, false, false);
                return Ok(GetCsvString(data, false));

            }, "api/nof/reportnotams/csv");
        }

        [Route("reportactivenotams", Name = "GetNofReportActiveNotam")]
        [HttpPost]
        public async Task<IHttpActionResult> GetReportNofDisseminatedActiveNotam([FromBody]NofReportViewModel model)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var timePeriod = ParseTimePeriod(model.StartTime, model.EndTime);

                model.Start = DateTime.ParseExact(string.Format("{0}/{1}/{2} {3}:{4}:00", model.Start.Day, model.Start.Month, model.Start.Year, timePeriod.StartHours, timePeriod.StartMinutes), "d/M/yyyy H:m:ss", CultureInfo.InvariantCulture);
                model.End = DateTime.ParseExact(string.Format("{0}/{1}/{2} {3}:{4}:59", model.Start.Day, model.Start.Month, model.Start.Year, timePeriod.EndHours, timePeriod.EndMinutes), "d/M/yyyy H:m:ss", CultureInfo.InvariantCulture);

                var notams = await Uow.NotamRepo.ReportActiveAsync(model.Start, model.End, model.QueryOptions);
                var totalCount = await Uow.NotamRepo.ReportActiveCountAsync(model.Start, model.End, model.QueryOptions);
                var response = GenerateNotamReportDto(notams, model.End, false);

                return Ok(ResponsewithPagingEnvelope(model.QueryOptions, "GetNofReportActiveNotam", response, totalCount));

            }, "api/nof/reportactivenotams");
        }

        [Route("reportactivenotams/csv")]
        [HttpPost]
        public async Task<IHttpActionResult> CsvReportActiveNotams([FromBody]ReportFilterCriteria rptFlterCriteria)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var timePeriod = ParseTimePeriod(rptFlterCriteria.StartTime, rptFlterCriteria.EndTime);

                var startDate =  DateTime.ParseExact(string.Format("{0}/{1}/{2} {3}:{4}:00", rptFlterCriteria.Start.Day, rptFlterCriteria.Start.Month, rptFlterCriteria.Start.Year, timePeriod.StartHours, timePeriod.StartMinutes), "d/M/yyyy H:m:ss", CultureInfo.InvariantCulture);
                var endDate = DateTime.ParseExact(string.Format("{0}/{1}/{2} {3}:{4}:59", rptFlterCriteria.Start.Day, rptFlterCriteria.Start.Month, rptFlterCriteria.Start.Year, timePeriod.EndHours, timePeriod.EndMinutes), "d/M/yyyy H:m:ss", CultureInfo.InvariantCulture);

                List<Notam> notams = null;
                if (rptFlterCriteria.Conditions != null)
                {
                    var filterCriteria = new FilterCriteria
                    {
                        Conditions = rptFlterCriteria.Conditions,
                        QueryOptions = rptFlterCriteria.QueryOptions
                    };
                    var filterQuery = Uow.NotamRepo.PrepareQueryFilter(filterCriteria);
                    notams = await Uow.NotamRepo.ReportActiveFilterCsvAsync(startDate, endDate, filterQuery);
                }
                else
                {
                    notams = await Uow.NotamRepo.ReportActiveCsvAsync(startDate, endDate);
                }

                var data = GenerateNotamReportDto(notams, endDate, false, false);

                return Ok(GetCsvString(data, false));

            }, "api/nof/reportactivenotams/csv");
        }

        [Route("reportstats")]
        [HttpPost]
        public async Task<IHttpActionResult> GetReportNofStats([FromBody]NofStatsViewModel model)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (model.AggregatePeriod == AggregatePeriod.Yearly || model.AggregatePeriod == AggregatePeriod.Monthly)
                {
                    var end = model.End.AddDays(-1);
                    model.Start = new DateTime(model.Start.Year, model.Start.Month, model.Start.Day);
                    model.End = new DateTime(end.Year, end.Month, end.Day);
                }
                else
                {
                    model.End = new DateTime(model.End.Year, model.End.Month, model.End.Day, 23, 59, 59);
                }

                var stats = await Uow.NotamRepo.StatsAsync(model.Start, model.End, model.AggregatePeriod);

                return Ok(stats);

            }, "api/nof/reportnotmas");
        }

        [Route("notams/trackinglist/{proposalId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNofDisseminatedNotam(int proposalId)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                {
                    return Unauthorized();
                }
                else
                {
                    return Ok(await Uow.NotamRepo.GetTrackingListByProposalIdAsync(proposalId));
                }
            }, $"api/nof/notams/trackinglist/{proposalId}");
        }


        [Route("notams/canbereplaced/{notamId}")]
        [HttpGet]
        public async Task<IHttpActionResult> CanBeReplaced(string notamId)
        {
            return await LogAction(async () =>
            {
                var count = await Uow.NotamRepo.GetNotamRefCountByNotamIdAsync(notamId.Replace("_", "/"));

                return Ok(count == 0);

            }, "api/notams/location,start");
        }

        [Route("notams/verifyobsolete/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> VerifyObsoleteNotam(string id)
        {
            return await LogAction(async () =>
            {
                var ntm = await Uow.NotamRepo.GetNoltamDtoByIdAsync(Guid.Parse(id));
                if (ntm != null)
                {
                    if (ntm.IsObsolete || ntm.IsReplaced) return Ok(true);
                    var prop = await Uow.ProposalRepo.GetByIdAsync(ntm.ProposalId);
                    if (prop == null) return BadRequest(string.Format(Resources.InvalidNotamID, id));
                    if (prop.ProposalType == NotamType.C ||
                        prop.Status == NotamProposalStatusCode.Rejected ||
                        prop.Status == NotamProposalStatusCode.Cancelled ||
                        prop.Status == NotamProposalStatusCode.ModifiedCancelled ||
                        prop.Status == NotamProposalStatusCode.Parked ||
                        prop.Status == NotamProposalStatusCode.ParkedDraft ||
                        prop.Status == NotamProposalStatusCode.ParkedPicked ||
                        prop.Status == NotamProposalStatusCode.Pending ||
                        prop.Status == NotamProposalStatusCode.Picked ||
                        prop.Status == NotamProposalStatusCode.Submitted ||
                        prop.Status == NotamProposalStatusCode.Withdrawn) return Ok(true);
                    return Ok(false);
                }
                else return BadRequest(string.Format(Resources.InvalidNotamID, id));

            }, "api/notams/verifyobsolete/{notamId}");
        }

        [Route("notams/filter", Name = "FilterNotams")]
        [HttpPost]
        public async Task<IHttpActionResult> FilterNotams([FromBody]FilterCriteria filterCriteria)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var filterQuery = Uow.NotamRepo.PrepareQueryFilter(filterCriteria);
                var totalCount = await Uow.NotamRepo.FilterCountAsync(filterCriteria, filterQuery);
                var notams = await Uow.NotamRepo.FilterAsync(filterCriteria, filterQuery);

                if (notams.Count > 0)
                {
                    for (var i = 0; i < notams.Count; i++)
                    {
                        if (notams[i].EffectArea != null)
                        {
                            notams[i].EffectAreaGeoJson = ConvertToGeoJson(notams[i].EffectArea);
                            notams[i].EffectArea = null;
                        }
                    }
                }

                return Ok(ResponsewithPagingEnvelope(filterCriteria.QueryOptions, "FilterNotams", notams, totalCount));

            }, "api/nof/notams/filter");
        }

        [Route("notams/reportfilter", Name = "ReportFilterNotams")]
        [HttpPost]
        public async Task<IHttpActionResult> FilterReportNotams([FromBody]ReportFilterCriteria rptFlterCriteria)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if(rptFlterCriteria.StartTime != null || rptFlterCriteria.EndTime != null)
                {
                    rptFlterCriteria.filterBy = "InForce";
                }

                rptFlterCriteria.Start = new DateTime(rptFlterCriteria.Start.Year, rptFlterCriteria.Start.Month, rptFlterCriteria.Start.Day);
                rptFlterCriteria.End = new DateTime(rptFlterCriteria.End.Year, rptFlterCriteria.End.Month, rptFlterCriteria.End.Day, 23, 59, 59);
                
                var filterCriteria = new FilterCriteria
                {
                    Conditions = rptFlterCriteria.Conditions,
                    QueryOptions = rptFlterCriteria.QueryOptions
                };

                var filterQuery = Uow.NotamRepo.PrepareQueryFilter(filterCriteria);
                var totalCount = await Uow.NotamRepo.ReportFilterCountAsync(rptFlterCriteria, filterQuery);
                var notams = await Uow.NotamRepo.ReportFilterAsync(rptFlterCriteria, filterQuery);

                var response = GenerateNotamReportDto(notams, rptFlterCriteria.End);

                return Ok(ResponsewithPagingEnvelope(filterCriteria.QueryOptions, "ReportFilterNotams", response, totalCount));

            }, "api/nof/notams/filter");
        }

        [Route("notams/reportactivefilter", Name = "ReportActiveFilterNotams")]
        [HttpPost]
        public async Task<IHttpActionResult> FilterActiveReportNotams([FromBody]ReportFilterCriteria rptFlterCriteria)
        {
            return await LogReadUncommitedTransaction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var timePeriod = ParseTimePeriod(rptFlterCriteria.StartTime, rptFlterCriteria.EndTime);

                rptFlterCriteria.Start = DateTime.ParseExact(string.Format("{0}/{1}/{2} {3}:{4}:00", rptFlterCriteria.Start.Day, rptFlterCriteria.Start.Month, rptFlterCriteria.Start.Year, timePeriod.StartHours, timePeriod.StartMinutes), "d/M/yyyy H:m:ss", CultureInfo.InvariantCulture);
                rptFlterCriteria.End = DateTime.ParseExact(string.Format("{0}/{1}/{2} {3}:{4}:59", rptFlterCriteria.Start.Day, rptFlterCriteria.Start.Month, rptFlterCriteria.Start.Year, timePeriod.EndHours, timePeriod.EndMinutes), "d/M/yyyy H:m:ss", CultureInfo.InvariantCulture);

                var filterCriteria = new FilterCriteria
                {
                    Conditions = rptFlterCriteria.Conditions,
                    QueryOptions = rptFlterCriteria.QueryOptions
                };

                var filterQuery = Uow.NotamRepo.PrepareQueryFilter(filterCriteria);
                var totalCount = await Uow.NotamRepo.ReportActiveFilterCountAsync(rptFlterCriteria, filterQuery);
                var notams = await Uow.NotamRepo.ReportActiveFilterAsync(rptFlterCriteria, filterQuery);
                var response = GenerateNotamReportDto(notams, rptFlterCriteria.End);

                return Ok(ResponsewithPagingEnvelope(filterCriteria.QueryOptions, "ReportActiveFilterNotams", response, totalCount));

            }, "api/nof/notams/reportactivefilter");
        }

        [Route("notams/icaoformat/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> ICaoFormat(Guid id)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var notam = await Uow.NotamRepo.GetByIdAsync(id);
                if (notam == null)
                    return NotFound();

                return Ok(ICAOFormatUtils.Generate(notam, true));

            }, $"api/nof/notams/icaoformat/{id}");
        }

        [Route("queryfilter")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveUserQueryFilter(UserQueryFilterViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (model.Username != System.Web.HttpContext.Current.User.Identity.Name)
                    return Unauthorized();

                if (!ModelState.IsValid)
                    return BadRequest("Invalid Request");

                var userFilter = await Uow.UserQueryFilterRepo.GetNotamFiltersBySlotAndUsernameAsync(model.SlotNumber, model.Username, model.UserFilterType);
                if (userFilter != null)
                {
                    userFilter.JsonBundle = JsonConvert.SerializeObject(model.FilterConditions);
                    Uow.UserQueryFilterRepo.Update(userFilter);
                }
                else
                {
                    Uow.UserQueryFilterRepo.Add(new UserQueryFilter
                    {
                        FilterName = model.FilterName,
                        SlotNumber = model.SlotNumber,
                        Username = model.Username,
                        UserFilterType = model.UserFilterType,
                        JsonBundle = JsonConvert.SerializeObject(model.FilterConditions)
                    });
                }
                await Uow.SaveChangesAsync();

                return Ok();

            }, "api/nof/queryfilter");
        }

        [Route("queryfiltername")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserQueryFilterNames(string username)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (username != System.Web.HttpContext.Current.User.Identity.Name)
                    return Unauthorized();

                var userFilterNames = await Uow.UserQueryFilterNameRepo.GetByUsernameAsync(username);

                return Ok(userFilterNames);

            }, "api/nof/queryfiltername");
        }

        [Route("queryfiltername")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveUserQueryFilterNames(UserQueryFilterNameViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (model.Username != System.Web.HttpContext.Current.User.Identity.Name)
                    return Unauthorized();

                if (!ModelState.IsValid)
                    return BadRequest("Invalid Request");

                var filter = await Uow.UserQueryFilterNameRepo.GetByUsernameAndSlotNumberAsync(model.Username, model.SlotNumber);

                if( filter == null )
                {
                    filter = new UserQueryFilterName
                    {
                        SlotNumber = model.SlotNumber,
                        Username = model.Username,
                        FilterName = model.FilterName
                    };
                    Uow.UserQueryFilterNameRepo.Add(filter);
                    
                    await Uow.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    filter.FilterName = model.FilterName;
                    Uow.UserQueryFilterNameRepo.Update(filter);
                    
                    await Uow.SaveChangesAsync();
                    return Ok();
                }
            }, "api/nof/queryfiltername");
        }


        [Route("queryfilter/{slotNumber}/{filterType}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteUserQueryFilter(int slotNumber, UserFilterType filterType)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var userFilter = await Uow.UserQueryFilterRepo.GetNotamFiltersBySlotAndUsernameAsync(slotNumber, System.Web.HttpContext.Current.User.Identity.Name, filterType);
                if (userFilter == null)
                {
                    return NotFound();
                }

                Uow.UserQueryFilterRepo.Delete(userFilter);

                var userFilterName = await Uow.UserQueryFilterNameRepo.GetByUsernameAndSlotNumberAsync(System.Web.HttpContext.Current.User.Identity.Name, slotNumber);
                if(userFilterName != null)
                {
                    Uow.UserQueryFilterNameRepo.Delete(userFilterName);
                }

                await Uow.SaveChangesAsync();

                return Ok();

            }, $"api/nof/queryfilter/{slotNumber}");
        }

        [Route("queryfilter/active/{username}/{filterType}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetActiveUserQueryFilters(string username, UserFilterType filterType)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (username != ApplicationContext.GetUserName())
                    return Unauthorized();

                var userFilters = await Uow.UserQueryFilterRepo.GetAllNotamFiltersByUsernameAsync(username, filterType);

                var response = new List<int>();
                foreach (var userFilter in userFilters)
                    response.Add(userFilter.SlotNumber);

                return Ok(response);

            }, $"api/nof/queryfilter/active/{username}");
        }

        [Route("queryfilter/{slotNumber}/{username}/{filterType}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserQueryFilter(int slotNumber, string username, UserFilterType filterType)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (username != ApplicationContext.GetUserName())
                    return Unauthorized();

                var userFilter = await Uow.UserQueryFilterRepo.GetNotamFiltersBySlotAndUsernameAsync(slotNumber, username, filterType);
                if (userFilter != null)
                {
                    var response = JsonConvert.DeserializeObject<FilterCondition[]>(userFilter.JsonBundle, new JsonSerializerSettings
                    {
                        ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
                    });
                    return Ok(response);
                }

                return NotFound();

            }, $"api/nof/queryfilter/{slotNumber}/{username}");
        }

        private List<NotamReportDto> GenerateNotamReportDto(List<Notam> notams, DateTime endDate, bool includeEffectiveArea = true, bool setObsolete = true)
        {
            var dtos = new List<NotamReportDto>();
            var culture = new CultureInfo("en-US");
            foreach (var notam in notams)
            {
                dtos.Add(new NotamReportDto
                {
                    Id = notam.Id,
                    RootId = notam.RootId,
                    NotamId = notam.NotamId,
                    ReferredNotamId = notam.ReferredNotamId,
                    ProposalId = notam.ProposalId,
                    Originator = notam.Originator,
                    OriginatorEmail = notam.OriginatorEmail,
                    OriginatorPhone = notam.OriginatorPhone,
                    ItemA = notam.ItemA,
                    StartActivity = (notam.StartActivity.HasValue) ? notam.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (notam.EndValidity.HasValue) ? notam.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((notam.Type == NotamType.C) ? "" : "PERM"),
                    Published = notam.Published,
                    Received = notam.Received,
                    NotamType = notam.Type,
                    Series = notam.Series,
                    Scope = notam.Scope,
                    Code23 = notam.Code23,
                    Code45 = notam.Code45,
                    Traffic = notam.Traffic,
                    SiteId = notam.ItemA == "CXXX" ? notam.ItemE.Substring(0, 4) : "",
                    Purpose = notam.Purpose,
                    IsReplaced = notam.ReplaceNotamId.HasValue,
                    IsEstimated = notam.Estimated,
                    IsPermanent = notam.Permanent,
                    ItemF = notam.ItemF,
                    ItemG = notam.ItemG,
                    Coordinates = notam.Coordinates,
                    Radius = notam.Radius,
                    LowerLimit = notam.LowerLimit,
                    UpperLimit = notam.UpperLimit,
                    ModifiedByOrg = notam.ModifiedByOrg,
                    ModifiedByUsr = notam.ModifiedByUsr,
                    Operator = notam.Operator,
                    EffectArea = notam.EffectArea,
                    Urgent = notam.Urgent,
                    SeriesChecklist = notam.SeriesChecklist,
                    EffectAreaGeoJson = includeEffectiveArea ? ConvertToGeoJson(notam.EffectArea) : null,
                    ItemE = notam.ItemE,
                    NoteToNof = notam.NoteToNof,
                    ItemEFrench = notam.ItemEFrench,
                    IcaoFormat = notam.IcaoText,
                    isObsolete = setObsolete ? Notam.IsObsoleteNotam(notam, endDate) : false
            });

            }
            return dtos;
        }

        private TimePeriod ParseTimePeriod(string startTime, string endTime)
        {
            var starttime = string.IsNullOrEmpty(startTime) ? "00:00" : startTime;
            var endtime = string.IsNullOrEmpty(endTime) ? "00:00" : endTime;
            
            var startTimeParts = starttime.Split(':');
            var endTimeParts = endtime.Split(':');

            var timePeriod = new TimePeriod
            {
                StartHours = 0,
                StartMinutes = 0,
                EndHours = 23,
                EndMinutes = 59
            };

            if (startTimeParts.Length == 2)
            {
                int.TryParse(startTimeParts[0], out timePeriod.StartHours);
                int.TryParse(startTimeParts[1], out timePeriod.StartMinutes);
            }
            if (endTimeParts.Length == 2)
            {
                int.TryParse(endTimeParts[0], out timePeriod.EndHours);
                int.TryParse(endTimeParts[1], out timePeriod.EndMinutes);
            }

            timePeriod.SanitizeData();

            return timePeriod;
        }

        private string GetCsvString(IList<NotamReportDto> notams, bool includeEnforce = true)
        {
            const int MaxCsvColumnLenght = 18000;

            StringBuilder csv = new StringBuilder();

            if (includeEnforce)
            {
                csv.AppendLine(@"""NOTAM #"",""TYPE"",""REF #"",""SER"",""ITEM A"",""SITE ID"",""PRM"",""EST"",""ORIGINATOR"",""APPROVER"",""START"",""END"",""RECEIVED"",""PUBLISHED"",""Q23"",""Q45"",""SCP"",""TRF"",""PURP"",""RADIUS"",""LOWER"",""UPPER"",""ITEM E"",""ITEM E (FR)"",""ICAO FORMAT"",""NOTES TO NOF"",""INFORCE""");
            }
            else
            {
                csv.AppendLine(@"""NOTAM #"",""TYPE"",""REF #"",""SER"",""ITEM A"",""SITE ID"",""PRM"",""EST"",""ORIGINATOR"",""APPROVER"",""START"",""END"",""RECEIVED"",""PUBLISHED"",""Q23"",""Q45"",""SCP"",""TRF"",""PURP"",""RADIUS"",""LOWER"",""UPPER"",""ITEM E"",""ITEM E (FR)"",""ICAO FORMAT"",""NOTES TO NOF""");
            }

            foreach (NotamReportDto notam in notams)
            {
                if (notam.ItemE != null)
                {
                    if (notam.ItemE.Length > MaxCsvColumnLenght)
                    {
                        notam.ItemE = notam.ItemE.Substring(0, MaxCsvColumnLenght);
                    }
                    notam.ItemE = notam.ItemE.Replace(Environment.NewLine, "\n");
                }
                else
                {
                    notam.ItemE = "";
                }

                if (notam.ItemEFrench != null)
                {
                    if (notam.ItemEFrench.Length > MaxCsvColumnLenght)
                    {
                        notam.ItemEFrench = notam.ItemEFrench.Substring(0, MaxCsvColumnLenght);
                    }
                    notam.ItemEFrench = notam.ItemEFrench.Replace(Environment.NewLine, "\n");
                }
                else
                {                        
                    notam.ItemEFrench = "";
                }

                if (notam.IcaoFormat != null)
                {
                    if (notam.IcaoFormat.Length > MaxCsvColumnLenght)
                    {
                        notam.IcaoFormat = notam.IcaoFormat.Substring(0, MaxCsvColumnLenght);
                    }
                    notam.IcaoFormat = notam.IcaoFormat.Replace(Environment.NewLine, "\n");
                }
                else
                {
                    notam.IcaoFormat = "";
                }

                if (notam.NoteToNof != null)
                {
                    if (notam.NoteToNof.Length > MaxCsvColumnLenght)
                    {
                        notam.NoteToNof = notam.NoteToNof.Substring(0, MaxCsvColumnLenght);
                    }
                    notam.NoteToNof = notam.NoteToNof.Replace(Environment.NewLine, "\n");
                }
                else
                {
                    notam.NoteToNof = "";
                }

                csv.Append(String.Format(@"""{0}"",", notam.NotamId));
                csv.Append(String.Format(@"""{0}"",", notam.NotamType.ToString()));
                csv.Append(String.Format(@"""{0}"",", notam.ReferredNotamId));
                csv.Append(String.Format(@"""{0}"",", notam.Series));
                csv.Append(String.Format(@"""{0}"",", notam.ItemA));
                csv.Append(String.Format(@"""{0}"",", notam.SiteId));
                csv.Append(String.Format(@"""{0}"",", notam.IsPermanent ? "Y" : "N"));
                csv.Append(String.Format(@"""{0}"",", notam.IsEstimated ? "Y" : "N"));
                csv.Append(String.Format(@"""{0}"",", notam.Originator.Replace('"', '\'')));
                csv.Append(String.Format(@"""{0}"",", notam.ModifiedByUsr.Replace('"', '\'')));
                csv.Append(String.Format(@"""{0}"",", notam.StartActivity));
                csv.Append(String.Format(@"""{0}"",", notam.EndValidity));
                csv.Append(String.Format(@"""{0}"",", notam.Received.ToString()));
                csv.Append(String.Format(@"""{0}"",", notam.Published.ToString()));
                csv.Append(String.Format(@"""{0}"",", notam.Code23));
                csv.Append(String.Format(@"""{0}"",", notam.Code45));
                csv.Append(String.Format(@"""{0}"",", notam.Scope));
                csv.Append(String.Format(@"""{0}"",", notam.Traffic));
                csv.Append(String.Format(@"""{0}"",", notam.Purpose));
                csv.Append(String.Format(@"""{0}"",", notam.Radius));
                csv.Append(String.Format(@"""{0}"",", notam.LowerLimit));
                csv.Append(String.Format(@"""{0}"",", notam.UpperLimit));
                csv.Append(String.Format(@"""{0}"",", notam.ItemE.Replace('"', '\'')));
                csv.Append(String.Format(@"""{0}"",", notam.ItemEFrench.Replace('"', '\'')));
                csv.Append(String.Format(@"""{0}"",", notam.IcaoFormat.Replace('"', '\'')));
                csv.Append(String.Format(@"""{0}"",", notam.NoteToNof.Replace('"', '\'')));
                if (includeEnforce)
                {
                    csv.Append(String.Format(@"""{0}""", notam.isObsolete ? "N" : "Y"));
                }
                csv.AppendLine();
            }

            return csv.ToString();
        }


        private IEventHubContext _eventHubContext;
    }
}
