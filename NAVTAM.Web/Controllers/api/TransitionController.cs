﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.TaskEngine.Mto;
using NAVTAM.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/transition")]
    [Authorize]
    public class TransitionController : ApiBaseController
    {
        readonly INotamQueuePublisher _queuePublisher;

        public TransitionController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, INotamQueuePublisher queuePublisher, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _queuePublisher = queuePublisher;
        }

        [HttpPost]
        [Route("queuejob")]
        public async Task<IHttpActionResult> QueueJob([FromBody] SubsClientIdsViewModel ids) => await _QueueJob(ids.subsClientIds);

        private async Task<IHttpActionResult> _QueueJob(string clientIds)
        {
            return await LogAction(async () =>
            {
                var jobs = await Uow.DisseminationJobRepo.GetAllAsync();
                var inProgresssCount = jobs.Count(j => j.Status == DisseminationJobStatus.Pending || j.Status == DisseminationJobStatus.InProcess);
                if (inProgresssCount > 0)
                    return BadRequest(Resources.MassDisseminationError);

                Uow.BeginTransaction();
                try
                {
                    var ids = clientIds.Split('|');
                    var selectedAftnIds = new List<string>();
                    var selectedSwimIds = new List<string>();
                    if (ids.Length > 0)
                    {
                        foreach (var id in ids)
                        {
                            int result = -1;
                            if (!int.TryParse(id, out result))
                                continue;

                            var client = await Uow.NdsClientRepo.GetByIdAsync(result);
                            if (client == null)
                                continue;

                            if (client.ClientType == NdsClientType.Aftn)
                            {
                                selectedAftnIds.Add(id);
                                client.LastSynced = DateTime.UtcNow;
                                Uow.NdsClientRepo.Update(client);
                            }
                            else if (client.ClientType == NdsClientType.Swim)
                            {
                                selectedSwimIds.Add(id);
                                //disseminateXml = true;
                                client.LastSynced = DateTime.UtcNow;
                                Uow.NdsClientRepo.Update(client);
                            }
                        }
                        await Uow.SaveChangesAsync();
                    }

                    var jobId = -1;

                    var configDelays = await Uow.ConfigurationValueRepo.GetByCategoryAsync("NDS");
                    if (selectedAftnIds.Count > 0)
                    {
                        var configAftn = configDelays.SingleOrDefault(e => e.Name == "AFTN_DisseminationDelay");
                        var delayInMilliSeconds = configAftn != null ? int.Parse(configAftn.Value) : 2000;
                        jobId = CreateJob(Uow, string.Join(" ", selectedAftnIds), false, delayInMilliSeconds);
                        PublishMessage(_queuePublisher, Uow, jobId);
                    }
                    if (selectedSwimIds.Count > 0)
                    {
                        var configSwim = configDelays.SingleOrDefault(e => e.Name == "SWIM_DisseminationDelay");
                        var delayInMilliSeconds = configSwim != null ? int.Parse(configSwim.Value) : 1000;
                        jobId = CreateJob(Uow, string.Join(" ", selectedSwimIds), true, delayInMilliSeconds);
                        PublishMessage(_queuePublisher, Uow, jobId);
                    }

                    Uow.Commit();

                    return Ok(jobId);
                }
                catch (Exception)
                {
                    Uow.Rollback();
                    throw;
                }
            }, $"api/transition/queuejob?clientids={clientIds}");
        }

        [HttpGet]
        [Route("listjobs")]
        public IHttpActionResult ListJobs()
        {
            return LogAction(() => Ok(Uow.DisseminationJobRepo.GetAll().ToList()), $"api/transition/listjobs");
        }

        [HttpPost]
        [Route("pausejob")]
        public IHttpActionResult PauseJob(int jobId)
        {
            return LogAction(() => ChangeJobStatus(jobId, DisseminationJobStatus.Paused), $"api/transition/pausejob?jobId={jobId}");
        }

        [HttpPost]
        [Route("resumejob")]
        public IHttpActionResult ResumeJob(int jobId)
        {
            return LogAction(() => ChangeJobStatus(jobId, DisseminationJobStatus.Pending), $"api/transition/resumejob?jobId={jobId}");
        }

        [HttpPost]
        [Route("canceljob")]
        public IHttpActionResult CancelJob(int jobId)
        {
            return LogAction(() => ChangeJobStatus(jobId, DisseminationJobStatus.Canceled), $"api/transition/canceljob?jobId={jobId}");
        }

        private IHttpActionResult ChangeJobStatus(int jobId, DisseminationJobStatus status)
        {
            try
            {
                var job = Uow.DisseminationJobRepo.GetById(jobId);
                if (job == null)
                    return BadRequest();

                job.Status = status;
                Uow.DisseminationJobRepo.Update(job);
                Uow.SaveChanges();

                return Ok(jobId);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        static int CreateJob(IUow uow, string addresses, bool disseminateXml, int delay)
        {
            var job = new DisseminationJob()
            {
                ClientIds = addresses,
                DisseminateXml = disseminateXml,
                LastNotam = 0,
                Status = DisseminationJobStatus.Pending,
                Delay = TimeSpan.FromMilliseconds(delay),
                BreakDate = GetBreakDate()
            };

            uow.DisseminationJobRepo.Add(job);
            uow.SaveChanges();

            return job.Id;
        }

        static DateTime? GetBreakDate()
        {
            var rawDate = ConfigurationManager.AppSettings["DisseminationJobBreakDate"] ?? "";
            DateTime breakDate;
            return DateTime.TryParse(rawDate, out breakDate) ? breakDate : new DateTime(DateTime.UtcNow.Year, 1, 1);
        }

        static void PublishMessage(INotamQueuePublisher publisher, IUow uow, int jobId)
        {
            var mto = new MessageTransferObject();

            // add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            // create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            var messageId = (int)MessageTransferObjectId.ProcessDisseminateJobMsg;

            // create inbound type
            mto.Add(MessageDefs.MessageIdKey, messageId, DataType.Int32);

            // add job id
            mto.Add(MessageDefs.EntityIdentifierKey, jobId, DataType.Int32);

            // date/time when the message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            publisher.Publish(uow, mto);
        }
    }

    public class SubsClientIdsViewModel
    {
        public string subsClientIds { get; set; }
    }

}