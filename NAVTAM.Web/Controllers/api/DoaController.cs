﻿using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    //[Authorize(Roles = "Administrator, CCS, NOF_ADMINISTRATOR")]
    //[Authorize(Roles = "Administrator")]
    [RoutePrefix("api/doas/admin")]
    public class DoaController : GeographyBaseController
    {
        public DoaController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("partials")]
        //This is a fix? for IE that cached the get request always. Everytime that a request is sent, 
        //we need to sent something different, like the parameter timeRequest that is not used on the Controller
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> GetAllPartial(string timeRequest)
        {
            return await LogAction(async () =>
            {
                var doas = await Uow.DoaRepo.GetAllAsync();
                
                // sort doas by name
                doas.Sort((d1, d2) => string.Compare(d1.Name, d2.Name, true));
                
                // make partial doas
                var response = doas.Where(d => d.DoaType == RegionType.Doa).Select(d => new DoaPartialDto { Id = d.Id, Name = d.Name });
                return Ok(response);

            }, "api/doas/admin/partials");
        }

        [HttpPost, Route("page", Name = "RegisteredDoasPage")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> GetDoasInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.DoaRepo.GetAllCountAsync(queryOptions);
                var doas = await Uow.OrganizationDoaRepo.GetDoaPartials(queryOptions, "; ");

                if (doas.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    doas = await Uow.OrganizationDoaRepo.GetDoaPartials(queryOptions, "; ");
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredDoasPage", doas, totalCount));

            }, "api/doas/admin/page");
        }

        [HttpGet]
        [Route("firs")]
        [Authorize(Roles = "Administrator, CCS")]
        public IHttpActionResult GetFirs()
        {
            return LogAction(() => Ok(Uow.SdoCacheRepo.GetFirs().Select(f => new { f.Id, f.Name, f.Designator })), "api/doas/admin/firs");
        }

        [HttpGet]
        [Route("validatepoint")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> ValidatePoint(string value)
        {
            return await LogAction(async () =>
            {
                var convertResult = await ValidateAndConvertToPoint(value);

                if (convertResult.Succeeded)
                    return Ok(convertResult.Result);

                return BadRequest();

            }, "api/doas/admin/validatepoint");
        }

        [HttpPost]
        [Route("validatedoaincanada")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> ValidateDoaInCanada([FromBody] DoaViewModel model)
        {
            return await LogAction(async () =>
            {
                var result = await IsDoaInCanada(model, Uow);
                return Ok(result);
            }, "api/doas/admin/validatedoaincanada");
        }

        [HttpGet]
        [Route("validatepoints")]
        [Authorize(Roles = "Administrator, CCS")]
        public IHttpActionResult ValidatePoints(string value)
        {
            return LogAction(() =>
            {
                var convertResult = ValidateAndConvertToPolygon(value);
                if (convertResult.Succeeded)
                    return Ok(convertResult.Result);

                return BadRequest();

            }, "api/doas/admin/validatepoints");
        }

        [HttpDelete]
        [Route("delete")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> Delete(int doaId)
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetByIdAsync(doaId);
                if (doa == null)
                    return NotFound();

                if(doa.DoaType == RegionType.Bilingual)
                    return BadRequest(Resources.ErrorDeleteBilingualRegion);

                if (doa.DoaType == RegionType.Northern)
                    return BadRequest(Resources.ErrorDeleteNorthernRegion);

                var orgCount = await Uow.OrganizationDoaRepo.CountOrganizationsAsync(doaId);
                if (orgCount > 0)
                    return BadRequest(Resources.DoaLinkedToOrg);

                Uow.DoaRepo.Delete(doa);
                Uow.SaveChanges();

                Logger.LogInfo(GetType(), $"Doa '{doa.Name}' removed  by '{ApplicationContext.GetUserName()}'.");

                return Ok();

            }, "api/doas/admin/delete");
        }

        [HttpGet]
        [Route("find")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> Find(int doaId)
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetByIdAsync(doaId);
                if (doa == null)
                    return BadRequest();

                var featureGeo = ConvertToGeoJson(doa.DoaGeoArea);

                var doaViewModel = new DoaViewModel
                {
                    Id = doa.Id,
                    Name = doa.Name,
                    Description = doa.Description,
                    Region = new RegionViewModel
                    {
                        Source = RegionSource.Geography,
                        Geography = doa.DoaGeoArea.ToString(),
                        GeoFeatures = featureGeo
                    }
                };

                return Ok(doaViewModel);

            }, "api/doas/admin/find");
        }

        [HttpGet]
        [Route("bilingual")]
        [Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> FindBilingualRegion()
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetBilingualRegionAsync();
                if (doa == null)
                    return BadRequest();

                var featureGeo = ConvertToGeoJson(doa.DoaGeoArea);

                var doaViewModel = new DoaViewModel
                {
                    Id = doa.Id,
                    Name = doa.Name,
                    Description = doa.Description,
                    Region = new RegionViewModel
                    {
                        Source = RegionSource.Geography,
                        Geography = doa.DoaGeoArea.ToString(),
                        GeoFeatures = featureGeo
                    }
                };

                return Ok(doaViewModel);

            }, "api/doas/admin/bilingual");

        }

        [HttpGet]
        [Route("northern")]
        [Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> FindNorthernRegion()
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetNorthernRegionAsync();
                if (doa == null)
                    return BadRequest();

                var featureGeo = ConvertToGeoJson(doa.DoaGeoArea);

                var doaViewModel = new DoaViewModel
                {
                    Id = doa.Id,
                    Name = doa.Name,
                    Description = doa.Description,
                    Region = new RegionViewModel
                    {
                        Source = RegionSource.Geography,
                        Geography = doa.DoaGeoArea.ToString(),
                        GeoFeatures = featureGeo
                    }
                };

                return Ok(doaViewModel);

            }, "api/doas/admin/northern");

        }


        [HttpGet]
        [Route("copy")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> Copy(int doaId)
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetByIdAsync(doaId);
                if (doa == null)
                    return BadRequest();

                var featureGeo = ConvertToGeoJson(doa.DoaGeoArea); // featureCol.Serialize(prettyPrint: false);

                var doaViewModel = new DoaViewModel
                {
                    Id = doa.Id,
                    Name = await GetUniqueDoaNameFrom(doa.Name, Uow),
                    Description = doa.Description,
                    Region = new RegionViewModel
                    {
                        Source = RegionSource.Geography,
                        Geography = doa.DoaGeoArea.ToString(),
                        GeoFeatures = featureGeo
                    }
                };

                return Ok(doaViewModel);

            }, "api/doas/admin/copy");
        }

        [HttpGet]
        [Route("exists")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> Exists(int doaId, string name)
        {
            return await LogAction(async () => Ok(await DoaNameExists(doaId, name)), "api/doas/admin/exists");
        }

        [HttpPost]
        [Route("uploadfile")]
        [Authorize(Roles = "Administrator, CCS")]
        public IHttpActionResult UploadDoaGeographyFile()
        {
            return LogAction(() =>
            {
                var validateResult = ValidateUploadedGeographyFile();
                if (validateResult.Succeeded)
                    return Ok(validateResult.Result);

                return BadRequest(validateResult.ErrorMessage);

            }, "api/doas/admin/uploadfile");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, CCS")]
        [Route("save")]
        public async Task<IHttpActionResult> SaveDoa([FromBody] DoaViewModel model)
        {
            return await LogAction(async () =>
            {
                // validate model
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                // validate the name is not empty
                if (string.IsNullOrWhiteSpace(model.Name))
                    return BadRequest(Resources.InvalidDoaName);

                // validate name doesn't exists
                if (await DoaNameExists(model.Id, model.Name))
                    return BadRequest(String.Format(Resources.DoaNameExists, model.Name));

                // try to create the geography
                var geoResult = CreateRegionGeography(model.Region, Uow);
                if (geoResult.Failed)
                    return BadRequest(geoResult.ErrorMessage);

                var doaGeoArea = ModifyDoaGeography(geoResult.Result, model.Changes);

                //Verify if the DOA is inside Canada 
                var nofDoa = await Uow.DoaRepo.GetNofDoaAsync();
                if (nofDoa != null && !nofDoa.DoaGeoArea.Intersects(doaGeoArea))
                    return BadRequest(Resources.RegionOutOfCanada);

                // Verify the DOA is counter clockwise
                var result = IsAreaLargerThanCanada(doaGeoArea);
                if(result)
                    return BadRequest(Resources.ErrorGeoFileClockwise);

                // try to add/update the DOA to the repository
                var doa = new Doa()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description,
                    DoaGeoArea = doaGeoArea,
                    DoaType = model.DoaType,
                };

                if (model.Id == 0)
                {
                    Uow.DoaRepo.Add(doa);
                }
                else
                {
                    Uow.DoaRepo.Update(doa);
                }

                await Uow.SaveChangesAsync();

                if (model.Id == 0)
                {
                    Logger.LogInfo(GetType(), $"DOA '{model.Name}' created  by '{ApplicationContext.GetUserName()}'.");
                }
                else
                {
                    Logger.LogInfo(GetType(), $"DOA '{model.Name}' updated  by '{ApplicationContext.GetUserName()}'.");
                }

                return Ok(doa.Id);

            }, "api/doas/admin/save");
        }

        [HttpPost]
        [Route("GetGeoFeatures")]
        [Authorize(Roles = "Administrator, CCS")]
        public IHttpActionResult GetGeoFeatures([FromBody] RegionMapRenderViewModel model)
        {
            return LogAction(() => ConvertToGeoFeatures(model), "api/doas/admin/GetGeoFeatures");
        }

        [HttpGet]
        [Route("PointInDoa")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> GetPointInDoa(int doaId, string value)
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetByIdAsync(doaId);
                var doaGeo = doa?.DoaGeoArea;

                if (doa == null || doaGeo == null)
                    return BadRequest();

                var locationResult = await TryGetAerodromeLocation(value, Uow);
                if (locationResult.Succeeded)
                    return Ok(GetDoaPointModel(doaGeo, value, locationResult.Result));

                if (DMSLocation.IsKnownFormat(value))
                    return Ok(GetDoaPointModel(doaGeo, value, value));

                return BadRequest();

            }, "api/doas/admin/PointInDoa");
        }

        [HttpPost]
        [Route("PointInRegion")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> GetPointInRegion([FromBody] LocationInRegionViewModel model)
        {
            return await LogAction(async () =>
            {
                // validate model
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                // try to create the geography
                var geoResult = CreateRegionGeography(model.Region, Uow);
                if (geoResult.Failed)
                    return BadRequest(geoResult.ErrorMessage);

                var doaGeo = geoResult.Result;

                var locationResult = await TryGetAerodromeLocation(model.Location, Uow);
                if (locationResult.Succeeded)
                    return Ok(GetDoaPointModel(doaGeo, model.Location, locationResult.Result));

                if (DMSLocation.IsKnownFormat(model.Location))
                    return Ok(GetDoaPointModel(doaGeo, model.Location, model.Location));

                return BadRequest();

            }, "api/doas/admin/PointInRegion");
        }

        [HttpGet]
        [Route("FindAhpsInDoa")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> FindAhpInDoa(int doaId, string match)
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetByIdAsync(doaId);
                if (doa == null)
                    return BadRequest();

                var aerodromes = await Uow.SdoCacheRepo.FilterAerodromesInArea(match, 100, doa.DoaGeoArea);

                return Ok(aerodromes.Select(a => CreateAerodromeDto(a)));

            }, "api/doas/admin/FindAhpsInDoa");
        }

        [HttpGet]
        [Route("FindAhpsOutsideDoa")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> FindAhpOutsideDoa(int doaId, string match)
        {
            return await LogAction(async () =>
            {
                var doa = await Uow.DoaRepo.GetByIdAsync(doaId);
                if (doa == null)
                    return BadRequest();

                var aerodromes = await Uow.SdoCacheRepo.FilterAerodromesOutsideArea(match, 100, doa.DoaGeoArea);

                return Ok(aerodromes.Select(a => CreateAerodromeDto(a)));

            }, "api/doas/admin/FindAhpsOutsideDoa");
        }

        private Task<bool> DoaNameExists(int doaId, string name)
        {
            var trimmed = (name ?? "").Trim();
            return doaId != 0 ? Uow.DoaRepo.Exists(doaId, trimmed) : Uow.DoaRepo.Exists(trimmed);
        }

        static string SerializeAerodromeLocation(DbGeography point)
        {
            return $"{point.Latitude ?? 0} {point.Longitude ?? 0}";
        }

        static DbGeography GetPointAndRadiusGeography(string location, int radius)
        {
            if (!DMSLocation.IsKnownFormat(location))
                throw new Exception(String.Format(Resources.DoaGeoInvalidPoint, location));

            var geoPoint = GetGeoPointFromLocation(DMSLocation.ToDecimalLatitudeLongitude(location));
            var nmRadius = GeoDataService.NMToMeters(radius);

            return geoPoint.Buffer(nmRadius);
        }

        static DbGeography GetGeoPointFromLocation(string location)
        {
            return DbGeography.PointFromText($"POINT({DMSLocation.FlipCoordinates(location)})", DbGeography.DefaultCoordinateSystemId);
        }

        static DbGeography GetGeoPointFromDmsLocation(DMSLocation location)
        {
            return DbGeography.PointFromText($"POINT({location.LongitudeDecimal} {location.LatitudeDecimal})", DbGeography.DefaultCoordinateSystemId);
        }

        static object CreateAerodromeDto(SdoSubjectDto dto)
        {
            return new { dto.Id, dto.Designator, dto.Name, Location = SerializeAerodromeLocation(dto.SubjectGeoRefPoint) };
        }

        static DoaPointModel GetDoaPointModel(DbGeography doa, string source, string location)
        {
            var point = GetGeoPointFromDmsLocation(DMSLocation.FromKnownFormat(location));
            var inDoa = point.Intersects(doa);
            return new DoaPointModel()
            {
                Source = source,
                InDoa = inDoa, //doa.Intersects(point.Buffer(10)),
                Location = $"{point.Latitude ?? 0} {point.Longitude ?? 0}"
            };
        }

        static async Task<string> GetUniqueDoaNameFrom(string name, IUow uow)
        {
            var uniqueName = $"{name} (copy)";
            var i = 1;
            while (await uow.DoaRepo.Exists(uniqueName))
            {
                uniqueName = $"{name} (copy {++i})";
            }
            return uniqueName;
        }

        static async Task<bool> IsDoaInCanada(DoaViewModel model, IUow uow)
        {
            var retValue = false;
            var nofDoa = await uow.DoaRepo.GetNofDoaAsync();
            if (nofDoa != null)
            {
                // try to create the geography
                var geoResult = CreateRegionGeography(model.Region, uow);
                if (geoResult.Succeeded)
                {
                    var DoaGeoArea = ModifyDoaGeography(geoResult.Result, model.Changes);
                    retValue = nofDoa.DoaGeoArea.Intersects(DoaGeoArea);
                }
            }
            else {
                //There is a problem with the NOF DOA, we return true
                retValue = true;
            }
            return retValue;
        }

    }
}
