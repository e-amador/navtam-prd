﻿using NAVTAM.SignalrHub;
using NAVTAM.ViewModels;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/signalrnotification")]
    public class SignalRNotificationController : ApiController
    {
        public SignalRNotificationController(IEventHubContext eventHubContext)
        {
            _eventHubContext = eventHubContext;
        }

        [HttpGet]
        [Route("publish/{id}/{status}")]
        public IHttpActionResult Publish(int id, int status)
        {
            _eventHubContext.Context.Clients.Group(HubChannelContants.ProposalChannel).OnEvent(new ChannelEvent
            {
                ChannelName = HubChannelContants.ProposalChannel,
                Name = "statusChanged",
                Data = new { Id = id, Status = status }
            });
            return Ok();
        }

        [HttpPost]
        [Route("statuschange")]
        public IHttpActionResult StatusChange([FromBody]SignalrViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //TODO: verify username and password...
            if (model.Username != "signalr_user" && model.Password != "_Password123!")
                return Unauthorized();

            _eventHubContext.Context.Clients.Group(HubChannelContants.ProposalChannel).OnEvent(new ChannelEvent
            {
                ChannelName = HubChannelContants.ProposalChannel,
                Name = "statusChanged",
                Data = new { Id = model.Id, Status = (int)model.Status}
            });

            return Ok();
        }

        private readonly IEventHubContext _eventHubContext;
    }
}