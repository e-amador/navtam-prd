﻿using Microsoft.AspNet.Identity;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NDS.Relay;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NAVTAM.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        public DashboardController(IUow uow, ILogger logger) : base(uow, logger)
        {
        }

        // GET: UserDashboard
        public async Task<ActionResult> Index()
        {
            return await LogAction(async () =>
            {
                ViewData["mapServerHost"] = ConfigurationManager.AppSettings["MapServerHost"];

                var userId = User.Identity.GetUserId();
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    var roles = await UserManager.GetRolesAsync(userId);

                    var dashboardAccess = new[] { CommonDefinitions.UserRole, CommonDefinitions.NofAdminRole, CommonDefinitions.NofUserRole, CommonDefinitions.FicUserRole, CommonDefinitions.OrgAdminRole };
                    if (dashboardAccess.Any(i => roles.Contains(i)))
                    {
                        if (await TrySetViewData(2, null))
                            return View();
                    }
                }

                return RedirectToAction("Login", "Account") as ActionResult;

            }, "Dashboard/Index");
        }

        public ActionResult Developer()
        {
            return View();
        }

        private async Task<bool> TrySetViewData(int catId, string sidebarName)
        {
            // cookie needs to match logged user (extra level of security)
            var cookie = HttpContext.Request.Cookies["usr_id"];
            if (cookie == null || cookie.Value != User.Identity.GetUserId())
                return false;

            var userId = cookie.Value;

            cookie = HttpContext.Request.Cookies["org_id"];
            if (cookie == null)
                return false;

            var orgId = cookie.Value;

            cookie = HttpContext.Request.Cookies["doa_id"];
            if (cookie == null)
                return false;

            var mvp = await GetMinValidityPeriod();

            var doaId = cookie.Value;

            ViewData["usr_id"] = userId;
            ViewData["org_id"] = orgId;
            ViewData["doa_id"] = doaId;
            ViewData["mvp"] = mvp;
            ViewData["category"] = catId;
            ViewData["canDoCatchAll"] = true;
            ViewData["nof_address"] = null;

            // prefill the address for the nof users
            if (IsNofUser(User))
            {
                var name = User.Identity.GetUserName();
                ViewData["nof_address"] = AftnConsts.NOFAddress;
            }

            return true;
        }

        const int DefaultMvp = 0;

        protected async Task<int> GetMinValidityPeriod()
        {
            var mvpValue = await Uow.ConfigurationValueRepo.GetValueAsync("NOTAM", "MinValidityPeriod") ?? "0";
            int mvp;
            return int.TryParse(mvpValue, out mvp) ? mvp : DefaultMvp;
        }

        private bool IsNofUser(IPrincipal user) => user.IsInRole(CommonDefinitions.NofUserRole) || user.IsInRole(CommonDefinitions.NofAdminRole);
    }
}