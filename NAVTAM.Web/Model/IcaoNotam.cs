﻿namespace NAVTAM.Model
{
    public class IcaoNotam
    {
        /// <summary>
        /// Notam identifier. E.g.: A0123/06
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Notam header. E.g.: A1234/06 NOTAMR A1212/06
        /// </summary>
        public string he { get; set; }
        /// <summary>
        /// Notam Q line. E.g.: CYOW/QMXLC/IV/NBO/A/000/999/5129N00028W005
        /// </summary>
        public string q { get; set; }
        /// <summary>
        /// E.g.: CYOW
        /// </summary>
        public string a { get; set; }
        /// <summary>
        /// E.g.: { type: 'point', coordinate: [-00.28,51.29] }
        /// </summary>
        public IcaNotamGeo ge { get; set; }
        /// <summary>
        /// Radius, e.g.: 5
        /// </summary>
        public int r { get; set; }
        /// <summary>
        /// B line, e.g.: 0609050500 
        /// </summary>
        public string b { get; set; }
        /// <summary>
        /// E.g.: C line, e.g.: 0704300500
        /// </summary>
        public string c { get; set; }
        /// <summary>
        /// D line
        /// </summary>
        public string d { get; set; }
        /// <summary>
        /// Start date, e.g.: 2016-10-19 15:25
        /// </summary>
        public string st { get; set; }
        /// <summary>
        /// End date, e.g.: 2016-11-19 17:25
        /// </summary>
        public string en { get; set; }
        /// <summary>
        /// English line
        /// </summary>
        public string ee { get; set; }
        /// <summary>
        /// French line.
        /// </summary>
        public string ef { get; set; }
        /// <summary>
        /// F line.
        /// </summary>
        public string f { get; set; }
        /// <summary>
        /// G line.
        /// </summary>
        public string g { get; set; }
    }

    public class IcaNotamGeo
    {
        public string type { get; set; }
        public string coordinate { get; set; }
    }
}