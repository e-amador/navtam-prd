﻿using NavCanada.Core.Scheduler;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utils.App.Scheduler
{
    public partial class Form1 : Form
    {
        SchedulerShell _schedulerShell;

        public Form1()
        {
            InitializeComponent();
            _schedulerShell = new SchedulerShell(ReadSchedulerSettings());

            btnStop.Enabled = false;
            btnPause.Enabled = false;
            btnStart.Enabled = true;
            btnScheduleJobs.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _schedulerShell.Start();
            lblScheduleInfo.Text = "Schedule Started...";
            btnStart.Enabled = false;
            btnPause.Enabled = true;
            btnStop.Enabled = true;
            btnScheduleJobs.Enabled = true;

        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            _schedulerShell.Pause();
            lblScheduleInfo.Text = "Schedule Paused...";
            btnPause.Enabled = false;
            btnStop.Enabled = false;
            btnStart.Enabled = true;
            btnScheduleJobs.Enabled = false;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _schedulerShell.Stop();
            lblScheduleInfo.Text = "Schedule Stopped";
            btnPause.Enabled = false;
            btnStop.Enabled = false;
            btnStart.Enabled = true;
            btnScheduleJobs.Enabled = false;
        }

        private void btnScheduleJobs_Click(object sender, EventArgs e)
        {
            _schedulerShell.StartJobs();
            btnScheduleJobs.Enabled = false;
        }

        private static SchedulerSettings ReadSchedulerSettings()
        {
            return new SchedulerSettings
            {
                DbConnectionString = "" //ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
            };
        }
    }
}
