﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IOrganizationDoaRepo : IRepository<OrganizationDoa>
    {
        Task<List<Organization>> GetOrganizationWithDoaId(int doaId);
        Task<int> CountOrganizationsAsync(int doaId);
        Task<List<DoaAdminPartialDto>> GetDoaPartials(QueryOptions queryOptions, string separator);
        Task<List<Doa>> GetOrganizationDoasAsync(int orgId);
        Task<List<Organization>> GetOrganizationsAssignedToDoaAsync(int doaId);
        Task UpdateOrganizationDoas(int orgId, IEnumerable<int> doaIds);
        Task<List<OrganizationDoa>> GetOrganizationsByDoaIdsAsync(List<int> doaIds);
    }
}
