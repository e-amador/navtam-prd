﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IProposalAcknowledgementRepo : IRepository<ProposalAcknowledgement>
    {
        Task<ProposalAcknowledgement> GetByProposalIdAndOrgIdAsync(int proposalId, int orgId);
        Task<List<ProposalAcknowledgement>> GetByProposalIdAsync(int proposalId);
        Task<bool> HasRecordsAsync(int proposalId);
    }
}