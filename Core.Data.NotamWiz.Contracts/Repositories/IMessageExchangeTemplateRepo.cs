﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Threading.Tasks;
    using Domain.Model.Entitities;
    using System.Collections.Generic;
    using System;
    using Domain.Model.Dtos;

    public interface IMessageExchangeTemplateRepo : IRepository<MessageExchangeTemplate>
    {
        Task<MessageExchangeTemplate> GetByIdAsync(Guid id);

        Task<MessageExchangeTemplate> GetByNameAsync(string name);

        Task<int> GetCountByName(string name);

        Task<List<MessageExchangeTemplateDto>> GetAllPartialsAsync();

        Task<List<MessageExchangeTemplateDto>> GetAllTemplateKeyParValuesAsync();

    }
}




