﻿using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface ISystemStatusRepo : IRepository<SystemStatus>
    {
        SystemStatus GetStatus();
    }
}
