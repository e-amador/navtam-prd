﻿using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface INotamTopicRepo  : IRepository<NotamTopic>
    {
    }
}
