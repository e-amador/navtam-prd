﻿using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IScheduleJobRepo : IRepository<ScheduleJob>
    {
    }
}
