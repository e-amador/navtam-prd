﻿using System;
using System.Collections.Generic;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface INdsOutgoingMessageRepo : IRepository<NdsOutgoingMessage>
    {
        List<NdsOutgoingMessage> GetLatestMessages(int max = 100);
        int GetCount();
    }
}
