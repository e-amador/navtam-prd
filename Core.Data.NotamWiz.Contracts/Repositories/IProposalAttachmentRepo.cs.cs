﻿using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IProposalAttachmentRepo : IRepository<ProposalAttachment>
    {
        Task<ProposalAttachment> GetByIdAsync(Guid id);
        Task<List<ProposalAttachment>> GetByProposalIdAsync(int proposalId);
    }
}
