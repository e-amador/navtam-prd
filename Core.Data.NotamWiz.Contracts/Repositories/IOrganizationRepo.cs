﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IOrganizationRepo : IRepository<Organization>
    {
        Task<List<OrgPartialDto>> GetAllPartialAsync();
        Task<Organization> GetByIdAsync(int id);
        Task<Organization> GetByIdAsync(int id, bool shouldIncludeUsers);
        Task<bool> IsNameAvailableAsync(int id, string name);
        Task<Organization> GetByNameAsync(string name);
        Task<List<Organization>> GetOrgsByIdsAsync(int[] idsArray);
        Task<Organization> GetByUserIdAsync(string userId, bool shouldIncludeUsers);
        Task<List<Organization>> GetAllAsyncWithDoa();
        Task<OrgDto> GetOrgInfoAsync(int id);
        Task<int> GetAllCountAsync();
        Task<int> GetAllCountAsync(QueryOptions queryOptions);
        Task<List<OrgDto>> GetAllAsync(QueryOptions queryOptions);
        Task<List<OrgDto>> GetOrganizationsByNsdId(int nsdId);
    }
}