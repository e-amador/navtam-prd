﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Collections.Generic;

    using Domain.Model.Entitities;

    public interface IPoisonMtoRepo : IRepository<PoisonMto>
    {
        List<PoisonMto> GetAllMessagesToRetry();
    }
}
