﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IUserQueryFilterNameRepo : IRepository<UserQueryFilterName>
    {
        Task<UserQueryFilterName> GetByUsernameAndSlotNumberAsync(string username, int slotNumber);
        Task<List<UserQueryFilterName>> GetByUsernameAsync(string username);
    }
}
