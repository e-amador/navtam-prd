﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface INdsIncomingRequestRepo : IRepository<NdsIncomingRequest>
    {
        List<NdsIncomingRequest> GetLatestMessages(int max = 100);
        int GetCount();
    }
}
