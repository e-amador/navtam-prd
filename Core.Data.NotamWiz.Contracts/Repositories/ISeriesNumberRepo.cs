﻿using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface ISeriesNumberRepo : IRepository<SeriesNumber>
    {
        SeriesNumber GetSeriesNumber(char series, int year);
        Task<SeriesNumber> GetSeriesNumberAsync(char series, int year);
        void SafeUpdateSeriesNumber(SeriesNumber seriesNumber);
    }
}
