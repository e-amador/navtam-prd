﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Threading.Tasks;

    using Domain.Model.Entitities;

    public interface IRegistrationRepo : IRepository<Registration>
    {
        Task<Registration> GetByIdAsync(int id);
        Task<Registration> GetByUserIdAsync(string userId);
        Task<Registration> GetByNameAsync(string name);
        Task<bool> ExistAsync(string name);
    }
}
