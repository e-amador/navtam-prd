﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using Domain.Model.Entitities;

    public interface IPermissionRepo : IRepository<Permission>
    {
    }
}
