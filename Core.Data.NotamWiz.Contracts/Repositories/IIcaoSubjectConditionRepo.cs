﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Domain.Model.Entitities;
    using Domain.Model.Dtos;
    using Common.Common;

    public interface IIcaoSubjectConditionRepo : IRepository<IcaoSubjectCondition>
    {
        Task<IcaoSubjectCondition> GetByIdAsync(int id);
        IcaoSubjectCondition GetByCode(long subjectId, string code);
        Task<IcaoSubjectCondition> GetByCodeAsync(long subjectId, string code);
        Task<List<string>> GetAllDistinctCodesAsync();
        Task<List<IcaoSubjectCondition>> GetBySubjectIdAsync(int subjectId);
        Task<IcaoSubjectConditionDto> GetModelAsync(int id);
        Task<List<IcaoSubjectConditionDto>> GetBySubjectIdActiveAsync(int subjectId, string lang);
        Task<List<IcaoSubjectConditionDto>> GetCancelledActiveOnlyAsync(int subjectId, string lang);
        Task<int> GetAllCountAsync(int subjectId);
        Task<List<IcaoSubjectConditionPartialDto>> GetAllAsync(int subjectId, QueryOptions queryOptions);
    }
}

