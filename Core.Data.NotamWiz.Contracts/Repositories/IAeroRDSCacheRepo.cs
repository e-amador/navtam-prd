﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface IAeroRDSCacheRepo : IRepository<AeroRDSCache>
    {
    }
}
