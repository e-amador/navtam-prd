﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IUserQueryFilterRepo : IRepository<UserQueryFilter>
    {
        Task<UserQueryFilter> GetProposalFiltersBySlotAndUsernameAsync(int slotNumber, string username);
        Task<List<UserQueryFilter>> GetAllProposalFiltersByUsernameAsync(string username);
        Task<UserQueryFilter> GetNotamFiltersBySlotAndUsernameAsync(int slotNumber, string username, UserFilterType filterType);
        Task<List<UserQueryFilter>> GetAllNotamFiltersByUsernameAsync(string username, UserFilterType filterType);
    }
}
