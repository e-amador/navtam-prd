﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface IUserRegistrationRepo : IRepository<UserRegistration>
    {
        Task<UserRegistration> GetUserRegistrationAsync(string userId);
    }
}
