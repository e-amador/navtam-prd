﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Threading.Tasks;

    using Domain.Model.Entitities;
    using System.Data.Entity.Spatial;
    using System.Collections.Generic;
    using Domain.Model.Dtos;
    using Common.Common;

    public interface IGeoRegionRepo : IRepository<GeoRegion>
    {
        Task<GeoRegion> GetByIdAsync(int id);

        GeoRegion GetByName(string name);

        Task<GeoRegion> GetByGeoLocationAsync(DbGeography geoLocation);

        Task<List<GeoRegionDto>> GetAllGeoRegionsAsync();

        Task<int> GetAllCountAsync();

        Task<List<GeoRegionDto>> GetGeoRegionPartials(QueryOptions queryOptions);

        Task<bool> Exists(int id, string name);
    }
}
