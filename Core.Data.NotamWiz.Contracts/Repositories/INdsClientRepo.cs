﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface INdsClientRepo : IRepository<NdsClient>
    {
        Task<NdsClient> GetByIdAsync(int id);
        IEnumerable<NdsClientIdAddressDto> GetMatchingAftnAddresses(string itemA, string series, bool allowAllSeries, bool bilingual, bool onlyActive);
        IEnumerable<NdsClientIdAddressDto> GetMatchingAftnAddressesByGeoRef(DbGeography region, int lowerLimit, int upperLimit, bool bilingual, bool onlyActive);
        bool CanAddressPerformQueries(string clientAddress);
        Task<NdsClient> GetByClientNameAsync(string clientName);
        string GetClientName(string clientAddress);
        Task<int> GetAllCountAsync();
        Task<List<NdsClientPartialDto>> GetAllAsync(QueryOptions queryOptions);
        Task<List<NdsClientAddressDto>> GetAllSubscriptionsAsync();
        Task<bool> Exists(string name);
        Task<bool> Exists(int id, string name);

    }
}
