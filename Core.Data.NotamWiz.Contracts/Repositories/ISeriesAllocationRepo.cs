﻿using System.Collections.Generic;
using NavCanada.Core.Domain.Model.Enums;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using Domain.Model.Entitities;
    using Domain.Model.Dtos;
    using Common.Common;

    public interface ISeriesAllocationRepo : IRepository<SeriesAllocation>
    {
        Task<SeriesAllocation> GetByIdAsync(int id);
        Task<SeriesAllocation> GetByIdWithRegionAsync(int id);
        Task<List<SeriesAllocation>> GetByRegionIdAndCodeAndCategoryIdAsync(int regionId, string code,
            DisseminationCategory category);
        Task<int> GetAllCountAsync(QueryOptions queryOptions);
        Task<List<SerieAllocationDto>> GetAllAsync(QueryOptions queryOptions);
    }
}
