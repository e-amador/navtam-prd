﻿using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface INotifySeriesRepo : IRepository<NotifySeries>
    {
    }
}
