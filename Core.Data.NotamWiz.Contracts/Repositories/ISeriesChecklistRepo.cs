﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface ISeriesChecklistRepo : IRepository<SeriesChecklist>
    {
        Task<SeriesChecklist> GetByIdAsync(string serie);
        Task<int> GetAllCountAsync(QueryOptions queryOptions);
        Task<List<SeriesChecklist>> GetAllAsync(QueryOptions queryOptions);
        Task<string> GetSeriesInUseAsync();
        List<string> GetAvailableSeries();
        Task<bool> Exists(string name);
    }
}
