﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using Domain.Model.Entitities;
    using Domain.Model.Enums;

    public interface INotificationTemplateRepo : IRepository<NotificationTemplate>
    {
        NotificationTemplate GetByTypeAndTemplateType(TemplateType templateType, NotificationType notificationType);
    }
}
