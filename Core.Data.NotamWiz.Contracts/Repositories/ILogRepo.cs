﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface ILogRepo : IRepository<Log>
    {
        /// <summary>
        /// Returns the logs between the two given dates.
        /// </summary>
        Task<List<Log>> GetByStartandEndDate(DateTime StartTime, DateTime EndTime);
        /// <summary>
        /// The results will be sorted in descending order. Meaning the first element in the list will be the latest log before the given date.
        /// </summary>
        Task<List<Log>> GetLogsBeforeDate(DateTime date, int total);
    }
}