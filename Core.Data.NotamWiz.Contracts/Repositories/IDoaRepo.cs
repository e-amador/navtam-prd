﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IDoaRepo : IRepository<Doa>
    {
        Task<Doa> GetByIdAsync(int id);
        Task<Doa> GetByIdWithFiltersAsync(int id);
        Task<bool> Exists(string name);
        Task<bool> Exists(int id,string name);
        Task<int> GetAllCountAsync();
        Task<int> GetAllCountAsync(QueryOptions queryOptions);
        Task<List<Doa>> GetByDoaIdsAsync(IEnumerable<string> doaIds);
        Doa GetBilingualRegion();
        Task<Doa> GetBilingualRegionAsync();
        Doa GetNorthernRegion();
        Task<Doa> GetNorthernRegionAsync();
        Doa GetNofDoa();
        Task<Doa> GetNofDoaAsync();
        Task<List<Doa>> GetDoasInLocationAsync(DbGeography doaGeoArea);
    }
}
