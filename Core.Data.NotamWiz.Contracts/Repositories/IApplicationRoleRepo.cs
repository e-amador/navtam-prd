﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IApplicationRoleRepo : IRepository<ApplicationRole>
    {
        Task<ApplicationRole> GetRoleByName(string roleName);
        Task<List<ApplicationRole>> GetByRoleIdsAsync(IEnumerable<string> roleIds);
        Task<List<ApplicationRole>> GetDistintUserRoleAndOrgAdminRoleAsync();
        Task<List<ApplicationRole>> GetWithUserRoleOrgAdminRoleAndCatchAllAsync();
        List<ApplicationRole> GetByRoleName(string roleId);
    }
}
