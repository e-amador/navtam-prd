﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using Domain.Model.Dtos;
    using Domain.Model.Entitities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface INofTemplateMessageRecordRepo : IRepository<NofTemplateMessageRecord>
    {
        Task<NofTemplateMessageRecordDto> GetBySlotAndUsernameAsync(int slotNumber, string username);
        Task<List<NofTemplateMessageRecordDto>> GetAllByUsernameAsync(string username);
    }
}





