﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IProposalAnnotationRepo : IRepository<ProposalAnnotation>
    {
        Task<ProposalAnnotation> GetByIdAsync(int id);
        Task<List<ProposalAnnotation>> GetByProposalIdAsync(int proposalId);
        Task<List<ProposalAnnotation>> GetByProposalIdDescAsync(int proposalId);
    }
}
