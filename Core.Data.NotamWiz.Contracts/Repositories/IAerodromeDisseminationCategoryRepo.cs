﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using Common.Common;
    using Domain.Model.Dtos;
    using Domain.Model.Entitities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAerodromeDisseminationCategoryRepo : IRepository<AerodromeDisseminationCategory>
    {
        Task<AerodromeDisseminationCategory> GetByAhpMidAsync(string ahpMid);
        Task<AerodromeDisseminationCategory> GetByAhpCodeIdAsync(string ahpCodeId);
        Task<AerodromeDisseminationCategory> GetByIdAsync(int id);
        Task<int> GetAllCountAsync();
        Task<int> GetAllCountAsync(QueryOptions queryOptions);
        Task<List<AerodromeDisseminationCategoryDto>> GetAllAsync(QueryOptions queryOptions);
    }
}
