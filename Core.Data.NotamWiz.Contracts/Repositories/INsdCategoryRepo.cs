﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface INsdCategoryRepo : IRepository<NsdCategory>
    {
        Task<List<NsdCategory>> GetRootChildrenAsync();
        Task<List<NsdCategory>> GetChildCategoriesAsync(int parentCategoryId);
        Task<List<NsdCategory>> GetActiveNsdsByOrganizationId(int orgId);
        Task<NsdCategory> GetByNsdId(int nsdId);
        Task<List<NsdCategory>> GetActiveNsds();
        Task<List<NsdCategory>> GetAllNsds();
        Task<List<NsdCategory>> GetOperationalNsdsByOrganizationId(int orgId);
        NsdCategory GetByName(string name);
    }
}