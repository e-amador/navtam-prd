﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using Domain.Model.Entitities;
    using Domain.Model.Enums;

    public interface INotificationTrackingRepo : IRepository<NotificationTracking>
    {
        NotificationTracking GetLastEmailSent(int packageId, TemplateType templateType, NotificationType notificationType);
    }
}
