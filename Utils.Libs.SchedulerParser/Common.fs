﻿module Utils.Libs.SchedulerParser.Common

open FParsec
open FParsec.Pipes

let _SUNRISE_IN_MINUTES_ = 4000000
let _SUNSET_IN_MINUTES_ = 5000000
let _MINUTES_IN_DAY_ = 1440

let test p str =
    match run p str with
    | Success(result, _, _)   -> printfn "Success: %A" result
    | Failure(errorMsg, _, _) -> printfn "Failure: %s" errorMsg

let ws = spaces

type ParserCase = TimeOnly | Calendar | WeekDay
type Month = Jan | Feb | Mar | Apr | May | Jun | Jul | Aug | Sep | Oct | Nov | Dec

type ParserSubcase = None | DaysRange | Continues | Distinct | DistinctAndDaysRange

type PeriodInMinutes = 
    { 
        start: int; 
        finish: int 
    }

type DayPeriod = 
    { 
        startDay : int; 
        endDay : int
    }

type ParserResult = 
    { 
        case : ParserCase; 
        subcase: ParserSubcase;
        included : bool;  
        months : Month[];
        days : DayPeriod[];
        minutes : PeriodInMinutes[] 
    } 
    
type IntHour(value) =
  do
    if value < 0 || value > 23 then
      invalidArg "value" "Hours between 0 and 23"
  member x.Value = value

type IntMinute(value) =
  do
    if value < 0 || value > 59 then
      invalidArg "value" "Minutes between 0 and 59"
  member x.Value = value

type IntDay(value) =
  do
    if value < 1 || value > 31 then
      invalidArg "value" "Day between 1 and 31"
  member x.Value = value

type Minutes = FourDigits | H24 | Sunrise | Sunset | SunriseMinus | SunsetPlus 

type Filter internal ()=

    static member Digits( count : int) = 
        %% +.(qty.[count] * digit)
        -%> ( System.String >> System.Int32.Parse) //System.String

    static member MayThrow (p: Parser<'t,'u>) : Parser<'t,'u> =
        fun stream ->
            let state = stream.State        
            try 
                p stream
            with e -> // catching all exceptions is somewhat dangerous
                stream.BacktrackTo(state)
                Reply(FatalError, messageError e.Message)

    static member Month : Parser<Month, unit> =
        (stringReturn "JAN" Jan)
            <|> (stringReturn "FEB" Feb)
            <|> (stringReturn "MAR" Mar)
            <|> (stringReturn "APR" Apr)
            <|> (stringReturn "MAY" May)
            <|> (stringReturn "JUN" Jun)
            <|> (stringReturn "JUL" Jul)
            <|> (stringReturn "AUG" Aug)
            <|> (stringReturn "SEP" Sep)
            <|> (stringReturn "OCT" Oct)
            <|> (stringReturn "NOV" Nov)
            <|> (stringReturn "DEC" Dec)

    static member WeekDay : Parser<int, unit> =
        (stringReturn "SUN" 0)
            <|> (stringReturn "MON" 1)
            <|> (stringReturn "TUE" 2)
            <|> (stringReturn "WED" 3)
            <|> (stringReturn "THU" 4)
            <|> (stringReturn "FRI" 5)
            <|> (stringReturn "SAT" 6)

    static member SunriseMinus : Parser<int, unit> = 
        Filter.MayThrow(
            %% %"SR " -- ws -- "MINUS" -- +.Filter.Digits 2 -? ws -%> fun x -> _SUNRISE_IN_MINUTES_ + (IntMinute x).Value) 

    static member SunsetPlus : Parser<int, unit> =
        Filter.MayThrow(
            %% %"SS " --ws -- "PLUS" -? +.Filter.Digits 2 -? ws -%> fun x -> _SUNSET_IN_MINUTES_ + (IntMinute x).Value) 

    static member Sunrise : Parser<int, unit> =
            %% %"SR" -%> _SUNRISE_IN_MINUTES_
         
    static member Sunset : Parser<int, unit> = 
            %% %"SS" -%> _SUNSET_IN_MINUTES_

    static member FourDigits : Parser<int, unit> = 
        Filter.MayThrow( 
            %% +.Filter.Digits 2 -- +.Filter.Digits 2 
            -- ws -%> fun x y -> (IntHour x).Value * 60 + (IntMinute y).Value) 

    static member SetOfMinutes : Parser<int, unit> = 
        let parser : Parser<int, unit> = 
            %[  Filter.SunsetPlus; Filter.SunriseMinus;  
                Filter.Sunrise; Filter.Sunset; Filter.FourDigits ]
        parser        

    static member SinglePeriodInMinutes : Parser<PeriodInMinutes, unit> = 
            %% ws -- %["DAILY"; ""] -- ws -?
            +.Filter.SetOfMinutes -- ws -- '-'  
            -- ws -- +.Filter.SetOfMinutes -- ws -%> fun x y -> 
                { 
                    start = x; 
                    finish = y 
                } 

    static member MultiplePeriodInMinutes : Parser<PeriodInMinutes[], unit> = 
        Filter.SinglePeriodInMinutes * qty.[0..] |>> (fun x -> x.ToArray() )  

