﻿module Utils.Libs.SchedulerParser.Calendar

open FParsec
open FParsec.Pipes
open Common

type MonthWithDays = { 
        month: Month; 
        days : DayPeriod[] 
    }

type DaysAndMinutes = { 
        month: Month; 
        dayPeriod: int; 
        minutePeriod : PeriodInMinutes
    }

type Parser ()=
    static member private SinglePeriodInDays : Parser<DayPeriod, unit> = 
        Filter.MayThrow(
            %% +.Filter.Digits 2 -- ws 
                -- '-' -? ws -- +.Filter.Digits 2 
                --ws -%>
                    fun x y -> 
                    let sDay = (IntDay x).Value; 
                    let eDay = (IntDay y).Value; 
                    { 
                        startDay = sDay; 
                        endDay = eDay;  
                    }) 

    static member private Excluded : Parser<bool, unit> = 
        %% ws -- %"EXC" -%> true

    static member private ManyPeriodInDays : Parser<DayPeriod[], unit> = 
        Parser.SinglePeriodInDays * qty.[1..] |>> (fun x -> x.ToArray() )  

    static member private Minutes : Parser<int, unit> = 
        %% ws -- %["DAILY"; ""] -? ws -- +.Filter.SetOfMinutes -%> fun x -> x  

    static member private Days : Parser<DayPeriod, unit> = 
        Filter.MayThrow(
            %% +.Filter.Digits 2 -- ' ' -? ws 
            -%> fun x -> 
                let day = (IntDay x).Value; 
                { 
                    startDay = day;  
                    endDay = day;  
                })  

    static member private ManyMinutes : Parser<int[], unit> = 
        Parser.Minutes * qty.[1..] |>> (fun x -> x.ToArray()) 

    static member private ManyDays : Parser<DayPeriod[], unit> = 
        Parser.Days * qty.[1..] |>> (fun x -> x.ToArray()) 

    static member private ManyPeriodInMinutes : Parser<PeriodInMinutes[], unit> = 
        Filter.SinglePeriodInMinutes * qty.[1..] |>> (fun x -> x.ToArray()) 

    static member private MatchingMonthDayWithMinutes : Parser<DaysAndMinutes, unit> = 
        Filter.MayThrow(
            %% +.Filter.Month -? ws -- +.Filter.Digits 2 -? 
               +.Parser.Minutes -- ws -%> fun x y z -> 
                    let day = (IntDay y).Value; 
                    { 
                        month = x; 
                        dayPeriod = day;
                        minutePeriod = { start = z; finish = 0 } 
                    })

    static member private MatchingMonthDayWithoutMinutes : Parser<DaysAndMinutes, unit> = 
        Filter.MayThrow(
            %% +.Filter.Month -? ws -- +.Filter.Digits 2 -- ws -%> 
                fun x y -> 
                    let day = (IntDay y).Value; 
                    { 
                        month = x; 
                        dayPeriod = day;
                        minutePeriod = { start = 0; finish = 0 } 
                    })

    static member private MatchingMonthsDays : Parser<MonthWithDays, unit> = 
        %% +.Filter.Month -? +.Parser.ManyDays -- ws -%> fun x y -> 
            { 
                month = x; 
                days = y 
            }

    static member private DaysTimeRangeSubcase : Parser<ParserResult, unit> =  
        %% ws -- +.Filter.Month -? ws -- +.Parser.ManyPeriodInDays -? ws -- +.Parser.ManyPeriodInMinutes -? ws -%> 
            fun x y z-> 
                { 
                    case = Calendar;
                    subcase = DaysRange;
                    included = true;  
                    months = [|x|]; 
                    days = y; 
                    minutes = z 
                }

    static member private ExcludedDaysRangeSubcase : Parser<ParserResult, unit> =  
        %% +.Parser.Excluded -? ws -- +.Filter.Month -? ws -- +.Parser.ManyPeriodInDays -? ws -%> 
            fun e x y -> 
                { 
                    case = Calendar;
                    subcase = DaysRange;
                    included = not e;  
                    months = [|x|]; 
                    days = y; 
                    minutes = [||] 
                }


    static member private ContinuesSubcase : Parser<ParserResult, unit> =  
        %% +.Parser.MatchingMonthDayWithMinutes -? 
            ws -- '-' ?- ws -- +.Parser.MatchingMonthDayWithMinutes -? ws -%> 
            fun x y -> 
                { 
                    case = Calendar;
                    subcase = Continues;
                    included = true;
                    months = [|x.month; y.month|]; 
                    days = [|{ startDay = x.dayPeriod; endDay = y.dayPeriod }|];
                    minutes = [|{start = x.minutePeriod.start; finish = y.minutePeriod.start};|]
                }

    static member private ExcludedContinuesSubcase : Parser<ParserResult, unit> =  
        %% +.Parser.Excluded -? ws -- +.Parser.MatchingMonthDayWithoutMinutes -? 
            ws -- '-' ?- ws -- +.Parser.MatchingMonthDayWithoutMinutes -- ws  -%> 
            fun e x y -> 
                { 
                    case = Calendar;
                    subcase = Continues;
                    included = not e;
                    months = [|x.month; y.month|]; 
                    days = [|{ startDay = x.dayPeriod; endDay = y.dayPeriod }|];
                    minutes = [||]
                }

    static member private DistinctSubcase : Parser<ParserResult, unit> =  
        %% ws -- +.Filter.Month -? ws -- +.Parser.ManyDays -? ws
            -- +.Parser.ManyPeriodInMinutes -? ws -%> 
            fun x y z -> 
                { 
                    case = Calendar;
                    included = true;
                    subcase = Distinct;
                    months = [|x|]; 
                    days = y;
                    minutes = z
                 }

    static member private ExcludedDistinctSubcase : Parser<ParserResult, unit> =  
        %% +.Parser.Excluded -? ws -- +.Filter.Month -? ws -- +.Parser.ManyDays -- ws -%> 
            fun e x y -> 
                { 
                    case = Calendar;
                    included = not e;
                    subcase = Distinct;
                    months = [|x|]; 
                    days = y;
                    minutes = [||]
                 }

    static member private DistinctAndDaysRangeSubcase : Parser<ParserResult, unit> =  
        let pComb = %[Parser.ManyPeriodInDays; Parser.ManyDays] * qty.[1..] 
                  |>> (fun result -> [| for r in result do for d in r -> d |]; )  

        %% ws -- +.Filter.Month -? ws -- +.pComb -? ws 
                    -- +.Parser.ManyPeriodInMinutes -? ws -%> 
                    fun x y z -> 
                        { 
                            case = Calendar;
                            included = true;
                            subcase = DistinctAndDaysRange;
                            months = [|x|]; 
                            days = y;
                            minutes = z
                         }

    static member private ExcludedDistinctAndDaysRangeSubcase : Parser<ParserResult, unit> =  
        let pComb = %[Parser.ManyPeriodInDays; Parser.ManyDays] * qty.[1..] 
                  |>> (fun result -> [| for r in result do for d in r -> d |]; )  

        %% +.Parser.Excluded -- ws -- +.Filter.Month -? ws -- +.pComb -? ws 
                    -- +.Parser.ManyPeriodInMinutes -? ws -%> 
                    fun e x y z -> 
                        { 
                            case = Calendar;
                            included = not e;
                            subcase = DistinctAndDaysRange;
                            months = [|x|]; 
                            days = y;
                            minutes = z
                         }


    static member Pipeline : Parser<ParserResult, unit> = 
        %[
            Parser.ExcludedDaysRangeSubcase;
            Parser.DaysTimeRangeSubcase; 
            Parser.ExcludedContinuesSubcase;
            Parser.ContinuesSubcase; 
            Parser.ExcludedDistinctSubcase; 
            Parser.DistinctSubcase; 
            Parser.ExcludedDistinctAndDaysRangeSubcase;
            Parser.DistinctAndDaysRangeSubcase]


    static member Parse( str : string ) : ParserResult<ParserResult, unit> = 
        run Parser.Pipeline str

