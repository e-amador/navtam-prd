﻿module Utils.Libs.SchedulerParser.Parser

open FParsec
open FParsec.Pipes
open Common
open Calendar
open TimeOnly
open WeekDay

type Agent ()=
    let ws = spaces

    static member Run( str : string ) : ParserResult<ParserResult, unit> = 
        let timeOnlyParser : Parser<_, unit> =  TimeOnly.Parser.Pipeline
        let calendarParser : Parser<_, unit> = Calendar.Parser.Pipeline
        let weekDayParser : Parser<_, unit> = WeekDay.Parser.Pipeline

//      let mutable mStr = str
//
//      let weekDayParserSpecialCase = run (many1 Filter.WeekDay) str
//      match weekDayParserSpecialCase with
//      | Success(result, _, _) -> mStr <- str + " "
//      | Failure(result, _, _) -> mStr <- str + " "
        
        run %[calendarParser; timeOnlyParser; weekDayParser] (str + " ") 
