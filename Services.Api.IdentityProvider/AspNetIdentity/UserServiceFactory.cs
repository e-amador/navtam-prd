﻿namespace NavCanada.Services.Api.IdentityProvider.AspNetIdentity
{
    using IdentityServer3.AspNetIdentity;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public static class UserServiceFactory
    {
        public static AspNetIdentityUserService<ApplicationUser, string> Create()
        {
            var context = new ApplicationDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            //return new AspNetIdentityUserService<ApplicationUser, string>(userManager);
            return new CustomUserService(userManager);
        }
    }

    public class CustomUserService : AspNetIdentityUserService<ApplicationUser, string>
    {
        public CustomUserService(UserManager<ApplicationUser, string> userManager, System.Func<string, string> parseSubject = null) 
            : base(userManager, parseSubject)
        {
        }

        //public override Task PreAuthenticateAsync(PreAuthenticationContext context)
        //{
        //    //var id = ctx.Request.Query.Get("signin");
        //    IEnumerable<System.Security.Claims.Claim> claims = null;
        //    context.AuthenticateResult = new AuthenticateResult("~/customlogin/", claims);
        //    return Task.FromResult(0);
        //}
    }
}