﻿using ApplicationUser = NavCanada.Services.Api.IdentityProvider.AspNetIdentity.ApplicationUser;

namespace NavCanada.Services.Api.IdentityProvider.AspNetIdentity
{
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("name=cnn")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}