﻿using Microsoft.Owin;
using NavCanada.Core.Common.Common;
using NavCanada.Services.Api.IdentityProvider.AspNetIdentity;
using NavCanada.Services.Api.IdentityProvider.Config;

[assembly: OwinStartup(typeof(NavCanada.Services.Api.IdentityProvider.Startup))]

namespace NavCanada.Services.Api.IdentityProvider
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography.X509Certificates;

    using IdentityServer3.Core.Configuration;
    using IdentityServer3.Core.Services;
    using IdentityServer3.EntityFramework;

    using Owin;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.Map("/identity", idsrvApp =>
            {
/*
                var idSrvServiceFactory = new IdentityServerServiceFactory()
                    .UseInMemoryClients(Clients.Get())
                    .UseInMemoryUsers(Users.Get())
                    .UseInMemoryScopes(Scopes.Get());
*/

                var idSrvServiceFactory = new IdentityServerServiceFactory();

                var efConfig = new EntityFrameworkServiceOptions
                {
                    ConnectionString = "cnn",
                    //Schema = "someSchemaIfDesired"
                };

                idSrvServiceFactory.ViewService = new Registration<IViewService, CustomViewService>();
                idSrvServiceFactory.UserService = new Registration<IUserService>(UserServiceFactory.Create());
                idSrvServiceFactory.RegisterClientStore(efConfig);
                idSrvServiceFactory.RegisterScopeStore(efConfig);
                idSrvServiceFactory.RegisterOperationalServices(efConfig);

                idSrvServiceFactory.ConfigureClients(Clients.Get());
                idSrvServiceFactory.ConfigureScopes(Scopes.Get());

                var cleanup = new TokenCleanup(efConfig, 60);
                cleanup.Start();

                var options = new IdentityServerOptions
                    {
                        Factory = idSrvServiceFactory,
                        SiteName = "Navcanada Identity Provider",
                        IssuerUri = EndPoints.PlatformIssueUri,
                        PublicOrigin = EndPoints.PlatformStsOrigin,
                        SigningCertificate = LoadCertificate(),
                        AuthenticationOptions = new AuthenticationOptions
                        {
                            EnablePostSignOutAutoRedirect = true,
                            LoginPageLinks = new List<LoginPageLink>
                            {
                                new LoginPageLink
                                {
                                    Type="createaccount",
                                    Text = "Create new Account",
                                    Href = "~/createuseraccount"
                                }
                            }
                        }
                    };
                    idsrvApp.UseIdentityServer(options);
                }
            );


        }

        X509Certificate2 LoadCertificate()
        { 
            return  new X509Certificate2(
                    string.Format(@"{0}\Certificates\idsrv3test.pfx", AppDomain.CurrentDomain.BaseDirectory),
                    "idsrv3test"
                );
        }
    
    }
}
