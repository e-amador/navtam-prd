﻿namespace NavCanada.Services.Api.IdentityProvider.Config
{
    using System.Collections.Generic;

    using IdentityServer3.Core.Models;

    public class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            return new List<Scope> { 
                new Scope{
                    Name = "datamanager",
                    DisplayName = "Data Manager",
                    Description = "Allow the application to manage data resources on your behalf",
                    Type = ScopeType.Resource,
                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("role", false)
                    }
                },
                new Scope{
                    Name = "roles",
                    Enabled = true,
                    DisplayName = "Application Roles",
                    Description = "Allow the application to manage roles on your behalf",
                    Type = ScopeType.Identity,
                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("role", true)
                    }
                },

                StandardScopes.OpenId,
                StandardScopes.ProfileAlwaysInclude, //StandardScopes.Profile
                StandardScopes.Address,
                StandardScopes.OfflineAccess
            };
        }
    }
}