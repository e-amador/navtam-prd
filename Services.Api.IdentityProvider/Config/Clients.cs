﻿using NavCanada.Core.Common.Common;

namespace NavCanada.Services.Api.IdentityProvider.Config
{
    using System.Collections.Generic;

    using IdentityServer3.Core.Models;

    public class Clients
    {

        public static IEnumerable<Client> Get()
        {
            return new[]
            {
                new Client
                {
                    ClientId = "AimForms-MvcAppHybrid",
                    ClientName = "AimForms",
                    Flow = Flows.Hybrid,
                    Enabled = true,
                    RequireConsent = false,
                    AllowRememberConsent = true,
                    AllowAccessToAllScopes = true,
                    IdentityTokenLifetime = 10,
                    AccessTokenLifetime = 60,
                    RedirectUris = new List<string>
                    {
                        EndPoints.MvcClientUrl
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        EndPoints.MvcClientUrl
                    },
                    //Required for access_token endpoint
                    ClientSecrets = new List<Secret>
                    {
                        new Secret(EndPoints.PlatformClientSecret.Sha256())
                    },
                }
            };
        }
    }
}