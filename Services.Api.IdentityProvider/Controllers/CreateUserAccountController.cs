﻿using CreateUserAccountModel = NavCanada.Services.Api.IdentityProvider.Models.CreateUserAccountModel;

namespace NavCanada.Services.Api.IdentityProvider.Controllers
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;

    using NavCanada.Services.Api.IdentityProvider.AspNetIdentity;

    public class CreateUserAccountController : Controller
    {
        /*
                public CreateUserAccountController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
                {
                    this.userManager = userManager;
                    this.roleManager = roleManager;
                }
        */

        [HttpGet]
        public ActionResult Index(string signin)
        {
            var userModel = new CreateUserAccountModel
            {
                Username = "user1",
                Firstname = "User",
                Lastname = "ONE",
                Password = "Passw0rd!",
                ConfirmPassword = "Passw0rd!",
                Email = "user1@email.com"
            };

            return View(userModel);
        }

        [HttpPost]
        public async Task<ActionResult> Index(string signin, CreateUserAccountModel model)
        {
            IdentityResult result = IdentityResult.Success;

            if (ModelState.IsValid)
            {
                var userId = Guid.NewGuid().ToString();
                result = await UserManager.CreateAsync(
                         new ApplicationUser
                         {
                             Id = userId,
                             UserName = model.Username,
                             FirstName = model.Firstname,
                             LastName = model.Lastname,
                             Email = model.Email
                         }, model.Password);

                if (result.Succeeded)
                {
                    result = await UserManager.AddClaimAsync(userId, new Claim(IdentityServer3.Core.Constants.ClaimTypes.GivenName, model.Firstname));
                    if (result.Succeeded)
                        result = await UserManager.AddClaimAsync(userId, new Claim(IdentityServer3.Core.Constants.ClaimTypes.FamilyName, model.Lastname));

                    if (result.Succeeded)
                        result = await UserManager.AddClaimAsync(userId, new Claim(IdentityServer3.Core.Constants.ClaimTypes.Role, model.Role));
                }
            }

            if (result.Succeeded)
                return Redirect("~/identity/" + IdentityServer3.Core.Constants.RoutePaths.Login + "?signin=" + signin);

            return View(model);
        }

        protected ApplicationUserManager UserManager
        {
            get { return this.userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { this.userManager = value; }
        }

        protected ApplicationRoleManager RoleManager
        {
            get { return this.roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>(); }
            private set { this.roleManager = value; }
        }

        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;
    }
}