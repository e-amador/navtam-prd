﻿using System;
using System.Configuration;
using System.Diagnostics;
using NavCanada.Core.Common.Security;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Exceptions;
using Log = Serilog.Log;

namespace Core.Diagnoistics.SeriLog
{
    public class LoggerSeri : NavCanada.Core.Common.Contracts.ILogger
    {
        static LoggerSeri()
        {
            lock (typeof(LoggerSeri))
            {
                InitSerilogs();
            }
        }

        static void InitSerilogs()
        {
            var logLevel = GetConfiguredLogLevel(LogEventLevel.Information);

            LoggingLevelSwitch levelSwitch = new LoggingLevelSwitch(logLevel);

            Log.Logger = new LoggerConfiguration().Enrich.WithExceptionDetails()
                 .MinimumLevel.ControlledBy(levelSwitch)
                 .WriteTo.MSSqlServer(LogConnectionString, "Log", logLevel, 50, null)
                 .CreateLogger();

            Log.Logger.Debug("Log initialized");

            var listener = new SerilogTraceListener.SerilogTraceListener();
            Trace.Listeners.Add(listener);

            Log.Logger.Debug("Trace log initialized");
        }

        static LogEventLevel GetConfiguredLogLevel(LogEventLevel defaultLevel)
        {
            LogEventLevel logLevel;
            return Enum.TryParse(ConfigurationManager.AppSettings["LogLevel"], out logLevel) ? logLevel : defaultLevel;
        }

        public bool CheckLogStatus()
        {
            return Log.Logger.IsEnabled(LogEventLevel.Information);
        }

        public string CheckLogLevel()
        {
            int level = 0;
            while (level < 5)
            {
                if (Log.Logger.IsEnabled((LogEventLevel)level))
                {
                    LogEventLevel loggingLevel = (LogEventLevel)level;
                    return loggingLevel.ToString();
                }

                ++level;
            }

            return "Log is not functioning properly contact administrator";
        }

        #region Account Login Logging

        public void UserLogout(string userName, string userId)
        {
            Log.Logger.Write(LogEventLevel.Information, "User {@Name} logged out. UserId: {@UserId}", userName, userId);
        }

        public void UserLogin(string userName, string userId)
        {
            Log.Logger.Write(LogEventLevel.Information, "User {@UserName} logged in. UserId: {@UserId}", userName, userId);
        }

        public void InvalidLogin(Type type, string username)
        {
            Log.Logger.Write(LogEventLevel.Information, "Invalid user login {@Username} in class {@Type}", username, type);
        }

        #endregion

        public void LogVerbose(string message)
        {
            Log.Logger.Verbose("{@Message}", message);
        }

        public void LogDebug(Type type, string message)
        {
            Log.Logger.Debug("{@Message} in {@Type}", message, type);
        }

        public void LogInfo(Type type, string message)
        {
            Log.Logger.Information("{@Message} in {@Type}", message, type);
        }

        public void LogWarn(Type type, string message, object obj = null)
        {
            Log.Logger.Warning("{@Message} in {@Type} ... {@Obj}", message, type, obj);
        }

        public void LogError(Exception exception, Type type, string message = null, object obj = null)
        {
            Log.Logger.Error(exception, "{@Message} in {@Type} ... {@Obj} ... {@InnerException}", message, type, obj, exception.InnerException);
        }

        public void LogFatal(Exception ex, Type type, string message = null)
        {
            Log.Logger.Fatal(ex, "{@Message} in {@Type} ... {@InnerException}", message ?? "Fatal error", type, ex.InnerException);
        }

        static string LogConnectionString =>
            CipherUtils.DecryptConnectionString((ConfigurationManager.ConnectionStrings["LogConnection"] ?? ConfigurationManager.ConnectionStrings["DefaultConnection"])?.ConnectionString);
    }
}