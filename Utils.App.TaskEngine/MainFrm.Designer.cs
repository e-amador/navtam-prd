﻿namespace NavCanada.Utils.Apps.TaskEngine
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartTaskEngine = new System.Windows.Forms.Button();
            this.btnStopTaskEngine = new System.Windows.Forms.Button();
            this.lblDiagnosticReq = new System.Windows.Forms.Label();
            this.lblDiagnosticRsp = new System.Windows.Forms.Label();
            this.btnPublishNotam = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnPublishText = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDelay = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCount = new System.Windows.Forms.Label();
            this.chkSubscribe = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnPubishDB = new System.Windows.Forms.Button();
            this.LblInfoStatus = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStartTaskEngine
            // 
            this.btnStartTaskEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStartTaskEngine.BackColor = System.Drawing.Color.LimeGreen;
            this.btnStartTaskEngine.Location = new System.Drawing.Point(282, 393);
            this.btnStartTaskEngine.Margin = new System.Windows.Forms.Padding(2);
            this.btnStartTaskEngine.Name = "btnStartTaskEngine";
            this.btnStartTaskEngine.Size = new System.Drawing.Size(131, 39);
            this.btnStartTaskEngine.TabIndex = 0;
            this.btnStartTaskEngine.Text = "Start TaskEngine";
            this.btnStartTaskEngine.UseVisualStyleBackColor = false;
            this.btnStartTaskEngine.Click += new System.EventHandler(this.OnStartTaskEngine);
            // 
            // btnStopTaskEngine
            // 
            this.btnStopTaskEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStopTaskEngine.BackColor = System.Drawing.Color.Salmon;
            this.btnStopTaskEngine.Location = new System.Drawing.Point(426, 394);
            this.btnStopTaskEngine.Margin = new System.Windows.Forms.Padding(2);
            this.btnStopTaskEngine.Name = "btnStopTaskEngine";
            this.btnStopTaskEngine.Size = new System.Drawing.Size(130, 37);
            this.btnStopTaskEngine.TabIndex = 1;
            this.btnStopTaskEngine.Text = "Stop TaskEngine";
            this.btnStopTaskEngine.UseVisualStyleBackColor = false;
            this.btnStopTaskEngine.Click += new System.EventHandler(this.OnStopTaskEngine);
            // 
            // lblDiagnosticReq
            // 
            this.lblDiagnosticReq.AutoSize = true;
            this.lblDiagnosticReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiagnosticReq.Location = new System.Drawing.Point(321, 43);
            this.lblDiagnosticReq.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDiagnosticReq.Name = "lblDiagnosticReq";
            this.lblDiagnosticReq.Size = new System.Drawing.Size(0, 13);
            this.lblDiagnosticReq.TabIndex = 6;
            this.lblDiagnosticReq.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblDiagnosticRsp
            // 
            this.lblDiagnosticRsp.AutoSize = true;
            this.lblDiagnosticRsp.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiagnosticRsp.Location = new System.Drawing.Point(321, 65);
            this.lblDiagnosticRsp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDiagnosticRsp.Name = "lblDiagnosticRsp";
            this.lblDiagnosticRsp.Size = new System.Drawing.Size(0, 13);
            this.lblDiagnosticRsp.TabIndex = 7;
            this.lblDiagnosticRsp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // btnPublishNotam
            // 
            this.btnPublishNotam.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPublishNotam.Location = new System.Drawing.Point(13, 111);
            this.btnPublishNotam.Name = "btnPublishNotam";
            this.btnPublishNotam.Size = new System.Drawing.Size(81, 35);
            this.btnPublishNotam.TabIndex = 9;
            this.btnPublishNotam.Text = "Publish XML";
            this.btnPublishNotam.UseVisualStyleBackColor = false;
            this.btnPublishNotam.Click += new System.EventHandler(this.btnPublishNotam_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 458);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(575, 22);
            this.statusStrip1.TabIndex = 12;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPublishText
            // 
            this.btnPublishText.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPublishText.Location = new System.Drawing.Point(98, 111);
            this.btnPublishText.Name = "btnPublishText";
            this.btnPublishText.Size = new System.Drawing.Size(85, 35);
            this.btnPublishText.TabIndex = 14;
            this.btnPublishText.Text = "Publish Text";
            this.btnPublishText.UseVisualStyleBackColor = false;
            this.btnPublishText.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gainsboro;
            this.button4.Location = new System.Drawing.Point(11, 70);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(546, 35);
            this.button4.TabIndex = 15;
            this.button4.Text = "Open Plain Text File";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(16, 153);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(541, 236);
            this.listView1.TabIndex = 16;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Delay (ms):";
            // 
            // txtDelay
            // 
            this.txtDelay.Location = new System.Drawing.Point(77, 7);
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(39, 20);
            this.txtDelay.TabIndex = 18;
            this.txtDelay.Text = "5000";
            this.txtDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblCount);
            this.panel1.Controls.Add(this.chkSubscribe);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtDelay);
            this.panel1.Location = new System.Drawing.Point(16, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(540, 54);
            this.panel1.TabIndex = 20;
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Lucida Console", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Green;
            this.lblCount.Location = new System.Drawing.Point(433, 10);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(97, 27);
            this.lblCount.TabIndex = 22;
            this.lblCount.Text = "    0";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkSubscribe
            // 
            this.chkSubscribe.AutoSize = true;
            this.chkSubscribe.Checked = true;
            this.chkSubscribe.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSubscribe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSubscribe.Location = new System.Drawing.Point(8, 32);
            this.chkSubscribe.Name = "chkSubscribe";
            this.chkSubscribe.Size = new System.Drawing.Size(124, 17);
            this.chkSubscribe.TabIndex = 20;
            this.chkSubscribe.Text = "Subscribe to Hub";
            this.chkSubscribe.UseVisualStyleBackColor = true;
            this.chkSubscribe.CheckedChanged += new System.EventHandler(this.chkLoop_CheckedChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Moccasin;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(186, 111);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 35);
            this.button1.TabIndex = 23;
            this.button1.Text = "Clean List";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnPubishDB
            // 
            this.btnPubishDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnPubishDB.Location = new System.Drawing.Point(436, 111);
            this.btnPubishDB.Name = "btnPubishDB";
            this.btnPubishDB.Size = new System.Drawing.Size(121, 35);
            this.btnPubishDB.TabIndex = 24;
            this.btnPubishDB.Text = "Publish DB";
            this.btnPubishDB.UseVisualStyleBackColor = false;
            this.btnPubishDB.Click += new System.EventHandler(this.btnPubishDB_Click);
            // 
            // LblInfoStatus
            // 
            this.LblInfoStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LblInfoStatus.AutoSize = true;
            this.LblInfoStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInfoStatus.Location = new System.Drawing.Point(146, 441);
            this.LblInfoStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblInfoStatus.Name = "LblInfoStatus";
            this.LblInfoStatus.Size = new System.Drawing.Size(41, 13);
            this.LblInfoStatus.TabIndex = 2;
            this.LblInfoStatus.Text = "label1";
            this.LblInfoStatus.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 480);
            this.Controls.Add(this.btnPubishDB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnPublishText);
            this.Controls.Add(this.btnStopTaskEngine);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnPublishNotam);
            this.Controls.Add(this.lblDiagnosticRsp);
            this.Controls.Add(this.lblDiagnosticReq);
            this.Controls.Add(this.LblInfoStatus);
            this.Controls.Add(this.btnStartTaskEngine);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NES Simulator ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartTaskEngine;
        private System.Windows.Forms.Button btnStopTaskEngine;
        private System.Windows.Forms.Label lblDiagnosticReq;
        private System.Windows.Forms.Label lblDiagnosticRsp;
        private System.Windows.Forms.Button btnPublishNotam;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnPublishText;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDelay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkSubscribe;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnPubishDB;
        private System.Windows.Forms.Label LblInfoStatus;
    }
}

