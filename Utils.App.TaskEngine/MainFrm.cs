﻿using Core.Diagnoistics.SeriLog;

namespace NavCanada.Utils.Apps.TaskEngine
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using NavCanada.Core.TaskEngine.Mto;
    using Core.Data.NotamWiz.EF;
    using Core.Data.NotamWiz.EF.Helpers;
    using Core.Domain.Model.Enums;
    using Core.SqlServiceBroker;
    using Core.SqlServiceBroker.Contracts;
    using Core.TaskEngine.Implementations;
    using System.Linq;
    using NDS.Common.Contracts;
    using System.Collections.Generic;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.TaskEngine.Contracts;
    using NavCanada.Core.Common.Common;

    public partial class Form1 : Form
    {
        private bool _hostTeInitialized;
        private TaskEngineShell _taskEngineHost;
        private readonly ILogger _logger;
        private StreamReader _reader;


        //private ManualResetEvent _completedResetEvent;
        private List<string> _messages = new List<string>();

        private int messageCount;
        private bool _fileReaded;
        private ISubscription _subscription = null;
        private Dictionary<string, int> _startTimes = new Dictionary<string, int>();

        public Form1()
        {
            InitializeComponent();
            LblInfoStatus.Text = "";
            btnStopTaskEngine.Enabled = false;
            btnStartTaskEngine.Enabled = true;

            btnPublishNotam.Enabled = false;
            btnPublishText.Enabled = false;

            toolStripStatusLabel1.Text = "A file needs to be open to execute an action...";
            LblInfoStatus.Text = "TaskEngine is currently stopped!";
            button4.Text = "Open Plain Text File";

            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;

            messageCount = 0;
            lblCount.Text = "    0";

            //Add column header
            listView1.Columns.Add("SEND PACKAGE ID", 200);
            listView1.Columns.Add("RECEIVED PACKAGE ID", 200);
            listView1.Columns.Add("ELAPSE TIME (ms)", 200);

            _logger = new LoggerSeri();
        }

        private void OnStartTaskEngine(object sender, EventArgs e)
        {
            LblInfoStatus.Text = "Starting TaskEngine...";
            btnStartTaskEngine.Enabled = false;

            StartListening();

            chkSubscribe.Enabled = false;
            Task.Run(() =>
            {
                _taskEngineHost = new TaskEngineShell(_logger);
                _taskEngineHost.Start();
            }).ContinueWith(continuation =>
            {
                Thread.Sleep(5000);
                _hostTeInitialized = true;
                Invoke(new Action(() =>
                {
                    LblInfoStatus.Text = "TaskEngine up and running!";
                    btnStopTaskEngine.Enabled = true;
                    btnPublishNotam.Enabled = _fileReaded;
                    btnPublishText.Enabled = _fileReaded;

                    if (_fileReaded)
                    {
                        btnPublishNotam.BackColor = System.Drawing.Color.DarkSeaGreen;
                        btnPublishText.BackColor = System.Drawing.Color.DarkSeaGreen;
                    }
                    else
                    {
                        btnPublishNotam.BackColor = System.Drawing.Color.Gainsboro;
                        btnPublishText.BackColor = System.Drawing.Color.Gainsboro;
                    }
                }));
            });
        }

        private void OnStopTaskEngine(object sender, EventArgs e)
        {
            LblInfoStatus.Text = "Stoping TaskEngine...";
            if (_hostTeInitialized)
            {
                _taskEngineHost.Stop();
            }
            LblInfoStatus.Text = "TaskEngine stopped!";

            btnStartTaskEngine.Enabled = true;
            btnStopTaskEngine.Enabled = false;
            chkSubscribe.Enabled = true;

            if (_subscription != null)
            {
                _subscription.Unsubscribe();
                _subscription = null;
            }

            button1_Click(null, null);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //|ID|TYPE|Q|A|B|C|D|E||F|G|
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                _reader = new StreamReader(openFileDialog1.FileName);
                btnPublishNotam.Enabled = btnStopTaskEngine.Enabled;
                btnPublishText.Enabled = btnStopTaskEngine.Enabled;

                if (btnStopTaskEngine.Enabled)
                {
                    btnPublishNotam.BackColor = System.Drawing.Color.DarkSeaGreen;
                    btnPublishText.BackColor = System.Drawing.Color.DarkSeaGreen;
                }
                else
                {
                    btnPublishNotam.BackColor = System.Drawing.Color.Gainsboro;
                    btnPublishText.BackColor = System.Drawing.Color.Gainsboro;
                }

                var textFileName = Path.GetFileName(openFileDialog1.FileName);
                var lineCount = File.ReadAllLines(openFileDialog1.FileName).Count();

                button4.Text = String.Format("file [{0}] is open", textFileName);
                button4.BackColor = System.Drawing.Color.DarkSeaGreen;

                _fileReaded = true;
                listView1.Items.Clear();
                toolStripStatusLabel1.Text = String.Format("The [{0}] file contains a total of {1} NOTAM", textFileName, lineCount);
            }
        }

        private void btnPublishNotam_Click(object sender, EventArgs e)
        {
            try
            {
                button4.Enabled = false;
                txtDelay.Enabled = false;

                _startTimes.Clear();
                Task.Run(() =>
                {
                    Invoke(new Action(async () =>
                    {
                        while (!_reader.EndOfStream)
                        {
                            var line = _reader.ReadLine();
                            AddSendItemToListView(line);
                            PublishMtoToQueue(CreateSimulatePublishNotamMto(line, true));
                            await Task.Delay(WaitFor(1000));
                        };
                        _reader.Close();
                        UpdateControlsStatus();
                    }));
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected errror: " + ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                button4.Enabled = false;
                txtDelay.Enabled = false;

                _startTimes.Clear();
                Task.Run(() =>
                {
                    Invoke(new Action(async () =>
                    {
                        while (!_reader.EndOfStream)
                        {
                            var line = _reader.ReadLine();
                            AddSendItemToListView(line);
                            PublishMtoToQueue(CreateSimulatePublishNotamMto(line, false));
                            await Task.Delay(WaitFor(1000));
                        };
                        _reader.Close();
                        UpdateControlsStatus();
                    }));
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected errror: " + ex.Message);
            }
        }

        private int WaitFor(int minDelay)
        {
            int delay;
            if (!int.TryParse(txtDelay.Text, out delay)) delay = minDelay;

            return delay;
        }

        private void UpdateControlsStatus()
        {
            btnPublishNotam.Enabled = false;
            btnPublishText.Enabled = false;
            btnPublishNotam.BackColor = System.Drawing.Color.Gainsboro;
            btnPublishText.BackColor = System.Drawing.Color.Gainsboro;
            txtDelay.Enabled = true;

            var textFileName = Path.GetFileName(openFileDialog1.FileName);
            toolStripStatusLabel1.Text = String.Format("The [{0}] was fully proccesed. You need to open a new file again", textFileName);

            button4.BackColor = System.Drawing.Color.Gainsboro;
            button4.Text = "Open Plain Text File";
            button4.Enabled = true;
        }

        private void AddSendItemToListView(string line)
        {
            //Add items in the listview
            string[] arr = new string[3];
            ListViewItem itm;

            var sections = line.Split('|');

            //Add first item
            arr[0] = sections[0];
            arr[1] = "";
            arr[2] = "";
            itm = new ListViewItem(arr);

            _startTimes.Add(sections[0], DateTime.Now.Millisecond);
            listView1.Items.Add(itm);

            Application.DoEvents();
        }

        private void AddReceivedItemToListView(string receivedId, INdsMessage message)
        {
            //Add items in the listview
            if (listView1.Items.Count > 0 && _startTimes.Count > 0)
            {
                foreach (ListViewItem itm in listView1.Items)
                {
                    if (itm.Text == receivedId)
                    {
                        if (_startTimes.ContainsKey(itm.Text))
                        {
                            var startTime = _startTimes[itm.Text];
                            int elapseTime = DateTime.Now.Millisecond - startTime;

                            itm.SubItems[1].Text = receivedId;
                            itm.SubItems[2].Text = elapseTime.ToString();

                            _startTimes.Remove(itm.Text);
                            break;
                        }
                    }
                }
            }
            else
            {
                //Add items in the listview
                string[] arr = new string[3];

                //Add first item
                arr[0] = receivedId;
                arr[1] = receivedId;
                arr[2] = "";
                listView1.Items.Add(new ListViewItem(arr));

                Application.DoEvents();
            }
            Application.DoEvents();
        }

        public void StartListening()
        {
            //if( this.chkSubscribe.Checked )
            //{
            //    _completedResetEvent = new ManualResetEvent(false);
            //    var listener = Task.Factory.StartNew(() =>
            //    {
            //        var dataBusConnStr = ConfigurationManager.AppSettings["HubConnectionString"];
            //        var topic = ConfigurationManager.AppSettings["TopicPath"];

            //        using (var dataBus = new SolaceVMRWrapper(dataBusConnStr))
            //        {
            //            //_subscription = dataBus.ReadFromTopic(topic, "Esteban", "", OnReadMessage);

            //            // wait until StopListening called
            //            _completedResetEvent.WaitOne();
            //            //subscription.Unsubscribe();
            //        }
            //    });
            //}
        }

        private void OnReadMessage(INdsMessage message)
        {
            Invoke(new Action(() =>
            {
                messageCount = messageCount + 1;
                lblCount.Text = messageCount.ToString().PadLeft(5);
                string notamId;
                if (message.Body.Contains("?xml version="))
                {
                    notamId = $"{message["series"]}{message["number"].ToString().PadLeft(4, '0')}/{message["year"].ToString().Substring(2)}";
                }
                else
                {
                    var parts = message.Body.Split('|');
                    notamId = parts[0];
                }
                AddReceivedItemToListView(notamId, message);
            }));
        }

        private MessageTransferObject CreateSimulatePublishNotamMto(string notamTxt, bool xmlFormat)
        {
            var mto = new MessageTransferObject();

            // add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            // create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            var messageId = (int)MessageTransferObjectId.SimulatePublishNotamMsg;

            // create inbound type
            mto.Add(MessageDefs.MessageIdKey, messageId, DataType.Int32);

            // define how the message will be publish (text or xml)
            mto.Add(MessageDefs.RegistrationStatusKey, xmlFormat, DataType.Boolean);

            //add unique identifier
            mto.Add(MessageDefs.EntityIdentifierKey, notamTxt, DataType.String);

            return mto;
        }

        private void PublishMtoToQueue(MessageTransferObject mto)
        {
            try
            {
                var uow = CreatedUow();
                var targetQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                targetQueue.Publish(mto);
                //MessageBox.Show("Successfully pushed Mto to the queue.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error pushing the Mto to the queue. Error details: " + ex.Message);
                // throw;
            }
        }

        private Uow CreatedUow()
        {
            var provider = new RepositoryProvider(new RepositoryFactories());
            return new Uow(provider);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        #region OLD_CODE

        private void OnDisseminateNotamClick(object sender, EventArgs e)
        {
            try
            {
                lblDiagnosticReq.Text = "";
                lblDiagnosticRsp.Text = "";
                var containsId = false;
                //if (!string.IsNullOrEmpty(txtNotamProposalId.Text))
                //{
                //    long id;
                //    if (long.TryParse(txtNotamProposalId.Text, out id))
                //    {
                //        PublishMtoToQueue(CreateDisseminateNotamMto(id));
                //        containsId = true;
                //    }
                //}

                if (!containsId)
                    MessageBox.Show("Error: Notam Id is required and must be a number!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected errror: " + ex.Message);
            }
        }

        private void OnDiagnosticMtoClick(object sender, EventArgs e)
        {
            try
            {
                // sender and receiver queues at the client
                lblDiagnosticReq.Text = "";
                lblDiagnosticRsp.Text = "";
                var uow = CreatedUow();
                var taskEngineQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);

                var ssbFactory = new SqlServiceBrokerFactory();
                var clientQueueSettings = ssbFactory.CreateNewDataStoreQueue(uow.DbContext);
                var clientQueue = new SqlServiceBrokerQueue(uow.DbContext, clientQueueSettings);

                // initiate conversation for the client
                var clientConversation = clientQueue as IConversationHandler;
                clientConversation.InitNewConversation();
                var clientConversationId = clientConversation.SenderConversationId.HasValue ? clientConversation.SenderConversationId.Value : Guid.Empty;

                // send mto over open channel to task engine (no conversation), include client's conversation handle on the mto payload
                var diagnosticMto = CreateDiagnosticMto(clientConversationId.ToString());
                taskEngineQueue.Publish(diagnosticMto);
                lblDiagnosticReq.Text = "'Ping' -> TaskEngine";

                // receive mto at the client side over the conversation originaly created by the client
                var tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                MessageTransferObject receivedMtoAtClient = null;
                var task = Task.Run(() =>
                {
                    receivedMtoAtClient = null;
                    while (receivedMtoAtClient == null)
                    {
                        Thread.Sleep(20);
                        receivedMtoAtClient = clientQueue.Retrieve<MessageTransferObject>();
                    }

                    if (token.IsCancellationRequested)
                        token.ThrowIfCancellationRequested();
                }, token);

                tokenSource.CancelAfter(120000);
                try
                {
                    task.Wait();
                    var response = receivedMtoAtClient[MessageDefs.DiagnosticChatPipeKey];
                    lblDiagnosticRsp.Text = string.Format(" '{0}' <- TaskEngine", response);
                }
                catch (AggregateException)
                {
                    lblDiagnosticReq.Text = "TaskEngine failed to respond.";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected errror: " + ex.Message);
            }
        }

        private void OnRoundTripClick(object sender, EventArgs e)
        {
            try
            {
                lblDiagnosticReq.Text = "";
                lblDiagnosticRsp.Text = "";
                // sender and receiver queues at the client
                var uow = CreatedUow();
                var taskEngineQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                var ssbFactory = new SqlServiceBrokerFactory();
                var clientQueueSettings = ssbFactory.CreateNewDataStoreQueue(uow.DbContext);
                var clientQueue = new SqlServiceBrokerQueue(uow.DbContext, clientQueueSettings);

                // sender and receiver queues at the task engine
                var taskEngineReceiverQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                var taskEngine2ClientQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings);

                // initiate conversation for the client
                var clientConversation = clientQueue as IConversationHandler;
                clientConversation.InitNewConversation();
                var clientConversationId = clientConversation.SenderConversationId.HasValue ? clientConversation.SenderConversationId.Value : Guid.Empty;

                // send mto over open channel to task engine (no conversation), include client's conversation handle on the mto payload
                var sampleMto = new MessageTransferObject();
                sampleMto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);
                sampleMto.Add(MessageDefs.SenderConversationIdKey, clientConversationId.ToString(), DataType.String);
                taskEngineQueue.Publish(sampleMto);

                // receive mto at the task engine
                var receivedMto = taskEngineReceiverQueue.Retrieve<MessageTransferObject>();

                // retrieve conversation handle from mto
                var senderConversationIdFromMto = Guid.Parse(sampleMto[MessageDefs.SenderConversationIdKey].Value.ToString());

                // create response mto
                var responseMto = new MessageTransferObject();
                responseMto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

                // send response mto back to client over the conversation expressed on the incoming mto
                (taskEngine2ClientQueue as IConversationHandler).SendOnConversation(senderConversationIdFromMto);
                taskEngine2ClientQueue.Publish(responseMto);

                // receive mto at the client side over the conversation originaly created by the client
                var receivedMtoAtClient = clientQueue.Retrieve<MessageTransferObject>();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected errror: " + ex.Message);
            }
        }

        private MessageTransferObject CreateDisseminateNotamMto(long id)
        {
            var mto = new MessageTransferObject();

            //add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)MessageTransferObjectId.DisseminateNotam, DataType.Int32);

            //add unique identifier
            mto.Add(MessageDefs.EntityIdentifierKey, id, DataType.Int64);

            var time = DateTime.UtcNow;

            //Message Time
            mto.Add(MessageDefs.ReceivedTimeKey, time, DataType.DateTime);

            //Received Time
            mto.Add(MessageDefs.MessageTimeKey, time, DataType.DateTime);
            return mto;
        }

        private MessageTransferObject CreateDiagnosticMto(string conversationIdKey)
        {
            var mto = new MessageTransferObject();

            //add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String, false);

            //conversation Id
            mto.Add(MessageDefs.SenderConversationIdKey, conversationIdKey, DataType.String);

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)255, DataType.Byte);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)MessageTransferObjectId.Diagnostic, DataType.Int32);

            mto.Add(MessageDefs.DiagnosticChatPipeKey, "Ping", DataType.String);

            return mto;
        }

        #endregion OLD_CODE

        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();

            messageCount = 0;
            lblCount.Text = "    0";
        }

        private void chkLoop_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_subscription != null)
            {
                _subscription.Unsubscribe();
                _subscription = null;
            }
        }

        private void btnPubishDB_Click(object sender, EventArgs e)
        {
            var form = new DisseminateNotamsForm(CreatedUow());
            form.ShowDialog();
        }
    }
}