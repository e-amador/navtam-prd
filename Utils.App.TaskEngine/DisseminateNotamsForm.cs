﻿using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NavCanada.Utils.Apps.TaskEngine
{
    public partial class DisseminateNotamsForm : Form
    {
        readonly IUow _uow;
        readonly ManualResetEvent _manualResetEvent;
        private int _count;

        public DisseminateNotamsForm(IUow uow)
        {
            _uow = uow;
            _manualResetEvent = new ManualResetEvent(false);
            InitializeComponent();
            cbxTimeUnit.SelectedIndex = 0;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            _manualResetEvent.Reset();
            PublishDBTask();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnStop.Enabled = false;
            btnStart.Enabled = true;
            StopDissemination();
        }

        private void PublishDBTask()
        {
            var delay = GetDelay();
            Task.Run(() =>
            {
                try
                {
                    var notams = _uow.NotamRepo.GetAll().Where(ValidSeedNotam).ToList();
                    while (PublishNotams(notams, delay));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            });
        }

        private TimeSpan GetDelay()
        {
            var units = cbxTimeUnit.SelectedIndex;
            var waitCount = (int)numWait.Value;

            if (units == 0)
                return TimeSpan.FromMinutes(waitCount);

            if (units == 1)
                return TimeSpan.FromSeconds(waitCount);

            return TimeSpan.FromMilliseconds(waitCount);
        }

        private bool PublishNotams(IEnumerable<Notam> notams, TimeSpan delay)
        {
            foreach (var notam in notams)
            {
                if (!ValidSeedNotam(notam))
                    continue;

                var cloned = new Notam();
                notam.CopyPropertiesTo(cloned);

                PublishNotam(cloned);

                Invoke(new Action(UpdateProgress));

                if (_manualResetEvent.WaitOne(delay))
                    return false;
            }
            return true;
        }

        private bool ValidSeedNotam(Notam notam)
        {
            return !notam.SeriesChecklist && notam.Type == NotamType.N;
        }

        private void UpdateProgress()
        {
            _count++;
            lblCount.Text = $"{_count}";
        }

        static Random _dayRandom = new Random();
        static Random _minRandom = new Random();

        const int MaxSeriesNumber = 9999;

        private void PublishNotam(Notam notam)
        {
            var today = DateTime.UtcNow;

            notam.Id = Guid.NewGuid();
            notam.RootId = Guid.NewGuid();
            notam.SequenceNumber = 0;

            notam.StartActivity = today.AddMinutes(15 + _minRandom.Next(30)); // +[0 - 30] mins 
            notam.EndValidity = today.AddDays(5 + _dayRandom.Next(10)); // +[0 - 10] days
            notam.EffectiveEndValidity = null;
            notam.Permanent = false;
            notam.Published = DateTime.UtcNow;

            notam.ReplaceNotam = null;
            notam.ReplaceNotamId = null;

            notam.Year = today.Year;

            notam.Urgent = _minRandom.Next(30) == 29;

            _uow.BeginTransaction();
            try
            {
                // pick up the series
                var seriesNumber = _uow.SeriesNumberRepo.GetSeriesNumber(notam.Series[0], notam.Year);
                if (seriesNumber == null)
                    return;

                var number = Math.Max(1, seriesNumber.Number);
                notam.Number = number;
                notam.NotamId = FormatNotamId(notam.Series, notam.Number, notam.Year);

                _uow.NotamRepo.Add(notam);
                _uow.SaveChanges();

                // update the series number
                seriesNumber.Number = number < MaxSeriesNumber ? number + 1 : 1;
                _uow.SeriesNumberRepo.SafeUpdateSeriesNumber(seriesNumber);

                PublishNotamToHub(notam.Id);
                
                _uow.Commit();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                _uow.Rollback();
            }
        }
        static string FormatNotamId(string series, int number, int year)
        {
            return $"{series}{number.ToString().PadLeft(4, '0')}/{year.ToString().PadLeft(2, '0').Substring(2)}";
        }

        private void PublishNotamToHub(Guid id)
        {
            var mto = MtoHelper.CreateMtoMessageWithKeyValue(MessageTransferObjectId.DisseminateNotam, id);
            var targetQueue = new SqlServiceBrokerQueue(_uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            targetQueue.Publish(mto);
        }

        private void DisseminateNotamsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopDissemination();
        }

        private void StopDissemination()
        {
            _manualResetEvent.Set();
            _manualResetEvent.WaitOne(TimeSpan.FromSeconds(2));
        }
    }
}
