﻿namespace NavCanada.Core.SqlServiceBroker.Contracts
{
    using System.Data.Entity;

    using Core.SqlServiceBroker;

    public interface ISqlServiceBrokerFactory
    {
        SqlServiceBrokerQueueSettings CreateNewDataStoreQueue(DbContext dbContext);

        void DeleteDataStoreQueue(DbContext dbContext, SqlServiceBrokerQueueSettings queueSettings);
    }
}