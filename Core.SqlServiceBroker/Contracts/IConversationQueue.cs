﻿namespace NavCanada.Core.SqlServiceBroker.Contracts
{
    using Common.Contracts;

    public interface IConversationQueue : IQueue, IConversationHandler
    {
    }
}