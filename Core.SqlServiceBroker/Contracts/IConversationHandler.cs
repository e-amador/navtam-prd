﻿namespace NavCanada.Core.SqlServiceBroker.Contracts
{
    using System;

    public interface IConversationHandler
    {
        Guid? SenderConversationId
        {
            get;
        }

        Guid? TargetConversationId
        {
            get;
        }

        void InitNewConversation();

        void ContinueConversation(Guid senderConversationId, Guid targetConversationId);

        void SendOnConversation(Guid senderConversationId);

        void ReceiveOnConversation(Guid targetConversationId);

        void EndConversation();
    }
}