﻿namespace NavCanada.Core.SqlServiceBroker
{
    using System.Data.Entity;

    using Contracts;

    public class SqlServiceBrokerFactory : ISqlServiceBrokerFactory
    {
        public SqlServiceBrokerQueueSettings CreateNewDataStoreQueue(DbContext dbContext)
        {
            var result = SqlServiceBroker.CreateNewQueueAndServices(dbContext);
            return result;
        }

        public void DeleteDataStoreQueue(DbContext dbContext, SqlServiceBrokerQueueSettings queueSettings)
        {
            SqlServiceBroker.DeleteQueueAndServices(dbContext, queueSettings);
        }
    }
}