﻿namespace NavCanada.Core.SqlServiceBroker
{
    using Common.Contracts;

    public class ClusterQueues : IClusterQueues
    {
        public ClusterQueues(IQueue taskEngineQueue, IQueue initiatorQueue )
        {
            TaskEngineQueue = taskEngineQueue;
            InitiatorQueue = initiatorQueue;
        }

        public IQueue TaskEngineQueue
        {
            get;
            private set;
        }

        public IQueue InitiatorQueue
        {
            get;
            private set;
        }
    }
}