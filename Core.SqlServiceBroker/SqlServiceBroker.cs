﻿namespace NavCanada.Core.SqlServiceBroker
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;


    internal static class SqlServiceBroker
    {
        public static Guid BeginConversation(DbContext dbContext, string initiatorServiceName, string targetServiceName, string messageContractName)
        {
            return BeginConversationInternal(dbContext, initiatorServiceName, targetServiceName, messageContractName, null, null);
        }

        public static Guid BeginConversation(DbContext dbContext, string initiatorServiceName, string targetServiceName, string messageContractName, int lifetime)
        {
            return BeginConversationInternal(dbContext, initiatorServiceName, targetServiceName, messageContractName, lifetime, null);
        }

        public static Guid BeginConversation(DbContext dbContext, string initiatorServiceName, string targetServiceName, string messageContractName, bool encryption)
        {
            return BeginConversationInternal(dbContext, initiatorServiceName, targetServiceName, messageContractName, null, encryption);
        }

        public static Guid BeginConversation(DbContext dbContext, string initiatorServiceName, string targetServiceName, string messageContractName, int lifetime, bool encryption)
        {
            return BeginConversationInternal(dbContext, initiatorServiceName, targetServiceName, messageContractName, lifetime, encryption);
        }

        public static void EndConversation(DbContext dbContext, Guid conversationHandle)
        {
            EndConversation(dbContext, conversationHandle, false);
        }

        public static void EndConversation(DbContext dbContext, Guid conversationHandle, bool withCleanup)
        {
            EndConversationInternal(dbContext, conversationHandle, false, null, null, withCleanup);
        }

        public static void EndConversation(DbContext dbContext, Guid conversationHandle, int errorCode, string errorDescription)
        {
            EndConversationInternal(dbContext, conversationHandle, true, errorCode, errorDescription, false);
        }

        public static void Send(DbContext dbContext, Guid conversationHandle, string messageType)
        {
            Send(dbContext, conversationHandle, messageType, null);
        }

        public static void Send(DbContext dbContext, Guid conversationHandle, string messageType, byte[] body)
        {
            SendInternal(dbContext, conversationHandle, messageType, body);
        }

        public static Message Receive(DbContext dbContext, string queueName)
        {
            return ReceiveInternal(dbContext, queueName, null, false, null);
        }

        public static Message Receive(DbContext dbContext, string queueName, Guid conversationHandle)
        {
            return ReceiveInternal(dbContext, queueName, conversationHandle, false, null);
        }

        public static Message WaitAndReceive(DbContext dbContext, string queueName, int waitTimeout)
        {
            return ReceiveInternal(dbContext, queueName, null, true, waitTimeout);
        }

        public static Message WaitAndReceive(DbContext dbContext, string queueName, Guid conversationHandle, int waitTimeout)
        {
            return ReceiveInternal(dbContext, queueName, conversationHandle, true, waitTimeout);
        }

        public static int QueryMessageCount(DbContext dbContext, string queueName, string messageContractName)
        {
            return QueryMessageCountInternal(dbContext, queueName, messageContractName);
        }

        private static Guid BeginConversationInternal(DbContext dbContext, string initiatorServiceName, string targetServiceName, string messageContractName, int? lifetime, bool? encryption)
        {
            var query = new StringBuilder();
            query.Append("BEGIN DIALOG @ch FROM SERVICE [" + initiatorServiceName + "] TO SERVICE @ts ON CONTRACT [" + messageContractName + "] WITH ENCRYPTION = ");
            if (encryption.HasValue && encryption.Value)
            {
                query.Append("ON ");
            }
            else
            {
                query.Append("OFF ");
            }
            if (lifetime.HasValue && lifetime.Value > 0)
            {
                query.Append(", LIFETIME = ");
                query.Append(lifetime.Value);
                query.Append(' ');
            }
            var handleParam = new SqlParameter("@ch", SqlDbType.UniqueIdentifier);
            handleParam.Direction = ParameterDirection.Output;
            var tsParam = new SqlParameter("@ts", targetServiceName);
            var count = dbContext.Database.ExecuteSqlCommand(query.ToString(), handleParam, tsParam);
            return (Guid)handleParam.Value;
        }

        private static void EndConversationInternal(DbContext dbContext, Guid conversationHandle, bool withError, int? errorCode, string errorDescription, bool withCleanup)
        {
            var query = new StringBuilder();
            var parameters = new List<SqlParameter>();
            query.Append("END CONVERSATION @ch");
            parameters.Add(new SqlParameter("@ch", conversationHandle));
            if (withError)
            {
                query.Append(" WITH ERROR = @ec DESCRIPTION = @desc");
                parameters.Add(new SqlParameter("@ec", errorCode));
                parameters.Add(new SqlParameter("@desc", errorDescription));
            }
            else if (withCleanup)
            {
                query.Append(" WITH CLEANUP");
            }
            var count = dbContext.Database.ExecuteSqlCommand(query.ToString(), parameters.ToArray());
        }

        public static Guid GetTargetConversationHandle(DbContext dbContext, string initiatorServiceName, string targetServiceName, string messageContractName, Guid initiatorConversationHadle)
        {
            const string query = @"select 
                                        conversation_handle 
                                   from 
                                        sys.conversation_endpoints 
                                   where 
                                        conversation_handle <> @initiator_handle AND 
                                        conversation_id = (
                                            select 
                                                conversation_id 
                                            from 
                                                sys.conversation_endpoints 
                                            where 
                                                conversation_handle = @initiator_handle)";
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@initiator_handle", SqlDbType.UniqueIdentifier)
                {
                    Value = initiatorConversationHadle
                }
            };
            return dbContext.Database.SqlQuery<Guid>(query, parameters.ToArray()).FirstOrDefault();
        }

        public static SqlServiceBrokerQueueSettings CreateNewQueueAndServices(DbContext dbContext)
        {
            var guid = Guid.NewGuid().ToString();
            var senderQueueName = guid + "-SendQueue";
            var result = new SqlServiceBrokerQueueSettings
            {
                ReceiveQueueName = guid + "-ReceiveQueue",
                SenderServiceName = "//navcanada.se.notamwiz/" + guid + "-SenderService",
                ReceiverServiceName = "//navcanada.se.notamwiz/" + guid + "-ReceiveService"
            };
            var query = "CREATE QUEUE [" + senderQueueName + "]";
            dbContext.Database.ExecuteSqlCommand(query);
            query = "CREATE SERVICE [" + result.SenderServiceName + "] ON QUEUE [" + senderQueueName + "]([" + result.ContractName + "])";
            dbContext.Database.ExecuteSqlCommand(query);
            query = "CREATE QUEUE [" + result.ReceiveQueueName + "]";
            dbContext.Database.ExecuteSqlCommand(query);
            query = "CREATE SERVICE [" + result.ReceiverServiceName + "] ON QUEUE [" + result.ReceiveQueueName + "]([" + result.ContractName + "])";
            dbContext.Database.ExecuteSqlCommand(query);
            query = "ALTER QUEUE [" + result.ReceiveQueueName + "] WITH POISON_MESSAGE_HANDLING ( STATUS = OFF )";
            dbContext.Database.ExecuteSqlCommand(query);
            return result;
        }

        public static void DeleteQueueAndServices(DbContext dbContext, SqlServiceBrokerQueueSettings settings)
        {
            var query = "DROP SERVICE [" + settings.ReceiverServiceName + "]";
            dbContext.Database.ExecuteSqlCommand(query);
            query = "DROP SERVICE [" + settings.SenderServiceName + "]";
            dbContext.Database.ExecuteSqlCommand(query);
            var senderQueueName = settings.ReceiveQueueName.Substring(0, settings.ReceiveQueueName.LastIndexOf('-')) + "-SendQueue";
            query = "DROP QUEUE [" + senderQueueName + "]";
            dbContext.Database.ExecuteSqlCommand(query);
            query = "DROP QUEUE [" + settings.ReceiveQueueName + "]";
            dbContext.Database.ExecuteSqlCommand(query);
        }

        private static void SendInternal(DbContext dbContext, Guid conversationHandle, string messageType, byte[] body)
        {
            var query = new StringBuilder();
            var parameters = new List<SqlParameter>();
            query.Append("SEND ON CONVERSATION @ch MESSAGE TYPE @mt ");
            parameters.Add(new SqlParameter("@ch", conversationHandle));
            parameters.Add(new SqlParameter("@mt", messageType));
            if (body != null && body.Length > 0)
            {
                query.Append(" (@msg)");
                parameters.Add(new SqlParameter("@msg", body));
            }
            dbContext.Database.ExecuteSqlCommand(query.ToString(), parameters.ToArray());
        }

        private static Message ReceiveInternal(DbContext dbContext, string queueName, Guid? conversationHandle, bool wait, int? waitTimeout)
        {
            var query = new StringBuilder();
            var parameters = new List<SqlParameter>();
            if (wait && waitTimeout.HasValue && waitTimeout.Value > 0)
            {
                query.Append("WAITFOR(");
            }
            query.Append("RECEIVE TOP(1) ");
            query.Append("conversation_group_id as ConversationGroupId, conversation_handle as ConversationHandle, " + "message_sequence_number as MessageSequenceNumber, service_name as ServiceName, service_contract_name as ServiceContractName, " + "message_type_name as MessageTypeName, validation, message_body as Body " + "FROM [");
            query.Append(queueName);
            query.Append("]");
            if (conversationHandle != null && conversationHandle.Value != Guid.Empty)
            {
                query.Append(" WHERE conversation_handle = @ch");
                parameters.Add(new SqlParameter("@ch", conversationHandle.Value));
            }
            if (wait && waitTimeout.HasValue && waitTimeout.Value > 0)
            {
                query.Append("), TIMEOUT @to");
                parameters.Add(new SqlParameter("@to", waitTimeout.Value));
            }
            try
            {
                return dbContext.Database.SqlQuery<Message>(query.ToString(), parameters.ToArray()).FirstOrDefault();
            }
            catch (TimeoutException)
            {
                return null;
            }
        }

        private static int QueryMessageCountInternal(DbContext dbContext, string queueName, string messageContractName)
        {
            return dbContext.Database.ExecuteSqlCommand("SELECT COUNT(*) FROM " + queueName + " WHERE message_type_name = @messageContractName", new SqlParameter("@messageContractName", messageContractName));
        }
    }
}