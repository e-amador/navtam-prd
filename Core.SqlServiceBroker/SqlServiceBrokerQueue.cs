﻿namespace NavCanada.Core.SqlServiceBroker
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;

    using Common.Exceptions;
    using Contracts;

    /// <summary>
    /// IQueue implementation with the use of SQLServer Service Broker
    /// </summary>
    public class SqlServiceBrokerQueue : IConversationQueue
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="settings"></param>
        /// <remarks>This method is called at a high frequency, multiple times per second</remarks>
        public SqlServiceBrokerQueue(DbContext dbContext, SqlServiceBrokerQueueSettings settings)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext", "dbContext cannot be null");
            }
            this._dbContext = dbContext;
            if (settings == null)
            {
                throw new ArgumentNullException("settings", "settings cannot be null");
            }
            if (string.IsNullOrWhiteSpace(settings.ContractName) || string.IsNullOrWhiteSpace(settings.MessageTypeName))
            {
                throw new ArgumentException("Invalid queue settings. At a minimum ContractName and MessageTypeName are required.", "settings");
            }
            this._settings = settings;
            if (this._settings.ReadWaitTimeout > 0)
            {
                this._dbContext.Database.CommandTimeout = 2 * this._settings.ReadWaitTimeout;
            }
        }

        public void Publish<T>(T message) where T : class
        {
            //this._logger.DebugFormat("Publishing message {0} to Service Broker Queue... ", message);
            if (!typeof(T).IsSerializable && !(typeof(ISerializable).IsAssignableFrom(typeof(T))))
            {
                throw new ArgumentException("A serializable Type is required", "message");
            }
            byte[] data;
            using (var ms = new MemoryStream())
            {
                var fmt = new BinaryFormatter(); // DHA-1104 consider using a different serializer, BinaryFormatter saves metadata information with the payload
                fmt.Serialize(ms, message);
                data = ms.ToArray();
            }
            if (!this._initiatorConversationHandle.HasValue) // if this is the first message sent for this queue session, start the dialog
            {
                this._initiatorConversationHandle = SqlServiceBroker.BeginConversation(this._dbContext, this._settings.SenderServiceName, this._settings.ReceiverServiceName, this._settings.ContractName);
            }
            SqlServiceBroker.Send(this._dbContext, this._initiatorConversationHandle.Value, this._settings.MessageTypeName, data);
        }

        public T Retrieve<T>() where T : class
        {
            var msg = this._targetConversationHandle.HasValue ? SqlServiceBroker.WaitAndReceive(this._dbContext, this._settings.ReceiveQueueName, this._targetConversationHandle.Value, this._settings.ReadWaitTimeout) : SqlServiceBroker.WaitAndReceive(this._dbContext, this._settings.ReceiveQueueName, this._settings.ReadWaitTimeout);
            return msg == null ? null : DecodeMessage<T>(msg);
        }

        public T RetrieveNoWait<T>() where T : class
        {
            var msg = this._targetConversationHandle.HasValue ? SqlServiceBroker.Receive(this._dbContext, this._settings.ReceiveQueueName, this._targetConversationHandle.Value) : SqlServiceBroker.Receive(this._dbContext, this._settings.ReceiveQueueName);
            return msg == null ? null : DecodeMessage<T>(msg);
        }

        public IList<T> RetrieveBatchWithInternalTransaction<T>(int maxCount) where T : class
        {
            using (var transaction = this._dbContext.Database.BeginTransaction())
            {
                try
                {
                    var result = RetrieveBatchInternal<T>(maxCount);
                    transaction.Commit();
                    return result;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public IList<T> RetrieveBatchWithInternalTransactionWithoutConversation<T>(int maxCount) where T : class
        {
            using (var transaction = this._dbContext.Database.BeginTransaction())
            {
                try
                {
                    var result = RetrieveBatchInternalWithoutConversation<T>(maxCount);
                    transaction.Commit();
                    return result;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public IList<T> RetrieveBatch<T>(int maxCount) where T : class
        {
            return RetrieveBatchInternal<T>(maxCount);
        }

        private IList<T> RetrieveBatchInternal<T>(int maxCount) where T : class
        {
            var msg = this._targetConversationHandle.HasValue ? SqlServiceBroker.WaitAndReceive(this._dbContext, this._settings.ReceiveQueueName, this._targetConversationHandle.Value, this._settings.ReadWaitTimeout) : SqlServiceBroker.WaitAndReceive(this._dbContext, this._settings.ReceiveQueueName, this._settings.ReadWaitTimeout);
            if (msg == null)
            {
                return new List<T>();
            }
            var result = new List<T>();
            if (!this._targetConversationHandle.HasValue)
            {
                this._targetConversationHandle = msg.ConversationHandle;
            }
            var countRetrieved = 0;
            do
            {
                result.Add(DecodeMessage<T>(msg));
                msg = SqlServiceBroker.Receive(this._dbContext, this._settings.ReceiveQueueName, this._targetConversationHandle.Value);
            }
            while (msg != null && (maxCount <= 0 || ++countRetrieved < maxCount));
            return result;
        }

        private IList<T> RetrieveBatchInternalWithoutConversation<T>(int maxCount) where T : class
        {
            var msg = SqlServiceBroker.WaitAndReceive(this._dbContext, this._settings.ReceiveQueueName, this._settings.ReadWaitTimeout);
            var result = new List<T>();
            if (msg == null)
            {
                return result;
            }
            var countRetrieved = 0;
            do
            {
                result.Add(DecodeMessage<T>(msg));
                msg = SqlServiceBroker.Receive(this._dbContext, this._settings.ReceiveQueueName);
            }
            while (msg != null && (maxCount <= 0 || ++countRetrieved < maxCount));
            return result;
        }

        private T DecodeMessage<T>(Message msg) where T : class
        {
            try
            {
                using (var ms = msg.CreateBodyStream)
                {
                    var fmt = new BinaryFormatter(); // DHA-1104 consider using a different serialzer, BinaryFormatter saves metadata information with the payload
                    return fmt.Deserialize(ms) as T;
                }
            }
            catch (Exception ex)
            {
                throw new MessageDeserializationException("Unable to deserialize this message", ex, msg.Body);
            }
        }

        #region IConversationHandler implementation

        public void InitNewConversation()
        {
            //this._logger.DebugFormat("Initialising new SSB conversation {0} and {1}... ", this._settings.SenderServiceName, this._settings.ReceiverServiceName);
            if (this._initiatorConversationHandle.HasValue || this._targetConversationHandle.HasValue)
            {
                throw new InvalidOperationException("This queue has already been used with a different conversation or outside of a conversation. Starting a new conversation in this queue's state is not supported");
            }
            this._initiatorConversationHandle = SqlServiceBroker.BeginConversation(this._dbContext, this._settings.SenderServiceName, this._settings.ReceiverServiceName, this._settings.ContractName);
            this._inConversation = true;
        }

        public void ContinueConversation(Guid senderConversationId, Guid targetConversationId)
        {
            //this._logger.Debug("Continuing Service Broker Queue conversation... ");
            if (this._initiatorConversationHandle.HasValue || this._targetConversationHandle.HasValue)
            {
                throw new InvalidOperationException("This queue has already been used with a different conversation or outside of a conversation. continuing a conversation in this queue's state is not supported");
            }
            this._initiatorConversationHandle = senderConversationId;
            this._targetConversationHandle = targetConversationId;
            this._inConversation = true;
        }

        public void SendOnConversation(Guid senderConversationId)
        {
            //this._logger.Debug("Sending on conversation. ");
            if (this._initiatorConversationHandle.HasValue)
            {
                throw new InvalidOperationException("This queue has already been used with a different conversation or outside of a conversation. continuing a conversation in this queue's state is not supported");
            }
            this._initiatorConversationHandle = senderConversationId;
            this._inConversation = true;
        }

        public void ReceiveOnConversation(Guid targetConversationId)
        {
            if (this._targetConversationHandle.HasValue)
            {
                throw new InvalidOperationException("This queue has already been used with a different conversation or outside of a conversation. continuing a conversation in this queue's state is not supported");
            }
            this._targetConversationHandle = targetConversationId;
            this._inConversation = true;
        }

        public void EndConversation()
        {
            //this._logger.Debug("End conversation. ");
            if (!this._inConversation)
            {
                throw new InvalidOperationException("This queue is not part of a conversation");
            }
            if (this._initiatorConversationHandle.HasValue)
            {
                SqlServiceBroker.EndConversation(this._dbContext, this._initiatorConversationHandle.Value);
            }
            this._initiatorConversationHandle = this._targetConversationHandle = null;
            this._inConversation = false;
        }

        public Guid? SenderConversationId
        {
            get
            {
                return this._inConversation ? this._initiatorConversationHandle : null;
            }
        }

        public Guid? TargetConversationId
        {
            get
            {
                return this._inConversation ? this._targetConversationHandle : null;
            }
        }

        //private readonly ILog _logger = LogManager.GetLogger(typeof(SqlServiceBrokerQueue));

        private readonly DbContext _dbContext;
        private readonly SqlServiceBrokerQueueSettings _settings;
        private Guid? _initiatorConversationHandle;
        private Guid? _targetConversationHandle;
        private bool _inConversation;

        #endregion IConversationHandler implementation
    }
}