﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class OrganizationListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}