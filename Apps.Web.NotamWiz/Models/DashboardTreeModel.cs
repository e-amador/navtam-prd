﻿using System.Collections.Generic;
namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public enum NodeType : int
    {
        Root = 0,
        Category = 1,
        Nsd = 2
    }
    public class DashboardTreeModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Expanded { get; set; }
        public NodeType NodeType { get; set; }  
        public List<DashboardTreeModel> Items { get; set; }
    }
}