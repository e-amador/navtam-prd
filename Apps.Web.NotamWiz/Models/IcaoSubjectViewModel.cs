﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class IcaoSubjectViewModel
    {

        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string NameFrench { get; set; }

        [Required]
        public string EntityCode { get; set; }

        [Required]
        [StringLength(2)]
        public string Code { get; set; }

        [Required]
        [StringLength(1)]
        public string Scope { get; set; }

        public bool AllowItemAChange { get; set; }
        public bool AllowScopeChange { get; set; }
        public bool AllowSeriesChange { get; set; }
    }
}