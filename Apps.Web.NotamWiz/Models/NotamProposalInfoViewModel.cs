﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System;

    public class NotamProposalInfoViewModel
    {
        public long Id { get; set; }

        public string OwnerId { get; set; }
        public string Owner { get; set; }
        public DateTime CreationDate { get; set; }
        public string Note { get; set; }
        public long ProposalId { get; set; }
    }
}
