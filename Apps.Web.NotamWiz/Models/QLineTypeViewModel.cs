﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using NavCanada.Core.Domain.Model.Enums;

    public class QLineTypeViewModel
    {
        public string FIR { get; set; }
        public string Code23 { get; set; }
        public string Code45 { get; set; }
        public TrafficType Traffic { get; set; }
        public PurposeType Purpose { get; set; }
        public ScopeType Scope { get; set; }
        public int Lower { get; set; }
        public int Upper { get; set; }
    }
}
