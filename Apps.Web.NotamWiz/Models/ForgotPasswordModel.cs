﻿using System.ComponentModel.DataAnnotations;

namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class ForgotPasswordModel
    {

        [Required]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
    }
}