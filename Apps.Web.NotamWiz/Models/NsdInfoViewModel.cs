﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class NsdInfoViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public float Version { get; set; }
        public string FileLocation { get; set; }
        public bool HasView { get; set; }
        public string ForSubjectType { get; set; }
    }
}