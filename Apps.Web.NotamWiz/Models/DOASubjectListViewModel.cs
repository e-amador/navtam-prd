﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class DOASubjectListViewModel
    {

       public long Id { get; set; }

      //  [Display(Name = "DN_Group_name", ResourceType = typeof(NOTAMWiz.Resources.Models.Account.Account))]

       public string Type { get; set; }
        
        public string Name { get; set; }

        public string Description { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; } 

    }
}