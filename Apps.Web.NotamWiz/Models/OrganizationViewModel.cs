﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class OrganizationViewModel
    {

        public long Id { get; set; }
        public bool IsReadOnlyOrg { get; set; }

        
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]        
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [Phone]
        public string Telephone { get; set; }

        [Required]
        public string TypeOfOperation { get; set; }

        public string HeadOffice { get; set; }

        public bool IsFic { get; set; }

        public string EmailDistribution { get; set; }

        public long? AssignedDoaId { get; set; }
        public string AssignedDoa { get; set; }

        public string FicId
        {
            get;
            set;
        }

        public SdoSubjectViewModel Fic
        {
            get;
            set;
        }
    }
}