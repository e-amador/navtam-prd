﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class NsdVersionViewModel
    {
        public long Id { get; set; }

        public long NsdId { get; set; }

        [Required]
        public string SupportedSubjects { get; set; }

        [Required]
        public float Version { get; set; }

        public string Description { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string ReleasedBy { get; set; }

        [Required]
        public string FileLocation { get; set; }

        public bool HasView { get; set; }

        public string SubjectsFilter { get; set; }

    }

}