﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ResetPasswordModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = @"The Password must have one capital, one lower case letter, one special character and one numerical character. It can not start with a special character or a digit.")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public string ConfirmationToken { get; set; }

        public bool PasswordHasExpired { get; set; }
    }
}