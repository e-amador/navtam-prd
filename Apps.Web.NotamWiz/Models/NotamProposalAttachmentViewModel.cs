﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System;

    public class NotamProposalAttachmentViewModel
    {
        public Guid Id { get; set; }

        public string OwnerId { get; set; }
        public string Owner { get; set; }

        public DateTime CreationDate { get; set; }

        public string FileName { get; set; }

        //public byte[] Attachment { get; set; }
    }
}
