﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class UserRegistrationManagementViewModel
    {
        public UserManagementViewModel userVM { get; set; }
        public OrganizationViewModel orgVM { get; set; }
        public string SelectedOrganizationName { get; set; }
        public long DOAId { get; set; }
        public string AirdromeMid { get; set; }

        public UserRegistrationManagementViewModel()
        {
            userVM = new UserManagementViewModel();
            orgVM = new OrganizationViewModel();
        }
    }
}