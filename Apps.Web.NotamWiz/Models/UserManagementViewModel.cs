﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// User Management View Model to facilitate the management of users
    /// </summary>
    
    public class UserManagementViewModel
    {
        public string UserId { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "'Password' must contains only alphanumeric characters and be between 8 and 16 characters long.")]
        public string Password { get; set; }

        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "'ConfirmPassword' must contains only alphanumeric characters and be between 8 and 16 characters long.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Phone]
        public string Telephone { get; set; }

        [Phone]
        public string Fax { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Position { get; set; }

        public bool IsApproved { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsDisabled { get; set; }

        public long? UserOrgId { get; set; }

        public string Reason { get; set; }

        public string RegistrationOrgName { get; set; }

        public string RegistrationOrgTelephone { get; set; }

        public string RegistrationOrgAddress { get; set; }

        public string RegistrationOrgEmail { get; set; }

        public string RegistrationOrgTypeOfOperation { get; set; }

        public string RegistrationOrgHeadOffice { get; set; }

        public List<string> UserRoles { get; set; }

    }
}