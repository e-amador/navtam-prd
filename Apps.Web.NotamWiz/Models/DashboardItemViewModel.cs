﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using NavCanada.Core.Domain.Model.Enums;

    public class DashboardItemViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool HasChildren { get; set; }

        public DashboardItemType Type { get; set; }
        public string SubjectType { get; set; }
        public string SubjectId { get; set; }
    }
}