﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class StatisticViewModel
    {

        public string StatusName { get; set; }

        public int StatusNumber { get; set; }

        public StatisticViewModel(string statusName, int statusNumber)
        {
            StatusName = statusName;
            StatusNumber = statusNumber;
        }
    }
}