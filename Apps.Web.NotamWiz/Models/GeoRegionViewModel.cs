﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using NavCanada.Core.Domain.Model.Dtos;

    public class GeoRegionViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public MultiPolygonDto Geo { get; set; }
    }
}