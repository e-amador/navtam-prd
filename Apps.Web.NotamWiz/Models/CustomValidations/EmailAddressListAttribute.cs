﻿namespace NavCanada.Applications.NotamWiz.Web.Models.CustomValidations
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    [AttributeUsage(AttributeTargets.Property)]
    public sealed class EmailAddressListAttribute : ValidationAttribute
    {
        private const string DefaultError = "'{0}' contains an invalid email address.";
        public EmailAddressListAttribute()
            : base(DefaultError) //
        {
        }

        public override bool IsValid(object value)
        {
            var emailAttribute = new EmailAddressAttribute();
            var list = value as IList<string>;
            return (list != null && list.All(emailAttribute.IsValid));
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(ErrorMessageString, name);
        }
    }

}