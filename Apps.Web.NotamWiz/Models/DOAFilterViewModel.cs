﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{

    public class DOAFilterViewModel
    {
        public long Id { get; set; }

        public long ParentDOAId { get; set; }

        public int Index { get; set; }

        public int Type { get; set; }

        public int Effect { get; set; }

        public string Value { get; set; }
    }
}