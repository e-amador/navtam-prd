﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public enum ManageMessageType
    {
        ChangePasswordSuccess,
        SetPasswordSuccess,
        RemoveLoginSuccess,
        ChangeDefaultLanguageSuccess
    }

    public class RegisterExternalLogin
    {
        [Required]
        [Display(Name = @"UserName")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPassword
    {
        [Required]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 8)]
        [DataType(DataType.Password)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = @"The Password must have one capital, one lower case letter, one special character and one numerical character. It can not start with a special character or a digit.")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
    }

    public class Login
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

    }

    /// <summary>
    /// Used for System Admin and CCS to add users in the user managment page.   
    /// </summary>
    public class AddUser
    {

        [Required]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 8)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|:"";',{ ]).{8,99})$", ErrorMessage = @"The Password must have one capital, one lower case letter, one special character and one numerical character. It can not start with a special character or a digit.")]
        [Required]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public long? OrganizationId { get; set; }

        [Required(ErrorMessage = "Email Address is required.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Address (if different from Organization)")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Title/Position")]
        public string Position { get; set; }

        [Display(Name = "Fax Number")]
        public string Fax { get; set; }

        [Required]
        [Display(Name = "Telephone")]
        public string Telephone { get; set; }
    }

    public class Register
    {

        [Required]
        [Display(Name = "User name *")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "'Password' must contains only alphanumeric characters and be between 8 and 16 characters long.")]
        [Required]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "'ConfirmPassword' must contains only alphanumeric characters and be between 8 and 16 characters long.")]
        [Display(Name = "Confirm password *")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public long? OrganizationId { get; set; }

        ///only validates if the required tags are here.
        //[Required(ErrorMessageResourceName= typeof @DbRes.T("Account", "EmailAddress")]
        //[Required(ErrorMessageResourceName = (@DbRes.T("EmailAddress", "Account")))]
        [Required(ErrorMessage = @"Email Address is required.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address *")]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "First name *")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name *")]
        public string LastName { get; set; }

        [Display(Name = "Address (if different from Organization)")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Title/Position *")]
        public string Position { get; set; }

        [Display(Name = "Fax number")]
        public string Fax { get; set; }

        [Required]
        [Display(Name = "Telephone *")]
        public string Telephone { get; set; }

        [Display(Name = "Why you need access to the application")]
        public string Reason { get; set; }

        [Required]
        [Display(Name = "Name of Organization *")]
        public string OrgName { get; set; }

        [Required]
        [Display(Name = "Address of Organization *")]
        public string OrgAddress { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email of Organization *")]
        [EmailAddress]
        public string OrgEmailAddress { get; set; }

        [Required]
        [Display(Name = "Telephone number of Organization *")]
        public string OrgTelephone { get; set; }

        [Required]
        [Display(Name = "Type of Operation *")]
        public string TypeOfOperation { get; set; }

        [Display(Name = "Head office information (if different from above)")]
        public string HeadOffice { get; set; }
    }


    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
