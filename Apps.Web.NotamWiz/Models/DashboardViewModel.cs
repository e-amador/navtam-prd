﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class DashboardViewModel
    {
        public ArrayList Statistics { get; set; }

        public DashboardViewModel()
        {
            Statistics = new ArrayList();
            for (int i = 0; i < 20; i++)
            {
                Statistics.Add(new StatisticViewModel("Status " + i, i+5));
            }
        }

        public IEnumerable<StatisticViewModel> GetStatistics()
        {
            return Statistics.Cast<StatisticViewModel>();
        }
    }
}