﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using Core.Domain.Model.Enums;

    using Westwind.Globalization;

    public class NotamProposalViewModel
    {
        public NotamProposalViewModel()
        {
            Type = NotamType.N.ToString();
            Status = NotamProposalStatusCode.Draft;
            Traffic = TrafficType.I.ToString();
            Purpose = PurposeType.B.ToString();
            Scope = ScopeType.A.ToString();
            NotamProposalsInfos = new List<NotamProposalInfoViewModel>();
            NotamProposalAttachments = new List<NotamProposalAttachmentViewModel>();
        }

        public string Code23
        {
            get;
            set;
        }

        public string Code45
        {
            get;
            set;
        }

        public bool ContainFreeText
        {
            get;
            set;
        }

        public string Coordinates
        {
            get;
            set;
        }

        public DateTime? EndValidity
        {
            get;
            set;
        }

        public bool Estimated
        {
            get;
            set;
        }

        public string Fir
        {
            get;
            set;
        }

        public long Id
        {
            get;
            set;
        }

        public int InternalStatus
        {
            get
            {
                if (EndValidity != null && (Status == NotamProposalStatusCode.Submitted || Status == NotamProposalStatusCode.ModifiedAndSubmitted))
                {
                    int SoontoExpireHrsDifference = Convert.ToInt16(ConfigurationManager.AppSettings.Get("SoontoExpireHrsDifference"));
                    var hours = ((DateTime)EndValidity - DateTime.Now).TotalHours;
                    if (Estimated && (EndValidity <= DateTime.Now || hours <= SoontoExpireHrsDifference))
                    {
                        return (int)NotamProposalStatusCode.SoontoExpire;
                    }
                }
                if (Status == NotamProposalStatusCode.Pending || Status == NotamProposalStatusCode.QueuePending || Status == NotamProposalStatusCode.Queued)
                {
                    return (int)NotamProposalStatusCode.Queued;
                }
                if (Status == NotamProposalStatusCode.Rejected)
                {
                    return (int)NotamProposalStatusCode.Rejected;
                }
                if (Status == NotamProposalStatusCode.Expired)
                {
                    return (int)NotamProposalStatusCode.Expired;
                }
                if (Status == NotamProposalStatusCode.WithdrawnPending)
                {
                    return (int)NotamProposalStatusCode.WithdrawnPending;
                }
                if (Status == NotamProposalStatusCode.Withdrawn)
                {
                    return (int)NotamProposalStatusCode.Withdrawn;
                }
                if (Status == NotamProposalStatusCode.Draft || Status == NotamProposalStatusCode.Undefined)
                {
                    return (int)NotamProposalStatusCode.Draft;
                }
                if (Status == NotamProposalStatusCode.Submitted && Type == "C")
                {
                    return ModifiedByNof ? (int)NotamProposalStatusCode.ModifiedCancelled : (int)NotamProposalStatusCode.Cancelled;
                }
                if (Status == NotamProposalStatusCode.Submitted && StartValidity != null && (DateTime)StartValidity <= DateTime.Now && EndValidity != null && (DateTime)EndValidity >= DateTime.Now)
                {
                    return ModifiedByNof ? (int)NotamProposalStatusCode.ModifiedEnforced : (int)NotamProposalStatusCode.Enforced;
                }
                if (Status == NotamProposalStatusCode.Ended)
                {
                    return (int)NotamProposalStatusCode.Ended;
                }
                return ModifiedByNof ? (int)NotamProposalStatusCode.ModifiedAndSubmitted : (int)NotamProposalStatusCode.Submitted;
            }
        }

        public bool IsCatchAll
        {
            get;
            set;
        }

        public string ItemA
        {
            get;
            set;
        }

        public string ItemD
        {
            get;
            set;
        } //schedule

        public string ItemE
        {
            get;
            set;
        }

        public string ItemEFrench
        {
            get;
            set;
        }

        public string ItemF
        {
            get;
            set;
        } //Limits

        public string ItemG
        {
            get;
            set;
        } //

        public string ItemX
        {
            get;
            set;
        }

        public int Lower
        {
            get;
            set;
        }

        public bool ModifiedByNof
        {
            get;
            set;
        }

        public string Nof
        {
            get;
            set;
        }

        public List<NotamProposalAttachmentViewModel> NotamProposalAttachments
        {
            get;
            set;
        }

        public List<NotamProposalInfoViewModel> NotamProposalsInfos
        {
            get;
            set;
        }

        public string NoteToNof
        {
            get;
            set;
        }

        public int Number
        {
            get;
            set;
        }

        public long OnBehalfOfOrganizationId
        {
            get;
            set;
        }

        public string Operator
        {
            get;
            set;
        }

        public NotamViewModel OriginalNotam
        {
            get;
            set;
        }

        public string OriginatorName
        {
            get;
            set;
        }

        public long PackageId
        {
            get;
            set;
        }

        public bool Permanent
        {
            get;
            set;
        }

        public string Purpose
        {
            get;
            set;
        }

        public int Radius
        {
            get;
            set;
        }

        public int ReferredNumber
        {
            get;
            set;
        }

        public string ReferredSeries
        {
            get;
            set;
        }

        public int ReferredYear
        {
            get;
            set;
        }

        public string RejectionReason
        {
            get;
            set;
        }

        public string Scope
        {
            get;
            set;
        }

        public string Series
        {
            get;
            set;
        }

        public bool SetBeginDateAtSubmit
        {
            get;
            set;
        }

        public DateTime? StartValidity
        {
            get;
            set;
        }

        public NotamProposalStatusCode Status
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get
            {
                switch (Status)
                {
                    case NotamProposalStatusCode.Draft:
                        return DbRes.T("Draft", "ReviewList");
                    case NotamProposalStatusCode.Queued:
                        return DbRes.T("Queued", "ReviewList");
                    case NotamProposalStatusCode.Pending:
                        return DbRes.T("Pending", "ReviewList");
                    case NotamProposalStatusCode.Rejected:
                        return DbRes.T("Rejected", "ReviewList");
                    case NotamProposalStatusCode.Submitted:
                        return DbRes.T("Submitted", "ReviewList");
                    case NotamProposalStatusCode.ModifiedAndSubmitted:
                        return DbRes.T("Modified and Submitted", "ReviewList");
                    case NotamProposalStatusCode.Withdrawn:
                        return DbRes.T("Withdrawn", "ReviewList");
                    case NotamProposalStatusCode.WithdrawnPending:
                        return DbRes.T("Withdrawn Pending", "ReviewList");
                    case NotamProposalStatusCode.QueuePending:
                        return DbRes.T("Queue Pending", "ReviewList");
                    case NotamProposalStatusCode.SoontoExpire:
                        return DbRes.T("Soon to Expire", "ReviewList");
                    case NotamProposalStatusCode.Ended:
                        return DbRes.T("Ended", "ReviewList");
                    case NotamProposalStatusCode.Enforced:
                        return DbRes.T("Enforced", "ReviewList");
                    case NotamProposalStatusCode.Cancelled:
                        return DbRes.T("Cancelled", "ReviewList");
                    case NotamProposalStatusCode.ModifiedEnforced:
                        return DbRes.T("Modified and Enforced", "ReviewList");
                    case NotamProposalStatusCode.ModifiedCancelled:
                        return DbRes.T("Modified and Cancelled", "ReviewList");
                    case NotamProposalStatusCode.Expired:
                        return DbRes.T("Expired", "ReviewList");
                    case NotamProposalStatusCode.JustForTestingPurposesNotToUseInProduction:
                        return "";
                    case NotamProposalStatusCode.Undefined:
                    default:
                        return DbRes.T("Undefined", "ReviewList");
                }
            }
        }

        public DateTime StatusTime
        {
            get;
            set;
        }

        public NotamViewModel SubmittedNotam
        {
            get;
            set;
        }

        public string Tokens
        {
            get;
            set;
        }

        public string Traffic
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public int Upper
        {
            get;
            set;
        }

        public int Year
        {
            get;
            set;
        }

        public float NsdVersion { get; set; }
        public bool VersionUpgraded
        {
            get;
            set;
        }
    }
}