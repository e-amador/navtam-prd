﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.Collections.Generic;

    using NavCanada.Core.Domain.Model.Dtos;

    public class DOAViewModel
    {

        public long Id { get; set; }

        public string Name { get; set; }


        public string Description { get; set; }

        public MultiPolygonDto DOAGeoArea { get; set; }
        public long? ParentDOAId { get; set; }



        public List<long> AssignToOrganizationIds { get; set; }

        public virtual List<DOAFilterViewModel> DoaFilters { get; set; }

        public DOAViewModel()
        {
            DoaFilters = new List<DOAFilterViewModel>();
        }
    }




}