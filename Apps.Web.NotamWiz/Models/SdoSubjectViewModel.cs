﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class SdoSubjectViewModel
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Mid { get; set; }
        public string SdoEntityName { get; set; }
        public string Designator { get; set; }
        public string Name { get; set; }
        public string SdoAttributes { get; set; }
    }
}
