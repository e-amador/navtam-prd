﻿namespace NavCanada.Applications.NotamWiz.Web.SignalRHubs
{
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    [HubName("SystemHealthMonitorHub")]
    public class SystemHealthMonitorHub : Hub
    {        
        public override Task OnConnected()
        {

            Clients.Others.systemConnected(Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Clients.Others.systemDisconnected(Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        public void updateInfo(string category, string type, string value)
        {

            

            if(Clients.User("aa").Data == null)
            {

            }
        }

        //public IEnumerable<string> GetConnection()
        //{
        //    Clients.Groups.
        //    return Clients
        //}

    }


}