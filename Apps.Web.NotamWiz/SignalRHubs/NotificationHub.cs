﻿namespace NavCanada.Applications.NotamWiz.Web.SignalRHubs
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    [Authorize]
    [HubName("NotificationHub")]
    public class NotificationHub : Hub
    {

        // TODO this need to be moved to DB queue
        public static FixedSizedQueue<ChatMessage> ChatHistory = new FixedSizedQueue<ChatMessage> { Limit = 10 };
        static readonly ConcurrentDictionary<string, string> Users = new ConcurrentDictionary<string, string>();

        public override Task OnConnected()
        {
            if (Context.User.Identity.Name == "Backend")
                return base.OnConnected(); ;

            Users.AddOrUpdate(Context.User.Identity.Name, Context.ConnectionId, (key, oldValue) => Context.ConnectionId);
            Clients.Others.userConnected(Context.User.Identity.Name);

            foreach (ChatMessage cMsg in ChatHistory.OrderBy(a => a.Date))
            {
                if (cMsg.Target == "ALL" || cMsg.Target == Context.User.Identity.Name)
                    Clients.Caller.addMessage(cMsg.User, cMsg.Message, cMsg.FormatedDate);
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            if (Context.User.Identity.Name == "Backend")
                return base.OnDisconnected(stopCalled); ;

            if (Users.ContainsKey(Context.User.Identity.Name))
            {
                string tmp;
                Users.TryRemove(Context.User.Identity.Name, out tmp);
            }

            Clients.Others.userDisconnected(Context.User.Identity.Name);

            return base.OnDisconnected(stopCalled);
        }

        public Task Subscribe(string type, long packageId)
        {
            return Groups.Add(Context.ConnectionId, type);
        }

        public Task UnSubscribe(string type)
        {
            return Groups.Remove(Context.ConnectionId, type);
        }

        public List<string> GetUsersList()
        {
            return Users.Keys.ToList();
        }

        /// <summary>
        /// Esteban: The two events commented below are not longer used. 
        /// They were replaced by proposalStatusChange where the action is triggered by the TaskEngine in response to a Notam Proposal Status change
        /// </summary>
        //[HubMethodName("sendNP")]
        //public void SendNp(long packageId, int status, DateTime statusTime, string displayId)
        //{
        //    Clients.Group("NP" + packageId).refreshPackageStatus(packageId, status, statusTime, displayId);
        //}

        //[HubMethodName("sendNP")]
        //public void SendNp(long packageId)
        //{
        //    Clients.Group("NP" + packageId).RefreshPackageNotificationInternalStatus(packageId);
        //}

        [HubMethodName("proposalStatusChange")]
        public void ProposalStatusChange(long packageId, int status, DateTime statusTime, string displayId)
        {
            Clients.All.proposalStatusChange(packageId, status, statusTime, displayId);
        }

        [HubMethodName("send")]
        public void Send(string message)
        {
            ChatMessage cMsg = new ChatMessage
            {
                Date = DateTime.Now, Message = HttpContext.Current.Server.HtmlEncode(message), User = Context.User.Identity.Name
            };
            ChatHistory.Enqueue(cMsg);

            if (cMsg.Target == "ALL")
                Clients.All.addMessage(cMsg.User, cMsg.Message, cMsg.FormatedDate);
            else
            {
                if (Users.ContainsKey(cMsg.Target))
                {
                    Clients.Client(Users[cMsg.Target]).addMessage(cMsg.User, cMsg.Message, cMsg.FormatedDate);

                    // don't double send to caller if he is sendind private messages to himself
                    if (cMsg.User != Context.User.Identity.Name)
                        Clients.Caller.addMessage(cMsg.User, cMsg.Message, cMsg.FormatedDate);
                }
                else
                {
                    Clients.Caller.addMessage(cMsg.User, "<strong>* User " + cMsg.Target + " is not logged in.</strong>", cMsg.FormatedDate);
                }
            }

        }
    }

    public class ChatMessage
    {
        public DateTime Date { get; set; }
        public string User { get; set; }
        string _message;
        public string Message
        {
            get
            {

                if (Target != "ALL")
                {
                    return "<img src='/Images/user/privateChat.png'/><span><strong>[" + Target + "]</strong> " + string.Join(" ", _message.Split(' ').Skip(1).ToArray()) + "</span>";
                }

                return "<img src='/Images/user/brodcastChat.png'/><span> " + _message + "</span>";


            }
            set
            {
                _message = value;
            }
        }
        public string FormatedDate { get { return Date.ToString("MM/dd/yy H:mm:ss UTC"); } }
        public string Target
        {
            get
            {
                if (_message.StartsWith("@"))
                {
                    var user = _message.Split(' ').FirstOrDefault();

                    if (user != null && user.Length > 1)
                        return user.Substring(1);
                }

                return "ALL";

            }
        }
    }

    public class FixedSizedQueue<T> : ConcurrentQueue<T>
    {


        public int Limit { get; set; }
        public new void Enqueue(T obj)
        {
            base.Enqueue(obj);
            lock (this)
            {
                T overflow;
                while (Count > Limit && TryDequeue(out overflow)) ;
            }
        }
    }

}