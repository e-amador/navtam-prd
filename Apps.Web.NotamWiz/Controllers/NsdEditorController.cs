﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Mime;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    using AutoMapper;

    using Microsoft.AspNet.Identity;

    using Code.Extentions;
    using Code.GeoTools;
    using Code.Helpers;
    using Filters;
    using Models;
    using Core.Common.Common;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;
    using Core.SqlServiceBroker;

    [Authorize]
    [TraceFilter]
    public class NsdEditorController : BaseController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof (NsdEditorController));

        public NsdEditorController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        [HttpPost]
        public ActionResult GetTimeScheduleApi()
        {
            return PartialView("TimeSchedule");
        }

        //[HttpPost]
        //public async Task<ActionResult> CreateNsd(string subjectId, long nsdId, float version)
        //{
        //    this.log.Object(new {Method = "CreateNSD", Parameters = new {subjectId, nsdId, version}});

        //    var nsdInfo = Mapper.Map<NsdInfoViewModel>(await Uow.NsdRepo.GetWithLastVersionAsync(nsdId));

        //    NotamSubjectViewModel theSubject = null;
        //    var doa = await GetUserDoaAsync();
        //    var bilingual = false;

        //    switch (nsdInfo.ForSubjectType)
        //    {
        //        case "SDO":
        //            var subject = await Uow.SdoSubjectRepo.GetByIdAsync(subjectId);
        //            bilingual = GeoDataService.IsInBilingualRegion(subject.SubjectGeo, Uow);
        //            theSubject = Mapper.Map<NotamSubjectViewModel>(subject, opt => { opt.Items["db"] = Uow; });
        //            break;
        //        case "OBS":

        //            var refPoint = doa.DoaGeoArea.GetReferancePoint();
        //            bilingual = GeoDataService.IsInBilingualRegion(doa.DoaGeoArea, Uow);
        //            theSubject = new NotamSubjectViewModel
        //            {
        //                Name = DbRes.T("Obstacle", "NsdEditor"),
        //                SubjectGeoJson = null,
        //                SubjectGeoRefLat = refPoint.Latitude,
        //                SubjectGeoRefLong = refPoint.Longitude,
        //                Designator = "",
        //                Ad = "",
        //                Fir = "",
        //                SubjectMid = "",
        //                SubjectType = "OBSTACLE",
        //                SubjectId = Guid.NewGuid().ToString()
        //            };
        //            break;            
        //    }

        //    return PartialView("NsdEditor", new NsdEditorViewModel
        //    {
        //        ReadOnly = false,
        //        PackageId = 0,
        //        PropsalType = NotamType.N.ToString(),
        //        nsd = nsdInfo,
        //        Bilingual = bilingual,
        //        DOAGeoJson = GeoDataService.GeoJsonMultiPolygonFromGeography(doa.DoaGeoArea),
        //        subject = theSubject,
        //        notam = new NotamProposalViewModel(),
        //        Setting = Uow.ConfigurationValueRepo.GetByCategory("NOTAM").Select(c => new {c.Name, c.Value}),
        //        IsFicRole = await IsUserInRoleAsync(HttpContext.User.Identity.GetUserId(), CommonDefinitions.FicUserRole)
        //    });
        //}


        [HttpGet]
        public async Task<ActionResult> CreateNotamProposal(long nsdId)
        {
            _log.Object(new { Method = "CreateNotamProposal", Parameters = new { nsdId } });

            var nsdInfo = Mapper.Map<NsdInfoViewModel>(await Uow.NsdRepo.GetWithLastVersionAsync(nsdId));

            var doa = await GetUserDoaAsync();


            return PartialView("NsdEditor", new NsdEditorViewModel
            {
                ReadOnly = false,
                PackageId = 0,
                PropsalType = NotamType.N.ToString(),
                nsd = nsdInfo,
                Bilingual = false,
                DOAGeoJson = GeoDataService.GeoJsonMultiPolygonFromGeography(doa.DoaGeoArea),
                subject = null,
                notam = new NotamProposalViewModel(),
                Setting = Uow.ConfigurationValueRepo.GetByCategory("NOTAM").Select(c => new { c.Name, c.Value }),
                IsFicRole = await IsUserInRoleAsync(HttpContext.User.Identity.GetUserId(), CommonDefinitions.FicUserRole)
            });
        }

        [HttpGet]
        public ActionResult IsInBilingualRegion(double latitud, double longitude)
        {
            var result = GeoDataService.IsInBilingualRegion(latitud, longitude, Uow);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> EditOrViewProposal(long packageId, string mode)
        {
            _log.Object(new {Method = "EditOrViewProposal", Parameters = new {packageId, mode}});

            var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(packageId);
            if (package == null || !package.Proposals.Any())
            {
                _log.Warn(
                    new { Method = "EditOrViewProposal", Parameters = new { packageId }, Warning = "Invalid Package Id." });
                return PartialView("NsdEditor", new NsdEditorViewModel
                {
                    Error = true,
                    Message = DbRes.T("No NOTAM Proposal found with this ID.", "NsdEditor")
                });
            }

            return await EditOrViewProposal(package, mode);
        }

        public async Task<ActionResult> EditOrViewProposalByNotamNumber(string notamNumber, string mode)
        {
            long nn = long.Parse(notamNumber.Substring(1, 4));
            _log.Object(new { Method = "EditOrViewProposalByNotamNumber", Parameters = new { _notamNumber = nn, mode } });

            var package = await Uow.NotamPackageRepo.GetFullPackageByNotamNumberAsync(nn);
            if (package == null || !package.Proposals.Any())
            {
                _log.Warn(
                    new { Method = "EditOrViewProposalByNotamNumber", Parameters = new { _notamNumber = nn }, Warning = "Invalid Notam Number." });
                return PartialView("NsdEditor", new NsdEditorViewModel
                {
                    Error = true,
                    Message = DbRes.T("No NOTAM Proposal found with this ID.", "NsdEditor")
                });
            }

            return await EditOrViewProposal(package, mode);
        }

        public async Task<ActionResult> EditOrViewProposal(NotamPackage package, string mode)
        {
            var proposalId = package.Proposals.Last().Id;

            var proposal = await Uow.NotamProposalRepo.GetFullProposalAsync(proposalId);

            if (mode != "view" && proposal.Status != NotamProposalStatusCode.Draft &&
                proposal.Status != NotamProposalStatusCode.Withdrawn &&
                proposal.Status != NotamProposalStatusCode.Rejected)
            {
                if (mode == "any")
                {
                    mode = "view";
                }
                else
                {
                    _log.Warn(
                        new
                        {
                            Method = "EditOrViewProposal",
                            Parameters = new { package.Id },
                            Warning = "Edit request for Proposal not in Draft Status"
                        });
                    return PartialView("NsdEditor", new NsdEditorViewModel
                    {
                        Error = true,
                        Message = DbRes.T("Proposal is not in editable state.", "NsdEditor")
                    });
                }
            }

            var proposalVm = Mapper.Map<NotamProposal, NotamProposalViewModel>(proposal);

            if (proposal.ModifiedByNof)
                proposalVm.SubmittedNotam =
                    Mapper.Map<Notam, NotamViewModel>(Uow.NotamRepo.GetById(proposal.SubmittedNotamId));

            var nsdInfo =
                Mapper.Map<NsdInfoViewModel>(
                    await Uow.NsdRepo.GetWithLastVersionAsync(package.NsdId));
            var subject = Uow.SdoSubjectRepo.GetById(package.SdoSubjectId);
            var doa = await GetUserDoaAsync();

            if (!GeoDataService.IsLocationWithinDOA(subject.SubjectGeoRefPoint, doa))
            {
                _log.Warn(
                    new
                    {
                        Method = "EditOrViewProposal",
                        Parameters = new { package.Id },
                        Warning = "Proposal subject not in user DOA"
                    });
                return PartialView("NsdEditor", new NsdEditorViewModel
                {
                    Error = true,
                    Message = DbRes.T("Proposal is not in your DOA", "NsdEditor")
                });
            }

            var vm = new NsdEditorViewModel
            {
                ReadOnly = mode == "view",
                PackageId = package.Id,
                PropsalType = proposal.Type.ToString(),
                nsd = nsdInfo,
                Bilingual = GeoDataService.IsInBilingualRegion(subject.SubjectGeo, Uow),
                DOAGeoJson = GeoDataService.GeoJsonMultiPolygonFromGeography(doa.DoaGeoArea),
                subject = Mapper.Map<NotamSubjectViewModel>(subject, opt => { opt.Items["db"] = Uow; }),
                notam = proposalVm,
                Setting = Uow.ConfigurationValueRepo.GetByCategory("NOTAM").Select(c => new { c.Name, c.Value }),
                IsFicRole = await IsUserInRoleAsync(HttpContext.User.Identity.GetUserId(), CommonDefinitions.FicUserRole)
            };

            return PartialView("NsdEditor", vm);
        }

        [HttpPost]
        public async Task<ActionResult> ViewProposal(long packageId)
        {
            _log.Object(new {Method = "ViewProposal", Parameters = new {packageId}});

            return await EditOrViewProposal(packageId, "view");
        }

        [HttpPost]
        public async Task<ActionResult> EditProposal(long packageId)
        {
            _log.Object(new {Method = "EditProposal", Parameters = new {packageId}});
            return await EditOrViewProposal(packageId, "edit");
        }

        [HttpPost]
        public async Task<ActionResult> EditOrViewProposalByNotamNumber(string notamNumber)
        {
            _log.Object(new { Method = "EditOrViewProposalByNotamNumber", Parameters = new { notamNumber } });

            return await EditOrViewProposalByNotamNumber(notamNumber, "any");
        }

        [HttpPost]
        public async Task<ActionResult> EditOrViewProposal(long packageId)
        {
            _log.Object(new {Method = "EditProposal", Parameters = new {packageId}});
            return await EditOrViewProposal(packageId, "any");
        }

        [HttpPost]
        public async Task<ActionResult> ReplaceProposal(long packageId)
        {
            _log.Object(new {Method = "ReplaceProposal", Parameters = new {packageId}});

            return await CreateProposalFromPackage(packageId, NotamType.R);
        }

        [HttpPost]
        public async Task<ActionResult> CancelProposal(long packageId)
        {
            _log.Object(new {Method = "CancelProposal", Parameters = new {packageId}});

            return await CreateProposalFromPackage(packageId, NotamType.C);
        }

        [HttpPost]
        public async Task<ActionResult> SaveNotamProposal(NotamProposalViewModel notamProposal, int nsdId, float version,string subjectId)
        {
            try
            {
                _log.Object(new { Method = "SaveNotamProposal", Parameters = new { notamProposal, nsdId, version, subjectId } });                

                var nsd = Uow.NsdRepo.GetById(nsdId);
                var subject = Uow.SdoSubjectRepo.GetById(subjectId);

                if (nsd == null)
                {
                    var errUid = this._log.ErrorWithUID(new InvalidDataException("Invalid NSD ID :" + nsdId));
                    return ProcessResult.GetErrorResult("NSDEC-09",
                        DbRes.T("Error - Invalid Request.", "NsdEditor") + errUid);
                }

                if (subject == null)
                {
                    var errUid = this._log.ErrorWithUID(new InvalidDataException("Invalid subject Id: " + subjectId));
                    return ProcessResult.GetErrorResult("NSDEC-10",
                        DbRes.T("Error - SOD Subject not defined", "NsdEditor") + errUid);
                }

                //TODO: Ask Omar the correct solution for this. For some reason the proposal passed from the client has a version number is 0.0.
                //TODO: A work around solution for now is updating the version number with the version number passed.
                if (Math.Abs(notamProposal.NsdVersion) < 0.1)
                    notamProposal.NsdVersion = version;

                var updateProp = await GetUpdatedNotamProposal(notamProposal);


                // Prevent saving NOTAMR and NOTAMC as per Story #12219
                if (updateProp.PackageId != 0 && (updateProp.Type == NotamType.R || updateProp.Type == NotamType.C))
                {
                    var errUid = this._log.ErrorWithUID(new InvalidDataException("Saving NOTAMR or NOTAMC is not allowed."));
                    return ProcessResult.GetErrorResult("NSDEC-14",
                        DbRes.T("Saving not allowed for this NOTAM Type", "NsdEditor") + errUid);
                }




                if (updateProp.Id > 0)
                {
                    Uow.NotamProposalRepo.Update(updateProp);
                    await Uow.SaveChangesAsync();
                    return ProcessResult.GetSuccessResult(
                    DbRes.T("Save NOTAM Proposal completed successfully.", "NsdEditor"),
                                            new { PackageId = notamProposal.PackageId, ProposalId = updateProp.Id }
                    );
                }
                if (notamProposal.PackageId != 0)
                {
                    var packge = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(notamProposal.PackageId);
                    if (packge == null)
                        return Content(DbRes.T("Invalid Proposal.", "NsdEditor"));

                    packge.Proposals.Add(updateProp);
                    await Uow.SaveChangesAsync();
                    return ProcessResult.GetSuccessResult(
                        DbRes.T("Save NOTAM Proposal completed successfully.", "NsdEditor"),
                        new { PackageId = packge.Id, ProposalId = packge.Proposals.Last().Id }
                        );

                }
                else
                {
                    var packge = new NotamPackage
                    {
                        Name = nsd.Name,
                        Description = nsd.Description,
                        NsdId = nsdId,
                        SdoSubjectId = subjectId,                            
                        SubjectCategory = DashboardItemType.SdoSubject,
                        SubjectType = subject.SdoEntityName,
                        Proposals = new List<NotamProposal>
                        {
                            updateProp
                        }
                    };
                    Uow.NotamPackageRepo.Add(packge);
                    await Uow.SaveChangesAsync();

                    return ProcessResult.GetSuccessResult(
                        DbRes.T("Save NOTAM Proposal completed successfully.", "NsdEditor"),
                        new { PackageId = packge.Id, ProposalId = packge.Proposals.Last().Id }
                        );
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error Saving Proposal", ex);
                return ProcessResult.GetErrorResult("NSDEC-13", DbRes.T("Error Saving NOTAM Proposal", "NsdEditor"));

            }
        }

        private async Task<NotamProposal> GetUpdatedNotamProposal(NotamProposalViewModel vm)
        {
            vm.Status = NotamProposalStatusCode.Draft;
            vm.StatusTime = DateTime.UtcNow;            
            vm.Operator = User.Identity.GetUserName();
            vm.OnBehalfOfOrganizationId = (await GetUserOrganizationAsync()).Id;

            var dbProp = await Uow.NotamProposalRepo.GetByIdAsync(vm.Id);

            var updatedProp = dbProp == null
                ? Mapper.Map<NotamProposalViewModel, NotamProposal>(vm)
                : Mapper.Map<NotamProposalViewModel, NotamProposal>(vm, dbProp);

            foreach (var inf in vm.NotamProposalsInfos)
            {
                var dbInfo = updatedProp.NotamProposalsInfos.SingleOrDefault(e => e.Id == inf.Id);
                if (dbInfo != null)
                {
                    Mapper.Map<NotamProposalInfoViewModel, NotamProposalInfo>(inf, dbInfo);
                }
                else
                {
                    var newInfo = Mapper.Map<NotamProposalInfoViewModel, NotamProposalInfo>(inf);
                    updatedProp.NotamProposalsInfos.Add(newInfo);
                }
            }

            return updatedProp;
        }

        [HttpPost]
        public async Task<ActionResult> DeleteNotamProposal(long packageId)
        {
            _log.Object(new {Method = "DeleteNotamProposal", Parameters = new {packageId}});

            try
            {
                var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(packageId);

                if (package == null)
                    return ProcessResult.GetErrorResult("NSDEC-01", DbRes.T("NOTAM Proposal not found.", "NsdEditor"));

                var packageOwner = package.Proposals.First().Operator;
                var notamProposalOrg = await Uow.OrganizationRepo.GetByOperatorAsync(packageOwner);
                var userOrg = await GetUserOrganizationAsync();

                if (notamProposalOrg == null || (notamProposalOrg.Id != userOrg.Id))
                    return ProcessResult.GetErrorResult("NSDEC-02",
                        DbRes.T("NOTAM Proposal belong to another organization.", "NsdEditor"));


                if (package.Proposals.Count == 1)
                {
                    var proposal = package.Proposals.FirstOrDefault();
                    if (proposal != null)
                    {
                        if( proposal.Status != NotamProposalStatusCode.Draft ||
                            proposal.Status != NotamProposalStatusCode.Withdrawn ||
                            proposal.Status != NotamProposalStatusCode.Rejected )
                            return ProcessResult.GetErrorResult("NSDEC-02",
                                DbRes.T("NOTAM Proposal can't be deleted in current status status.", "NsdEditor"));


                        Uow.BeginTransaction();
                        try
                        {
                            Uow.NotamProposalRepo.Delete(proposal);
                            Uow.NotamPackageRepo.Delete(package);
                            var trackings = await Uow.AimslRequestTrackingRepo.GetByNotamProposalIdAsync(proposal.Id);
                            foreach (var tracking in trackings)
                                Uow.AimslRequestTrackingRepo.Delete(tracking);

                            await Uow.SaveChangesAsync();
                            Uow.Commit();
                        }
                        catch(Exception e)
                        {
                            Uow.Rollback();
                            _log.Error("Error Deleting Notam Proposal", e);
                            throw;
                        }
                    }
                }
                else
                {
                    var latestProposal = package.Proposals.Last();
                    if (latestProposal != null)
                    {
                        if (latestProposal.Status != NotamProposalStatusCode.Draft ||
                            latestProposal.Status != NotamProposalStatusCode.Withdrawn ||
                            latestProposal.Status != NotamProposalStatusCode.Rejected)
                            return ProcessResult.GetErrorResult("NSDEC-02",
                                DbRes.T("NOTAM Proposal can't be deleted in current status status.", "NsdEditor"));

                        Uow.BeginTransaction();
                        try
                        {
                            Uow.NotamProposalRepo.Delete(latestProposal);
                            var trackings = await Uow.AimslRequestTrackingRepo.GetByNotamProposalIdAsync(latestProposal.Id);
                            foreach (var tracking in trackings)
                                Uow.AimslRequestTrackingRepo.Delete(tracking);

                            await Uow.SaveChangesAsync();
                            Uow.Commit();
                        }
                        catch (Exception e)
                        {
                            Uow.Rollback();
                            _log.Error("Error Deleting Notam Proposal", e);
                            throw;
                        }
                    }
                }

                return ProcessResult.GetSuccessResult(
                    DbRes.T("Delete NOTAM Proposal operation completed successfully.", "NsdEditor"));
            }
            catch (Exception)
            {
                return ProcessResult.GetErrorResult("500", DbRes.T(". Operation failed to complete.", "NsdEditor"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> WithdrawNotamProposal(long packageId)
        {
            _log.Object(new {Method = "WithdrawNotamProposal", Parameters = new {packageId}});

            try
            {
                var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(packageId);
                if (package == null)
                    return ProcessResult.GetErrorResult("NSDEC-03", DbRes.T("NOTAM Proposal not found.", "NsdEditor"));

                //var packageOwner = package.Proposals.First().OriginatorId;
                //var notamProposalOrg = await Uow.OrganizationRepo.GetByUserIdAsync(packageOwner, false);
                //var userOrg = await GetUserOrganizationAsync();

                //if (notamProposalOrg == null || (notamProposalOrg.Id != userOrg.Id))
                //    return ProcessResult.GetErrorResult("NSDEC-04",
                //        DbRes.T("NOTAM Proposal belong to another organization.", "NsdEditor"));

                var proposal = package.Proposals.Last();
                if (proposal.Status != NotamProposalStatusCode.Queued &&
                    proposal.Status != NotamProposalStatusCode.QueuePending)
                    return ProcessResult.GetErrorResult("NSDEC-05", DbRes.T("Invalid NOTAM Proposal Status.", "NsdEditor"));


                proposal.StatusTime = DateTime.UtcNow;
                proposal.Status = NotamProposalStatusCode.WithdrawnPending;
                await Uow.SaveChangesAsync();

                //Publish Mto to target Queeue
                var mto = CreateMtoForNotamChangeStatus(MessageTransferObjectId.WithdrawNotamProposal, proposal.Id);
                PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);

                Uow.Commit();

                return
                    ProcessResult.GetSuccessResult(DbRes.T("NOTAM Proposal Status changed to WithdrawnInProgress",
                        "NsdEditor"));
            }
            catch (Exception)
            {
                Uow.Rollback();
                throw;
            }
        }

        [HttpGet]
        public PartialViewResult GetNsdView(string fileLocation, float version)
        {
            _log.Object(new {Method = "GetNSDView", Parameters = new {fileLocation, version}});
            return
                PartialView(string.Format("NOTAM-NSDs/{0}/V{1}", fileLocation,
                    version.ToString(CultureInfo.InvariantCulture).Replace(".", "R")));
        }

        public async Task<ActionResult> SaveAttachement(IEnumerable<HttpPostedFileBase> attachmentsFiles, long proposalId, Guid[] fileIds)
        {
            _log.Object(new {Method = "SaveAttachement", Parameters = new {attachmentsFiles, proposalId, fileIds}});

            var np = await Uow.NotamProposalRepo.GetFullProposalAsync(proposalId);

            if (np == null)
                return ProcessResult.GetErrorResult("NSDEC-0012", DbRes.T("Invalid input.", "NsdEditor"));

            if (np.NotamProposalAttachments.Count + attachmentsFiles.Count() > 25)
                return ProcessResult.GetErrorResult("NSDEC-0011",
                    DbRes.T("Maximum attachment count (25 files) reached.", "NsdEditor"));

            foreach (var file in attachmentsFiles)
            {
                if (file.ContentLength > 52428800)
                    return ProcessResult.GetErrorResult("NSDEC-0011",
                        DbRes.T("File " + file.FileName + " size is more than allowed threshold (50MB).", "NsdEditor"));
            }

            var index = 0;
            foreach (var file in attachmentsFiles)
            {
                var target = new MemoryStream();
                file.InputStream.CopyTo(target);
                np.NotamProposalAttachments.Add(new NotamProposalAttachment
                {
                    Id = fileIds[index],
                    FileName = file.FileName,
                    Attachment = target.ToArray(),
                    CreationDate = DateTime.Now,
                    OwnerId = User.Identity.GetUserId()
                });

                index++;
            }

            await Uow.SaveChangesAsync();

            return Json(new {fileId = np.NotamProposalAttachments.Last().Id});
        }

        public async Task<ActionResult> RemoveAttachement(string[] fileNames, long proposalId, Guid fileId)
        {
            _log.Object(new {Method = "RemoveAttachement", Parameters = new {fileNames, proposalId, fileId}});

            var np = Uow.NotamProposalRepo.GetById(proposalId);
            if (np == null)
                return Content("Error - NOTAM Proposal not found");

            var att = np.NotamProposalAttachments.SingleOrDefault(a => a.Id == fileId);
            if (att == null)
                return Content("Error Attachment not Found");

            var attchmentOwnerOrg = await Uow.OrganizationRepo.GetByUserIdAsync(att.OwnerId, false);
            var userOrg = await GetUserOrganizationAsync();

            if (attchmentOwnerOrg.Id != userOrg.Id)
                return Content("Error Attachment owned by another organization");

            Uow.NotamProposalAttachmentRepo.Delete(att);
            await Uow.SaveChangesAsync();

            return ProcessResult.GetSuccessResult();
        }

        public ActionResult DownloadFile(Guid fileId)
        {
            _log.Object(new {Method = "DownloadFile", Parameters = new {fileId}});

            var att = Uow.NotamProposalAttachmentRepo.GetById(fileId);
            if (att != null)
            {
                var cd = new ContentDisposition
                {
                    FileName = att.FileName,
                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                return File(att.Attachment, "application/force-download");
            }
            return null;
        }

        private async Task<ActionResult> CreateProposalFromPackage(long packageId, NotamType propsalType)
        {
            _log.Object(new {Method = "CreateProposalFromPackage", Parameters = new {packageId, propsalType}});

            // get last proposal in this package
            var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(packageId);

            if (package == null || package.Proposals.Count == 0)
                return Content(DbRes.T("Invalid Proposal.", "NsdEditor"));

            var acceptedStatuses = new List<NotamProposalStatusCode>
            {
                NotamProposalStatusCode.Enforced,
                NotamProposalStatusCode.ModifiedAndSubmitted,
                NotamProposalStatusCode.Submitted,
                NotamProposalStatusCode.ModifiedEnforced
            };

            if (!acceptedStatuses.Contains(package.Proposals.Last().Status))
                return Content(DbRes.T("Invalid Proposal Status.", "NsdEditor"));


            NsdVersion nsdVersion = Uow.NsdVersionRepo.GetLatest(package.NsdId);

            var nsdInfo =
                Mapper.Map<NsdInfoViewModel>(
                    await Uow.NsdRepo.GetWithLastVersionAsync(package.NsdId));

            var nsdControllerName = string.Format("NavCanada.Applications.NotamWiz.Web.NSDs.{0}.V{1}.{2}Controller",
                nsdInfo.FileLocation,
                nsdInfo.Version.ToString(CultureInfo.InvariantCulture).Replace(".", "R"),
                nsdInfo.FileLocation);

            var t = Type.GetType(nsdControllerName);

            dynamic controller = Activator.CreateInstance(t, Uow, ClusterQueues);
            NotamProposalViewModel newProposal =
                await controller.CreateProposalFromBaseAsync(package.Proposals.Last().Id, propsalType);

            if (Math.Abs(newProposal.NsdVersion - nsdInfo.Version) > 0.1)
            {
                newProposal.NsdVersion = nsdInfo.Version;
                newProposal.Tokens = "";
                newProposal.VersionUpgraded = true;
            }

            var subject = Uow.SdoSubjectRepo.GetById(package.SdoSubjectId);

            var doa = await GetUserDoaAsync();

            var vm = new NsdEditorViewModel
            {
                ReadOnly = false,
                PackageId = packageId,
                PropsalType = propsalType.ToString(),
                nsd = nsdInfo,
                Bilingual = GeoDataService.IsInBilingualRegion(subject.SubjectGeo, Uow),
                DOAGeoJson = GeoDataService.GeoJsonMultiPolygonFromGeography(doa.DoaGeoArea),
                subject = Mapper.Map<NotamSubjectViewModel>(subject, opt => { opt.Items["db"] = Uow; }),
                notam = newProposal,
                Setting = Uow.ConfigurationValueRepo.GetByCategory("NOTAM").Select(c => new { c.Name, c.Value }),
                IsFicRole = await IsUserInRoleAsync(HttpContext.User.Identity.GetUserId(), CommonDefinitions.FicUserRole)

            };

            return PartialView("NsdEditor", vm);
        }

    }
}