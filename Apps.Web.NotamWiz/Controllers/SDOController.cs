﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Web.Mvc;

    using Code.Extentions;

    [Authorize]
    public class SDOController : Controller
    {
        readonly ILog _log = LogManager.GetLogger(typeof(SDOController));

        //
        // GET: /AIRS/        
        public FileResult ProcessRequest(string param)
        {

            _log.Object(new { Method = "ProcessRequest", Parameters = new { param} });

            if (this.Request.Params["$filter"] == "" ||
                string.IsNullOrEmpty(this.Request.Params["$select"]))
            {
                return null;
            }

            try
            {
                string url = ConfigurationManager.AppSettings.Get("aeroRDS_Services_URL");
                var externalRequest = (HttpWebRequest)WebRequest.Create(url + param + "?" + this.Request.QueryString);
                //this.Request.CopyTo(externalRequest);
                var externalResponse = (HttpWebResponse)externalRequest.GetResponse();
                return File(externalResponse.GetResponseStream(), externalResponse.ContentType);
            }
            catch(Exception e)
            {
                this._log.Warn(new { Warning = "Error calling aeroRDS", Method = "ProcessRequest", Parameters = new { param, this.Request} }, e);
                return null;
            }
        }

    }
}
