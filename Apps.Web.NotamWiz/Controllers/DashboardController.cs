﻿using NavCanada.Core.Common.Common;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;


    using Microsoft.AspNet.Identity;

    using Code.Extentions;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;

    using Newtonsoft.Json;

    [Authorize(Roles = "User, FIC_ROLE")]
    public class DashboardController : BaseController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(DashboardController));

        public DashboardController(IUow uow, IClusterQueues clusterQueues) : base(uow, clusterQueues)
        {
        }

        public async Task<ActionResult> GetNotamPackage(long PackageId)
        {
            _log.Object(new
            {
                Method = "GetNotamPackage",
                Parameters = new
                {
                    PackageId
                }
            });

            var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(PackageId);
            var results = Mapper.Map<NotamPackage, NotamPackageViewModel>(package, opt =>
            {
                opt.Items["db"] = Uow;
            });
            
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            _log.Object(new
            {
                Method = "Index"
            });

            // get current users profile information
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }

            //if the user is not approved re-direct to login->
            if (!user.IsApproved)
            {
                TempData["userMessage"] = user.IsApproved != true ? DbRes.T("AccountNotApprovedWaitForApproval", "DashboardController") : TempData["userMessage"] = DbRes.T("AccountNotApprovedWaitForApproval", "DashboardController");
                return RedirectToAction("Login", "Account");
            }

            //grant access to user if approved -->
            var subject = new NotamSubjectViewModel
            {
                SubjectCategory = DashboardItemType.Undefined,
                SubjectType = ""
            };
            var viewParameters = new Dictionary<string, object>
            {
                { "subject", subject }
            };

            return View(viewParameters);
        }

        public async Task<ActionResult> Proposal_Read([DataSourceRequest] DataSourceRequest request, long packageId)
        {
            _log.Object(new
            {
                Method = "Proposal_Read",
                Parameters = new
                {
                    request,
                    packageId
                }
            });
            try
            {
                var proposals = await Uow.NotamProposalRepo.GetByPackageIdAsync(packageId);
                return Json(Mapper.Map<List<NotamProposal>, List<NotamProposalViewModel>>(proposals.ToList()).ToDataSourceResult(request));
            }
            catch
            {
                _log.Error("Could not retrieve proposals for NOTAM package id:" + packageId);
                return Json(new List<NotamProposalViewModel>().ToDataSourceResult(request));
            }
        }

        public ActionResult ReviewList(DashboardItemType category, string type, string subjectId, string name)
        {
            _log.Object(new
            {
                Method = "ReviewList",
                Parameters = new
                {
                    category,
                    type,
                    subjectId,
                    name
                }
            });
            var subjectMid = category == DashboardItemType.SdoSubject ? Uow.SdoSubjectRepo.GetById(subjectId).Mid : "";
            var subject = new NotamSubjectViewModel
            {
                SubjectCategory = category,
                SubjectType = type,
                SubjectId = subjectId,
                Name = name,
                SubjectMid = subjectMid
            };

            var viewParameters = new Dictionary<string, object>();
            viewParameters.Add("subject", subject);
            
            return PartialView(viewParameters);
        }

        public async Task<ActionResult> LoadReviewList([DataSourceRequest] DataSourceRequest request, DashboardTreeModel model)
        {
            _log.Object(new
            {
                Method = "LoadReviewList",
                Parameters = new
                {
                    request,
                    model
                }
            });

            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (currentUser == null || (currentUser.OrganizationId == null))
                return Json(new List<NotamPackageViewModel>().ToDataSourceResult(request));

            var expiredRecordsFilter = DateTime.Now.AddDays(-await GetUserPreferenceExpiredRecordsFilter());
            var doa = (await Uow.OrganizationRepo.GetWithDoaIdAsync(currentUser.OrganizationId.Value)).AssignedDoa;

            List<NotamPackage> packages = null;

            switch (model.NodeType)
            {
                case NodeType.Root:
                    packages = await Uow.NotamPackageRepo.GetAllInPackageAsync(expiredRecordsFilter, doa.DoaGeoArea);
                    break;
                case NodeType.Category:
                    packages = await Uow.NotamPackageRepo.GetAllInPackageByCategoryAsync(model.Name, expiredRecordsFilter, doa.DoaGeoArea);
                    break;
                case NodeType.Nsd:
                    packages = await Uow.NotamPackageRepo.GetAllInPackageByNsdIdAsync(model.Id, expiredRecordsFilter, doa.DoaGeoArea);
                    break;
            }
            if (packages == null)
            {
                return Json(new List<NotamPackageViewModel>().ToDataSourceResult(request));
            }
            var results = Mapper.Map<List<NotamPackage>, List<NotamPackageViewModel>>(packages, opt =>
            {
                opt.Items["db"] = Uow;
            }).ToDataSourceResult(request);
           
            return Json(results);
        }

        public async Task<ActionResult> ReviewList_Read([DataSourceRequest] DataSourceRequest request, NotamSubjectViewModel subject)
        {
            _log.Object(new
            {
                Method = "ReviewList_Read",
                Parameters = new
                {
                    request,
                    subject
                }
            });
            
            var enCategory = subject.SubjectCategory;
            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            var expiredRecordsFilter = DateTime.Now.AddDays(-await GetUserPreferenceExpiredRecordsFilter());
            var doa = (await Uow.OrganizationRepo.GetWithDoaIdAsync((long)currentUser.OrganizationId)).AssignedDoa;
          
            var packages = Uow.NotamPackageRepo.GetPackagesForSubject(enCategory, subject.SubjectId, expiredRecordsFilter, doa.DoaGeoArea);
            if (packages == null)
            {
                return Json(new List<NotamPackageViewModel>().ToDataSourceResult(request));
            }
            var results = Mapper.Map<List<NotamPackage>, List<NotamPackageViewModel>>(packages.ToList(), opt =>
            {
                opt.Items["db"] = Uow;
            }).ToDataSourceResult(request);

            return Json(results);
        }

        public async Task<ActionResult> TreeView()
        {
            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            var org = Uow.OrganizationRepo.GetById(currentUser.OrganizationId);

            if (org.AssignedDoaId == null)
            {
                return Content(new List<DashboardTreeModel>().ToCamelJson(), "application/json");
            }
            var doa = await Uow.DoaRepo.GetByIdWithFiltersAsync((long)org.AssignedDoaId);
            if (doa == null)
            {
                return null;
            }

            var nsds = await Uow.NsdRepo.GetAllActiveNSDsAsync();
            
            var treeModel = new List<DashboardTreeModel>();
            var rootLevel = new DashboardTreeModel
            {
                Id = 0,
                Name = "All",
                Expanded = true,
                NodeType = NodeType.Root,
                Items = new List<DashboardTreeModel>()
            };
            
            treeModel.Add(rootLevel);

            foreach (var nsd in nsds)
            {
                var categoryName = nsd.Category.ToUpper();
                var category = rootLevel.Items.FirstOrDefault(e => e.Name == categoryName);

                if (category == null)
                {
                    var isNotCatchAll = categoryName != NSDDefinitions.CatchAll;
                    var item = new DashboardTreeModel
                    {
                        Id = nsd.Id,
                        Name = categoryName,
                        Expanded = false,
                        NodeType = isNotCatchAll ? NodeType.Category : NodeType.Nsd,
                        Items = new List<DashboardTreeModel>()
                    };
                    if (isNotCatchAll)
                    {
                        item.Items.Add(new DashboardTreeModel
                        {
                            Id = nsd.Id,
                            Name = nsd.Name.ToUpper(),
                            Expanded = false,
                            NodeType = NodeType.Nsd,
                            Items = null
                        });
                    }

                    rootLevel.Items.Add(item);
                }
                else
                {
                    category.Items.Add(new DashboardTreeModel
                    {
                        Id = nsd.Id,
                        Name = nsd.Name.ToUpper(),
                        NodeType = NodeType.Nsd,
                        Expanded = false,
                        Items = null
                    });
                }
            }


            return Content(treeModel.ToCamelJson(), "application/json");
           
        }

        public async Task<JsonResult> Subjects(long? uid, int category, string type, string subjectId, Doa doa = null)
        {
            _log.Info("Subjects");

            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            var org = Uow.OrganizationRepo.GetById(currentUser.OrganizationId);

            if (doa == null || doa.DoaGeoArea == null)
            {
                if (org.AssignedDoaId == null)
                {
                    return Json(new List<DashboardItemViewModel>(), JsonRequestBehavior.AllowGet);
                }
                doa = await Uow.DoaRepo.GetByIdWithFiltersAsync((long)org.AssignedDoaId);
            }
            if (doa == null)
            {
                return null;
            }

            var itemType = (DashboardItemType)category;
            switch (itemType)
            {
                case DashboardItemType.Undefined:
                case DashboardItemType.All:
                case DashboardItemType.SubjectList:
                {
                    var dashboardItems =
                        Uow.DashboardItemRepo.GetAll()
                            .Where(i => i.ParentId == null)
                            .OrderBy(a => a.OrderOfPrecedence)
                            .ToList()
                            .Select(d => new DashboardItemViewModel
                            {
                                Id = d.Id,
                                Name =
                                    d.Type == DashboardItemType.All
                                        ? DbRes.T("All", "Dashboard")
                                        : DbRes.T("Subject", "Dashboard"),
                                HasChildren = d.Type == DashboardItemType.SubjectList,
                                Type = d.Type,
                                SubjectType = d.SdoSubjectType,
                                SubjectId = d.SubjectId
                            });

                    var sdos =
                        Uow.SdoSubjectRepo.GetWithinGeo(doa.DoaGeoArea)
                            .Where(s => s.ParentId == null && (s.SdoEntityName != CommonDefinitions.Fir || org.IsFic))
                            .OrderBy(o => o.SdoEntityName)
                            .ToList();

                    var filteredList =
                        ApplyDoaFilters(sdos, doa)
                            .Where(s => s.ParentId == null)
                            .Select(d => new DashboardItemViewModel
                            {
                                Name = d.Designator == null ? d.Name : d.Designator + " - " + d.Name,
                                HasChildren = d.Children!= null && d.Children.Any(),
                                Type = DashboardItemType.SdoSubject,
                                SubjectType = d.SdoEntityName,
                                SubjectId = d.Id
                            }).ToList();
                  
                    var results = new List<DashboardItemViewModel>();
                    results.AddRange(dashboardItems);
                    results.AddRange(filteredList);
                    return Json(results, JsonRequestBehavior.AllowGet);
                  
                }
                case DashboardItemType.SdoSubject:
                {
                    var sdos = Uow.SdoSubjectRepo.GetAllWithChildren().Where(s => s.ParentId == subjectId && (s.SubjectGeoRefPoint == null || s.SubjectGeoRefPoint.Intersects(doa.DoaGeoArea))).ToList();
                    var filteredList = ApplyDoaFilters(sdos, doa).ToList();
                    var results = filteredList.Select(d => new DashboardItemViewModel
                    {
                        Name = d.Designator == null ? d.Name : d.Designator + " - " + d.Name,
                        HasChildren = d.Children.Any(),
                        Type = DashboardItemType.SdoSubject,
                        SubjectType = d.SdoEntityName,
                        SubjectId = d.Id
                    }).ToList();

                   return Json(results, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new List<DashboardItemViewModel>(), JsonRequestBehavior.AllowGet);
        }

        private void AddPath(List<SdoSubject> result, List<SdoSubject> source, DoaFilterType type, string[] pathParts, int pathPos, string parentId = null)
        {
            _log.Object(new
            {
                Method = "AddPath",
                Parameters = new
                {
                    result,
                    source,
                    type,
                    pathParts,
                    pathPos,
                    parentId
                }
            });
            
            var path = pathParts[pathPos];
            var regPattern = path.Replace("*", "\\w*").Replace("?", "\\w");
           
            try
            {
                var added = source.Where(s => (parentId == null || parentId == s.ParentId) && Regex.IsMatch(type == DoaFilterType.SubjectType ? s.SdoEntityName : s.Designator, regPattern, RegexOptions.IgnoreCase)).ToList();
                result.AddRange(added.Where(a => !result.Any(r => r.Id == a.Id)));
                pathPos++;

                if (pathPos < pathParts.Length)
                {
                    foreach (var parent in added)
                    {
                        AddPath(result, source, type, pathParts, pathPos, parent.Id);
                    }
                }
            }
            catch (Exception)
            {
                _log.Warn(new
                {
                    Method = "ApplyFilter",
                    Parameters = new
                    {
                        result,
                        source,
                        type,
                        pathParts,
                        pathPos,
                        parentId
                    },
                    Warning = "Invalid Filter value " + path
                });
            }
        }

        private List<SdoSubject> ApplyDoaFilters(List<SdoSubject> sdosOrg, Doa doa)
        {
            _log.Object(new
            {
                Method = "ApplyDOAFilters",
                Parameters = new
                {
                    sdosOrg,
                    doa
                }
            });

            var sdos = ApplySharedDoaFilters(sdosOrg, doa);
            var filters = doa.DoaFilters.OrderBy(f => f.Index).ToList();
            
            if (filters.Count == 0)
            {
                return sdos;
            }

            var result = sdos.ToList();
            foreach (var filter in filters)
            {
                ApplyFilter(result, sdosOrg, filter.Effect, filter.Type, filter.Value);
            }

            return result;
        }

        private void ApplyFilter(List<SdoSubject> result, List<SdoSubject> sdosOrg, DoaFilterEffect effect, DoaFilterType type, string value)
        {
            _log.Object(new
            {
                Method = "ApplyFilter",
                Parameters = new
                {
                    result,
                    sdosOrg,
                    effect,
                    type,
                    value
                }
            });

            var filterValues = value.Split(',');
            if (filterValues.Length == 0)
            {
                return;
            }
            switch (effect)
            {
                case DoaFilterEffect.Exclude:
                {
                    foreach (var v in filterValues)
                    {
                        var regPattern = v.Replace("*", "\\w*").Replace("?", "\\w");
                        try
                        {
                            result.RemoveAll(s => Regex.IsMatch(type == DoaFilterType.SubjectType ? s.SdoEntityName : s.Designator, regPattern, RegexOptions.IgnoreCase));
                        }
                        catch (Exception)
                        {
                            _log.Warn(new
                            {
                                Method = "ApplyFilter",
                                Parameters = new
                                {
                                    result,
                                    sdosOrg,
                                    effect,
                                    type,
                                    value
                                },
                                Warning = "Invalid Filter value " + v
                            });
                        }
                    }
                    break;
                }
                case DoaFilterEffect.Include:
                {
                    foreach (var v in filterValues)
                    {
                        var pathParts = v.Split('|');
                        if (pathParts.Length == 0)
                        {
                            break;
                        }
                        AddPath(result, sdosOrg, type, pathParts, 0);
                    }
                    break;
                }
            }
        }

        private List<SdoSubject> ApplySharedDoaFilters(List<SdoSubject> sdosOrg, Doa doa)
        {
            _log.Object(new
            {
                Method = "ApplySharedDOAFilters",
                Parameters = new
                {
                    sdosOrg,
                    doa
                }
            });

            var result = sdosOrg.ToList();

            if (doa.SharedDoaDefinitionId != null)
            {
                var filtersParametres = JsonConvert.DeserializeObject<Dictionary<string, string>>(doa.SharedDoaParameters);
                var sharedFilters = Uow.DoaSharedFilterRepo.GetFilterForSharedDoa((long)doa.SharedDoaDefinitionId).OrderBy(f => f.Index).ToList();
                foreach (var filter in sharedFilters)
                {
                    var filterValue = filter.Value;
                    foreach (var fv in filtersParametres.Keys)
                    {
                        filterValue = filterValue.Replace(fv, filtersParametres[fv]);
                    }
                    ApplyFilter(result, sdosOrg, filter.Effect, filter.Type, filterValue);
                }
            }

            return result;
        }
    }
}