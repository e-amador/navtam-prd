﻿using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;


namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.UI;

    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;

    using Filters;
    using Models;
    using Core.Common.Common;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;
    using Core.SqlServiceBroker;

    using Recaptcha;

    using Westwind.Globalization;

    public class AccountController : BaseController
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(BaseController));

        public AccountController(IUow uow, IClusterQueues clusterQueues)
            : base(uow, clusterQueues)
        {
        }

        [AllowAnonymous]
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RemoteLogin(string userName, string password)
        {
            var user = await UserManager.FindAsync(userName, password);
            if (user.IsApproved)
            {
                await SignInAsync(user, true);
                //FormsAuthentication.SetAuthCookie(UserName, false);
                return new ContentResult { Content = "logged in" };
            }
                
            return new ContentResult { Content = " not logged in" };
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> AcknowledgeSla(string currentUserName)
        {
            //TODO: Move to a business logic
            var currentuser = await Uow.UserProfileRepo.GetByUserNameAsync(currentUserName);
            var lastDigitalSla =  await Uow.DigitalSlaRepo.GetLastVersionAsync();

            currentuser.DigitalSla = lastDigitalSla;
            Uow.UserProfileRepo.Update(currentuser);
    
            await Uow.SaveChangesAsync();

            return Json("Successful", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> GetDigitalSlaApi()
        {
            var userProfile = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            ViewBag.CurrentUserName = userProfile.UserName;
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return PartialView("DigitalSla", await Uow.DigitalSlaRepo.GetLastVersionAsync());
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Message = TempData["userMessage"];
            ViewBag.DigitalSla = "no";
            return View(new Login());
        }

        [AllowAnonymous]
        public ViewResult RegistrationSuccessful()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ViewResult> ConfirmationEmail(string userId, string token)
        {
            try
            {
                var validEmailResult = await UserManager.ConfirmEmailAsync(userId, Uri.UnescapeDataString(token));

                var result = validEmailResult.Succeeded;
                if (result)
                {
                    //Uow.BeginTransaction();
                    var registration = await  Uow.RegistrationRepo.GetByUserIdAsync(userId);
                    if (registration != null)
                    {
                        var mto = CreateMtoForUserRegistration(MessageTransferObjectId.RegistrationNotificationEmail,null,
                            registration.Id, RegistrationStatus.Initial, null);
                            PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);

                         await Uow.SaveChangesAsync();
                    }
                    else
                        result = false;
                }
                ViewBag.ConfirmationStatus = result ? "Succeeded" : "Failed";
            }
            catch (Exception)
            {
                Uow.Rollback();
                ViewBag.ConfirmationStatus = "Failed";
            }

            return View();
        }

        [AllowAnonymous]
        public ViewResult ResetPassword(string userId, string token, bool passwordExpired)
        {
            try
            {
                if(!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(token))
                {
                    return 
                        View(new ResetPasswordModel
                        {
                            UserId = userId,
                            ConfirmationToken = token,
                            PasswordHasExpired = passwordExpired
                        });
                }
                //TODO: Redirect to a more specific error page
                return View("Error");
            }
            catch (Exception)
            {
                ViewBag.ConfirmationStatus = "Failed";
            }

            return View("ConfirmationEmail");
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await UserManager.FindByIdAsync(model.UserId);
                    if (user != null)
                    {
                        var result = await UserManager.ResetPasswordAsync(model.UserId, Uri.UnescapeDataString(model.ConfirmationToken), model.Password);
                        if (result.Succeeded)
                        {
                            user.LastPasswordChangeDate = DateTime.UtcNow;
                            result = await UserManager.UpdateAsync(user);
                            if (result.Succeeded )
                                return RedirectToAction("Login");
                        }
                        return View("Error"); //TODO: Redirect to a more specific error page
                    }
                }
                else
                    return View(model);
            }
            catch (Exception)
            {
                //TODO: logging here ??
            }

            return View("Error");  //TODO: Redirect to a more specific error page
        }



        public ActionResult Manage()
        {
            ViewBag.ReturnUrl = Url.Action("Manage");
            ViewBag.HideManageLink = true;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(LocalPassword model)
        {
            var userId = User.Identity.GetUserId();

            var userProfile = await UserManager.FindByIdAsync(userId);

            ViewBag.HasLocalPassword = userProfile != null && userProfile.PasswordHash != null;  

            ViewBag.ReturnUrl = Url.Action("Manage");
            ViewBag.HideManageLink = true;

            if (ViewBag.HasLocalPassword)
            {
                if (ModelState.IsValid)
                {

                    IdentityResult result = await UserManager.ChangePasswordAsync(userId, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                        return RedirectToAction("Manage", new { Message = ManageMessageType.ChangePasswordSuccess });
                        
                    AddErrors(result);
                    ModelState.AddModelError("", "[{EM_InvalidCurrentOrNewPassword}]");
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                var state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                        if (result.Succeeded)
                            return RedirectToAction("Manage", new { Message = ManageMessageType.SetPasswordSuccess });
                            
                        AddErrors(result);
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", "{[EM_UnableToCreateAccount}]-" +  User.Identity.Name);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return PartialView("ManagePassword");
        }

        public ActionResult ManagePassword(ManageMessageType? message)
        {
            ViewBag.Breadcrumb = DbRes.T("Manage Password", "AccountRes");

            ViewBag.StatusMessage =
             message == ManageMessageType.ChangePasswordSuccess ? "[{CM_PasswordChanged}]"
             : message == ManageMessageType.SetPasswordSuccess ? "[{CM_PasswordSet}]"
             : message == ManageMessageType.RemoveLoginSuccess ? "[{CM_ExternalLoginRemoved}]"
             : "";
            ViewBag.HasLocalPassword = true;
            return PartialView();
        }

        public async Task<ActionResult> ChangeExpiredRecordsFilter()
        {
            ViewBag.Breadcrumb = DbRes.T("Expired Records Filter", "AccountRes");
            
            ViewBag.ReturnUrl = Url.Action("Manage");
            ViewBag.HideManageLink = true;

            var expiredRecordsDateFilter = int.MinValue;

            var userProfile = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (userProfile.UserPreferenceId.HasValue)
            {
                var userPrefernceExpired = await Uow.UserPreferenceRepo.GetExpiredAsync(userProfile.UserPreferenceId.Value);
                if (userPrefernceExpired != null)
                    expiredRecordsDateFilter = userPrefernceExpired.ExpiredRecordsDateFilter;
            }

            ViewBag.currentExpiredRecordsFilter = expiredRecordsDateFilter != int.MinValue
                ? expiredRecordsDateFilter
                : (Convert.ToInt16(ConfigurationManager.AppSettings.Get("ExpiredRecordsFilter")));

            return PartialView("_ChangeDefaultExpiredRecordsFilterPartial");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeExpiredRecordsFilter(int? expiredRecordfilter)
        {
            ViewBag.HideManageLink = true;
            if (ModelState.IsValid)
            {
                var userProfile = await Uow.UserProfileRepo.GetByIdAsync(User.Identity.GetUserId(), false);

                if (userProfile != null)
                {
                    if (userProfile.UserPreferenceId.HasValue && expiredRecordfilter != null)
                    {
                        var userPreference = await Uow.UserPreferenceRepo.GetExpiredAsync(userProfile.UserPreferenceId.Value);
                        if (userPreference != null)
                        {
                            userPreference.ExpiredRecordsDateFilter = expiredRecordfilter.Value;
                            Uow.UserPreferenceRepo.Update(userPreference);
                            await Uow.SaveChangesAsync();

                            return RedirectToAction("Manage");
                        }
                    }
                    else
                    {
                        var userPreference = new UserPreference
                        {
                            ExpiredRecordsDateFilter =
                                Convert.ToInt16(ConfigurationManager.AppSettings.Get("ExpiredRecordsFilter"))
                        };

                        Uow.UserPreferenceRepo.Add(userPreference);
                        await Uow.SaveChangesAsync();

                        userProfile.UserPreferenceId = userPreference.Id;
                        await Uow.SaveChangesAsync();

                        return RedirectToAction("Manage");
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> ChangeDefaultLanguage()
        {
            ViewBag.Breadcrumb = DbRes.T("Language", "AccountRes");
            ViewBag.HideManageLink = true;

            var userprofile = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            ViewBag.ReturnUrl = Url.Action("Manage");
            ViewBag.LanguageList = Enum.GetNames(typeof(UserLanguage)).ToList();
            ViewBag.currentDefaultLanguage = ((UserLanguage)userprofile.DefaultLanguage).ToString();
            return PartialView("_ChangeDefaultLanguagePartial");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeDefaultLanguage(string selectedLanguage)
        {
            ViewBag.HideManageLink = true;
            if (ModelState.IsValid)
            {
                if (!String.IsNullOrEmpty(selectedLanguage))
                {
                    var userProfile = await Uow.UserProfileRepo.GetByIdAsync(User.Identity.GetUserId(), false);
                    try
                    {
                        userProfile.DefaultLanguage = Convert.ToInt16((UserLanguage)Enum.Parse(typeof(UserLanguage), selectedLanguage));

                        Uow.UserProfileRepo.Update(userProfile);
                        await Uow.SaveChangesAsync();
                        
                        Session["Culture"] = new CultureInfo(selectedLanguage);
                    }
                    catch
                    {
                        ModelState.AddModelError("", "[{EM_UknownError}]");
                    }
                }
            }
            return RedirectToAction("Manage");
        }

        [AllowAnonymous]
        public async Task<ActionResult> Register()
        {
            ViewBag.HideRegisterLink = true;
            ViewBag.RecaptchaKey = ConfigurationManager.AppSettings["RecaptchaPublicKey"];

            var digitalSla = await Uow.DigitalSlaRepo.GetLastVersionAsync();
            ViewBag.DigitalSla = digitalSla != null 
                ? digitalSla.Content
                : "";

            return View();
        }

        [HttpPost, RecaptchaControlMvc.CaptchaValidator]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(Register model, bool captchaValid, string captchaErrorMessage, bool condition)
        {
            var lastDigitalSla = await Uow.DigitalSlaRepo.GetLastVersionAsync();

            if (lastDigitalSla != null )
                ViewBag.DigitalSla = lastDigitalSla.Content;

            if (ModelState.IsValid)
            {
                if (!ValidRecaptcha())
                {
                    ModelState.AddModelError("captcha", DbRes.T("Invalid reCAPTCHA request. Missing response value.", "AccountRes"));


                    return View(model);
                }
                if (!condition)
                {
                    ModelState.AddModelError("Condition", DbRes.T("Please read and accept terms and conditions .", "AccountRes"));
                    return View(model);
                }
                try
                {
                    var user = new UserProfile
                    {
                        UserName = model.UserName,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.EmailAddress,
                        PhoneNumber = model.Telephone,
                        Address = !string.IsNullOrEmpty(model.Address) ? model.Address : model.OrgAddress,
                        Fax = model.Fax,
                        Position = model.Position,
                        LastPasswordChangeDate = DateTime.UtcNow
                    };

                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var registration = await CreateRegistrationObjectAsync(model);
                        
                        var userProfile = await UserManager.FindByNameAsync(model.UserName);

                        await UserManager.AddToRoleAsync(user.Id, CommonDefinitions.UserRole);

                        var tokenConfirmation = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                        Uow.BeginTransaction();
                        try
                        {
                            userProfile.RegistrationId = registration.Id;
                            Uow.UserProfileRepo.Update(userProfile);
                            await Uow.SaveChangesAsync();

                            //Publish Mto to target Queeue
                            var mto = CreateMtoForUserRegistration(MessageTransferObjectId.RegistrationNotificationEmail,null,
                                registration.Id, RegistrationStatus.ConfirmEmail, Uri.EscapeDataString(tokenConfirmation));

                            PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                            Uow.Commit();
                        }
                        catch(Exception)
                        {
                            Uow.Rollback();
                            throw;
                        }
                        return RedirectToAction("RegistrationSuccessful", "Account");
                    }
                        
                    AddErrors(result);

                    ViewBag.HideRegisterLink = true;
                    ViewBag.RecaptchaKey = ConfigurationManager.AppSettings["RecaptchaPublicKey"];

                    return View(model);
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Uow.Rollback();
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException("Fail to register user - " + message, raise);
                        }
                    }
                    throw raise;

                }
                catch (Exception err)
                {
                    ViewBag.HideRegisterLink = true;
                    ViewBag.RecaptchaKey = ConfigurationManager.AppSettings["RecaptchaPublicKey"];
                    ViewBag.errMessage = DbRes.T("ErrorInteralCode: Fail to register user - ", "AccountRes") + err.Message;
                    // ViewBag.Organizations = orgs;
                    return View(model);
                }
            }

            ViewBag.errMessage = DbRes.T("UnknownError: Fail to register user ", "AccountRes");
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [HandleAntiForgeryError]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();

            return RedirectToAction("Index", "Home");
        }


        [AllowAnonymous]
        public ActionResult ForgotPasswordRequest()
        {
            return View(new ForgotPasswordModel());
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPasswordRequest(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Username);
                if (user != null)
                {
                    if (string.Compare(user.Email, model.EmailAddress, StringComparison.OrdinalIgnoreCase) == 0 )
                    {
                        var tokenConfirmation = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        
                        //Publish Mto to target Queeue
                        var mto = CreateMtoForUserForgotPassword(MessageTransferObjectId.RegistrationNotificationEmail,
                            user.Id, Uri.EscapeDataString(tokenConfirmation));

                        PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);

                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError("InvalidResetPasswordRequest", "Invalid username/password combination");                                                
                }
            }

            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Location = OutputCacheLocation.None)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(Login model, string returnUrl)
        {
            try
            {
                //Login form not completed:
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", DbRes.T( "Incorrect login information!", "Home"));
                    return View(model);
                }

                var user = await UserManager.FindAsync(model.UserName, model.Password);

                //only allow admin / ccs user to page
                if (user != null && 
                    (UserManager.IsInRole(user.Id, CommonDefinitions.AdminRole) || 
                        UserManager.IsInRole(user.Id, CommonDefinitions.CcsRole) ||
                        UserManager.IsInRole(user.Id, CommonDefinitions.OrgAdminUser)
                ))
                {
                    if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                    {
                        ViewBag.ConfirmationStatus = "Unconfirmed";
                        return View("ConfirmationEmail") ;
                    }

                    var lastDigitalSla = await Uow.DigitalSlaRepo.GetLastVersionAsync();

                    DigitalSla userDigitalSla = null;
                    if( user.DigitalSlaId.HasValue )
                        userDigitalSla = await Uow.DigitalSlaRepo.GetByIdAsync(user.DigitalSlaId.Value);

                    Session["Culture"] = new CultureInfo(((UserLanguage)user.DefaultLanguage).ToString());

                    if (!user.IsApproved)
                    {
                        ModelState.AddModelError("", DbRes.T("AccountNotApprovedWaitForApproval", "AccountControllerRes"));
                        return View(model);
                    }

                    if (user.LockoutEnabled)
                    {
                        ModelState.AddModelError("", "Your account has been disabled.");
                        return View(model);
                    }
                    await SignInAsync(user, true);

                    if (userDigitalSla == null || Math.Abs(lastDigitalSla.Version - userDigitalSla.Version) > 0.1)
                        return RedirectToAction("Sla", "Account");

                    //if admin -> redirect to mfwic page                       
                    return RedirectToAction("Index", "AdminHome", new { area = "Admin" });
                }

                ModelState.AddModelError("", DbRes.T("Incorrect permissions; contact system administrator", "Home"));
                return View(model);
            }
            catch (InvalidOperationException err)
            {
                ModelState.AddModelError("", "Internal code: " + err.Message + ". Please contact system administrator.");
                return View(model);
            }
            catch (Exception err)
            {
                ModelState.AddModelError("", "Error happens when login user" + " " + model.UserName + "." + " " + err.Message + ". Please contact system administrator.");
                return View(model);
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> Sla()
        {
           return View(await Uow.DigitalSlaRepo.GetLastVersionAsync());
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> SlaClientResponse()
        {
            try
            {
                var isAccepted = Request.Form["chk-accepted"] == "on";

                if (!HttpContext.User.Identity.IsAuthenticated || !isAccepted)
                    return RedirectToAction("Login", "Account");
                     
                //Login form not completed:
                if (ModelState.IsValid)
                {
                    var userName = HttpContext.User.Identity.Name;
                    var user = await UserManager.FindByNameAsync(userName);

                    if (user != null)
                    {
                        var digitalSla = await Uow.DigitalSlaRepo.GetLastVersionAsync();
                        user.DigitalSlaId = digitalSla.Id;
                        await UserManager.UpdateAsync(user);

                        //if admin -> redirect to mfwic page                       
                        if (UserManager.IsInRole(user.Id, CommonDefinitions.AdminRole) ||
                            UserManager.IsInRole(user.Id, CommonDefinitions.CcsRole) ||
                            UserManager.IsInRole(user.Id, CommonDefinitions.OrgAdminRole))
                        {
                            return RedirectToAction("Index", "AdminHome", new { area = "Admin" });
                        }

                        //if not -> redirect to dashboard
                        return RedirectToAction("Index", "Dashboard");
                    }
                }
                else
                {
                    ModelState.AddModelError("", DbRes.T("Incorrect Sla information!", "Login"));
                }
            }
            catch (InvalidOperationException err)
            {
                ModelState.AddModelError("", "Internal code: " + err.Message + ". Please contact system administrator.");
            }
            catch (Exception err)
            {
                ModelState.AddModelError("", "Internal code: " + err.Message + ". Please contact system administrator.");
            }

            return View(await Uow.DigitalSlaRepo.GetLastVersionAsync());
        }

        private async Task SignInAsync(UserProfile user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }

        #region Helpers

        #region Recaptcha

        public bool ValidRecaptcha()
        {
            string response = Request["g-recaptcha-response"];

            string urlTemplate = ConfigurationManager.AppSettings["RecaptchaUrlTemplate"];
            string secretKey = ConfigurationManager.AppSettings["RecaptchaPrivateKey"];
            if (string.IsNullOrEmpty(urlTemplate) || string.IsNullOrEmpty(secretKey))
                throw new Exception("Invalid Recaptcha configuration.");

            string url = string.Format(urlTemplate, secretKey, response);
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            try
            {
                using (WebResponse webResponse = req.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        string jsonResponse = reader.ReadToEnd();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        ReCpatchaResponse data = js.Deserialize<ReCpatchaResponse>(jsonResponse);
                        return Convert.ToBoolean(data.success);
                    }
                }
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }

        class ReCpatchaResponse
        {
            public string success { get; set; }
        }

        #endregion

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task<Registration> CreateRegistrationObjectAsync(Register model)
        {
            var registration = await Uow.RegistrationRepo.GetByNameAsync(model.OrgName);
            if (registration != null )
                return registration;

            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
                throw new NullReferenceException("The registration user can't be found in the database");
                 
            registration = new Registration
            {
                Name = model.OrgName,
                Address = model.OrgAddress,
                EmailAddress = model.OrgEmailAddress,
                Telephone = model.OrgTelephone,
                TypeOfOperation = model.TypeOfOperation,
                HeadOffice = model.HeadOffice,
                Status = (int)RegistrationStatus.Initial,
                StatusTime = DateTime.Now,
                RegisteredUserId = user.Id,
                RegisteredUserName = model.UserName,
                RegisteredUserFirstName = model.FirstName,
                RegisteredUserLastName = model.LastName,
                RegisteredUserEmail = model.EmailAddress

            };
            Uow.RegistrationRepo.Add(registration);
            await Uow.SaveChangesAsync();
            return registration;
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", "Fail to register user - " + error);
            }
        }
        #endregion
    }
}