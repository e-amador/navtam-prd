﻿using NavCanada.Applications.NotamWiz.Web.Code.aeroRDS;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System.Linq;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;

    using Code.IdentityManagers;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;
    using Core.SqlServiceBroker;
    using Core.TaskEngine.Mto;
    using Code.Helpers;

    public class BaseController : AsyncController
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(BaseController));
        IDbResWrapper _dbRes;
        public IDbResWrapper DbRes
        {
            get { return _dbRes ?? (_dbRes = new DbResWrapper()); }
            set { _dbRes = value; }
        }

        IUtility _utility;

        public IUtility Utility
        {
            get { return _utility ?? (_utility = new Utility(this, Uow, UserManager, RoleManager)); }
            set { _utility = value; } 
        }

        IAeroRdsUtility _aeroRdsUtility;

        public IAeroRdsUtility AeroRdsUtility
        {
            get { return _aeroRdsUtility ?? (_aeroRdsUtility = new AeroRdsUtility()); }
            set { _aeroRdsUtility = value; } 
        }


        protected BaseController(IUow uow, IClusterQueues clusterQueues)
        {
            ClusterQueues = clusterQueues;
            Uow = uow;
        }

        protected BaseController(IUow uow, AppUserManager userManager, AppRoleManager roleManager)
        {
            Uow = uow;
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public AppUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<AppUserManager>(); }
            set { _userManager = value; }
        }

        public AppRoleManager RoleManager
        {
            get { return _roleManager ?? HttpContext.GetOwinContext().Get<AppRoleManager>(); }
            set { _roleManager = value; }
        }
        private AppUserManager _userManager;
        private AppRoleManager _roleManager;

        protected IUow Uow { get; private set; }
        protected IClusterQueues ClusterQueues { get; private set; }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (SkipAuthorization(filterContext))
                return;

            IPrincipal principal = HttpContext.User;

            if (principal.Identity == null || (principal.Identity != null && !principal.Identity.IsAuthenticated))
            {
                // auth failed, redirect to login page
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }

            var user = UserManager.FindByName(principal.Identity.Name);
            if (user == null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }

            if (user.DigitalSlaId.HasValue)
            {
                var digitalSla = Uow.DigitalSlaRepo.GetLastVersion();
                if (user.DigitalSlaId.Value != digitalSla.Id)
                    filterContext.Result = new HttpUnauthorizedResult();
            }
            else
                filterContext.Result = new HttpUnauthorizedResult();

        }

        protected override void Dispose(bool disposing)
        {
            Uow.Dispose();
            base.Dispose(disposing);

        }

        private static bool SkipAuthorization(AuthorizationContext filterContext)
        {
            return filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any()
                   || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any();
        }

        public async Task<bool> IsUserInRoleAsync(string userId, string roleName)
        {
            return await Utility.IsUserInRoleAsync(userId, roleName);
        }

        public async Task<Organization> GetUserOrganizationAsync()
        {
            return await Utility.GetUserOrganizationAsync();
        }

        public async Task<Doa> GetUserDoaAsync()
        {
            var e = await Utility.GetUserDoaAsync();

            return e;
        }

        public async Task<UserProfile> GetUser()
        {
            return await Utility.GetUser();
        }

        public async Task<string> GetConfigurationValue(string category, string name, string defaultValue)
        {

            return await Utility.GetConfigurationValue(category,  name, defaultValue);

         
        }

        public async Task SetViewBagRolesForCssAndAdminAsync(string userId)
        {
            await Utility.SetViewBagRolesForCssAndAdminAsync(userId);
        }

        public async Task<int> GetUserPreferenceExpiredRecordsFilter()
        {
            return await Utility.GetUserPreferenceExpiredRecordsFilter();
        }

        protected void PublishMtoToTargetQueue(MessageTransferObject mto, SqlServiceBrokerQueueSettings targetQueueName)
        {
            Utility.PublishMtoToTargetQueue(mto,targetQueueName);
        }

        protected MessageTransferObject CreateMtoForUserRegistration(MessageTransferObjectId messageId, string userId, long registrationId, RegistrationStatus regStatus, string tokenConfirmation)
        {
            return Utility.CreateMtoForUserRegistration(messageId, userId, registrationId, regStatus, tokenConfirmation);
        }

        protected MessageTransferObject CreateMtoForNotamChangeStatus(MessageTransferObjectId messageId, long notamId)
        {
            return Utility.CreateMtoForNotamChangeStatus(messageId, notamId);
        }

        protected MessageTransferObject CreateMtoForUserForgotPassword(MessageTransferObjectId messageId, string userId, string tokenConfirmation)
        {
            return Utility.CreateMtoForUserForgotPassword(messageId, userId, tokenConfirmation);
        }
    }
}