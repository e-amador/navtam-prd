﻿define(["jquery", "kendo/kendo.core.min"], function ($) {

    return {

        proposalVM: null,
        userText: "",                   // textarae
        Validator: null,                  // Form validator


        init: function (vm, prsr, vldtor) {

            thisNSDFormManager = this;

            thisNSDFormManager.proposalVM = vm;
            thisNSDFormManager.parser = prsr;
            thisNSDFormManager.Validator = vldtor;



            thisNSDFormManager.proposalVM.nsdTokens.bind("change", function (e) {
                debugger;
                thisNSDFormManager.proposalVM.validModel = false;
                $("#conditionSectionIcon").attr("src", "/Images/no.png");
                $("#reviewSectionIcon").attr("src", "/Images/no.png");


                if (thisNSDFormManager.Validator !== undefined && thisNSDFormManager.Validator !== null && !thisNSDFormManager.Validator.validate()) {
                    return;
                }

                var usedTokens = "";
                thisNSDFormManager.userText = "";
                var loop = true;
                while (loop) {
                    try {
                        thisNSDFormManager.parseAuto();
                        break;
                    } catch (e) {
                        var inputChanged = false;
                        $.each(e.expected, function (i) {

                            var key = this.description;

                            if (this.type !== "literal" && !key.endsWith("_CODE") && usedTokens.indexOf("<" + key + ">") >= 0) {
                                loop = false;
                            } else {



                                if (thisNSDFormManager.proposalVM.nsdTokens.hasOwnProperty(key)) {
                                    var value = thisNSDFormManager.proposalVM.nsdTokens[key];
                                    if (value !== null && value !== "") {
                                        usedTokens += "<" + key + ">";
                                        if (key.endsWith("_FT") || key.endsWith("_FTF")) {
                                            thisNSDFormManager.userText = thisNSDFormManager.userText + "|" + value + "|";
                                        } else {
                                            thisNSDFormManager.userText = thisNSDFormManager.userText + value;
                                        }
                                        inputChanged = true;
                                    }
                                }
                                else if (this.type !== "literal") {
                                    loop = false;
                                }
                            }
                        });

                        if (!inputChanged) {
                            $.each(e.expected, function (i) {

                                if (this.type === "literal") {
                                    thisNSDFormManager.userText = thisNSDFormManager.userText + this.value;
                                    inputChanged = true;
                                    return false;
                                }

                            });
                        }

                        if (!inputChanged) {
                            loop = false;
                        }
                    }
                }

            });

            thisNSDFormManager.proposalVM.nsdTokens.trigger('change');

        },

        /// loads SDO using OData
        ///@arguments <entityname> ie: airport (canadian/effective/latest/Ahp)
        ///@arguments <odata> odata selection statement ($top=5$skip=2)
        getSDOEntitys: function (entityName, odata) {

            var _url = "/SDO/" + entityName;
            if (odata !== undefined && odata !== "") {
                _url += "?" + odata;
            }
            var result = null;

            $.ajax({
                url: _url,
                type: "GET",
                dataType: 'json',
                async: false,
            })
                    .done(function (data) {

                        result = data.value;

                    })
                  .fail(function (jqXHR, textStatus, err) {

                  });

            return result;
        },

        parseAuto: function () {

            while (true) {
                try {
                    var resultTree = thisNSDFormManager.parser.parse(thisNSDFormManager.userText, thisNSDFormManager.proposalVM.PropsalType === "C" ? { startRule: "startCancel" } : {startRule: "start"});
                    this.proposalVM.validModel = true;
                    debugger;
                    $("#conditionSectionIcon").attr("src", "/Images/yes.png");
                    break;
                } catch (e) {
                    if (e.expected.length === 1 && e.expected[0].type === "literal") {
                        thisNSDFormManager.userText = thisNSDFormManager.userText + e.expected[0].value;
                        continue;
                    }

                    throw e;
                }
            }
        },

    }

});
