﻿
define(["jquery", "Resources", 'bingMaps', 'utilities', 'kendoExt'], function ($, res, bingMap, util) {

    var DOAEditor;
    var labels = {};

    return {

        model: null,

        init: function (lbls) {
            labels = lbls;
            DOAEditor = this;
            DOAEditor.model = kendo.observable($.extend({

            },

            $("#dataModel").data("model")));

            kendo.bind($('#DOAForm'), DOAEditor.model);

            // Create map.
            var mapOptions = {
                zoom: 3,
                enableClickableLogo: false,
                showTools: true,
            };

            DOAEditor.initUI();
            bingMap.init($("#mapContainer"), mapOptions, function () {
                $(bingMap).on("GeoChanged", DOAEditor.updateDOA);
                bingMap.setModelGeo(DOAEditor.model.DOAGeoArea);
            });
        },

        initUI: function () {

            $("#DOAWindow").kendoWindow({
                actions: ["Maximize", "Close"],
                width: 730,
                height: 620,
                resizable: true,
                pinned: true,
                animation: {
                    open: {
                        effects: { fadeIn: {} },
                        duration: 200,
                        show: true
                    },
                    close: {
                        effects: { fadeOut: {} },
                        duration: 600,
                        hide: true
                    }
                },
                close: function () {
                    $("#DOAWindow").data("kendoWindow").destroy();
                },
                visible: true,
                title: labels.doaWindow_title,
                modal: true,
            }).data("kendoWindow").open().center();

            $("#tabStrip").kendoTabStrip({
                animation: false,
                width: "100%",
                height: "100%",
                activate: function (e) {
                }
            });

            $("#assignToOrg").kendoMultiSelect({
                resizable: true,
                optionLabel: labels.assignToOrg_optionLabel,
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "Id",
                valuePrimitive: true,
                dataSource: {
                    type: "odata-v4",
                    pageSize: 20,
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "/odata/Organizations?",
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: function (data) {
                            return data["value"];
                        },
                        total: function (data) {
                            return data["odata.count"];
                        },
                        model: {
                            Id: "Id"
                        }
                    }
                },
            });

            function setDOAValidator() {
                DOAValidator = $("#DOAForm").kendoValidator({
                    messages: {
                        required: function (input) {
                            if (input.is("[name=txtName]")) {
                                return labels.doaForm_validator_nameRequired;
                            }
                            if (input.is("[name=txtDescription]")) {
                                return labels.doaForm_validator_descriptionRequired;
                            }
                            return "";
                        },
                        //hasItems: function () {
                        //    return labels.doaForm_validator_organizationRequired;
                        //}
                    },
                    rules: {
                        //hasItems: function (input) {
                        //    if (input.is("[name=assignToOrg]")) {
                        //        //Get the MultiSelect instance
                        //        var ms = input.data("kendoMultiSelect");
                        //        if (ms.value().length === 0) {
                        //            return false;
                        //        }
                        //    }
                        //    return true;
                        //}
                    }
                }).data("kendoValidator");
            }

            setDOAValidator();

            function warningGeoRegion(show) {
                // Luis - I have to do it manually, simulating Kendo, because I didn't find
                // the way of make Kendo validator works without to be connected with
                // an element.
                if (show) {
                    $("#mapContainer").addClass('borderInvalid');
                    $("[name='spanForMapPlace']").addClass("k-widget k-tooltip k-tooltip-validation");
                    $("[name='spanForMapPlace']").prop("role", "alert");
                    $("[name='spanForMapPlace']").append('<span class="k-icon k-warning"></span>');
                    $("[name='spanForMapPlace']").append(labels.doaForm_validator_geoRequired);
                    $("[name='spanForMapPlace']").show();
                } else {
                    $("#mapContainer").removeClass('borderInvalid');
                    $("[name='spanForMapPlace']").hide();
                    $("[name='spanForMapPlace']").removeClass("k-widget k-tooltip k-tooltip-validation");
                    $("[name='spanForMapPlace']").removeProp("role");
                    $("[name='spanForMapPlace']").text("");
                }
            }

            $("#btnReset").on('click', DOAEditor.Reset);
            $("#btnDiscard").on('click', DOAEditor.Discard);
            $("#btnSave").on("click", function (e) {
                if (DOAValidator.validate() === false) {
                    e.preventDefault();
                } else {
                    warningGeoRegion(false);
                    if (DOAEditor.model.DOAGeoArea === null ||
                        DOAEditor.model.DOAGeoArea.Coordinates === null ||
                        DOAEditor.model.DOAGeoArea.Coordinates.length === 0) {
                        warningGeoRegion(true);
                        e.preventDefault();
                    } else {
                        DOAEditor.Save();
                    }
                }
            });

            $("#doaFiltersGrid").kendoGrid({
                resizable: true,
                selectable: true,
                Filterable: false,
                Groupable: false,
                scrollable: true,
                pageSize: 10,
                batch: false,
                pageable: false,
                editable: {
                    mode: "popup"
                },
                toolbar: [
                      {
                          name: "addFilter",
                          text: labels.doaFiltersGrid_addLabel
                      },
                      {
                          name: "editFilter",
                          text: labels.doaFiltersGrid_editLabel

                      },
                      {
                          name: "deleteFilter",
                          text: labels.doaFiltersGrid_deleteLabel

                      },
                       {
                           name: "upFilter",
                           text: labels.doaFiltersGrid_moveUpLabel

                       },
                        {
                            name: "downFilter",
                            text: labels.doaFiltersGrid_moveDownLabel

                        }

                ],
                columns: [
                   // { field: "Index", title: RES.dbRes("Index") },
                    {
                        field: "Type",
                        title: labels.doaFiltersGrid_filterOnLabel,
                        values: [{ "value": 0, "text": labels.doaFiltersGrid_subjectTypeLabel }, { "value": 1, "text": labels.doaFiltersGrid_specificSubjectLabel }]
                    },
                    {
                        field: "Effect",
                        title: labels.doaFiltersGrid_effectLabel,
                        values: [{ "value": 0, "text": labels.doaFiltersGrid_includeLabel }, { "value": 1, "text": labels.doaFiltersGrid_excludeLabel }]
                    },
                    { field: "Value", title: labels.doaFiltersGrid_subjectTypeLabel },
                ],
                dataSource: {
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                //Index: { type: "number", defaultValue: 0, validation: { required: true, min: 1 }, editable: true },
                                Type: { type: "number", defaultValue: 0, validation: { required: true }, editable: true },
                                Effect: { type: "number", defaultValue: 0, validation: { required: true }, editable: true },
                                Value: { type: "string", editable: true },
                            }
                        }
                    },
                    sort: {
                        field: "Index", dir: "asc"
                    },
                    data: DOAEditor.model.DoaFilters,

                },
                dataBound: function (e) {
                    $(".k-grid-editFilter").hide();
                    $(".k-grid-deleteFilter").hide();
                    $(".k-grid-upFilter").hide();
                    $(".k-grid-downFilter").hide();
                },
                change: function () {
                    var grid = $("#doaFiltersGrid").data("kendoGrid");
                    var row = grid.select();
                    var data = grid.dataItem(row);
                    if (data) {
                        $(".k-grid-editFilter").show();
                        $(".k-grid-deleteFilter").show();
                        var dataRows = grid.items();
                        var rowIndex = dataRows.index(grid.select());
                        if (rowIndex > 0) {
                            $(".k-grid-upFilter").show();
                        }

                        if (rowIndex < dataRows.length - 1) {
                            $(".k-grid-downFilter").show();
                        }

                    } else {

                        $(".k-grid-editFilter").hide();
                        $(".k-grid-deleteFilter").hide();
                        $(".k-grid-upFilter").hide();
                        $(".k-grid-downFilter").hide();

                    }
                }
            });

            $(".k-grid-addFilter").on('click', function () {
                var grid = $("#doaFiltersGrid").data("kendoGrid");
                var dataSource = grid.dataSource;
                var total = dataSource.data().length;
                dataSource.insert(total, {});
                dataSource.page(dataSource.totalPages());
                grid.editRow(grid.tbody.children().last());
            });

            $(".k-grid-editFilter").on('click', function () {
                var grid = $("#doaFiltersGrid").data("kendoGrid");
                var row = grid.select();
                if (row) {
                    grid.editRow(grid.tbody.children().last());
                }
            });

            $(".k-grid-deleteFilter").on('click', function () {
                var grid = $("#doaFiltersGrid").data("kendoGrid");
                var row = grid.select();
                if (row) {
                    grid.removeRow(row);
                }
            });

            $(".k-grid-upFilter").on('click', function up(uid) {
                var grid = $("#doaFiltersGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                var index = grid.dataSource.indexOf(dataItem);
                var newIndex = Math.max(0, index - 1);

                if (newIndex != index) {
                    grid.dataSource.remove(dataItem);
                    grid.dataSource.insert(newIndex, dataItem);
                }
                grid.select(grid.table.find('tr[data-uid="' + dataItem.uid + '"]'));
                return false;
            });

            $(".k-grid-downFilter").on('click', function down(uid) {
                var grid = $("#doaFiltersGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                var index = grid.dataSource.indexOf(dataItem);
                var index = grid.dataSource.indexOf(dataItem);
                var newIndex = Math.min(grid.dataSource.total() - 1, index + 1);

                if (newIndex != index) {
                    grid.dataSource.remove(dataItem);
                    grid.dataSource.insert(newIndex, dataItem);
                }

                grid.select(grid.table.find('tr[data-uid="' + dataItem.uid + '"]'));

                return false;
            });
        },
        Discard: function () {
            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.extDialog_confirm_title,
                message: labels.extDialog_confirm_message,
                width: '400px',
                height: '150px',
                icon: "k-Ext-warning",
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
             ).done(function (response) {
                 if (response.button === 'OK') {
                     $("#DOAWindow").data("kendoWindow").close();
                 }
             });

        },
        Save: function (event) {
            var url = DOAEditor.model.Id == 0 ? "/DomainOfAuthority/Create" : "/DomainOfAuthority/Update";
            var adding = DOAEditor.model.Id == 0;

            $.ajax({
                data: { doavm: DOAEditor.model.toJSON() },
                cache: false,
                type: "POST",
                dataType: "json",
                url: url,
                success: function (data, status, jqXHR) {
                    if (data.Success) {

                        kendo.ui.ExtAlertDialog.show({
                            title: labels.extDialog_success_title,
                            message: adding ? labels.extDialog_success_creation : labels.extDialog_success_update,
                            icon: "k-Ext-information"
                        });
                        $("#DOAWindow").data("kendoWindow").close();
                        if ($('#doaGrid')) {
                            $('#doaGrid').data('kendoGrid').dataSource.read();
                            $('#doaGrid').data('kendoGrid').refresh();
                        }
                    } else {
                        util.showErrorMessage(adding ? labels.extDialog_fail_creation : lables.extDialog_fail_update, data);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    util.showErrorMessage(adding ? labels.extDialog_fail_creation : lables.extDialog_fail_update, textStatus);
                }
            });
        },

        Reset: function () {
            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.extDialog_confirm_title,
                message: labels.extDialog_reset_DOA,
                width: '400px',
                height: '150px',
                icon: "k-Ext-warning",
            })
                 ).done(function (response) {
                     if (response.button === 'OK') {
                         bingMap.clearAll();
                     }

                 });
        },

        updateDOA: function (event, geometry) {
            DOAEditor.model.set("DOAGeoArea", geometry);
        }
    }
});



