﻿
define(["jquery", "Resources", "utilities", "kendoExt", "kendo/kendo.grid.min"], function ($, res, util, bing) {
   
    var DOAManager;
    var labels = {};

    return {
        init: function (lbls) {
            labels = lbls;
            DOAManager = this;
            DOAManager.initUI();
        },

        initUI: function () {
            $("#doaGrid").kendoGrid({
                resizable: true,
                selectable: true,
                Filterable: true,
                Groupable: true,
                scrollable: true,
                pageSize: 10,
                batch: false,
                toolbar: [
                    { template: '<a id="CreateDOA" class="k-button" >' + labels.doaGrid_createDoA_title + '</a>' },
                    { template: '<a id="EditDOA" class="k-button" >' + labels.doaGrid_editDOA_title + '</a>' },
                    { template: '<a id="DeleteDOA" class="k-button" >' + labels.doaGrid_deleteDOA_title + '</a>' }
                ],
                pageable: { refresh: true, buttonCount: 5 },
                columns: [
                    { field: "Name", title: labels.doaGrid_columnName_title },
                    { field: "Description", title: labels.doaGrid_columnDescription_title }
                ],
                dataSource: {
                    schema: { model: { id: "Id" }, data: function (data) { return data.Data; },
                    },
                    transport: {
                        read: { url: "DomainOfAuthority/GetDOAs", dataType: "json", type: "POST" },
                    }
                },
                change: function () {
                    var grid = $("#doaGrid").data("kendoGrid");
                    var row = grid.select();
                    var data = grid.dataItem(row);
                    if (data) {
                        $("#EditDOA").show();
                        $("#DeleteDOA").show();
                    } else {
                        $("#EditDOA").hide();
                        $("#DeleteDOA").hide();
                    }
                },
                dataBound: function onDataBound(e) {
                    $("#EditDOA").hide();
                    $("#DeleteDOA").hide();
                    var grid = e.sender;
                    var data = grid.dataSource.view();
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Name === "Canada Airspace") {
                            var selector = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] .k-icon");
                            var deleteButton = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] .k-button");
                            var editButton = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] .k-button");
                            selector.remove();
                            deleteButton.remove();
                            editButton.remove();
                        }
                    }
                }
            });

            $("#CreateDOA").on('click', function () {
                $.ajax({
                    url: "DomainOfAuthority/Create",
                    success: function (data) {
                        $("#controlWindow").html("");
                        $("#controlWindow").html(data);
                    },
                    cache: false
                });
            });

            $("#DeleteDOA").on('click', function () {
                var grid = $("#doaGrid").data("kendoGrid");
                var selectedRow = grid.select();
                var dataItem = grid.dataItem(selectedRow);
                if (!dataItem) {
                    kendo.ui.ExtAlertDialog.show({
                        title: labels.deleteDOA_title,
                        message: labels.deleteDOA_message,
                        icon: "k-Ext-warning"
                    });
                    return;
                } 

                $.when(kendo.ui.ExtOkCancelDialog.show({
                    title: labels.extDialog_confirm_title,
                    message: labels.extDialog_deleteDOA_message + dataItem.Name + "?",
                    width: '400px',
                    height: '150px',
                    icon: "k-Ext-warning",
                    //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
                })).done(function (response) {
                    if (response.button === 'OK') {
                        $.ajax({
                            type: "DELETE",
                            url: "DomainOfAuthority/Delete",
                            data: { doaId: dataItem.Id },
                            success: function (data, status, jqXHR) {
                                if (data.Success) {
                                    util.showSuccessMessage(labels.extDialog_deleteDOA_confirmation);
                                    $('#doaGrid').data('kendoGrid').dataSource.read();
                                    $('#doaGrid').data('kendoGrid').refresh();

                                } else {
                                    util.showErrorMessage(labels.extDialog_deleteDOA_error, data.Error);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                util.showErrorMessage(labels.extDialog_deleteDOA_error, errorThrown);
                            }
                        });
                    }  
                });
            });


            $("#EditDOA").on('click', function () {
                var grid = $("#doaGrid").data("kendoGrid");
                var selectedRow = grid.select();
                var dataItem = grid.dataItem(selectedRow);
                if (!dataItem) {
                    kendo.ui.ExtAlertDialog.show({
                        title: labels.extDialog_editDOA_title,
                        message: labels.extDialog_editDOA_message,
                        icon: "k-Ext-warning"
                    });
                    return;
                }

                $.ajax({
                    url: "DomainOfAuthority/Edit",
                    data: { doaId: dataItem.Id },
                    success: function (data) {
                        $("#controlWindow").html("");
                        $("#controlWindow").html(data);
                    },
                    cache: false
                });
            });
        }
    }
});



