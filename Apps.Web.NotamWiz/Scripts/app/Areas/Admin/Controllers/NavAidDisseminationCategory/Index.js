﻿
define(["jquery", "Resources", "bingMapsV8", "kendo/kendo.grid.min", "kendo/kendo.tooltip.min", "kendo/kendo.tabstrip.min"], function ($, res, bingMap) {

    var disseminationLevels = $("#disseminationLevels").data("model");
    var thisModel = null;
    var labels = {};

    return {
        init: function (lbs) {
            thisModel = this;
            labels = lbs;

            // Create map.
            var mapOptions = {
                zoom: 4,
            };

            bingMap.init($("#mapView"), mapOptions, function () {
                $("#mapViewTab").show();
                thisModel.initUI();
                /*Microsoft.Maps.Events.addHandler(bingMap.map, 'click', function (e) {
                    if (e.targetType == 'map') {
                        $("#menu").data("kendoContextMenu").close();
                    }
                });*/
            });


        },



        ahpsLoadedIntoMap: false,
        initUI: function () {

            $("#tabStrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
                activate: function (e) {
                    //if (e.contentElement.id === "tabStrip-2" && !thisModel.ahpsLoadedIntoMap) {
                    //  thisModel.ahpsLoadedIntoMap = true;
                    //thisModel.LoadAhpsIntoMap();
                    //}
                }
            });

            thisModel.ahpsLoadedIntoMap = true;
            thisModel.LoadAhpsIntoMap();


            $("#aerodromeDisseminationCategoryGrid").kendoGrid({
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [5, 10, 25, 99999],
                    buttonCount: 5,
                    messages: {
                        display: labels.displayLabel,
                        empty: labels.noData,
                        page: labels.enterPage,
                        of: labels.ofLabel,
                        itemsPerPage: labels.itemsPerPage,
                        first: labels.firstPage,
                        last: labels.lastPage,
                        next: labels.nextPage,
                        previous: labels.previousPage,
                        refresh: labels.refreshTheGrid,
                        morePages: labels.morePages
                    }
                },
                scrollable: true,
                filterable: true,
                sortable: true,
                columns: [
                    {
                        field: "AhpCodeId", title: labels.aerodromeCode, width: 150, editable: false,
                        template: "<span>#: AhpCodeId #</span> <img src='/Images/info.png' style='float:right'/>"
                    },
                    {
                        field: "AhpName",
                        title: labels.aerodromeName,
                        width: "*",
                        editable: false
                    },
                    {
                        field: "DisseminationCategory",
                        title: labels.disseminationCategory,
                        width: 200,
                        values: disseminationLevels
                    }
                ],
                toolbar: [
                    //{ name: "create", text: labels.addLabel },
                    { name: "save", text: labels.saveLabel },
                    { name: "cancel", text: labels.cancelLabel }
                ],
                editable: {
                    mode: "incell"
                },
                dataSource: {
                    serverPaging: true,
                    schema: {
                        data: function (data) { return data.Data; },
                        total: 'Total',
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                AhpMid: { editable: false },
                                AhpCodeId: { editable: false },
                                AhpName: { editable: false },
                                DisseminationCategory: { field: "DisseminationCategory", type: "number", defaultValue: 0, validation: { required: true } },
                            }
                        }
                    },
                    transport: {
                        create: { url: "AerodromeDisseminationCategory/ADC_Create", dataType: "json", type: "POST" },
                        read: { url: "AerodromeDisseminationCategory/ADC_Read", dataType: "json", type: "POST" },
                        update: { url: "AerodromeDisseminationCategory/ADC_Update", dataType: "json", type: "POST" },
                        //destroy: { url: "AerodromeDisseminationCategory/ADC_Destroy", dataType: "json", type: "POST" }

                    }
                },
                edit: function (e) {
                    $("#aerodromeDisseminationCategoryGrid").find(".k-grid-save-changes").show();
                    $("#aerodromeDisseminationCategoryGrid").find(".k-grid-cancel-changes").show();
                },
                dataBound: function (e) {
                    $("#aerodromeDisseminationCategoryGrid").find(".k-grid-save-changes").hide();
                    $("#aerodromeDisseminationCategoryGrid").find(".k-grid-cancel-changes").hide();
                },



            });


            $("#aerodromeDisseminationCategoryGrid").kendoTooltip({
                filter: "td:nth-child(1)", //this filter selects the first column cells
                position: "right",
                showOn: "click",
                content: function (e) {

                    var dataItem = $("#aerodromeDisseminationCategoryGrid").data("kendoGrid").dataItem(e.target.closest("tr"));
                    var content = labels.couldNotLoadData + dataItem.AhpCodeId;

                    $.ajax({
                        url: '/Info/Ahp',
                        type: "POST",
                        data: { mid: dataItem.AhpMid },
                        cache: false,
                        async: false,
                        success: function (data) {
                            content = data;
                        }
                    });

                    return content;
                },
                show: function () {
                    //this.refresh();
                }
            }).data("kendoTooltip");



            /* $("#menu").kendoContextMenu({
                 orientation: "vertical",
                 target: "#mapView",
                 filter: ".NOTACLASS",
                 animation: {
                     open: { effects: "fadeIn" },
                     duration: 500
                 },
                 select: function (e) {
 
                     if (thisModel.selectedAhpOnMap) {
                         debugger;
                         var newCategory = null;
 
                         if (e.item.className.indexOf("_i_") !== -1) {
                             newCategory = "International";
                         } else if (e.item.className.indexOf("_u_") !== -1) {
                             newCategory = "US";
 
                         } else if (e.item.className.indexOf("_n_") !== -1) {
                             newCategory = "National";
                         }
 
                         if (newCategory !== thisModel.selectedAhpOnMap.feature["Dissemination Category"]) {
                           
                             updateSelectedAhpCategory(newCategory);
 
                         }
 
                         thisModel.selectedAhpOnMap = null;
                     }
                 }
             });
             
 
             $("#menuContainer").show();
             */

            $("#tabStrip").show();

        },
        updateSelectedAhpCategory: function (newCategory) {
            $.ajax({
                url: '/AerodromeDisseminationCategory/UpdateAhpCategory',
                data: { mid: thisModel.selectedAhpOnMap.Mid, category: newCategory },
                type: "POST",
                cache: false,
                async: false,
                success: function (data) {
                    if (data.Success) {
                        thisModel.selectedAhpOnMap.DisseminationCategory = newCategory;
                        thisModel.setAhpIcon(thisModel.selectedAhpOnMap, true);
                        $("#selectDisseminationCategory").data("kendoDropDownList").select(function (dataItem) {
                            return dataItem.value === newCategory;
                        });
                    }
                }
            });
        },



        selectedAhpOnMap: null,
        LoadAhpsIntoMap: function () {


            $("#selectDisseminationCategory").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: disseminationLevels,
                change: function (e) {
                    //debugger;
                    if (thisModel.selectedAhpOnMap) {
                        var newCategory = $('#selectDisseminationCategory').data("kendoDropDownList").dataItem($('#selectDisseminationCategory').data("kendoDropDownList").select()).value
                        thisModel.updateSelectedAhpCategory(newCategory);
                    }
                }
            });

            var ahpData = thisModel.loadAhpData();
            Microsoft.Maps.loadModule('Microsoft.Maps.Clustering', function () {
                var clusterLayer = new Microsoft.Maps.ClusterLayer(ahpData, { gridSize: 100 });
                bingMap.map.layers.insert(clusterLayer);
            });

            $("#selectAhp").kendoDropDownList({
                optionLabel: labels.selectAD,
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "Mid",
                dataSource: new kendo.data.DataSource({ data: ahpData }),
                change: function (e) {
                    thisModel.selectAhp();
                }
            });



            $("#actionPanel").show();


        },

        setAhpIcon: function (pin, selected) {

            if (pin) {
                if (selected) {
                    pin.setOptions({ icon: pin.DisseminationCategory == 0 ? "/Images/pushpinRedSelect.png" : pin.DisseminationCategory == 1 ? "/Images/pushpinBlueSelect.png" : "/Images/pushpinGreenSelect.png", anchor: new Microsoft.Maps.Point(13, 41) });
                } else {
                    pin.setOptions({ icon: pin.DisseminationCategory == 0 ? "/Images/pushpinRed.png" : pin.DisseminationCategory == 1 ? "/Images/pushpinBlue.png" : "/Images/pushpinGreen.png", anchor: new Microsoft.Maps.Point(8, 26) });
                }
            }
        },

        loadAhpData: function () {
            var result = null;
            $.ajax({
                dataType: "json",
                url: '/AerodromeDisseminationCategory/GetAhpsLocation',
                type: "Get",
                cache: false,
                async: false,
                success: function (data) {
                    result = $.map(data, function (n, i) {
                        var location = new Microsoft.Maps.Location(n.Latitude, n.Longitude);
                        var pin = new Microsoft.Maps.Pushpin(location, { title: n.Code, subTitle: n.Title });
                        var item = $.extend(pin, n);
                        thisModel.setAhpIcon(item);
                        // Add a handler to the pushpin drag
                        Microsoft.Maps.Events.addHandler(item, 'mouseup', function (e) {

                            if (thisModel.infobox) {
                                thisModel.infobox.setMap(null);
                            }

                            thisModel.infobox = new Microsoft.Maps.Infobox(e.target.getLocation(), {
                                title: e.target.Code, description: e.target.Title, actions: [{
                                    label: labels.National, eventHandler: function () {
                                        thisModel.infobox.setMap(null);
                                        thisModel.updateSelectedAhpCategory(0);


                                    }
                                },
                                {
                                    label: labels.US, eventHandler: function () {
                                        thisModel.infobox.setMap(null);
                                        thisModel.updateSelectedAhpCategory(1);

                                    }
                                },
                                {
                                    label: labels.International, eventHandler: function () {
                                        thisModel.infobox.setMap(null);
                                        thisModel.updateSelectedAhpCategory(2);
                                    }
                                }]
                            });

                            thisModel.infobox.setMap(bingMap.map);


                            thisModel.setAhpIcon(thisModel.selectedAhpOnMap);
                            thisModel.selectedAhpOnMap = e.target;
                            thisModel.setAhpIcon(thisModel.selectedAhpOnMap, true);
                            $("#selectAhp").data("kendoDropDownList").select(function (dataItem) {
                                return dataItem.Mid === thisModel.selectedAhpOnMap.Mid;
                            });
                        });
                        return item;
                    });
                }
            });

            return result;
        },

        resetSelect: function () {

            if (thisModel.selectedAhpOnMap) {
                thisModel.selectedAhpOnMap._options._state = 4;
            }
        },
        selectAhp: function () {
            thisModel.resetSelect();

            var item = $('#selectAhp').data("kendoDropDownList").dataItem($('#selectAhp').data("kendoDropDownList").select());
            thisModel.selectedAhpOnMap = item;
            if (thisModel.selectedAhpOnMap) {

                $("#selectDisseminationCategory").data("kendoDropDownList").select(function (dataItem) {
                    return dataItem.value === item.DisseminationCategory;
                });

                bingMap.map.setView({
                    bounds: Microsoft.Maps.LocationRect.fromLocations(thisModel.selectedAhpOnMap.getLocation()),
                });
                bingMap.map.setView({
                    zoom: 13
                })
                thisModel.setAhpIcon(thisModel.selectedAhpOnMap, true);
            }
        }

    };
});
