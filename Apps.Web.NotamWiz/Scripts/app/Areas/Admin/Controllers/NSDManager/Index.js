﻿define(["jquery", "Resources", "kendo/kendo.grid.min", "kendo/kendo.multiselect.min"], function ($, res) {
    var nsdManagerObject, orgs;
    var labels = {};
    return {

        init: function (lbs) {
            labels = lbs;
            orgs = $("#model").data("orgs");
            nsdManagerObject = this;
            nsdManagerObject.initUI();
        },

        initUI: function () {

            var grid = $('#nsd_grid').kendoGrid({
                dataSource: {
                    transport: {
                        read: { url: "/NSDManager/GetNsdList", dataType: "json" },
                    },
                    schema: {
                        data: "Data",
                        model: {
                            id: "Id",
                            Active: { default: false }
                            //Orgs: {type:"string"}
                        },
                    },
                    change: function (e) {
                        var dirty = false;
                        $.each(this._data, function () {
                            if (this.dirty == true) {
                                dirty = true;
                            }
                        });

                        if (this._destroyed.length > 0) dirty = true;

                        if (dirty) {
                            $("#nsd_grid").find(".k-grid-save-changes").show();
                            $("#nsd_grid").find(".k-grid-cancel-changes").show();
                        } else {
                            $("#nsd_grid").find(".k-grid-save-changes").hide();
                            $("#nsd_grid").find(".k-grid-cancel-changes").hide();
                        }
                    }
                },
                columns: [
                    { field: "Name" },
                    { field: "Category" },
                    { field: "Description" },
                    {
                        field: "Active",
                        template: '<input type="checkbox" #= Active ? "checked=checked" : "" #  disabled="disabled" ></input>',
                        width: 100
                    },

                    {
                        field: "LimitedToOrganizatations",
                        template: function (data) {
                            var res = [];
                            if (data.LimitedToOrganizatations !== undefined) {
                                $.each(data.LimitedToOrganizatations, function (idx, elem) {
                                    res.push(elem.Name);
                                });
                            }

                            if (res.length === 0) {
                                res.push("<ALL>");
                            }

                            return res.join(", ");
                        },
                        editor: function (container, options) {
                            $("<select multiple='multiple' data-bind='value : LimitedToOrganizatations'/>").appendTo(container).kendoMultiSelect({
                                dataTextField: "Name",
                                dataValueField: "Id",
                                dataSource: orgs,
                                change: function () {
                                    // Get selected cell
                                    var grid = $('#nsd_grid').data("kendoGrid");
                                    var selected = grid.select();
                                    var item = grid.dataItem(selected);
                                    item.set("LimitedsAccess", item.LimitedToOrganizatations !== null && item.LimitedToOrganizatations.length > 0)
                                }
                            });
                        },
                        filterable: false
                    }
                ],
                toolbar: [
                    { name: "save", text: labels.saveChanges },
                    { name: "cancel", text: labels.discarChanges }
                ],
                filterable: {
                    mode: "menu"
                },

                saveChanges: function (e) {

                    var nsdList = $("#nsd_grid").data("kendoGrid").dataSource.data();


                    $.ajax({
                        contentType: 'application/json',
                        type: 'POST',
                        url: 'NSDManager/UpdateList',
                        data: JSON.stringify(nsdList),
                        success: function (success) {
                            var kGrid = $('#nsd_grid').data("kendoGrid");
                            if (success) {

                                kGrid.refresh();
                                $("#nsd_grid").find(".k-grid-save-changes").hide();
                                $("#nsd_grid").find(".k-grid-cancel-changes").hide();
                            } else {
                                kGrid.refresh();
                                $("#nsd_grid").find(".k-grid-save-changes").hide();
                                $("#nsd_grid").find(".k-grid-cancel-changes").hide();
                            }
                        },
                        error: function (e, ctx, d) {
                            var kGrid = $('#nsd_grid').data("kendoGrid");
                            kGrid.refresh();
                            $("#nsd_grid").find(".k-grid-save-changes").hide();
                            $("#nsd_grid").find(".k-grid-cancel-changes").hide();
                        }
                    });
                },

                detailTemplate: $("#nsdVersionsTemplate").html(),
                detailInit: nsdManagerObject.nsdDetailInit,
                pageSize: 10,
                batch: false,
                pageable: true,
                scrollable: true,
                selectable: true,
                editable: "incell"

            }).data("kendoGrid");
        },

        nsdDetailInit: function (e) {
            var detailRow = e.detailRow;

            detailRow.find(".nsdVersionsGrid").kendoGrid({
                pageable: true,
                sortable: true,
                animation: {
                    open: { effects: "fadeIn" }
                },
                columns: [
                    { field: "Description", title: labels.descriptionLabel },
                    { field: "ReleaseDate", title: labels.releaseDate, template: "#= kendo.toString(kendo.parseDate(ReleaseDate, 'yyyy-MM-dd hh:mm'), 'dd MMM yyyy hh:mm') #" },
                    { field: "SupportedSubjects", title: labels.supportedSubjects },
                    { field: "Version", title: labels.versionLabel },
                ],
                dataSource: {
                    schema: {
                        model: { id: "Id" },
                        data: function (data) { return data.Data; },

                    },
                    transport: {
                        sort: { field: "Version", dir: "desc" },
                        read: { url: "NSDManager/GetNSDVersions", dataType: "json", type: "POST", data: { "nsdId": e.data.Id } }
                    }
                },
            });
        },

        orgsFilter: function (element) {

            var menu = $(element).parent();
            menu.find(".k-filter-help-text").text("Show records for organization:");
            menu.find("[data-role=dropdownlist]").remove();

            var multiSelect = element.kendoMultiSelect({
                dataTextField: "Name",
                dataValueField: "Id",
                dataSource: orgs
            }).data("kendoMultiSelect");

            menu.find("[type=submit]").on("click", { widget: multiSelect }, filterByOrg);
        },

        filterByOrg: function (e) {

            var selectedOrgs = e.data.widget.dataItems();
            var filter = { logic: "or", filters: [] };
            for (var i = 0; i < selectedOrgs.length; i++) {
                //filter.filters.push({ field: "Orgs", operator: "eq", value: selectedOrgs[i].Name })
            }
            $("#nsd_grid").data("kendoGrid").dataSource.filter(filter);
        }
    }
});
