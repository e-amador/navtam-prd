﻿
define(["jquery", "Resources", 'bingMaps', 'kendoExt'], function ($, res, bingMap) {

    var model = null;
    var thisBilingual = null;
    var labels = {};

    return {
        init: function (lbs) {
            labels = lbs;
            debugger;
            model = $("#dataModel").data("model");
            thisBilingual = this;

            var mapOptions = {
                center: new Microsoft.Maps.Location(56.5, -72),
                zoom: 5,
                showTools: true,

            };

            bingMap.init($("#mapContainer"), mapOptions, function () {
                debugger;
                bingMap.setModelGeo(model);
                thisBilingual.initUI();
            });
        },

        initUI: function () {            
            $(bingMap).on("GeoChanged", this.updateRegion);
            $("#btnReset").on('click', this.Reset);
            $("#btnDiscard").on('click', this.Discard);
            $("#btnSave").on('click', this.Save);
        },


        updateRegion: function (event, multiPolygon) {
            debugger;
            thisBilingual.TheRegion = multiPolygon;
            thisBilingual.updateAction("UpdatedRegion");
        },

        Discard: function () {
            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.confirmLabel,
                message: labels.areYouSureMessage,
                width: '400px',
                height: '150px',
                icon: "k-Ext-warning",
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
              ).done(function (response) {
                  if (response.button === 'OK') {
                      $.ajax({
                          cache: false,
                          success: function (data) {

                               thisBilingual.TheRegion = data.geo;
                               bingMap.setModelGeo(data.geo);
                               bingMap.clearAll();
                              
                          },
                          error: function (a,b,c,d) {
                              kendo.ui.ExtAlertDialog.show({
                                  title: labels.errorLabel,
                                  message: labels.errorUpdatingRegion,
                                  icon: "k-Ext-information"
                              });
                          },
                          type: "POST",
                          dataType: "json",
                          url: "/BilingualRegion/GetTheRegion"
                      });
                  }

              });
        },

        Reset: function () {

            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.confirmLabel,
                message: labels.areYouSureYouWant,
                width: '400px',
                height: '150px',
                icon: "k-Ext-warning",
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
                 ).done(function (response) {
                     if (response.button === 'OK') {
                         $.ajax({
                             cache: false,
                             success: function () {
                                 thisBilingual.TheRegion = null;
                                 bingMap.setModelGeo(thisBilingual.TheRegion);
                                 bingMap.clearAll();

                                 kendo.ui.ExtAlertDialog.show({
                                     title: labels.successLabel,
                                     message: labels.resetSuccessfully,
                                     icon: "k-Ext-information"
                                 });

                                 thisBilingual.updateAction();

                             },
                             error: function () {
                                 kendo.ui.ExtAlertDialog.show({
                                     title: labels.errorLabel,
                                     message: labels.errorUpdatingRegion,
                                     icon: "k-Ext-information"
                                 });
                             },
                             type: "POST",
                             dataType: "json",
                             url: "/BilingualRegion/Reset"
                         });
                     }

                 });
        },

        Save: function () {

            $("#regionType").val("");

            $.ajax({
                data: { updatedRegion: thisBilingual.TheRegion },
                cache: false,
                success: function () {
                    kendo.ui.ExtAlertDialog.show({
                        title: labels.successLabel,
                        message: labels.updateSuccesfully,
                        icon: "k-Ext-information"
                    });

                    thisBilingual.updateAction();
                },
                error: function () {
                    kendo.ui.ExtAlertDialog.show({
                        title: labels.errorLabel,
                        message: labels.errorUpdatingRegion,
                        icon: "k-Ext-information"
                    });
                },
                type: "POST",
                dataType: "json",
                url: "/BilingualRegion/Save"
            });
        },

        updateAction: function (change) {

            if (change === "UpdatedRegion") {
                $("#btnSave").show();
                $("#btnDiscard").show();
                $("#btnReset").hide();
            } else {
                $("#btnSave").hide();
                $("#btnDiscard").hide();
                $("#btnReset").show();
            }
        }
    }
});



