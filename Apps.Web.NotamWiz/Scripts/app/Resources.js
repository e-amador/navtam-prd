﻿
define(["jquery"], function($)
{
    return {
        resources: {
            "dbRes": function dbRes(resId)
            {
                return resources[resId] || resId;
            }
        },
        getResources: function(resourceSet)
        {
            var currentLang = $("#currentCulture").data("lang").indexOf("fr") ? "en" : "fr";
            var that = this;
            $.ajax({
                url: "/JavaScriptResourceHandler.axd",
                type: "GET",
                dataType: "text",
                async: false,
                cache: false,
                data: {
                    ResourceSet: resourceSet,
                    LocaleId: currentLang,
                    //VarName:'',
                    ResourceType: "resdb",
                    ResourceMode: 1
                },
                success: function(data)
                {
                    that.resources = $.extend({
                        "dbRes": function dbRes(resId)
                        {
                            return resources[resId] || resId;
                        }
                    }, eval(data));
                }
            });
            return this.resources;
        }
    };
});