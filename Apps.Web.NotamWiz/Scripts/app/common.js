﻿
function getRequireBaseUrl()
{
    // This function will return the baseUrl for the require.js configuration.  It is needed so that our code will work properly on IIS as a root app 
    // or in a virtual directory.  It will look for a script scr of app/common.js.  If it finds it, it will return the left characters before 
    // /app/common.js.  If you rename this file, or move it please update the thisJs variable.
    var scripts = document.getElementsByTagName("script");
    var baseUrl = "/scripts";
    var thisJs = "/app/common.js";
    var indx = -1;
    var bFound = false;

    for (i = 0; (i < scripts.length) && (!bFound); i++)
    {
        indx = scripts[i].src.lastIndexOf(thisJs);

        if (indx > -1)
        {
            bFound = true;
            baseUrl = scripts[i].src.substring(0, indx);
        }
    }

    return (baseUrl);
}

function setupRequireJS(bust) {

    require.config({
        urlArgs: "bust=" + bust,
        baseUrl: getRequireBaseUrl(),
        waitSeconds: 0,
        paths: {
            "blueBird": "app/bluebird.min",
            "async": 'RequireJSPlugins/async',
            "jquery": "jquery-2.1.4",
            "kendo": "kendo/",
            'kendo-mvc': 'kendo/kendo.aspnetmvc.min',
            "kendoExt": "app/kendo.web.Ext",
            "kendoCustomDataBinders": "app/kendoCustomDataBinders",
            "signalr": "jquery.signalR-2.2.0.min",
            "signalr.hubs": "/signalr/hubs?",
            "jquery.layout": "jquery.layout-1.4.3",
            "jquery-ui": "jquery-ui-1.11.4",
            "layout": "app/layout",
            "notify": "app/notification",
            "chat": "app/chat",
            "systemStatusNotification": "app/systemStatusNotification",
            "c": "app/Controllers",
            "admin": "app/Areas/Admin",
            "NSDFormManager": "app/NSDFormManager",
            "javaExt": "app/extensions",
            "bingMaps": "app/bingMapControl",
            "bingMapsV8": "app/bingMapControlV8",
            "googleMaps": "app/googleMapControl",
            "Resources": "app/Resources",
            "ProposalModel": "app/ProposalModel",
            "utilities": "app/utilities",
            "Schedule": "app/NOTAMSchedule/Schedule",
            "mapping": "app/mapping",
            "jsonpath": "app/jsonpath-0.8.0",
            "kendo-web": 'kendo/kendo.web.min',
            "bootstrap": "app/bootstrap.min",
        },
        shim: {
            'jquery': { exports: '$' },
            "signalr": { deps: ["jquery"] },
            "signalr.hubs": { deps: ["signalr"] },
            'kendo': { deps: ["jquery"] },
            'kendo/kendo.core.min': { deps: ["jquery"] },
            "jquery-ui": { deps: ['jquery'] },
            "jquery.layout": { deps: ['jquery-ui'] },
            "layout": { deps: ['jquery', 'jquery-ui', 'jquery.layout'] },
            "notify": { deps: ['jquery', 'signalr.hubs'] },
            "chat": { deps: ['jquery', 'signalr.hubs'] },
            "utilities": { deps: ["jquery", "javaExt"] },
            "kendoExt": { deps: ["jquery", "kendo/kendo.all.min"] },
            "kendoCustomDataBinders": { deps: ["jquery", "kendo/kendo.binder.min"] },
            "mapping": { deps: ["bingMaps"] },
            "kendo-web": { deps: ["jquery"] },
            "bootstrap" : { deps :['jquery'] }
        },

    });

    require(["app/app", "jquery", "jquery.layout"], function (app, $) {
        app.init();
    });
}