﻿define('bingMapsV8', ['jquery', 'utilities', 'async!//www.bing.com/api/maps/mapcontrol' , 'kendo/kendo.panelbar.min', 'kendo/kendo.dropdownlist.min', 'kendo/kendo.numerictextbox.min', 'kendo/kendo.maskedtextbox.min', 'kendo/kendo.progressbar.min'], function ($, util) {

   
    var mm = Microsoft.Maps;
    var thisMap;

    //return mapReadyDfd.promise();
    return {
        key: 'AsnetwydeC-II7WzjzdOwSxSJKzLWaUt_69it1mR-38YRSWCKFKI0WAC9Rvwimf4', // TODO this my own(omar) development key,  should change to actual purchase bing key        
      
        mapContainer: null,      
        theMap: null,
        mapTools: null,
        init: function (mapContainer, mapOptions, callBack) {

            thisMap = this;
            thisMap.options = $.extend({
                credentials: thisMap.key,
                center: new mm.Location(63.3, -104),
                mapTypeId: mm.MapTypeId.aerial,
                zoom: 10,
                showScalebar: false,
                showCopyright: false,
                showBreadcrumb: false,
                showDashboard: false,
                showMapTypeSelector: false,
                enableSearchLogo: false,
                enableClickableLogo: false,
                showTools: false,
                showLoadingProgressBar: true
            }, mapOptions);

            
            thisMap.mapContainer = mapContainer;
            $.ajax({
                url: "/Map/GetMapView",
                cache: false,
                async: false,
                type: "GET",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    thisMap.mapContainer.html(data);
                }
            });

            // get UI hooks
            thisMap.theMap              = thisMap.mapContainer.find(".theMap")[0];
          
        


            thisMap.map = new mm.Map(thisMap.theMap, thisMap.options);
            //Microsoft.Maps.loadModule('Microsoft.Maps.DrawingTools', function () {

            //    var tools = new Microsoft.Maps.DrawingTools(thisMap.map);
            //    tools.showDrawingManager(function (manager) {                   
            //    });

            //    Microsoft.Maps.loadModule('Microsoft.Maps.Clustering', function () {
                    Microsoft.Maps.loadModule('Microsoft.Maps.GeoJson', function () {
                        callBack();
                    });
            //    });
            //});
                 
            



        },



    }
});