﻿define(["jquery", "Resources", "bingMaps"], function ($, res, bingMap) {

    //var RES = res.getResources("AhpDetailes");
    var thisObject = null;
 

    return {

        init: function () {

            var geo = $("#geo").data("model");
            var ahpInfo = $("#ahpInfo").data("model");
            thisObject = this;

                   // Create map.
            var mapOptions = {
                zoom: 7,
            };

            bingMap.init($("#mapContainer"), mapOptions, function () {
                bingMap.setModelGeo(geo);
                thisObject.initUI();
                bingMap.showInfo(ahpInfo.TxtName, ahpInfo);
            });


            
        },

        initUI: function () {

        }
    }
});