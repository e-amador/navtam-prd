﻿
define(["jquery", "kendo/kendo.grid.min", "kendo/kendo.window.min"], function ($) {

    var model;
    var labels = {};

    return {

        init: function (lbs) {
            model = $("#dataModel").data("model");
            labels = lbs;
            this.initUI();
        },

        initUI: function () {

            $("#btnCancelNSDSelecion").on('click', function () {
                   $("#NSDSelector-Popup").data("kendoWindow").close();
            });


            $("#NSDList").kendoGrid({
                height:"100%",
                resizable: true,
                selectable: true,
                filterable: true,
                groupable: true,
                sortable: true,
                scrollable: true,
                pageable: true,
                columns: [
                    { field: "Category", title: labels.categoryLabel, width: 100 },
                    { field: "Name", title: labels.nameLabel, width: 200 },
                    { field: "Description", title: labels.descriptionLabel, width: "~150" },
                    {command:[{
                        name: "Start NOTAM Proposal",
                        click: function (e) {
                            var grid = $('#NSDList').data("kendoGrid");
                            var tr = $(e.currentTarget).closest("tr");
                            grid.select(tr);
                            var dataItem =  this.dataItem(tr);
                            var theSubject = model.subject;

                            $("#NSDEditor-Popup").data("kendoWindow")
                                .content("")
                                .refresh({
               
                                    url: "../NsdEditor/CreateNSD",
                                    type: "POST",
                                    data: { subjectId: theSubject.SubjectId, nsdId: dataItem.Id, version: dataItem.Version },
                                });

                        }
                    }]
                    }
                ],
                dataSource: {
                    schema: {
                        model: { id: "Id" },
                        data: function (data) { return data.Data; },

                    },
                    transport: {
                        read: {
                            url: "Nsds/GetNsdsForSubject",
                            dataType: "json",
                            type: "POST",
                            data: model.subject
                        }
                    }
                }

            });
        },
        
    };
});