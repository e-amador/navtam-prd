﻿define(["jquery", "Resources", "notify", "utilities", "kendo/kendo.grid.min", "kendo/kendo.tooltip.min", "kendo/kendo.window.min"], function ($, res, notify, Util) {

    var RES, thisMonitor;

    // Note: is this file included from anywhere?

    return {

        
        init: function () {
            RES = res.getResources("SystemMonitor");
            thisMonitor = this;
            thisMonitor.initUI();

            notify.init();

            $(notify).off('updateSystemStatus');

            $(notify).on('updateSystemStatus', function () {
            });
        },

    

        initUI: function () {

            $("#statusGrid").kendoGrid({
                resizable: true,
                selectable: true,
                Filterable: true,
                Groupable: true,
                scrollable: true,
                sortable: {
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    buttonCount: 5
                },
                columns: [
                    { field: "Id", title: RES.dbRes("Id"), width: 10 },
                    { field: "Type", title: RES.dbRes("Type"), width: 130 },
                    { field: "Name", title: RES.dbRes("Name"), width: "~75" },
                    { field: "MachineName", title: RES.dbRes("Running on"), width: "~120" },
                    { field: "Online", title: RES.dbRes("Online"), width: 160 },
                    { field: "Status", title: RES.dbRes("Status"), width: 160 },
                    { field: "Memory", title: RES.dbRes("End Date"), width: 160},
                    { field: "CPU", title: RES.dbRes("Status"), width: 100 },                    
                ],
                detailTemplate: kendo.template($("#statusDetailes").html()),
                detailInit: this.detailInit,
                height: "100%",
                change: function (e) {

                  
                },
                dataBound: function (e) {                  
                },
                dataSource: {
                    schema: {
                        model: { id: "Id" },
                        data: function (data) { return data.Data; },

                    },
                    sort: { field: "FinalInternalStatus", dir: "desc" },
                    transport: {
                        read: {
                            url: "SystemMonitor/GetSystems",
                            dataType: "json",
                            type: "GET"                         
                        }
                    }

                }
            });
         
       


        },

        detailInit: function (e) {
            var detailRow = e.detailRow;

            detailRow.find(".detailGrid").kendoGrid({
                animation: {
                    open: { effects: "fadeIn" }
                },
                columns: [
                    { field: "ItemE", title: RES.dbRes("Message"), width: "~300" },
                    { field: "StartValidity", title: RES.dbRes("StartDate"), width: 160, template: "#=  (StartValidity === null) ? ' ' : kendo.toString(kendo.parseDate(StartValidity, 'dd/MM/yyyy HH:mm UTC'), 'dd/MM/yyyy HH:mm UTC') #" },
                    { field: "EndValidity", title: RES.dbRes("EndDate"), width: 160, template: "#= (EndValidity === null) ? ' ' : kendo.toString(kendo.parseDate(EndValidity, 'dd/MM/yyyy HH:mm UTC'), 'dd/MM/yyyy HH:mm UTC') #" },
                    { field: "Type", title: RES.dbRes("Type"), width: 115 }

                ],
                dataSource: {
                    schema: {
                        model: { id: "Id" },
                        data: function (data) { return data.Data; },

                    },
                    sort: { field: "StatusTime", dir: "desc" },
                    transport: {

                        read: {
                            url: "Dashboard/Proposal_Read",
                            dataType: "json",
                            type: "POST",
                            data: { "packageId": e.data.Id }
                        }
                    }
                }
            });


        },
      


    };

});