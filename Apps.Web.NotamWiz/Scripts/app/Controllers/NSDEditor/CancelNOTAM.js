﻿define(["jquery", "kendoExt"], function ($) {
    var RES;

    return {
        init: function (notamPackageId) {
            debugger;
             RES = res.getResources("NSDEditor");

             $.when(kendo.ui.ExtOkCancelDialog.show({
                title: RES.dbRes("Confirm"),
                message: RES.dbRes("Are you sure you want to Cancel This NOTAM?"),
                width: '400px',
                height: '150px',
                icon: "k-Ext-warning",
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
              ).done(function (response) {
                  if (response.button === 'OK') {
                      $.ajax({
                          cache: false,
                          success:  DrawingPage.ReceiveData,
                          error: function(){
                              kendo.ui.ExtAlertDialog.show({
                                  title: RES.dbRes("Error"),
                                  message: RES.dbRes("Error Canceling NOTAM"),
                                  icon: "k-Ext-information"
                              });
                          },
                          type: "POST",
                          dataType: "json",
                          data:{packageId: notamPackageId },
                          url: "/NsdEditor/CancelProposalDirect"
                      });
                  }

              });
        }
    }
});