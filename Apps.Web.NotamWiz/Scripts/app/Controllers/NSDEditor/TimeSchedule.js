﻿define(["jquery", "Resources", "utilities", "kendo-web", "kendoExt"], function ($, res, Util) {

    var thisIndex, expandedRow;
    var grid;
    var scheduler;
    var currentEvent;
    var MydataSource;
    var check;
    var validator;
    var startDatetime = null;
    var endDatetime = null;
    var proposalVM = null;
    var timeScheduleData = null;
    var docisready = false;
    var labels = {};

    return {

        init: function (TimeScheduleData, BeginDatetime, ExpiryDatetime, model) {

            thisIndex = this;
            proposalVM = model;
            grid: $("#grid");
            scheduler: $("#scheduler").data("kendoScheduler");
            check: "success";
            validator: null;
            timeScheduleData = TimeScheduleData;
            startDatetime = BeginDatetime;
            endDatetime = ExpiryDatetime;

        },
        PostInitUI: function ()
        {
            
            //$("#addandclear").hide();
            $("#weekdaystd").hide();
            $("#schedulertd").hide();
            $("#schedulerInfotd").hide();
            $("#SubSelectiontd").hide();
            document.getElementById("Exclude").disabled = true;
            

            $("#FromOperationSelection").hide();
            $("#ToOperationSelection").hide();

            $("#FromOperationSelection").data("kendoDropDownList").wrapper.hide();
            $("#ToOperationSelection").data("kendoDropDownList").wrapper.hide();
            thisIndex._ResetSubSelection();

        },

        initUI: function (lbls) {
            labels = lbls;

            thisIndex._PopulateTimeObject();
            thisIndex._PopulateMinutesObject();
            validator = $("#errorinput").kendoValidator({
                rules: {
                    Overlapping: function (e) {
                        if (check === "error_Overlapping") {
                            $("#errorinput").addClass('Invalid')
                            return false;
                        }

                        $("#errorinput").removeClass('Invalid')
                        return true;
                    },
                    SevenDayGap: function (e) {
                        if (check === "error_SevenDayGap") {
                            $("#errorinput").addClass('Invalid')
                            return false;
                        }

                        $("#errorinput").removeClass('Invalid')
                        return true;
                    },
                    PastDate: function (e) {
                        if (check === "error_PastDate") {
                            $("#errorinput").addClass('Invalid')
                            return false;
                        }

                        $("#errorinput").removeClass('Invalid')
                        return true;
                    }
                },
                messages: {
                    Overlapping: ((labels.timeSchdlr_valOverlaping_mesage)),
                    SevenDayGap: ((labels.timeSchdlr_valDaysGap_mesage)),
                    PastDate: ((labels.timeSchdlr_valPastDate_mesage))
                }


            }).data("kendoValidator");

            $('[class^="checkbox"]').keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == 13) {
                    thisIndex._clickCheckBox(this);
                }
                event.stopPropagation();
            });
            $('[class^="RadioButtonSelection"]').keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == 13) {
                    thisIndex._CheckRadionButton(this);
                }
                event.stopPropagation();
            });

            $("#FromTimeObject").kendoDropDownList({
                animation: {
                    close: {
                        effects: "zoom:out",
                        duration: 300
                    }
                },
                read: {
                    cache: false
                }
            });
            $("#FromOperationSelection").kendoDropDownList({
                animation: {
                    close: {
                        effects: "zoom:out",
                        duration: 300
                    }
                }
            });
            $("#FromMinutesObject").kendoDropDownList({
                animation: {
                    close: {
                        effects: "zoom:out",
                        duration: 300
                    }
                }
            });

            $("#ToTimeObject").kendoDropDownList({
                animation: {
                    close: {
                        effects: "zoom:out",
                        duration: 300
                    }
                }
            });
            $("#ToOperationSelection").kendoDropDownList({
                animation: {
                    close: {
                        effects: "zoom:out",
                        duration: 300
                    }
                }
            });
            $("#ToMinutesObject").kendoDropDownList({
                animation: {
                    close: {
                        effects: "zoom:out",
                        duration: 300
                    }
                }
            });
            $('.RadioButtonSelectionInEx').click(function () {
                // clear all controls
                thisIndex._ResetSubSelection();
                thisIndex._ResetSelection();
                if ($(this).attr("value") == "Include") {
                    $("#SubSelectiontd").show();
                    thisIndex._reformateview("RadioButtonSelectionInEx");
                    
                }
                if ($(this).attr("value") == "Exclude") {
                    $("#SubSelectiontd").show();
                    document.getElementById("weekdays").disabled = true;
                    document.getElementById("timeonly").disabled = true;
                    document.getElementById("calendar").disabled = false;
                    document.getElementById("calendar").checked = true;
                    $("#schedulertd").show();
                    $("#schedulerInfotd").show();                    
                }
            });

            // hide and unhide controls
            $('.RadioButtonSelectionWCT').click(function () {
                // clear all controls
                thisIndex._ResetSubSelection();
                if ($(this).attr("value") == "weekdays") {
                    $("#weekdaystd").show();
                    $("#timeslectortd").show();
                    $("#schedulertd").hide();
                    $("#schedulerInfotd").hide();       
                    document.getElementById("Continues").disabled = true;
                    $("#ContinuesDiv").hide();
                                    }
                if ($(this).attr("value") == "calendar") {
                    $("#schedulertd").show();
                    $("#timeslectortd").show();
                    $("#schedulerInfotd").show();
                    $("#weekdaystd").hide();
                    document.getElementById("Continues").disabled = false;
                    $("#ContinuesDiv").show();
                }

                if ($(this).attr("value") == "timeonly") {
                    $("#timeslectortd").show();
                    $("#schedulertd").hide();
                    $("#schedulerInfotd").hide();
                    $("#weekdaystd").hide();
                    document.getElementById("Continues").disabled = true;
                    $("#ContinuesDiv").hide();
                }
            });
            $("#FromTimeObject").change(function () {
                if ($("#FromTimeObject").val() === "SR") {
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[0].hidden = false;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[1].hidden = true;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[2].hidden = false;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[3].hidden = true;

                    var dropdownlist = $("#FromOperationSelection").data("kendoDropDownList");

                    dropdownlist.select(0);

                    dropdownlist.wrapper.show();


                }
                else if ($("#FromTimeObject").val() === "SS") {

                    $("#" + "FromOperationSelection" + "_listbox .k-item")[0].hidden = false;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[1].hidden = true;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[2].hidden = true;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[3].hidden = false;
                    var dropdownlist = $("#FromOperationSelection").data("kendoDropDownList");

                    dropdownlist.select(0);

                    dropdownlist.wrapper.show();
                }
                else {
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[0].hidden = true;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[1].hidden = false;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[2].hidden = true;
                    $("#" + "FromOperationSelection" + "_listbox .k-item")[3].hidden = true;

                    var dropdownlist = $("#FromOperationSelection").data("kendoDropDownList");

                    dropdownlist.select(1);

                    dropdownlist.wrapper.hide();
                }
            });

            $("#ToTimeObject").change(function () {

                if ($("#ToTimeObject").val() === "SR") {

                    $("#" + "ToOperationSelection" + "_listbox .k-item")[0].hidden = false;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[1].hidden = true;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[2].hidden = false;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[3].hidden = true;

                    var dropdownlist = $("#ToOperationSelection").data("kendoDropDownList");

                    dropdownlist.select(0);

                    dropdownlist.wrapper.show();
                }
                else if ($("#ToTimeObject").val() === "SS") {

                    $("#" + "ToOperationSelection" + "_listbox .k-item")[0].hidden = false;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[1].hidden = true;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[2].hidden = true;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[3].hidden = false;

                    var dropdownlist = $("#ToOperationSelection").data("kendoDropDownList");

                    dropdownlist.select(0);

                    dropdownlist.wrapper.show();
                }
                else {
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[0].hidden = true;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[1].hidden = false;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[2].hidden = true;
                    $("#" + "ToOperationSelection" + "_listbox .k-item")[3].hidden = true;

                    var dropdownlist = $("#ToOperationSelection").data("kendoDropDownList");

                    dropdownlist.select(1);

                    dropdownlist.wrapper.hide();
                }
            });

            //$("#addandclear").click(function () {

            //    thisIndex._AddEventstoGrid();
            //    thisIndex._ResetSubSelection();

            //});

            $("#add").click(function () {

                thisIndex._AddEventstoGrid();
                thisIndex._ResetSubSelection();

            });

            $("#GenerateTextbtn").click(function () {

                document.getElementById("Generatedtext").value = thisIndex._GenerateText();

            });

            $("#Okbtn").click(function () {
                document.getElementById("Generatedtext").value = thisIndex._GenerateText();
                //proposalVM.NOTAM.set("ItemD", document.getElementById("Generatedtext").value);    
                proposalVM.nsdTokens.set("ItemD", document.getElementById("Generatedtext").value);
                proposalVM.nsdTokens.set("TimeScheduleData", MydataSource.data());
                $("#TimeScheduleAPI-Popup").data("kendoWindow").close();

            });

            $("#ClearGridandScheduler").click(function () {
                thisIndex._ClearGrid();
                thisIndex._ResetSubSelection();
                thisIndex._ResetSelection();
                thisIndex._ResetActionSelection();
                document.getElementById("Generatedtext").value = "";

            });

            $("#scheduler").kendoScheduler({
                height: 330,
                selectable: true,
                width: 598,
                add: function (e) {
                    if (!thisIndex._checkAvailability(e.event.start, e.event.end, e.event)) {
                        e.preventDefault();
                    }

                },
                save: function (e) {
                    if (!thisIndex._checkAvailability(e.event.start, e.event.end, e.event)) {
                        e.preventDefault();
                    }
                },
                editable: {
                    confirmation: false
                },
                views: [
                    { type: "month", selected: true },
                ],
                batch: true,
                dataSource: []
    , edit: function (e) {
        e.preventDefault(); //prevent popup editing
        var dataSource = this.dataSource;
        var event = e.event;

        if (event.isNew()) {

            setTimeout(function () {
                event.id = Math.floor(Math.random() * 100) + 1;
                dataSource.add(event);
                thisIndex._editEvent(event);
            }
            );
        } else {
            thisIndex._editEvent(event);
        }
    }
            });
            var products;
            MydataSource = new kendo.data.DataSource({
                data: products,
                schema: {
                    model: {
                        fields: {
                            DateId: { editable: false },
                            EndDateId: { editable: false },
                            TimeId: { editable: false },
                            EndTimeId: { editable: false },
                            ContinuesEvent: { editable: false },
                            RadioButtonSelectionWCT: { editable: false },
                            IncludeorExclude: { editable: false },
                            StartDateEndDate: { editable: false },
                            StartTimeEndTime: { editable: false }
                        }
                    }
                }
                ,
                change: function (e) {
                    thisIndex._reformateview("grid");
                }
            });

            $("#grid").kendoGrid({
                refresh: true,
                scrollable: true,
                dataSource: MydataSource,
                height: 250,
                width: 600,
                editable: {
                    confirmation: false // the confirmation message for destroy command
                },
                columns: [
                    {
                        field: "DateId",
                        title: labels.timeSchdlr_fieldDate_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "EndDateId",
                        title: labels.timeSchdlr_fieldEndDate_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "TimeId",
                        title: labels.timeSchdlr_fieldTime_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "EndTimeId",
                        title: labels.timeSchdlr_fieldEndTime_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                       {
                           field: "ContinuesEvent",
                           title: labels.timeSchdlr_fieldContEvent_title,
                           width: 1,
                           hidden: true,
                           editable: false
                       }
                    ,
                    {
                        field: "RadioButtonSelectionWCT",
                        title: labels.timeSchdlr_fieldrbSelection_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "IncludeorExclude",
                        title: labels.timeSchdlr_fieldAction_title,
                        width: 100,
                        editable: false
                    },
                    {
                        field: "StartDateEndDate",
                        title: labels.timeSchdlr_fieldDateTime_title,
                        editable: false
                    }, {
                        field: "StartTimeEndTime",
                        title: labels.timeSchdlr_fieldPeriod_title,
                        editable: false
                    }
                , { command: [{ className: "btn-destroy", name: "destroy", text: "Remove" }], width: 120 }
                ]
            });
            // grid
            if (timeScheduleData.replace(/""/g, '') !== "null") {
                var dates = JSON.parse(timeScheduleData);
                for (var idx = 0; idx < dates.length; idx++) {
                    MydataSource.add({
                        RadioButtonSelectionWCT: dates[idx].RadioButtonSelectionWCT,
                        DateId: dates[idx].DateId,
                        EndDateId: dates[idx].EndDateId,
                        TimeId: dates[idx].TimeId,
                        EndTimeId: dates[idx].EndTimeId,
                        ContinuesEvent: dates[idx].ContinuesEvent,
                        IncludeorExclude: dates[idx].IncludeorExclude,
                        StartDateEndDate: dates[idx].StartDateEndDate,
                        StartTimeEndTime: dates[idx].StartTimeEndTime
                    });
                }

            }
            // create on one click instead of double click
            scheduler.wrapper.on("mouseup touchend", ".k-scheduler-table td, .k-event", function (e) {
                var target = $(e.target);
                if (target.hasClass("k-si-close")) {
                    return;
                }

                target = $(e.currentTarget);
                if (target.hasClass("k-event")) {
                    var event = scheduler.occurrenceByUid(target.data("uid"));
                    scheduler.editEvent(event);
                } else {
                    var slot = scheduler.slotByElement(target[0]);
                    //slot.endDate.setDate(slot.endDate.getDate() - 1)
                    scheduler.addEvent({
                        start: slot.startDate,                                                      
                        end: slot.endDate
                    });
                }
            });
            $(document.body).keydown(function (e) {
                if (e.altKey && e.shiftKey && e.keyCode == 87)
                {
                    scheduler.wrapper.focus();
                }
                if (e.altKey && e.shiftKey && e.keyCode == 69) {
                    $("#FromTimeObject").data("kendoDropDownList").focus();
                }
                if (e.keyCode == 9)
                {
                    if ($("#Okbtn").is(":focus")) {
                        e.preventDefault();
                        document.getElementById("Include").focus();
                    }
                }
            });
        },

        _editEvent: function editEvent(event) {
            scheduler.cancelEvent(event);
        },


        _Validate: function () {
            return validator.validate();

        },
        _occurrencesInRangeByResource: function occurrencesInRangeByResource(start, end, resourceFieldName, event, resources) {
            var scheduler = $("#scheduler").getKendoScheduler();
            var occurrences = scheduler.occurrencesInRange(start, end);
            var idx = occurrences.indexOf(event);
            if (idx > -1) {
                occurrences.splice(idx, 1);
            }

            event = $.extend({}, event, resources);

            return thisIndex._filterByResource(occurrences, resourceFieldName, event[resourceFieldName]);
        },

        _filterByResource: function filterByResource(occurrences, resourceFieldName, value) {
            var result = [];
            var occurrence;

            for (var idx = 0, length = occurrences.length; idx < length; idx++) {
                occurrence = occurrences[idx];
                if (occurrence[resourceFieldName] === value) {
                    result.push(occurrence);
                }
            }
            return result;
        },

        _attendeeIsOccupied: function attendeeIsOccupied(start, end, event, resources) {
            var occurrences = thisIndex._occurrencesInRangeByResource(start, end, "attendee", event, resources);
            if (occurrences.length > 0) {
                return true;
            }
            return false;
        },


        _roomIsOccupied: function roomIsOccupied(start, end, event, resources) {
            var occurrences = thisIndex._occurrencesInRangeByResource(start, end, "roomId", event, resources);
            if (occurrences.length > 0) {
                return true;
            }
            return false;
        },


        _checkAvailability: function checkAvailability(start, end, event, resources) {
            if (thisIndex._attendeeIsOccupied(start, end, event, resources)) {
                setTimeout(function () {
                }, 0);

                return false;
            }

            if (thisIndex._roomIsOccupied(start, end, event, resources)) {

                setTimeout(function () {
                }, 0);
                return false;
            }

            return true;
        },

        _clickCheckBox: function clickCheckBox(box) {
            var $box = $(box);
            $box.prop('checked', !$box.prop('checked'));

        },

        // end check box entr key event action

        // radio button enter key event action

        _CheckRadionButton: function CheckRadionButton(box) {
            var $box = $(box);
            $box.click();
        },

        _GenerateText: function GenerateText()
        {
            var GeneratedText_Include;
            var GeneratedText_Exclude = "EXC";
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            if (datasourcedata.length > 0)
            {
                for (var i = 0; i < datasourcedata.length; i++)
                {
                    if (datasourcedata[i].IncludeorExclude === "Include")
                    {
                        if (datasourcedata[i].StartDateEndDate === "DAILY")
                        {
                            if (datasourcedata[i].StartTimeEndTime === "H24")
                            {
                                GeneratedText_Include = datasourcedata[i].StartDateEndDate
                            }
                            else if (datasourcedata[i].StartTimeEndTime.includes("SS") || datasourcedata[i].StartTimeEndTime.includes("SR"))
                            {
                                GeneratedText_Include = datasourcedata[i].StartTimeEndTime.replace(/:/g, '');
                            }
                            else
                            {
                                GeneratedText_Include = datasourcedata[i].StartDateEndDate + " " + datasourcedata[i].StartTimeEndTime.replace(/:/g, '');
                            }
                        }

                        else if (typeof GeneratedText_Include === 'undefined' || GeneratedText_Include === "")
                        {
                            GeneratedText_Include = datasourcedata[i].StartDateEndDate + " " + datasourcedata[i].StartTimeEndTime.replace(/:/g, '');
                        }
                        else
                        {
                            GeneratedText_Include = GeneratedText_Include + ", " + datasourcedata[i].StartDateEndDate + " " + datasourcedata[i].StartTimeEndTime.replace(/:/g, '');

                        }

                    }
                    else if (datasourcedata[i].IncludeorExclude === "Exclude")
                    {

                        if (GeneratedText_Exclude === "EXC")
                        {
                            GeneratedText_Exclude = GeneratedText_Exclude + " " + datasourcedata[i].StartDateEndDate
                        }
                        else
                        {
                            if (startDatetime.getMonth() != endDatetime.getMonth())
                            {
                                GeneratedText_Exclude = GeneratedText_Exclude + ", " + datasourcedata[i].StartDateEndDate;
                            }
                            else
                            {
                                GeneratedText_Exclude = GeneratedText_Exclude + ", " + datasourcedata[i].StartDateEndDate.substring(5);
                            }

                        }
                    }

                }
            }
            else
            {
                return null;
            }

            if (GeneratedText_Exclude === "EXC") {
                return (GeneratedText_Include).replace(/\s\s+/g, ' ');
            }
            else {
                return (GeneratedText_Include + ", " + GeneratedText_Exclude).replace(/\s\s+/g, ' ');
            }
        },
        _ClearGrid: function ClearGrid() {
            $("#grid").data('kendoGrid').dataSource.data([]);
        },
        _ClearCalanderScheduler: function ClearCalanderScheduler() {
            scheduler = $("#scheduler").data("kendoScheduler");
            var raw = scheduler.dataSource.data();
            var length = raw.length;
            // iterate and remove "done" items
            var item, i;
            for (i = length - 1; i >= 0; i--) {

                item = raw[i];
                scheduler.dataSource.remove(item);
            }
            $(' .k-state-selected', '#scheduler').removeClass('k-state-selected');
        },

        _ResetActionSelection: function ResetActionSelection() {
            document.getElementById("Include").checked = false;
            document.getElementById("Exclude").checked = false;
            document.getElementById("Exclude").disabled = true;
            thisIndex._Validate();
        },
        _ResetSelection: function ResetSelection() {
            document.getElementById("weekdays").checked = false;
            document.getElementById("calendar").checked = false;
            document.getElementById("timeonly").checked = false;
            $("#weekdaystd").hide();
            $("#schedulertd").hide();
            $("#schedulerInfotd").hide();
            $("#timeslectortd").hide();
            $("#SubSelectiontd").hide();
            thisIndex._Validate();

        },
        _ResetSubSelection: function ResetSubSelection() {

            $("#FromTimeObject").data("kendoDropDownList").select(2);
            $("#FromOperationSelection").data("kendoDropDownList").select(0);
            $("#FromMinutesObject").data("kendoDropDownList").select(0);

            $("#ToTimeObject").data("kendoDropDownList").select(2);
            $("#ToOperationSelection").data("kendoDropDownList").select(0);
            $("#ToMinutesObject").data("kendoDropDownList").select(0);

            $(".checkboxDays").removeAttr('checked');

            $("#Continues").removeAttr('checked');

            $("#FromOperationSelection").hide();
            $("#ToOperationSelection").hide();

            thisIndex._ClearCalanderScheduler();

        },

        _PopulateTimeObject: function PopulateTimeObject() {
            var FromList = document.getElementById('FromTimeObject');
            var ToList = document.getElementById('ToTimeObject');

            FromList.options[FromList.options.length] = new Option("SR", "SR");
            FromList.options[FromList.options.length] = new Option("SS", "SS");

            ToList.options[ToList.options.length] = new Option("SR", "SR");
            ToList.options[ToList.options.length] = new Option("SS", "SS");
            for (var i = 0; i < 24; i++) {
                if (i < 10) {
                    FromList.options[FromList.options.length] = new Option("0" + i, "0" + i);
                    ToList.options[ToList.options.length] = new Option("0" + i, "0" + i);
                }
                else {
                    FromList.options[FromList.options.length] = new Option(i, i);
                    ToList.options[ToList.options.length] = new Option(i, i);
                }
            }
        },
        _PopulateMinutesObject: function PopulateMinutesObject() {
            var Fromselect = document.getElementById('FromMinutesObject');
            var Toselect = document.getElementById('ToMinutesObject');

            for (var i = 0; i < 13; i++) {
                var j = (i) * 5
                if (j < 10) {
                    Fromselect.options[Fromselect.options.length] = new Option("0" + j, "0" + j);
                    Toselect.options[Toselect.options.length] = new Option("0" + j, "0" + j);
                }
                else if( j< 56) {
                    Fromselect.options[Fromselect.options.length] = new Option(j, j);
                    Toselect.options[Toselect.options.length] = new Option(j, j);
                }
                else
                {
                    Fromselect.options[Fromselect.options.length] = new Option(j-1, j-1);
                    Toselect.options[Toselect.options.length] = new Option(j-1, j-1);
                }
            }

        },
        _SortGrid: function SortGrid() {
            var kendoGrid = $("#grid").data('kendoGrid');
            if (kendoGrid.dataSource.data().length > 0) {
                var dsSort = [];
                dsSort.push({ field: "IncludeorExclude", dir: "desc" });
                dsSort.push({ field: "DateId", dir: "asc" });
                kendoGrid.dataSource.sort(dsSort);
            }

        },
        _getDayofYear: function getDayofYear(Date) {
            var now = new Date();
            var start = new Date(now.getFullYear(), 0, 0);
            var diff = now - start;
            var oneDay = 1000 * 60 * 60 * 24;
            var day = Math.floor(diff / oneDay);
        },

        _getcurrentDateGap: function getcurrentDateGap(DateId1, DateId2)
        {
            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            var selectedyear1 = DateId1.substring(0, 4);
            var selectedMonth1 = (parseInt(DateId1.substring(4, 6)) - 1).toString();
            var selectedDay1 = DateId1.substring(6, 8);
            var firstDate = new Date(selectedyear1, selectedMonth1, selectedDay1);

            if (typeof DateId2.getMonth === 'function') {
                return Math.round((firstDate.getTime() - DateId2.getTime()) / (oneDay));
            }
            else {
                var selectedyear2 = DateId2.substring(0, 4);
                var selectedMonth2 = (parseInt(DateId2.substring(4, 6)) - 1).toString();
                var selectedDay2 = DateId2.substring(6, 8);

                var SecondDate = new Date(selectedyear2, selectedMonth2, selectedDay2);

                return Math.round((firstDate.getTime() - SecondDate.getTime()) / (oneDay));
            }
        },
        //Check if event date ranges follows within start and end of notam 
        _checkDates: function checkDates(DateId, EndDateId, StartTime, EndTime)
        {
                                                                          
            // to deal with time only option
            if (DateId === "0" && EndDateId === "1")
            {
                if(new Date().getDate() === startDatetime.getDate() && new Date().getMonth() === startDatetime.getMonth())
                {
                    if ((startDatetime.getHours() * 60) + startDatetime.getMinutes() > parseInt(StartTime))
                    {
                        return false;
                    }
                }
            }
            // deal with week day option
            else if (parseInt(DateId) > 0 && parseInt(DateId) < 8 )
            {
                if ((parseInt(DateId) - 1) === startDatetime.getDay())
                {
                    if ((startDatetime.getHours() * 60) + startDatetime.getMinutes() + (startDatetime.getDay() * 24 * 60) > parseInt(StartTime))
                    {
                        return false;
                    }
                }
            }
            // deal with calander option
            else
            {
                var notamstartdate;
                var notamenddate;
                if (!startDatetime)
                {                                    
                    notamstartdate = new Date();
                    notamstartdate.setHours(0);      
                }
                else
                {                                    
                    notamstartdate = new Date(startDatetime);
                    notamstartdate.setHours(0);
                }                  

                var currentbiggestgap = thisIndex._getcurrentDateGap(DateId, notamstartdate);
                if (currentbiggestgap < 0)
                {
                    return false;
                }

                if (!endDatetime)
                {
                }
                else
                {                                    
                    notamenddate = new Date(endDatetime);
                    notamenddate.setHours(0);
                    currentbiggestgap = thisIndex._getcurrentDateGap(EndDateId, notamenddate);
                    if (currentbiggestgap > 0)
                    {
                        return false;
                    }
                    var endDateId = notamenddate.getFullYear().toString() + (((notamenddate.getMonth() + 1) < 10) ? "0" + (notamenddate.getMonth() + 1).toString() : (notamenddate.getMonth() + 1).toString()) + ((notamenddate.getDate() < 10) ? "0" + notamenddate.getDate() : notamenddate.getDate());
                    var end = new Date(notamenddate.getFullYear(), 0, 0);
                    var enddateindays = thisIndex._getcurrentDateGap(endDateId, end);

                    if ((((endDatetime.getHours() * 60) + endDatetime.getMinutes()) + (enddateindays * 24 * 60)) < parseInt(EndTime)) {
                        return false;
                    }

                }
                currentbiggestgap = thisIndex._getcurrentDateGap(EndDateId, endDatetime);
                if (currentbiggestgap > 0)
                {
                    return false;
                }
                var startDateId = startDatetime.getFullYear().toString() + (((startDatetime.getMonth() + 1) < 10) ? "0" + (startDatetime.getMonth() + 1).toString() : (startDatetime.getMonth() + 1).toString()) + ((startDatetime.getDate() < 10) ? "0" + startDatetime.getDate() : startDatetime.getDate());
                var now = new Date();
                var start = new Date(now.getFullYear(), 0, 0);
                var dateindays = thisIndex._getcurrentDateGap(startDateId, start);

                if ((((startDatetime.getHours() * 60) + startDatetime.getMinutes()) + (dateindays * 24 * 60)) > parseInt(StartTime))
                {
                    return false;
                }
            }

            return true;
        },

        _checkforduplicates: function checkforduplicates(ActionSelection, DateId, EndDateId, StartDateEndDate, StartTime, EndTime) {
            if (thisIndex._checkDates(DateId, EndDateId,StartTime, EndTime)) {
                var kendoGrid = $("#grid").data('kendoGrid');
                var datasourcedata = kendoGrid.dataSource.data();
                if (datasourcedata.length > 0) {
                    for (var i = 0; i < datasourcedata.length; i++) {
                        var DateIdArray = datasourcedata[i].DateId.split(',');
                        var EndDateIdArray = datasourcedata[i].EndDateId.split(',');
                        for (var j = 0 ; j < DateIdArray.length; j++) {
                            var TimeIdArray = datasourcedata[i].TimeId.split(',');
                            var EndTimeIdArray = datasourcedata[i].EndTimeId.split(',');
                            for (var g = 0 ; g < TimeIdArray.length; g++) {
                                if ((parseInt(TimeIdArray[g]) <= parseInt(StartTime) && parseInt(EndTimeIdArray[g]) >= parseInt(StartTime)) // start date between an existing start end date
                               || (parseInt(TimeIdArray[g]) <= parseInt(EndTime) && parseInt(EndTimeIdArray[g]) >= parseInt(EndTime)) // end date between an existing start end date
                                || (parseInt(TimeIdArray[g]) >= parseInt(StartTime) && parseInt(EndTimeIdArray[g]) <= parseInt(EndTime))) {
                                    return "error_Overlapping";
                                }
                            }
                        }
                    }
                }
                if (datasourcedata.length > 0) {
                    var biggestgap = -1;

                    for (var i = 0; i < datasourcedata.length; i++) {

                        if (datasourcedata[i].IncludeorExclude === ActionSelection) {
                            var EndDateIdArray = datasourcedata[i].EndDateId.split(',');
                            for (var j = 0 ; j < EndDateIdArray.length; j++) {

                                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                                var StartDate = new Date(DateId.substring(0, 4), (parseInt(DateId.substring(4, 6)) - 1).toString(), DateId.substring(6, 8));
                                var EndDate = new Date(EndDateId.substring(0, 4), (parseInt(EndDateId.substring(4, 6)) - 1).toString(), EndDateId.substring(6, 8));
                                var EndDateFromDataSource = new Date(EndDateIdArray[j].substring(0, 4), (parseInt(EndDateIdArray[j].substring(4, 6)) - 1).toString(), EndDateIdArray[j].substring(6, 8));

                                var currentbiggestgapfromStartDate = Math.round(Math.abs((StartDate.getTime() - EndDateFromDataSource.getTime()) / (oneDay)));
                                var currentbiggestgapfromEndDate = Math.round(Math.abs((EndDate.getTime() - EndDateFromDataSource.getTime()) / (oneDay)));

                                var currentbiggestgap;
                                if (currentbiggestgapfromStartDate <= currentbiggestgapfromEndDate) {
                                    currentbiggestgap = currentbiggestgapfromStartDate;
                                }
                                else {
                                    currentbiggestgap = currentbiggestgapfromEndDate;
                                }

                                if (currentbiggestgap <= 7 || biggestgap === -1) {
                                    biggestgap = currentbiggestgap;
                                }
                                else {
                                    if (biggestgap > 7) {
                                        biggestgap = currentbiggestgap;
                                    }
                                }
                            }
                        }
                    }
                    if (biggestgap > 7) {
                        return "error_SevenDayGap";
                    }
                }

                return "success";
            }
            return "error_PastDate";
        },
        _split: function split(arr) {
            var res = [];
            var subres = [];
            for (var i = 0; i < arr.length; i++) {
                var length = subres.length;
                if (length === 0 || subres[length - 1] === parseInt(arr[i]) - 1) {
                    subres.push(parseInt(arr[i]));
                } else {
                    res.push(subres);
                    subres = [parseInt(arr[i])];
                }
            }
            res.push(subres);
            return res;
        },

        _checkforContinuity: function checkforContinuity(RadioButtonSelectionWCT, DateId, EndDateId, IncludeorExclude, StartDateEndDate, StartTimeEndTime, StartTime, EndTime, continues)
        {
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            if (datasourcedata.length > 0) {
                for (var i = 0; i < datasourcedata.length; i++) {
                    if (RadioButtonSelectionWCT === "Calander") {
                        if (continues) {
                            if (datasourcedata[i].ContinuesEvent) {
                                //if (datasourcedata[i].TimeId === StartTime && datasourcedata[i].EndTimeId === EndTime) {

                                var _dayarray = datasourcedata[i].DateId.split(',');
                                _dayarray.sort(function (a, b) { return a - b });
                                if (Math.abs(thisIndex._getcurrentDateGap(_dayarray[0], DateId)) === 1 || Math.abs(thisIndex._getcurrentDateGap(_dayarray[_dayarray.length - 1], DateId)) === 1) {

                                    datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                                    var startimearray = datasourcedata[i].TimeId.split(',');
                                    startimearray.sort(function (a, b) { return a - b });
                                    var jointstarttimearray = startimearray.join(',');
                                    datasourcedata[i].TimeId = jointstarttimearray;

                                    datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                                    var endtimearray = datasourcedata[i].EndTimeId.split(',');
                                    endtimearray.sort(function (a, b) { return a - b });
                                    var jointendtimearray = endtimearray.join(',');
                                    datasourcedata[i].EndTimeId = jointendtimearray;

                                    datasourcedata[i].DateId = datasourcedata[i].DateId + "," + DateId;
                                    datasourcedata[i].EndDateId = datasourcedata[i].EndDateId + "," + EndDateId;
                                    var daysarray = datasourcedata[i].DateId.split(',');
                                    daysarray.sort(function (a, b) { return a - b });
                                    var days = daysarray.join(',');
                                    datasourcedata[i].DateId = days;
                                    datasourcedata[i].EndDateId = days;
                                    if (StartTimeEndTime === "H24") {
                                        _StartDateEndDate = datasourcedata[i].StartDateEndDate.substring(0, 3) + " " + daysarray[0].substring(6, 8) + " " + "0:00" + "-" + StartDateEndDate.substring(0, 3) + " " +  daysarray[daysarray.length - 1].substring(6, 8) + " " + "0:00";
                                    }
                                    else {
                                        var datetimearray = StartTimeEndTime.split('-');
                                        _StartDateEndDate = datasourcedata[i].StartDateEndDate.substring(0, 3) + " " + daysarray[0].substring(6, 8) + " " + datetimearray[0] + "-" + StartDateEndDate.substring(0, 3) + " " + daysarray[daysarray.length - 1].substring(6, 8) + " " + datetimearray[1];
                                    }
                                    datasourcedata[i].StartDateEndDate = _StartDateEndDate.toUpperCase();
                                    return "1";
                                }
                                //}
                            }
                        }
                        else {

                            if (String(datasourcedata[i].DateId).substring(0, 10) === String(datasourcedata[i].EndDateId).substring(0, 10) && datasourcedata[i].RadioButtonSelectionWCT === "Calander" && datasourcedata[i].IncludeorExclude === IncludeorExclude &&
                                String(DateId).substring(0, 10) === String(EndDateId).substring(0, 10) && datasourcedata[i].StartTimeEndTime === StartTimeEndTime &&
                                String(datasourcedata[i].DateId).substring(4, 6) === String(DateId).substring(4, 6)) {
                                //hand starttime ids


                                datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                                var startimearray = datasourcedata[i].TimeId.split(',');
                                startimearray.sort(function (a, b) { return a - b });
                                var jointstarttimearray = startimearray.join(',');
                                datasourcedata[i].TimeId = jointstarttimearray;

                                datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                                var endtimearray = datasourcedata[i].EndTimeId.split(',');
                                endtimearray.sort(function (a, b) { return a - b });
                                var jointendtimearray = endtimearray.join(',');
                                datasourcedata[i].EndTimeId = jointendtimearray;

                                datasourcedata[i].DateId = datasourcedata[i].DateId + "," + DateId;
                                datasourcedata[i].EndDateId = datasourcedata[i].EndDateId + "," + EndDateId;
                                var daysarray = datasourcedata[i].DateId.split(',');
                                var _StartDateEndDate = datasourcedata[i].StartDateEndDate + " " + String(StartDateEndDate).substring(4, 6);
                                daysarray.sort(function (a, b) { return a - b });
                                var days = daysarray.join(" ");

                                var splitarray = thisIndex._split(daysarray);
                                var _SqstartDateEndDate;
                                for (var w = 0; w < splitarray.length; w++) {

                                    if (splitarray[w].length > 1) {
                                        _SqstartDateEndDate = ((_SqstartDateEndDate === undefined) ? "" : _SqstartDateEndDate) + " " + (splitarray[w][0].toString()).slice(-2) + "-" + (splitarray[w][splitarray[w].length - 1].toString()).slice(-2);
                                    }
                                    else if (splitarray[w].length === 1) {
                                        _SqstartDateEndDate = ((_SqstartDateEndDate === undefined) ? "" : _SqstartDateEndDate) + " " + (splitarray[w][0].toString()).slice(-2);
                                    }

                                }
                                datasourcedata[i].StartDateEndDate = String(datasourcedata[i].StartDateEndDate).substring(0, 3) + " " + _SqstartDateEndDate;
                                return "1";
                            }

                        }
                    }

                    else if (RadioButtonSelectionWCT === "TimeOnly") {

                        datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime + " " + StartTimeEndTime;
                        datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ MINUS/g, "M");
                        datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ PLUS/g, "P");


                        var StartTimeEndTimeArray = datasourcedata[i].StartTimeEndTime.split(' ');
                        StartTimeEndTimeArray.sort(function (a, b) { return a - b });
                        var FinalDateTimeArray = StartTimeEndTimeArray.join(" ");
                        FinalDateTimeArray = FinalDateTimeArray.replace(/M/g, " MINUS");
                        FinalDateTimeArray = FinalDateTimeArray.replace(/P/g, " PLUS");
                        datasourcedata[i].StartTimeEndTime = FinalDateTimeArray;

                        datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                        var StartTimeArray = datasourcedata[i].TimeId.split(',');
                        StartTimeArray.sort(function (a, b) { return a - b });
                        var FinalStartTimeArray = StartTimeArray.join(",");
                        datasourcedata[i].TimeId = FinalStartTimeArray;


                        datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                        var EndTimeArray = datasourcedata[i].EndTimeId.split(',');
                        EndTimeArray.sort(function (a, b) { return a - b });
                        var FinalEndTimeArray = EndTimeArray.join(",");
                        datasourcedata[i].EndTimeId = FinalEndTimeArray;

                        return "1";

                    }

                    else if (RadioButtonSelectionWCT === "WeekDays") {

                        if (datasourcedata[i].StartTimeEndTime === StartTimeEndTime) {
                            datasourcedata[i].DateId = datasourcedata[i].DateId + "," + DateId;
                            datasourcedata[i].EndDateId = datasourcedata[i].EndDateId + "," + EndDateId;

                            datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                            var startimearray = datasourcedata[i].TimeId.split(',');
                            startimearray.sort(function(a, b){return a-b});
                            var jointstarttimearray = startimearray.join(',');
                            datasourcedata[i].TimeId = jointstarttimearray;

                            datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                            var endtimearray = datasourcedata[i].EndTimeId.split(',');
                            endtimearray.sort(function (a, b) { return a - b });
                            var jointendtimearray = endtimearray.join(',');
                            datasourcedata[i].EndTimeId = jointendtimearray;

                            var daysarray = datasourcedata[i].DateId.split(',');
                            daysarray.sort(function (a, b) { return a - b });
                            var days = daysarray.join(",");
                            datasourcedata[i].DateId = days;
                            datasourcedata[i].EndDateId = days;

                            var _StartDateEndDate;

                            for (var w = 0; w < daysarray.length ; w++) {
                                var weekdayElement = document.getElementById(parseInt(daysarray[w]));
                                if (_StartDateEndDate !== undefined) {

                                    _StartDateEndDate = _StartDateEndDate + " ";
                                    _StartDateEndDate = _StartDateEndDate + (weekdayElement.name);
                                }
                                else {
                                    _StartDateEndDate = (weekdayElement.name);
                                }
                            }
                            datasourcedata[i].StartDateEndDate = _StartDateEndDate;
                            datasourcedata[i].StartTimeEndTime = StartTimeEndTime;
                            return "1";
                        }
                        else if (datasourcedata[i].DateId === DateId) {
                            datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                            var StartTimeArray = datasourcedata[i].TimeId.split(',');
                            StartTimeArray.sort(function (a, b) { return a - b });
                            var FinalStartTimeArray = StartTimeArray.join(",");
                            datasourcedata[i].TimeId = FinalStartTimeArray;


                            datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                            var EndTimeArray = datasourcedata[i].EndTimeId.split(',');
                            EndTimeArray.sort(function (a, b) { return a - b });
                            var FinalEndTimeArray = EndTimeArray.join(",");
                            datasourcedata[i].EndTimeId = FinalEndTimeArray;

                            datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime + " " + StartTimeEndTime;
                            datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ MINUS/g, "M");
                            datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ PLUS/g, "P");
                            var StartTimeEndTimeArray = datasourcedata[i].StartTimeEndTime.split(' ');
                            StartTimeEndTimeArray.sort(function (a, b) { return a - b });
                            var FinalDateTimeArray = StartTimeEndTimeArray.join(" ");
                            FinalDateTimeArray = FinalDateTimeArray.replace(/M/g, " MINUS");
                            FinalDateTimeArray = FinalDateTimeArray.replace(/P/g, " PLUS");
                            datasourcedata[i].StartTimeEndTime = FinalDateTimeArray;
                            return "1";
                        }
                    }

                }
            }
            return "0";

        },

        _unique: function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        },

        _checkforDateContinuity: function checkforDateContinuity() {
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            if (datasourcedata.length > 0) {

                for (var i = datasourcedata.length - 1; i >= 0 ; i--) {
                    if (!datasourcedata[i].ContinuesEvent) {
                        for (var j = 0; j < datasourcedata.length; j++) {
                            if (datasourcedata[i].uid !== datasourcedata[j].uid) {
                                if (datasourcedata[i].DateId === datasourcedata[j].DateId && datasourcedata[i].IncludeorExclude === datasourcedata[j].IncludeorExclude) {
                                    datasourcedata[j].StartTimeEndTime = datasourcedata[j].StartTimeEndTime + " " + datasourcedata[i].StartTimeEndTime;


                                    var StartTimeEndTimeArray = datasourcedata[j].StartTimeEndTime.split(" ");
                                    StartTimeEndTimeArray.sort(function (a, b) { return a - b });
                                    var UniqueStartTimeEndTimeArray = thisIndex._unique(StartTimeEndTimeArray);
                                    var FinalDateTimeArray = UniqueStartTimeEndTimeArray.join(" ");
                                    datasourcedata[i].StartTimeEndTime = FinalDateTimeArray;

                                    datasourcedata[j].TimeId = datasourcedata[j].TimeId + "," + datasourcedata[i].TimeId;
                                    var StartTimeArray = datasourcedata[j].TimeId.split(',');
                                    StartTimeArray.sort(function (a, b) { return a - b });
                                    var FinalStartTimeArray = StartTimeArray.join(",");
                                    datasourcedata[j].TimeId = FinalStartTimeArray;


                                    datasourcedata[j].EndTimeId = datasourcedata[j].EndTimeId + "," + datasourcedata[i].EndTimeId;
                                    var EndTimeArray = datasourcedata[j].EndTimeId.split(',');
                                    EndTimeArray.sort(function (a, b) { return a - b });
                                    var FinalEndTimeArray = EndTimeArray.join(",");
                                    datasourcedata[j].EndTimeId = FinalEndTimeArray;


                                    var itemtoremove = datasourcedata[i];
                                    datasourcedata.remove(itemtoremove);
                                    j = datasourcedata.length;
                                    i--;
                                }
                                else if (datasourcedata[i].StartTimeEndTime === datasourcedata[j].StartTimeEndTime && datasourcedata[i].IncludeorExclude === datasourcedata[j].IncludeorExclude
                                    && String(datasourcedata[j].DateId).substring(4, 6) === String(datasourcedata[i].DateId).substring(4, 6)
                                    && datasourcedata[i].DateId !== datasourcedata[j].DateId) {
                                    datasourcedata[j].DateId = datasourcedata[j].DateId + "," + datasourcedata[i].DateId;
                                    datasourcedata[j].EndDateId = datasourcedata[j].EndDateId + "," + datasourcedata[i].EndDateId;

                                    var daysarray = datasourcedata[j].DateId.split(',');
                                    daysarray.sort(function (a, b) { return a - b });
                                    var days = daysarray.join(",");
                                    datasourcedata[j].DateId = days;
                                    datasourcedata[j].EndDateId = days;
                                    var _StartDateEndDate;
                                    if (datasourcedata[j].RadioButtonSelectionWCT === "WeekDays") {

                                        for (var w = 0; w < daysarray.length ; w++) {
                                            var weekdayElement = document.getElementById(parseInt(daysarray[w]));
                                            if (_StartDateEndDate !== undefined) {
                                                _StartDateEndDate = _StartDateEndDate + " ";
                                                _StartDateEndDate = _StartDateEndDate + (weekdayElement.name);
                                            }
                                            else {
                                                _StartDateEndDate = (weekdayElement.name);
                                            }
                                        }
                                        datasourcedata[j].StartDateEndDate = _StartDateEndDate;
                                    }
                                    else if (datasourcedata[j].RadioButtonSelectionWCT === "Calander") {
                                        var splitarray = thisIndex._split(daysarray);
                                        var _SqstartDateEndDate;
                                        for (var w = 0; w < splitarray.length; w++) {

                                            if (splitarray[w].length > 1) {
                                                _SqstartDateEndDate = ((_SqstartDateEndDate === undefined) ? "" : _SqstartDateEndDate) + " " + (splitarray[w][0].toString()).slice(-2) + "-" + (splitarray[w][splitarray[w].length - 1].toString()).slice(-2);
                                            }
                                            else if (splitarray[w].length === 1) {
                                                _SqstartDateEndDate = ((_SqstartDateEndDate === undefined) ? "" : _SqstartDateEndDate) + " " + (splitarray[w][0].toString()).slice(-2);
                                            }

                                        }
                                        datasourcedata[j].StartDateEndDate = String(datasourcedata[j].StartDateEndDate).substring(0, 3) + " " + _SqstartDateEndDate;
                                    }
                                    var itemtoremove = datasourcedata[i];
                                    datasourcedata.remove(itemtoremove);
                                    j = datasourcedata.length;
                                    i--;
                                }
                            }
                        }

                    }
                }

            }

        },
        _reformateview: function reformateview(p1) {
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            var action;
            var RadioButtonSelectionWCT;
            for (var i = 0; i < datasourcedata.length; i++) {

                if (datasourcedata[i].IncludeorExclude === "Include") {
                    action = datasourcedata[i].IncludeorExclude;
                    RadioButtonSelectionWCT = datasourcedata[i].RadioButtonSelectionWCT;
                }
            }

            if (action === "Include" && RadioButtonSelectionWCT === "Calander") {
                if (document.getElementById("Exclude").checked === false) {
                    document.getElementById("calendar").disabled = false;
                    document.getElementById("weekdays").disabled = true;
                    document.getElementById("timeonly").disabled = true;
                }
            }
            else if (action === "Include" && RadioButtonSelectionWCT === "WeekDays") {
                if (document.getElementById("Exclude").checked === false) {
                    document.getElementById("weekdays").disabled = false;
                    document.getElementById("calendar").disabled = true;
                    document.getElementById("timeonly").disabled = true;
                    document.getElementById("Exclude").disabled = false;
                }
            }
            else if (action === "Include" && RadioButtonSelectionWCT === "TimeOnly") {
                if (document.getElementById("Exclude").checked === false) {
                    document.getElementById("timeonly").disabled = false;
                    document.getElementById("calendar").disabled = true;
                    document.getElementById("weekdays").disabled = true;
                    document.getElementById("Exclude").disabled = false;
                }
            }
            else {
                if (p1 === "grid") {
                    document.getElementById('ClearGridandScheduler').click();
                }
                else {
                    document.getElementById("calendar").disabled = false;
                    document.getElementById("weekdays").disabled = false;
                    document.getElementById("timeonly").disabled = false;
                    document.getElementById("Exclude").disabled = true;
                }

            }
        },


        _AddEventstoGrid: function AddEventstoGrid() {
            // days of the week code
            thisIndex._Validate();
            var _StartTime = $("#FromTimeObject").val();
            var _EndTime = $("#ToTimeObject").val();
            var _StartTimeEndTime;
            var checkboxes = document.getElementsByClassName("checkboxDays");

            var _Daysofweek;
            var _dateId;
            var _enddateId;
            var _StartDateEndDate;
            var _starttimeId;
            var _endtimeId;

            var _startTimeTimeOnlyId;
            var _endTimeTimeOnlyId;

            if ($("#FromOperationSelection").val() === "empty") { $("#FromOperationSelection").val(":"); }
            if ($("#ToOperationSelection").val() === "empty") { $("#ToOperationSelection").val(":"); }
            if ($("#FromTimeObject").val() != 'SR' && $("#FromTimeObject").val() != 'SS') {
                _StartTime = _StartTime + ($("#FromOperationSelection").val());
                _StartTime = _StartTime + ($("#FromMinutesObject").val());
            }
            else {
                if (($("#FromOperationSelection").val() === " MINUS" || $("#FromOperationSelection").val() === " PLUS") && $("#FromMinutesObject").val() !== "00") {
                    _StartTime = _StartTime + ($("#FromOperationSelection").val());
                    _StartTime = _StartTime + ($("#FromMinutesObject").val());
                }
            }
            if ($("#ToTimeObject").val() != 'SR' && $("#ToTimeObject").val() != 'SS') {
                _EndTime = _EndTime + ($("#ToOperationSelection").val());
                _EndTime = _EndTime + ($("#ToMinutesObject").val());
            }
            else {
                if (($("#ToOperationSelection").val() === " MINUS" || $("#ToOperationSelection").val() === " PLUS") && $("#ToMinutesObject").val() !== "00") {
                    _EndTime = _EndTime + ($("#ToOperationSelection").val());
                    _EndTime = _EndTime + ($("#ToMinutesObject").val());
                }
            }

            if ($("#FromTimeObject").val() != 'SR' && $("#FromTimeObject").val() != 'SS') {
                _starttimeId = parseInt($("#FromTimeObject").val()) * 60;
            }
            else {
                if ($("#FromTimeObject").val() === 'SR') {
                    _starttimeId = 25 * 60;
                }
                else {
                    _starttimeId = 26 * 60;
                }
            }

            if ($("#FromOperationSelection").val() === " MINUS ") {

                _starttimeId = _starttimeId - parseInt(($("#FromMinutesObject").val()));
            }
            else {
                _starttimeId = _starttimeId + parseInt(($("#FromMinutesObject").val()));
            }

            if ($("#ToTimeObject").val() != 'SR' && $("#ToTimeObject").val() != 'SS') {
                _endtimeId = parseInt($("#ToTimeObject").val()) * 60;

            }
            else {
                if ($("#ToTimeObject").val() === 'SR') {
                    _endtimeId = 25 * 60;
                }
                else {
                    _endtimeId = 26 * 60;
                }
            }

            if ($("#ToOperationSelection").val() === " MINUS ") {

                _endtimeId = _endtimeId - parseInt(($("#ToMinutesObject").val()));
            }
            else {
                _endtimeId = _endtimeId + parseInt(($("#ToMinutesObject").val()));
            }

            _StartTimeEndTime = _StartTime + "-" + _EndTime;

            if (_StartTimeEndTime === "00:00-00:00") {
                _StartTimeEndTime = "H24";
                _endtimeId = (23 * 60) + 59;
            }


            _startTimeTimeOnlyId = _starttimeId
            _endTimeTimeOnlyId = _endtimeId;

            _starttimeId = _starttimeId.toString();
            _endtimeId = _endtimeId.toString();
            if (document.getElementById('timeonly').checked && _StartTimeEndTime !== "") {
                if (document.getElementById('Include').checked) {
                    check = thisIndex._checkforduplicates("Include", "0", "1", "DAILY", _starttimeId, _endtimeId);
                    if (check === "success") {
                        if (thisIndex._checkforContinuity("TimeOnly", "0", "1", "Include", "DAILY", _StartTimeEndTime, _starttimeId, _endtimeId) === "0") {
                            MydataSource.add({ RadioButtonSelectionWCT: "TimeOnly", DateId: "0", EndDateId: "1", TimeId: _starttimeId, EndTimeId: _endtimeId, ContinuesEvent: false, IncludeorExclude: "Include", StartDateEndDate: "DAILY", StartTimeEndTime: _StartTimeEndTime });
                        }
                    }
                    else {
                        thisIndex._Validate();
                        check = "success";
                        //alert("Overlapping Events or more than 7 day gap are not allowed");
                    }
                }
                else {
                    check = thisIndex._checkforduplicates("Exclude", "0", "1", "DAILY", _starttimeId, _endtimeId);
                    if (check === "success") {
                        if (thisIndex._checkforContinuity("TimeOnly", "0", "1", "Exclude", "DAILY", _StartTimeEndTime, _starttimeId, _endtimeId) === "0") {
                            MydataSource.add({ RadioButtonSelectionWCT: "TimeOnly", DateId: "0", EndDateId: "1", TimeId: _starttimeId, EndTimeId: _endtimeId, ContinuesEvent: false, IncludeorExclude: "Exclude", StartDateEndDate: "DAILY", StartTimeEndTime: _StartTimeEndTime });
                        }
                    }
                    else {
                        thisIndex._Validate();
                        check = "success";
                    }
                }
            }

            if (document.getElementById('weekdays').checked)
            {
                var OrginalStartTimeId = _starttimeId.valueOf();
                var OrginalEndTimeId = _endtimeId.valueOf();
                for (var i = 0, l = checkboxes.length; i < l; ++i) {
                    if (checkboxes[i].checked)
                    {

                        if (parseInt(OrginalEndTimeId) <= parseInt(OrginalStartTimeId))
                        {
                            _endtimeId = (parseInt(OrginalEndTimeId) + ((parseInt(checkboxes[i].id)) * 24 * 60) + (24 * 60)).toString();
                    }
                        else
                        {
                            _endtimeId = (parseInt(OrginalEndTimeId) + ((parseInt(checkboxes[i].id) - 1) * 24 * 60)).toString();
                        }

                        _starttimeId = (parseInt(OrginalStartTimeId) + ((parseInt(checkboxes[i].id) - 1) * 24 * 60)).toString();

                    // Get value of each selected checkbox and build the query string
                   
                        check = thisIndex._checkforduplicates(((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), checkboxes[i].id.toString(), checkboxes[i].id.toString(), checkboxes[i].name, _starttimeId, _endtimeId);
                        if (check === "success") {
                            if (thisIndex._checkforContinuity("WeekDays", checkboxes[i].id.toString(), checkboxes[i].id.toString(), ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _StartDateEndDate, _StartTimeEndTime, _starttimeId, _endtimeId) === "0") {
                                MydataSource.add({ RadioButtonSelectionWCT: "WeekDays", DateId: checkboxes[i].id.toString(), EndDateId: checkboxes[i].id.toString(), TimeId: _starttimeId.toString(), EndTimeId: _endtimeId.toString(), ContinuesEvent: false, IncludeorExclude: ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), StartDateEndDate: checkboxes[i].name, StartTimeEndTime: _StartTimeEndTime });
                                debugger;
                            }
                        }
                        else {
                            thisIndex._Validate();
                            check = "success";
                        }

                        thisIndex._checkforDateContinuity();
                    }
                }
            }
                // calendar code
            else if (document.getElementById('calendar').checked) {

                for (var i = 0; i < scheduler.dataSource.total() ; i++) {

                    var obj;
                    obj = scheduler.dataSource.data()[i];
                    obj.end.setDate(obj.end.getDate() - 1);



                    var dailyevent = 0;

                    if (String(obj.start).substring(0, 15) === String(obj.end).substring(0, 15)) {
                        dailyevent = 1;
                    }

                    _dateId = obj.start.getFullYear().toString() +
                              (((obj.start.getMonth() + 1) < 10) ? "0" + (obj.start.getMonth() + 1).toString() : (obj.start.getMonth() + 1).toString())
                              +
                              ((obj.start.getDate() < 10) ? "0" + obj.start.getDate() : obj.start.getDate());

                    _enddateId = obj.start.getFullYear().toString() +
                             (((obj.end.getMonth() + 1) < 10) ? "0" + (obj.end.getMonth() + 1).toString() : (obj.end.getMonth() + 1).toString())
                             +
                             ((obj.end.getDate() < 10) ? "0" + obj.end.getDate() : obj.end.getDate());


                    var now = new Date();
                    var start = new Date(now.getFullYear(), 0, 0);
                    var dateindays = thisIndex._getcurrentDateGap(_dateId, start);

                    if (parseInt(_endTimeTimeOnlyId) <= parseInt(_startTimeTimeOnlyId)) {
                        _endtimeId = (parseInt(_endTimeTimeOnlyId) + (dateindays * 24 * 60) + (24 * 60)).toString();
                    }
                    else {
                        _endtimeId = (parseInt(_endTimeTimeOnlyId) + (dateindays * 24 * 60)).toString();
                    }

                    _starttimeId = (parseInt(_startTimeTimeOnlyId) + (dateindays * 24 * 60)).toString();

                    if (document.getElementById('Continues').checked) {
                        while (Number(obj.start) !== Number(obj.end)) {
                            _StartDateEndDate = String(obj.start).substring(4, 10) + " " + _StartTime + "-" + ((dailyevent === 0) ? String(obj.start).substring(8, 10) : "") + " " + _EndTime;
                            check = thisIndex._checkforduplicates(((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _dateId, _enddateId, _StartDateEndDate, _starttimeId, _endtimeId);
                            if (check === "success") {

                                if (thisIndex._checkforContinuity("Calander", _dateId, _dateId, ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _StartDateEndDate, _StartTimeEndTime, _starttimeId, _endtimeId, true) === "0") {
                                    MydataSource.add({
                                        RadioButtonSelectionWCT: "Calander", DateId: _dateId, EndDateId: _dateId, TimeId: _starttimeId, EndTimeId: _endtimeId, ContinuesEvent: true, IncludeorExclude: ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"),
                                        StartDateEndDate: _StartDateEndDate.toUpperCase(), StartTimeEndTime: ""
                                    });
                                }
                            }

                            else {
                                thisIndex._Validate();
                                check = "success";
                            }

                            obj.start.setDate(obj.start.getDate() + 1);
                            _dateId = obj.start.getFullYear().toString() +
                              (((obj.start.getMonth() + 1) < 10) ? "0" + (obj.start.getMonth() + 1).toString() : (obj.start.getMonth() + 1).toString())
                              +
                              ((obj.start.getDate() < 10) ? "0" + obj.start.getDate() : obj.start.getDate());

                            dateindays = thisIndex._getcurrentDateGap(_dateId, start);

                            if (parseInt(_endTimeTimeOnlyId) <= parseInt(_startTimeTimeOnlyId)) {
                                _endtimeId = (parseInt(_endTimeTimeOnlyId) + (dateindays * 24 * 60) + (24 * 60)).toString();
                            }
                            else {
                                _endtimeId = (parseInt(_endTimeTimeOnlyId) + (dateindays * 24 * 60)).toString();
                            }

                            _starttimeId = (parseInt(_startTimeTimeOnlyId) + (dateindays * 24 * 60)).toString();

                        }
                        _StartDateEndDate = String(obj.start).substring(4, 10) + " " + _StartTime + "-" + ((dailyevent === 0) ? String(obj.end).substring(8, 10) + " " : "") + _EndTime;
                        check = thisIndex._checkforduplicates(((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _dateId, _enddateId, _StartDateEndDate, _starttimeId, _endtimeId);
                        if (check === "success") {

                            if (thisIndex._checkforContinuity("Calander", _dateId, _enddateId, ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _StartDateEndDate, _StartTimeEndTime, _starttimeId, _endtimeId, true) === "0") {
                                MydataSource.add({
                                    RadioButtonSelectionWCT: "Calander", DateId: _dateId, EndDateId: _enddateId, TimeId: _starttimeId, EndTimeId: _endtimeId, ContinuesEvent: true, IncludeorExclude: ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"),
                                    StartDateEndDate: _StartDateEndDate.toUpperCase(), StartTimeEndTime: ""
                                });
                            }
                        }

                        else {
                            thisIndex._Validate();
                            check = "success";
                        }

                    }
                    else {
                        while (Number(obj.start) !== Number(obj.end)) {
                            _StartDateEndDate = String(obj.start).substring(4, 10);
                            check = thisIndex._checkforduplicates(((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _dateId, _enddateId, _StartDateEndDate, _starttimeId, _endtimeId);
                            if (check === "success") {
                                if (thisIndex._checkforContinuity("Calander", _dateId, _dateId, ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _StartDateEndDate, _StartTimeEndTime, _starttimeId, _endtimeId) === "0") {
                                    MydataSource.add({
                                        RadioButtonSelectionWCT: "Calander", DateId: _dateId, EndDateId: _dateId, TimeId: _starttimeId, EndTimeId: _endtimeId, ContinuesEvent: false, IncludeorExclude: ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"),
                                        StartDateEndDate: _StartDateEndDate.toUpperCase(), StartTimeEndTime: _StartTimeEndTime
                                    });
                                }
                            }
                            else {
                                thisIndex._Validate();
                                check = "success";
                            }

                            obj.start.setDate(obj.start.getDate() + 1);
                            _dateId = obj.start.getFullYear().toString() +
                          (((obj.start.getMonth() + 1) < 10) ? "0" + (obj.start.getMonth() + 1).toString() : (obj.start.getMonth() + 1).toString())
                          +
                          ((obj.start.getDate() < 10) ? "0" + obj.start.getDate() : obj.start.getDate());

                            dateindays = thisIndex._getcurrentDateGap(_dateId, start);

                            if (parseInt(_endTimeTimeOnlyId) <= parseInt(_startTimeTimeOnlyId)) {
                                _endtimeId = (parseInt(_endTimeTimeOnlyId) + (dateindays * 24 * 60) + (24 * 60)).toString();
                            }
                            else {
                                _endtimeId = (parseInt(_endTimeTimeOnlyId) + (dateindays * 24 * 60)).toString();
                            }

                            _starttimeId = (parseInt(_startTimeTimeOnlyId) + (dateindays * 24 * 60)).toString();
                            dailyevent = 1;
                        }
                        _StartDateEndDate = String(obj.start).substring(4, 10) + ((dailyevent === 0) ? "-" + String(obj.end).substring(8, 10) : "");
                        check = thisIndex._checkforduplicates(((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _dateId, _enddateId, _StartDateEndDate, _starttimeId, _endtimeId);
                        if (check === "success") {
                            if (thisIndex._checkforContinuity("Calander", _dateId, _enddateId, ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"), _StartDateEndDate, _StartTimeEndTime, _starttimeId, _endtimeId) === "0") {
                                MydataSource.add({
                                    RadioButtonSelectionWCT: "Calander", DateId: _dateId, EndDateId: _enddateId, TimeId: _starttimeId, EndTimeId: _endtimeId, ContinuesEvent: false, IncludeorExclude: ((document.getElementById('Include').checked === true) ? "Include" : "Exclude"),
                                    StartDateEndDate: _StartDateEndDate.toUpperCase(), StartTimeEndTime: _StartTimeEndTime
                                });
                            }
                        }
                        else {
                            thisIndex._Validate();
                            check = "success";
                        }
                    }

                    thisIndex._checkforDateContinuity();
                    obj.end.setDate(obj.end.getDate() + 1);

                }
            }
            thisIndex._SortGrid();
        }

    };
});