﻿define(["jquery", "Resources", "notify", "utilities", "kendoExt", "kendo/kendo.grid.min", "kendo/kendo.tooltip.min", "kendo/kendo.window.min"], function($, res, notify, util) {

    var resources, self;
    var labels = {};
    var initialLoading = false;
    var showGridLoadingImage = false;

    var treeModel = {
        nodeType: "",
        id: "",
        name: "",
        items: []
    };

    return {
        type: "",
        category: -1,
        subjectId: "",
        uid: -1,
        treeView: null,

        init: function (lbs) {
            labels = lbs;
            resources = res.getResources("Dashboard");
            self = this;

            this.initReviewList();
            self.initUI();

            labels = lbs;
        },

        initReviewList: function () {
            notify.init();

            function getTreeviewDatasource() {
                $.ajax({
                    url: '/Dashboard/TreeView',
                    type: "GET",
                    data: {},
                    contentType: 'application/json',
                    dataType: 'json',
                    cache: false, // sometimes IE fails to make a proper request...
                    async: false
                })
                .done(function (data) {
                        self.treeView = data;
                    });
            }

            getTreeviewDatasource();

            $(notify).on('proposalStatusChange', function (event, packageId, status, date, displayId) {
                if ($("#ReviewList").data("kendoGrid").dataSource.data.length === 0) {
                    return;
                }
                var packge = $("#ReviewList").data("kendoGrid").dataSource.get(packageId);
                if (!packge) {
                    return;
                }

                // get notam syntax from server
                $.ajax({
                    url: '/Dashboard/GetNotamPackage',
                    type: "GET",
                    data: { PackageId: packageId },
                    contentType: 'application/json',
                    dataType: 'json',
                    async: true
                })
                .done(function (data) {
                    var notamPackage = $("#ReviewList").data("kendoGrid").dataSource.get(packageId);
                    if (notamPackage !== null && notamPackage !== undefined) {
                        var keys = Object.keys(data);
                        for (var k in keys) {
                            if (keys.hasOwnProperty(k)) {
                                if (notamPackage[keys[k]] && keys[k] !== "SDOSubject") {
                                    if (keys[k] === "Proposals") {
                                        // update top proposal rejection reason if required
                                        if (notamPackage.Proposals && notamPackage.Proposals.length > 0 && data.Proposals && data.Proposals.length > 0) {
                                            notamPackage.Proposals[0].set("RejectionReason", data.Proposals[0].RejectionReason);
                                        }
                                        if (data.Proposals[0].SubmittedNotam) {
                                            notamPackage.Proposals[0].SubmittedNotam = data.Proposals[0].SubmittedNotam;
                                            notamPackage.NotamNumber = data.Proposals[0].SubmittedNotam.Number;
                                        }

                                    } else {
                                        notamPackage.set(keys[k], data[keys[k]]);
                                    }
                                }
                            }
                        }

                        //updating Grid ID after is distributed (submitted) by INO
                        if (displayId !== "" && notamPackage.get("DisplayedNotamNumber") === "") {
                            notamPackage.set("DisplayedNotamNumber", displayId);
                        }
                    }
                })
                .fail(function (/*jqXHR, textStatus, err*/) {

                }).complete(function () {
                });
            });
        },

        initUI: function () {

            initialLoading = true;


            function onSearch() {
                var q = $("#txtSearchString").val();
                var treeview = $("#subjectTree").data("kendoTreeView");
                treeview.dataSource.query({
                    filter: {
                        logic: "or",
                        filters: [
                            { field: "Name", operator: "contains", value: q }
                        ]
                    }
                });
            };

            function onClear() {
                $("#txtSearchString").val("");
                var treeview = $("#subjectTree").data("kendoTreeView");
                treeview.dataSource.query({
                    filter: {
                        logic: "or",
                        filters: [
                            { field: "Name", operator: "contains", value: "" }
                        ]
                    }
                });
                treeview.select(".k-first");
                treeview.trigger('select', { node: treeview.select() });
                treeview.expand(treeview.findByText("Subject List"));
                // expand all nodes
                treeview.collapse(".k-item");
            };

            $("#btnSearch").kendoButton({
                click: onSearch
            });
            $("#txtSearchString").keypress(function (e) {
                if (e.keyCode === 13) {
                    onSearch();
                };
            });
            $("#btnClear").kendoButton({
                click: onClear
            });
            window.layout.middleLayout.close("west");

            $("#subjectTree").kendoTreeView({
                dataSpriteCssClassField: "SubjectType",
                dataSource: self.treeView,

                dataTextField: "name",

                select: function (e) {
                    if (!initialLoading) { //avoid kendo transport to call twice the server when page load.
                        var treeview = $("#subjectTree").data("kendoTreeView");
                        var data = treeview.dataItem(e.node);
                        if (data !== undefined && data !== null) {

                            treeModel.id = data.id;
                            treeModel.name = data.name;
                            treeModel.nodeType = data.nodeType;
                            $("#btnCreateNewNOTAMProposal").prop("disabled", treeModel.nodeType!=="Nsd");

                            var dataGrid = $('#ReviewList').data('kendoGrid');
                            if (dataGrid !== undefined && dataGrid !== null) {
                                showGridLoadingImage = true;
                                dataGrid.dataSource.read(treeModel);
                                dataGrid.refresh();
                            }
                        }
                    }
                },
                dataBound: function (/*e*/) {
                    if (initialLoading) {
                        initialLoading = false;
                        var treeview = $("#subjectTree").data("kendoTreeView");
                        var selectedNode = treeview.dataItem(treeview.select());
                        if (!selectedNode) {
                            treeview.select(".k-first");
                            treeview.trigger('select', { node: treeview.select() });
                        }
                    }
                }
            });

            $("#ReviewList").kendoTooltip({
                filter: "td:nth-child(7):contains('Rejected')", //this filter selects the first column cells
                position: "right",
                width: 250,

                content: function (e) {
                    var dataItem = $("#ReviewList").data("kendoGrid").dataItem(e.target.closest("tr"));
                    var content = "<div style='text-align: left' ><strong>Reason:</strong></br><p>" + dataItem.Proposals[0].RejectionReason + "</p><div>";
                    return content;
                }
            }).data("kendoTooltip");

            var nsdEditorWindow = $("#NSDEditor-Popup").kendoWindow({
                actions: ["Maximize", "Close"],
                draggable: true,
                resizable: true,
                pinned: true,
                visible: false,
                width: "90%",
                height: "90%",
                title: "",
                modal: true,
                close: function () { $("#NSDSelector-Popup").data("kendoWindow").close(); }
            }).data("kendoWindow");

            nsdEditorWindow.element.parent().attr("id", "NSDEditorContainer");
            nsdEditorWindow.element.parent().css({ top: 50, left: 50 });

            nsdEditorWindow.bind("refresh", function () {
                var winWidth = $(window).width() - 100;
                var winHeight = $(window).height() - 150;
                $("#NSDEditorContainer").css({ width: winWidth, height: winHeight });
                $("#NSDEditor-Popup").data("kendoWindow").open();
            });

            var nsdSelectorWindow = $("#NSDSelector-Popup").kendoWindow({
                height: 500,
                width: 800,
                resizable: false,
                visible: false,
                title: labels.selectNotamScenario,
                modal: true,
                close: function () {
                }
            }).data("kendoWindow");

            nsdSelectorWindow.bind("refresh", function () {
                $("#NSDSelector-Popup").data("kendoWindow").center();
                $("#NSDSelector-Popup").data("kendoWindow").open();
            });

            $("#NSDEditor-Popup").on("DataChanged", function (/*e*/) {
                debugger;
                self.refreshReviewList();
            });

            $("#ReviewList").kendoGrid({
                resizable: true,
                selectable: true,
                filterable: true,
                scrollable: true,
                sortable: {
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    buttonCount: 5
                },
                toolbar: kendo.template(self.fixTemplate($("#toolbar").html())),
                columns: [
                    { field: "DisplayedNotamNumber", title: labels.notamId, width: "5%" },
                    { field: "Name", title: labels.proposalType, width: "10%" },
                    { field: "TopParentSDOSubject.Designator", title: labels.locationLabel, width: "10%" },
                    { field: "SDOSubject.Designator", title: labels.nameLabel, width: "10%" },
                    { field: "Condition", title: labels.conditionLabel, width: "31%" },
                    { field: "FinalStartValidity", title: labels.startDate, width: "10%", template: "#=  (FinalStartValidity === null) ? ' ' : kendo.toString(kendo.parseDate(FinalStartValidity, 'dd/MM/yyyy HH:mm UTC'), 'dd/MM/yyyy HH:mm UTC') #" },
                    { field: "FinalEndValidity", title: labels.endDate, width: "10%", template: "#= (FinalEndValidity === null) ? ' ' : kendo.toString(kendo.parseDate(FinalEndValidity, 'dd/MM/yyyy HH:mm UTC'), 'dd/MM/yyyy HH:mm UTC') #" },
                    { field: "FinalStatus", title: labels.statusLabel, width: "10%" },
                    {
                        field: "FinalInternalStatus",
                        sortable: false,
                        filterable: false,
                        resizable: false,
                        title: " ",
                        width: "4%",
                        template: "#if(data.FinalInternalStatus === 12) { #<img src='/Images/Notifications/data-error.png'/>#}  else if(data.FinalInternalStatus === 4) { #<img src='/Images/Notifications/data-error.png'/>#} else if(data.FinalInternalStatus === 11) { #<img src='/Images/Notifications/data-warning.png'/>#} else if(data.FinalInternalStatus === 13) { #<img src='/Images/Notifications/data-ended.png'/>#} else if(data.FinalInternalStatus === 14) { #<img src='/Images/Notifications/data-enforced.png'/>#} else if(data.FinalInternalStatus === 5) { #<img src='/Images/Notifications/data-submitted.png'/>#} else if(data.FinalInternalStatus === 7) { #<img src='/Images/Notifications/data-withdrown.png'/>#} else if(data.FinalInternalStatus === 2) { #<img src='/Images/Notifications/data-work-in-progress.png'/>#} else if(data.FinalInternalStatus === 15) { #<img src='/Images/Notifications/cancelled.png'/>#} else if(data.FinalInternalStatus === 17) { #<img src='/Images/Notifications/ModifiedAndCancelled.png'/>#} else if(data.FinalInternalStatus === 16) { #<img src='/Images/Notifications/ModifiedAndEnforced.png'/>#} else if(data.FinalInternalStatus === 6) { #<img src='/Images/Notifications/ModifiedAndSubmitted.png'/>#} else if(data.FinalInternalStatus === 18) { #<img src='/Images/Notifications/data-expired.png'/>#} else { #<img src='/Images/Notifications/data-saved.png'/>#}#"
                    }
                ],
                detailTemplate: kendo.template($("#notamProposalTemplate").html()),
                detailInit: this.detailInit,
                height: "100%",
                width: "100%",
                change: function (/*e*/) {

                    var selectedRow = this.select();
                    var grid = $("#ReviewList").data("kendoGrid");

                    $("#ReviewList").find(".k-grid .k-state-selected").each(function (/*i*/) {
                        $(this).removeClass("k-state-selected");
                    });

                    self.updateActionOptions(grid.dataItem(selectedRow).FinalInternalStatus, grid.dataItem(selectedRow).FinalNotamType);
                },
                dataBound: function (/*e*/) {
                    $.connection.hub.start().done(function () {
                        console.log("client is now connected to SignalR hub");
                    });

                    if (self.selectedPackage === null) {
                        return;
                    }

                    var grid = $("#ReviewList").data("kendoGrid");

                    var newSelection = [];
                    var pageData = grid.dataSource.view();
                    for (var i = 0; i < pageData.length; i++) {
                        if (pageData[i].Id === self.selectedPackage) {
                            newSelection.push(grid.tbody.find(">tr[data-uid='" + pageData[i].uid + "']"));
                            break;
                        }
                    }
                    grid.select(newSelection);

                },
                dataSource: {
                    schema: {
                        model: { id: "Id" },
                        data: function (data) { return data.Data; }

                    },
                    sort: { field: "FinalInternalStatus", dir: "desc" },
                    transport: {
                        read: {
                            //url: "/Dashboard/ReviewList_Read",
                            url: "/Dashboard/LoadReviewList",
                            dataType: "json",
                            type: "POST",
                            data: function () {
                                return {
                                    "id": treeModel.id,
                                    "nodeType": treeModel.nodeType
                                }
                            }
                        }
                    },
                    requestStart: function () {
                        if (showGridLoadingImage) {
                            kendo.ui.progress($("#ReviewList"), true);
                        }
                    },
                    requestEnd: function () {
                        if (showGridLoadingImage) {
                            showGridLoadingImage = false;
                            kendo.ui.progress($("#ReviewList"), false);
                        }
                    }
                }
            });
            $("#ReviewList").kendoTooltip({
                filter: "td:nth-child(10)", //this filter selects the first column cells
                position: "right",
                content: function (e) {
                    var dataItem = $("#ReviewList").data("kendoGrid").dataItem(e.target.closest("tr"));
                    switch (dataItem.FinalInternalStatus) {
                        case 18:
                            return labels.expiredLabel;
                        case 17:
                            return labels.modifiedAndCaceled;
                        case 16:
                            return labels.modifiedAndEnforced;
                        case 15:
                            return labels.canceledLabel;
                        case 14:
                            return labels.enforcedLabel;
                        case 13:
                            return labels.endedLabel;
                        case 11:
                            return labels.soonToExpired;
                        case 9:
                            return labels.queuedLabel;
                        case 8:
                            return labels.withdrawnPending;
                        case 7:
                            return labels.withdrawnLabel;
                        case 6:
                            return labels.modifiedAndSubmitted;
                        case 5:
                            return labels.submittedLabel;
                        case 4:
                            return labels.rejectedLabel;
                        case 3:
                            return labels.pendingLabel;
                        case 2:
                            return labels.queuedLabel;
                        case 1:
                            return labels.draftLabel;
                        case 0:
                            return labels.undefinedLabel;
                        default:
                            return labels.savedLabel;
                    }
/*
                    var content = dataItem.Text;
                    return content;
*/
                }
            }).data("kendoTooltip");
            // map ui events
            $("#btnCreateNewNOTAMProposal").on('click', self.createNewNotam);
            $("#btnViewNOTAM").on('click', self.viewNotam);
            $("#btnCancelNOTAM").on('click', self.cancelNotam);
            $("#btnReplaceNOTAM").on('click', self.replaceNotam);
            $("#btnEditDraft").on('click', self.editDraft);
            $("#btnWithdrawProposal").on('click', self.withdrawProposal);
            $("#btnDeleteProposal").on('click', self.deleteProposal);
            $("#btnShowRejectionReason").on('click', self.showRejectReason);
            $("#quickAccessId").on("keyup", function (e) {
                if (e.keyCode === 13) {
                    var n = $("#quickAccessId").val();
                    if (n && n.length === 8)
                    {
                        var re = /^[a-zA-Z][0-9]{4}\/[0-9]{2}$/;
                        if (re.test(n))
                        {
                            $("#NSDEditor-Popup").data("kendoWindow")
                                .content("")
                                .refresh({
                                    url: "../NsdEditor/EditOrViewProposalByNotamNumber",
                                    type: "POST",
                                    data: { notamNumber: n }
                                });
                        }

                    }
                }
            });
            //$("#quickAccessId").on("keypress", function (evt) {
            //    evt = (evt) ? evt : window.event;
            //    var charCode = (evt.which) ? evt.which : evt.keyCode;
            //    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            //        return false;
            //    }
            //    return true;
            //});
        },

        fixTemplate: function (template) {
            template = template.replace(/\\u0026/g, "&");
            template = template.replace(/%23/g, "#");
            template = template.replace(/%3D/g, "=");
            template = template.replace(/&#32/g, " ");
            template = template.replace(/\\/g, "");
            template = template.replace(/#/g, "\\\\#");
            return template;
        },

        detailInit: function (e) {
            var detailRow = e.detailRow;

            detailRow.find(".detailGrid").kendoGrid({
                animation: {
                    open: { effects: "fadeIn" }
                },
                columns: [
                    { field: "ItemE", title: labels.messageLabel, width: "~300" },
                    { field: "StartValidity", title: labels.startDate, width: 160, template: "#=  (StartValidity === null) ? ' ' : kendo.toString(kendo.parseDate(StartValidity, 'dd/MM/yyyy HH:mm UTC'), 'dd/MM/yyyy HH:mm UTC') #" },
                    { field: "EndValidity", title: labels.endDate, width: 160, template: "#= (EndValidity === null) ? ' ' : kendo.toString(kendo.parseDate(EndValidity, 'dd/MM/yyyy HH:mm UTC'), 'dd/MM/yyyy HH:mm UTC') #" },
                    { field: "Type", title: labels.typeLabel, width: 115 }

                ],
                dataSource: {
                    schema: {
                        model: { id: "Id" },
                        data: function (data) { return data.Data; }

                    },
                    sort: { field: "StatusTime", dir: "desc" },
                    transport: {

                        read: {
                            url: "Dashboard/Proposal_Read",
                            dataType: "json",
                            type: "POST",
                            data: { "packageId": e.data.Id }
                        }
                    }
                }
            });
        },
        updateActionOptions: function (status, finalNotamType) {

            $('#btnViewNOTAM').show();

            switch (status) {

                case 4: // 'Rejected'
                    $('#btnShowRejectionReason').show();
                    $("#btnEditDraft").show();
                    $("#btnDeleteProposal").show();
                    $("#btnCancelNOTAM").hide();
                    $("#btnReplaceNOTAM").hide();
                    $("#btnWithdrawProposal").hide();
                    break;
                case 1: //  'Draft'                
                case 7: // 'Withdrawn'
                    $('#btnShowRejectionReason').hide();
                    $("#btnEditDraft").show();
                    $("#btnDeleteProposal").show();
                    $("#btnCancelNOTAM").hide();
                    $("#btnReplaceNOTAM").hide();
                    $("#btnWithdrawProposal").hide();

                    break;
                case 5:     // Submitted
                case 11:    // Soon To Expire
                case 6:     // Modified And Submitted
                case 14:    // Enforced
                case 16:    // Modified and Enforced
                    $('#btnShowRejectionReason').hide();
                    $("#btnEditDraft").hide();
                    if (finalNotamType !== "C") {
                        $("#btnCancelNOTAM").show();
                        $("#btnReplaceNOTAM").show();
                    }
                    else {
                        $("#btnCancelNOTAM").hide();
                        $("#btnReplaceNOTAM").hide();
                    }
                    $("#btnWithdrawProposal").hide();
                    $("#btnDeleteProposal").hide();

                    break;
                case 2:     // Queued
                    $('#btnShowRejectionReason').hide();
                    $("#btnEditDraft").hide();
                    $("#btnCancelNOTAM").hide();
                    $("#btnReplaceNOTAM").hide();
                    $("#btnWithdrawProposal").show();
                    $("#btnDeleteProposal").hide();

                    break;
                case 0:     // Undefined'
                case 3:     // Pending'
                case 9:     // Pending'
                case 8:     // Pending Withdraw'
                case 13:    // Ended
                case 15:    // Cancelled
                case 17:    // Modified and Cancelled
                case 18:    // Expired
                default:
                    $('#btnShowRejectionReason').hide();
                    $("#btnEditDraft").hide();
                    $("#btnCancelNOTAM").hide();
                    $("#btnReplaceNOTAM").hide();
                    $("#btnWithdrawProposal").hide();
                    $("#btnDeleteProposal").hide();

                    break;
            }
        },

        createNewNotam: function () {
            if (treeModel.nodeType === 'Nsd') {

                // TODO Remove when all  NSDs support the new NSD centric approach.
                if (([1,2].indexOf(treeModel.id) === -1 )) {
                    kendo.ui.ExtAlertDialog.show({
                        title: "Not supported yet!",
                        message: "This NSD does not support the new NSD centric approach yet.",
                        icon: "k-Ext-information"
                    });
                    return;
                }

                $("#NSDEditor-Popup")
                    .data("kendoWindow")
                    .content("")
                    .refresh({
                        url: "../NsdEditor/CreateNotamProposal",
                        type: "GET",
                        data: { nsdId: treeModel.id }
                    });
            }
        },
        showRejectReason: function (/*e*/) {
            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            var dataItem = grid.dataItem(selectedRow);

            kendo.ui.ExtAlertDialog.show({
                title: labels.rejectReason,
                message: dataItem.Proposals[0].RejectionReason,
                icon: "k-ext-information"
            });
        },

        deleteProposal: function (/*e*/) {
            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            var dataItem = grid.dataItem(selectedRow);

            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.confirmLabel,
                message: labels.confirmProposalDelete,
                width: '400px',
                height: '150px',
                icon: "k-ext-warning"
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
             ).done(function (response) {
                 if (response.button === 'OK') {
                     $.ajax({
                         url: '/NsdEditor/DeleteNotamProposal',
                         type: 'POST',
                         dataType: 'json',
                         async: false,
                         cache: false,
                         data: { packageId: dataItem.Id },
                         success: function (data) {
                             if (data.Success === true) {
                                 self.refreshReviewList();
                                 $('#btnShowRejectionReason').hide();
                                 $("#btnEditDraft").hide();
                                 $("#btnCancelNOTAM").hide();
                                 $("#btnReplaceNOTAM").hide();
                                 $("#btnWithdrawProposal").hide();
                                 $("#btnDeleteProposal").hide();
                                 $("#btnViewNOTAM").hide();
                             } else {

                                 util.showErrorMessage(labels.errorDeletingProposal, data);
                             }
                         },
                         error: function (a, b, c) {
                             util.showErrorMessage(labels.errorDeletingProposal, new { Code: "JS", Error: c });
                         }
                     });
                 }

             });

        },

        withdrawProposal: function (/*e*/) {
            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            var dataItem = grid.dataItem(selectedRow);

            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.confirmLabel,
                message: labels.confirmProposalWithdraw,
                width: '400px',
                height: '150px',
                icon: "k-ext-warning"
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
             ).done(function (response) {
                 if (response.button === 'OK') {

                     $.ajax({
                         url: '/NsdEditor/WithdrawNotamProposal',
                         type: 'POST',
                         dataType: 'json',
                         async: false,
                         cache: false,
                         data: { packageId: dataItem.Id },
                         success: function (data) {
                             if (data.Success === true) {
                                 self.refreshReviewList();
                                 kendo.ui.ExtAlertDialog.show({
                                     title: labels.successLabel,
                                     message: data.Message,
                                     icon: "k-ext-information"
                                 });
                             } else {
                                 util.showErrorMessage(labels.errorWithdrawingProposal, data);
                             }
                         },
                         error: function (a, b, c) {
                             util.showErrorMessage(labels.errorWithdrawingProposal, new { Code: "JS", Error: c });
                         }

                     });
                 }

             });

        },

        editDraft: function (/*e*/) {
            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            var dataItem = grid.dataItem(selectedRow);

            $("#NSDEditor-Popup").data("kendoWindow")
                .content("")
                .refresh({
                    url: "../NsdEditor/EditProposal",
                    type: "POST",
                    data: { packageId: dataItem.Id }
                });
        },

        viewNotam: function (/*e*/) {

            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            var dataItem = grid.dataItem(selectedRow);

            $("#NSDEditor-Popup").data("kendoWindow")
                .content("")
                .refresh({
                    url: "../NsdEditor/ViewProposal",
                    type: "POST",
                    data: { packageId: dataItem.Id }
                });
        },

        cancelNotam: function (/*e*/) {
            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            var dataItem = grid.dataItem(selectedRow);

            if (dataItem.NoUserInputCancel) {
                kendo.ui.ExtOkCancelDialog.show({
                    title: labels.confirmLabel,
                    message: labels.cancelNotamNotification + " [" + dataItem.Id + "]?",
                    width: '400px',
                    height: '150px',
                    icon: "k-ext-warning"
                }).done(function (response) {
                    if (response.button === 'OK') {
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: dataItem.NSDUrl + "/CancelProposalDirect",
                            data: { packageId: dataItem.Id },
                            async: false,
                            success: function (result) {
                                if (result.Success) {
                                    ///
                                    // Removed based in JAZZ-Feedback item #11485 
                                    ///
                                    //kendo.ui.ExtAlertDialog.show({
                                    //    title: labels.TD("Completed"),
                                    //    message: labels.TD("Cancel NOTAM Proposal Created Successfully."),
                                    //    icon: "k-ext-information"
                                    //});
                                } else {
                                    kendo.ui.ExtAlertDialog.show({
                                        title: labels.errorLabelCol + result.Code,
                                        message: labels.errorCreatingCaccelNotam + " \n" + result.Error,
                                        icon: "k-ext-error"
                                    });
                                }

                            },
                            error: function (/*xhr, ajaxOptions, thrownError*/) {
                                debugger;
                            }
                        });
                    }
                });
            } else {
                $("#NSDEditor-Popup").data("kendoWindow")
                   .content("")
                   .refresh({
                       url: "../NsdEditor/CancelProposal",
                       type: "POST",
                       error: function (xhr, error) {
                           handelError(labels.errorLabel, xhr, error);
                       },
                       data: { packageId: dataItem.Id }
                   });

            }

        },

        replaceNotam: function (/*e*/) {
            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            var dataItem = grid.dataItem(selectedRow);

            $("#NSDEditor-Popup").data("kendoWindow")
                .content("")
                .refresh({
                    url: "../NsdEditor/ReplaceProposal",
                    type: "POST",
                    error: function (xhr, error) {
                        handelError(labels.errorLabel, xhr, error);
                    },
                    data: { packageId: dataItem.Id },
                });
        },

        refreshReviewList: function () {
            var grid = $("#ReviewList").data("kendoGrid");
            var selectedRow = grid.select();
            if (selectedRow !== null && selectedRow !== undefined && selectedRow.length > 0) {
                self.selectedPackage = grid.dataItem(selectedRow).Id;
            }
            $('#ReviewList').data('kendoGrid').dataSource.read();
            $('#ReviewList').data('kendoGrid').refresh();
        }

    };
});