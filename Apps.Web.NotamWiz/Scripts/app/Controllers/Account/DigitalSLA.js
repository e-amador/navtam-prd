﻿define(["jquery", "Resources", "utilities"], function ($, res, Util) {

    var thisIndex, model;

    return {

        init: function () {
            thisIndex = this;
             model = $("#SLAdataModel").data("model");
            this.initUI();

        },

        initUI: function () {
            document.getElementById("DigitalSLAContent").value += model.Content;
            $("#AcceptSLA").change(function () {
                debugger;
                $.ajax({
                    cache: false,
                    type: "POST",
                    data: { "CurrentUserName": $("#CurrentUser").data("currentusername") },
                    url: '../Account/AcknowledgeSLA',
                });
                $("#DigitalSLAAPI-Popup").data("kendoWindow").close();
            }
            );
        }
    }
});