﻿define(["jquery", "signalr.hubs", "kendo/kendo.notification.min"], function ($) {
    
    var hubStarted = false;
    var notifier = null;

    return {

       
        init: function () {
           
            notifier = $(this);

            if (hubStarted) return;

            // map all signalr messages to javascript events

            var tryingToReconnect = false;
            $.connection.hub.reconnecting(function () {
                tryingToReconnect = true;
                notifier.trigger('reconnecting');
            });

            $.connection.hub.reconnected(function () {
                tryingToReconnect = false;
                notifier.trigger('reconnected');
            });

            $.connection.hub.disconnected(function () {
                if (tryingToReconnect) {
                    notifier.trigger('disconnected');
                }
            });
            
            $.connection.NotificationHub.client.addMessage = function (user, message, date) {notifier.trigger('addMessage', [user, message, date]); };
            $.connection.NotificationHub.client.userConnected = function (userName) { notifier.trigger('userConnected', [userName]); };
            $.connection.NotificationHub.client.userDisconnected = function (userName) { notifier.trigger('userDisconnected', [userName]); };
            $.connection.NotificationHub.client.proposalStatusChange = function (packageId, status, date, displayId) { notifier.trigger('proposalStatusChange', [packageId, status, date, displayId]); };
            $.connection.signalrAppenderHub.client.onLoggedEvent = function (formattedEvent, loggedEvent) { notifier.trigger('onLoggedEvent', [formattedEvent, loggedEvent]); };

            /*
            Esteban: The events below are not longer used (the action was triggered from the client app using signalR Groups). 
            They were replaced by proposalStatusChange where action is triggered by the TaskEngine in response to a Notam Proposal Status change
            
            $.connection.NotificationHub.client.refreshPackageStatus = function (packageId, status, date, displayId) { notifier.trigger('refreshPackageStatus', [packageId, status, date, displayId]); };
            $.connection.NotificationHub.client.RefreshPackageNotificationInternalStatus = function (packageId) { notifier.trigger('RefreshPackageNotificationInternalStatus', [packageId]); };
            */

            // stat hub for all notifications 
            $.connection.hub.start().done(function () {                
                hubStarted = true;
                notifier.trigger('connected');

            });
            
        }
    };
});