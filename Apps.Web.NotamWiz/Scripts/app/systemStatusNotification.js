﻿define(["jquery", "Resources", "notify", "kendo/kendo.notification.min", "kendo/kendo.window.min"], function ($, res, notify) {


    return {

        init: function () {
            // Chat Notify begin
            var notification = $("#notification").kendoNotification({

                autoHideAfter: 3000,
                stacking: "up",
                templates: [{
                    type: "connect",
                    template: $("#userConnectedTemplate").html()
                }, {
                    type: "disconnect",
                    template: $("#userDisconnectedTemplate").html()
                }]

            }).data("kendoNotification");


            $(notify).on("userConnected", function (event, userName) {
                notification.show({
                    title: "User " + userName + " Logged in",
                    message: ""
                }, "connect");
            });

            $(notify).on("userDisconnected", function (event, userName) {
                notification.show({
                    title: "User " + userName + " Logged out",
                    message: ""
                }, "disconnect");
            });


            $(notify).on("reconnecting", function (event) {

                //$("body").append('<div id="reconnectingMask" class="k-loading-mask" style="width:100%;height:100%"><div class="k-loading-image"><span class="k-loading-text">Reconnecting...</span><div class="k-loading-color"></div></div></div>');
                $("body").append('<div id="reconnectingWindow"></div>');
                $("#reconnectingWindow").kendoWindow({ actions: {}, modal: true,visible: false,draggable: false, title:"Connection Lost", width:400, height:200 });
                $("#reconnectingWindow").data("kendoWindow").content("<p style='padding:20px'>Please wait while the system try to reconnect...</p>").open().center();

                $('#systemUpIndicator').hide();
                $('#systemDownIndicator').show();
            });

            $(notify).on("disconnected", function (event) {

                var window = $("#reconnectingWindow").data("kendoWindow");
                if (window) {
                    window.close();
                }

                $("body").append('<div id="disconnectedWindow"></div>');
                $("#disconnectedWindow").kendoWindow({ actions: {}, modal: true, visible: false,draggable: false, title:"Connection is down", width:400, height:200 });

                $("#disconnectedWindow").data("kendoWindow").content("<p style='padding:20px'>The browser could not establish a connection to the server.</p><p>Please contact NAV Canada Customer Support for more information</p>").open().center();


                //$("#reconnectingMask").remove();
                //$("body").append('<div id="systemDownMask" class="k-loading-mask" style="width:100%;height:100%"><div class="k-loading-image"><span class="k-loading-text">System is DOWN...</span><div class="k-loading-color"></div></div></div>');

                $('#systemUpIndicator').hide();
                $('#systemDownIndicator').show();
            });

            $(notify).on("connected", this.connectionEstablished);
            $(notify).on("reconnected", this.connectionEstablished);


        },
        connectionEstablished: function () {
            var window = $("#reconnectingWindow").data("kendoWindow");
            if (window) {
                window.close();
            }
             var window = $("#disconnectedWindow").data("kendoWindow");
            if (window) {
                window.close();
            }

            //$("#reconnectingMask").remove();
            //$("#systemDownMask").remove();
            $('#systemDownIndicator').hide();
            $('#systemUpIndicator').show();
        }
    };
});
