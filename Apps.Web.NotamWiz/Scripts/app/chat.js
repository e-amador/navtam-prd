﻿define(["jquery","notify", "signalr.hubs"], function ($, notify) {
    return {
        init: function () {

            
            var toggle = false;
            var timer = null;

            $(notify).on("addMessage", function (event, user, message, date) {
                $("#chatStandby").hide();
                $("#chatActive").show();
                if (timer) {
                    clearTimeout(timer); //cancel the previous timer.
                    timer = null;
                }

                toggle = (!toggle) ? true : false;
                if (toggle) {
                    timer = setTimeout(function () {
                        $("#chatActive").hide();
                        $("#chatStandby").show();
                    }, 10000);
                }

                $('.fixedContent').append("<div class='userwrap'><span class='user'>" + user + " (" + date + "):</span><br><span class='messages'>" + message + "</span></div>");
                $(".fixedContent").scrollTop($(".fixedContent").height());
            });


            $(function () {



                var toggle = false;
                var user = '@User.Identity.Name';
                var searchBoxText = "Type here... (Use @@User for Private Message)";
                var fixIntv;
                var fixedBoxsize = $('#fixedChat').outerHeight() + 'px';
                var Parent = $("#fixedChat"); // cache parent div
                var Header = $(".fixedHeader"); // cache header div
                var Chatbox = $(".userinput"); // cache header div
                Parent.css('height', '30px');

                Header.click(function () {
                    toggle = (!toggle) ? true : false;
                    if (toggle) {
                        Parent.animate({ 'height': fixedBoxsize }, { duration: 300, queue: false });
                        Parent.animate({ 'width': '400px' }, { duration: 300, queue: false });
                        Parent.animate({ 'bottom': '32px' }, { duration: 300, queue: false });
                        Parent.animate({ 'right': '-3px' }, { duration: 300, queue: true });
                        Header.animate({ backgroundColor: '#005D7E' }, { duration: 300, queue: false });

                        Header.animate({ borderTopColor: "#3d5a99" }, { duration: 300, queue: false });
                        Header.animate({ borderLeftColor: "#3d5a99" }, { duration: 300, queue: false });
                        Header.animate({ borderRightColor: "#3d5a99" }, { duration: 300, queue: false });
                        Header.animate({ borderBottomColor: "#3d5a99" }, { duration: 300, queue: false });
                        $('#userinput').show();
                        $("#chatActive").hide();
                        $("#chatStandby").show();

                    }
                    else {
                        Parent.animate({ 'height': '30px' }, { duration: 300, queue: false });
                        Parent.animate({ 'width': '30px' }, { duration: 300, queue: false });
                        Parent.animate({ 'bottom': '0px' }, { duration: 300, queue: false });
                        Parent.animate({ 'right': '20px' }, { duration: 300, queue: true });
                        Header.animate({ backgroundColor: '#005D7E' }, { duration: 300, queue: false });

                        Header.animate({ borderTopColor: "#005D7E" }, { duration: 300, queue: false });
                        Header.animate({ borderLeftColor: "#005D7E" }, { duration: 300, queue: false });
                        Header.animate({ borderRightColor: "#005D7E" }, { duration: 300, queue: false });
                        Header.animate({ borderBottomColor: "#005D7E" }, { duration: 300, queue: false });
                        $('#userinput').hide();

                    }

                });

                function getDate() {
                    var d = new Date(),
                        minutes = d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes(),
                        hours = d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours(),
                        ampm = d.getHours() >= 12 ? 'pm' : 'am',
                        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                    return days[d.getDay()] + ' ' + months[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear() + ' ' + hours + ':' + minutes + ampm;
                }


                Chatbox.focus(function () {
                    $(this).val(($(this).val() == searchBoxText) ? '' : $(this).val());
                }).blur(function () {
                    $(this).val(($(this).val() == '') ? searchBoxText : $(this).val());
                }).keyup(function (e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13) {
                        
                        $.connection.hub.start().done(function () {
                            $.connection.NotificationHub.server.send($(e.target).val().replace(/(\r\n|\n|\r)/gm, ""));
                        });
                        $(this).val('');
                        e.preventDefault();
                        return false;
                    }

                });
            });

            $("#fixedChat").show();

        }
    }
});