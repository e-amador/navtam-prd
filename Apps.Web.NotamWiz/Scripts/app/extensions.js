﻿define(function () {
    Array.prototype.deepJoin = function (seperator) {
        seperator = typeof seperator !== 'undefined' ? seperator : '';

        var result = "";
        this.forEach(function (entry) {
            if (Array.isArray(entry)) {
                result += entry.deepJoin(seperator);
            } else {
                if (entry === null || entry === undefined) {
                    result += seperator;
                } else {
                    result += entry.toString() + seperator;
                }
            }
        });

        return result;
    }


    String.prototype.deepJoin = function () {
        return this;
    }

    String.prototype.hasNumber = function () {
        return /\d/.test(this)
    }

    String.prototype.insertAt = function (index, string) {
        return this.substr(0, index) + string + this.substr(index);
    }

    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };

    String.prototype.startsWith = function (prefix) {
        return this.slice(0, prefix.length) === prefix;
    };


});