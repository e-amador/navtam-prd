﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
    using NavCanada.Applications.NotamWiz.Web.Controllers;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Dtos;
    using NavCanada.Core.Domain.Model.Entitities;
    using NavCanada.Core.Domain.Model.Enums;

    using Westwind.Globalization;

    [Authorize(Roles = "Administrator")]
    public class AerodromeDisseminationCategoryController : BaseController
    {
        readonly ILog log = LogManager.GetLogger(typeof(AerodromeDisseminationCategoryController));

        public AerodromeDisseminationCategoryController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        public class ValueText
        {
            public int value { get; set; }
            public string text { get; set; }
            public string img { get; set; }
        }


        // GET: Admin/AerodromeDisseminationCategory
        public ActionResult Index()
        {
            log.Object(new { Method = "Index", Parameters = new { } });

            var model = new List<ValueText>{
                new ValueText{value    = (int)DisseminationCategory.National, text = "National"},
                new ValueText{value    = (int)DisseminationCategory.US, text = "US" },
                new ValueText{value    = (int)DisseminationCategory.International, text= "International"},
            };

            return View(model);
        }


        public ActionResult ADC_Read([DataSourceRequest]DataSourceRequest request)
        {

            this.log.Object(new { Method = "ADC_Read", Parameters = new { request = request } });

            var result = Uow.AerodromeDisseminationCategoryRepo.GetAll().Project().To<AerodromeDisseminationCategoryViewModel>();

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }

        private ActionResult ADC_Create([DataSourceRequest]DataSourceRequest request, AerodromeDisseminationCategoryViewModel adc)
        {

            this.log.Object(new { Method = "ADC_Create", Parameters = new { request = request, adc = adc } });

            // Will keep the inserted entitites here. Used to return the result later.
            var entities = new List<AerodromeDisseminationCategory>();
            //if (ModelState.IsValid)
            //{
            var entity = Mapper.Map<AerodromeDisseminationCategoryViewModel, AerodromeDisseminationCategory>(adc);
            Uow.AerodromeDisseminationCategoryRepo.Add(entity);
            entities.Add(entity);
            Uow.SaveChanges();

            // }
            // Return the inserted entities. The grid needs the generated ProductID. Also return any validation errors.
            return Json(entities.ToDataSourceResult(request, ModelState, s => Mapper.Map<AerodromeDisseminationCategory, AerodromeDisseminationCategoryViewModel>(s)));
        }

        public ActionResult ADC_Update([DataSourceRequest]DataSourceRequest request, AerodromeDisseminationCategoryViewModel adc)
        {

            this.log.Object(new { Method = "ADC_Update", Parameters = new { request = request, acd = adc } });

            // Will keep the updated entitites here. Used to return the result later.
            var entities = new List<AerodromeDisseminationCategory>();
            if (ModelState.IsValid)
            {
                var entity = Uow.AerodromeDisseminationCategoryRepo.GetById(adc.Id);
                if (entity != null)
                {
                    if (!string.IsNullOrEmpty(adc.AhpCodeId) && entity.AhpCodeId != adc.AhpCodeId)
                        entity.AhpCodeId = adc.AhpCodeId;

                    if (!string.IsNullOrEmpty(adc.AhpMid) && entity.AhpMid != adc.AhpMid)
                        entity.AhpMid = adc.AhpMid;

                    if (!string.IsNullOrEmpty(adc.AhpName) && entity.AhpName != adc.AhpName)
                        entity.AhpName = adc.AhpName;

                    if (entity.DisseminationCategory != adc.DisseminationCategory)
                        entity.DisseminationCategory = adc.DisseminationCategory;

                    Uow.AerodromeDisseminationCategoryRepo.Update(entity);
                    Uow.SaveChanges();
                }

            }
            // Return the updated entities. Also return any validation errors.
            return Json(entities.ToDataSourceResult(request, ModelState, s => Mapper.Map<AerodromeDisseminationCategory, AerodromeDisseminationCategoryViewModel>(s)));
        }



        // this method is not allowed currently
        private ActionResult ADC_Destroy([DataSourceRequest]DataSourceRequest request, AerodromeDisseminationCategoryViewModel adc)
        {
            this.log.Object(new { Method = "ADC_Destroy", Parameters = new { request = request, adc = adc } });

            // Will keep the destroyed entitites here. Used to return the result later.
            var entities = new List<AerodromeDisseminationCategory>();
            if (ModelState.IsValid)
            {
                var entity = Mapper.Map<AerodromeDisseminationCategoryViewModel, AerodromeDisseminationCategory>(adc);
                entities.Add(entity);
                Uow.AerodromeDisseminationCategoryRepo.Delete(entity);                
                Uow.SaveChanges();

            }
            // Return the destroyed entities. Also return any validation errors.
            return Json(entities.ToDataSourceResult(request, ModelState, s => Mapper.Map<AerodromeDisseminationCategory, AerodromeDisseminationCategoryViewModel>(s)));
        }


        private MultiPolygonDto GetTrinagleGeo(DbGeography point)
        {
            double lat1 = (double)point.Latitude;
            double lon1 = (double)point.Longitude;
            double lat2 = lat1 + 0.01;
            double lon2 = lon1 + 0.01;
            double lat3 = lat1 + 0.01;
            double lon3 = lon1 - 0.01;

            return  GeoDataService.GeoJsonMultiPolygonFromGeography(DbGeography.PolygonFromText("POLYGON((" + lon1.ToString() + " " + lat1.ToString() + "," +
                lon2.ToString() + " " + lat2.ToString() + "," +
                lon3.ToString() + " " + lat3.ToString() + "," +
                lon1.ToString() + " " + lat1.ToString() + "))", GeoDataService.SRID));

        }

        [HttpPost]
        public ActionResult UpdateAhpCategory(string mid, int category)
        {
            DisseminationCategory cat = (DisseminationCategory)category;
            AerodromeDisseminationCategory adc = Uow.AerodromeDisseminationCategoryRepo.GetAll().FirstOrDefault(a => a.AhpMid == mid);
            if(adc != null)
            {
                adc.DisseminationCategory = cat;                
                Uow.SaveChanges();

                return ProcessResult.GetSuccessResult();
            }
            return ProcessResult.GetErrorResult("ADCC-0001",DbRes.T("Invalid Aerodrome Dissemination Category Id" , "AerodromeDisseminationCategory"));

        }

        public ActionResult GetAhpsLocation()
        {

            var result = Uow.AerodromeDisseminationCategoryRepo.GetAll().Where(a=>a.AhpCodeId.Length==4).ToList().Select(a => new
                    {
                        Mid = a.AhpMid,
                        Code = a.AhpCodeId,
                        Title = a.AhpName,
                        Name = a.AhpCodeId +" - "+a.AhpName,
                        a.DisseminationCategory,
                        //Geo = a.Location == null ? null : GetTrinagleGeo(a.Location),
                        Longitude = a.Location.Longitude,
                        Latitude = a.Location.Latitude

                    });


            var serializer = new JavaScriptSerializer {MaxJsonLength = Int32.MaxValue};

            var jsonResult = new ContentResult
            {
                Content = serializer.Serialize(result),
                ContentType = "application/json"
            };

            return jsonResult;
        }

    }
}