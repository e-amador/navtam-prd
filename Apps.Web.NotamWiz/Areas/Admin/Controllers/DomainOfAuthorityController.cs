﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Microsoft.AspNet.Identity;

    using Code.Helpers;
    using Web.Controllers;
    using Models;
    using Core.Common.Common;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;


    [Authorize(Roles = "Administrator, CCS")]
    public class DomainOfAuthorityController : BaseController
    {
        readonly ILog _log = LogManager.GetLogger(typeof(DomainOfAuthorityController));

        public DomainOfAuthorityController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }


        // GET: Admin/DomainOfAuthority
        public async Task<ActionResult> Index()
        {
            string id = User.Identity.GetUserId();
            if (await IsUserInRoleAsync(id, CommonDefinitions.CcsRole))
            {
                ViewBag.CCSRole = true;
            }
            if (await IsUserInRoleAsync(id, CommonDefinitions.OrgAdminRole))
            {
                ViewBag.OrgAdminRole = true;
            }

            return View();
        }
        
        public ActionResult GetDOAs([DataSourceRequest] DataSourceRequest request)
        {
            var doas = Uow.DoaRepo.GetAll().AsEnumerable().Select(d => new DOAViewModel()
            {
                Id = d.Id,
                Description = d.Description,                   
                Name = d.Name
            });

            return Json(doas.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Create()
        {
            return View("DOAEditor", new DOAViewModel());
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public async Task<ActionResult> Edit(long doaId)
        {
            Doa doa = await Uow.DoaRepo.GetByIdWithFiltersAsync(doaId);
            if (doa == null)
            {
                return ProcessResult.GetErrorResult("DOAC-0001", DbRes.T("Invalid DOA Id.", "DOA"));
            }

            DOAViewModel vm = Mapper.Map<Doa, DOAViewModel>(doa??new Doa());

            vm.AssignToOrganizationIds = (await Uow.OrganizationRepo.GetOrgsAssignedToDoaAsync(doa.Id)).Select(o=>o.Id).ToList();

            return View("DOAEditor", vm);
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public async Task<ActionResult> Delete(long doaId)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    Doa doa = Uow.DoaRepo.GetById(doaId);

                    if (doa == null)
                    {
                        return ProcessResult.GetErrorResult("DOAC-0002", DbRes.T("Invalid DOA Id.", "DOA"));
                    }
                    var orgIdsUsingThisDoa = await Uow.OrganizationRepo.GetOrgsAssignedToDoaAsync(doaId);
                    if (orgIdsUsingThisDoa.Any())
                    {
                        return ProcessResult.GetErrorResult("DOAC-0003", DbRes.T("Can not delete DOA; DOA is used by one or more Organization", "DOA"));
                    }
                    Uow.DoaRepo.Delete(doa);
                    await Uow.SaveChangesAsync();
                    Uow.Commit();

                }
                catch (Exception ex)
                {
                    _log.Error(ex);
                    Uow.Rollback();
                    return ProcessResult.GetErrorResult("DOAC-0003", DbRes.T(ex.Message));
                }

                return ProcessResult.GetSuccessResult(DbRes.T("Delete successful"));
            }
            return ProcessResult.GetErrorResult("DOAC-0003", DbRes.T("Model Statte not valid."));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Create(DOAViewModel doavm)
        {
            try
            {
                Doa newDoa = Mapper.Map<DOAViewModel, Doa>(doavm, opt => { opt.Items["db"] = Uow; });

                Uow.DoaRepo.Add(newDoa);

                long[] orgsToAssignTo = doavm.AssignToOrganizationIds.ToArray();
                var orgs = await Uow.OrganizationRepo.GetOrgsByIdsAsync(orgsToAssignTo);
                foreach (Organization org in orgs)
                {
                    org.AssignedDoa = newDoa;
                    Uow.OrganizationRepo.Update(org);
                }
                

                await Uow.SaveChangesAsync();

                return ProcessResult.GetSuccessResult(DbRes.T("DOA Created Successfully", "DOA"), new {Id = newDoa.Id});
            }
            catch(Exception e)
            {
                _log.Error(e);
                Uow.Rollback();
                return ProcessResult.GetErrorResult("DOAC-0004", DbRes.T("Invalid DOA Id.", "DOA"));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Update(DOAViewModel doavm)
        {
            Uow.BeginTransaction();
            try
            {
                Doa doa = Uow.DoaRepo.GetById(doavm.Id);
                if(doa == null)
                {
                    return ProcessResult.GetErrorResult("DOAC-0005", DbRes.T("Invalid DOA Id.", "DOA"));
                }

                var orgs = await Uow.OrganizationRepo.GetOrgsAssignedToDoaAsync(doa.Id);
                foreach (Organization org in orgs)
                {
                    org.AssignedDoa = null;
                    Uow.OrganizationRepo.Update(org);
                }

                Mapper.Map<DOAViewModel, Doa>(doavm, doa, opt => { opt.Items["db"] = Uow; });
                Uow.DoaRepo.Update(doa);

                if(doavm.AssignToOrganizationIds != null && doavm.AssignToOrganizationIds.Any())
                {
                    long[] orgsToAssignTo =  doavm.AssignToOrganizationIds.ToArray();
                    orgs = await Uow.OrganizationRepo.GetOrgsByIdsAsync(orgsToAssignTo);
                    foreach (Organization org in orgs)
                    {
                        org.AssignedDoa = doa;
                        Uow.OrganizationRepo.Update(org);
                    }
                }

                await Uow.SaveChangesAsync();

                Uow.Commit();

                return ProcessResult.GetSuccessResult(DbRes.T("DOA Updated Successfully", "DOA")); //JsonRequestBehavior.AllowGet if the Get verb is used
             }
            catch(Exception e)
            {
                _log.Error(e);
                Uow.Rollback();
                return ProcessResult.GetErrorResult("DOAC-0004", DbRes.T("Invalid DOA Id.", "DOA"));
            }
        }

    }
}