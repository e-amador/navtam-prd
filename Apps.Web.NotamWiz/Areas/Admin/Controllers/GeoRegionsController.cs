﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Code.Extentions;
    using Code.Helpers;
    using Web.Controllers;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;

    [Authorize(Roles = "Administrator")]
    public class GeoRegionsController : BaseController
    {
        readonly ILog _log = LogManager.GetLogger(typeof(GeoRegionsController));

        // GET: Admin/GeoRegions
        public GeoRegionsController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        public ActionResult Index()
        {
            _log.Object(new { Method = "Index", Parameters = new { } });

            return View();
        }

        public async Task<ActionResult> GetGeoRegions([DataSourceRequest] DataSourceRequest request)
        {
            _log.Object(new { Method = "GetGeoRegions", Parameters = new { request = request } });

            var geoRegions = await Uow.GeoRegionRepo.GetAllAsync();

            var result = Mapper.Map<List<GeoRegion>, List<GeoRegionViewModel>>(geoRegions);

            return Json(result.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Create()
        {
            _log.Object(new { Method = "Create/Get" });
            return View("GeoRegionEditor", new GeoRegionViewModel());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Create(GeoRegionViewModel model)
        {
            _log.Object(new { Method = "Create/Post", Parameters = new { regionVM = model } });

            var newRegion = Mapper.Map<GeoRegionViewModel, GeoRegion>(model);

            newRegion.Geo = newRegion.Geo.MakeValid();
            Uow.GeoRegionRepo.Add(newRegion);
            await Uow.SaveChangesAsync();

            return Json(Mapper.Map<GeoRegion, GeoRegionViewModel>(newRegion));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public async Task<ActionResult> Edit(int id)
        {
            _log.Object(new { Method = "Edit/Post", Parameters = new { id = id } });

            var geoRegion = await Uow.GeoRegionRepo.GetByIdAsync(id);

            return View("GeoRegionEditor", Mapper.Map<GeoRegion, GeoRegionViewModel>(geoRegion));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Update(GeoRegionViewModel model)
        {
            _log.Object(new { Method = "Update", Parameters = new { regionVM = model } });
            try
            {
                var geoRegion = await Uow.GeoRegionRepo.GetByIdAsync(model.Id);
                if (geoRegion != null)
                {
                    var updatedRegion = Mapper.Map<GeoRegionViewModel, GeoRegion>(model);
                    geoRegion.Geo = updatedRegion.Geo.MakeValid();

                    Uow.GeoRegionRepo.Update(geoRegion);
                    await Uow.SaveChangesAsync();
                    return Json(Mapper.Map<GeoRegion, GeoRegionViewModel>(geoRegion));
                }
                return ProcessResult.GetErrorResult("GRC-0001", DbRes.T("Can't update Geographical Region; Region not found.", "GeoRegions"));
            }
            catch (Exception ex)
            {
                _log.Object(new { Method = "UPdate", Parameters = new { id = model.Id }, Exception = ex.Message });
                return ProcessResult.GetErrorResult("GRC-0001", DbRes.T("Can't update Geographical Region.", "GeoRegions"));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Delete(int id)
        {
            _log.Object(new { Method = "Delete", Parameters = new { id = id } });

            try
            {
                var updatedRegion = await Uow.GeoRegionRepo.GetByIdAsync(id);

                if (updatedRegion != null)
                {
                    Uow.GeoRegionRepo.Delete(updatedRegion);
                    await Uow.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _log.Object(new { Method = "Delete", Parameters = new { id = id }, Exception = ex.Message });
                // TODO(omar): handle other errors
                return ProcessResult.GetErrorResult("GRC-0001", DbRes.T("Can't delete Geographical Region; Region in Use.", "GeoRegions"));
            }

            return ProcessResult.GetSuccessResult();
        }
    }
}