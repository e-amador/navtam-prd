﻿using System;
using System.Net;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Microsoft.AspNet.Identity;

    using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
    using NavCanada.Applications.NotamWiz.Web.Controllers;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Common.Common;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    using Westwind.Globalization;

    [Authorize(Roles = "Administrator, CCS")]
    public class OrganizationController : BaseController
    {
        
        public OrganizationController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        [Authorize(Roles = "Administrator, Organization Administrator, CCS")]
        public async Task<ActionResult> Index()
        {
            ViewBag.Breadcrumb = DbRes.T("Organization", "Admin");
                       
            string userId = User.Identity.GetUserId();
            await SetViewBagRolesForCssAndAdminAsync(userId);

            // collect all the DOAs
            var doaList = await Uow.DoaRepo.GetAllAsync();
            ViewBag.DoaList = doaList.Select(doa => new
            {
                value = doa.Id,
                text = doa.Name,
                description = doa.Description
            }).ToList();

            if (await IsUserInRoleAsync(userId, CommonDefinitions.OrgAdminRole))
            {
                return View(await GetUserOrganizationAsync());
            }

            // Is this list used in the view?
            var organizations = await Uow.OrganizationRepo.GetAllAsync();
            return View(organizations);
        }

        [Authorize(Roles = "Administrator, Organization Administrator, CCS")]
        public  async Task<ActionResult> HierarchyBinding_Organizations([DataSourceRequest] DataSourceRequest request)
        {
            string Id = User.Identity.GetUserId();
            if (await IsUserInRoleAsync(Id, CommonDefinitions.OrgAdminRole))
            {
                List<OrganizationViewModel> result = new List<OrganizationViewModel>()
                {
                    Mapper.Map<Organization, OrganizationViewModel>(await GetUserOrganizationAsync())
                };
                
                return Json( result.ToDataSourceResult(request));
            }
            else
            {
                var organizations = await Uow.OrganizationRepo.GetAllAsyncWithDoa();
                var mapped = organizations.Select(org => Mapper.Map<Organization, OrganizationViewModel>(org)).ToList();
                return Json(mapped.ToDataSourceResult(request));
                //return Json(Uow.OrganizationRepo.GetAll().AsEnumerable().Select(org => Mapper.Map<Organization, OrganizationViewModel>(org)).ToDataSourceResult(request));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<ActionResult> Create([DataSourceRequest] DataSourceRequest request, OrganizationViewModel orgVm)
        {
            ModelState.Remove("Id");

            if (ModelState.IsValid)
            {
                try
                {
                    Organization org = Mapper.Map<OrganizationViewModel, Organization>(orgVm);

                    if (await OrganizationExists(org.Name))
                    {
                        string create_error = DbRes.T("Organization " + org.Name + " already exists in database");
                        ModelState.AddModelError("Name", create_error);
                        return ProcessResult.GetErrorResult("ORGC-02", create_error);
                        //return Json(orgVM);
                    }
                    Uow.OrganizationRepo.Add(org);
                    Uow.SaveChanges();
                    return Json(Mapper.Map<Organization, OrganizationViewModel>(org));

                }
                catch (Exception)
                {
                    Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                    var errors = new List<string> {DbRes.T("Invalid Operation has ocurred", "OrgManagementRes")};
                    return Json(errors);
                }
            }

            var errorList = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(errorList); 
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize(Roles = "Administrator, CCS")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, OrganizationViewModel orgVM)
        {

            if (orgVM.IsFic && string.IsNullOrEmpty(orgVM.FicId))
                ModelState.AddModelError("FIC", DbRes.T("FIC selection is required when the Organization is flagged as a FIC.", "OrganizationRes"));

            if (ModelState.IsValid)
            {
                

                try
                {
                    Organization org = Uow.OrganizationRepo.GetById(orgVM.Id);

                    if (org != null && !org.IsReadOnlyOrg)
                    {
                        org = Mapper.Map<OrganizationViewModel, Organization>(orgVM, org);
                        Uow.OrganizationRepo.Update(org);
                        Uow.SaveChanges();
                        return Json(Mapper.Map<Organization, OrganizationViewModel>(org));
                    }

                }
                catch (System.Exception e)
                {
                    string update_error = DbRes.T(e.Message);
                    ModelState.AddModelError("Name", update_error);
                    return ProcessResult.GetErrorResult("ORGC-02", update_error);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                var errors = ModelState.Select(x => x.Value.Errors)
                        .Where(y => y.Count > 0)
                        .ToList();
                string error = errors[0][0].ErrorMessage;
                return ProcessResult.GetErrorResult("ORGC-02", DbRes.T(error, "NsdEditor"));
            }

            return Json(orgVM);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize(Roles = "Administrator, CCS")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, OrganizationViewModel orgVM)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Organization org = Uow.OrganizationRepo.GetById(orgVM.Id);

                    if (org != null && !org.IsReadOnlyOrg)
                    {
                        if (org.Users.Where(e => e.IsDeleted == false).Count() == 0) 
                        {
                            Uow.OrganizationRepo.Delete(org);
                            Uow.SaveChanges();
                            return Json(Mapper.Map<Organization, OrganizationViewModel>(org));
                        }
                        else
                        {
                            string delete_error = DbRes.T("Organization " + org.Name + " still has users associated to it.");
                            ModelState.AddModelError("Name", delete_error);
                            return ProcessResult.GetErrorResult("ORGC-02", delete_error);
                        }
                    }
                }
                catch (System.Exception e)
                {
                    string delete_error = DbRes.T(e.Message);
                    ModelState.AddModelError("Name", delete_error);
                    return ProcessResult.GetErrorResult("ORGC-02", delete_error);
                }
            }
            return null;
        }

        public async Task<bool> OrganizationExists(string name)
        {
            return await Uow.OrganizationRepo.ExistsByNameAsync(name);
        }

    }
}