﻿using Microsoft.AspNet.Identity;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;
    using Web.Controllers;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;

    [Authorize(Roles = "CCS")]
    public class WaitingApprovalController : BaseController
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(WaitingApprovalController));

        public WaitingApprovalController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        public async Task<ActionResult> Index()
        {
            await SetViewBagRolesForCssAndAdminAsync(User.Identity.GetUserId());

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> ReadWaitingApprovalUsers([DataSourceRequest] DataSourceRequest request, 
            [Bind(Prefix = "models")] IEnumerable<UserRegistrationManagementViewModel> userRegistrationManagement)
        {
            var userProfiles = await Uow.UserProfileRepo.GetUnconfirmedEmailUsersAsync();

            var userRegMngtViewModelLst = Mapper.Map<List<UserProfile>, List<UserRegistrationManagementViewModel>>(userProfiles);

            for (var iter = 0; iter < userProfiles.Count; iter++)
            {
                var userProfile = userProfiles[iter];
                var userRegMngtViewModel = userRegMngtViewModelLst[iter];

                //if (userProfile.RegistrationId.HasValue)
                {
                    //var registration = await Uow.RegistrationRepo.GetByIdAsync(userProfile.RegistrationId.Value);
                    userRegMngtViewModel.userVM = Mapper.Map<UserProfile, UserManagementViewModel>(userProfile);

                    //if (registration != null)
                        //userRegMngtViewModel.orgVM = Mapper.Map<Registration, OrganizationViewModel>(registration);
                }
            }

            return (Json(userRegMngtViewModelLst.ToDataSourceResult(request)));
        }

        public async Task<ActionResult> DeleteUser(string userId)
        {
            try
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user == null)
                    throw new NullReferenceException();

                var result = await UserManager.DeleteAsync(user);
                if( !result.Succeeded )
                    throw new InvalidOperationException();

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                _logger.Error("Deleting User", e);
                //TODO: Add more specific error message.
                return Json(new
                {
                    Success = false,
                    Message = string.Format("Error trying to delete user with id={0}!", userId )
                });
            }
        }

        public async Task<ActionResult> ResendEmail(string userId)
        {
            try
            {
                var userProfile = await UserManager.FindByIdAsync(userId);
                if (userProfile == null || (!userProfile.RegistrationId.HasValue))
                    throw new NullReferenceException(string.Format("User [{0}] was not found in our system.", userId));

                var userRegistrationsInfo = await Uow.RegistrationRepo.GetByIdAsync(userProfile.RegistrationId.Value);

                if( userRegistrationsInfo == null )
                    throw new NullReferenceException(string.Format("There is not registration record linked to the user {0}.", userProfile.UserName));

                var tokenConfirmation = await UserManager.GenerateEmailConfirmationTokenAsync(userId);

                //Publish Mto to target Queeue
                var mto = CreateMtoForUserRegistration(MessageTransferObjectId.RegistrationNotificationEmail,null,
                    userRegistrationsInfo.Id, RegistrationStatus.ConfirmEmail, Uri.EscapeDataString(tokenConfirmation));

                PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                await Uow.SaveChangesAsync();

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                _logger.Error("Resending Email to UserId " + userId, e);
                //TODO: Add more specific error message.
                return Json(new
                {
                    Success = false,
                    Message = string.Format("Error trying to send email to user with id={0}!", userId )
                });
            }
        }
    }
}