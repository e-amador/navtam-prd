﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
using NavCanada.Applications.NotamWiz.Web.Controllers;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class NavAidDisseminationCategoryController : BaseController
    {

        readonly ILog log = LogManager.GetLogger(typeof(NavAidDisseminationCategoryController));

        public NavAidDisseminationCategoryController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }


        internal class ValueText
        {
            public int value { get; set; }
            public string text { get; set; }

        }

        // GET: Admin/NavAidDisseminationCategory
        public ActionResult Index()
        {
            this.log.Object(new { Method = "Index", Parameters = new { } });

            ViewBag.DisseminationLevels = new List<ValueText>(){
            new ValueText(){value    = (int)DisseminationCategory.National, text = "National"},
            new ValueText(){value    = (int)DisseminationCategory.US, text = "US"},
            new ValueText(){value    = (int)DisseminationCategory.International, text= "International"},
            };


            return View();
        }
    }
}