﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using System;
    using System.Linq;

    using AutoMapper;

    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Domain.Model.Entitities;

    public class UserProfile2UserManagementViewModel : ITypeConverter<UserProfile, UserManagementViewModel>
    {
        ILog log = LogManager.GetLogger(typeof(UserProfile2UserManagementViewModel));

        public UserManagementViewModel Convert(ResolutionContext context)
        {
            UserProfile userProfile = context.SourceValue as UserProfile;
            try
            {
                return new UserManagementViewModel()
                {

                    UserId = userProfile.Id,
                    EmailAddress = userProfile.Email,
                    UserName = userProfile.UserName,
                    FirstName = userProfile.FirstName,
                    LastName = userProfile.LastName,
                    UserOrgId = userProfile.OrganizationId,
                    IsApproved = userProfile.IsApproved,
                    IsDeleted = userProfile.IsDeleted,
                    IsDisabled = userProfile.LockoutEnabled,
                    //Password = "*****",
                    //ConfirmPassword = "*****",
                    Telephone = userProfile.PhoneNumber,
                    Address = userProfile.Address,
                    Fax = userProfile.Fax,
                    Position = userProfile.Position,
                    UserRoles = userProfile.Roles.Select(r => r.RoleId).ToList()

                };


            }
            catch (Exception ex)
            {
                this.log.Error("Exception converting from UserProfile (" + userProfile.UserName + ") to UserManagementVM", ex);
                return null;
            }
        }


    }



    public class UserManagementViewModel2UserProfileConverter : ITypeConverter<UserManagementViewModel, UserProfile>
    {
        ILog mylog = LogManager.GetLogger(typeof(UserManagementViewModel2UserProfileConverter));
        public UserProfile Convert(ResolutionContext context)
        {

            UserManagementViewModel umVM = context.SourceValue as UserManagementViewModel;
            try
            {
                UserProfile up = (UserProfile)context.DestinationValue ?? new UserProfile();
                up.Id = umVM.UserId;
                up.Email = umVM.EmailAddress;
                up.FirstName = umVM.FirstName;
                up.LastName = umVM.LastName;
                up.UserName = umVM.UserName;
                up.IsApproved = umVM.IsApproved;
                up.IsDeleted = umVM.IsDeleted;
                up.LockoutEnabled = umVM.IsDisabled;
                up.Position = umVM.Position;
                up.PhoneNumber = umVM.Telephone;
                up.Address = umVM.Address;
                up.Fax = umVM.Fax;
                up.OrganizationId = umVM.UserOrgId == null ? -1 : umVM.UserOrgId;

                return up;
            }
            catch (Exception ex)
            {
                this.mylog.Error("Exception converting from UserManagementVM (" + umVM.UserName + ") to UserProfile", ex);
                return null;
            }
        }
    }
}