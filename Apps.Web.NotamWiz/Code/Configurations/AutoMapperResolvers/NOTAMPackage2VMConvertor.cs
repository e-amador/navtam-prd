﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    public class NOTAMPackage2VMConvertor : ITypeConverter<NotamPackage, NotamPackageViewModel>
    {
        public NotamPackageViewModel Convert(ResolutionContext context)
        {
            IUow db = (IUow)context.Options.Items["db"];

            NotamPackage package = (NotamPackage)context.SourceValue;
            
            Nsd nsd = db.NsdRepo.GetById(package.NsdId);

            NotamProposal lastProposal = package.Proposals == null || package.Proposals.Count == 0 ? null : package.Proposals.Last();

            NsdVersion nsdVer = lastProposal == null ? 
                db.NsdVersionRepo.GetLatest(package.NsdId) :
                db.NsdVersionRepo.GetByVersion(package.NsdId, lastProposal.NsdVersion);
            
            return new NotamPackageViewModel
            {

                Id = package.Id,
                Name = package.Name,    
                NoUserInputCancel = nsdVer.NoUserInputCancel,
                NSDUrl = "/NSD/" + nsd.FilesLocation + "/V" + nsdVer.Version.ToString().Replace(".", "R"),
                Proposals = Mapper.Map<List<NotamProposal>, List<NotamProposalViewModel>>(package.Proposals.ToList()),
                SDOSubject = package.SdoSubject == null ? null : Mapper.Map<SdoSubject, SdoSubjectViewModel>(package.SdoSubject),
                TopParentSDOSubject = package.SdoSubject == null ? null : Mapper.Map<SdoSubject, SdoSubjectViewModel>(db.SdoSubjectRepo.GetTopParent(package.SdoSubject.Id)),
                SDOSubjectId = package.SdoSubjectId,
                NSDId = package.NsdId,
                SubjectCategory = package.SubjectCategory.ToString(),
                SubjectType = package.SubjectType,
                TempSubjectId = package.TempSubjectId,
            };

        }
    }
}