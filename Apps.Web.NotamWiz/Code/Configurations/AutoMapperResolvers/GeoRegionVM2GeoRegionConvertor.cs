﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using AutoMapper;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Domain.Model.Entitities;

    public class GeoRegionVm2GeoRegionConvertor : ITypeConverter<GeoRegionViewModel, GeoRegion>
    {
        ILog log = LogManager.GetLogger(typeof(GeoRegionVm2GeoRegionConvertor));

        public GeoRegion Convert(ResolutionContext context)
        {
            this.log.Object(new { Method = "Convert", Parameters = new { context = context } });

            var region = context.SourceValue as GeoRegionViewModel;

            return new GeoRegion
            {
                Id = region.Id,
                Name = region.Name,
                Geo = GeoDataService.GetDbGeographyFromMultiPolygon(region.Geo)

            };

        }
    }
}