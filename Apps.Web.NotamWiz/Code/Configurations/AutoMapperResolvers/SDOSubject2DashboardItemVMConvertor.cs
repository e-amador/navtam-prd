﻿using AutoMapper;
using NavCanada.SE.NOTAMWiz.Models;
using NavCanada.SE.NOTAMWiz.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NavCanada.SE.NotamWiz.UI.Wiz.Web.Models;

namespace NavCanada.SE.NOTAMWiz.App_Start.AutoMapperResolvers
{
    public class SDOSubject2DashboardItemVMConvertor : ITypeConverter<SDOSubject, DashboardItemViewModel>
    {


        public DashboardItemViewModel Convert(ResolutionContext context)
        {
            NOTAMWizDBContext db  = (NOTAMWizDBContext)context.Options.Items["db"];
            SDOSubject d = context.SourceValue as SDOSubject;

            DashboardItemViewModel dvm = new DashboardItemViewModel()
            {
                Name = d.Designator == null ? d.Name : d.Designator + " - " + d.Name,
                HasChildren = db.SDOSubjects.Any(a => a.ParentId == d.Id),
                Type = enDashboardItemTypes.SDOSubject,
                SubjectType = d.SDOEntityName,
                SubjectId = d.Id
            };


            return dvm;
        }

    }
}