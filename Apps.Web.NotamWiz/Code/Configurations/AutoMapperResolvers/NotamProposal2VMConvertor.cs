﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using System;
    using System.Linq;
    using System.Web;

    using AutoMapper;

    using Microsoft.AspNet.Identity;

    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Domain.Model.Entitities;
    using NavCanada.Core.Domain.Model.Enums;

    public class NotamMProposal2VMConvertor : ITypeConverter<NotamProposal, NotamProposalViewModel>
    {
        public NotamProposalViewModel Convert(ResolutionContext context)
        {
            var n = context.SourceValue as NotamProposal;
            var nvm = new NotamProposalViewModel
            {
                Id = n.Id,
                Nof = n.Nof,
                Series = n.Series,
                Number = n.Number,
                Year = n.Year,
                Type = n.Type.ToString(),
                ReferredSeries = n.ReferredSeries,
                ReferredNumber = n.ReferredNumber,
                ReferredYear = n.ReferredYear,
                Fir = n.Fir,
                Code23 = n.Code23,
                Code45 = n.Code45,
                Traffic = n.Traffic.ToString(),
                Purpose = n.Purpose.ToString(),
                Scope = n.Scope.ToString(),
                Lower = n.Lower,
                Upper = n.Upper,
                Coordinates = n.Coordinates,
                Radius = n.Radius,
                ItemA = n.ItemA,
                StartValidity = n.StartValidity,
                EndValidity = n.EndValidity,
                Estimated = n.Estimated,
                ItemD = n.ItemD,
                ItemE = n.ItemE,
                ItemEFrench = n.ItemEFrench,
                ItemF = n.ItemF,
                ItemG = n.ItemG,
                ItemX = n.ItemX,
                Operator = n.Operator,
                NoteToNof = n.NoteToNof,
                IsCatchAll = n.IsCatchAll,
                ContainFreeText = n.ContainFreeText,
                ModifiedByNof = n.ModifiedByNof,
                RejectionReason = n.RejectionReason,
                Status = n.Status,
                StatusTime = n.StatusTime,
                SubmittedNotam = Mapper.Map<Notam, NotamViewModel>(n.SubmittedNotam),
                OriginatorName =    n.OriginatorName,
                OnBehalfOfOrganizationId = n.OnBehalfOfOrganizationId,
                Tokens = n.Tokens,
                PackageId = n.PackageId,
                Permanent = n.Permanent,
                SetBeginDateAtSubmit = n.SetBeginDateAtSubmit,
                NotamProposalAttachments = n.NotamProposalAttachments == null ? null : (from a in n.NotamProposalAttachments select new NotamProposalAttachmentViewModel
                {
                    Id = a.Id,
                    OwnerId = a.OwnerId,
                    Owner = a.Owner != null ? a.Owner.UserName : "",
                    CreationDate = a.CreationDate,
                    FileName = a.FileName
                    //Attachment = a.Attachment
                }).ToList(),
                NotamProposalsInfos = n.NotamProposalsInfos == null ? null : (from a in n.NotamProposalsInfos select new NotamProposalInfoViewModel
                {
                    Id = a.Id,
                    OwnerId = a.OwnerId,
                    Owner = a.Owner != null ? a.Owner.UserName : "",
                    CreationDate = a.CreationDate,
                    Note = a.Note
                }).ToList()
            };
            return nvm;
        }
    }

    public class NotamProposalVm2MConvertor : ITypeConverter<NotamProposalViewModel, NotamProposal>
    {
        public NotamProposal Convert(ResolutionContext context)
        {
            var nv = context.SourceValue as NotamProposalViewModel;
            var np = context.DestinationValue as NotamProposal;
            var currentUserId = HttpContext.Current.User.Identity.GetUserId();
            var n = new NotamProposal
            {
                Id = nv.Id,
                Nof = nv.Nof,
                Series = nv.Series,
                Number = nv.Number,
                Year = nv.Year,
                Type = (NotamType)Enum.Parse(typeof(NotamType), nv.Type),
                ReferredSeries = nv.ReferredSeries,
                ReferredNumber = nv.ReferredNumber,
                ReferredYear = nv.ReferredYear,
                Fir = nv.Fir,
                Code23 = nv.Code23,
                Code45 = nv.Code45,
                Traffic = (TrafficType)Enum.Parse(typeof(TrafficType), nv.Traffic),
                Purpose = (PurposeType)Enum.Parse(typeof(PurposeType), nv.Purpose),
                Scope = (ScopeType)Enum.Parse(typeof(ScopeType), nv.Scope),
                Lower = nv.Lower,
                Upper = nv.Upper,
                Coordinates = nv.Coordinates,
                Radius = nv.Radius,
                ItemA = nv.ItemA,
                StartValidity = nv.StartValidity,
                EndValidity = nv.EndValidity,
                Estimated = nv.Estimated,
                ItemD = nv.ItemD,
                ItemE = nv.ItemE,
                ItemEFrench = nv.ItemEFrench,
                ItemF = nv.ItemF,
                ItemG = nv.ItemG,
                ItemX = nv.ItemX,
                Operator = nv.Operator,
                NoteToNof = nv.NoteToNof,
                IsCatchAll = nv.IsCatchAll,
                ContainFreeText = nv.ContainFreeText,
                ModifiedByNof = nv.ModifiedByNof,
                Status = nv.Status,
                RejectionReason = nv.RejectionReason,
                StatusTime = nv.StatusTime,
                SubmittedNotam = Mapper.Map<NotamViewModel, Notam>(nv.SubmittedNotam),
                OriginatorName = nv.OriginatorName,
                OnBehalfOfOrganizationId = nv.OnBehalfOfOrganizationId,
                Tokens = nv.Tokens,
                PackageId = nv.PackageId,
                Permanent = nv.Permanent,
                SetBeginDateAtSubmit = nv.SetBeginDateAtSubmit,
                NotamProposalAttachments = nv.NotamProposalAttachments != null ? (from a in nv.NotamProposalAttachments select new NotamProposalAttachment
                {
                    Id = a.Id,
                    OwnerId = string.IsNullOrEmpty(a.OwnerId) ? currentUserId : a.OwnerId,
                    CreationDate = a.CreationDate,
                    FileName = a.FileName
                    //Attachment = a.Attachment
                }).ToList() : null,
                NotamProposalsInfos = nv.NotamProposalsInfos != null ? (from a in nv.NotamProposalsInfos select new NotamProposalInfo
                {
                    Id = a.Id,
                    OwnerId = string.IsNullOrEmpty(a.OwnerId) ? currentUserId : a.OwnerId,
                    CreationDate = a.CreationDate,
                    Note = a.Note
                }).ToList() : null
            };
            return n;
        }
    }
}