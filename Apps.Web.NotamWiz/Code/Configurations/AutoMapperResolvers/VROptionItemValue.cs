﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NavCanada.SE.NOTAMWiz.App_Start.AutoMapperResolvers
{

    public class VROptionItemValue : IValueResolver
    {
        string itemName;
        public VROptionItemValue(string item)
        {
            itemName = item;
        }

        public ResolutionResult Resolve(ResolutionResult source)
        {
            return source.New(source.Context.Options.Items[itemName]);
        }
    }
}