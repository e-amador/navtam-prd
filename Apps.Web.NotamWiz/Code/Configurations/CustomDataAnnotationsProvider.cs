﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    using Westwind.Globalization;

    public class CustomDataAnnotationsProvider : DataAnnotationsModelMetadataProvider
    {        
        protected override ModelMetadata CreateMetadata(
                             IEnumerable<Attribute> attributes,
                             Type containerType,
                             Func<object> modelAccessor,
                             Type modelType,
                             string propertyName)
        {
            string key = string.Empty;
            string localizedValue = string.Empty;


            foreach (var attr in attributes)
            {
                if (attr != null)
                {
                    if (attr is DisplayAttribute)
                    {
                        key = ((DisplayAttribute)attr).Name;
                        if (!string.IsNullOrEmpty(key))
                        {
                            localizedValue = DbRes.T(key, containerType.ToString());
                            ((DisplayAttribute)attr).Name = localizedValue;
                        }
                    }
                    else if (attr is ValidationAttribute)
                    {
                        key = ((ValidationAttribute)attr).ErrorMessage;
                        if (!string.IsNullOrEmpty(key))
                        {
                            localizedValue = DbRes.T(key, containerType.ToString());
                            ((ValidationAttribute)attr).ErrorMessage = localizedValue;
                        }
                    }
                }
            }
            return base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
        }
    }
}