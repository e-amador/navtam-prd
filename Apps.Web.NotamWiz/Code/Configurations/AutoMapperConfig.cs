﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using AutoMapper;

    using Microsoft.AspNet.Identity;

    using AutoMapperResolvers;
    using Extentions;
    using GeoTools;
    using Models;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;

    public static class AutoMapperConfig
    {
        public static void RegisterMaps()
        {
            Mapper.CreateMap<Registration, OrganizationViewModel>()
            .IgnoreAllNonExisting();

            Mapper.CreateMap<Organization, OrganizationViewModel>()
                .ForMember(dest=>dest.AssignedDoa, opts=>opts.MapFrom(src=>src.AssignedDoa.Name))
                .IgnoreAllNonExisting();
            Mapper.CreateMap<OrganizationViewModel, Organization>()
                .IgnoreAllNonExisting()
                .ForMember(dest=>dest.AssignedDoa, opts=>opts.Ignore())
                .ForMember(a => a.EmailDistribution, opt => opt.MapFrom(s => (string.IsNullOrEmpty(s.EmailDistribution) ? s.EmailAddress.trimmer(" ", "\n") : s.EmailDistribution.trimmer(new[] { " ", "\n" }))));


            Mapper.CreateMap<UserProfile, UserManagementViewModel>().ConvertUsing(new UserProfile2UserManagementViewModel());
            Mapper.CreateMap<UserManagementViewModel, UserProfile>().ConvertUsing(new UserManagementViewModel2UserProfileConverter());

            Mapper.CreateMap<SeriesAllocation, SeriesAllocationViewModel>()
              .IgnoreAllNonExisting();

            Mapper.CreateMap<SeriesAllocationViewModel, SeriesAllocation>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<UserProfile, UserRegistrationManagementViewModel>()
                .ForMember(dest => dest.SelectedOrganizationName, opts => opts.MapFrom(src =>
                    src.Organization != null ? src.Organization.Name : ""
                ))
                .ForMember(dest => dest.DOAId, opts => opts.MapFrom(src =>
                    src.Organization != null ? src.Organization.AssignedDoaId : null
                ))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<GeoRegion, GeoRegionViewModel>().ConvertUsing(new GeoRegion2GeoRegionVmConvertor());
            Mapper.CreateMap<GeoRegionViewModel, GeoRegion>().ConvertUsing(new GeoRegionVm2GeoRegionConvertor());


            Mapper.CreateMap<DoaFilter, DOAFilterViewModel>();
            Mapper.CreateMap<DOAFilterViewModel, DoaFilter>().IgnoreAllNonExisting();

            Mapper.CreateMap<Doa, DOAViewModel>()
                .IgnoreAllNonExisting()
                .ForMember(a => a.DOAGeoArea, opt => opt.MapFrom(s => GeoDataService.GeoJsonMultiPolygonFromGeography(s.DoaGeoArea)));

            Mapper.CreateMap<DOAViewModel, Doa>().ConvertUsing(new DOAVM2DOAConvertor());

            Mapper.CreateMap<IcaoSubjectViewModel, IcaoSubject>().IgnoreAllNonExisting();
            Mapper.CreateMap<IcaoSubject, IcaoSubjectViewModel>().IgnoreAllNonExisting();

            Mapper.CreateMap<IcaoSubjectConditionViewModel, IcaoSubjectCondition>()
                .ForMember(a => a.I, opt => opt.MapFrom(s => s.Traffic.Contains("I")))
                .ForMember(a => a.V, opt => opt.MapFrom(s => s.Traffic.Contains("V")))
                .ForMember(a => a.N, opt => opt.MapFrom(s => s.Purpose.Contains("N")))
                .ForMember(a => a.B, opt => opt.MapFrom(s => s.Purpose.Contains("B")))
                .ForMember(a => a.O, opt => opt.MapFrom(s => s.Purpose.Contains("O")))
                .ForMember(a => a.M, opt => opt.MapFrom(s => s.Purpose.Contains("M")))
                .ForMember(a => a.Subject, opt => opt.Ignore());

            Mapper.CreateMap<IcaoSubjectCondition, IcaoSubjectConditionViewModel>()
                .ForMember(a => a.Traffic, opt => opt.MapFrom(s => string.Format("{0}{1}", s.I ? "I" : "", s.V ? "V" : "")))
                .ForMember(a => a.Purpose, opt => opt.MapFrom(s => string.Format("{0}{1}{2}{3}", s.N ? "N" : "", s.B ? "B" : "", s.O ? "O" : "", s.M ? "M" : "")));
                

            Mapper.CreateMap<SdoSubject, SdoSubjectViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<SdoSubjectViewModel, SdoSubject>().IgnoreAllNonExisting();

            Mapper.CreateMap<NotamProposalAttachmentViewModel, NotamProposalAttachment>()
                .ForMember(dest => dest.OwnerId, opts => opts.MapFrom(src => src.OwnerId ?? HttpContext.Current.User.Identity.GetUserId()))
                .ForMember(dest => dest.Owner, opts => opts.Ignore())
                .IgnoreAllNonExisting();

            Mapper.CreateMap<AerodromeDisseminationCategoryViewModel, AerodromeDisseminationCategory>().IgnoreAllNonExisting();
            Mapper.CreateMap<AerodromeDisseminationCategory, AerodromeDisseminationCategoryViewModel>().IgnoreAllNonExisting();

            Mapper.CreateMap<NotamProposalAttachment, NotamProposalAttachmentViewModel>()
                .ForMember(dest => dest.Owner, opts => opts.MapFrom(src => src.Owner != null ? src.Owner.UserName : ""))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<NotamProposalInfoViewModel, NotamProposalInfo>()
                .ForMember(dest => dest.OwnerId, opts => opts.MapFrom(src => src.OwnerId ?? HttpContext.Current.User.Identity.GetUserId()))
                .ForMember(dest => dest.Owner, opts => opts.Ignore())
                .IgnoreAllNonExisting();

            Mapper.CreateMap<NotamProposalInfo, NotamProposalInfoViewModel>()
                .ForMember(dest => dest.Owner, opts => opts.MapFrom(src => src.Owner != null ? src.Owner.UserName : ""))
                .IgnoreAllNonExisting();


            //// NOTAM Models <--> View Models
            //Mapper.CreateMap<NotamProposal, NotamProposalViewModel>().ConvertUsing(new NotamMProposal2VMConvertor());
            //Mapper.CreateMap<NotamProposalViewModel, NotamProposal>().ConvertUsing(new NotamProposalVm2MConvertor());
            Mapper.CreateMap<NotamProposal, NotamProposalViewModel>()
                .ForMember(dest => dest.Type, opts => opts.MapFrom(src => src.Type.ToString()))
                .ForMember(dest => dest.Traffic, opts => opts.MapFrom(src => src.Traffic.ToString()))
                .ForMember(dest => dest.Purpose, opts => opts.MapFrom(src => src.Purpose.ToString()))
                .ForMember(dest => dest.Scope, opts => opts.MapFrom(src => src.Scope.ToString()))
                .ForMember(dest => dest.NoteToNof, opts => opts.MapFrom(src => src.NoteToNof))
                .ForMember(dest => dest.SubmittedNotam, opts => opts.MapFrom(src => Mapper.Map<Notam, NotamViewModel>(src.SubmittedNotam)))
                .ForMember(dest => dest.NotamProposalAttachments, opts => opts.MapFrom(src => src.NotamProposalAttachments != null ? Mapper.Map<List<NotamProposalAttachment>, List<NotamProposalAttachmentViewModel>>(src.NotamProposalAttachments.ToList()) : null))
                .ForMember(dest => dest.NotamProposalsInfos, opts => opts.MapFrom(src => src.NotamProposalsInfos != null ? Mapper.Map<List<NotamProposalInfo>, List<NotamProposalInfoViewModel>>(src.NotamProposalsInfos.ToList()) : null))
                .IgnoreAllNonExisting();


            Mapper.CreateMap<NotamProposalViewModel, NotamProposal>()
                        .ForMember(dest => dest.Type, opts => opts.MapFrom(src => (NotamType)Enum.Parse(typeof(NotamType), src.Type)))
                        .ForMember(dest => dest.Traffic, opts => opts.MapFrom(src => (TrafficType)Enum.Parse(typeof(TrafficType), string.IsNullOrEmpty(src.Traffic) ? "K" : src.Traffic)))
                        .ForMember(dest => dest.Purpose, opts => opts.MapFrom(src => (PurposeType)Enum.Parse(typeof(PurposeType), src.Purpose)))
                        .ForMember(dest => dest.Scope, opts => opts.MapFrom(src => (ScopeType)Enum.Parse(typeof(ScopeType), src.Scope)))
                        .ForMember(dest => dest.NoteToNof, opts => opts.MapFrom(src => src.NoteToNof))
                        .ForMember(dest => dest.SubmittedNotam, opts => opts.MapFrom(src => Mapper.Map<NotamViewModel, Notam>(src.SubmittedNotam)))
                        .ForMember(dest => dest.NotamProposalsInfos, opts => opts.Ignore())
                        .ForMember(dest => dest.NotamProposalAttachments, opts => opts.Ignore())               
                        .IgnoreAllNonExisting();

            Mapper.CreateMap<Notam, NotamViewModel>().ConvertUsing(new Notam2VmConvertor());
            Mapper.CreateMap<NotamViewModel, Notam>().ConvertUsing(new Notamvm2MConvertor());

            Mapper.CreateMap<NotamProposalViewModel, NotamViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<SdoSubject, NotamSubjectViewModel>().ConvertUsing(new SDOSubject2NOTAMSubjectVMConvertor());

            Mapper.CreateMap<NotamPackage, NotamPackageViewModel>().ConvertUsing(new NOTAMPackage2VMConvertor());

            Mapper.CreateMap<Nsd, NsdViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<NsdViewModel, Nsd>().IgnoreAllNonExisting();
            Mapper.CreateMap<NsdVersion, NsdVersionViewModel>().IgnoreAllNonExisting();
            Mapper.CreateMap<NsdVersionViewModel, NsdVersion>().IgnoreAllNonExisting();
            Mapper.CreateMap<Organization, OrganizationListViewModel>()
                .IgnoreAllNonExisting();
            Mapper.CreateMap<OrganizationListViewModel, Organization>()
                .IgnoreAllNonExisting();


            Mapper.CreateMap<Nsd, NsdInfoViewModel>().ConvertUsing(new NSD2NSDInfoConvertor());

            Mapper.AssertConfigurationIsValid();
        }
    }
}