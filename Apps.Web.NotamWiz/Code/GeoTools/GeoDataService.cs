﻿using Microsoft.Ajax.Utilities;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Code.GeoTools
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity.Spatial;
    using System.Data.SqlTypes;
    using System.Device.Location;
    using System.Diagnostics;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;

    using Microsoft.SqlServer.Types;

    using aeroRDS;
    using Extentions;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Dtos;
    using Core.Domain.Model.Entitities;

    public static class GeoDataService
    {
        public static readonly int SRID = 4326;

        private static readonly List<DirectionAccordingToRunwayOrintation> directionAccordingToRunwayOrintations = new List<DirectionAccordingToRunwayOrintation>
        {
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 1,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 2,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 3,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 4,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 5,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 6,
                RightRCL = "SE",
                RightRCLFrench = "AU SE",
                LeftRCL = "NW",
                LeftRCLFrench = "AU NW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 7,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SE",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 8,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 9,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 10,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 11,
                RightRCL = "SOUTH",
                RightRCLFrench = "AU SUD",
                LeftRCL = "NORTH",
                LeftRCLFrench = "AU NORD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 12,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 13,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 14,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 15,
                RightRCL = "SW",
                RightRCLFrench = "AU SW",
                LeftRCL = "NE",
                LeftRCLFrench = "AU NE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 16,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 17,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 18,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 19,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 20,
                RightRCL = "WEST",
                RightRCLFrench = "A L'OUEST",
                LeftRCL = "EAST",
                LeftRCLFrench = "A L'EAST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 21,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 22,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 23,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 24,
                RightRCL = "NW",
                RightRCLFrench = "AU NW",
                LeftRCL = "SE",
                LeftRCLFrench = "AU SE"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 25,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 26,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 27,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 28,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 29,
                RightRCL = "NORTH",
                RightRCLFrench = "AU NORD",
                LeftRCL = "SOUTH",
                LeftRCLFrench = "AU SUD"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 30,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 31,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 32,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 33,
                RightRCL = "NE",
                RightRCLFrench = "AU NE",
                LeftRCL = "SW",
                LeftRCLFrench = "AU SW"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 34,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 35,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            },
            new DirectionAccordingToRunwayOrintation
            {
                RwyNum = 36,
                RightRCL = "EAST",
                RightRCLFrench = "A L'EAST",
                LeftRCL = "WEST",
                LeftRCLFrench = "A L'OUEST"
            }
        };

        private static readonly List<MinMaxDirection> directions = new List<MinMaxDirection>
        {
            new MinMaxDirection
            {
                Name = "N",
                Min = 350,
                Max = 360
            },
            new MinMaxDirection
            {
                Name = "N",
                Min = 0,
                Max = 11.99
            },
            new MinMaxDirection
            {
                Name = "NNE",
                Min = 12,
                Max = 34.99
            },
            new MinMaxDirection
            {
                Name = "NE",
                Min = 35,
                Max = 56.99
            },
            new MinMaxDirection
            {
                Name = "ENE",
                Min = 57,
                Max = 79.99
            },
            new MinMaxDirection
            {
                Name = "E",
                Min = 80,
                Max = 101.99
            },
            new MinMaxDirection
            {
                Name = "ESE",
                Min = 102,
                Max = 124.99
            },
            new MinMaxDirection
            {
                Name = "SE",
                Min = 125,
                Max = 146.99
            },
            new MinMaxDirection
            {
                Name = "SSE",
                Min = 147,
                Max = 169.99
            },
            new MinMaxDirection
            {
                Name = "S",
                Min = 170,
                Max = 191.99
            },
            new MinMaxDirection
            {
                Name = "SSW",
                Min = 192,
                Max = 214.99
            },
            new MinMaxDirection
            {
                Name = "SW",
                Min = 215,
                Max = 236.99
            },
            new MinMaxDirection
            {
                Name = "WSW",
                Min = 237,
                Max = 259.99
            },
            new MinMaxDirection
            {
                Name = "W",
                Min = 260,
                Max = 281.99
            },
            new MinMaxDirection
            {
                Name = "WNW",
                Min = 282,
                Max = 304.99
            },
            new MinMaxDirection
            {
                Name = "NW",
                Min = 305,
                Max = 326.99
            },
            new MinMaxDirection
            {
                Name = "NNW",
                Min = 327,
                Max = 349.99
            }
        };

        private static ILog log = LogManager.GetLogger(typeof(GeoDataService));

        public static string DecimalToDMS(double dec, bool isLatitude)
        {
            var minus = dec < 0;
            dec = Math.Abs(dec);
            var degree = Math.Floor(dec);
            var tmp = (dec - degree) * 60;
            var mintue = Math.Floor(tmp);
            var seconds = Math.Floor((tmp - mintue) * 60);
            if (isLatitude)
            {
                return degree.ToString("00") + mintue.ToString("00") + seconds.ToString("00") + (minus ? 'S' : 'N');
            }
            return degree.ToString("000") + mintue.ToString("00") + seconds.ToString("00") + (minus ? 'W' : 'E');
        }

        public static string DegreesToCardinalDetailed(double degrees)
        {
            string[] caridnals = {
                "N",
                "NNE",
                "NE",
                "ENE",
                "E",
                "ESE",
                "SE",
                "SSE",
                "S",
                "SSW",
                "SW",
                "WSW",
                "W",
                "WNW",
                "NW",
                "NNW",
                "N"
            };
            return caridnals[(int)Math.Round(degrees * 10 % 3600 / 225)];
        }

        public static float DMSToDecimal(string str)
        {
            if (str.Length > 8 || str.Length < 7)
            {
                return float.NaN;
            }
            var degrees = int.Parse(str.Length == 7 ? str.Substring(0, 2) : str.Substring(0, 3));
            var minutes = int.Parse(str.Length == 7 ? str.Substring(2, 2) : str.Substring(3, 2));
            var seconds = int.Parse(str.Length == 7 ? str.Substring(4, 2) : str.Substring(5, 2));
            var sufx = str.Length == 7 ? str.Substring(6, 1) : str.Substring(7, 1);
            var sing = sufx == "W" || sufx == "S" ? -1 : 1;
            var result = (float)(sing * (degrees + (minutes * 60.0 + seconds) / 3600.0));
            return result;
        }

        /// <summary>
        /// Converts degrees, minutes, seconds coordinates to rounded degrees, minutes coordinates.
        /// </summary>
        /// <param name="coordinates">The coordinates in format DDMMSS[N/S] DDDMMSS[E/W]</param>
        /// <returns>Coordinates in format DDMM[N/S] DDDMM[E/W]</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// coordinates
        /// </exception>
        public static string DMSToRoundedDM(string coordinates)
        {
            if (!IsValidDMSLocation(coordinates))
            {
                throw new ArgumentOutOfRangeException("coordinates");
            }
            var latitudeLongitude = coordinates.Split(' ');
            var latitude = latitudeLongitude[0];
            var longitude = latitudeLongitude[1];
            if (!IsValidDMS(latitude) || !IsValidDMS(longitude))
            {
                throw new ArgumentOutOfRangeException("coordinates");
            }
            var latitudeDegrees = Convert.ToDecimal(latitude.Substring(0, 2));
            var latitudeMinutes = Convert.ToDecimal(latitude.Substring(2, 2));
            var latitudeSeconds = Convert.ToDecimal(latitude.Substring(4, 2));
            var latitudeDirection = latitude.Substring(6, 1);
            var longitudeDegrees = Convert.ToDecimal(longitude.Substring(0, 3));
            var longitudeMinutes = Convert.ToDecimal(longitude.Substring(3, 2));
            var longitudeSeconds = Convert.ToDecimal(longitude.Substring(5, 2));
            var longitudeDirection = longitude.Substring(7, 1);
            if (latitudeSeconds > 29)
            {
                latitudeMinutes = latitudeMinutes + 1;
            }
            if (latitudeMinutes > 59)
            {
                latitudeMinutes = latitudeMinutes - 60;
                latitudeDegrees = latitudeDegrees + 1;
            }
            if (latitudeDegrees > 89)
            {
                latitudeDegrees = latitudeDegrees - 90;
            }
            if (longitudeSeconds > 29)
            {
                longitudeMinutes = longitudeMinutes + 1;
            }
            if (longitudeMinutes > 59)
            {
                longitudeMinutes = longitudeMinutes - 60;
                longitudeDegrees = longitudeDegrees + 1;
            }
            if (longitudeDegrees > 179)
            {
                longitudeDegrees = longitudeDegrees - 180;
            }
            return string.Format("{0:00}{1:00}{2} {3:000}{4:00}{5}", latitudeDegrees, latitudeMinutes, latitudeDirection, longitudeDegrees, longitudeMinutes, longitudeDirection);
        }

        public static ActionResult Exculde(MultiPolygonDto theRegion, MultiPolygonDto excludedRegion, string name, string descriptiuon = "")
        {
            var exGeo = GeographyFromGeoJsonMultiPolygon(excludedRegion).MakeValid().FixOrentation();

            //DbGeography region = db.TheBilingualRegion.FirstOrDefault().TheRegion;
            var newRegion = GeographyFromGeoJsonMultiPolygon(theRegion).STIntersection(exGeo.ReorientObject());

            //SqlGeography newRegion = region==null?SqlGeography.Null: SqlGeography.Parse(region.AsText());

            //newRegion = newRegion.STIntersection(exGeo.ReorientObject());
            return GetGeoJson(newRegion, name, descriptiuon);
        }

        public static double FeetToNM(double feet)
        {
            return feet / 6076.11568;
        }

        /// <summary>
        /// Build an SqlGeography Polygon from a bounding box given in discrete coordinates.
        /// </summary>
        /// <param name="nwLat">Northwest Latitude</param>
        /// <param name="nwLong">Northwest Longitude</param>
        /// <param name="seLat">Southeast Latitude</param>
        /// <param name="seLong">Southeast Longitude</param>
        /// <returns>SqlGeography Polygon</returns>
        public static SqlGeography GeograhyFromBoundingBoxNwSe(double nwLat, double nwLong, double seLat, double seLong)
        {
            return GeographyFromBoundingBox(new BoundingBoxDto
            {
                East = seLong,
                North = nwLat,
                South = seLat,
                West = nwLong
            });
        }

        /// <summary>
        /// Build an SqlGeography Polygon from a bounding box.
        /// </summary>
        /// <param name="box">Bounding Box</param>
        /// <returns>SqlGeography Polygon</returns>
        public static SqlGeography GeographyFromBoundingBox(BoundingBoxDto box)
        {
            var geob = new SqlGeographyBuilder();
            geob.SetSrid(SRID);
            geob.BeginGeography(OpenGisGeographyType.Polygon);
            geob.BeginFigure(box.North, box.West);
            geob.AddLine(box.South, box.West);
            geob.AddLine(box.South, box.East);
            geob.AddLine(box.North, box.East);
            geob.AddLine(box.North, box.West);
            geob.EndFigure();
            geob.EndGeography();
            var geog = geob.ConstructedGeography;
            Debug.WriteLine(geog.AsGml().Value);
            return geog;
        }

        /// <summary>
        /// Build an SqlGeography LineString from a GeoJson LineString.
        /// </summary>
        /// <param name="line">GeoJson LineString</param>
        /// <returns>SqlGeography LineString</returns>
        public static SqlGeography GeographyFromGeoJsonLineString(LineStringDto line)
        {
            var geob = new SqlGeographyBuilder();
            geob.SetSrid(SRID);
            geob.BeginGeography(OpenGisGeographyType.LineString);
            geob.BeginFigure(line.Coordinates[0][0], line.Coordinates[0][1]);
            foreach (var pair in line.Coordinates.Skip(1))
            {
                geob.AddLine(pair[0], pair[1]);
            }
            geob.EndFigure();
            geob.EndGeography();
            var geog = geob.ConstructedGeography;
            Debug.WriteLine(geog.AsGml().Value);
            return geog;
        }

        /// <summary>
        /// Build an SqlGeography Polygon from a GeoJson Polygon.
        /// </summary>
        /// <param name="poly">GeoJson Polygon</param>
        /// <returns>SqlGeography Polygon</returns>
        public static SqlGeography GeographyFromGeoJsonMultiPolygon(MultiPolygonDto multiPoly)
        {
            if (multiPoly == null || multiPoly.Coordinates == null || multiPoly.Coordinates.Length == 0)
            {
                return SqlGeography.Null;
            }
            var geob = new SqlGeographyBuilder();
            geob.SetSrid(SRID);
            geob.BeginGeography(OpenGisGeographyType.MultiPolygon);
            for (var i = 0; i < multiPoly.Coordinates.Length; i++)
            {
                geob.BeginGeography(OpenGisGeographyType.Polygon);
                for (var figure = 0; figure < multiPoly.Coordinates[i].Length; figure++)
                {
                    geob.BeginFigure(multiPoly.Coordinates[i][figure][0][0], multiPoly.Coordinates[i][figure][0][1]);
                    foreach (var pair in multiPoly.Coordinates[i][figure])
                    {
                        geob.AddLine(pair[0], pair[1]);
                    }
                    geob.EndFigure();
                }
                geob.EndGeography();
            }
            geob.EndGeography();
            var geog = geob.ConstructedGeography;
            return geog;
        }

        /// <summary>
        /// Build an SqlGeography Point from a GeoJson Point.
        /// </summary>
        /// <param name="point">GeoJson Point</param>
        /// <returns>SqlGeography Point</returns>
        public static SqlGeography GeographyFromGeoJsonPoint(PointDto point)
        {
            var geob = new SqlGeographyBuilder();
            geob.SetSrid(SRID);
            geob.BeginGeography(OpenGisGeographyType.Point);
            geob.BeginFigure(point.Coordinates[0], point.Coordinates[1]);
            geob.EndFigure();
            geob.EndGeography();
            var geog = geob.ConstructedGeography;
            Debug.WriteLine(geog.AsGml().Value);
            return geog;
        }

        /// <summary>
        /// Build an SqlGeography Polygon from a GeoJson Polygon.
        /// </summary>
        /// <param name="poly">GeoJson Polygon</param>
        /// <returns>SqlGeography Polygon</returns>
        public static SqlGeography GeographyFromGeoJsonPolygon(PolygonDto poly)
        {
            var geob = new SqlGeographyBuilder();
            geob.SetSrid(SRID);
            geob.BeginGeography(OpenGisGeographyType.Polygon);
            geob.BeginFigure(poly.Coordinates[0][0][0], poly.Coordinates[0][0][1]);
            foreach (var pair in poly.Coordinates[0].Skip(1))
            {
                geob.AddLine(pair[0], pair[1]);
            }
            geob.EndFigure();
            geob.EndGeography();
            var geog = geob.ConstructedGeography;
            Debug.WriteLine(geog.AsGml().Value);
            return geog;
        }

        /// <summary>
        ///  Build a GeoJson LineString from an SqlGeography LineString.
        /// </summary>
        /// <param name="geography">SqlGeography LineString</param>
        /// <returns>GeoJson LineString</returns>
        public static LineStringDto GeoJsonLineStringFromGeography(SqlGeography geography)
        {
            CheckGeographyType(geography, "LineString");
            var points = new List<double[]>();
            for (var i = 1; i <= geography.STNumPoints(); i++)
            {
                var point = new double[2];
                var sp = geography.STPointN(i);
                //we can safely round the lat/long to 5 decimal places as that's 1.11m at equator, reduces data transferred to client
                point[0] = Math.Round((double)sp.Lat, 5);
                point[1] = Math.Round((double)sp.Long, 5);
                points.Add(point);
            }
            return new LineStringDto
            {
                Coordinates = points.ToArray()
            };
        }

        /// <summary>
        ///  Build a GeoJson LineString from an SqlGeography LineString.
        /// </summary>
        /// <param name="geography">SqlGeography LineString or MultiLineString</param>
        /// <returns>GeoJson MultiLineString</returns>
        public static MultiLineStringDto GeoJsonMultiLineStringFromGeography(SqlGeography geography)
        {
            if ((string)geography.STGeometryType() == "LineString")
            {
                var points = new List<double[]>();
                for (var i = 1; i <= geography.STNumPoints(); i++)
                {
                    var point = new double[2];
                    var sp = geography.STPointN(i);
                    //we can safely round the lat/long to 5 decimal places as that's 1.11m at equator, reduces data transferred to client
                    point[0] = Math.Round((double)sp.Lat, 5);
                    point[1] = Math.Round((double)sp.Long, 5);
                    points.Add(point);
                }
                var mls = new MultiLineStringDto
                {
                    Coordinates = new double[1][][]
                };
                mls.Coordinates[0] = points.ToArray();
                return mls;
            }
            if ((string)geography.STGeometryType() == "MultiLineString")
            {
                var lscoords = new List<double[][]>();
                for (var g = 1; g <= geography.STNumGeometries(); g++)
                {
                    var geo = geography.STGeometryN(g);
                    Debug.Assert((string)geo.STGeometryType() == "LineString");
                    var points = new List<double[]>();
                    for (var i = 1; i <= geo.STNumPoints(); i++)
                    {
                        var point = new double[2];
                        var sp = geography.STPointN(i);
                        //we can safely round the lat/long to 5 decimal places as that's 1.11m at equator, reduces data transferred to client
                        point[0] = Math.Round((double)sp.Lat, 5);
                        point[1] = Math.Round((double)sp.Long, 5);
                        points.Add(point);
                    }
                    lscoords.Add(points.ToArray());
                }
                var mls = new MultiLineStringDto
                {
                    Coordinates = lscoords.ToArray()
                };
                return mls;
            }
            throw new ArgumentException("GeoJsonMultiLineStringFromGeography will only convert MultiLineString and LineString geometries.");
        }

        public static MultiPolygonDto GeoJsonMultiPolygonFromGeography(DbGeography geography)
        {
            return geography == null ? null : GeoJsonMultiPolygonFromGeography(SqlGeography.Parse(geography.AsText()));
        }

        /// <summary>
        /// Create a GeoJson MultiPolygon from an SQL Server geography type.
        /// States and Countries are MultiPolygons on the client since they can come in many pieces.
        /// In the database, the geography may be a MultiPolygon or Polygon.
        /// Due to reduction, the geography may be a GeometryCollection containing 
        /// any types. This method extracts all external polygons from a geography
        /// and constructs a GeoJsonMultiPolygon.
        /// 
        /// To keep things simple in the client, we treat a Polygon as a Multi-polygon
        /// containing a single Polygon.
        /// </summary>
        /// <remarks>
        /// Indices in a MultiPolygon are:
        /// 1. polygon 
        /// 2. ring (only first ring is used since only exterior is drawn)
        /// 3. point
        /// 4. lat, long
        /// </remarks>
        /// <param name="geography">geography</param>
        /// <returns>MultiPolygon</returns>
        public static MultiPolygonDto GeoJsonMultiPolygonFromGeography(SqlGeography geography)
        {
            geography = geography.MakeValid();
            var polycoords = new List<double[][][]>();
            if (geography.IsNull)
            {
                return new MultiPolygonDto
                {
                    Coordinates = polycoords.ToArray()
                };
            }
            var geoType = (string)geography.STGeometryType();
            if (geoType == "Polygon")
            {
                polycoords.Add(PolygonCoordinatesFromGeography(geography));
            }

            // For a MultiPolygon or GeometryCollection, the same is repeated for each contained polygon
            else if (geoType == "MultiPolygon" || geoType == "GeometryCollection")
            {
                for (var g = 1; g <= geography.STNumGeometries(); g++)
                {
                    var geo = geography.STGeometryN(g);
                    if ((string)geo.STGeometryType() == "Polygon")
                    {
                        polycoords.Add(PolygonCoordinatesFromGeography(geo));
                    }
                    else
                    {
                        Debug.WriteLine("Ignoring " + (string)geo.STGeometryType());
                    }
                }
            }
            else if (geoType == "Point")
            {
                var r = new double[1][][];
                r[0] = new double[1][];
                r[0][0] = new double[2]
                {
                    geography.Lat.Value,
                    geography.Long.Value
                };
                polycoords.Add(r);
            }
            else
            {
                throw new ArgumentException("GeoJsonMultiPolygonFromGeography will only convert GeometryCollection, MultiPolygon and Polygon geometries.");
            }

            // return a MultiPolygon containing the coordinates of the polygons
            var mpoly = new MultiPolygonDto
            {
                Coordinates = polycoords.ToArray()
            };
            return mpoly;
        }

        /// <summary>
        /// Build a GeoJson Point from an SqlGeography Point.
        /// </summary>
        /// <param name="geography"SqlGeography Point></param>
        /// <returns>GeoJson Point</returns>
        public static PointDto GeoJsonPointFromGeography(SqlGeography geography)
        {
            CheckGeographyType(geography, "Point");
            var point = new PointDto
            {
                Coordinates = new double[2]
            };
            //we can safely round the lat/long to 5 decimal places as that's 1.11m at equator, reduces data transferred to client
            point.Coordinates[0] = Math.Round((double)geography.Lat, 5);
            point.Coordinates[1] = Math.Round((double)geography.Long, 5);
            return point;
        }

        public static ActionResult GetADGeo(string mid, double radius)
        {
            if (mid == null || mid == "" || radius < 0)
            {
                throw new HttpException(400, "Bad Request");
            }
            var ahp = AeroRds.ODataCtx.Ahp.Where(a => a.Mid == mid).FirstOrDefault();
            if (ahp == null)
            {
                throw new HttpException(404, "Item Not Found");
            }
            return WriteLargeJson(GetJsonGeoFromDBGeography(ahp.GEOLocation.ToEntityDbGeography().Buffer(radius * 1852), "AD", ahp.TxtName));
        }

        public static DbGeography GetDbGeographyFromMultiPolygon(MultiPolygonDto multiPoly)
        {
            if (multiPoly == null || multiPoly.Coordinates == null || multiPoly.Coordinates.Length == 0)
            {
                return null;
            }
            var geob = new SqlGeographyBuilder();
            geob.SetSrid(SRID);
            geob.BeginGeography(OpenGisGeographyType.MultiPolygon);
            for (var i = 0; i < multiPoly.Coordinates.Length; i++)
            {
                geob.BeginGeography(OpenGisGeographyType.Polygon);
                for (var figure = 0; figure < multiPoly.Coordinates[i].Length; figure++)
                {
                    geob.BeginFigure(multiPoly.Coordinates[i][figure][0][0], multiPoly.Coordinates[i][figure][0][1]);
                    foreach (var pair in multiPoly.Coordinates[i][figure])
                    {
                        geob.AddLine(pair[0], pair[1]);
                    }
                    geob.EndFigure();
                }
                geob.EndGeography();
            }
            geob.EndGeography();
            var geog = geob.ConstructedGeography;
            return DbGeography.FromText(geog.ToString(), SRID).MakeValid();
        }

        public static string GetDirection(double d)
        {
            d = Math.Round((d + 360) % 360, 2);
            return directions.Where(a => a.Min <= d && a.Max >= d).First().Name;
        }

        public static ActionResult GetFIRGeo(string fir)
        {
            var ase = AeroRds.ODataCtx.Ase.Where(a => a.CodeType == "FIR" && a.CodeId == fir).FirstOrDefault();
            if (ase == null)
            {
                throw new HttpException(404, "Item Not Found");
            }
            var firInfo = AeroRds.ODataCtx.GeoInstanceCache.Where(a => a.EntityType == "Ase" && a.Mid == ase.Mid).Select(f => new
            {
                Geo = f.GeoInstance.ToEntityDbGeography(),
                Name = ase.TxtName
            }).FirstOrDefault();
            var data = new MapDataDto();
            var feature = new FeatureDto
            {
                Geometry = GeoJsonMultiPolygonFromGeography(firInfo.Geo),
                Properties = new Dictionary<string, string>
                {
                    { "NAME", firInfo.Name }
                }
            };
            data.Name = "FIR";
            data.Features.Add(feature);
            return WriteLargeJson(data);
        }

        public static Dictionary<string, DbGeography> GetFIRsGeoInfo()
        {
            var firsInfo = new Dictionary<string, DbGeography>();
            var firs = AeroRds.ODataCtx.Ase.Where(a => a.CodeType == "FIR");
            foreach (var fir in firs)
            {
                var mid = fir.Mid;
                var firGeo = AeroRds.ODataCtx.GeoInstanceCache.Where(a => a.EntityType == "Ase" && a.Mid == mid).FirstOrDefault();
                if (firGeo != null)
                {
                    firsInfo[fir.CodeId] = firGeo.GeoInstance.ToEntityDbGeography();
                }
            }
            return firsInfo;
        }

        public static ActionResult GetGeoJson(SqlGeography geo, string name = "TheRegion", string desc = "")
        {
            var data = new MapDataDto();
            geo = geo.MakeValid();
            var feature = new FeatureDto
            {
                Geometry = GeoJsonMultiPolygonFromGeography(geo),
                Properties = new Dictionary<string, string>
                {
                    { "NAME", desc }
                }
            };
            data.Name = name;
            data.Features.Add(feature);
            return WriteLargeJson(data);
        }

        public static MapDataDto GetJsonGeoFromDBGeography(DbGeography dbGeo, string type, string name)
        {
            var data = new MapDataDto();
            var feature = new FeatureDto
            {
                Geometry = GeoJsonMultiPolygonFromGeography(dbGeo),
                Properties = new Dictionary<string, string>
                {
                    { "NAME", name }
                }
            };
            data.Name = type;
            data.Features.Add(feature);
            return data;
        }

        public static ActionResult GetLocationGeo(double lat, double lon, double radius)
        {
            var geo = SqlGeography.STPointFromText(new SqlChars("POINT(" + lon + " " + lat + ")"), SRID).STBuffer(radius * 1852);
            return GetGeoJson(geo, "LOCATION", "Location");
        }

        public static ActionResult Include(MultiPolygonDto theRegion, MultiPolygonDto includedRegion, string name, string description = "")
        {
            var addedRegion = GeographyFromGeoJsonMultiPolygon(includedRegion).MakeValid().FixOrentation();
            if (theRegion == null || theRegion.Coordinates == null || theRegion.Coordinates.Length == 0)
            {
                return GetGeoJson(addedRegion, name, description);
            }

            //DbGeography region = db.TheBilingualRegion.FirstOrDefault().TheRegion;
            var region = GeographyFromGeoJsonMultiPolygon(theRegion);
            //SqlGeography newRegion = region==null? addedRegion : SqlGeography.Parse(region.AsText()).STUnion(addedRegion);
            var newRegion = region.STUnion(addedRegion);
            return GetGeoJson(newRegion, name, description);
        }

        public static bool IsInBilingualRegion(double lat, double lon, IUow db)
        {
            return IsInBilingualRegion(DbGeography.PointFromText("Point(" + lon + " " + lat + ")", SRID), db);
        }

        public static bool IsInBilingualRegion(DbGeography geoLocation, IUow db)
        {
            if (geoLocation == null)
            {
                return false;
            }
            var region = db.BilingualRegionRepo.GetAll().FirstOrDefault();
            if (region == null || region.TheRegion == null || region.TheRegion.IsEmpty)
            {
                return false;
            }
            return region.TheRegion.ToSqlGeography().STIntersects(geoLocation.ToSqlGeography()).IsTrue;
        }

        public static bool IsLocationWithinDOA(DbGeography loc, Doa doa)
        {
            var location = DbGeography.PointFromText("POINT(" + loc.Longitude + " " + loc.Latitude + ")", SRID);
            return location.Intersects(doa.DoaGeoArea);
        }

        public static bool IsLocationWithinDOA(GeoCoordinate loc, Doa doa)
        {
            var location = DbGeography.PointFromText("POINT(" + loc.Longitude + " " + loc.Latitude + ")", SRID);
            return location.Intersects(doa.DoaGeoArea);
        }

        public static bool IsLocationWithinDOA(double lat, double lon, Doa doa)
        {
            var location = DbGeography.PointFromText("POINT(" + lon + " " + lat + ")", SRID);
            return location.Intersects(doa.DoaGeoArea);
        }

        public static bool IsValidDecimalLocation(string str)
        {
            if (str.IsNullOrWhiteSpace())
                return false;

            var parts = str.Split(' ');
            var regex = new Regex(@"^-?\d+\.\d*$");
            return regex.Match(parts[0]).Success && regex.Match(parts[1]).Success && IsValidDMS(DecimalToDMS(double.Parse(parts[0]), true)) && IsValidDMS(DecimalToDMS(double.Parse(parts[1]), false));
        }

        public static bool IsValidDMS(string str)
        {
            if (str.Length > 8 || str.Length < 7)
            {
                return false;
            }
            var regex = new Regex(@"((0[0-9]{2}|1[0-7][0-9])([0-5][0-9][0-5][0-9])([WE]))|([0-7][0-9][0-5][0-9][0-5][0-9][NS])");
            return regex.Match(str).Success;
        }

        public static bool IsValidDMSLocation(string str)
        {
            if (str.IsNullOrWhiteSpace())
                return false;

            var parts = str.Split(' ');
            return parts.Length == 2 && parts[0].Length == 7 && parts[1].Length == 8 && IsValidDMS(parts[0]) && IsValidDMS(parts[1]);
        }

        /// <summary>
        /// Validate string to be valid DMS or Decimal geo coordinate location
        /// </summary>
        /// <param name="location">DMS or decimal space separated "lat lon" string</param>
        /// <returns>true if valid</returns>
        public static bool IsValidLocation(string location)
        {
            return  IsValidDMSLocation(location) || IsValidDecimalLocation(location);
        }

        public static double MetersToNM(double met)
        {
            return met * 0.000539957;
        }

        public static double MeterToFeet(double m)
        {
            return m * 3.28084;
        }

        public static double NMToFeet(double nm)
        {
            return nm * 6076.11568;
        }

        public static double NMToMeters(double nm)
        {
            return nm / 0.000539957;
        }

        public static DbGeography ReversePolygon(SqlGeography polygon)
        {
            var invertedSqlGeography = polygon.ReorientObject();
            return DbSpatialServices.Default.GeographyFromProviderValue(invertedSqlGeography);

            //if (sqlGeography.STArea() > invertedSqlGeography.STArea())
            //{
            //  sqlGeography = invertedSqlGeography;
            //}

            //return DbSpatialServices.Default.GeographyFromProviderValue(sqlGeography);
        }

        public static string RunwayDirectionToCardinal(int runwayDirection, bool right, bool french)
        {
            var info = directionAccordingToRunwayOrintations.Single(r => r.RwyNum == runwayDirection);
            return french ? (right ? info.RightRCLFrench : info.LeftRCLFrench) : (right ? info.RightRCL : info.LeftRCL);
        }

        private static void CheckGeographyType(SqlGeography geography, params string[] permitted)
        {
            var stype = (string)geography.STGeometryType();
            if (!permitted.Contains(stype))
            {
                throw new ArgumentException("This conversion cannot handle " + stype + ", it can only handle the following types: " + string.Join(",", permitted));
            }
        }

        private static double[][][] PolygonCoordinatesFromGeography(SqlGeography polygon)
        {
            Debug.Assert((string)polygon.STGeometryType() == "Polygon");
            var ringsCount = (int)polygon.NumRings();
            var coordinates = new double[ringsCount][][];
            for (var r = 1; r <= ringsCount; r++)
            {
                // convert only the first (exterior) ring
                var ring = polygon.RingN(r);
                var points = new List<double[]>();
                for (var i = 1; i <= ring.STNumPoints(); i++)
                {
                    var point = new double[2];
                    var sp = ring.STPointN(i);
                    //we can safely round the lat/long to 5 decimal places as that's 1.11m at equator, reduces data transferred to client
                    point[0] = Math.Round((double)sp.Lat, 5);
                    point[1] = Math.Round((double)sp.Long, 5);
                    points.Add(point);
                }
                coordinates[r - 1] = points.ToArray();
            }
            return coordinates;
        }

        /// <summary>
        /// Method used to write out MapDataModel JSON setting max length property from settings files
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static LargeJsonResult WriteLargeJson(MapDataDto data)
        {
            var maxjsonlength = 0;

            //get maxjsonlength from web.config
            int.TryParse(ConfigurationManager.AppSettings["maxjsonlength"], out maxjsonlength);
            return new LargeJsonResult
            {
                Data = data,
                MaxJsonLength = maxjsonlength
            };
        }

        internal class DirectionAccordingToRunwayOrintation
        {
            public string LeftRCL
            {
                get;
                set;
            }

            public string LeftRCLFrench
            {
                get;
                set;
            }

            public string RightRCL
            {
                get;
                set;
            }

            public string RightRCLFrench
            {
                get;
                set;
            }

            public int RwyNum
            {
                get;
                set;
            }
        }

        internal class MinMaxDirection
        {
            public double Max
            {
                get;
                set;
            }

            public double Min
            {
                get;
                set;
            }

            public string Name
            {
                get;
                set;
            }
        }
    }
}