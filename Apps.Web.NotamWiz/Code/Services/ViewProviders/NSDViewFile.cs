﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Services.ViewProviders
{
    using System.IO;
    using System.Web.Hosting;

    public class NSDViewFile : System.Web.Hosting.VirtualFile
    {

        public NSDViewFile(string path)
            : base(path)
        {

        }

        public override System.IO.Stream Open()
        {

            string viewPAth = HostingEnvironment.MapPath(this.VirtualPath);

            if(File.Exists(viewPAth))
            {
                return File.OpenRead(viewPAth);
            }

            return null;

          
        }
    }
}