﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Services.ViewProviders
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Security.Cryptography;
    using System.Web.Caching;
    using System.Web.Hosting;

    public class NSDViewProvider : VirtualPathProvider
    {
        public NSDViewProvider()
            : base()
        {

        }


        private bool IsNSDViewPath(string virtualPath)
        {
            if (!virtualPath.Contains("/Nsds/") || !virtualPath.EndsWith(".cshtml"))
                return false;

            return true;

        }

        public override bool FileExists(string virtualPath)
        {
            if(IsNSDViewPath(virtualPath))
            {
                string viewPAth = HostingEnvironment.MapPath(virtualPath);
                bool exists=  File.Exists(viewPAth);
                return exists;
            }
            else
            {
                return base.FileExists(virtualPath);
            }
        }

        // Simply return the virtualPath on every request.
        public override string GetFileHash(string virtualPath, System.Collections.IEnumerable virtualPathDependencies)
        {           

            if (IsNSDViewPath(virtualPath))
            {
                using (var md5 = MD5.Create())
                {
                    string viewPAth = HostingEnvironment.MapPath(virtualPath);
                    using (var stream = File.OpenRead(viewPAth))
                    {
                        string hash = System.Text.Encoding.Default.GetString(md5.ComputeHash(stream));
                        return hash;
                    }
                }
            }
            return base.GetFileHash(virtualPath, virtualPathDependencies);
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            if (IsNSDViewPath(virtualPath))
            {
                return null;
            }
            else
            {
                return base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
            }
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            if (IsNSDViewPath(virtualPath))
                return new NSDViewFile(virtualPath);
            else
                return base.GetFile(virtualPath);
        }
    }
}