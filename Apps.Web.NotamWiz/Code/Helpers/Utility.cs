﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NavCanada.Applications.NotamWiz.Web.Code.IdentityManagers;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Mto;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using log4net;

namespace NavCanada.Applications.NotamWiz.Web.Code.Helpers
{
    public interface IUtility
    {
        IUow Uow { get; }
        IClusterQueues ClusterQueues { get; }
        AppUserManager UserManager { get; }
        AppRoleManager RoleManager { get; }
        Controller Controller { get; set; }
        Task<bool> IsUserInRoleAsync(string userId, string roleName);
        Task<Organization> GetUserOrganizationAsync();
        Task<UserProfile> GetUser();
        Task<string> GetConfigurationValue(string category, string name, string defaultValue);
        Task<Doa> GetUserDoaAsync();
        Task SetViewBagRolesForCssAndAdminAsync(string userId);
        Task<int> GetUserPreferenceExpiredRecordsFilter();
        
        void PublishMtoToTargetQueue(MessageTransferObject mto, SqlServiceBrokerQueueSettings targetQueueName);
        
        MessageTransferObject CreateMtoForUserRegistration(MessageTransferObjectId messageId, 
            string userId, long registrationId, RegistrationStatus regStatus, string tokenConfirmation);
        MessageTransferObject CreateMtoForNotamChangeStatus(MessageTransferObjectId messageId, long notamId);

        MessageTransferObject CreateMtoForUserForgotPassword(MessageTransferObjectId messageId, string userId,
            string tokenConfirmation);
    }

    public class Utility : IUtility
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(Utility));
        public IUow Uow { get; private set; }
        public IClusterQueues ClusterQueues { get; private set; }
        private AppUserManager _userManager;
        private AppRoleManager _roleManager;
        public AppUserManager UserManager
        {
            get { return this._userManager ?? Controller.HttpContext.GetOwinContext().GetUserManager<AppUserManager>(); }
            private set { this._userManager = value; }
        }

        public AppRoleManager RoleManager
        {
            get { return this._roleManager ?? Controller.HttpContext.GetOwinContext().Get<AppRoleManager>(); }
            private set { this._roleManager = value; }
        }

        public Controller Controller { get; set; }

        public Utility(Controller controller,  IUow uow, AppUserManager userManager, AppRoleManager roleManager)
        {
            Controller = controller;
            Uow = uow;
            UserManager = userManager;
            RoleManager = roleManager;
            
        }

        public async Task<bool> IsUserInRoleAsync(string userId, string roleName)
        {
            var role = await RoleManager.FindByNameAsync(roleName);
            var currentUser = await UserManager.FindByIdAsync(userId);

            if (role == null)
                return false;

            return currentUser.Roles.Any(r => r.RoleId == role.Id);
        }

        public async Task<Organization> GetUserOrganizationAsync()
        {
            var currentUser = await UserManager.FindByIdAsync(Controller.User.Identity.GetUserId());
            if (currentUser.OrganizationId != null)
            {
                var orgId = (long)currentUser.OrganizationId;
                return Uow.OrganizationRepo.GetById(orgId);
            }

            return null;
        }

        public async Task<UserProfile> GetUser()
        {
            return await UserManager.FindByIdAsync(Controller.User.Identity.GetUserId());
        }

        public async Task<Doa> GetUserDoaAsync()
        {
            Organization org = await GetUserOrganizationAsync();
            if (org.AssignedDoaId == null)
            {
                return null;
            }

            return Uow.DoaRepo.GetById(org.AssignedDoaId);
        }

        public async Task SetViewBagRolesForCssAndAdminAsync(string userId)
        {
            Controller.ViewBag.CCSRole = await IsUserInRoleAsync(userId, CommonDefinitions.CcsRole);
            Controller.ViewBag.AdminRole = await IsUserInRoleAsync(userId, CommonDefinitions.AdminRole);
            Controller.ViewBag.OrgAdminRole = await IsUserInRoleAsync(userId, CommonDefinitions.OrgAdminRole);
        }
        public async Task<int> GetUserPreferenceExpiredRecordsFilter()
        {
            var currentUser = await UserManager.FindByIdAsync(Controller.User.Identity.GetUserId());

            if (currentUser.UserPreferenceId != null)
            {
               return Uow.UserPreferenceRepo.GetById(currentUser.UserPreferenceId).ExpiredRecordsDateFilter;
            }

            return (Convert.ToInt16(ConfigurationManager.AppSettings.Get("ExpiredRecordsFilter")));

        }

        public void PublishMtoToTargetQueue(MessageTransferObject mto, SqlServiceBrokerQueueSettings targetQueueName)
        {
            try
            {
                var targetQueue = new SqlServiceBrokerQueue(Uow.DbContext, targetQueueName);
                targetQueue.Publish(mto);
            }
            catch (Exception ex)
            {
                var message = string.Format("Error publishing Mto to targetQueue [{0}]. Details: ", targetQueueName.ReceiveQueueName);
                _logger.Error(message, ex);
            }
        }

        public MessageTransferObject CreateMtoForUserRegistration(MessageTransferObjectId messageId, string userId, long registrationId, RegistrationStatus regStatus, string tokenConfirmation)
        {
            var mto = new MessageTransferObject();

            //add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            //registration status
            mto.Add(MessageDefs.RegistrationStatusKey, (int)regStatus, DataType.Int32);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)messageId, DataType.Int32);

            //add unique identifier
            if (regStatus == RegistrationStatus.Completed)
            {
                mto.Add(MessageDefs.EntityIdentifierKey, userId, DataType.String);
            }
            else
            {
                mto.Add(MessageDefs.EntityIdentifierKey, registrationId, DataType.String);
            }

            //Time when message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            //Email Token Confirmation
            if (regStatus == RegistrationStatus.ConfirmEmail && !string.IsNullOrEmpty(tokenConfirmation))
                mto.Add(MessageDefs.EmailTokenConfirmationKey, tokenConfirmation, DataType.String);

            return mto;
        }

        public MessageTransferObject CreateMtoForNotamChangeStatus(MessageTransferObjectId messageId, long notamId)
        {
            var mto = new MessageTransferObject();

            //add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)messageId, DataType.Int32);

            //add unique identifier
            mto.Add(MessageDefs.EntityIdentifierKey, notamId, DataType.Int64);

            var time = DateTime.UtcNow;

            //Time when message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            return mto;
        }

        public MessageTransferObject CreateMtoForUserForgotPassword(MessageTransferObjectId messageId, string userId, string tokenConfirmation)
        {
            var mto = new MessageTransferObject();

            //add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            //registration status
            mto.Add(MessageDefs.RegistrationStatusKey, RegistrationStatus.ForgotPassword, DataType.Int32);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)messageId, DataType.Int32);

            //add unique identifier
            mto.Add(MessageDefs.EntityIdentifierKey, userId, DataType.String);

            //Time when message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            mto.Add(MessageDefs.EmailTokenConfirmationKey, tokenConfirmation, DataType.String);

            return mto;
        }

        public async Task<string> GetConfigurationValue(string category, string name, string defaultValue)
        {
            var result = defaultValue;
            var cv = await Uow.ConfigurationValueRepo.GetByCategoryandNameAsync(category, name);
            if (cv != null)
            {
                result = cv.Value;
            }
            return result;
        }
    }
}