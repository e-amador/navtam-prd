﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Westwind.Globalization;

namespace NavCanada.Applications.NotamWiz.Web.Code.Helpers
{
    public interface IDbResWrapper
    {
        string T(string resId, string resourceSet = null, string lang = null);
    }

    public class DbResWrapper : IDbResWrapper
    {
        public virtual string T(string resId, string resourceSet = null, string lang = null)
        {
            return DbRes.T(resId, resourceSet, lang);
        }
    }
}