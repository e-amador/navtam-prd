﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Extentions
{
    using System;
    using System.Device.Location;

    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Core.Domain.Model.Dtos;

    public static class GeoCoordinateExt
    {

        public static double BearingTo(this GeoCoordinate from, GeoCoordinate to)
        {
            var d1 = ToRadians(from.Latitude);
            var d2 = ToRadians(to.Latitude);
            var dd = ToRadians(to.Longitude - from.Longitude);

            // see http://mathforum.org/library/drmath/view/55417.html
            var y = Math.Sin(dd) * Math.Cos(d2);
            var x = Math.Cos(d1) * Math.Sin(d2) -
                    Math.Sin(d1) * Math.Cos(d2) * Math.Cos(dd);
            var r = Math.Atan2(y, x);

            return (ToDegrees(r) + 360) % 360;
        }

        public static double ToRadians(double v)
        {
            return v * Math.PI / 180;
        }


        public static double ToDegrees(double v)
        {
            return v * 180 / Math.PI;
        }

  
        /// <summary>
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="brng"></param>
        /// <param name="dist"> in meters</param>
        /// <returns></returns>
        public static GeoCoordinate DestinationPoint(this GeoCoordinate from, double brng, double dist)
        {
            var θ =  ToRadians(brng);
            var δ = dist / 6371 / 1000; // angular distance in radians

            var φ1 = ToRadians(from.Latitude);
            var λ1 = ToRadians(from.Longitude);

            var φ2 = Math.Asin(Math.Sin(φ1) * Math.Cos(δ) +
                                Math.Cos(φ1) * Math.Sin(δ) * Math.Cos(θ));
            var λ2 = λ1 + Math.Atan2(Math.Sin(θ) * Math.Sin(δ) * Math.Cos(φ1),
                                     Math.Cos(δ) - Math.Sin(φ1) * Math.Sin(φ2));
            λ2 = (λ2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI; // normalise to -180..+180º

            return new GeoCoordinate( ToDegrees(φ2), ToDegrees(λ2));
        }

        /// <summary>
        /// Convert DMS or Decimal location to GeoCoordinate
        /// </summary>
        /// <param name="geo"></param>
        /// <param name="latlon"></param>
        /// <returns></returns>
        public static GeoCoordinate FromLocation(this GeoCoordinate geo, string location)
        {
            if (GeoDataService.IsValidDMSLocation(location))
                return geo.FromDMS(location);
            else if (GeoDataService.IsValidDecimalLocation(location))
            {
                string[] loc = location.Split(' ');
                geo.Latitude = double.Parse(loc[0]);
                geo.Longitude = double.Parse(loc[1]);
                
                return geo;
            }
            return null;
        }


        private static string GetDMS(double dec, bool isLat)
        {
            bool minus = dec < 0;
            dec = Math.Abs(dec);
            double degrees = Math.Floor(dec);
            double seconds = (dec - degrees) * 3600;
            double minutes = Math.Floor(seconds / 60);
            seconds = Math.Floor(seconds - (minutes * 60));
            return isLat ? ((int)degrees).ToString("D2") + ((int)minutes).ToString("D2") + ((int)seconds).ToString("D2") + (minus ? 'S' : 'N') : ((int)degrees).ToString("D3") + ((int)minutes).ToString("D2") + ((int)seconds).ToString("D2") + (minus ? 'W' : 'E');

        }

        public static string ToDMSLocation(this GeoCoordinate geo)
        {           
            return GetDMS(geo.Latitude, true)+" "+GetDMS(geo.Longitude, false);
        }

        public static GeoCoordinate FromDMS(this GeoCoordinate geo, string latlon)
        {
            string[] loc = latlon.Split(' ');
            return geo.FromDMS(loc[0], loc[1]);
        }

        public static GeoCoordinate FromDMS(this GeoCoordinate geo, string lat, string lon)
        {
            DmsPointDto point = new DmsPointDto(lat, lon);
            geo.Latitude = point.Latitude;
            geo.Longitude = point.Longitude;
            return geo;
        }

        public static double DistanceToLine(this GeoCoordinate from, GeoLineDto line)
        {
            var a = line.X.GetDistanceTo(line.Y);
            var b = line.X.GetDistanceTo(from);
            var c = line.Y.GetDistanceTo(from);


            var s = (a + b + c) / 2;

            var area = Math.Sqrt(s * (s - a) * (s - b) * (s - c));


            var h = area / (a * 0.5);

            return h;
        }

        /// <summary>
        /// Calculate angle a b c
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static double Angle(this GeoCoordinate b, GeoCoordinate a, GeoCoordinate c)
        {
            var p12 = b.GetDistanceTo(a);
            var p13 = b.GetDistanceTo(c);
            var p23 = a.GetDistanceTo(c);

            return ToDegrees(Math.Acos((Math.Pow(p12, 2) + Math.Pow(p13, 2) - Math.Pow(p23, 2)) / (2 * p12 * p13)));
        }
        
    }
}