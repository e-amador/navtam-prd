﻿using ILog = log4net.ILog;

namespace NavCanada.Applications.NotamWiz.Web.Code.Extentions
{
    using System;

    public static class Log4NetExt
    {
        public static string ErrorWithUID(this ILog logger, Exception e, string message="")
        {
            string uid = " {"+Guid.NewGuid().ToString()+"} ";
            logger.Error(message??""+ uid, e);
            return uid;

        }


        public enum LogLevel{
            INFO,
            DEBUG,
            WARNING,
            ERROR 
        }

        public static void Object(this ILog logger, object o, LogLevel level = LogLevel.INFO, Exception ex = null)
        {
            object details = o;
            //try
            //{
            //    details = JsonConvert.SerializeObject(o, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            //}
            //catch
            //{

            //}
            switch(level)
            {
                case LogLevel.INFO:
                    logger.Info(details, ex);
                    break;
                case LogLevel.DEBUG:
                    logger.Debug(details, ex);
                    break;
                case LogLevel.WARNING:
                    logger.Warn(details, ex);
                    break;    
                case LogLevel.ERROR:
                    logger.Error(details, ex);
                    break;    

            }


        }
     
    }
}