﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize]
    public class UserProfilesController : BaseODataController
    {
        public UserProfilesController(IUow uow)
            : base(uow)
        {
        }

        // GET: odata/UserProfile
        [EnableQuery]
        public IQueryable<UserProfile> GetUserProfiles()
        {
            return Uow.UserProfileRepo.GetAll();
        }

        // GET: odata/UserProfile(5)
        [EnableQuery]
        public UserProfile GetUserProfile([FromODataUri] long key)
        {
            return Uow.UserProfileRepo.GetById(key);
        }

    }
}