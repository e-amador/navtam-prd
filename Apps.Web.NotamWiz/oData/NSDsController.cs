﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;

    [Authorize(Roles = "User")]
    public class NsdsController : BaseODataController
    {
        public NsdsController(IUow uow)
            : base(uow)
        {
        }


        // GET: odata/NSDs
        [EnableQuery]
        public IQueryable<Nsd> GetNSDs()
        {
            return  Uow.NsdRepo.GetAll();
        }

        // GET: odata/NSDs(5)
        [EnableQuery]
        public SingleResult<Nsd> GetNSD([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.NsdRepo.GetAll().Where(nSD => nSD.Id == key));
        }
      
        [EnableQuery]
        public IQueryable<Organization> GetLimitedToOrganizatations([FromODataUri] long key)
        {
            return Uow.NsdRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.LimitedToOrganizatations);
        }

        // GET: odata/NSDs(5)/Versions
        [EnableQuery]
        public IQueryable<NsdVersion> GetVersions([FromODataUri] long key)
        {
            return Uow.NsdRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.Versions);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Uow.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NSDExists(long key)
        {
            return Uow.NsdRepo.GetAll().Any(e => e.Id == key);
        }
    }
}
