﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize(Roles = "User")]
    public class IcaoSubjectsController : BaseODataController
    {
        public IcaoSubjectsController(IUow uow)
            : base(uow)
        {
        }


        // GET: odata/ICAO_Subject
        [EnableQuery]
        public IQueryable<IcaoSubject> GetIcaoSubjects()
        {
            return Uow.IcaoSubjectRepo.GetAll();
        }

        // GET: odata/ICAO_Subject(5)
        [EnableQuery]
        public SingleResult<IcaoSubject> GetIcaoSubject([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.IcaoSubjectRepo.GetAll().Where(c => c.Id == key));
        }
  

        // GET: odata/ICAO_Subject(5)/Conditions
        [EnableQuery]
        public IQueryable<IcaoSubjectCondition> GetConditions([FromODataUri] long key)
        {
            return Uow.IcaoSubjectConditionRepo.GetAll().Where(e => e.SubjectId == key);
        }

        private bool IcaoSubjectExists(long key)
        {
            return Uow.IcaoSubjectRepo.GetAll().Any(e => e.Id == key) ;
        }
    }
}
