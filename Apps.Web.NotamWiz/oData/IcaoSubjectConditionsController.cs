﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize(Roles = "User")]
    public class IcaoSubjectConditionsController : BaseODataController
    {
        public IcaoSubjectConditionsController(IUow uow)
            : base(uow)
        {
        }

        // GET: odata/ICAO_SubjectCondition
        [EnableQuery]
        public IQueryable<IcaoSubjectCondition> GetIcaoSubjectConditions()
        {
            return Uow.IcaoSubjectConditionRepo.GetAll();
        }

        // GET: odata/ICAO_SubjectCondition(5)
        [EnableQuery]
        public SingleResult<IcaoSubjectCondition> GetIcaoSubjectConditions([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.IcaoSubjectConditionRepo.GetAll().Where(c => c.Id == key));
        }

   

        // GET: odata/ICAO_SubjectCondition(5)/Subject
        [EnableQuery]
        public SingleResult<IcaoSubject> GetSubject([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.IcaoSubjectConditionRepo.GetAll().Where(m => m.Id == key).Select(m => m.Subject));
        }

        private bool IcaoSubjectConditionsExists(long key)
        {
            return Uow.IcaoSubjectConditionRepo.GetAll().Any(e => e.Id == key);
        }
    }
}
