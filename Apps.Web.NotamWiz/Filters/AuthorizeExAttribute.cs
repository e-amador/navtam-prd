﻿namespace NavCanada.Applications.NotamWiz.Web.Filters
{
    using System;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AuthorizeExAttribute : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            IPrincipal user = httpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
/*
                var user  .FindByName(userName);
                if (user != null && user.DigitalSlaId.HasValue)
                {
                    //var digitalSla = Uow.DigitalSlaRepo.GetLastVersion();
                    //if (user.DigitalSlaId.Value == digitalSla.Id)
                    return; // The user already accept the latest SLA. 
                }
*/

                return false;
            }

/*
            if ((RoleList.Length > 0) && !RoleList.Select(p=>p.ToString()).Any<string>(new Func<string, bool>(user.IsInRole)))
            {
                return false;
            }
*/
            return base.AuthorizeCore(httpContext);
        }

    }
}