﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;

    /// <summary>
    /// 
    /// </summary>
    public class TraceFilter : ActionFilterAttribute, IExceptionFilter
    {
        private readonly ILog log = LogManager.GetLogger("TraceActivity");
        private DateTime StartExecutionTime { get; set; } 

        private string Parameter { get; set; }
        private ActionDescriptor CurrentAction { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            StartExecutionTime = DateTime.UtcNow;
            this.log.Object(new { User = HttpContext.Current.User.Identity, StartExecuting = new { Controller = filterContext.Controller.GetType().Name, Action = filterContext.ActionDescriptor.ActionName, filterContext.ActionParameters } });
            
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if(filterContext.Exception == null)
            {
                TimeSpan executionTime = DateTime.UtcNow - StartExecutionTime;
                this.log.Object(new { User = HttpContext.Current.User.Identity, Executed = new {Controller = filterContext.Controller.GetType().Name, Action = filterContext.ActionDescriptor.ActionName}, ExecutionTime = executionTime.Seconds.ToString() });
            }

            base.OnActionExecuted(filterContext);
        }



        public void OnException(ExceptionContext filterContext)
        {
            if(!filterContext.ExceptionHandled)
            {
                Exception exception = filterContext.Exception;
                this.log.Error(new { User = HttpContext.Current.User.Identity, ErrorExecuting = new {Context = filterContext}}, exception);
                filterContext.Result = new RedirectResult("/ErrorView");
                filterContext.ExceptionHandled = true;
            }
        }

        private string DictionaryToJson(IDictionary<string, object> dict)
        {
            var entries = dict.Select(d =>
                string.Format("\"{0}\": [{1}]", d.Key, string.Join(",", d.Value.ToString())));
            return "{" + string.Join(",", entries) + "}";
        }
    }
}