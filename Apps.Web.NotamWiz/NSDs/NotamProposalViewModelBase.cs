﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs
{
    using System;

    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class NotamProposalViewModelBase
    {
        public string SdoEntityId{get;set;}
        public bool Bilingual { get; set; }
        public DateTime? StartValidity { get; set; }
        public DateTime? EndValidity { get; set; }
        public bool Estimated { get; set; }
        public bool Permanent { get; set; }
        public bool SetBeginDateAtSubmit { get; set; }
        public string ItemD { get; set; } //schedule
        public string NoteToNof { get; set; }
        public bool ItemDChangedByNof { get; set; }
        public string OriginatorName { get; set; }
        public List<clsTimeSch> TimeScheduleData { get; set; }
        public int VSP { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }

    public class clsTimeSch
    {
        public string RadioButtonSelectionWCT { get; set; }
        public string DateId { get; set; }
        public string EndDateId { get; set; }
        public string TimeId { get; set; }
        public string EndTimeId { get; set; }
        public string ContinuesEvent { get; set; }
        public string IncludeorExclude { get; set; }
        public string StartDateEndDate { get; set; }
        public string StartTimeEndTime { get; set; }
     }
}