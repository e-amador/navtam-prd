﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs
{
    using System.Linq;
    using System.Text.RegularExpressions;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using System;
    using Utils.Libs.SchedulerParser;
    using FParsec;
    using System.Collections.Generic;
    using Microsoft.FSharp.Core;

    public static class CommonValidators
    {
        public static bool IsValidFreeText(string notamFreeText, IUow uow)
        {
            notamFreeText = notamFreeText.ToUpper();

            var setting = uow.ConfigurationValueRepo.GetAll().Where(c => c.Category == "NOTAM").Select(c => new { c.Name, c.Value });
            string allowedSymbols = setting.Where(s => s.Name == "NOTAMAllowedSymbols").Select(s => s.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(allowedSymbols))
            {
                Regex regex = new Regex(allowedSymbols);
                Match match = regex.Match(notamFreeText);
                if (!match.Success)
                {
                    return false;
                }
            }

            string forbiddenWords = setting.Where(s => s.Name == "NOTAMForbiddenWords").Select(s => s.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(forbiddenWords))
            {
                string[] words = forbiddenWords.Split('|');
                if (words.Any(w => notamFreeText.Contains(w)))
                {
                    return false;
                }
            }

            return true;
        }

        #region ScheduleValidator   
        public static bool ValidateTimeSchedule(string ItemD, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
            var SubItemDElements = ItemD.Split(',');
            List<ParserValidationResult> ParserResultList = new List<ParserValidationResult>();

            foreach (var item in SubItemDElements)
            {
                ParserValidationResult parserResult;
                bool validSubItemD = ValidateSubSchedule(item, NotamStartValidity, NotamEndValidity, out parserResult);
                if (!validSubItemD)
                {
                    return false;
                }
                else
                {
                    ParserResultList.Add(parserResult);
                }
            }
            return ValidateFullSchedule(ParserResultList, NotamStartValidity, NotamEndValidity);
        }
        public static bool ValidateFullSchedule(List<ParserValidationResult> ParserResultList, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
                if (ParserResultList[0].parserResult.@case == Common.ParserCase.Calendar && ParserResultList[0].parserResult.included)
                {
                    return ValidateCalendarIncludeCase(ParserResultList, NotamStartValidity, NotamEndValidity);
                }
                if (ParserResultList[0].parserResult.@case == Common.ParserCase.WeekDay && ParserResultList[0].parserResult.included)
                {
                    return ValidateWeekDayCase(ParserResultList, NotamStartValidity, NotamEndValidity);
                }
                if (ParserResultList[0].parserResult.@case == Common.ParserCase.TimeOnly && ParserResultList[0].parserResult.included)
                {
                    return ValidateTimeCase(ParserResultList, NotamStartValidity, NotamEndValidity);
                }
                if (ParserResultList[0].parserResult.@case == Common.ParserCase.Calendar && !ParserResultList[0].parserResult.included)
                {
                    return ValidateCalendarExcludeCase(ParserResultList, NotamStartValidity, NotamEndValidity);
                }
            return false;
        }
        public static bool ValidateCalendarIncludeCase(List<ParserValidationResult> ParserResultList, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
            var FullCalendarList = new List<Tuple<DateTime, DateTime,bool>>();
            foreach (var item in ParserResultList)
            {
                if (item.parserResult.@case != Common.ParserCase.Calendar || !item.parserResult.included)
                {
                    return false;
                }
                FullCalendarList.AddRange(item.calendarDateList);
            }

            return ValidateAllDates(FullCalendarList, NotamStartValidity, NotamEndValidity);
        }
        public static bool ValidateCalendarExcludeCase(List<ParserValidationResult> ParserResultList, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
            var FullCalendarList = new List<Tuple<DateTime, DateTime, bool>>();
            foreach (var item in ParserResultList)
            {
                if (item.parserResult.@case != Common.ParserCase.Calendar || item.parserResult.included)
                {
                    return false;
                }
                FullCalendarList.AddRange(item.calendarDateList);
            }

            return ValidateAllDates(FullCalendarList, NotamStartValidity, NotamEndValidity);
        }
        public static bool ValidateTimeCase(List<ParserValidationResult> ParserResultList, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
            ParserResultList.RemoveAt(0);
            var FullCalendarList = new List<Tuple<DateTime, DateTime, bool>>();
            foreach (var item in ParserResultList)
            {
                if (item.parserResult.@case != Common.ParserCase.Calendar || item.parserResult.included)
                {
                    return false;
                }
                FullCalendarList.AddRange(item.calendarDateList);
            }

            return ValidateAllDates(FullCalendarList, NotamStartValidity, NotamEndValidity);
        }
        public static bool ValidateWeekDayCase(List<ParserValidationResult> ParserResultList, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
            int currentCase = Common.ParserCase.WeekDay.Tag;
            var FullCalendarList = new List<Tuple<DateTime, DateTime, bool>>();
            foreach (var item in ParserResultList)
            {
                if (item.parserResult.@case == Common.ParserCase.WeekDay && item.parserResult.included)
                {
                    if (currentCase == Common.ParserCase.Calendar.Tag)
                    {
                        return false;
                    }
                }
                else if (item.parserResult.@case == Common.ParserCase.Calendar && !item.parserResult.included)
                {
                    currentCase = Common.ParserCase.Calendar.Tag;
                }
                else
                {
                    return false;
                }

                FullCalendarList.AddRange(item.calendarDateList);
            }

            return ValidateAllDates(FullCalendarList, NotamStartValidity, NotamEndValidity);
        }
        public static bool ValidateSubSchedule(string SubItemD, DateTime NotamStartValidity, DateTime NotamEndValidity, out ParserValidationResult _parserValidationResult)
        {
            var result = Parser.Agent.Run(SubItemD);
            Common.ParserResult _parserResult = null;
            _parserValidationResult = null;

            if (result.IsSuccess)
            {
                _parserResult = CastSuccess(result);
                if (_parserResult.@case == Common.ParserCase.TimeOnly)
                {
                    return ValidateTimeOnlyCase(_parserResult, NotamStartValidity, NotamEndValidity, out _parserValidationResult);
                }
                if (_parserResult.@case == Common.ParserCase.Calendar)
                {
                    return ValidateCalendarCase(_parserResult, NotamStartValidity, NotamEndValidity, out _parserValidationResult);
                }
                if (_parserResult.@case == Common.ParserCase.WeekDay)
                {
                    return ValidateWeekDayCase(_parserResult, NotamStartValidity, NotamEndValidity, out _parserValidationResult);
                }
            }
            return false;
        }
        #region TimeOnlyValidator

        private static bool ValidateTimeOnlyCase(Common.ParserResult parserResult, DateTime NotamStartValidity, DateTime NotamEndValidity, out ParserValidationResult _parserValidationResult)
        {
            var TimeList = new List<Common.PeriodInMinutes>();
            _parserValidationResult = new ParserValidationResult(parserResult, null);
            if (parserResult.included)
            {
                bool isValidTime = ValidTimeList(parserResult, out TimeList);
                if (isValidTime)
                {
                    if (TimeList != null && TimeList.Any())
                    {
                        if (TimeList.Count() == 1 && TimeList[0].start == 0 && TimeList[0].finish == 1440)
                        {
                            return MustStartAndEndWithValidityDates(NotamStartValidity.Date,
                                                                   NotamEndValidity.Date.AddHours(23).AddMinutes(59),
                                                                   NotamStartValidity, NotamEndValidity);
                        }
                        else
                        {
                            bool SRorSS = false; 
                            foreach (var item in parserResult.minutes)
                            {
                                SRorSS = SRorSS || isSRorSS(item.start) || isSRorSS(item.finish);
                            }
                            if (!SRorSS)
                            {
                                DateTime EventStDteTme, EventEndDteTme;
                                EventStDteTme = NotamStartValidity.Date.AddMinutes(parserResult.minutes[0].start);
                                EventEndDteTme = NotamEndValidity.Date.AddMinutes(parserResult.minutes[parserResult.minutes.Length - 1].finish);
                                return (MustStartAndEndWithValidityDates(EventStDteTme, EventEndDteTme, NotamStartValidity, NotamEndValidity));
                            }
                        }
                    }
                    return true;
                }
            }
            return false;
        }
        private static bool ValidTimeList(Common.ParserResult parserResult, out List<Common.PeriodInMinutes> TimeList)
        {
            var myEnumerator = parserResult.minutes.GetEnumerator();
            TimeList = new List<Common.PeriodInMinutes>();
            bool isValidTimeObject, eventStTmeisSRorSS, eventEndTmeisSRorSS;

            while (myEnumerator.MoveNext())
            {
                foreach (Common.PeriodInMinutes item in TimeList)
                {
                    if (parserResult.subcase != Common.ParserSubcase.Continues)
                    {
                        if (!TimeMustNotOverlap(item.start, item.finish, ((Common.PeriodInMinutes)myEnumerator.Current).start, ((Common.PeriodInMinutes)myEnumerator.Current).finish)
                            ||
                            !TimeMustBeInChronologicalOrder(item.start, item.finish, ((Common.PeriodInMinutes)myEnumerator.Current).start, ((Common.PeriodInMinutes)myEnumerator.Current).finish))
                        {
                            return false;
                        }
                    }
                }
                isValidTimeObject = IsValidTimeObject(((Common.PeriodInMinutes)myEnumerator.Current).start,
                                        ((Common.PeriodInMinutes)myEnumerator.Current).finish, out eventStTmeisSRorSS, out eventEndTmeisSRorSS);
                if (isValidTimeObject)
                {
                    if (!eventStTmeisSRorSS && !eventEndTmeisSRorSS)
                    {
                        TimeList.Add((Common.PeriodInMinutes)myEnumerator.Current);
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        private static bool TimeMustNotOverlap(int EventStTme1, int EventEndTme1, int EventStTme2, int EventEndTme2)
        {
            return !(EventStTme1 <= EventEndTme2 && EventStTme2 <= EventEndTme1);
        }
        private static bool TimeMustBeInChronologicalOrder(int EventStTme1, int EventEndTme1, int EventStTme2, int EventEndTme2)
        {
            return (EventStTme1 <= EventEndTme1 && EventStTme1 <= EventStTme2 && EventEndTme1 <= EventEndTme2 && EventStTme2 <= EventEndTme2);
        }

        private static bool isSRorSS(int EventTme)
        {
            return ((Enumerable.Range(0, 60).Contains(EventTme - Common._SUNRISE_IN_MINUTES_))
                    || (Enumerable.Range(0, 60).Contains(EventTme - Common._SUNSET_IN_MINUTES_)));
        }
        private static bool IsValidTimeObject(int EventStTme, int EventEndTme, out bool EventStTmeisSRorSS, out bool EventEndTmeisSRorSS)
        {
            bool validTime = false;
            EventStTmeisSRorSS = false;
            EventEndTmeisSRorSS = false;

            if ((Enumerable.Range(0, Common._MINUTES_IN_DAY_ + 1).Contains(EventStTme))
                &&
                (Enumerable.Range(0, Common._MINUTES_IN_DAY_ + 1).Contains(EventEndTme)))
            {
                validTime = true;
            }
            else if (isSRorSS(EventStTme)
                    &&
                    (Enumerable.Range(0, Common._MINUTES_IN_DAY_).Contains(EventEndTme))
                    )
            {
                validTime = true;
                EventStTmeisSRorSS = true;
            }
            else if ((Enumerable.Range(0, Common._MINUTES_IN_DAY_).Contains(EventStTme))
                    &&
                    (isSRorSS(EventEndTme)))                
            {
                validTime = true;
                EventEndTmeisSRorSS = true;
            }
            else if (isSRorSS(EventStTme)
                    &&
                    (isSRorSS(EventEndTme)))
            {
                validTime = true;
                EventStTmeisSRorSS = true;
                EventStTmeisSRorSS = true;
            }
            return validTime;
        }
        #endregion

        #region CalendarValidator

        private static void MonthsandYears(int stMonth, int endMonth, out int StMonth, out int EndMonth, out int StYear, out int EndYear)
        {
            StMonth = stMonth + 1;
            EndMonth = endMonth + 1;
            StYear = ((StMonth >= DateTime.UtcNow.Month) ? DateTime.UtcNow.Year : (DateTime.UtcNow.Year + 1));
            EndYear = ((StMonth <= EndMonth) ? StYear : (DateTime.UtcNow.Year + 1));
        }
        private static DateTime? Convert_to_DateTime(int Year, int Month, int Day)
        {
            try
            {
                return new DateTime(Year, Month, Day);
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }
        private static IEnumerable<DateTime> datesArray(DateTime stDate, DateTime endDate)
        {
            IEnumerable<DateTime> dates = Enumerable.Range(0, 1 + endDate.Subtract(stDate).Days)
                  .Select(offset => stDate.AddDays(offset))
                  .ToArray();
            return dates;
        }
        private static List<DateTime> CalendarDatesFromParser(Common.ParserResult parserResult)
        {
            int _stMonth, _endMonth, _stYear, _endYear;
            MonthsandYears(parserResult.months[0].Tag, (parserResult.months.Length == 2 ? parserResult.months[1].Tag : parserResult.months[0].Tag)
                          , out _stMonth, out _endMonth, out _stYear, out _endYear);
            DateTime? stDate, endDate;
            var DatesEnumerator = new List<DateTime>();

            foreach (var day in parserResult.days)
            {
                stDate = Convert_to_DateTime(_stYear, _stMonth, day.startDay);
                endDate = Convert_to_DateTime(_endYear, _endMonth, day.endDay);
                if (!stDate.HasValue || !endDate.HasValue || endDate < stDate)
                {
                    return null;
                }
                if (parserResult.subcase != Common.ParserSubcase.Continues)
                {
                    var buffer = datesArray((DateTime)stDate, (DateTime)endDate).GetEnumerator();
                    while (buffer.MoveNext())
                    {
                        DatesEnumerator.Add(buffer.Current);
                    }
                }
                else
                {
                    DatesEnumerator.Add((DateTime)stDate);
                    DatesEnumerator.Add((DateTime)endDate);
                }
            }
            return DatesEnumerator;
        }
        private static bool DatesMustNotOverlap(DateTime Date1, DateTime Date2)
        {
            return !(Date1 == Date2);
        }
        private static bool DatesMustBeInChronologicalOrder(DateTime Date1, DateTime Date2)
        {
            return (Date1 < Date2);
        }
        private static bool ValidateCalendarCase(Common.ParserResult parserResult, DateTime NotamStartValidity, DateTime NotamEndValidity, out ParserValidationResult _parserValidationResult)
        {
            var _calendarDateList = new List<Tuple<DateTime, DateTime, bool>>();
            bool isValid = isValidCalendarDates(parserResult, NotamStartValidity, NotamEndValidity, out _calendarDateList);
            _parserValidationResult = new ParserValidationResult(parserResult, _calendarDateList);
            return isValid;
        }
        private static bool isValidCalendarDates(Common.ParserResult parserResult, DateTime NotamStartValidity, DateTime NotamEndValidity, out List<Tuple<DateTime, DateTime,bool>> ValidCalendarDateList)
        {

            ValidCalendarDateList = new List<Tuple<DateTime, DateTime,bool>>();
            var TimeList = new List<Common.PeriodInMinutes>();
            if (!parserResult.included && parserResult.minutes.Count() > 0)
            {
                return false;
            }
            // validate time part
            if (parserResult.minutes != null && parserResult.minutes.Count() > 0)
            {
                if(!parserResult.included)
                {
                    return false;
                }
                else if (!ValidTimeList(parserResult, out TimeList))
                {
                    return false;
                }
            }

            var calendarDates = CalendarDatesFromParser(parserResult);
            if (calendarDates == null || !calendarDates.Any())
            {
                return false;
            }
            var Datesbuffer = new List<DateTime>();
            var calendarDatesEnumerator = calendarDates.GetEnumerator();
            while (calendarDatesEnumerator.MoveNext())
            {
                foreach (DateTime item in Datesbuffer)
                {
                    // validate Dates only. 
                    if (!DatesMustBeInChronologicalOrder(item, calendarDatesEnumerator.Current))
                    {
                        return false;
                    }
                    // if no minutes then check if dates are within notam Validity Dates
                    if (TimeList == null || !TimeList.Any())
                    {
                        if (!MustbeWithinValidityDates(calendarDatesEnumerator.Current.Date, calendarDatesEnumerator.Current.Date, NotamStartValidity.Date, NotamEndValidity.Date))
                        {
                            return false;
                        }
                    }
                }
                Datesbuffer.Add(calendarDatesEnumerator.Current);
            }
            if (Datesbuffer == null || !Datesbuffer.Any())
            {
                return false;
            }

            if (TimeList != null && TimeList.Any())
            {
                if (parserResult.subcase != Common.ParserSubcase.Continues)
                {
                    var ValidTimeListEnumerator = TimeList.GetEnumerator();
                    while (ValidTimeListEnumerator.MoveNext())
                    {
                        var IntermValidCalendarDateList = new List<Tuple<DateTime, DateTime, bool>>();
                        foreach (DateTime item in Datesbuffer)
                        {
                            DateTime stDateTme = item, endDateTme = item;
                            stDateTme = stDateTme.AddMinutes(ValidTimeListEnumerator.Current.start);
                            endDateTme = endDateTme.AddMinutes(ValidTimeListEnumerator.Current.finish);
                            if (!MustbeWithinValidityDates(stDateTme, endDateTme, NotamStartValidity, NotamEndValidity))
                            {
                                return false;
                            }
                            foreach (Tuple<DateTime, DateTime, bool> item2 in ValidCalendarDateList)
                            {
                                if (!DateTimesMustNotOverlap(stDateTme, endDateTme, item2.Item1, item2.Item2))
                                {
                                    return false;
                                }
                            }
                            if (IntermValidCalendarDateList != null && IntermValidCalendarDateList.Any())
                            {
                                if (!DatesMustNotExceedSevenDayGap(IntermValidCalendarDateList.Last().Item1, IntermValidCalendarDateList.Last().Item2, stDateTme, endDateTme))
                                {
                                    return false;
                                }
                            }
                            IntermValidCalendarDateList.Add(new Tuple<DateTime, DateTime, bool>(stDateTme, endDateTme, parserResult.included));
                        }
                        ValidCalendarDateList.AddRange(IntermValidCalendarDateList);
                        ValidCalendarDateList.Sort();
                    }
                }
                else
                {
                    if (TimeList.Count > 1 && Datesbuffer.Count > 2)
                    {
                        return false;
                    }
                    else
                    {
                        DateTime stDateTme = Datesbuffer[0], endDateTme = Datesbuffer[1];
                        stDateTme = stDateTme.AddMinutes(TimeList[0].start);
                        endDateTme = endDateTme.AddMinutes(TimeList[0].finish);
                        if (!MustbeWithinValidityDates(stDateTme, endDateTme, NotamStartValidity, NotamEndValidity))
                        {
                            return false;
                        }
                        ValidCalendarDateList.Add(new Tuple<DateTime, DateTime, bool>(stDateTme, endDateTme, parserResult.included));
                    }
                }
            }
            else if(parserResult.minutes == null && parserResult.minutes.Count() == 0)
            {
                foreach (DateTime item in calendarDates)
                {
                    ValidCalendarDateList.Add(new Tuple<DateTime, DateTime, bool>(item.Date, item.Date.AddHours(23).AddMinutes(59), parserResult.included));
                }
            }
            return true;
        }
        private static bool DatesMustNotExceedSevenDayGap(DateTime EventStDteTme1, DateTime EventEndDteTme1, DateTime EventStDteTme2, DateTime EventEndDteTme2)
        {
            return !DateTimesMustNotOverlap(EventStDteTme1, EventEndDteTme1.AddDays(7), EventStDteTme2, EventEndDteTme2.AddDays(7));
        }
        private static bool MustbeWithinValidityDates(DateTime EventStDteTme, DateTime EventEndDteTme, DateTime StartValidity, DateTime EndValidity)
        {
            return (EventStDteTme >= StartValidity) && (EventEndDteTme <= EndValidity);
        }
        private static bool MustStartAndEndWithValidityDates(DateTime EventStDteTme, DateTime EventEndDteTme, DateTime StartValidity, DateTime EndValidity)
        {
            return (EventStDteTme == StartValidity  && EventEndDteTme == EndValidity);
        }
        private static bool MustNotStartAndEndWithValidityDates(DateTime EventStDteTme, DateTime EventEndDteTme, DateTime StartValidity, DateTime EndValidity)
        {
            return (EventStDteTme > StartValidity && EventEndDteTme < EndValidity);
        }
        private static bool DateTimesMustNotOverlap(DateTime EventStDteTme1, DateTime EventEndDteTme1, DateTime EventStDteTme2, DateTime EventEndDteTme2)
        {
            return !(EventStDteTme1 <= EventEndDteTme2 && EventStDteTme2 <= EventEndDteTme1);
        }
        private static bool ValidateAllDates(List<Tuple<DateTime, DateTime,bool>> CalanderDateTimes, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
            var IntermValidCalendarDateList = new List<Tuple<DateTime, DateTime, bool>>();
            var ValidTimeListEnumerator = CalanderDateTimes.GetEnumerator();
            List<Tuple<DateTime, DateTime, bool>> IncludeList = CalanderDateTimes.Where(x => x.Item3 == true).ToList();
            List<Tuple<DateTime, DateTime, bool>> ExculdeList = CalanderDateTimes.Where(x => x.Item3 == false).ToList();

            while (ValidTimeListEnumerator.MoveNext())
            {
                if (ValidTimeListEnumerator.Current.Item3)
                {
                    foreach (Tuple<DateTime, DateTime, bool> item in IntermValidCalendarDateList)
                    {
                        if (IntermValidCalendarDateList != null && IntermValidCalendarDateList.Any())
                        {
                            if (item.Item3)
                            {
                                if (!DateTimesMustNotOverlap(item.Item1, item.Item2, ValidTimeListEnumerator.Current.Item1, ValidTimeListEnumerator.Current.Item2))
                                {
                                    return false;
                                }
                                if (!DatesMustNotExceedSevenDayGap(IntermValidCalendarDateList.Last().Item1, IntermValidCalendarDateList.Last().Item2, ValidTimeListEnumerator.Current.Item1, ValidTimeListEnumerator.Current.Item2))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    bool MatchingAnInclude = false;
                    foreach (Tuple<DateTime, DateTime, bool> item in IntermValidCalendarDateList)
                    {
                        if (IntermValidCalendarDateList != null && IntermValidCalendarDateList.Any())
                        {
                            if (item.Item3)
                            {
                                if (DateTimesMustNotOverlap(item.Item1, item.Item2, ValidTimeListEnumerator.Current.Item1, ValidTimeListEnumerator.Current.Item2))
                                {
                                    MatchingAnInclude = true;
                                }
                            }
                            else
                            {
                                if (!DateTimesMustNotOverlap(item.Item1, item.Item2, ValidTimeListEnumerator.Current.Item1, ValidTimeListEnumerator.Current.Item2))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    if(!MatchingAnInclude && IncludeList != null && IncludeList.Count > 0)
                    {
                        return false;
                    }
                }
                IntermValidCalendarDateList.Add(new Tuple<DateTime, DateTime,bool>(ValidTimeListEnumerator.Current.Item1, ValidTimeListEnumerator.Current.Item2, ValidTimeListEnumerator.Current.Item3));
            }
            
            if (IncludeList != null && IncludeList.Count > 0)
            {
                // sch must start and end with notam start and end date.
                if(!MustStartAndEndWithValidityDates(IncludeList[0].Item1,IncludeList[IncludeList.Count()-1].Item2, NotamStartValidity, NotamEndValidity))
                {
                    return false;
                }
            }

            if (ExculdeList != null && ExculdeList.Count > 0)
            {
                // sch must start and end with notam start and end date.
                if (!MustNotStartAndEndWithValidityDates(ExculdeList[0].Item1.Date, ExculdeList[ExculdeList.Count() - 1].Item2.Date, NotamStartValidity.Date, NotamEndValidity.Date))
                {
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region WeekDayValidator

        private static List<DateTime> ProcessDaysofWeeektoCalendarDates(DateTime dtStartDate, DateTime targetDate, List<int> daysOfWeek)
        {
            DateTime dtLoop = dtStartDate;
            List<DateTime> dtRequiredDates = new List<DateTime>();
            for (int i = dtStartDate.DayOfYear; i <= targetDate.DayOfYear; i++)
            {
                foreach (int day in daysOfWeek)
                {
                    if ((int)dtLoop.DayOfWeek == day)
                    {
                        dtRequiredDates.Add(dtLoop);
                    }
                }
                dtLoop = dtLoop.AddDays(1).Date;
            }
            return dtRequiredDates;
        }

        private static List<DateTime> CalendarDatesFromWeekDay(Common.ParserResult parserResult, DateTime NotamStartValidity, DateTime NotamEndValidity)
        {
            
            List<DateTime> CalendarDates = new List<DateTime>();

            foreach (var day in parserResult.days)
            {
                List<int> daysOfWeek = new List<int>();
                for (int i = day.startDay; i <= day.endDay; i++)
                {
                    daysOfWeek.Add(i);
                }
                //daysOfWeek = Enumerable.Range(day.startDay, day.endDay);
                CalendarDates.AddRange(ProcessDaysofWeeektoCalendarDates(NotamStartValidity, NotamEndValidity, daysOfWeek));
            }
            CalendarDates.Sort();
            return CalendarDates;
        }
        private static bool ValidateWeekDayCase(Common.ParserResult parserResult, DateTime NotamStartValidity, DateTime NotamEndValidity, out ParserValidationResult _parserValidationResult)
        {
            var _calendarDateList = new List<Tuple<DateTime, DateTime, bool>>();
            bool isValid = isValidWeekDayDates(parserResult, NotamStartValidity, NotamEndValidity, out _calendarDateList);
            _parserValidationResult = new ParserValidationResult(parserResult, _calendarDateList);
            return isValid;
        }
        private static bool isValidWeekDayDates(Common.ParserResult parserResult, DateTime NotamStartValidity, DateTime NotamEndValidity, out List<Tuple<DateTime, DateTime, bool>> ValidCalendarDateList)
        {
            ValidCalendarDateList = new List<Tuple<DateTime, DateTime, bool>>();
            if (!parserResult.included)
            {
                return false;
            }            
            var TimeList = new List<Common.PeriodInMinutes>();
            // validate time part
            if (parserResult.minutes != null || parserResult.minutes.Any())
            {
                if (!ValidTimeList(parserResult, out TimeList))
                {
                    return false;
                }
            }

            var calendarDates = CalendarDatesFromWeekDay(parserResult, NotamStartValidity, NotamEndValidity);
            if (calendarDates == null || !calendarDates.Any())
            {
                return false;
            }

            if (TimeList != null && TimeList.Any())
            {
                var ValidTimeListEnumerator = TimeList.GetEnumerator();
                while (ValidTimeListEnumerator.MoveNext())
                {
                    var IntermValidCalendarDateList = new List<Tuple<DateTime, DateTime, bool>>();
                    foreach (DateTime item in calendarDates)
                    {
                        DateTime stDateTme = item, endDateTme = item;
                        stDateTme = stDateTme.Date.AddMinutes(ValidTimeListEnumerator.Current.start);
                        endDateTme = endDateTme.Date.AddMinutes(ValidTimeListEnumerator.Current.finish);
                        if (!MustbeWithinValidityDates(stDateTme, endDateTme, NotamStartValidity, NotamEndValidity))
                        {
                            return false;
                        }
                        foreach (Tuple<DateTime, DateTime, bool> item2 in ValidCalendarDateList)
                        {
                            if (!DateTimesMustNotOverlap(stDateTme, endDateTme, item2.Item1, item2.Item2))
                            {
                                return false;
                            }
                        }
                        if (IntermValidCalendarDateList != null && IntermValidCalendarDateList.Any())
                        {
                            if (!DatesMustNotExceedSevenDayGap(IntermValidCalendarDateList.Last().Item1, IntermValidCalendarDateList.Last().Item2, stDateTme, endDateTme))
                            {
                                return false;
                            }
                        }
                        IntermValidCalendarDateList.Add(new Tuple<DateTime, DateTime, bool>(stDateTme, endDateTme, parserResult.included));
                    }
                    ValidCalendarDateList.AddRange(IntermValidCalendarDateList);
                    ValidCalendarDateList.Sort();
                }
            }
            else
            {
                foreach (DateTime item in calendarDates)
                {
                    ValidCalendarDateList.Add(new Tuple<DateTime, DateTime, bool>(item.Date, item.Date.AddHours(23).AddMinutes(59), parserResult.included));
                }
            }
            return true;
        }
        #endregion
        public class ParserValidationResult
        {
            public Common.ParserResult parserResult;
            public List<Tuple<DateTime, DateTime,bool>> calendarDateList = new List<Tuple<DateTime, DateTime,bool>>();

            public ParserValidationResult(Common.ParserResult _parserResult, List<Tuple<DateTime, DateTime,bool>> _calendarDateList)
            {
                parserResult = _parserResult;
                calendarDateList = _calendarDateList;
            }
        }
        #endregion
        private static Common.ParserResult CastSuccess(CharParsers.ParserResult<Common.ParserResult, Unit> fshartObj)
        {
            return ((CharParsers.ParserResult<Common.ParserResult, Unit>.Success)fshartObj).Item1 as Common.ParserResult;
        }
        private static string CastFailure(CharParsers.ParserResult<Common.ParserResult, Unit> fshartObj)
        {
            var error = ((CharParsers.ParserResult<Common.ParserResult, Unit>.Failure)fshartObj).Item1;
            return error.Replace("\r\n", "").Replace("\n", "").Replace("\r", "");
        }
    }
}