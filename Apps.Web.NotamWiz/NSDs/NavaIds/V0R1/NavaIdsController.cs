﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.NavaIds.V0R1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;

    public class NavaIdsController : NsdBaseController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(NavaIdsController));

        public NavaIdsController(IUow uow, IClusterQueues clusterQueues)
            : base(uow, clusterQueues)
        {
        }

        public override ActionResult Index( string id )
        {
            var sdoParts = id.Split('^');
            if( sdoParts.Length < 2 )
                throw new InvalidOperationException("The id parameter showld contaimn subjectId and Type");

            var subjectType = sdoParts[1];

            var navaIdTypes = GetNavaIdType(subjectType);
            var navaIdOptions = new List<NavaOption>();
            foreach (var navaType in navaIdTypes)
                navaIdOptions.AddRange(GetNavaIdOptions(navaType, Thread.CurrentThread.CurrentUICulture.Name));

            ViewBag.NavaIdTypes = Json(navaIdTypes);
            ViewBag.NavaIdOptions = Json(navaIdOptions);

            return View(new ViewModel());
        }


        protected override async Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase)
        {
            this._log.Object(new
            {
                Method = "GenerateAsync",
                Parameters = new
                {
                    proposalType,
                    notamProposal,
                    notamProposalBase
                }
            });

            var proposal = notamProposalBase as ViewModel;
            if (proposal == null)
                throw new NullReferenceException("notamProposalBase"); //TODO: What to do in this case?

            notamProposal.IsCatchAll = false;
            notamProposal.Type = proposalType;
            notamProposal.Code23 = GetQ23Code(proposal.NavaType.Text, Subject.Designator);

            var isQ23CodeId = notamProposal.Code23 == "ID";

            notamProposal.Fir = Subject.Fir;
            notamProposal.Code45 = proposal.NavaOption.Value != 1 ? "AS" : "XX";
            notamProposal.Traffic = isQ23CodeId ? "I" : "IV";
            notamProposal.Purpose = isQ23CodeId ? "NBO" : "BO";
            notamProposal.Scope = GetQlineScope(isQ23CodeId, Subject.Ad);
            notamProposal.Lower = 0;
            notamProposal.Upper = 999;
            notamProposal.Coordinates = GeoDataService.DMSToRoundedDM(string.Format("{0} {1}", GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLat, true), GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLong, false)));
            notamProposal.Radius = notamProposal.Scope == "A" ? 5 : 25;
            notamProposal.ItemA = GetItemA(Subject.Ad);

            var sdoSubject = await Uow.SdoSubjectRepo.GetByDesignatorAsync(Subject.Ad);
            
            notamProposal.Series = GetNotamSeriesByGeoRegion(sdoSubject.SubjectGeo, "MR");

            var frequency = GetFrequency(proposal.NavaType.Text, Subject.SdoAttributes);

            if (proposalType == "C")
            {
                notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
                notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
                notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;

                notamProposal.Code45 = "AK";
                notamProposal.ItemE = GenerateItemEForCancellation(Subject.Name, proposal, Subject, frequency, "en");
                notamProposal.ItemEFrench = proposal.Bilingual ? GenerateItemEForCancellation(Subject.Name, proposal, Subject, frequency, "fr") : "";
                return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
            }

            notamProposal.ItemE = GenerateItemE(Subject.Name, proposal, Subject, frequency, "en");
            notamProposal.ItemEFrench = proposal.Bilingual ? GenerateItemE(Subject.Name, proposal, Subject, frequency, "fr") : "";

            return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
        }


        protected override async Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal)
        {
            return notamProposal;
        }

        protected override async Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase)
        {
            this._log.Object(new
            {
                Method = "ValidateAsync",
                Parameters = new
                {
                    notamProposalBase
                }
            });
            try
            {
                var validator = new NavaIdsValidator(this, Uow);
                var validationResult = await validator.ValidateAsync(notamProposalBase as ViewModel);
                if (!validationResult.IsValid)
                {
                    var result = new ProcessResult
                    {
                        Success = false,
                        ErrorCode = "CAC-0001"
                    };
                    foreach (var failure in validationResult.Errors)
                    {
                        result.Errors.Add(failure.ErrorMessage);
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                this._log.Error(e);
            }
            return new ProcessResult();
        }

        private static string GenerateItemE(string name, ViewModel model, NotamSubjectViewModel subject, string freq,  string language)
        {
            const string space = " ";

            var navaOptions = GetNavaIdOptions(model.NavaType, language);

            var optionValue = 0;
            if (model.NavaType.Text == model.NavaOption.Group)
                optionValue = model.NavaOption.Value > navaOptions.Count ? 0 : model.NavaOption.Value;

            var option = navaOptions.First(x => x.Value == optionValue);

            var itemEStrbldr = new StringBuilder();

            itemEStrbldr.Append(name);  //name
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(model.NavaType.Text); //navatype
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(subject.Designator); //ident (codeId)
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(freq); 
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(option.Text);

            return itemEStrbldr.ToString();
        }

        private static string GenerateItemEForCancellation(string name, ViewModel model, NotamSubjectViewModel subject, string freq, string language)
        {
            const string space = " ";

            var itemEStrbldr = new StringBuilder();

            itemEStrbldr.Append(name); //name
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(model.NavaType.Text); //navatype
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(subject.Designator); //ident (codeId)
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(freq);
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(language == "en" ? "RESUMED NORMAL OPS" : "RETOUR A LA NORMALE");

            return itemEStrbldr.ToString();
        }

        private string GetQ23Code(string navaType, string codeId)
        {
            switch (navaType)
            {
                case "VOR/DME" : return "NM";
                case "VOR": return "NV";
                case "DME": return codeId[0] == 'I' ? "ID" : "ND";
                case "NDB": return "NB";
                case "TACAN": return "NN";
                case "VORTAC": return "NT";
                default:
                    return "";
            }
        }

        private string GetQlineScope(bool isQ23CodeId, string aerodrome)
        {
            var scope = "";
            if (!string.IsNullOrEmpty(aerodrome))
            {
                scope = !isQ23CodeId ? "AE" : "E";
            }
            else if (isQ23CodeId)
                scope = "A";

            return scope;
        }

        private string GetFrequency(string navaType, string sdoAttributes)
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(sdoAttributes);

            var fr = "";
            var uom = ""; 
            var ch = "";

            try
            {
                switch (navaType)
                {
                    case "NDB":
                    case "VOR":
                        fr = data.ValFreq != null ? data.ValFreq.ToString() : "";
                        uom = data.UomFreq != null ? data.UomFreq.ToString() : "";
                        ch = "";
                        break;
                    case "DME":
                        fr = data.ValGhostFreq != null ? data.ValGhostFreq.ToString() : "";
                        uom = data.UomGhostFreq != null ? data.UomGhostFreq.ToString() : "";
                        ch = data.CodeChannel != null ? data.CodeChannel.ToString() : ""; ;
                        break;
                    case "VOR/DME":
                        if (navaType == "DME")
                        {
                            //todo: get the value from the contained object
                        }
                        else
                        {
                            fr = data.ValFreq != null ? data.ValFreq.ToString() : "";
                            uom = data.UomFreq != null ? data.UomFreq.ToString() : "";
                            ch = "";
                        }
                        break;
                    case "TACAN":
                        fr = "";
                        uom = "";
                        ch = ""; ;
                        break;
                    case "VORTAC":
                        if (navaType == "TACAN")
                        {
                            //todo: get the value from the contained object
                        }
                        else
                        {
                            fr = data.ValFreq != null ? data.ValFreq.ToString() : "";
                            uom = data.UomFreq != null ? data.UomFreq.ToString() : "";
                            ch = "";
                        }
                        break;
                }
            }
            catch (Exception)
            {
                //TODO: Log errors here...
            }

            return fr + uom + ch;
        }

        private static List<NavaOption> GetNavaIdOptions(NavaOption navaSelection, string language)
        {
            //TODO: From resources
            switch (navaSelection.Text)
            {
                case "VOR":
                case "DME":
                case "VOR/DME":
                case "VORTAC":
                    return language != "fr" ? 
                        new List<NavaOption>
                        {
                            new NavaOption { Text = "U/S", Group = navaSelection.Text,  Value = 0},
                            new NavaOption { Text = "UNMONITORED", Group = navaSelection.Text,  Value = 1}
                        }
                    : new List<NavaOption>
                        {
                            new NavaOption { Text = "U/S",  Group = navaSelection.Text,  Value = 0},
                            new NavaOption { Text = "SANS SURVEILLANCE", Group = navaSelection.Text,  Value = 1}
                        };
                case "NDB": //
                    return language != "fr" ?
                        new List<NavaOption>
                        {
                            new NavaOption { Text = "U/S",  Group = navaSelection.Text,  Value = 0},
                            new NavaOption { Text = "UNMONITORED", Group = navaSelection.Text,  Value = 1},
                            new NavaOption { Text = "OPR AT 50 PCT PWR OR LESS", Group = navaSelection.Text,  Value = 2}

                        }
                    : new List<NavaOption>
                        {
                            new NavaOption { Text = "U/S",  Group = navaSelection.Text,  Value = 0},
                            new NavaOption { Text = "SANS SURVEILLANCE", Group = navaSelection.Text,  Value = 1},
                            new NavaOption { Text = "OPR A 50 PCT PWR OU MOINS", Group = navaSelection.Text,  Value = 2}
                        };
                default:  // "TACAN": 
                    return language != "fr" ?
                        new List<NavaOption>
                        {
                            new NavaOption { Text = "U/S",  Group = navaSelection.Text,  Value = 0},
                            new NavaOption { Text = "UNMONITORED",  Group = navaSelection.Text,  Value = 1},
                            new NavaOption { Text = "AZM U/S DME AVBL", Group = navaSelection.Text,  Value = 2},
                            new NavaOption { Text = "DME U/S AZM AVBL", Group = navaSelection.Text,  Value = 3},
                            new NavaOption { Text = "DME U/S AZM UNMONITORED", Group = navaSelection.Text,  Value = 4},
                            new NavaOption { Text = "AZM U/S DME UNMONITORED", Group = navaSelection.Text,  Value = 5}
                        }
                    : new List<NavaOption>
                        {
                            new NavaOption { Text = "U/S",  Group = navaSelection.Text,  Value = 0},
                            new NavaOption { Text = "SANS SURVEILLANCE", Group = navaSelection.Text,  Value = 1},
                            new NavaOption { Text = "AZM U/S DME AVBL", Group = navaSelection.Text,  Value = 2},
                            new NavaOption { Text = "DME U/S AMZN AVBL", Group = navaSelection.Text,  Value = 3},
                            new NavaOption { Text = "DME U/S AZM SANS SURVEILLANCE", Group = navaSelection.Text,  Value = 4},
                            new NavaOption { Text = "AZM U/S DME SANS SURVEILLANCE", Group = navaSelection.Text,  Value = 5}
                        };
            }
        }

        private static List<NavaOption> GetNavaIdType(string subjectType)
        {
            switch (subjectType)
            {
                case "NDB":
                    return new List<NavaOption> { new NavaOption { Text = "NDB", Value = 0 } };
                case "DME":
                    return new List<NavaOption> { new NavaOption { Text = "DME", Value = 0 } };
                case "VOR":
                    return new List<NavaOption> { new NavaOption { Text = "VOR", Value = 0 } };
                case "TACAN":
                    return new List<NavaOption> { new NavaOption { Text = "TACAN", Value = 0 } };
                case "VORTAC":
                    return new List<NavaOption>
                    {
                        new NavaOption { Text = "VOR",  Value = 0 },
                        new NavaOption { Text = "TACAN",  Value = 1 },
                    };
                default: //"VOR/DME":
                    return new List<NavaOption>
                    {
                        new NavaOption { Text = "VOR",  Value = 0 },
                        new NavaOption { Text = "DME",  Value = 1 },
                        new NavaOption { Text = "VOR/DME",  Value = 2 }
                    };
            }
        }
    }
}