﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.NavaIds.V0R1
{
    using System.Threading.Tasks;

    using FluentValidation;
    using FluentValidation.Results;

    using NavCanada.Core.Data.NotamWiz.Contracts;

    public class NavaIdsValidator : AbstractValidator<ViewModel>
    {
        private readonly NsdBaseController controller;

        private readonly IUow uow;

        public NavaIdsValidator(NsdBaseController controller, IUow uow)
        {
            this.controller = controller;
            this.uow = uow;
        }

        public override Task<ValidationResult> ValidateAsync(ValidationContext<ViewModel> context)
        {
            return base.ValidateAsync(context);
        }
    }
}