﻿using FluentValidation;
using log4net;
using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
using NavCanada.Core.Domain.Model.Entitities;
using System.Threading.Tasks;

namespace NavCanada.Applications.NotamWiz.Web.NSDs
{
    public class NotamProposalBaseValidator : AbstractValidator<NotamProposalViewModelBase>
    {

        private int _vsp = int.MinValue;

        private async Task<int> GetVsp(NotamProposalViewModelBase v)
        {
            if (_vsp == int.MinValue)
            {
                // check for VSP configuration
                var defaultVSP = "[{'user':'', 'role':'','subject':'', 'vsp':48} ]";
                var json = await Controller.GetConfigurationValue("NOTAM", "VSP", defaultVSP);
                var vsps = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<VSP[]>(json);
                var user = await Controller.GetUser();

                VSP bestMatch = new VSP() { vsp = 48 };
                int matchLevel = 0;
                foreach (var vsp in vsps)
                {
                    int matchCounter = 0;
                    if (vsp.user == user.UserName) matchCounter += 10;
                    if (await Controller.IsUserInRoleAsync(user.Id, vsp.role)) matchCounter += 10;
                    if (Controller.Subject.SubjectType == vsp.subject) matchCounter += 10;
                    if (vsp.user == "") matchCounter++;
                    if (vsp.role == "") matchCounter++;
                    if (vsp.subject == "") matchCounter++;

                    if (matchCounter > matchLevel)
                    {
                        bestMatch = vsp;
                        matchLevel = matchCounter;
                    }
                }

                _vsp = bestMatch.vsp;

            }

            v.VSP = _vsp;

            return _vsp;
        }

        private class VSP
        {
            public string user { get; set; }
            public string role { get; set; }
            public string subject { get; set; }
            public int vsp { get; set; }
        }

        private readonly ILog log = LogManager.GetLogger(typeof(NotamProposalBaseValidator));
        private NsdBaseController Controller
        {
            get;
            set;
        }

        public NotamProposalBaseValidator(NsdBaseController controller, IDbResWrapper dbRes)
        {
            this.log.Object(new
            {
                Method = "NotamProposalBaseValidator",
                Parameters = new
                {
                    controller
                }
            });

            Controller = controller;




            //
            // Requirments #373452   https://jazznc.deveng.local:9443/rm/web#action=com.ibm.rdm.web.pages.showArtifact&artifactURI=https%3A%2F%2Fjazznc.deveng.local%3A9443%2Frm%2Fresources%2F_YmEPoMyyEeO6FYe9fps2Gw
            //
            RuleFor(v => v.StartValidity)
                .NotNull()
                .GreaterThanOrEqualTo(DateTime.UtcNow)
                .When(v => !v.SetBeginDateAtSubmit)
                .WithMessage(dbRes.T("Invalid Start Validaity Date and/or Time", "NSD-Common"));

            When(v => !v.SetBeginDateAtSubmit, () =>
            {
                RuleFor(v => v).MustAsync(MustBeValidVsp).WithMessage(dbRes.T("Start Date/Time is invalid - cannot be greater than {0} hours from current UTC time", "NSD-Common"), v => v.VSP).WithName(dbRes.T("Start Validaity", "NSD-Common"));
            });


            When(v => !v.Permanent, () =>
            {
                RuleFor(v => v.EndValidity)
                    .NotNull()
                    .WithMessage(dbRes.T("End Validity is required.", "NSD-Common"));

                RuleFor(v => v.EndValidity)
                    .LessThanOrEqualTo(v => v.StartValidity.Value.AddHours(2208))
                    .When(v => !v.SetBeginDateAtSubmit)
                    .WithMessage(dbRes.T("Validaity period must not exceed 2208 hourse", "NSD-Common"));

                RuleFor(v => v.EndValidity)
                    .GreaterThanOrEqualTo(v => v.StartValidity.Value.AddMinutes(30))
                    .When(v => !v.SetBeginDateAtSubmit)
                    .WithMessage(dbRes.T("Validaity period must exceed 30 minutes", "NSD-Common"));

                RuleFor(v => v.EndValidity)
                    .GreaterThanOrEqualTo(v => DateTime.UtcNow.AddMinutes(30))
                    .When(v => v.SetBeginDateAtSubmit)
                    .WithMessage(dbRes.T("Validaity period must exceed 30 minutes", "NSD-Common"));
            });

            When(v => !(string.IsNullOrEmpty(v.ItemD)), () =>
            {
                RuleFor(v => v).MustAsync(MustBeValidTimeScheduleData).WithMessage(dbRes.T("Invalid ItemD", "NSD-Common"), v => v.ItemD).WithName(dbRes.T("ItemD", "NSD-Common"));
            });

        }

        private async Task<bool> MustBeValidVsp(NotamProposalViewModelBase v)
        {
            if (v.StartValidity == null)
            {
                return true;
            }

            int vsp = await GetVsp(v);
            var hours = (v.StartValidity - DateTime.UtcNow).Value.TotalHours;
            return vsp == 0 || hours <= vsp;
        }

        private async Task<bool> MustBeValidTimeScheduleData(NotamProposalViewModelBase v)
        {
            return CommonValidators.ValidateTimeSchedule(v.ItemD, (DateTime)v.StartValidity, (DateTime)v.EndValidity);
        }
    }
}