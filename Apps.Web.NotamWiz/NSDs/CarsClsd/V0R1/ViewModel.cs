﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.CarsClsd.V0R1
{
    public class ViewModel : NotamProposalViewModelBase
    {

        public string SchText
        {
            get;
            set;
        }
        public int CondStatusValue
        {
            get;
            set;
        }
    }
}