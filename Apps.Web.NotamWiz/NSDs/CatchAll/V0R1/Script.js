﻿define(["jquery", "Resources", "utilities", "NSDFormManager", "kendoExt"], function($, res, Util, nsdManager)
{
    var RES = res.getResources("NSD-CatchAll");
    var proposal = null;
    var thisNSD = null;
    var formValidator = null;
    var nsdEditor = null;
    var currentLanguage = null;
    var enFVlaueType = Object.freeze({ SFC: 0, Number: 1, });
    var enGVlaueType = Object.freeze({ UNL: 0, Number: 1, });
    var enFandGMeasurementUnit = Object.freeze({ AMSL: 0, AGL: 1, FL: 2 });
    return {
        subjectPushPin: null,
        validationErrorMessage: null,
        initalNOTAMTextEnglish: null,
        initalNOTAMTextFrench: null,
        init: function(editor, model)
        {
            thisNSD = this;
            nsdEditor = editor;
            proposal = model;
            currentLanguage = $("#currentLanguage").data("model");

            proposal.createNOTAMViewModel($.extend($("#nsdModel").data("model"), {
                Bilingual : kendo.observable(false)
                //Bilingual: proposal.Options.Bilingual,
                //SubjectLocation: Util.decimalToDMS(proposal.subject.SubjectGeoRefLat, "lat") + " " + Util.decimalToDMS(proposal.subject.SubjectGeoRefLong, "long"),
                //ItemA: thisNSD.getICAOItemA(proposal.subject.Designator)
            }));

            thisNSD.initUI();
            formValidator = $("#mainTable").data("kendoValidator");
            editor.conditionValidator = formValidator;

            proposal.nsdTokens.bind("change", thisNSD.validate);
            if (proposal.PropsalType === "C")
            {
                thisNSD.initalNOTAMTextEnglish = proposal.nsdTokens.EnglishText_FT;
                thisNSD.initalNOTAMTextFrench = proposal.nsdTokens.FrenchText_FT;
            }
        },
        getICAOItemA: function(desig)
        {
            return desig.match(/\d+/g) !== null ? "CXXX" : desig;
        },
        initUI: function()
        {
            // hide all optional input
            $("#mainTable .optionalContainer").hide();
            //thisNSD.subjectPushPin = nsdEditor.bingMap.addPushPin(
            //    proposal.subject.SubjectGeoRefLat,
            //    proposal.subject.SubjectGeoRefLong,
            //    {
            //        draggable: false,
            //        height: 50,
            //        width: 50,
            //        anchor: new Microsoft.Maps.Point(0, 50),
            //        icon: "/Images/BluePushpin.png"
            //    });


            // Setup Kendo widgets

            $("#sdoSubjectSelector").kendoDropDownList({
                optionLabel: RES.dbRes("Select SDO Subject..."),
                filter: "contains",
                dataTextField: "DisplayName",
                valuePrimitive: true,
                dataValueField: "Id",
                dataSource: {
                    type: "odata-v4",
                    pageSize: 20,
                    serverFiltering: true,                   
                    transport: {
                        read: {
                            url: "/odata/SdoSubjects?$top=30000&$select=Id, SdoEntityName,Designator,Name,SubjectGeo,SubjectGeoRefPoint &$expand=Parent($select=SdoEntityName,Designator,Name ) &$orderby=ParentId,SdoEntityName",
                            dataType: "json"
                        },
                        parameterMap: function (data, type) {
                            var ahpId = document.getElementById("sdoSubjectSelector").value;
                            if (type === "read" && !data.filter && ahpId) {
                                return { $filter: "Id eq '" + ahpId + "'" };
                            }

                            if (type === "read" && data.filter && data.filter.filters.length>0) {

                                var filterValue = data.filter.filters[0].value;
                                return { $filter: "not IsDeleted and (contains(SdoEntityName, '" + filterValue + "') or contains(Designator, '" + filterValue + "') or contains(Name, '" + filterValue + "') or (Parent ne null and (contains(Parent/SdoEntityName, '" + filterValue + "') or contains(Parent/Designator, '" + filterValue + "') or contains(Parent/Name, '" + filterValue + "')   ) ))" };
                            }
                        }
                    },
                    schema: {
                        data: function (data) {
                            return data["value"];
                        },
                        total: function (data) {
                            return data["odata.count"];
                        },
                        model: {
                            id: "Id"
                        },
                        parse: function (response) {
                            $.each(response.value, function (idx, elem) {
                                elem.DisplayName = (elem.Parent ? (">> @" + (elem.Parent.Parent ? elem.Parent.Parent.Designator : elem.Parent.Designator) + " " + elem.Designator) : (elem.Designator + " " + elem.Name)) + " (" + elem.SdoEntityName + ")";
                            });
                            return response;
                        }
                    },
                    change: function (e) {
                        thisNSD.handelLinkedSDOSubjectChange();
                    }
                },
                change: function (e) {
                    thisNSD.handelLinkedSDOSubjectChange();
                }
            });


            $("#subjectSelector").kendoDropDownList({
                optionLabel: RES.dbRes("Select Subject..."),
                filter: "contains",
                dataTextField: "DisplayName",
                valuePrimitive: true,
                cascadeFrom: "sdoSubjectSelector",
                cascadeFromField: "EntityCode",
                //template: '<span class="k-state-default">#: EntityCode # </span> <span class="k-state-default"> #: DisplayName # </span>',
                dataValueField: "Id",
                dataSource: {
                    type: "odata-v4",
                    pageSize: 20,
                    serverFiltering: true,
                    filter: {
                        logic: "or",
                        filters: [
                            { field: "Name", operator: "contains", value: $("#subjectsSelector").text() },
                            { field: "EntityCode", operator: "contains", value: $("#subjectsSelector").text() }
                        ]
                    },
                    transport: {
                        read: {
                            url: "/odata/IcaoSubjects",
                            dataType: "json"
                        },
                        parameterMap: function(data, type)
                        {
                            if (type === "read") {
                                var sdo = $("#sdoSubjectSelector").data("kendoDropDownList").dataSource.get($("#sdoSubjectSelector").data("kendoDropDownList").value());
                                var defaultFilter = sdo ? " EntityCode eq '" + sdo.SdoEntityName + "'" : "";
                                var filter = $.grep(data.filter.filters, function(n, i)
                                {
                                    return n.field === "DisplayName";
                                });
                                if (filter.length === 0)
                                {
                                    return defaultFilter !== "" ? { $filter: defaultFilter } : null;
                                }
                                return currentLanguage.startsWith("en") ?
                                    { $filter: (defaultFilter !== "" ? defaultFilter + " and " : "") + "contains(tolower(Name),'" + filter[0].value + "')" }
                                    :
                                    { $filter: (defaultFilter !== "" ? defaultFilter + " and " : "") + "contains(tolower(NameFrench),'" + filter[0].value + "')" };
                            }
                        }
                    },
                    schema: {
                        data: function(data)
                        {
                            return data["value"];
                        },
                        total: function(data)
                        {
                            return data["odata.count"];
                        },
                        model: {
                            id: "Id"
                        },
                        parse: function(response)
                        {
                            $.each(response.value, function(idx, elem)
                            {
                                elem.DisplayName = currentLanguage.startsWith("en") ? elem.EntityCode + " - " + elem.Name : elem.EntityCode + " - " + elem.NameFrench;
                            });
                            return response;
                        }
                    },
                    change: function(e) {
                        thisNSD.handelSubjectChange();
                    }
                },
                change: function(e)
                {
                    $("#conditionSelector").data("kendoDropDownList").value(null);
                    thisNSD.handelSubjectChange();
                },
                dataBound: function (e) {
                    if (proposal.ReadOnly) {
                        $("#subjectSelector").data("kendoDropDownList").enable(false);
                    }
                }
            });
            $("#conditionSelector").kendoDropDownList({
                optionLabel: RES.dbRes("Select Condition..."),
                cascadeFrom: "subjectSelector",
                cascadeFromField: "SubjectId",
                filter: "contains",
                dataTextField: "DisplayName",
                valuePrimitive: true,
                //template: '<span class="k-state-default">#: Code#</span> # if("en" === "'+currentLanguage+'"){ # <span class="k-state-default">#: Description#</span># } else { # <span class="k-state-default">#: DescriptionFrench # </span> # } #',
                dataValueField: "Id",
                dataSource: {
                    type: "odat-v4",
                    serverFiltering: true,
                    filter: {
                        logic: "or",
                        filters: [
                            { field: "Description", operator: "contains", value: $("#conditionSelector").text() },
                            { field: "Code", operator: "contains", value: $("#conditionSelector").text() }
                        ]
                    },
                    transport: {
                        read: {
                            url: function()
                            {
                                return "/odata/IcaoSubjects(" + $("#subjectSelector").val() + ")/Conditions";
                            },
                            dataType: "json"
                        },
                        parameterMap: function(data, type)
                        {
                            if (type === "read")
                            {
                                var filter = $.grep(data.filter.filters, function(n, i)
                                {
                                    return n.field === "DisplayName";
                                });
                                if (filter.length === 0)
                                {
                                    return { $filter: "CancelNotamOnly eq " + (proposal.PropsalType === "C" ? "true" : "false") + " and Active eq true" };
                                }
                                return currentLanguage.startsWith("en") ?
                                    { $filter: "CancelNotamOnly eq " + (proposal.PropsalType === "C" ? "true" : "false") + " and Active eq true and  (contains(tolower(Code),'" + filter[0].value + "') or contains(tolower(Description),'" + filter[0].value + "'))" }
                                    :
                                    { $filter: "CancelNotamOnly eq " + (proposal.PropsalType === "C" ? "true" : "false") + " and Active eq true and  (contains(tolower(Code),'" + filter[0].value + "') or contains(tolower(DescriptionFrench),'" + filter[0].value + "'))" };
                            }
                        }
                    },
                    schema: {
                        data: function(data)
                        {
                            return data["value"];
                        },
                        total: function(data)
                        {
                            return data["odata.count"];
                        },
                        model: {
                            id: "Id"
                        },
                        parse: function(response)
                        {
                            $.each(response.value, function(idx, elem)
                            {
                                elem.DisplayName = currentLanguage.startsWith("en") ? elem.Code + " - " + elem.Description : elem.Code + " - " + elem.DescriptionFrench;
                            });
                            return response;
                        }
                    },
                    change: function(e)
                    {
                        thisNSD.handelConditionChange();
                    }
                },
                change: function(e)
                {
                    thisNSD.handelConditionChange();
                },
                open: function(e)
                {
                    thisNSD.conditionChange = true;
                },
                dataBound: function(e)
                {
                    if (proposal.ReadOnly)
                    {
                        $("#conditionSelector").data("kendoDropDownList").enable(false);
                    }
                }
            });
            function validateItemFandG()
            {
                var f = parseInt($("#ItemF").val());
                var g = parseInt($("#ItemG").val());
              
                if (proposal.nsdTokens.ItemGUNL && f >= 0)
                {
                    return true;
                }
                if (proposal.nsdTokens.FMeasurementUnit === proposal.nsdTokens.GMeasurementUnit)
                {
                    return f <= g;
                }
                if (proposal.nsdTokens.ItemFSFC && (proposal.nsdTokens.ItemGUNL || g > 0))
                {
                    return true;
                }
                var lower = parseInt(proposal.nsdTokens.FMeasurementUnit) === enFandGMeasurementUnit.AMSL ? ~~((f) / 100) : f;
                var upper = parseInt(proposal.nsdTokens.GMeasurementUnit) === enFandGMeasurementUnit.AMSL ? ~~((g + 99) / 100) : g;
                return lower <= upper;
            };
            /*$("#mainTable").kendoTooltip({
                filter: "input"
            });*/
            $("#ItemA").kendoMaskedTextBox({
                mask: "~~~~ ~~~~ ~~~~ ~~~~ ~~~~",
                rules: {
                    "~": /[A-Z]/
                }
            });
            $("#subjectLocation").kendoMaskedTextBox({
                change: function()
                {
                    thisNSD.updateSubjectLocation();
                }
            });
            function updateLowerLimit(unit)
            {
                unit = parseInt($("input[name=FMeasurementUnit]:checked", "#mainTable").val());
                var lower = parseInt($("#ItemF").val());
                var cond = $("#conditionSelector").data("kendoDropDownList").dataItem();
                switch (unit)
                {
                    case enFandGMeasurementUnit.AMSL:
                        proposal.nsdTokens.set("LowerLimit", ~~((lower) / 100));
                        break;
                    case enFandGMeasurementUnit.AGL:
                        break;
                    case enFandGMeasurementUnit.FL:
                        proposal.nsdTokens.set("LowerLimit", lower);
                        break;
                }
            }
            $("input[name='FMeasurementUnit']").change(updateLowerLimit);
            $("#ItemF").kendoNumericTextBox({
                placeholder: RES.dbRes("Item F"),
                value: 0,
                format: "n0",
                decimals: 0,
                min: 1,
                max: 999999,
                spin: updateLowerLimit,
                change: updateLowerLimit
            });
            function updateUpperLimit()
            {
                unit = parseInt($("input[name=GMeasurementUnit]:checked", "#mainTable").val());
                var upper = parseInt($("#ItemG").val());
                var cond = $("#conditionSelector").data("kendoDropDownList").dataItem();
                switch (unit)
                {
                    case enFandGMeasurementUnit.AMSL:
                        proposal.nsdTokens.set("UpperLimit", ~~((upper + 99) / 100));
                        break;
                    case enFandGMeasurementUnit.AGL:
                        break;
                    case enFandGMeasurementUnit.FL:
                        proposal.nsdTokens.set("UpperLimit", upper);
                        break;
                }
            }
            $("input[name='GMeasurementUnit']").change(updateUpperLimit);
            $("#ItemG").kendoNumericTextBox({
                placeholder: RES.dbRes("Item G"),
                value: 0,
                format: "n0",
                decimals: 0,
                min: 1,
                max: 999999,
                spin: updateUpperLimit,
                change: updateUpperLimit
            });
            $("#Scope").kendoMaskedTextBox({
                mask: "~~",
                clearPromptChar: true,
                rules: {
                    "~": /[AEWK]/
                }
            });
            $("#Series").kendoMaskedTextBox({
                mask: "~",
                clearPromptChar: true,
                rules: {
                    "~": /[A-Z]/
                }
            });
            $("#Radius").kendoNumericTextBox({
                placeholder: RES.dbRes("Radius"),
                value: 0,
                format: "n0",
                decimals: 0,
                min: 1,
                max: 9999,
            });
            $("#LowerLimit").kendoNumericTextBox({
                placeholder: RES.dbRes("Lower Limit"),
                value: 0,
                format: "000",
                decimals: 0,
                min: 0,
                max: 999,
            });
            $("#UpperLimit").kendoNumericTextBox({
                placeholder: RES.dbRes("Upper Limit"),
                value: 999,
                format: "000",
                decimals: 0,
                min: 0,
                max: 999,
            });
            $("#btnResetLocation").on("click", function()
            {
                var sdo = $("#sdoSubjectSelector").data("kendoDropDownList").dataSource.get($("#sdoSubjectSelector").data("kendoDropDownList").value());
                if (sdo) {
                    var geoRefLocation = WKTModule.Read(sdo.SubjectGeoRefPoint.Geography.WellKnownText)._location;
                    proposal.nsdTokens.set("SubjectLocation", Util.decimalToDMS(geoRefLocation.latitude, "lat") + " " + Util.decimalToDMS(geoRefLocation.longitude, "long"));
                    thisNSD.updateSubjectLocation();
                }
            });
            $("#btnGetSeries").on("click", function()
            {
                var tmp = $("#Series").val();
                if (!thisNSD.getSeries(true))
                {
                    kendo.ui.ExtAlertDialog.show({
                        title: RES.dbRes("Data Required"),
                        message: RES.dbRes("Subject, Condition and Item A are required to calculate NOTAM Series."),
                        icon: "k-ext-warning"
                    });
                }
            });
            $("#itemFSFC").change(function()
            {
                if ($(this).is(":checked"))
                {
                    $(".ItemFValueContainer").hide();
                    proposal.nsdTokens.set("LowerLimit", 0);
                }
                else
                {
                    $(".ItemFValueContainer").show();
                }
            });
            $("#itemGUNL").change(function()
            {
                if ($(this).is(":checked"))
                {
                    $(".ItemGValueContainer").hide();
                    proposal.nsdTokens.set("UpperLimit", 999);
                }
                else
                {
                    $(".ItemGValueContainer").show();
                }
            });
            if (proposal.PropsalType === "C")
            {
                $(".hideOnCancelNOTAM").hide();
                $("#subjectSelector").data("kendoDropDownList").enable(false);
                /*                
                $("#ItemA").data("kendoMaskedTextBox").enable(false);
                $("#subjectLocation").data("kendoMaskedTextBox").enable(false);
                $("#btnResetLocation").prop("disabled", "disabled");
                $("input[name='FMeasurementUnit']").prop("disabled", "disabled");
                $("#ItemF").data("kendoNumericTextBox").enable(false);
                $("input[name='GMeasurementUnit']").prop("disabled", "disabled");
                $("#ItemG").data("kendoNumericTextBox").enable(false);
                $("#Scope").data("kendoMaskedTextBox").enable(false);
                $("#Series").data("kendoMaskedTextBox").enable(false);
                $("#Radius").data("kendoNumericTextBox").enable(false);
                $("#LowerLimit").data("kendoNumericTextBox").enable(false);
                $("#UpperLimit").data("kendoNumericTextBox").enable(false);
                $("#btnGetSeries").prop("disabled", "disabled");
                $("#itemFSFC").prop("disabled", "disabled");
                $("#itemGUNL").prop("disabled", "disabled");                    
                */
            }
            $("#mainTable").kendoValidator({
                rules: {
                    all: function(input) {
                        
                        //console.log(input.attr('name'));
                        //input.closest(".k-widget").find("input").removeClass("k-invalid")
                        var inputName = input.attr("name");
                        if (!inputName && input.hasClass("k-formatted-value") && input.siblings("input"))
                        {
                            inputName = $(input.siblings("input")[0]).attr("name");
                        }
                        switch (inputName)
                        {
                            case "sdoSubjectSelector":
                                {
                                    
                                    return thisNSD.setValidationError(function () {
                                        return $("#sdoSubjectSelector").data("kendoDropDownList").dataSource.get($("#sdoSubjectSelector").data("kendoDropDownList").value());
                                    }, RES.dbRes("Invalid Aeronautical Subject"));

                            }

                            case "subjectSelector":
                                if (thisNSD.setValidationError(function()
                                {
                                    return $("#subjectSelector").data("kendoDropDownList").dataItem().Id != "";
                                }, RES.dbRes("Invalid Subject")))
                                {
                                    return thisNSD.setValidationError(function()
                                    {
                                        return !$("#conditionSelector").prop("disabled");
                                    }, RES.dbRes("Invalid Condition"));
                                }
                                return false;
                            case "conditionSelector":
                                if ($("#subjectSelector").data("kendoDropDownList").dataItem().Id === "")
                                {
                                    return true;
                                }
                                var result = thisNSD.setValidationError(function()
                                {
                                    return $("#conditionSelector").data("kendoDropDownList").dataItem().Id != "";
                                }, RES.dbRes("Invalid Condition"));
                                return result;
                            case "purposeN":
                            case "purposeB":
                            case "purposeO":
                            case "purposeM":
                                if ($("#conditionSelector").data("kendoDropDownList").dataItem().Id === "")
                                {
                                    return true;
                                }
                                if (!$("#conditionSelector").data("kendoDropDownList").dataItem().AllowPurposeChange)
                                {
                                    return true;
                                }
                                return thisNSD.setValidationError(function()
                                {
                                    if (proposal.nsdTokens.PurposeN && proposal.nsdTokens.PurposeB && proposal.nsdTokens.PurposeO && proposal.nsdTokens.PurposeM)
                                    {
                                        return false;
                                    }
                                    if (!(proposal.nsdTokens.PurposeN || proposal.nsdTokens.PurposeB || proposal.nsdTokens.PurposeO || proposal.nsdTokens.PurposeM))
                                    {
                                        return false;
                                    }
                                    return true;
                                }, RES.dbRes("Invalid Purpose"));
                            case "ItemA":
                                var subject = $("#subjectSelector").data("kendoDropDownList").dataItem();
                                if (subject && subject.Code && subject.AllowItemAChange)
                                {
                                    return thisNSD.setValidationError(function()
                                    {
                                        return input.val().trim().length > 4;
                                    }, RES.dbRes("Invalid Item A"));
                                }
                                return true;
                            case "subjectLocation":
                                var subjectLocation = $("#subjectLocation").val();
                                if (thisNSD.setValidationError(function()
                                {
                                    return Util.IsValidLocation(subjectLocation);
                                }, RES.dbRes("Invalid Location")))
                                {
                                    return thisNSD.setValidationError(function()
                                    {
                                        return proposal.IsWithinDOA(subjectLocation);
                                    }, RES.dbRes("Subject is outside your Domain of Authority"));
                                }
                                return false;
                            case "conditionText":
                                if ($("#conditionSelector").data("kendoDropDownList").dataItem().Id === "")
                                {
                                    return true;
                                }
                                if (proposal.PropsalType === "C" && !thisNSD.setValidationError(function()
                                {
                                    return input.val() != thisNSD.initalNOTAMTextEnglish;
                                }, RES.dbRes("Condition Text should be updated to Cancel this NOTAM.")))
                                {
                                    return false;
                                }
                                if (thisNSD.setValidationError(function()
                                {
                                    return input.val().length > 0;
                                }, RES.dbRes("Text Condition is Required")))
                                {
                                    return thisNSD.setValidationError(function()
                                    {
                                        return Util.validateFreeTextInput(input.val().toUpperCase(), proposal.Setting);
                                    }, RES.dbRes("Invalid Condition Text"));
                                }
                                return false;
                            case "conditionTextFr":
                                if ($("#conditionSelector").data("kendoDropDownList").dataItem().Id === "")
                                {
                                    return true;
                                }
                                if (proposal.nsdTokens.Bilingual) {
                                    if (proposal.PropsalType === "C" && !thisNSD.setValidationError(function() {
                                        return input.val() != thisNSD.initalNOTAMTextFrench;
                                    }, RES.dbRes("French Condition Text should be updated to Cancel this NOTAM."))) {
                                        return false;
                                    }
                                    if (thisNSD.setValidationError(function() {
                                        return input.val().length > 0;
                                    }, RES.dbRes("French Text Condition is Required"))) {
                                        return thisNSD.setValidationError(function() {
                                            return Util.validateFreeTextInput(input.val().toUpperCase(), proposal.Setting);
                                        }, RES.dbRes("Invalid French Condition Text"));
                                    }
                                    return false;
                                } 
                                return true;
                            case "ItemF":
                                var cond = $("#conditionSelector").data("kendoDropDownList").dataItem();
                                if (!cond || !cond.AllowFgEntry || proposal.nsdTokens.ItemFSFC)
                                {
                                    return true;
                                }
                                return thisNSD.setValidationError(function()
                                {
                                    var f = parseInt($("#ItemF").val());                                    
                                    if (parseInt(proposal.nsdTokens.FMeasurementUnit) === enFandGMeasurementUnit.FL && f > 999) {
                                        return false;
                                    }
                                    return validateItemFandG();
                                }, RES.dbRes("Invalid Item F Value"));
                            case "ItemG":
                                var cond = $("#conditionSelector").data("kendoDropDownList").dataItem();
                                if (!cond.AllowFgEntry || proposal.nsdTokens.ItemGUNL)
                                {
                                    return true;
                                }
                                return thisNSD.setValidationError(function()
                                {
                                    var g = parseInt($("#ItemG").val());
                                    if (parseInt(proposal.nsdTokens.GMeasurementUnit) === enFandGMeasurementUnit.FL && g > 999) {
                                        return false;
                                    }

                                    return validateItemFandG();
                                }, RES.dbRes("Invalid Item G Value"));
                            case "Radius":
                                var cond = $("#conditionSelector").data("kendoDropDownList").dataItem();
                                if (!cond.Radius)
                                {
                                    return true;
                                }
                                var val = parseInt(input.val());
                                return thisNSD.setValidationError(function()
                                {
                                    return val >= cond.Radius;
                                }, RES.dbRes("Invalid Radius Value"));
                            case "LowerLimit":
                                var cond = $("#conditionSelector").data("kendoDropDownList").dataItem();
                                var lower = parseInt($("#LowerLimit").val());
                                var upper = parseInt($("#UpperLimit").val());
                                return thisNSD.setValidationError(function()
                                {
                                    return lower >= 0 && lower <= upper && upper <= 999;
                                }, cond.AllowFgEntry ? RES.dbRes("Invalid Item F Value") : RES.dbRes("Invalid Lower Limit Values"));
                            case "UpperLimit":
                                var cond = $("#conditionSelector").data("kendoDropDownList").dataItem();
                                var lower = parseInt($("#LowerLimit").val());
                                var upper = parseInt($("#UpperLimit").val());
                                return thisNSD.setValidationError(function()
                                {
                                    return lower >= 0 && lower <= upper && upper <= 999;
                                }, cond.AllowFgEntry ? RES.dbRes("Invalid Item G Value") : RES.dbRes("Invalid Upper Limit Values"));
                            case "Scope":
                                var subject = $("#subjectSelector").data("kendoDropDownList").dataItem();
                                if (!subject || !subject.Code || !subject.AllowScopeChange)
                                {
                                    return true;
                                }
                                var val = input.val().trim();
                                return thisNSD.setValidationError(function()
                                {
                                    return ["A", "E", "W", "K", "AE", "AW"].indexOf(val) > -1;
                                }, RES.dbRes("Invalid Scope"));
                            case "Series":
                                if ($("#conditionSelector").data("kendoDropDownList").dataItem().Id === "")
                                {
                                    return true;
                                }
                                var subject = $("#subjectSelector").data("kendoDropDownList").dataItem();
                                if (!subject || !subject.Code || !subject.AllowSeriesChange)
                                {
                                    return true;
                                }
                                var NOTAMUsedSeries = $.grep(proposal.Setting, function(e)
                                {
                                    return e.Name == "NOTAMUsedSeries";
                                })[0];
                                var val = input.val().trim();
                                if (NOTAMUsedSeries && NOTAMUsedSeries.Value && val.length === 1)
                                {
                                    return thisNSD.setValidationError(function()
                                    {
                                        return NOTAMUsedSeries.Value.indexOf(val) >= 0;
                                    }, RES.dbRes("Series not in Use"));
                                }
                                return thisNSD.setValidationError(function()
                                {
                                    return input.val().trim().length === 1;
                                }, RES.dbRes("Invalid Series"));
                        }
                        return true;
                    }
                },
                messages: {
                    all: function(input)
                    {
                        //input.closest(".k-widget").addClass("k-invalid");
                        return thisNSD.validationErrorMessage;
                    }
                }
            });
        },
        setValidationError: function(condition, error)
        {
            var result = condition();
            if (!result)
            {
                thisNSD.validationErrorMessage = error;
            }
            else
            {
                thisNSD.validationErrorMessage = "";
            }
            return result;
        },
        updateSubjectLocation: function() {
            var loc = proposal.nsdTokens.SubjectLocation;
            if (!Util.IsValidLocation(loc))
            {
                return;
            }
            var parts = Util.locationToDecimal(loc).split(" ");
            var location = new Microsoft.Maps.Location(parts[0], parts[1]);
            thisNSD.subjectPushPin.setLocation(location);
            nsdEditor.bingMap.centerMap(location);
        },
        getSeries: function(override)
        {
            var methodUrl = proposal.NSDURL + "/GetNotamSeries";
            var result = false;
            var subject = $("#subjectSelector").data("kendoDropDownList").dataItem();
            if (subject && subject.AllowSeriesChange && subject.Code && proposal.nsdTokens.ItemA.length >= 4 && (override || $("#Series").val().trim() == ""))
            {
                $.ajax({
                    url: methodUrl,
                    type: "POST",
                    data: JSON.stringify({ subjectCode: proposal.nsdTokens.ItemA.substring(0, 4), qCode: subject.Code }),
                    contentType: "application/json",
                    dataType: "json",
                    async: false,
                }).done(function(data)
                {
                    //proposal.nsdTokens.set("Series", data["Series"]);
                    result = true;
                }).fail(function(jqXHR, textStatus, err)
                {
                }).complete(function()
                {
                });
            }
            return result;
        },
        handelLinkedSDOSubjectChange: function () {
            thisNSD.conditionChange = true;

            nsdEditor.bingMap.userLayer.clear();
            nsdEditor.bingMap.displayLayer.clear();

            if (!proposal.ReadOnly && proposal.PropsalType === "N") {
                proposal.nsdTokens.set("ICAOSubjectConditionId", 0);
                proposal.nsdTokens.set("ICAOSubjectId", 0);
            }
            var sdo = $("#sdoSubjectSelector").data("kendoDropDownList").dataSource.get($("#sdoSubjectSelector").data("kendoDropDownList").value());
            if (sdo) {

                var ahpFir = sdo;
                while (!['Ahp', 'Fir'].includes(ahpFir.SdoEntityName))
                        ahpFir = ahpFir.Parent;

                proposal.nsdTokens.set("ItemA", thisNSD.getICAOItemA(ahpFir.Designator));

                //$("#subjectSectionIcon").attr("src", "/Images/yes.png");
                var shape = WKTModule.Read(sdo.SubjectGeo.Geography.WellKnownText);
                nsdEditor.bingMap.displayLayer.push(shape);
                var geoRefLocation = WKTModule.Read(sdo.SubjectGeoRefPoint.Geography.WellKnownText)._location;

                $.ajax({
                    url: '/NsdEditor/IsInBilingualRegion',
                    type: "GET",
                    data: {
                        latitud: geoRefLocation.latitude,
                        longitude: geoRefLocation.longitude
                    },
                    contentType: 'application/json',
                    dataType: 'json',
                    cache: false, // sometimes IE fails to make a proper request...
                    context: proposal
                    //async: false
                })
                .done(function (result) {
                    proposal.nsdTokens.set("Bilingual", result);
                });

                nsdEditor.bingMap.map.setView({ center: geoRefLocation, zoom: 13 });
                proposal.nsdTokens.set("SubjectLocation", Util.decimalToDMS(geoRefLocation.latitude, "lat") + " " + Util.decimalToDMS(geoRefLocation.longitude, "long"));

                thisNSD.subjectPushPin = nsdEditor.bingMap.addPushPin(
                    geoRefLocation.latitude,
                    geoRefLocation.longitude,
                    {
                        draggable: false,
                        height: 50,
                        width: 50,
                        anchor: new Microsoft.Maps.Point(0, 50),
                        icon: "/Images/BluePushpin.png"
                    });

                $("#subjectLocation").data("kendoMaskedTextBox").enable(true && !proposal.ReadOnly);
                $("#btnResetLocation").prop("disabled", "false"); 
            } else {
                //$("#subjectSectionIcon").attr("src", "/Images/no.png");
                $("#subjectLocation").data("kendoMaskedTextBox").enable(false);
                $("#btnResetLocation").prop("disabled", "disabled");
            }
        },
        handelSubjectChange: function() {
            thisNSD.conditionChange = true;

            //proposal.nsdTokens.unbind("change", thisNSD.validate);
            $("#mainTable .optionalContainer").hide();
            var subject = $("#subjectSelector").data("kendoDropDownList").dataSource.get(proposal.nsdTokens.ICAOSubjectId);
            if (!subject)
            {
                return;
            }
            if (subject.AllowItemAChange)
            {
                $("#mainTable .itemAContainer").show();
            }
            else
            {
                $("#mainTable .itemAContainer").hide();
                //proposal.nsdTokens.set("ItemA", "");
            }
            proposal.nsdTokens.set("Scope", subject.Scope);
            if (subject.AllowScopeChange)
            {
                $("#mainTable .ScopeContainer").show();
            }
            else
            {
                $("#mainTable .ScopeContainer").hide();
            }
            if (subject.AllowSeriesChange)
            {
                $("#mainTable .seriesContainer").show();
            }
            else
            {
                $("#mainTable .seriesContainer").hide();
                //proposal.nsdTokens.set("Series", "");
            }
        },
        handelConditionChange: function()
        {
            proposal.nsdTokens.unbind("change", thisNSD.validate);
            
            thisNSD.conditionChange = false;
            var subject = $("#subjectSelector").data("kendoDropDownList").dataSource.get(proposal.nsdTokens.ICAOSubjectId);
            var cond = $("#conditionSelector").data("kendoDropDownList").dataSource.get(proposal.nsdTokens.ICAOSubjectConditionId);
            if (!cond)
            {
                return;
            }
            if (cond.AllowFgEntry)
            {
                $("#mainTable .itemFGContainer").show();
                $("#LowerLimit").data("kendoNumericTextBox").enable(false);
                $("#UpperLimit").data("kendoNumericTextBox").enable(false);
            }
            else
            {
                $("#mainTable .itemFGContainer").hide();
                proposal.nsdTokens.set("ItemF", null);
                proposal.nsdTokens.set("ItemG", null);
                proposal.nsdTokens.set("ItemFSFC", false);
                proposal.nsdTokens.set("ItemGUNL", false);
                $("#LowerLimit").data("kendoNumericTextBox").enable(true && !proposal.ReadOnly);
                $("#UpperLimit").data("kendoNumericTextBox").enable(true && !proposal.ReadOnly);
            }
            if (cond && cond.Code)
            {
                if (cond.AllowPurposeChange)
                {
                    $("#mainTable .itemPurposeContainer").show();
                }
                else
                {
                    $("#mainTable .itemPurposeContainer").hide();
                }
                if (cond.I && cond.V)
                {
                    $("#mainTable .trafficContainer").show();
                }
                else
                {
                    $("#mainTable .trafficContainer").hide();
                }
                proposal.nsdTokens.set("PurposeN", cond.N);
                proposal.nsdTokens.set("PurposeB", cond.B);
                proposal.nsdTokens.set("PurposeO", cond.O);
                proposal.nsdTokens.set("PurposeM", cond.M);
                proposal.nsdTokens.PurposeM = cond.M;
                proposal.nsdTokens.set("Traffic", (!cond.I && !cond.V)?"K": (cond.I ? "I" : "") + (cond.V ? "V" : ""));
                proposal.nsdTokens.set("Scope", subject.Scope);
                proposal.nsdTokens.set("LowerLimit", Util.pad(cond.Lower, 3));
                proposal.nsdTokens.set("UpperLimit", Util.pad(cond.Upper, 3));
                proposal.nsdTokens.set("Radius", cond.Radius);
                //proposal.nsdTokens.trigger('change');
            }
            if (subject.AllowSeriesChange)
            {
                thisNSD.getSeries();
            }

            thisNSD.conditionChange = false;
            proposal.nsdTokens.bind("change", thisNSD.validate);
            if (!proposal.ReadOnly)
                proposal.nsdTokens.trigger("change");
        },
        isValidModel: function () {
            return proposal.nsdTokens.SdoEntityId &&
                (proposal.nsdTokens.ICAOSubjectId || 0) > 0  &&
                (proposal.nsdTokens.ICAOSubjectConditionId || 0) > 0 &&
                (proposal.nsdTokens.SetBeginDateAtSubmit || proposal.nsdTokens.StartValidity);
        },
        conditionChange: false,
        validate: function (e) {
            proposal.validModel = false;
            Util.setConditionValid(false);

            if (thisNSD.conditionChange)
                return;

            if (!formValidator.validate() || !thisNSD.isValidModel())
                return;

            if (proposal.nsdTokens.Bilingual === true) {
                $(".eFieldFrench").show();
            } else {
                $(".eFieldFrench").show();
            }

            var reviewTab = $("#reviewTab").kendoTabStrip().data("kendoTabStrip");
            reviewTab.enable(reviewTab.tabGroup.children().eq(1), false);

            proposal.generateNOTAM();
        }
    };
});