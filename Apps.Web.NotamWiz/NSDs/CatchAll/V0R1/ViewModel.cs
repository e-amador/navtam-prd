﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.CatchAll.V0R1
{
    public class ViewModel : NotamProposalViewModelBase
    {
      

        public string EnglishText_FT
        {
            get;
            set;
        }

        public enFandGMeasurementUnit FMeasurementUnit
        {
            get;
            set;
        }

        public string FrenchText_FT
        {
            get;
            set;
        }

        public enFandGMeasurementUnit GMeasurementUnit
        {
            get;
            set;
        }

        public long ICAOSubjectConditionId
        {
            get;
            set;
        }

        public long ICAOSubjectId
        {
            get;
            set;
        }

        public string ItemA
        {
            get;
            set;
        }

        public int? ItemF
        {
            get;
            set;
        }

        public bool ItemFSFC
        {
            get;
            set;
        }

        public int? ItemG
        {
            get;
            set;
        }

        public bool ItemGUNL
        {
            get;
            set;
        }

        public int LowerLimit
        {
            get;
            set;
        }

        public bool PurposeB
        {
            get;
            set;
        }

        public bool PurposeM
        {
            get;
            set;
        }

        public bool PurposeN
        {
            get;
            set;
        }

        public bool PurposeO
        {
            get;
            set;
        }

        public int Radius
        {
            get;
            set;
        }

        public string Scope
        {
            get;
            set;
        }

        public string Series
        {
            get;
            set;
        }

        public string SubjectLocation
        {
            get;
            set;
        }

        public string Traffic
        {
            get;
            set;
        }

        public int UpperLimit
        {
            get;
            set;
        }
    }

    public enum enFandGMeasurementUnit
    {
        AMSL,
        AGL,
        FL
    }
}