﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs
{
    public class NOTAMGenerationResult
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        public string ItemE { get; set; }
        public string ItemEFrench { get; set; }
    }
}