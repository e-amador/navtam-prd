﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.RwyClsd.V0R1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Code.Extentions;
    using Code.GeoTools;
    using Code.Helpers;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;

    public class RwyClsdController : NsdBaseController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(RwyClsdController));

        public RwyClsdController(IUow uow, IClusterQueues clusterQueues) : base(uow, clusterQueues)
        {
        }

        public override ActionResult Index(string id)
        {
            ViewBag.ClosureReasons = Json(GetClosureReasonsByCulture(Thread.CurrentThread.CurrentUICulture.Name));
            return View(new ViewModel());
        }

        protected override async Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase)
        {
            _log.Object(new
            {
                Method = "GenerateAsync",
                Parameters = new
                {
                    proposalType,
                    notamProposal,
                    notamProposalBase
                }
            });
            var proposal = notamProposalBase as ViewModel;
            if (proposal == null)
            {
                throw new NullReferenceException("notamProposalBase"); //TODO: What to do in this case?
            }
            notamProposal.IsCatchAll = false;
            notamProposal.ContainFreeText = proposal.ShowFreeText;
            notamProposal.Fir = Subject.Fir;
            notamProposal.Type = proposalType;
            notamProposal.Code23 = "MR";
            notamProposal.Code45 = "LC";
            notamProposal.Traffic = "IV";
            notamProposal.Purpose = "NBO";
            notamProposal.Scope = "A";
            notamProposal.Lower = 0;
            notamProposal.Upper = 999;
            notamProposal.Radius = 5;
            notamProposal.ItemA = GetItemA(Subject.Ad);

            var subjectParent = await Uow.SdoSubjectRepo.GetByDesignatorAsync(Subject.Ad);
            //TODO: Verify that runway is closest to the parent airport... 
            //var closestAeroDrome = await Uow.SdoSubjectRepo.GetClosestAeroDromeWithingAreaAsync(rwyLoc, 5);

            notamProposal.Series = GetNotamSeriesByGeoRegion(subjectParent.SubjectGeo, "MR");
            notamProposal.Coordinates = GeoDataService.DMSToRoundedDM(string.Format("{0} {1}", GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLat, true), GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLong, false)));

            if (proposalType == "C")
            {
                notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
                notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
                notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;

                notamProposal.Code45 = "AK";
                notamProposal.ItemE = GenerateItemEForCancellation(Subject.Designator, "en");
                notamProposal.ItemEFrench = proposal.Bilingual ? GenerateItemEForCancellation(Subject.Designator, "fr") : "";
                
                return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
            }
            
            notamProposal.ItemE = GenerateItemE(Subject.Designator, proposal, "en");
            notamProposal.ItemEFrench = proposal.Bilingual ? GenerateItemE(Subject.Designator, proposal, "fr") : "";

            return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
        }

        protected override async Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal)
        {
            return notamProposal;
        }

        protected override async Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase)
        {
            this._log.Object(new
            {
                Method = "ValidateAsync",
                Parameters = new
                {
                    notamProposalBase
                }
            });
            try
            {
                var validator = new RwyClsdValidator(this, Uow, DbRes);
                var validationResult = await validator.ValidateAsync(notamProposalBase as ViewModel);
                if (!validationResult.IsValid)
                {
                    var result = new ProcessResult
                    {
                        Success = false,
                        ErrorCode = "CAC-0001"
                    };
                    foreach (var failure in validationResult.Errors)
                    {
                        result.Errors.Add(failure.ErrorMessage);
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                this._log.Error(e);
            }
            return new ProcessResult();
        }

        private static string GenerateItemE(string rwyDesignator, ViewModel rwyClsdProposal, string language)
        {
            const string space = " ";
            var itemEStrbldr = new StringBuilder();
            itemEStrbldr.Append(rwyDesignator);
            itemEStrbldr.Append(space);
            itemEStrbldr.Append("CLSD");
            //reason
            itemEStrbldr.Append(space);
            if (rwyClsdProposal.Reason.Value != "0") // skip this part when no reason...
            {
                itemEStrbldr.Append(language == "en" ? "DUE" : "CAUSE");
                itemEStrbldr.Append(space);
                if (rwyClsdProposal.ShowFreeText)
                {
                    itemEStrbldr.Append(language == "en" ? rwyClsdProposal.FreeText : rwyClsdProposal.FreeTextFr);
                }
                else
                {
                    itemEStrbldr.Append(GetReasonText(rwyClsdProposal.Reason.Value, language));
                }
            }
            if (rwyClsdProposal.RdAvlSelectedValue < 3) // taxi availability option selected
            {
                switch (rwyClsdProposal.RdAvlSelectedValue)
                {
                    case 0: //full length
                        itemEStrbldr.Append(space);
                        itemEStrbldr.Append(language == "en" ? "AVBL AS TWY" : "AVBL COMME TWY");
                        break;
                    case 1: //partial length
                        itemEStrbldr.Append(space);
                        var from = language == "en" ? "AVBL AS TWY FM" : "AVBL COMME TWY DE";
                        var to = language == "en" ? "TO" : "A";
                        itemEStrbldr.Append(string.Format("{0} {1} {2} {3}", from, rwyClsdProposal.RangeFrom, to, rwyClsdProposal.RangeTo));
                        break;
                    case 2: //cardinal direction
                        if (rwyClsdProposal.RdDirSelectedValue != 0)
                        {
                            itemEStrbldr.Append(space);
                            var cardDirTxt = language == "en" ? string.Format("AVBL AS TWY {0} OF {1} ", GetCardinalPointText(rwyClsdProposal.RdDirSelectedValue, "en"), "XXXX") : string.Format("AVBL AS TWY {0} DE {1} ", GetCardinalPointText(rwyClsdProposal.RdDirSelectedValue, "fr"), "YYYY");
                            itemEStrbldr.Append(cardDirTxt);
                        }
                        break;
                }
                if (rwyClsdProposal.WingSpan.HasValue)
                {
                    itemEStrbldr.Append(space);
                    var wingSpanTxt = language == "en" ? "FOR ACFT WITH WING SPAN LESS THAN" : "POUR ACFT D’UNE ENVERGURE DE MOINS DE";
                    itemEStrbldr.Append(string.Format("{0} {1} FT", wingSpanTxt, rwyClsdProposal.WingSpan));
                }
                if (rwyClsdProposal.Weight.HasValue)
                {
                    itemEStrbldr.Append(space);
                    var weightTxt = language == "en" ? string.Format("FOR ACFT WITH WT LESS THAN {0} POUNDS", rwyClsdProposal.Weight) : string.Format("POUR ACFT D’UNE ENVERGURE DE MOINS DE {0} LIVRES", rwyClsdProposal.Weight);
                    itemEStrbldr.Append(weightTxt);
                }
            }
            return itemEStrbldr.ToString().Replace("  ", space);
        }

        private static string GenerateItemEForCancellation(string rwyDesignator, string language)
        {
            const string space = " ";
            var itemEStrbldr = new StringBuilder();
            itemEStrbldr.Append(space);
            itemEStrbldr.Append(rwyDesignator);
            itemEStrbldr.Append(space);
            itemEStrbldr.Append("OPN");
            return itemEStrbldr.ToString();
        }

        private static string GetCardinalPointText(int cardinalindex, string language)
        {
            //TODO: From resources
            switch (cardinalindex)
            {
                case 1:
                    return language == "en" ? "NORTH" : "NORD";
                case 2:
                    return language == "en" ? "SOUTH" : "SUD";
                case 3:
                    return language == "en" ? "EAST" : "EST";
                case 4:
                    return language == "en" ? "WEST" : "QUEST";
            }
            return "";
        }

        private static List<Reason> GetClosureReasonsByCulture(string language)
        {
            //TODO: From resources 
            return language != "fr" ? new List<Reason>
            {
                new Reason
                {
                    Text = "NO REASON",
                    Value = "0"
                },
                new Reason
                {
                    Text = "PAINTING",
                    Value = "1"
                },
                new Reason
                {
                    Text = "LINE PAINTING",
                    Value = "2"
                },
                new Reason
                {
                    Text = "CONST",
                    Value = "3"
                },
                new Reason
                {
                    Text = "MAINT",
                    Value = "4"
                },
                new Reason
                {
                    Text = "CRACK FILLING",
                    Value = "5"
                },
                new Reason
                {
                    Text = "RESURFACING",
                    Value = "6"
                },
                new Reason
                {
                    Text = "GRADING AND PACKING",
                    Value = "7"
                },
                new Reason
                {
                    Text = "DISABLED ACFT",
                    Value = "8"
                },
                new Reason
                {
                    Text = "OTHER",
                    Value = "-1"
                }
            } : new List<Reason>
            {
                new Reason
                {
                    Text = "NO REASON",
                    Value = "0"
                },
                new Reason
                {
                    Text = "PEINTURE",
                    Value = "1"
                },
                new Reason
                {
                    Text = "PEINTURE DE LIGNES",
                    Value = "2"
                },
                new Reason
                {
                    Text = "CONST",
                    Value = "3"
                },
                new Reason
                {
                    Text = "MAINT",
                    Value = "4"
                },
                new Reason
                {
                    Text = "COLMATAGE",
                    Value = "5"
                },
                new Reason
                {
                    Text = "SURACAGE DU REVETEMENT",
                    Value = "6"
                },
                new Reason
                {
                    Text = "NIVELAGE ET REMPLISSAGE",
                    Value = "7"
                },
                new Reason
                {
                    Text = "ACFT EN PANNE",
                    Value = "8"
                },
                new Reason
                {
                    Text = "OTHER",
                    Value = "-1"
                }
            };
        }

        private static string GetReasonText(string reasonValue, string language)
        {
            var reason = GetClosureReasonsByCulture(language).FirstOrDefault(e => e.Value == reasonValue);
            return reason != null ? reason.Text : "";
        }
    }
}