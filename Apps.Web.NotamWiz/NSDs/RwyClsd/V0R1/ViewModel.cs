namespace NavCanada.Applications.NotamWiz.Web.NSDs.RwyClsd.V0R1
{
    public class ViewModel : NotamProposalViewModelBase
    {
        public string DirFreeText
        {
            get;
            set;
        }

        public string DirFreeTextFr
        {
            get;
            set;
        }

        public string FreeText
        {
            get;
            set;
        }

        public string FreeTextFr
        {
            get;
            set;
        }

        public string RangeFrom
        {
            get;
            set;
        }

        public string RangeTo
        {
            get;
            set;
        }

        public int RdAvlSelectedValue
        {
            get;
            set;
        }

        public int RdDirSelectedValue
        {
            get;
            set;
        }

        public Reason Reason
        {
            get;
            set;
        }

        public string RwyDesgCode
        {
            get;
            set;
        }

        public bool ShowFreeText
        {
            get;
            set;
        }

        public int? Weight
        {
            get;
            set;
        }

        public int? WingSpan
        {
            get;
            set;
        }
    }
}