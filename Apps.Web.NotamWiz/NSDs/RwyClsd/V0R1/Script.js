﻿define(["jquery", "Resources", "utilities", "NSDFormManager", "kendo/kendo.dropdownlist.min", "kendo/kendo.validator.min", "kendoCustomDataBinders"], function ($, res, Util, nsdManager) {
    var resources = res.getResources("NSD-RwyClsd");
    var self;
    var vm = null;
    var kendoValidator = null;
    return {
        init: function (editor, model) {
            self = this;
            vm = model;
            self.closureReasons = $("#lookupData").data("reasons").Data;
            vm.createNOTAMViewModel($.extend($("#nsdModel").data("model"), {
                bilingual: vm.Options.Bilingual,
                rwyDesgCode: "",
                reason: self.closureReasons[0],
                freeText: "",
                freeTextFr: "",
                dirFreeText: "",
                dirFreeTextFr: "",
                showFreeText: false,
                rdAvlSelectedValue: 3,
                rdDirSelectedValue: 0,
                rangeFrom: null,
                rangeTo: null,
                wingSpan: null,
                weight: null,
                rdRwys: [0, 1, 2, 3],
                rdDirs: [0, 1, 2, 3, 4],
                clearAvailabilitySelection: function () {
                    vm.nsdTokens.set("rdAvlSelectedValue", 3);
                    this.clearRangeAndCardinal();
                    this.clearRestrictions();
                },
                clearRangeAndCardinal: function (rb) {
                    if (rb === undefined || rb.currentTarget.id === "rdRwy1" || rb.currentTarget.id === "rdRwy2") {
                        vm.nsdTokens.set("rangeFrom", null);
                        vm.nsdTokens.set("rangeTo", null);
                    }
                    vm.nsdTokens.set("rdDirSelectedValue", 0);
                    vm.nsdTokens.set("dirFreeText", "");
                    vm.nsdTokens.set("dirFreeTextFr", "");
                },
                clearRestrictions: function () {
                    vm.nsdTokens.set("wingSpan", null);
                    vm.nsdTokens.set("weight", null);
                },
                showTaxywayAvailabilitySection: function (index) {
                    return vm.nsdTokens.get("rdAvlSelectedValue") === index;
                },
                showDirFreeTextDesc: function () {
                    return vm.nsdTokens.get("rdDirSelectedValue") !== 0;
                },
                showTaxywayRestrictionsSection: function () {
                    return vm.nsdTokens.get("rdAvlSelectedValue") === "0" ||
                        vm.nsdTokens.get("rdAvlSelectedValue") === "1" ||
                        vm.nsdTokens.get("rdAvlSelectedValue") === "2";
                }
            }));
            Util.hidePermement();
            self.initUI(self.closureReasons);
            self.loadSdoData();
            kendoValidator = $("#frm-reason").data("kendoValidator");
            editor.conditionValidator = kendoValidator;
            //kendo.bind($('#rwy-clsd'), vm.nsdTokens);
            vm.nsdTokens.bind("change", self.validate);
        },
        initUI: function (reasons) {
            $("#cbx-reason").kendoDropDownList({
                dataTextField: "Text",
                dataValueField: "Value",
                dataSource: reasons,
                select: function (e) {
                    var item = this.dataItem(e.item);
                    vm.nsdTokens.set("showFreeText",
                        item.Value === self.closureReasons[self.closureReasons.length - 1].Value);
                    if (!vm.nsdTokens.showFreeText) {
                        vm.nsdTokens.set("freeText", "");
                        vm.nsdTokens.set("freeTextFr", "");
                    }
                },
                index: 0
            });
            $("#ac-wingspan").kendoNumericTextBox({
                value: 0,
                format: "n0",
                decimals: 0,
                min: 0,
                max: 9999
            });
            $("#ac-weight").kendoNumericTextBox({
                value: 0,
                format: "n0",
                decimals: 0,
                min: 0,
                max: 9999999
            });
            $("#ac-wingspan").data("kendoNumericTextBox").wrapper.width("100px");
            $("#ac-weight").data("kendoNumericTextBox").wrapper.width("100px");
            $("#frm-reason").kendoValidator({
                messages: {
                    freeTextRequired: "Description in English required.",
                    freeTextFrRequired: "Description in French required."
                },
                rules: {
                    freeTextRequired: function (input) {
                        if (input.is("[name=free-text]")) {
                            if (vm.nsdTokens.get("showFreeText")) {
                                return input.val() !== "";
                            }
                        }
                        return true;
                    },
                    freeTextFrRequired: function (input) {
                        if (input.is("[name=free-text-fr]")) {
                            if (vm.nsdTokens.get("showFreeText")) {
                                return input.val() !== "";
                            }
                        }
                        return true;
                    }
                },
            });
        },
        loadSdoData: function () {
            vm.nsdTokens.set("rwyDesgCode", vm.subject.Designator.split("-")[1]);
            vm.NOTAM.set("ItemA", vm.subject.Designator.match(/\d+/g) !== null ? "CXXX" : vm.subject.Designator);
        },
        validate: function (e) {
            vm.validModel = kendoValidator.validate();
            vm.generateNOTAM("/NSD/RwyClsd/V0R1/GenerateNotam");
        }
    };
});