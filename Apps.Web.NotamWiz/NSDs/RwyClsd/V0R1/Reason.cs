﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.RwyClsd.V0R1
{
    public class Reason
    {
        public string Text
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }
    }
}