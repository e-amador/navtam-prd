﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.TwyClsd.V0R1
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Code.Extentions;
    using Code.GeoTools;
    using Code.Helpers;
    using Models;
    using NSDs;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;

    public class TwyClsdController : NsdBaseController
    {
        private readonly ClosureReasons _closureReasons = new ClosureReasons();
        private readonly ILog _log = LogManager.GetLogger(typeof(TwyClsdController));

        public TwyClsdController(IUow uow, IClusterQueues clusterQueues)
            : base(uow, clusterQueues)
        {
        }

        public ActionResult GetAvailableTaxiways([DataSourceRequest] DataSourceRequest request, NotamSubjectViewModel subject)
        {
            _log.Object(new
            {
                Method = "GetAvailableTaxiways",
                Parameters = new
                {
                    request,
                    subject
                }
            });
            var list = Uow.SdoSubjectRepo.GetAllByEntityNameAndMid("Twy", subject.SubjectMid).Select(item => new
            {
                Value = item.Id,
                Text = item.Designator
            }).ToList();
            list.Insert(0, new
            {
                Value = "",
                Text = Thread.CurrentThread.CurrentUICulture.Name == "fr" ? "TOUTES TWY" : "ALL TWY"
            });
            return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public override ActionResult Index(string id)
        {
            TempData["ClosureReasons"] = _closureReasons[Thread.CurrentThread.CurrentUICulture.Name].Select(item => new
            {
                Value = item.Key,
                Text = item.Value.Name
            }).ToList();
            ViewBag.ClosureReasons = Json(TempData["ClosureReasons"]);
            return View(new ViewModel
            {
                ClosedTaxiways = new List<ClosedTaxiway>()
            });
        }

        protected override async Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase)
        {
            _log.Object(new
            {
                Method = "GenerateAsync",
                Parameters = new
                {
                    proposalType,
                    notamProposal,
                    notamProposalBase
                }
            });
            var proposal = notamProposalBase as ViewModel;
            if (proposal != null)
            {
                notamProposal.IsCatchAll = false;
                notamProposal.ContainFreeText = proposal.IsOtherClosureReason;
                notamProposal.Type = proposalType;
                if (proposalType == "C")
                {
                    notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
                    notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
                    notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;
                    
                    notamProposal.Code45 = "AK";
                    string itemETwyDesig;
                    string itemETwyDesigFrench;
                    BuildTwyDesig(proposal.ClosedTaxiways, out itemETwyDesig, out itemETwyDesigFrench);
                    notamProposal.ItemE = string.Format("TWY {0} OPN", itemETwyDesig);
                    notamProposal.ItemEFrench = proposal.Bilingual ? string.Format("TWY {0} OPN", itemETwyDesigFrench) : string.Empty;
                }
                else
                {
                    notamProposal.Code45 = "LC";
                    string itemETwyDesig;
                    string itemETwyDesigFrench;
                    var itemETwyDesigPartialClosure = string.Empty;
                    var itemETwyDesigPartialClosureFrench = string.Empty;
                    var itemEReason = string.Empty;
                    var itemEReasonFrench = string.Empty;
                    var allTwyClosedTaxiway = proposal.ClosedTaxiways.FirstOrDefault(closedTaxiway => closedTaxiway.TaxiwayId == "");
                    if (allTwyClosedTaxiway != null)
                    {
                        itemETwyDesig = "ALL TWY CLSD";
                        itemETwyDesigFrench = "TOUTES TWY CLSD";
                        if (allTwyClosedTaxiway.ClosureType == 1)
                        {
                            itemETwyDesig = string.Format("{0} BTN {1} AND {2} CLSD", itemETwyDesig, allTwyClosedTaxiway.Point1, allTwyClosedTaxiway.Point2);
                            itemETwyDesigFrench = string.Format("{0} BTN {1} ET {2} CLSD", itemETwyDesigFrench, allTwyClosedTaxiway.Point1French, allTwyClosedTaxiway.Point2French);
                        }
                    }
                    else
                    {
                        var closedTaxiways = new Collection<ClosedTaxiway>();
                        var partiallyClosedTaxiways = new Collection<ClosedTaxiway>();
                        foreach (var closedTaxiway in proposal.ClosedTaxiways)
                        {
                            switch (closedTaxiway.ClosureType)
                            {
                                case 0:
                                    closedTaxiways.Add(closedTaxiway);
                                    break;
                                case 1:
                                    partiallyClosedTaxiways.Add(closedTaxiway);
                                    break;
                            }
                        }
                        BuildTwyDesig(closedTaxiways, out itemETwyDesig, out itemETwyDesigFrench);
                        BuildTwyDesigPartialClosure(partiallyClosedTaxiways, out itemETwyDesigPartialClosure, out itemETwyDesigPartialClosureFrench);
                        if (!string.IsNullOrEmpty(itemETwyDesig))
                        {
                            itemETwyDesig = string.Format("TWY {0} CLSD", itemETwyDesig);
                            itemETwyDesigFrench = string.Format("TWY {0} CLSD", itemETwyDesigFrench);
                        }
                    }
                    if (proposal.ClosureReason > 0)
                    {
                        if (proposal.IsOtherClosureReason)
                        {
                            itemEReason = string.Format(" DUE {0}", proposal.ClosureReasonText);
                            itemEReasonFrench = string.Format(" CAUSE {0}", proposal.ClosureReasonTextFrench);
                        }
                        else
                        {
                            itemEReason = string.Format(" DUE {0}", _closureReasons["en"][proposal.ClosureReason].EFieldName);
                            itemEReasonFrench = string.Format(" CAUSE {0}", _closureReasons["fr"][proposal.ClosureReason].EFieldName);
                        }
                    }
                    notamProposal.ItemE = string.Format("{0}{1}{2}.", itemETwyDesig, itemETwyDesigPartialClosure, itemEReason).Trim();
                    notamProposal.ItemEFrench = string.Format("{0}{1}{2}.", itemETwyDesigFrench, itemETwyDesigPartialClosureFrench, itemEReasonFrench).Trim();
                }
                var subjectParent = await Uow.SdoSubjectRepo.GetByDesignatorAsync(Subject.Ad);
                notamProposal.Type = proposalType;
                notamProposal.Code23 = "MX";
                notamProposal.Traffic = "IV";
                notamProposal.Purpose = "M";
                notamProposal.Scope = "A";
                notamProposal.Lower = 0;
                notamProposal.Upper = 999;
                notamProposal.Radius = 5;
                notamProposal.Series = GetNotamSeriesByGeoRegion(subjectParent.SubjectGeo, "MX");
                notamProposal.Fir = Subject.Fir;
                notamProposal.Coordinates = GeoDataService.DMSToRoundedDM(string.Format("{0} {1}", GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLat, true), GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLong, false)));
                notamProposal.ItemA = GetItemA(Subject.Ad);
            }
            return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
        }

        protected override async Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal)
        {
            return notamProposal;
        }

        protected override async Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase)
        {
            _log.Object(new
            {
                Method = "ValidateAsync",
                Parameters = new
                {
                    notamProposalBase
                }
            });
            try
            {
                var validator = new TwyClsdValidator(this);
                var validationResult = await validator.ValidateAsync(notamProposalBase as ViewModel);
                if (!validationResult.IsValid)
                {
                    var result = new ProcessResult
                    {
                        Success = false,
                        ErrorCode = "CAC-0001"
                    };
                    foreach (var failure in validationResult.Errors)
                    {
                        result.Errors.Add(failure.ErrorMessage);
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                _log.Error(e);
            }
            return new ProcessResult();
        }

        private static void BuildTwyDesig(IList<ClosedTaxiway> closedTaxiways, out string twyDesig, out string twyDesigFrench)
        {
            twyDesig = string.Empty;
            twyDesigFrench = string.Empty;
            if (closedTaxiways != null && closedTaxiways.Count > 0)
            {
                twyDesig = closedTaxiways[0].TaxiwayDesignator;
                twyDesigFrench = closedTaxiways[0].TaxiwayDesignator;
                for (var i = 1; i < closedTaxiways.Count; i++)
                {
                    if (i == closedTaxiways.Count - 1)
                    {
                        twyDesig = string.Format("{0} AND {1}", twyDesig, closedTaxiways[i].TaxiwayDesignator);
                        twyDesigFrench = string.Format("{0} ET {1}", twyDesigFrench, closedTaxiways[i].TaxiwayDesignator);
                    }
                    else
                    {
                        twyDesig = string.Format("{0}, {1}", twyDesig, closedTaxiways[i].TaxiwayDesignator);
                        twyDesigFrench = string.Format("{0}, {1}", twyDesigFrench, closedTaxiways[i].TaxiwayDesignator);
                    }
                }
            }
        }

        private static void BuildTwyDesigPartialClosure(ICollection<ClosedTaxiway> closedTaxiways, out string twyDesigPartialClosure, out string twyDesigPartialClosureFrench)
        {
            twyDesigPartialClosure = string.Empty;
            twyDesigPartialClosureFrench = string.Empty;
            if (closedTaxiways != null && closedTaxiways.Count > 0)
            {
                foreach (var closedTaxiway in closedTaxiways)
                {
                    twyDesigPartialClosure = string.Format("{0}\nTWY {1} BTN {2} AND {3} CLSD", twyDesigPartialClosure, closedTaxiway.TaxiwayDesignator, closedTaxiway.Point1, closedTaxiway.Point2);
                    twyDesigPartialClosureFrench = string.Format("{0}\nTWY {1} BTN {2} ET {3} CLSD", twyDesigPartialClosureFrench, closedTaxiway.TaxiwayDesignator, closedTaxiway.Point1French, closedTaxiway.Point2French);
                }
            }
        }

        private class ClosureReason
        {
            public ClosureReason(string name, string eFieldName)
            {
                Name = name;
                EFieldName = eFieldName;
            }

            public string EFieldName
            {
                get;
                private set;
            }

            public string Name
            {
                get;
                private set;
            }
        }

        private class ClosureReasons
        {
            private readonly Dictionary<string, Dictionary<int, ClosureReason>> dictionary = new Dictionary<string, Dictionary<int, ClosureReason>>
            {
                { "en", new Dictionary<int, ClosureReason>
                {
                    { 0, new ClosureReason("No Reason", "") },
                    { 1, new ClosureReason("Painting", "PAINTING") },
                    { 2, new ClosureReason("Line Painting", "LINE PAINTING") },
                    { 3, new ClosureReason("Const", "CONST") },
                    { 4, new ClosureReason("Maint", "MAINT") },
                    { 5, new ClosureReason("Crack Filling", "CRACK FILLING") },
                    { 6, new ClosureReason("Resurfacing", "RESURFACING") },
                    { 7, new ClosureReason("Grading and Packing", "GRADING AND PACKING") },
                    { 8, new ClosureReason("Disabled Acft", "DISABLED ACFT") },
                    { 9, new ClosureReason("Other", "") }
                } },
                { "fr", new Dictionary<int, ClosureReason>
                {
                    { 0, new ClosureReason("No Reason", "") },
                    { 1, new ClosureReason("Peinture", "PEINTURE") },
                    { 2, new ClosureReason("Peinture de lignes", "PEINTURE DE LIGNES") },
                    { 3, new ClosureReason("Const", "CONST") },
                    { 4, new ClosureReason("Maint", "MAINT") },
                    { 5, new ClosureReason("Colmatage", "COLMATAGE") },
                    { 6, new ClosureReason("Suracage du revetement", "SURACAGE DU REVETEMENT") },
                    { 7, new ClosureReason("Nivelage et remplissage", "NIVELAGE ET REMPLISSAGE") },
                    { 8, new ClosureReason("Acft en panne", "ACFT EN PANNE") },
                    { 9, new ClosureReason("Other", "") }
                } }
            };

            public Dictionary<int, ClosureReason> this[string language]
            {
                get
                {
                    return dictionary[language];
                }
            }
        }
    }
}