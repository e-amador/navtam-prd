﻿define(["jquery", "Resources", "utilities", "mapping", "jsonpath", "kendoExt", "kendoCustomDataBinders", "javaExt"], function ($, res, Util) {
    var resources = res.getResources("NSD-TwyClsd");
    var viewModel = null;
    var self = null;
    var conditionValidator = null;
    var taxiwaysConditionValidator = null;
    var nsdEditor = null;
    return {
        validationErrorMessage: null,
        init: function (editor, model) {
            self = this;
            nsdEditor = editor;
            viewModel = model;
            self.closureReasons = $("#nsdData").data("closurereasons").Data;
            viewModel.createNOTAMViewModel($.extend($("#nsdModel").data("model"), {
                Bilingual: viewModel.Options.Bilingual,
                TaxiwayId: viewModel.subject.SubjectId,
                ClosureType: 0,
                ClosureReason: self.closureReasons[0].Value
            }));
            self.initUI(self.closureReasons);
            self.setupUpNSDEditor();
            taxiwaysConditionValidator = $("#taxiwaysConditionContainer").data("kendoValidator");
            conditionValidator = $("#nsdConditionContainer").data("kendoValidator");
            editor.conditionValidator = conditionValidator;
        },
        initUI: function (closureReasonsDataSource) {
            self.obstPushPin = nsdEditor.bingMap.addPushPin(
                viewModel.subject.SubjectGeoRefLat,
                viewModel.subject.SubjectGeoRefLong,
                {
                    draggable: false,
                    height: 50,
                    width: 50,
                    anchor: new Microsoft.Maps.Point(0, 50),
                    icon: "/Images/BluePushpin.png"
                });
            $("#availableTaxiways").kendoDropDownList(
                {
                    dataSource: {
                        schema: {
                            data: function (data) {
                                return data.Data;
                            }
                        },
                        transport: {
                            read: {
                                url: "TwyClsd/GetAvailableTaxiways",
                                dataType: "json",
                                type: "POST",
                                data: viewModel.subject
                            }
                        }
                    },
                    dataTextField: "Text",
                    dataValueField: "Value",
                    select: function (e) {
                        var item = this.dataItem(e.item);
                        viewModel.nsdTokens.set("AllTaxiwaysSelected",
                            item.Value === "");
                        $("#closureTypeFull").prop("checked", true).change();
                    },
                    dataBound: function (e) {
                        if (viewModel.nsdTokens.get("ClosedTaxiways").length < 1) {
                            self.validateAddTaxiway();
                        }
                    },
                    index: 0
                }
            );
            $("input[name=closureTypes]").change(function (e) {
                var isPartialClosure = $(this).val() === "1";
                viewModel.nsdTokens.set("IsPartialClosure", isPartialClosure);
                if (!isPartialClosure) {
                    viewModel.nsdTokens.set("Point1", "");
                    viewModel.nsdTokens.set("Point1French", "");
                    viewModel.nsdTokens.set("Point2", "");
                    viewModel.nsdTokens.set("Point2French", "");
                }
                taxiwaysConditionValidator.validate();
            });
            $("#addTaxiway").click(function (e) {
                e.preventDefault();
                self.validateAddTaxiway();
            });
            $("#closedTaxiways").kendoGrid(
                {
                    dataSource: {
                        data: viewModel.nsdTokens.get("ClosedTaxiways")
                    },
                    dataBound: function (e) {
                        $(".k-grid-" + resources.dbRes("Remove") + "").find("span").addClass("k-icon k-delete");
                    },
                    scrollable: false,
                    columns: [
                        { field: "TaxiwayDesignator", title: resources.dbRes("Taxiway"), width: "15%" },
                        { field: "ClosureTypeName", title: resources.dbRes("Closure Type"), width: "15%" },
                        {
                            template: function (dataItem) {
                                if (dataItem.IsPartialClosure) {
                                    if (viewModel.nsdTokens.get("Bilingual")) {
                                        return kendo.template($("#closedTaxiwaysPointsBilingualTemplate").html())(dataItem);
                                    }
                                    return kendo.template($("#closedTaxiwaysPointsTemplate").html())(dataItem);
                                }
                                return "";
                            },
                            width: "55%"
                        },
                        {
                            command: {
                                text: resources.dbRes("Remove"),
                                click: function (e) {
                                    e.preventDefault();
                                    self.validateRemoveTaxiway(this.dataItem($(e.currentTarget).closest("tr")));
                                }
                            },
                            title: "&nbsp;"
                        }
                    ]
                }
            );
            $("#closureReason").kendoDropDownList({
                dataSource: closureReasonsDataSource,
                dataTextField: "Text",
                dataValueField: "Value",
                select: function (e) {
                    var item = this.dataItem(e.item);
                    var closureReason = item.Value;
                    viewModel.nsdTokens.set("ClosureReason", closureReason);
                    var isOtherClosureReason = closureReason === self.closureReasons[self.closureReasons.length - 1].Value;
                    viewModel.nsdTokens.set("IsOtherClosureReason", isOtherClosureReason);
                    if (!isOtherClosureReason) {
                        viewModel.nsdTokens.set("ClosureReasonText", "");
                        viewModel.nsdTokens.set("ClosureReasonTextFrench", "");
                    }
                    self.validate();
                },
                index: 0
            });
            $("input[name=closureReasonText]").change(function (e) {
                self.validate();
            });
            $("input[name=closureReasonTextFrench]").change(function (e) {
                self.validate();
            });
            $("#taxiwaysConditionContainer").kendoValidator({
                rules: {
                    point1Required: function (input) {
                        if (input.is("[name=point1]")) {
                            if (viewModel.nsdTokens.get("IsPartialClosure")) {
                                return input.val().trim() !== "";
                            }
                        }
                        return true;
                    },
                    point2Required: function (input) {
                        if (input.is("[name=point2]")) {
                            if (viewModel.nsdTokens.get("IsPartialClosure")) {
                                return input.val().trim() !== "";
                            }
                        }
                        return true;
                    },
                    point1FrenchRequired: function (input) {
                        if (input.is("[name=point1French]")) {
                            if (viewModel.nsdTokens.get("IsPartialClosure")) {
                                if (viewModel.nsdTokens.get("Bilingual")) {
                                    return input.val().trim() !== "";
                                }
                            }
                        }
                        return true;
                    },
                    point2FrenchRequired: function (input) {
                        if (input.is("[name=point2French]")) {
                            if (viewModel.nsdTokens.get("IsPartialClosure")) {
                                if (viewModel.nsdTokens.get("Bilingual")) {
                                    return input.val().trim() !== "";
                                }
                            }
                        }
                        return true;
                    }
                },
                messages: {
                    all: function () {
                        return self.validationErrorMessage;
                    },
                    point1Required: resources.dbRes("Point 1 is required."),
                    point1FrenchRequired: resources.dbRes("Point 1 (Fr) is required."),
                    point2Required: resources.dbRes("Point 2 is required."),
                    point2FrenchRequired: resources.dbRes("Point 2 (Fr) is required."),
                }
            });
            $("#nsdConditionContainer").kendoValidator({
                rules: {
                    closedTaxiwaysRequired: function (input) {
                        if (input.is("[name=hasClosedTaxiways]")) {
                            var valid = input.val() === "true";
                            if (valid) {
                                $("#closedTaxiways").removeClass("invalid");
                            }
                            else {
                                $("#closedTaxiways").addClass("invalid");
                            }
                            return valid;
                        }
                        return true;
                    },
                    closureReasonText: function (input) {
                        if (input.is("[name=closureReasonText]")) {
                            if (viewModel.nsdTokens.get("IsOtherClosureReason")) {
                                return input.val().trim() !== "";
                            }
                        }
                        return true;
                    },
                    closureReasonTextFrench: function (input) {
                        if (input.is("[name=closureReasonTextFrench]")) {
                            if (viewModel.nsdTokens.get("IsOtherClosureReason")) {
                                if (viewModel.nsdTokens.get("Bilingual")) {
                                    return input.val().trim() !== "";
                                }
                            }
                        }
                        return true;
                    }
                },
                messages: {
                    all: function () {
                        return self.validationErrorMessage;
                    },
                    closedTaxiwaysRequired: resources.dbRes("Please select a taxiway to close."),
                    closureReasonText: resources.dbRes("Closure Reason Text is required."),
                    closureReasonTextFrench: resources.dbRes("Closure Reason Text (Fr) is required.")
                }
            });
        },
        setupUpNSDEditor: function () {
            Util.hidePermement();
        },
        addTaxiway: function () {
            var availableTaxiwaysDropDownList = $("#availableTaxiways").data("kendoDropDownList");
            var taxiwayId = availableTaxiwaysDropDownList.value();
            var taxiwayDesignator = availableTaxiwaysDropDownList.text();
            var selectedClosureType = $("input[name=closureTypes]:checked");
            var closureType = selectedClosureType.val();
            var closureTypeName = $("label[for=" + selectedClosureType.attr("id") + "]").text().trim();
            var isPartialClosure = closureType === "1";
            var point1 = $("#point1").val();
            var point1French = $("#point1French").val();
            var point2 = $("#point2").val();
            var point2French = $("#point2French").val();
            var reset = false;
            var update = false;
            var closedTaxiways = viewModel.nsdTokens.get("ClosedTaxiways");
            if (taxiwayId === "") {
                reset = true;
            }
            else {
                for (var i = 0; i < closedTaxiways.length; i++) {
                    if (closedTaxiways[i].TaxiwayId === "") {
                        reset = true;
                        break;
                    }
                    else {
                        if (closedTaxiways[i].TaxiwayId === taxiwayId) {
                            closedTaxiways[i].ClosureType = closureType;
                            closedTaxiways[i].ClosureTypeName = closureTypeName;
                            closedTaxiways[i].IsPartialClosure = isPartialClosure;
                            closedTaxiways[i].Point1 = point1;
                            closedTaxiways[i].Point1French = point1French;
                            closedTaxiways[i].Point2 = point2;
                            closedTaxiways[i].Point2French = point2French;
                            update = true;
                            break;
                        }
                    }
                }
            }
            if (reset) {
                closedTaxiways = [];
            }
            if (!update) {
                closedTaxiways.push({
                    TaxiwayId: taxiwayId,
                    TaxiwayDesignator: taxiwayDesignator,
                    ClosureType: closureType,
                    ClosureTypeName: closureTypeName,
                    IsPartialClosure: isPartialClosure,
                    Point1: point1,
                    Point1French: point1French,
                    Point2: point2,
                    Point2French: point2French
                });
            }
            self.refreshTaxiwayConditions(closedTaxiways);
        },
        removeTaxiway: function (item) {
            var closedTaxiways = viewModel.nsdTokens.get("ClosedTaxiways");
            for (var i = closedTaxiways.length - 1; i >= 0; i--) {
                if (closedTaxiways[i].TaxiwayId === item.TaxiwayId) {
                    closedTaxiways.splice(i, 1);
                    break;
                }
            }
            self.refreshTaxiwayConditions(closedTaxiways);
        },
        refreshTaxiwayConditions: function (closedTaxiways) {
            var closedTaxiwaysGrid = $("#closedTaxiways").data("kendoGrid");
            closedTaxiwaysGrid.dataSource.data(closedTaxiways);
            closedTaxiwaysGrid.refresh();
            viewModel.nsdTokens.set("TaxiwayId", viewModel.subject.SubjectId);
            viewModel.nsdTokens.set("TaxiwayDesignator", "");
            viewModel.nsdTokens.set("AllTaxiwaysSelected", false);
            viewModel.nsdTokens.set("ClosureType", 0);
            viewModel.nsdTokens.set("IsPartialClosure", false);
            viewModel.nsdTokens.set("Point1", "");
            viewModel.nsdTokens.set("Point1French", "");
            viewModel.nsdTokens.set("Point2", "");
            viewModel.nsdTokens.set("Point2French", "");
            viewModel.nsdTokens.set("ClosedTaxiways", closedTaxiways);
            viewModel.nsdTokens.set("HasClosedTaxiways", closedTaxiways.length > 0);
        },
        validate: function () {
            viewModel.validModel = false;
            Util.setConditionValid(false);
            if (conditionValidator.validate()) {
                viewModel.generateNOTAMCheck(false);
            }
        },
        validateAddTaxiway: function () {
            if (taxiwaysConditionValidator.validate()) {
                self.addTaxiway();
                self.validate();
            }
        },
        validateRemoveTaxiway: function (item) {
            if (taxiwaysConditionValidator.validate()) {
                self.removeTaxiway(item);
                self.validate();
            }
        }
    };
});