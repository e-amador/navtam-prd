﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.Obst.V0R1
{
    public class ViewModel : NotamProposalViewModelBase
    {
        public string ItemA
        {
            get;
            set;
        }

        public int Elevation
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public string Lighted
        {
            get;
            set;
        }

        public string Location
        {
            get;
            set;
        }

        public string ObstacleType
        {
            get;
            set;
        }

        public string Painted
        {
            get;
            set;
        }
    }
}