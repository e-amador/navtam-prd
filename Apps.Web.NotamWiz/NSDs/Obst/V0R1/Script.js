﻿define(["jquery", "Resources", "utilities", "mapping", "jsonpath", "kendoExt", "kendoCustomDataBinders", "javaExt"], function ($, res, Util)
{
    var RES = res.getResources("NSD-Obst");
    var viewModel = null;
    var self = null;
    var conditionValidator = null;
    var obstacleTypes = $("#nsdData").data("obstacletypes");
    var nsdEditor = null;

    return {
        obstPushPin: null,
        validationErrorMessage: null,
        init: function(editor, model)
        {
            self = this;
            nsdEditor = editor;
            viewModel = model;
            viewModel.createNOTAMViewModel($.extend($("#nsdModel").data("model"), {
                Bilingual: viewModel.Options.Bilingual,
                ObstacleType: "",
                //Location: Util.decimalToDMS(viewModel.subject.SubjectGeoRefLat, "lat") + " " + Util.decimalToDMS(viewModel.subject.SubjectGeoRefLong, "long"),
                Height: 10,
                Elevation: 10,
                Lighted: "N",
                Painted: "O"
            }));
            self.obstPushPin = null;
            self.initUI();
            self.setupUpNSDEditor();
            conditionValidator = $("#nsdConditionContainer").data("kendoValidator");
            editor.conditionValidator = conditionValidator;
            viewModel.nsdTokens.bind("change", self.validate);
        },
        getICAOItemA: function (desig) {
            return desig.match(/\d+/g) !== null ? "CXXX" : desig;
        },
        initUI: function()
        {
            $("#sdoSubjectSelector").kendoDropDownList({
                optionLabel: RES.dbRes("Select SDO Subject..."),
                filter: "contains",
                dataTextField: "DisplayName",
                valuePrimitive: true,
                dataValueField: "Id",
                dataSource: {
                    type: "odata-v4",
                    pageSize: 20,
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "/odata/SdoSubjects?$top=30000&$select=Id, SdoEntityName,Designator,Name,SubjectGeo,SubjectGeoRefPoint &$expand=Parent($select=SdoEntityName,Designator,Name ) &$orderby=ParentId,SdoEntityName",
                            dataType: "json"
                        },
                        parameterMap: function (data, type) {
                            var ahpId = document.getElementById("sdoSubjectSelector").value;
                            if (type === "read" && !data.filter && ahpId) {
                                return { $filter: "Id eq '" + ahpId + "'" };
                            }

                            if (type === "read" && data.filter && data.filter.filters.length > 0) {

                                var filterValue = data.filter.filters[0].value;
                                return { $filter: "not IsDeleted and SdoEntityName eq 'Ahp' and (contains(SdoEntityName, '" + filterValue + "') or contains(Designator, '" + filterValue + "') or contains(Name, '" + filterValue + "') or (Parent ne null and (contains(Parent/SdoEntityName, '" + filterValue + "') or contains(Parent/Designator, '" + filterValue + "') or contains(Parent/Name, '" + filterValue + "')   ) ))" };
                            }

                            return { $filter: "not IsDeleted and SdoEntityName eq 'Ahp'" };
                        }
                    },
                    schema: {
                        data: function (data) {
                            return data["value"];
                        },
                        total: function (data) {
                            return data["odata.count"];
                        },
                        model: {
                            id: "Id"
                        },
                        parse: function (response) {
                            $.each(response.value, function (idx, elem) {
                                elem.DisplayName = (elem.Parent ? (">> @" + (elem.Parent.Parent ? elem.Parent.Parent.Designator : elem.Parent.Designator) + " " + elem.Designator) : (elem.Designator + " " + elem.Name)) + " (" + elem.SdoEntityName + ")";
                            });
                            return response;
                        }
                    },
                    change: function (e) {
                        self.handelLinkedSDOSubjectChange();
                    }
                },
                change: function (e) {
                    self.handelLinkedSDOSubjectChange();
                }
            });
            $("#obstacleTypes").kendoDropDownList({
                dataTextField: "Text",
                dataValueField: "Value",
                dataSource: obstacleTypes.Data,
                index: 0
            });
            $("#obstacleLocation").kendoMaskedTextBox({
                change: function()
                {
                    self.updateObstacleLocation();
                }
            });
            $("#obstacleHeight").kendoNumericTextBox({
                decimals: 0,
                format: "#",
                min: 10,
                max: 3000
            });
            $("#obstacleElevation").kendoNumericTextBox({
                decimals: 0,
                format: "#",
                min: 10,
                max: 99999
            });
            $("#nsdConditionContainer").kendoValidator({
                rules: {
                    all: function(input)
                    {
                        var inputName = input.attr("name");
                        if (!inputName && input.hasClass("k-formatted-value") && input.siblings("input"))
                        {
                            inputName = $(input.siblings("input")[0]).attr("name");
                        }
                        switch (inputName)
                        {
                            case "sdoSubjectSelector":
                                {

                                    return self.setValidationError(function () {
                                        var val = $("#sdoSubjectSelector").data("kendoDropDownList").dataSource.get($("#sdoSubjectSelector").data("kendoDropDownList").value());
                                        var span = $(input).parent().find(".k-dropdown-wrap");
                                        if (!val ) {
                                            $(span).addClass("k-invalid");
                                        }
                                        else {
                                            $(span).removeClass("k-invalid");
                                        }
                                        return val;
                                    }, RES.dbRes("Invalid Aeronautical Subject"));

                                }

                            case "obstacleTypes":
                                return self.setValidationError(function()
                                {
                                    var selectedType = $("#obstacleTypes").data("kendoDropDownList").dataItem();
                                    var span = $(input).parent().find(".k-dropdown-wrap");
                                    if (!selectedType || !selectedType.Value)
                                    {
                                        $(span).addClass("k-invalid");
                                    }
                                    else
                                    {
                                        $(span).removeClass("k-invalid");
                                    }
                                    return selectedType && selectedType.Value;
                                }, RES.dbRes("Obstacle Type not defined."));
                            case "obstacleLocation":
                                var obstacleLocation = $("#obstacleLocation").val();
                                if (self.setValidationError(function()
                                {
                                    return Util.IsValidLocation(obstacleLocation);
                                }, RES.dbRes("Invalid Obstacle Location")))
                                {
                                    return self.setValidationError(function()
                                    {
                                        return viewModel.IsWithinDOA(obstacleLocation);
                                    }, RES.dbRes("Obstacle is outside your Domain of Authority"));
                                }
                                return false;
                        }
                        return true;
                    }
                },
                messages: {
                    all: function(input)
                    {
                        //input.closest(".k-widget").addClass("k-invalid");
                        return self.validationErrorMessage;
                    }
                }
            });
        },

        handelLinkedSDOSubjectChange: function () {

            viewModel.nsdTokens.unbind("change", self.validate); //disable validations to avoid multiples calls to server

            nsdEditor.bingMap.userLayer.clear();
            nsdEditor.bingMap.displayLayer.clear();
            var sdo = $("#sdoSubjectSelector").data("kendoDropDownList").dataSource.get($("#sdoSubjectSelector").data("kendoDropDownList").value());
            if (sdo) {

                //$("#subjectSectionIcon").attr("src", "/Images/yes.png");obstacleLocation
                var shape = WKTModule.Read(sdo.SubjectGeo.Geography.WellKnownText);
                nsdEditor.bingMap.displayLayer.push(shape);
                var geoRefLocation = WKTModule.Read(sdo.SubjectGeoRefPoint.Geography.WellKnownText)._location;           

                nsdEditor.bingMap.map.setView({ center: geoRefLocation, zoom: 13 });

                var locationDms = Util.decimalToDMS(geoRefLocation.latitude, "lat") + " " + Util.decimalToDMS(geoRefLocation.longitude, "long");

                viewModel.nsdTokens.set("ItemA", sdo.Designator);

                if (viewModel.nsdTokens.Location !== locationDms) {
                    viewModel.nsdTokens.set("Location", locationDms);
                    self.setObstLocation(geoRefLocation);
                }

                self.subjectPushPin = nsdEditor.bingMap.addPushPin(
                    geoRefLocation.latitude,
                    geoRefLocation.longitude,
                    {
                        draggable: false,
                        height: 50,
                        width: 50,
                        anchor: new Microsoft.Maps.Point(0, 50),
                        icon: "/Images/BluePushpin.png"
                    });

                self.validate(); // triggering manual validatios after severals updates
                viewModel.nsdTokens.bind("change", self.validate); //enable validations again
            }
        },
        setObstLocation: function (location) {

            if (!location && !self.obstPushPin) {
                return;
            }

            // remove pushpin if invalid location
            if (!location && self.obstPushPin) {
                nsdEditor.bingMap.removePushPin(self.obstPushPin);
                self.obstPushPin = null;
                return;
            }

            if (self.obstPushPin) {
                self.obstPushPin.setLocation(location);
            } else {


                self.obstPushPin = nsdEditor.bingMap.addPushPin(
                    location.latitude,
                    location.longitude,
                    {
                        draggable: false,
                        height: 50,
                        width: 50,
                        anchor: new Microsoft.Maps.Point(24, 46),
                        icon: "/Images/Obstacle.png"
                    });
            }
        },
        setValidationError: function(condition, error)
        {
            var result = condition();
            if (result)
            {
                self.validationErrorMessage = "";
            }
            else
            {
                self.validationErrorMessage = error;
            }
            return result;
        },
        validate: function(e)
        {
            viewModel.validModel = false;
            Util.setConditionValid(false);

            if (!conditionValidator.validate() )
                return;

            if (viewModel.nsdTokens.Bilingual === true) {
                $(".eFieldFrench").show();
            } else {
                $(".eFieldFrench").show();
            }

            debugger;
            var reviewTab = $("#reviewTab").kendoTabStrip().data("kendoTabStrip");
            reviewTab.enable(reviewTab.tabGroup.children().eq(1), false);

            viewModel.generateNOTAM();
        },
        setupUpNSDEditor: function()
        {
            Util.hidePermement();
        },
        updateObstacleLocation: function()
        {
            var loc = viewModel.nsdTokens.Location;
            if (!Util.IsValidLocation(loc))
            {
                self.setObstLocation(null);
                return;
            }
            var parts = Util.locationToDecimal(loc).split(" ");
            var location = new Microsoft.Maps.Location(parts[0], parts[1]);
            self.setObstLocation(location);
            nsdEditor.bingMap.centerMap(location);
        }
    };
});