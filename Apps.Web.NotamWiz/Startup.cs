﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(NavCanada.Applications.NotamWiz.Web.Startup))]
namespace NavCanada.Applications.NotamWiz.Web
{
    using System.Web.Configuration;

    using Microsoft.AspNet.SignalR;

    using Owin;

    public partial class Startup
    {

        // add this static variable

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            // Any connection or hub wire up and configuration should go here            
            string sqlConnectionString = WebConfigurationManager.ConnectionStrings["SignalRConnection"].ConnectionString;
            GlobalHost.DependencyResolver.UseSqlServer(sqlConnectionString);
            
            app.MapSignalR(new HubConfiguration
            {
                EnableDetailedErrors = true, //Do this to help debugging, set to false in production
                EnableJSONP = true, //Do this if any of your SignalR hubs will be called by a proxy hub (like an appender in an external process)
                EnableJavaScriptProxies = true
            });

       }

    }
}
