namespace NavCanada.Applications.NotamWiz.Web.log4net.SignalR
{
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    [HubName("signalrAppenderHub")]
    public class SignalrAppenderHub : Hub
    {
        private const string Log4NetGroup = "Log4NetGroup";

        public SignalrAppenderHub()
        {
            SignalrAppender.LocalInstance.MessageLogged = OnMessageLogged;
        }

        [HubMethodName("listen")]
        public void Listen()
        {
            Groups.Add(Context.ConnectionId, Log4NetGroup);
        }

        public void OnMessageLogged(LogEntry e)
        {
            try
            {
                Clients.Group(Log4NetGroup).onLoggedEvent(e.FormattedEvent, e.LoggingEvent);
            }
            catch (System.OutOfMemoryException )
            {
                // TODO : investigate 
            }
        }
    }
}