﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NavCanada.Core.Proxies.AeroRdsProxy
{
    public class AeroRDSUrlBuilder : IAeroRDSUrlBuilder
    {
        public string GetLoadRecordsUrl() => "LoadRecord";

        public string GetAirspacesActiveDateUrl(string adjacentAirspaces)
        {
            var activeDateFormatted = DashFormatDate(DateTime.UtcNow);
            return $"AirspaceFeature?ActiveDate={activeDateFormatted}&$inlinecount=allpages&$filter=(Type eq 'Fir' and (startswith(Identifier, 'C'){GetOrIdentifierFilter(adjacentAirspaces)}))";
        }

        public string GetAirspacesEffectiveDateUrl(DateTime effectiveDate, string adjacentAirspaces)
        {
            var effectiveDateFormatted = DashFormatDate(effectiveDate);
            return $"AirspaceFeature?$inlinecount=allpages&$filter=(Type eq 'Fir' and EffectiveDate eq datetime'{effectiveDateFormatted}' and (startswith(Identifier, 'C'){GetOrIdentifierFilter(adjacentAirspaces)}))";
        }

        public string GetAerodromesActiveDateUrl(string nonCadAds)
        {
            var activeDateFormatted = DashFormatDate(DateTime.UtcNow);
            return $"AerodromeFeature?ActiveDate={activeDateFormatted}&$inlinecount=allpages&$filter=(Country eq 'CAN'{GetOrIdentifierFilter(nonCadAds)})";
        }

        public string GetAerodromesEffectiveDateUrl(DateTime effectiveDate, string nonCadAds)
        {
            var effectiveDateFormatted = DashFormatDate(effectiveDate);
            return $"AerodromeFeature?$inlinecount=allpages&$filter=(EffectiveDate eq datetime'{effectiveDateFormatted}' and (Country eq 'CAN'{GetOrIdentifierFilter(nonCadAds)}))";
        }

        public string GetFeatureActiveDateUrl(string featureName)
        {
            return $"{featureName}?ActiveDate={DashFormatDate(DateTime.UtcNow)}&$inlinecount=allpages";
        }

        public string GetFeatureEffectiveDateUrl(string featureName, DateTime effectiveDate)
        {
            return $"{featureName}?$inlinecount=allpages&$filter=(EffectiveDate eq datetime'{DashFormatDate(effectiveDate)}')";
        }

        public static string DashFormatDate(DateTime date) => $"{date.Year}-{FormatInt(date.Month, 2)}-{FormatInt(date.Day, 2)}";

        static string GetOrIdentifierFilter(string values) =>
            !string.IsNullOrEmpty(values) ? GetOrEqFilter("Identifier", values.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)) : string.Empty;

        static string GetOrEqFilter(string field, IEnumerable<string> values)
        {
            var ors = string.Join(" or ", values.Select(s => $"{field} eq '{s}'"));
            return !string.IsNullOrEmpty(ors) ? $" or {ors}" : string.Empty;
        }

        static string FormatInt(int value, int places) => value.ToString().PadLeft(places, '0');
    }
}
