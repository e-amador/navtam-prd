﻿namespace NavCanada.Core.Proxies.AeroRdsProxy
{
    using Microsoft.OData.Client;
    using Default;
    using SDOData.Models.SDOEntities;
    using global::System.Collections.Generic;

    public interface IAeroRdsProxy
    {
        Ahp GetAhpByMid(string mid);
        List<Rwy> GetExpandedRwyForAhp(string mid);
        bool IsRdnDisplaced(string rdnMid);

        //????
        Container ODataDataContext { get; }
        Container ODataGlobalDataContext { get; }
    }
}
