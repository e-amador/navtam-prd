﻿using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace NavCanada.Core.Proxies.AeroRdsProxy
{
    public class AeroRdsFeatureProxy : IAeroRdsFeatureProxy
    {
        public AeroRdsFeatureProxy(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public T GetFeature<T>(string featureQuery)
        {
            return JsonConvert.DeserializeObject<T>(GetFeatureRaw(featureQuery));
        }

        public string GetFeatureRaw(string featureQuery)
        {
            var request = WebRequest.Create($"{_baseUrl}/{featureQuery}");
            request.UseDefaultCredentials = true;
            using (var response = request.GetResponse())
            {
                using (var stream = new StreamReader(response.GetResponseStream()))
                {
                    return stream.ReadToEnd();
                }
            }
        }

        public IAeroRDSUrlBuilder GetUrlBuilder() => new AeroRDSUrlBuilder();

        readonly string _baseUrl;
    }
}
