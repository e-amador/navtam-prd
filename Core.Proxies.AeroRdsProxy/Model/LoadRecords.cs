﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class LoadRecord
    {
        public int ID { get; set; }
        public string Source { get; set; }
        public DateTime Date { get; set; }
        public string IsFAAData { get; set; }
    }

    public class LoadRecords
    {
        public IList<LoadRecord> Value { get; set; }
    }
}
