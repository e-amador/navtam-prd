﻿namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class GeographicalLocation
    {
        public WellKnownValue WellKnownValue { get; set; }
    }

    public class WellKnownValue
    {
        public int CoordinateSystemId { get; set; }
        public string WellKnownText { get; set; }
        public string WellKnownBinary { get; set; }
    }
}
