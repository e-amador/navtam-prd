﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class ServiceUnit
    {
        public string ServiceUnitId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Status { get; set; }
        public string FK_Organization { get; set; }
        public string FK_Aerodrome { get; set; }
        public string Name { get; set; }
        public string Identifier { get; set; }
        public string Type { get; set; }
        public string Classification { get; set; }
        public string Latitude { get; set; }
        public string LatitudeInDecimal { get; set; }
        public string Longitude { get; set; }
        public string LongitudeInDecimal { get; set; }
        public string Datum { get; set; }
        public string Remark { get; set; }
        public GeographicalLocation GeographicalLocation { get; set; }
        public int SEQ { get; set; }
    }

    public class ServiceUnits
    {
        public IList<ServiceUnit> Value { get; set; }
    }
}
