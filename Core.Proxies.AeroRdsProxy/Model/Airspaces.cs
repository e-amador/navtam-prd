﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class Airspaces
    {
        public IList<Airspace> Value { get; set; }
    }

    public class Airspace
    {
        public string AirspaceId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Identifier { get; set; }
        public string LocalTypeDesignator { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string LocationIndicator { get; set; }
        public string Activity { get; set; }
        public string Military { get; set; }
        public string UpperLimitCode { get; set; }
        public double? UpperLimitValue { get; set; }
        public double? UpperLimitValueInFeet { get; set; }
        public string UOMUpperLimit { get; set; }
        public string LowerLimitCode { get; set; }
        public double? LowerLimitValue { get; set; }
        public double? LowerLimitValueInFeet { get; set; }
        public string UOMLowerLimit { get; set; }
        public string MaximumLimitCode { get; set; }
        public string MaximumLimitValue { get; set; }
        public string UOMMaximumLimit { get; set; }
        public string MinimumLimitCode { get; set; }
        public string MinimumLimitValue { get; set; }
        public string UOMMinimumLimit { get; set; }
        public string Remark { get; set; }
        public string LowerLimitValueInFL { get; set; }
        public GeographicalLocation GeographicalLocation { get; set; }
        public string FK_Timetable { get; set; }
        public string FK_AirspaceAuthority { get; set; }
        public int SEQ { get; set; }
    }
}
