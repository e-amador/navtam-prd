﻿using System;

namespace NavCanada.Core.Proxies.AeroRdsProxy
{
    public interface IAeroRDSUrlBuilder
    {
        string GetLoadRecordsUrl();
        string GetAerodromesActiveDateUrl(string nonCadAds);
        string GetAerodromesEffectiveDateUrl(DateTime effectiveDate, string nonCadAds);
        string GetAirspacesActiveDateUrl(string adjacentAirspaces);
        string GetAirspacesEffectiveDateUrl(DateTime effectiveDate, string adjacentAirspaces);
        string GetFeatureActiveDateUrl(string featureName);
        string GetFeatureEffectiveDateUrl(string featureName, DateTime effectiveDate);
    }
}