﻿using Uri = System.Uri;

namespace NavCanada.Core.Proxies.AeroRdsProxy
{

    using Default;
    using SDOData.Models.SDOEntities;
    using global::System.Collections.Generic;
    using global::System.Linq;

    public class AeroRdsProxy : IAeroRdsProxy
    {
        public AeroRdsProxy(string serviceUrl)
        {
            _oDataCtx = new Container(new Uri(serviceUrl));
            _oDataGlobalCtx = new Container(new Uri(serviceUrl.Replace("/Canadian/", "/Global/")));
        }

        public virtual Ahp GetAhpByMid(string mid)
        {
            return _oDataCtx.Ahp.Where(a => a.Mid == mid).FirstOrDefault();
        }

        public virtual List<Rwy> GetExpandedRwyForAhp(string mid)
        {
            return _oDataCtx.Rwy.Expand(r => r.Rdn).Where(r => r.Ahp.Mid == mid).ToList();
        }

        public virtual bool IsRdnDisplaced(string rdnMid)
        {
            return _oDataCtx.Rdd.Where(r => r.FK_RdnUid == rdnMid && r.CodeType == "DPLM").Count() > 0;
        }

        // ?????
        public Container ODataDataContext
        {
            get
            {
                return _oDataCtx;
            }
        }
        public Container ODataGlobalDataContext
        {
            get
            {
                return _oDataGlobalCtx;
            }
        }

        private readonly Container _oDataCtx;
        private readonly Container _oDataGlobalCtx;
    }
}
