﻿using System.Configuration;

namespace NDS.Relay
{
    public class AftnConsts
    {
        static AftnConsts()
        {
            _nofAddress = GetConfig("Aftn_NOFAddress", "CTSCNCHN");
            _requestExceedsMaxNumber = GetConfig("Aftn_RequestExceedsMaxNumber", "YOUR REQ MSG EXCEEDS MAX NR OF 100/VOTRE MSG REQ DEPASSE NR MAX DE 100");
            _notamNotIssued = GetConfig("Aftn_NotamNotIssued", "NOTAM NOT ISSUED/NOTAM NON EMIS");
            _notamNotInDatabase = GetConfig("Aftn_NotamNotInDatabase", "NOTAM NO LONGER IN DATABASE/NOTAM N’EST PLUS DISPONIBLE EN BASE DE DONNEES");
            _notamExpired = GetConfig("Aftn_NotamExpired", "NOTAM EXPIRED/NOTAM EXPIRE");
            _notamCanceled = GetConfig("Aftn_NotamCanceled", "NOTAM CANCELLED BY/NOTAM ANNULE PAR");
            _notamReplaced = GetConfig("Aftn_NotamReplaced", "NOTAM REPLACED BY/NOTAM REMPLACE PAR");
            _incorrectRequestFormat = GetConfig("Aftn_IncorrectRequestFormat", "INCORRECT REQ MSG FORMAT. PLEASE COR AND RPT. FOR DETAILS SEE: HTTP://WWW.NAVCANADA.CA / FORMAT DU MSG DE REQ ERRONE. VEUILLEZ COR ET RPT. POUR DETAILS, VOIR : HTTP://WWW.NAVCANADA.CA");
            _userCannotPerformQueries = GetConfig("Aftn_UserCannotPerformQueries", "YOU ARE NOT ALLOWED TO PERFORM QUERIES/VOUS N'ETES PAS AUTORISE A EFFECTUER DES REQUETES");
            _noValidNotamInDatabase = GetConfig("Aftn_NoValidNotamInDatabase", "NO VALID NOTAM IN DATABASE/PAS DE NOTAM VALIDE EN BASE DE DONNEES");
            _seriesNotManaged = GetConfig("Aftn_SeriesNotManaged", "REQUESTED NOF OR SERIES NOT MANAGED/NOF OU SERIE REQ NON GERES");
        }

        public const char SOH = '\u0001';
        public const char STX = '\u0002';
        public const char ETX = '\u0003';
        public const char VT = '\v';
        public const string CRLF = "\r\n";
        public const string TRANS_ID_DATE = "%TRANS_ID_DATE%";
        public const string CYHQ = "CYHQ";
        public const string RQRHeader = "RQR CYHQ";

        public static string NOFAddress => _nofAddress;
        public static string RequestExceedsMaxNumber => _requestExceedsMaxNumber;
        public static string NotamNotIssued => _notamNotIssued;
        public static string NotamNotInDatabase => _notamNotInDatabase;
        public static string NotamExpired => _notamExpired;
        public static string NotamCanceled => _notamCanceled;
        public static string NotamReplaced => _notamReplaced;
        public static string IncorrectRequestFormat => _incorrectRequestFormat;
        public static string UserCannotPerformQueries => _userCannotPerformQueries;
        public static string NoValidNotamInDatabase => _noValidNotamInDatabase;
        public static string SeriesNotManaged => _seriesNotManaged;

        static string GetConfig(string name, string defaultValue)
        {
            return ConfigurationManager.AppSettings[name] ?? defaultValue;
        }

        static string _nofAddress;
        static string _requestExceedsMaxNumber;
        static string _notamNotIssued;
        static string _notamNotInDatabase;
        static string _notamExpired;
        static string _notamCanceled;
        static string _notamReplaced;
        static string _incorrectRequestFormat;
        static string _userCannotPerformQueries;
        static string _noValidNotamInDatabase;
        static string _seriesNotManaged;
    }
}
