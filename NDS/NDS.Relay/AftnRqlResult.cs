﻿using NavCanada.Core.Domain.Model.Dtos;
using System.Collections.Generic;

namespace NDS.Relay
{
    public class AftnRqlResult
    {
        public AftnRqlResult(IEnumerable<RqlNotamResultDto> results = null)
        {
            Results = results;
        }

        public static AftnRqlResult NoSeriesExistsResult => new AftnRqlResult();

        public IEnumerable<RqlNotamResultDto> Results { get; set; }
        public bool SeriesExists => Results != null;
    }
}
