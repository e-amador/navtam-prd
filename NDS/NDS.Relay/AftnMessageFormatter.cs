﻿using System;
using System.Text;

namespace NDS.Relay
{
    public static class AftnMessageFormatter
    {
        public static string FormatMessage(string priority, string addresses, string originAddress, string text, DateTime date)
        {
            var message = new StringBuilder();

            // e.g.: <SOH>ABC0044 14033608<CR><LF> 
            message.Append($"{AftnConsts.SOH}{AftnConsts.TRANS_ID_DATE}{AftnConsts.CRLF}");
            
            // e.g.: GG CYYCYFYX<CR><LF>
            message.Append($"{priority} {addresses}{AftnConsts.CRLF}");

            // e.g.: 140335 CYEGYFYX<CR><LF>
            message.Append($"{GetDDHHMM(date)} {originAddress}{AftnConsts.CRLF}");

            message.Append($"{AftnConsts.STX}{text}{AftnConsts.VT}{AftnConsts.ETX}");

            return message.ToString();
        }

        static string GetDDHHMM(DateTime d) => $"{Format2Digits(d.Day)}{Format2Digits(d.Hour)}{Format2Digits(d.Minute)}";
        static string Format2Digits(int v) => v.ToString().PadLeft(2, '0');
    }
}
