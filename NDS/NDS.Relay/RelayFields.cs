﻿namespace NDS.Relay
{
    public static class RelayFields
    {
        public static string MSG_TYPE => nameof(MSG_TYPE);
        public static string NOTAM_TYPE => nameof(NOTAM_TYPE);
        public static string MSG_ID => nameof(MSG_ID);
        public static string AFTN_HEADER => nameof(AFTN_HEADER);
        public static string Series => nameof(Series).ToUpper();
        public static string Item_A => nameof(Item_A).ToUpper();
        public static string Q_Code => nameof(Q_Code).ToUpper();
        public static string Traffic => nameof(Traffic).ToUpper();
        public static string Purpose => nameof(Purpose).ToUpper();
        public static string Scope => nameof(Scope).ToUpper();
        public static string Lower => nameof(Lower).ToUpper();
        public static string Upper => nameof(Upper).ToUpper();
    }
}
