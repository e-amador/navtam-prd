﻿using System;

namespace NDS.Relay
{
    public class AftnParserResult<T> where T: class
    {
        static AftnParserResult<T> Succeed(T value) => new AftnParserResult<T> { Result = value };
        static AftnParserResult<T> Fail(string errorMessage) => new AftnParserResult<T> { ErrorMessage = errorMessage };
        public T Result { get; set; }
        public string ErrorMessage { get; set; }
        public bool Succeeded => Result != null;
    }

    public static class AftnMessageParser
    {
        /// <summary>
        /// Parses an AFTN message
        /// Example: [SOH]CGA0005 29133553[CRLF]GG CTSCNDSE[CRLF]290935 CTSCNDSE[CRLF][STX]RQN CYHQ A0001/18[VT][ETX]
        /// </summary>
        /// <param name="rawMessage"></param>
        /// <returns></returns>
        public static AftnMessage ParseMessage(string rawMessage)
        {
            if (string.IsNullOrEmpty(rawMessage))
                return null;

            // expecting <SOH>
            if (rawMessage[0] != AftnConsts.SOH)
                return null;

            // find <STX> position after <SOH>
            var stxIndex = rawMessage.IndexOf(AftnConsts.STX, 1);
            if (stxIndex == -1)
                return null;

            // copy the header
            var messageHeaders = rawMessage.Substring(1, stxIndex - 1);

            // find <VT><ETX> after <STX>
            var etxIndex = rawMessage.IndexOf($"{AftnConsts.VT}{AftnConsts.ETX}", stxIndex + 1);
            if (etxIndex == -1)
            {
                // not found, try to find only <ETX> after <STX>
                etxIndex = rawMessage.IndexOf(AftnConsts.ETX, stxIndex + 1);
            }

            if (etxIndex == -1)
                return null;

            // copy the body
            var messageText = rawMessage.Substring(stxIndex + 1, etxIndex - stxIndex - 1);

            // parse the headers
            var cursor = 0;

            // first row (transmission id & time)
            var firstRow = ReadNextLine(messageHeaders, ref cursor).Split(' ');

            // second row (priority & destination address)
            var secondRow = ReadNextLine(messageHeaders, ref cursor).Split(' ');

            // third row (filing time & origin address)
            var thirdRow = ReadNextLine(messageHeaders, ref cursor).Split(' ');

            return new AftnMessage
            {
                TransId = firstRow.GetColumn(0, string.Empty),
                TransTime = firstRow.GetColumn(1, string.Empty),
                Priority = secondRow.GetColumn(0, string.Empty),
                DestinationAddress = secondRow.GetColumn(1, string.Empty),
                FilingTime = thirdRow.GetColumn(0, string.Empty),
                OriginAddress = thirdRow.GetColumn(1, string.Empty),
                Headers = messageHeaders,
                Text = messageText
            };
        }

        /// <summary>
        /// Parses an AFTN request from a valid AFTN message.
        /// </summary>
        /// <param name="rawMessage"></param>
        /// <returns></returns>
        public static AftnRequestMessage ParseRequestMessage(AftnMessage rawMessage)
        {
            // must have both headers and text
            if (string.IsNullOrEmpty(rawMessage?.Headers) || string.IsNullOrEmpty(rawMessage?.Text))
                return null;

            var cursor = 0;

            // ignore first line
            var firstLine = ReadNextLine(rawMessage.Headers, ref cursor);

            // expecting [PRIORITY] [NOF_ADDRESS]
            //if (rawMessage.DestinationAddress != AftnConsts.NOFAddress)
            //    return null;

            // expecting [DATE(6)] [RESPONSE_ADDRESS]
            if (!rawMessage.FilingTime.ExpectedLength(6) || !rawMessage.OriginAddress.ExpectedLength(8))
                return null;

            // response address
            var responseAddress = rawMessage.OriginAddress;

            var payload = rawMessage.Text;

            var requestType = AftnRequestType.Unknown;

            // detect request type
            cursor = 0;
            switch (GetNextToken(payload, ref cursor))
            {
                case "RQN":
                    requestType = AftnRequestType.Rqn;
                    break;
                case "RQL":
                    requestType = AftnRequestType.Rql;
                    break;
                default:
                    return null;
            }

            // read the Notam office e.g.: CYHQ
            var notamOffice = GetNextToken(payload, ref cursor);

            var queryAndLanguage = cursor < payload.Length ? payload.Substring(cursor).Trim() : string.Empty;

            // split the rest of the query (C0123/19, LANG=C) in the comma
            var queryParts = queryAndLanguage.Split(',');

            // request body
            var requestBody = queryParts.Length > 0 ? queryParts[0] : string.Empty;

            // request language ( LANG=C)
            var requestLanguage = queryParts.Length > 1 ? ParseRequestLanguage(queryParts[1].Trim()) : string.Empty;

            return new AftnRequestMessage()
            {
                Type = requestType,
                ReplyAddress = responseAddress,
                NotamOffice = notamOffice,
                RequestBody = requestBody,
                Language = requestLanguage
            };
        }

        public static AftnRequestMessage ParseRequestMessage(string rawMessage) => ParseRequestMessage(ParseMessage(rawMessage));

        public static string ParseSenderAddress(string text)
        {
            var safeText = text ?? string.Empty;
            var cursor = 0;
            var firstLine = ReadNextLine(safeText, ref cursor);
            var secondLine = ReadNextLine(safeText, ref cursor);
            var parts = secondLine.Split(' ');
            return parts.Length == 2 ? parts[1] : string.Empty;
        }

        static bool IsValid(this AftnMessage message)
        {
            if (string.IsNullOrEmpty(message.Priority))
                return false;

            return true;
        }

        static T GetColumn<T>(this T[] arr, int index, T defValue = default(T)) => arr.Length > index ? arr[index] : defValue;

        static bool ExpectedLength(this string text, int length) => (text ?? "").Length == length;

        static string ParseRequestLanguage(string text)
        {
            // expects LANG=C (may have one or more CRLFs after)
            var p = 0;
            var parts = ReadNextLine(text, ref p).Split('=');
            return parts.Length == 2 && "LANG" == parts[0] ? parts[1] : string.Empty;
        }

        static string ReadNextLine(string text, ref int position)
        {
            var len = text.Length;

            if (position >= len)
                return string.Empty;

            var crIndex = text.IndexOf('\r', position);
            if (crIndex == -1)
                crIndex = len;

            var line = text.Substring(position, crIndex - position);

            position = Math.Min(len, crIndex + (crIndex < len - 1 && text[crIndex + 1] == '\n' ? 2 : 1));

            return line;
        }

        static string GetNextToken(string text, ref int position)
        {
            var len = text.Length;
            var spIndex = text.IndexOf(' ', position);
            if (spIndex == -1)
            {
                var token = text.Substring(position);
                position = len;
                return token;
            }
            else
            {
                var token = text.Substring(position, spIndex - position);
                position = spIndex + 1;
                return token;
            }
        }
    }
}
