﻿namespace NDS.Relay
{
    public enum AftnRequestType : byte
    {
        Rqn, Rql, Unknown
    }

    public class AftnRequestMessage
    {
        public AftnRequestType Type { get; set; }
        public string ReplyAddress { get; set; }
        public string NotamOffice { get; set; }
        public string RequestBody { get; set; }
        public string Language { get; set; }
    }
}
