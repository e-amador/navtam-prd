﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;

namespace NDS.Relay
{
    public enum AftnRqnResultType : byte
    {
        Active,
        Replaced,
        Canceled,
        Expired,
        Obsolete, // 3 months or older
        Missing,
        LimitReached
    }

    public class AftnRqnResult
    {
        public static AftnRqnResult CreateNotamResult(AftnRqnResultType resultType, Notam notam, string replaceNumber)
        {
            return new AftnRqnResult { ResultType = resultType,  NotamNumber = notam.NotamId, Notam = notam, ReplaceNumber = replaceNumber };
        }

        public static AftnRqnResult CreateMissingNotamsResult(IEnumerable<string> notamNumbers)
        {
            return new AftnRqnResult { ResultType = AftnRqnResultType.Missing, NotamNumber = string.Join(" ", notamNumbers) };
        }

        public static AftnRqnResult CreateLimitReachedResult()
        {
            return new AftnRqnResult { ResultType = AftnRqnResultType.LimitReached };
        }

        public string NotamNumber { get; set; }
        public AftnRqnResultType ResultType { get; set; }
        public Notam Notam { get; set; }
        public string ReplaceNumber { get; set; }
    }
}
