﻿namespace NDS.Relay
{
    public class AftnMessage
    {
        public string TransId { get; set; }
        public string TransTime { get; set; }
        public string Priority { get; set; }
        public string DestinationAddress { get; set; }
        public string FilingTime { get; set; }
        public string OriginAddress { get; set; }
        public string Headers { get; set; }
        public string Text { get; set; }
    }
}
