﻿using System.Collections.Generic;
using System.Linq;
using NDS.Common.Contracts;

namespace DataBus.Consumer.ViewModels
{
    public class MessageViewModel : ViewModelBase
    {
        public MessageViewModel(INdsMessage message)
        {
            _messageBody = message.Body;
            _messageProperties = message.Properties.ToList();
        }

        public string Content => _messageBody;
        public string Metadata => string.Join(" ", _messageProperties.Select(p => $"{p.Name}={p.Value}"));
        public List<Property> Properties => _messageProperties;

        public override string ToString()
        {
            return Content;
        }

        readonly string _messageBody;
        readonly List<Property> _messageProperties;
    }
}
