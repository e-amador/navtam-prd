﻿using NDS.Common.Contracts;
using NDS.SolaceVMR;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows;

namespace DataBus.Consumer.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            ReadQueueCommand = new DelegateCommand<object>(obj => ReadQueueExecute(), obj => ReadQueueCanExecute());
            ReadTopicCommand = new DelegateCommand<object>(obj => ReadTopicExecute(), obj => ReadTopicCanExecute());
            CleanQueueCommand = new DelegateCommand<object>(obj => CleanQueueExecute());
            CleanTopicCommand = new DelegateCommand<object>(obj => CleanTopicExecute());
            RequestCleanCommand = new DelegateCommand<object>(obj => { });
            //ConnectionString = "address=10.128.232.194;username=nds@default;password=password"; // "Endpoint=sb://win-ffk2v1a3k25/TestServiceBus;StsEndpoint=https://win-ffk2v1a3k25:9355/TestServiceBus;RuntimePort=5671;ManagementPort=9355;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=Bj7Tsw6JXG8RPSObwr7L1+P6R0f5DDRpi9k3u8cEL0k=;TransportType=Amqp";
            ConnectionString = "address=tcps:10.128.197.16:55443;username=nds@default;password=password;seclevel=2"; // "Endpoint=sb://win-ffk2v1a3k25/TestServiceBus;StsEndpoint=https://win-ffk2v1a3k25:9355/TestServiceBus;RuntimePort=5671;ManagementPort=9355;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=Bj7Tsw6JXG8RPSObwr7L1+P6R0f5DDRpi9k3u8cEL0k=;TransportType=Amqp";
            QueueName = "NDS-B-AFTN-SNK-DEV";
            TopicName = "NDS/B/XN/YOW";
            QueueAction = "Read";
            TopicAction = "Read";
            AsyncMessage = "Waiting..";
            QueueMessages = new ObservableCollection<MessageViewModel>();
            TopicMessages = new ObservableCollection<MessageViewModel>();
            _synchronizationContext = SynchronizationContext.Current;
        }

        #region Bound properties

        private string _connectionString;
        public string ConnectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
                OnPropertyChanged();
                UpdateConnectionString();
            }
        }

        private string _queueName;
        public string QueueName
        {
            get
            {
                return _queueName;
            }
            set
            {
                if (_queueName != value)
                {
                    _queueName = value;
                    OnPropertyChanged();
                    UpdateCommands();
                }
            }
        }

        private string _topicName;
        public string TopicName
        {
            get
            {
                return _topicName;
            }
            set
            {
                if (_topicName != value)
                {
                    _topicName = value;
                    OnPropertyChanged();
                    UpdateCommands();
                }
            }
        }

        private string _topicFilter;
        public string TopicFilter
        {
            get
            {
                return _topicFilter;
            }
            set
            {
                if (_topicFilter != value)
                {
                    _topicFilter = value;
                    OnPropertyChanged();
                    UpdateCommands();
                }
            }
        }

        private string _queueAction;
        public string QueueAction
        {
            get
            {
                return _queueAction;
            }
            set
            {
                _queueAction = value;
                OnPropertyChanged();
            }
        }

        private string _topicAction;
        public string TopicAction
        {
            get
            {
                return _topicAction;
            }
            set
            {
                _topicAction = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<MessageViewModel> _queueMessages;
        public ObservableCollection<MessageViewModel> QueueMessages
        {
            get
            {
                return _queueMessages;
            }
            set
            {
                _queueMessages = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<MessageViewModel> _topicMessages;
        public ObservableCollection<MessageViewModel> TopicMessages
        {
            get
            {
                return _topicMessages;
            }
            set
            {
                _topicMessages = value;
                OnPropertyChanged();
            }
        }

        private string _asyncMessage;
        public string AsyncMessage
        {
            get
            {
                return _asyncMessage;
            }
            set
            {
                _asyncMessage = value;
                OnPropertyChanged();
            }
        }
        #endregion

        public DelegateCommand<object> ReadQueueCommand { get; set; }
        public DelegateCommand<object> ReadTopicCommand { get; set; }
        public DelegateCommand<object> CleanQueueCommand { get; set; }
        public DelegateCommand<object> CleanTopicCommand { get; set; }
        public DelegateCommand<object> RequestCleanCommand { get; set; }

        private bool ReadQueueCanExecute()
        {
            return "Stop".Equals(QueueAction) || CanRead(_queueName);
        }

        private void ReadQueueExecute()
        {
            if ("Stop".Equals(QueueAction))
            {
                StopListeningQueue();
                QueueAction = "Start";
            }
            else
            {
                StartListeningQueue();
                QueueAction = "Stop";
            }
        }

        private void StartListeningQueue()
        {
            _queueSubscription = _dataBus.ReadFromQueue(_queueName, message => 
            {
                _synchronizationContext.Send(obj => QueueMessages.Add(new MessageViewModel(message)), null);
            });
        }

        private void StopListeningQueue()
        {
            _queueSubscription?.Unsubscribe();
            _queueSubscription = null;
        }

        private bool ReadTopicCanExecute()
        {
            return "Stop".Equals(TopicAction) || CanRead(_topicName);
        }

        private void ReadTopicExecute()
        {
            if ("Stop".Equals(TopicAction))
            {
                StopListeningTopic();
                TopicAction = "Start";
            }
            else
            {
                StartListeningTopic();
                TopicAction = "Stop";
            }
        }

        private void StartListeningTopic()
        {
            try
            {
                if (_dataBus == null)
                    UpdateConnectionString();

                _topicSubscription = _dataBus.ReadFromTopic(_topicName, "desktop_client", _topicFilter, message =>
                {
                    _synchronizationContext.Send(obj => TopicMessages.Add(new MessageViewModel(message)), null);
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void StopListeningTopic()
        {
            _topicSubscription?.Unsubscribe();
            _topicSubscription = null;
        }

        private void CleanQueueExecute()
        {
            QueueMessages.Clear();
            //QueueCleaned?.Invoke(null, null);
        }

        private void CleanTopicExecute()
        {
            TopicMessages.Clear();
        }

        private bool CanRead(string name)
        {
            return !string.IsNullOrEmpty(name) && _dataBus != null;
        }

        private void UpdateConnectionString()
        {
            if (!string.IsNullOrEmpty(_connectionString))
            {
                _dataBus = new SolaceVMRWrapper(_connectionString);
                StopListeningQueue();
                StopListeningTopic();
                UpdateCommands();
            }
        }

        private void UpdateCommands()
        {
            ReadQueueCommand.RaiseCanExecuteChanged();
            ReadTopicCommand.RaiseCanExecuteChanged();
        }

        private IDataBus _dataBus;
        private ISubscription _queueSubscription;
        private ISubscription _topicSubscription;
        readonly SynchronizationContext _synchronizationContext;
    }
}
