﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NDS.Common;
using NDS.DataBus.Contracts;

namespace NDS.QueryMonitor
{
    public class DataBusQueryMonitor
    {
        static string MessageCountAttribute = "count";
        static string MessageNumberAttibute = "number";

        public DataBusQueryMonitor(IDataBus dataBus, INotamQueryService queryService, ILogger logger)
        {
            if (dataBus == null)
                throw new ArgumentException("Null dataBus");

            if (queryService == null)
                throw new ArgumentException("Null queryService");

            if (logger == null)
                throw new ArgumentException("Null logger");

            _dataBus = dataBus;
            _queryService = queryService;
            _logger = logger;
        }

        public void StartListening(string queueName, string replytoFieldName)
        {
            if (string.IsNullOrEmpty(queueName))
                throw new ArgumentException("Queue name is empty.");

            if (string.IsNullOrEmpty(replytoFieldName))
                throw new ArgumentException("Reply queue name is empty.");

            CreateQueue(queueName);

            _queueSubscription?.Unsubscribe();
            _queueSubscription = _dataBus.ReadFromQueue(queueName, message => ProcessMessage(message, replytoFieldName));

            _logger.Log("Info", $"Started listening to queue: '{queueName}'.");
        }

        public void StopListening()
        {
            _queueSubscription?.Unsubscribe();
        }

        private void CreateQueue(string queueName)
        {
            var subscription = _dataBus.CreateQueue(queueName) as ISubscription;
            subscription?.Unsubscribe();
        }

        private void ProcessMessage(INdsMessage message, string replytoFieldName)
        {
            var replyQueue = (message[replytoFieldName] ?? "").ToString();
            var query = message.Body;
            if (string.IsNullOrEmpty(replyQueue))
            {
                _logger.Log("Error", "Reply queue is empty!");
                return;
            }
            if (string.IsNullOrEmpty(query))
            {
                _logger.Log("Error", "Query is empty!");
                return;
            }
            Task.Factory.StartNew(() => ProcessMessageAsync(replyQueue, query));
        }

        private void ProcessMessageAsync(string replyQuery, string query)
        {
            var replyQueue = _dataBus.TryPostToQueue(replyQuery);
            if (replyQueue != null)
            {
                var queryResults = _queryService.ExecuteQuery(query).ToList();
                var count = queryResults.Count;
                for (int index = 0; index < count; index++)
                {
                    replyQueue.PostMessage(CreateReplyMessage(count, index + 1, queryResults[index]));
                }
            }
            else
            {
                _logger.Log("Error", $"Reply queue '{replyQuery}' does not exist!");
            }
        }

        private INdsMessage CreateReplyMessage(int count, int number, string result)
        {
            var metadata = new List<Property>
            {
                Property.Create(MessageCountAttribute, count),
                Property.Create(MessageNumberAttibute, number),
            };
            return _dataBus.CreateMessage(result, metadata);
        }

        private ISubscription _queueSubscription;
        readonly IDataBus _dataBus;
        readonly INotamQueryService _queryService;
        readonly ILogger _logger;
    }
}
