﻿using System;
using Microsoft.ServiceBus.Messaging;
using NDS.DataBus.Contracts;

namespace NDS.DataBus.ServiceBus
{
    public class TopicClientWrapper : IPostQueue, ISubscription
    {
        public TopicClientWrapper(TopicClient client)
        {
            _topicClient = client;
        }

        public void PostMessage(INdsMessage message)
        {
            var brokeredMessage = message as BrokeredMessageWrapper;
            if (brokeredMessage == null)
                throw new ArgumentException("Unexpected message type!");
            _topicClient.Send(brokeredMessage.InnerMessage);
        }

        public void Unsubscribe()
        {
            _topicClient.Close();
        }

        public void Dispose()
        {
        }

        readonly TopicClient _topicClient;
    }
}
