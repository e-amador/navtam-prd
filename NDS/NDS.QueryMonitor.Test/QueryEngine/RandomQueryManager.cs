﻿using System;
using System.Collections.Generic;
using NDS.Common;

namespace NDS.QueryMonitor.Test.QueryEngine
{
    public class RandomQueryManager : INotamQueryService
    {
        public IEnumerable<string> ExecuteQuery(string query)
        {
            var rand = new Random();
            for (int i = 0; i < rand.Next(2, 8); i++)
            {
                yield return $"{query} - response# {i}.";
            }
        }
    }
}
