﻿using System;
using NDS.Common;
using NDS.Common.CommonDefinitions;
using NDS.DataBus.ServiceBus;
using NDS.QueryMonitor.Test.AFTNClient;
using NDS.QueryMonitor.Test.QueryEngine;

namespace NDS.QueryMonitor.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = "Endpoint=sb://win-ffk2v1a3k25/TestServiceBus;StsEndpoint=https://win-ffk2v1a3k25:9355/TestServiceBus;RuntimePort=5671;ManagementPort=9355;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=Bj7Tsw6JXG8RPSObwr7L1+P6R0f5DDRpi9k3u8cEL0k=;TransportType=Amqp";
            var dataBus = new ServiceBusWrapper(connectionString);

            var queryService = new RandomQueryManager();
            var logger = new ConsoleLogger();

            var monitor = new DataBusQueryMonitor(dataBus, queryService, logger);
            monitor.StartListening(NDSCommonDefintions.Queue, "replyto");

            var testClient = new TestClient();
            testClient.Run(dataBus);

            Console.WriteLine("Press ENTER...");
            Console.ReadLine();

            monitor.StopListening();
        }
    }

    class ConsoleLogger : ILogger
    {
        public void Log(string category, string message)
        {
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()} {category} {message}");
        }
    }
}
