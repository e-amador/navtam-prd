﻿using System;

namespace NDS.Common.Contracts
{
    public interface IPostQueue : IDisposable
    {
        void PostMessage(INdsMessage message);
    }
}
