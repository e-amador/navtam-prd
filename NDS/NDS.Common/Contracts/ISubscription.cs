﻿namespace NDS.Common.Contracts
{
    public interface ISubscription
    {
        void Unsubscribe();
    }
}
