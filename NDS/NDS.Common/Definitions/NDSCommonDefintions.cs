﻿namespace NDS.Common.Definitions
{
    public class NDSCommonDefintions
    {
        public static string Distress = "Distress";
        public static string Urgency = "Urgency";
        public static string FlightSafetyMessages = "FlightSafetyMessages";
        public static string AISM = "AeronauticalInformationServicesMessages";
        public static string Administrative = "Administrative";
        public static string Queue = "CYZZNYXU__RequestQueue";
        public static string DefaultNotamTopic = "NOTAM";
        public static string DefaultSnowtamTopic = "SNOWTAM";
    }
}
