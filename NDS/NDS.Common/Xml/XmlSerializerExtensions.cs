﻿using Business.Common;
using Core.Common.Geography;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using ModelEnums = NavCanada.Core.Domain.Model.Enums;

namespace NDS.Common.Xml
{
    public static class XmlSerializerExtensions
    {
        static XmlSerializerExtensions()
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream("NDS.Common.Xml.NDSTypes.xsd"))
            {
                _xmlSchemaSet = new XmlSchemaSet();
                _xmlSchemaSet.Add(null, XmlReader.Create(stream));
            }
        }

        public static string SerializeAsXml<T>(this T value, string ns)
        {
            if (value == null)
                return string.Empty;

            try
            {
                var xmlserializer = new XmlSerializer(typeof(T), ns);

                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                namespaces.Add("nes", ns);

                var stringWriter = new StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, value, namespaces);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred serializing XML", ex);
            }
        }

        public static string ValidateNotamXml(Notam notam)
        {
            return ValidateNotamXml(notam?.ToXml());
        }

        public static string ValidateNotamXml(string xml)
        {
            using (var xmlReader = XmlReader.Create(new StringReader(xml)))
            {
                var xDoc = XDocument.Load(xmlReader);

                string message = null;
                xDoc.Validate(_xmlSchemaSet, (o, e) => { message = e.Message; });

                return message;
            }
        }

        const string NotamDatePattern = "yyMMddHHmm";

        public static string ToXml(this Notam value)
        {
            var result = new NotamType();
            // version
            result.Version = 1;
            // NOF
            result.NOF = value.Nof;
            // Series
            result.Series = value.Series;
            // Number
            result.Number = value.Number.ToString();
            // Year
            result.Year = value.Year.ToString("0000").Substring(2);
            // Type
            result.Type = MapEnum<ModelEnums.NotamType, NotamTypeType>(value.Type);
            if (result.Type != NotamTypeType.N)
            {
                // Referred Series
                result.ReferredSeries = value.ReferredSeries;
                // Referred Number
                result.ReferredNumber = value.ReferredNumber.ToString();
                // Referred Year
                result.ReferredYear = value.ReferredYear.ToString("0000").Substring(2);
            }
            // QLine
            result.QLine = CreateQLine(value);
            // Coordinates
            result.FullCoordinates = NormalizeCoordinates(value.Coordinates, 6, 7);
            // itemA
            result.ItemA = (value.ItemA ?? "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            // StartValidity
            result.StartValidity = ICAOTextUtils.FormatNotamDate(value.StartActivity);
            // EndValidity
            result.EndValidity = ICAOTextUtils.FormatNotamDate(value.EndValidity);
            // Estimation
            if (value.EndValidity.HasValue && value.Estimated)
                result.Estimation =  "EST";
            // ItemD
            result.ItemD = NullifyString(value.ItemD);
            // ItemE
            result.ItemE = value.ItemE ?? string.Empty;
            // ItemEFrench
            result.ItemEFrench = NullifyString(value.ItemEFrench);
            // ItemF
            result.ItemF = NullifyString(value.ItemF);
            // ItemG
            result.ItemG = NullifyString(value.ItemG);
            // ItemX
            //result.ItemX = NullifyString(value.ItemX);
            // Operator
            result.Operator = NullifyString(value.Operator);
            return SerializeAsXml(result, "http://www.navcanada.ca/NES/CommonTypes");
        }

        private static string NullifyString(string value)
        {
            return !string.IsNullOrEmpty(value) ? value : null;
        }

        private static QLineType CreateQLine(Notam value)
        {
            return new QLineType()
            {
                FIR = value.Fir,
                Code23 = value.Code23,
                Code45 = value.Code45,
                Traffic = MapEnum<TrafficType>(value.Traffic),
                Purpose = MapEnum<PurposeType>(value.Purpose),
                Scope = MapEnum<ScopeType>(value.Scope),
                Lower = value.LowerLimit.ToString("000"),
                Upper = value.UpperLimit.ToString("000"),
                Coordinates = ICAOTextUtils.RoundDMSCoordinates(value.Coordinates, string.Empty),
                Radius = value.Radius.ToString("000")
            };
        }

        private static string NormalizeCoordinates(string coords, int latDigits, int lngDigits)
        {
            var location = DMSLocation.FromDMS(coords);
            return location != null ? location.ToDMS(string.Empty) : coords;
        }

        private static OutEnum MapEnum<InEnum, OutEnum>(InEnum value)
        {
            return (OutEnum)Enum.Parse(typeof(OutEnum), value.ToString());
        }

        private static OutEnum MapEnum<OutEnum>(string value)
        {
            return (OutEnum)Enum.Parse(typeof(OutEnum), value);
        }

        static XmlSchemaSet _xmlSchemaSet;
    }
}
