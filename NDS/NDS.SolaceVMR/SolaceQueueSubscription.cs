﻿using NDS.SolaceVMR.Configuration;
using SolaceSystems.Solclient.Messaging;
using System;
using INdsMessage = NDS.Common.Contracts.INdsMessage;
using INdsSubscrition = NDS.Common.Contracts.ISubscription;

namespace NDS.SolaceVMR
{
    public class SolaceQueueSubscription : INdsSubscrition
    {
        public SolaceQueueSubscription(SessionConfiguration config, string queueName, Action<INdsMessage> callback)
        {
            _context = ContextFactory.Instance.CreateContext(new ContextProperties(), null);
            _session = CreateSession(config);

            _queue = ContextFactory.Instance.CreateQueue(queueName);

            var flowProps = new FlowProperties()
            {
                AckMode = MessageAckMode.ClientAck,
                BindBlocking = false
            };

            _flow = _session.CreateFlow(flowProps, _queue, null, MessageHandler, FlowMessageHandler);

            _callback = callback;
        }

        private void MessageHandler(object sender, MessageEventArgs args)
        {
            using (var message = args.Message)
            {
                try
                {
                    _callback?.Invoke(new SolaceMessageWrapper(message));

                    var ack = _flow.Ack(message.ADMessageId);

                    if (ack != ReturnCode.SOLCLIENT_OK)
                        throw new Exception($"Message was not acknowledged: {message}");
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void FlowMessageHandler(object sender, FlowEventArgs args)
        {
        }

        private void SessionHandler(object sender, SessionEventArgs args)
        {
        }

        private void SessionMessageHandler(object sender, MessageEventArgs args)
        {
            using (var message = args.Message)
            {
                _callback?.Invoke(new SolaceMessageWrapper(message));
            }
        }

        public void Unsubscribe()
        {
            try
            {
                _session?.Disconnect();
                _session?.Dispose();
                _session = null;

                _flow?.Dispose();
                _flow = null;

                _queue?.Dispose();
                _queue = null;

                _topicEndpoint?.Dispose();
                _topicEndpoint = null;

                _context?.Dispose();
                _context = null;

                _callback = null;
            }
            catch (Exception)
            {
                // todo: add logging here
            }
        }

        private ISession CreateSession(SessionConfiguration config)
        {
            var sessionProperties = ConfigureSessionProperties(config);
            var session = _context.CreateSession(sessionProperties, SessionMessageHandler, SessionHandler);

            if (session.Connect() != ReturnCode.SOLCLIENT_OK)
            {
                session.Dispose();
                throw new Exception("Cannot start VMR session!");
            }

            return session;
        }

        private SessionProperties ConfigureSessionProperties(SessionConfiguration config)
        {
            var sessionProperties = SessionUtils.NewSessionPropertiesFromConfig(config);
            sessionProperties.TopicDispatch = true; // This session property must be set to use topic dispatch capabilities
            sessionProperties.CalculateMessageExpiration = true;
            return sessionProperties;
        }

        private IContext _context;
        private ISession _session;
        private IQueue _queue;
        private ITopicEndpoint _topicEndpoint;
        private IFlow _flow;
        private Action<INdsMessage> _callback;
    }
}
