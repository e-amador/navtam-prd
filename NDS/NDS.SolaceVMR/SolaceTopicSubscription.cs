﻿using NDS.SolaceVMR.Configuration;
using SolaceSystems.Solclient.Messaging;
using System;
using INdsMessage = NDS.Common.Contracts.INdsMessage;
using INdsSubscrition = NDS.Common.Contracts.ISubscription;

namespace NDS.SolaceVMR
{
    public class SolaceTopicSubscription : INdsSubscrition
    {
        public SolaceTopicSubscription(SessionConfiguration config, string topicPath, string filter, 
            string subscriptionName, Action<INdsMessage> callback)
        {
            _context = ContextFactory.Instance.CreateContext(new ContextProperties(), null);
            _session = CreateSession(config);
            _topic = ContextFactory.Instance.CreateTopic(topicPath);

            //_topicEndpoint = CreateTopicEndpoint(topicPath, subscriptionName);
            //_flow = CreateTopicFlow(topicPath, filter);

            if (_session.Subscribe(_topic, true) != ReturnCode.SOLCLIENT_OK)
                throw new Exception($"Cannot subscribe to topic {topicPath}!");

            _callback = callback;
        }

        //public SolaceTopicSubscription(SessionConfiguration config, string topicPath, string filter,
        //    string subscriptionName, Action<INdsMessage> callback)
        //{
        //    _context = ContextFactory.Instance.CreateContext(new ContextProperties(), null);
        //    _session = CreateSession(config);
        //    _topic = ContextFactory.Instance.CreateTopic(topicPath);
        //    _topicEndpoint = CreateTopicEndpoint(topicPath, subscriptionName);
        //    _flow = CreateTopicFlow(topicPath, filter);
        //    _callback = callback;
        //}

        private void MessageHandler(object sender, MessageEventArgs args)
        {
            using (var message = args.Message)
            {
                _callback?.Invoke(new SolaceMessageWrapper(message));
            }
        }

        private void FlowMessageHandler(object sender, FlowEventArgs args)
        {
        }

        private void SessionHandler(object sender, SessionEventArgs args)
        {
        }

        private void SessionMessageHandler(object sender, MessageEventArgs args)
        {
            using (var message = args.Message)
            {
                _callback?.Invoke(new SolaceMessageWrapper(message));
            }
        }

        public void Unsubscribe()
        {
            try
            {
                _session?.Unsubscribe(_topic, true);
                _session?.Disconnect();
                _session?.Dispose();
                _session = null;

                _flow?.Dispose();
                _flow = null;

                _topic?.Dispose();
                _topic = null;

                _topicEndpoint?.Dispose();
                _topicEndpoint = null;

                _context?.Dispose();
                _context = null;

                _callback = null;
            }
            catch (Exception)
            {
                // log error
            }
        }

        private ISession CreateSession(SessionConfiguration config)
        {
            var sessionProperties = ConfigureSessionProperties(config);
            var session = _context.CreateSession(sessionProperties, SessionMessageHandler, SessionHandler);

            if (session.Connect() != ReturnCode.SOLCLIENT_OK)
            {
                session.Dispose();
                throw new Exception("Cannot start VMR session!");
            }

            return session;
        }

        private ITopicEndpoint CreateTopicEndpoint(string topicPath, string subscriptionName)
        {
            var topicEndpoint = ContextFactory.Instance.CreateDurableTopicEndpointEx($"{topicPath}/{subscriptionName}");
            var flags = ProvisionFlag.IgnoreErrorIfEndpointAlreadyExists | ProvisionFlag.WaitForConfirm;

            var errorCode = _session.Provision(topicEndpoint, new EndpointProperties(), flags, null);
            if (errorCode != ReturnCode.SOLCLIENT_OK)
            {
                topicEndpoint.Dispose();
                throw new Exception("Failed to provision topic endpoint");
            }

            return topicEndpoint;
        }

        private SessionProperties ConfigureSessionProperties(SessionConfiguration config)
        {
            var sessionProperties = SessionUtils.NewSessionPropertiesFromConfig(config);
            sessionProperties.TopicDispatch = true; // This session property must be set to use topic dispatch capabilities
            sessionProperties.CalculateMessageExpiration = true;
            return sessionProperties;
        }

        private IFlow CreateTopicFlow(string topicPath, string filter)
        {
            var flowProps = new FlowProperties() { FlowStartState = false, Selector = filter };
            var flow = _session.CreateFlow(flowProps, _topicEndpoint, _topic, MessageHandler, FlowMessageHandler);

            if (flow.Start() != ReturnCode.SOLCLIENT_OK)
            {
                flow.Dispose();
                throw new Exception($"Cannot create a flow to topic {topicPath}!");
            }

            return flow;
        }

        private IContext _context;
        private ISession _session;
        private ITopic _topic;
        private ITopicEndpoint _topicEndpoint;
        private IFlow _flow;
        private Action<INdsMessage> _callback;
    }
}
