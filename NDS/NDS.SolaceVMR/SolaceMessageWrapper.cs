﻿using SolaceSystems.Solclient.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using INdsMessage = NDS.Common.Contracts.INdsMessage;
using NdsProperty = NDS.Common.Contracts.Property;

namespace NDS.SolaceVMR
{
    public class SolaceMessageWrapper : INdsMessage
    {
        public SolaceMessageWrapper(IMessage message)
        {
            _message = message;
            Body = System.Text.Encoding.ASCII.GetString(message.XmlContent ?? message.BinaryAttachment);
        }

        public IMessage InnerMessage => _message;

        public SolaceMessageWrapper(string content, IEnumerable<NdsProperty> properties)
        {
            Body = content;
            _message = ContextFactory.Instance.CreateMessage();
            _message.XmlContent = Encoding.ASCII.GetBytes(content);
            _message.CreateUserPropertyMap();
            foreach (var prop in properties)
            {
                _message.UserPropertyMap.AddString(prop.Name, prop.Value.ToString());
            }
        }

        public object this[string name]
        {
            get
            {
                var props = _message.UserPropertyMap;
                if (props != null)
                {
                    try
                    {
                        return props.GetString(name);
                    }
                    catch (Exception)
                    {
                        return "";
                    }
                }
                return "";
            }
        }

        public string Body { get; set; }

        public IEnumerable<NdsProperty> Properties
        {
            get
            {
                var propMap = _message.UserPropertyMap;
                while (propMap.HasNext())
                {
                    var pair = propMap.GetNext();
                    yield return NdsProperty.Create(pair.Key, pair.Value?.Value?.ToString());
                }
            }
        }

        private IMessage _message;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                _message?.Dispose();
                _message = null;

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SolaceMessageWrapper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
