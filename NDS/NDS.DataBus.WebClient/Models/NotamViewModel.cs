﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NDS.DataBus.WebClient.Models
{
    public class NotamViewModel
    {
        public string Id { get; set; }
        public string Operator { get; set; }
        public string ItemA { get; set; }
        public string Fir { get; set; }
        public string Coordinates { get; set; }
        public string Body { get; set; }
        public string Metadata { get; set; }
    }
}