﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions  } from '@angular/http';
import { INotam } from './service.contracts';
import { Observable } from 'rxjs/Observable';
import '../rxjs-extensions';

@Injectable()
export class QueueService {
    private CreateTopicSubscriptionUrl = "api/topic/create";
    private DrainTopicUrl = "api/topic/drain";
    private UnsubscribeTopicUrl = "api/topic/stoplistening";

    constructor(private http: Http) {}

    createTopic(userName: string, password: string, topic: string, subscription: string, filters: string): Observable<string> {

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let request = { topic: topic, subscription: subscription , filters: filters };

        let url = `${this.CreateTopicSubscriptionUrl}?userName=${userName}&password=${password}&topic=${topic}&subscription=${subscription}&filters=${filters}`;

        return this.http
            .post(url, request, options)
            .map(this.extractSubscriptionId)
            .catch(this.handleError);       
    }

    unsubscribe(id: string): Observable<any> {
        let url = `${this.UnsubscribeTopicUrl}?id=${id}`;
        return this.http.post(url, {}).catch(this.handleError);       
    }

    drainQueue(id: string): Observable<INotam[]> {
        let url = `${this.DrainTopicUrl}?id=${id}`;
        return this.http
            .get(url)
            .map(this.extractNotams)
            .catch(this.handleError);       
    }

    private extractSubscriptionId(res: Response): string {
        return res.json() || "";
    }

    private extractNotams(res: Response): INotam[] {
        return res.json() || [];
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }
}