﻿using System;
using System.Collections.Generic;
using System.Linq;
using NDS.DataBus.Contracts;

namespace NDS.Tests
{
    public class DataBusMock : IDataBus
    {
        #region Test utilities

        public bool QueueExists(string name) => _queues.ContainsKey(name);

        #endregion

        public INdsMessage CreateMessage(string content, IEnumerable<Property> properties)
        {
            return new MessageMock(content, properties);
        }

        public IPostQueue CreateQueue(string name)
        {
            var queue = new QueueMock(name);
            _queues.Add(name, queue);
            return new PostQueueMock(queue);
        }

        public IPostQueue CreateTopic(string name)
        {
            throw new NotImplementedException();
        }

        public ISubscription ReadFromQueue(string name, Action<INdsMessage> callback)
        {
            return new SubscriptionMock(_queues[name], callback);
        }

        public ISubscription ReadFromTopic(string name, string subscriptionName, string filter, Action<INdsMessage> callback)
        {
            throw new NotImplementedException();
        }

        public IPostQueue TryPostToQueue(string name)
        {
            return _queues.ContainsKey(name) ? new PostQueueMock(_queues[name]) : null;
        }

        public IPostQueue TryPostToTopic(string name)
        {
            throw new NotImplementedException();
        }

        public void SimulateQueuePost(string queueName, MessageMock message)
        {
            _queues[queueName].Post(message);
        }

        public int GetQueueSubscriptionCount(string queueName)
        {
            var queue = _queues.ContainsKey(queueName) ? _queues[queueName] : null;
            return queue != null ? queue.Suspcriptions.Count : -1;
        }

        readonly Dictionary<string, QueueMock> _queues = new Dictionary<string, QueueMock>();
        public bool DeleteQueue(string path)
        {
            throw new NotImplementedException();
        }
    }

    public class QueueMock
    {
        public QueueMock(string name)
        {
            Name = name;
            Suspcriptions = new List<Action<INdsMessage>>();
            Messages = new List<INdsMessage>();
        }

        public string Name { get; }
        public List<Action<INdsMessage>> Suspcriptions { get; }
        public List<INdsMessage> Messages { get; }

        public void Post(INdsMessage message)
        {
            Messages.Add(message);
            Suspcriptions.ForEach(subs => subs.Invoke(message));
        }
    }

    public class MessageMock : INdsMessage
    {
        private List<Property> _properties;

        public MessageMock(string body, params Property[] properties)
        {
            Body = body;
            _properties = new List<Property>(properties);
        }

        public MessageMock(string body, IEnumerable<Property> properties)
        {
            Body = body;
            _properties = new List<Property>(properties);
        }

        public object this[string name] => _properties.Where(p => p.Name == name).Select(p => p.Value).FirstOrDefault();

        public string Body { get; set; }

        public IEnumerable<Property> Properties => _properties;

        public void Dispose()
        {
        }
    }

    public class PostQueueMock : IPostQueue
    {
        public PostQueueMock(QueueMock queue)
        {
            _queue = queue;
        }

        public void PostMessage(INdsMessage message)
        {
            _queue.Post(message);
        }

        public void Dispose()
        {
        }

        readonly QueueMock _queue;
    }

    public class SubscriptionMock : ISubscription
    {
        public SubscriptionMock(QueueMock queue, Action<INdsMessage> callback)
        {
            _queue = queue;
            _callback = callback;
            queue.Suspcriptions.Add(callback);
        }

        public void Unsubscribe()
        {
            _queue.Suspcriptions.Remove(_callback);
        }

        readonly QueueMock _queue;
        readonly Action<INdsMessage> _callback;

    }
}
