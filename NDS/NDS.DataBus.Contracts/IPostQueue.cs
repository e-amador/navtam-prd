﻿using System;

namespace NDS.DataBus.Contracts
{
    public interface IPostQueue : IDisposable
    {
        void PostMessage(INdsMessage message);
    }
}
