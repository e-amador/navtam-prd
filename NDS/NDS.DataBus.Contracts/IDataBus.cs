﻿using System;
using System.Collections.Generic;

namespace NDS.DataBus.Contracts
{
    public interface IDataBus
    {
        /// <summary>
        /// Creates a new queue
        /// </summary>
        /// <param name="name">Queue name</param>
        /// <returns></returns>
        IPostQueue CreateQueue(string name);
        /// <summary>
        /// Deletes Queue
        /// </summary>
        /// <param name="name">name/path of queue</param>
        /// <returns>If Queue is deleted, returns true</returns>
        bool DeleteQueue(string name);

        /// <summary>
        /// Post to an existing queue. 
        /// </summary>
        /// <param name="name">Queue name.</param>
        /// <returns>If queue does not exists returns null</returns>
        IPostQueue TryPostToQueue(string name);

        /// <summary>
        /// Creates a new topic
        /// </summary>
        /// <param name="name">Topic name</param>
        /// <returns></returns>
        IPostQueue CreateTopic(string name);

        /// <summary>
        /// Post to an existing topic. 
        /// </summary>
        /// <param name="name">Topic name.</param>
        /// <returns>If topic does not exists returns null</returns>
        IPostQueue TryPostToTopic(string name);

        /// <summary>
        /// Subscribe to an existing queue
        /// </summary>
        /// <param name="name">Topic name</param>
        /// <returns>Handle for the subscription</returns>
        ISubscription ReadFromQueue(string name, Action<INdsMessage> callback);

        /// <summary>
        /// Subscribe to an existing topic
        /// </summary>
        /// <param name="name">Topic name</param>
        /// <param name="subscriptionName">Subscription name (optional).</param>
        /// <param name="filter">Filter (TBD)</param>
        /// <returns>Handle for the subscription</returns>
        ISubscription ReadFromTopic(string name, string subscriptionName, string filter, Action<INdsMessage> callback);

        /// <summary>
        /// Creates a Message
        /// </summary>
        /// <param name="content">Message content as string.</param>
        /// <param name="properties">All the properties</param>
        /// <returns></returns>
        INdsMessage CreateMessage(string content, IEnumerable<Property> properties);
    }
}
