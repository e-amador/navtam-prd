﻿namespace NDS.DataBus.Contracts
{
    public interface ISubscription
    {
        void Unsubscribe();
    }
}
