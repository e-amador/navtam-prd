﻿using NSD.SolaceBus.Configuration;
using SolaceSystems.Solclient.Messaging;
using System;
using INdsMessage = NDS.DataBus.Contracts.IMessage;
using INdsSubscrition = NDS.DataBus.Contracts.ISubscription;

namespace NSD.SolaceBus
{
    public class SolaceTopicSubscription : INdsSubscrition
    {
        public SolaceTopicSubscription(SessionConfiguration config, string topicPath, Action<INdsMessage> callback)
        {
            _context = ContextFactory.Instance.CreateContext(new ContextProperties(), null);

            var sessionProperties = SessionUtils.NewSessionPropertiesFromConfig(config);
            sessionProperties.TopicDispatch = true; // This session property must be set to use topic dispatch capabilities
            sessionProperties.CalculateMessageExpiration = true;

            _session = _context.CreateSession(sessionProperties, MessageHandler, SessionHandler);
            if (_session.Connect() != ReturnCode.SOLCLIENT_OK)
                throw new Exception("Cannot start VMR session!");

            _topic = ContextFactory.Instance.CreateTopic(topicPath);

            if (_session.Subscribe(_topic, true) != ReturnCode.SOLCLIENT_OK)
                throw new Exception($"Cannot subscribe to topic {topicPath}!");

            _callback = callback;
        }

        private void SessionHandler(object sender, SessionEventArgs args)
        {
        }

        private void MessageHandler(object sender, MessageEventArgs args)
        {
            using (var message = args.Message)
            {
                _callback?.Invoke(new SolaceMessageWrapper(message));
            }
        }

        public void Unsubscribe()
        {
            try
            {
                _session?.Unsubscribe(_topic, true);
                _session?.Dispose();
                _session = null;

                _topic?.Dispose();
                _topic = null;

                _context?.Dispose();
                _context = null;

                _callback = null;
            }
            catch (Exception)
            {
                // log error
            }
        }

        private IContext _context;
        private ISession _session;
        private ITopic _topic;
        private Action<INdsMessage> _callback;
    }
}
