﻿using System.Collections.Generic;
using SolaceSystems.Solclient.Messaging;
using INdsMessage = NDS.DataBus.Contracts.IMessage;
using NdsProperty = NDS.DataBus.Contracts.Property;
using System.Text;
using System;

namespace NSD.SolaceBus
{
    public class SolaceMessageWrapper : INdsMessage
    {
        public SolaceMessageWrapper(IMessage message)
        {
            _message = message;
            Body = System.Text.Encoding.ASCII.GetString(message.XmlContent ?? message.BinaryAttachment);
        }

        public IMessage InnerMessage => _message;

        public SolaceMessageWrapper(string content, IEnumerable<NdsProperty> properties)
        {
            Body = content;
            _message = ContextFactory.Instance.CreateMessage();
            _message.XmlContent = Encoding.ASCII.GetBytes(content);
            _message.CreateUserPropertyMap();
            foreach (var prop in properties)
            {
                _message.UserPropertyMap.AddString(prop.Name, prop.Value.ToString());
            }
        }

        public object this[string name]
        {
            get
            {
                var props = _message.UserPropertyMap;
                if (props != null)
                {
                    try
                    {
                        return _message.UserPropertyMap.GetString(name);
                    }
                    catch (Exception)
                    {
                        return "";
                    }
                }
                return "";
            }
        }

        public string Body { get; set; }

        public IEnumerable<NDS.DataBus.Contracts.Property> Properties
        {
            get
            {
                // >> todo, implement properly!
                var id = this["id"].ToString();
                if (!string.IsNullOrEmpty(id))
                {
                    yield return NDS.DataBus.Contracts.Property.Create("id", id);
                }
            }
        }

        private IMessage _message;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                _message?.Dispose();
                _message = null;

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SolaceMessageWrapper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
