﻿using NDS.DataBus.Contracts;
using NSD.SolaceBus.Configuration;
using SolaceSystems.Solclient.Messaging;
using System;
using System.Collections.Generic;
using INdsMessage = NDS.DataBus.Contracts.IMessage;
using INdsSubscription = NDS.DataBus.Contracts.ISubscription;

namespace NSD.SolaceBus
{
    public class SolaceVMRWrapper : IDataBus, IDisposable
    {
        static SolaceVMRWrapper()
        {
            var factoryProps = new ContextFactoryProperties() { SolClientLogLevel = SolLogLevel.Error };
            _contextFactory.Init(factoryProps);
        }

        public SolaceVMRWrapper(string connectionString)
        {
            var dict = ParseConnectionString(connectionString);
            var address = dict["address"];
            var userName = dict["username"];
            var password = dict["password"];

            int? port = null;
            if (dict.ContainsKey("port"))
                port = int.Parse(dict["port"]);

            InitConfiguration(address, port, userName, password);
        }

        public SolaceVMRWrapper(string address, int? port, string userName, string password)
        {
            InitConfiguration(address, port, userName, password);
        }

        private void InitConfiguration(string address, int? port, string userName, string password)
        {
            _configuration = new SessionConfiguration();
            _configuration.IpPort = IpPort.Parse(port.HasValue ? $"{address}:{port.Value}" : address);
            _configuration.Compression = true;
            _configuration.DeliveryMode = MessageDeliveryMode.Persistent;
            _configuration.SetRouterUsername(UserVpn.Parse(userName));
            _configuration.SetUserPassword(password);
        }

        public INdsMessage CreateMessage(string content, IEnumerable<Property> properties)
        {
            return new SolaceMessageWrapper(content, properties);
        }

        public IPostQueue CreateQueue(string name)
        {
            throw new NotImplementedException();
        }

        public IPostQueue CreateTopic(string name)
        {
            return new SolaceTopicPoster(_configuration, name);
        }

        public bool DeleteQueue(string name)
        {
            throw new NotImplementedException();
        }

        public INdsSubscription ReadFromQueue(string name, Action<INdsMessage> callback)
        {
            throw new NotImplementedException();
        }

        public INdsSubscription ReadFromTopic(string name, string subscriptionName, string filter, Action<INdsMessage> callback)
        {
            return new SolaceTopicSubscription(_configuration, name, callback);
        }

        public IPostQueue TryPostToQueue(string name)
        {
            throw new NotImplementedException();
        }

        public IPostQueue TryPostToTopic(string name)
        {
            return new SolaceTopicPoster(_configuration, name);
        }

        private Dictionary<string, string> ParseConnectionString(string connectionString)
        {
            var dict = new Dictionary<string, string>();
            var props = (connectionString ?? "").Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var prop in props)
            {
                var cols = prop.Split('=');
                if (cols.Length == 2)
                {
                    dict.Add(cols[0], cols[1]);
                }
            }
            return dict;
        }

        private SessionConfiguration _configuration;

        static ContextFactory _contextFactory => ContextFactory.Instance;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                //_contextFactory.Cleanup(); // >> Dont!!

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SolaceVMRWrapper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
