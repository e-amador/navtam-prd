﻿using System;

namespace NavCanada.Core.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime RemoveSeconds(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0);
        }
    }
}