﻿namespace NavCanada.Core.Common.Contracts
{
    public interface IClusterQueues
    {
        IQueue TaskEngineQueue
        {
            get;
        }

        IQueue InitiatorQueue
        {
            get;
        }
    }
}
