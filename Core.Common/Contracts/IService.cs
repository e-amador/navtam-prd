﻿namespace NavCanada.Core.Common.Contracts
{
    public interface IService
    {
        void Start();

        void Stop();
    }
}
