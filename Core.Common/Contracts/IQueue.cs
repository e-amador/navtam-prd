﻿namespace NavCanada.Core.Common.Contracts
{
    using System.Collections.Generic;

    public interface IQueue
    {
        /// <summary>
        /// Push message into the queue (synchronously)
        /// </summary>
        /// <typeparam name="T">Type of the message beeing published. This type must be serializable</typeparam>
        /// <param name="message">Message</param>
        void Publish<T>(T message) where T : class;

        /// <summary>
        /// Message is retrieved from the queue (sync), it becomes invisible and locked for other processes and threads reading from the queue
        /// </summary>
        /// <typeparam name="T"></typeparam>
        T Retrieve<T>() where T : class;

        /// <summary>
        /// Message is retrieved from the queue (sync), it becomes invisible and locked for other processes and threads reading from the queue (there is no wait)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        T RetrieveNoWait<T>() where T : class;

        /// <summary>
        /// Batch of messages are retrieved from the queue, all messages become invisible and locked for other processes reading from the queue. Uses an internal transaction
        /// to control the locking
        /// </summary>
        /// <typeparam name="T"></typeparam>
        IList<T> RetrieveBatchWithInternalTransaction<T>(int maxCount) where T : class;

        /// <summary>
        /// Batch of messages are retrieved from the queue, all messages become invisible and locked for other processes reading from the queue. Uses an internal transaction
        /// to control the locking. Conversation handler will not be used to retrieve messages.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        IList<T> RetrieveBatchWithInternalTransactionWithoutConversation<T>(int maxCount) where T : class;

        /// <summary>
        /// Batch of messages are retrieved from the queue, all messages become invisible and locked for other processes reading from the queue. External open transactions will control the locking
        /// og the queue
        /// </summary>
        /// <typeparam name="T"></typeparam>
        IList<T> RetrieveBatch<T>(int maxCount) where T : class;
    }
}
