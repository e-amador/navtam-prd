﻿using System;

namespace Core.Common.Geography
{
    public class DMSValue
    {
        public int Degrees { get; }
        public int Minutes { get; }
        public int Seconds { get; }

        public DMSValue(int degrees, int minutes, int seconds)
        {
            Degrees = degrees;
            Minutes = minutes;
            Seconds = seconds;
        }

        public DMSValue Round()
        {
            var min = Seconds >= 30 ? Minutes + 1 : Minutes;
            var deg = Degrees;

            if (min > 59)
                deg = (Math.Sign(Degrees) > 0) ? deg = Degrees + 1 : Degrees - 1;

            return new DMSValue(deg, min % 60, 0);
        }

        public static DMSValue FromDecimal(string value)
        {
            return FromDecimal(double.Parse(value));
        }

        public static DMSValue FromDecimal(double value)
        {
            return Math.Abs(value) < 1000.0 ? FromDecimalDeg(value) : FromDecimalDegMin(value);
        }

        public static DMSValue FromDecimalDeg(double value)
        {
            var sign = Math.Sign(value);

            value = Math.Abs(value);
            var degrees = Math.Floor(value);

            value = (value - degrees) * 60;
            var minutes = Math.Floor(value);

            value = (value - minutes) * 60;
            var seconds = Math.Min(59, Math.Round(value));

            return new DMSValue((int)(sign * degrees), (int)minutes, (int)seconds);
        }

        public static DMSValue FromDecimalDegMin(double value)
        {
            var sign = Math.Sign(value);

            var degMin = (int)Math.Truncate(Math.Abs(value));

            var degrees = sign * (degMin / 100);
            var minutes = degMin % 100;

            var decMins = Math.Abs(value - Math.Truncate(value));
            var seconds = Math.Min(59, (int)Math.Round(decMins * 60));

            return new DMSValue(degrees, minutes, seconds);
        }

        public string AsLatitude()
        {
            return AsText("00", 'N', 'S');
        }

        public string AsLatitudeDM()
        {
            return AsTextDM("00", 'N', 'S');
        }

        public string AsLongitude()
        {
            return AsText("000", 'E', 'W');
        }

        public string AsLongitudeDM()
        {
            return AsTextDM("000", 'E', 'W');
        }

        public double AsDecimal => GetAsDecimal();

        public bool IsValidLatitude()
        {
            return IsValid(90);
        }

        public bool IsValidLongitude()
        {
            return IsValid(180);
        }

        private double GetAsDecimal()
        {
            var sign = Math.Sign(Degrees);
            return sign * ((double)Math.Abs(Degrees) + (double)Minutes / 60.0 + (double)Seconds / 3600.0);
        }

        private bool IsValid(int maxDegrees)
        {
            return IsValid(maxDegrees, Degrees, Minutes, Seconds);
        }

        private string AsText(string degMask, char positiveDirection, char negativeDirection)
        {
            var dir = Math.Sign(Degrees) > 0 ? positiveDirection : negativeDirection;
            return $"{Math.Abs(Degrees).ToString(degMask)}{Minutes.ToString("00")}{Seconds.ToString("00")}{dir}";
        }

        private string AsTextDM(string degMask, char positiveDirection, char negativeDirection)
        {
            var dir = Math.Sign(Degrees) > 0 ? positiveDirection : negativeDirection;
            return $"{Math.Abs(Degrees).ToString(degMask)}{Minutes.ToString("00")}{dir}";
        }

        internal static bool IsValid(int maxDegrees, int degrees, int minutes, int seconds)
        {
            var deg = degrees + Math.Sign(degrees) * (minutes > 0 ? 1 : seconds > 0 ? 1 : 0);
            return deg >= -maxDegrees && deg <= maxDegrees && minutes >= 0 && minutes <= 59 && seconds >= 0 && seconds <= 59;
        }
    }
}
