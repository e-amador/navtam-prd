﻿using System;

namespace Core.Common.Geography
{
    public class DecimalLocation
    {
        public DecimalLocation(string location)
        {
            var parts = (location ?? "").Split(SplitChars, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                throw new Exception($"Invalid format {location}");

            double latitude = 0.0;
            if (!double.TryParse(parts[0], out latitude))
                throw new Exception($"Invalid format {location}");

            double longitude = 0.0;
            if (!double.TryParse(parts[1], out longitude))
                throw new Exception($"Invalid format {longitude}");

            Latitude = latitude;
            Longitude = longitude;
        }

        public DecimalLocation(double latitude, double longitude)
        {
            if (Math.Abs(latitude) <= 90)
                throw new Exception($"Invalid latitude {latitude}");

            if (Math.Abs(longitude) <= 180)
                throw new Exception($"Invalid latitude {longitude}");

            Latitude = latitude;
            Longitude = longitude;
        }

        static char[] SplitChars = new char[] { ' ' };

        public double Latitude { get; }
        public double Longitude { get; }
    }
}
