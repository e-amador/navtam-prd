﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NavCanada.Core.Common.Geography
{
    public static class GeoRegionFixer
    {
        /// <summary>
        /// Process the text of a GeoRegion. For now it only supports POLYGON and MULTIPOLYGON regions.
        /// </summary>
        /// <param name="text">Region Text to parse and process.</param>
        /// <returns></returns>
        public static string Process(string text)
        {
            var multiPoly = GeoMultiPolygon.TryParse(text);
            if (multiPoly != null)
            {
                multiPoly.Normalize();
                return multiPoly.ToString();
            }

            var poly = GeoPolygon.TryParse(text);
            if (poly != null)
            {
                poly.Normalize();
                return poly.ToString();
            }

            return text;
        }
    }

    public abstract class GeoRegion
    {
        public abstract string Name { get; }

        public abstract int PointCount { get; }

        public abstract string RenderBody();

        public override string ToString()
        {
            return $"{Name} {RenderBody()}";
        }
    }

    public class GeoRing : GeoRegion
    {
        public override string Name => "RING";

        public override int PointCount => Points.Count;

        public GeoRing(IEnumerable<GeoPoint> points)
        {
            Points = points.ToList();
        }

        public static GeoRing Parse(string text, int from, int length)
        {
            return new GeoRing(ParsePoints(text, from, from + length - 1));
        }

        public List<GeoPoint> Points { get; private set; }

        public override string RenderBody()
        {
            var body = string.Join(", ", Points.Select(p => $"{CustomRound(p.X)} {CustomRound(p.Y)}"));
            return $"({body})";
        }

        public bool Normalize()
        {
            Points = Normalize(Points);
            return Points.Count >= 4;
        }

        static List<GeoPoint> Normalize(List<GeoPoint> points)
        {
            var trail = new List<GeoPoint>();
            GeoPoint last = null;
            foreach (var p in points)
            {
                // skip repeated points
                if (last != null && p.GetHashCode() == last.GetHashCode())
                    continue;

                last = p;
                trail.Add(p);

                RemoveInvalidTriangle(trail);
            }

            // ensure the polygon is closed
            if (trail.Count > 0 && trail[0].GetHashCode() != trail[trail.Count - 1].GetHashCode())
                trail.Add(trail[0]);

            return trail;
        }

        static void RemoveInvalidTriangle(List<GeoPoint> trail)
        {
            var count = trail.Count;
            if (count < 3)
                return;

            var a = trail[count - 3];
            var b = trail[count - 2];
            var c = trail[count - 1];

            if (a.BelongsToLine(b, c) || c.BelongsToLine(a, b))
            {
                trail.RemoveAt(count - 2);
                return;
            }

            var dab = a.Distance(b);
            var dbc = b.Distance(c);
            var dac = a.Distance(c);

            // 1.1132 m at the equator
            if (dab < 0.00001)
            {
                trail.RemoveAt(count - 2);
                return;
            }

            // 1.1132 m at the equator
            if (dac < 0.00001)
            {
                trail.RemoveAt(count - 2);
                return;
            }

            // 1.1132 m at the equator
            if (dbc < 0.00001)
            {
                trail.RemoveAt(count - 2);
                return;
            }

            // 5 * 1.1132 m at the equator
            var minHeigh = 5 * 0.00001;

            if (calcTriangleAltitude(dab, dbc, dac) < minHeigh || calcTriangleAltitude(dac, dab, dbc) < minHeigh || calcTriangleAltitude(dbc, dac, dab) < minHeigh)
            {
                trail.RemoveAt(count - 2);
                return;
            }
        }

        static double calcTriangleAltitude(double a, double b, double c)
        {
            var s = (a + b + c) / 2.0;
            return c > 0 ? 2.0 * Math.Sqrt(s * (s - a) * (s - b) * (s - c)) / c : 0.0;
        }

        static double PrecisionBase = 1000000.0;

        static double CustomRound(double d)
        {
            var intD = Math.Round(d * PrecisionBase);
            return intD / PrecisionBase;
        }

        static IEnumerable<GeoPoint> ParsePoints(string text, int from, int to)
        {
            var last = from;
            for (var i = from; i <= to; i++)
            {
                if (text[i] == ',' && i > last)
                {
                    yield return GeoPoint.Parse(text, last, i - last);
                    last = i + 1;
                }
            }
            if (last < to)
            {
                yield return GeoPoint.Parse(text, last, to - last + 1);
            }
        }
    }

    public class GeoPolygon : GeoRegion
    {
        public GeoPolygon(IEnumerable<GeoRing> rings)
        {
            Rings = rings.ToList();
        }

        public List<GeoRing> Rings { get; set; }

        public override string Name => "POLYGON";
        public override int PointCount => Rings.Sum(r => r.PointCount);

        public override string RenderBody()
        {
            var body = string.Join(", ", Rings.Select(r => r.RenderBody()));
            return $"({body})";
        }

        public static GeoPolygon Parse(string text, int from)
        {
            return new GeoPolygon(ParseRings(text, from));
        }

        public static GeoPolygon TryParse(string text)
        {
            var keyword = "POLYGON";
            return text.StartsWith(keyword) ? Parse(text, keyword.Length) : null;
        }

        public bool Normalize()
        {
            Rings = Rings.Where(r => r.Normalize()).ToList();
            return Rings.Count > 0;
        }

        static IEnumerable<GeoRing> ParseRings(string text, int from)
        {
            var len = text.Length;
            var opens = 0;
            var last = from;
            for (int i = from; i < len; i++)
            {
                switch (text[i])
                {
                    case '(':
                        if (++opens == 2)
                            last = i + 1;
                        break;
                    case ')':
                        opens--;
                        if (opens == 1)
                            yield return GeoRing.Parse(text, last, i - last);
                        if (opens == 0)
                            yield break;
                        break;
                }
            }
        }
    }

    public class GeoMultiPolygon : GeoRegion
    {
        public GeoMultiPolygon(IEnumerable<GeoPolygon> polys)
        {
            Polygons = polys.ToList();
        }

        public List<GeoPolygon> Polygons { get; set; }

        public override string Name => "MULTIPOLYGON";
        public override int PointCount => Polygons.Sum(p => p.PointCount);

        public override string RenderBody()
        {
            var body = string.Join(", ", Polygons.Select(p => p.RenderBody()));
            return $"({body})";
        }

        public static GeoMultiPolygon Parse(string text, int from)
        {
            return new GeoMultiPolygon(ParsePolygons(text, from));
        }

        public static GeoMultiPolygon TryParse(string text)
        {
            var keyword = "MULTIPOLYGON";
            return text.StartsWith(keyword) ? Parse(text, keyword.Length) : null;
        }

        public bool Normalize()
        {
            Polygons = Polygons.Where(p => p.Normalize()).ToList();
            return Polygons.Count > 0;
        }

        static IEnumerable<GeoPolygon> ParsePolygons(string text, int from)
        {
            var len = text.Length;
            var opens = 0;
            var last = from;
            for (int i = from; i < len; i++)
            {
                switch (text[i])
                {
                    case '(':
                        if (++opens == 2)
                            last = i;
                        break;
                    case ')':
                        opens--;
                        if (opens == 1)
                            yield return GeoPolygon.Parse(text, last);
                        if (opens == 0)
                            yield break;
                        break;
                }
            }
        }
    }

    public class GeoPoint
    {
        public GeoPoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; set; }
        public double Y { get; set; }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + (int)(1000000 * X); // X.GetHashCode();
                hash = hash * 23 + (int)(1000000 * Y); //Y.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"({X},{Y})";
        }

        public double Distance(GeoPoint other)
        {
            var deltaX = X - other.X;
            var deltaY = Y - other.Y;
            return Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
        }

        public bool BelongsToLine(GeoPoint a, GeoPoint b, double tolerance = 0.0000001)
        {
            var distA = Distance(a);
            var distB = Distance(b);
            var distAB = a.Distance(b);
            return distA + distB - distAB < tolerance;
        }

        public static GeoPoint Parse(string text, int from, int length)
        {
            var pointText = text.Substring(from, length);
            var coords = pointText.Split(CoordSeparator, StringSplitOptions.RemoveEmptyEntries);

            if (coords.Length != 2)
                throw new Exception($"Invalid point format: {pointText}");

            return new GeoPoint(double.Parse(coords[0]), double.Parse(coords[1]));
        }

        static char[] CoordSeparator = new char[] { ' ' };
    }
}
