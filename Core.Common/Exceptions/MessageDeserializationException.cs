﻿namespace NavCanada.Core.Common.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class MessageDeserializationException : Exception
    {
        public MessageDeserializationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }

        public MessageDeserializationException(string message, Exception innerException, byte[] rawMessageBytes)
            : base(message, innerException)
        {
            RawMessageBytes = rawMessageBytes;
        }

        public MessageDeserializationException(string message, byte[] rawMessageBytes)
            : base(message)
        {
            RawMessageBytes = rawMessageBytes;
        }

        public MessageDeserializationException(byte[] rawMessageBytes)
        {
            RawMessageBytes = rawMessageBytes;
        }

        public byte[] RawMessageBytes { get; private set;
        }
    }
}
