﻿using System;

namespace NavCanada.Core.Common.Airac
{
    public static class AIRACUtils
    {
        static DateTime _refDate;

        static AIRACUtils()
        {
            _refDate = new DateTime(2019, 1, 3);
        }

        public static DateTime GetActiveAIRACCycleDate(DateTime date)
        {
            var bareDate = new DateTime(date.Year, date.Month, date.Day);
            return bareDate < _refDate ? GetAIRACCycleBackward(bareDate) : GetAIRACCycleForward(bareDate);
        }

        const int CycleDays = 28;

        static DateTime GetAIRACCycleForward(DateTime date)
        {
            var current = _refDate;

            while (current != date && current.AddDays(CycleDays) <= date)
                current = current.AddDays(CycleDays);

            return current;
        }

        static DateTime GetAIRACCycleBackward(DateTime date)
        {
            var current = _refDate;

            while (current > date)
                current = current.AddDays(-CycleDays);

            return current;
        }
    }
}
