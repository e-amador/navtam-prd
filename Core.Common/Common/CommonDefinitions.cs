﻿namespace NavCanada.Core.Common.Common
{
    public static class CommonDefinitions
    {
        // make sure to update the db seed function and/or the database if you need to change any of the following definitions
        public static string AdminRole => "Administrator";
        public static string OrgAdminRole => "Organization Administrator";
        public static string CcsRole => "CCS";
        public static string UserRole => "User";
        public static string AdminUser => "Admin";
        public static string BackendUser => "Backend";
        public static string CcsUser => "CCS";
        public static string NofUser => "nof";
        public static string NofOrganization => "NOF";
        public static string CanCatchAll => "CanCatchAll";
        public static string NavcanadaOrg => "NAV CANADA";
        public static string AdminDefaultPassword => "password";

        /* Nav Group */
        public static string NavUserRole => "NAV_ROLE";

        /* Nof Group*/
        public static string NofUserRole => "NOF_ROLE";
        public static string NofAdminRole => "NOF_ADMINISTRATOR";

        /* Developer Group*/
        public static string DeveloperRole => "Developer";
        public static string DeveloperPassword => "Backend12341!";

        /* Fic Group */
        public static string FicUserRole => "FIC_ROLE";

        // vsp names
        public static string NOF_VSP => "NOF_VSP";
        public static string USER_VSP => "USER_VSP";

        public static string ForbiddenWordsConfigName => "NOTAMForbiddenWords";

        // geography srid https://docs.microsoft.com/en-us/sql/relational-databases/spatial/spatial-reference-identifiers-srids
        public static int Srid => 4326;

        // checklist
        public static string CheckListPubMessage => "CheckListPubMessage";

        //Default Minutes to Expire a NOTAM
        public static int DefaultExpireMinutes = 50;

        // nautical miles in meters
        public static double NAUTICAL_MILES_IN_METERS_25 = 46300;
    }
}
