﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace NavCanada.Core.Common.Common
{
    public static class AppSettings
    {
        /// <summary>
        /// Return type from configuration/application settings. Usage -> AppSettings<T>.Get(keyName);
        /// If the configuration setting is null it returns the default value of the type.
        /// </summary>
        /// <typeparam name="T">Type you want to expect to return</typeparam>
        /// <param name="keyName">key name of the configuration/application settings</param>
        /// <returns>typed value from the configuration/application settings</returns>
        public static T Get<T>(string keyName)
        {
            var configurationSetting = ConfigurationManager.AppSettings[keyName];
            if (configurationSetting == null) return default(T);

            var convert = TypeDescriptor.GetConverter(typeof(T));
            return (T)(convert.ConvertFromInvariantString(configurationSetting));
        }
    }
}


