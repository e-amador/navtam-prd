﻿namespace NavCanada.Core.Common.Common
{
    public static class ScheduleIdentity
    {
        public static string AeroRdsEffectiveCacheSyncScheduler => "AERORDS_CACHE_SYNC";
        public static string AeroRdsEffectiveCacheLoadScheduler => "AERORDS_CACHE_LOAD";
        public static string ExpiredProposalScheduler => "EXPIRED_PROPOSAL";
        public static string ChecklistScheduler => "GENERATE_CHECKLIST";
        public static string CheckSeriesNumbersScheduler => "CHECK_SERIES_NUMBERS";
        public static string HubConnectionTestScheduler => "HUB_CONNECTION_TEST";
        public static string NdsQueryGuard => "NDS_QUERY_GUARD";
        public static string BroadcastNotamsToSubscribers=> "BROADCAST_NOTAMS_TO_SUBSCRIBERS";
    }
}
