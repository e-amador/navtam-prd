﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Common.Common
{
    public class FilterCondition
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public object Value { get; set; }
    }

    public class FilterCriteria
    {
        public QueryOptions QueryOptions { get; set; }
        public List<FilterCondition> Conditions { get; set; }
    }

    public class ReportFilterCriteria
    {
        public QueryOptions QueryOptions { get; set; }
        public List<FilterCondition> Conditions { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string filterBy { get; set; }
    }
}
