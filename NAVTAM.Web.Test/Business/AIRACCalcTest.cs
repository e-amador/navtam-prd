﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Common.Airac;
using System;

namespace NAVTAM.Web.Test.Business
{
    [TestClass]
    public class AIRACCalcTest
    {
        [TestMethod]
        public void AIRACCalc_Forward()
        {
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2019, 1, 3)), new DateTime(2019, 1, 3));
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2019, 3, 28)), new DateTime(2019, 3, 28));
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2019, 10, 20)), new DateTime(2019, 10, 10));
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2019, 12, 5)), new DateTime(2019, 12, 5));
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2021, 1, 10)), new DateTime(2020, 12, 31));
        }

        [TestMethod]
        public void AIRACCalc_Backward()
        {
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2018, 10, 11)), new DateTime(2018, 10, 11));
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2003, 1, 23)), new DateTime(2003, 1, 23));
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2005, 9, 20)), new DateTime(2005, 9, 1));
            Assert.AreEqual(AIRACUtils.GetActiveAIRACCycleDate(new DateTime(2012, 7, 25)), new DateTime(2012, 6, 28));
        }
    }
}
