﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NAVTAM.Helpers;

namespace NAVTAM.Web.Test.Helpers
{
    [TestClass]
    public class AftnHelperTests
    {
        [TestMethod]
        public void AftnHelper_valid_chars_test()
        {
            AftnHelper.ValidateAftnChars(" ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-?:().,'=/+\"", "");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void AftnHelper_invalid_chars_test()
        {
            AftnHelper.ValidateAftnChars("abcdefghijklmnñáéíúó", "{0}");
        }
    }
}
