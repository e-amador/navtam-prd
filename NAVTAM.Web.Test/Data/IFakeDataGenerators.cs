﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.CARSCLSD.V10;
using NAVTAM.NsdEngine.CATCHALL.V10;
using System.Collections.Generic;

namespace NAVTAM.Web.Test.Data
{
    public interface IFakeDataGenerators
    {
        List<Doa> Doas { get; }
        List<ConfigurationValue> ConfigurationValuesData { get; }
        List<SdoCache> SdoCacheData { get; }
        List<UserProfile> UserProfiles { get; }
        List<IcaoSubjectCondition> IcaoSubjectConditions { get; }
        List<IcaoSubject> IcaoSubjects { get; }
        List<AerodromeDisseminationCategory> AerodromeDisseminationCategories { get; }
        List<BilingualRegion> BilingualRegions { get; }
        List<GeoRegion> GeoRegions { get; }
        List<SeriesAllocation> SeriesAllocations { get; }
        List<Proposal> Proposals { get; }
        List<ProposalHistory> ProposalHistories { get; }
        List<SeriesNumber> SeriesNumbers { get; }
        List<Notam> DisseminatedNotams { get; }

        List<Notam> DisseminatedNotamGenerator();
        List<SeriesNumber> SeriesNumberGenerator();
        List<ProposalHistory> ProposalHistoryGenerator();
        List<Proposal> ProposalGenerator();
        List<SeriesAllocation> SeriesAllocationGenerator();
        List<GeoRegion> GeoRegionGenerator();
        List<BilingualRegion> BilingualRegionGenerator();
        List<AerodromeDisseminationCategory> AerodromeDisseminationCategoryGenerator();
        List<IcaoSubjectCondition> IcaoSubjectConditionGenerator();
        List<IcaoSubject> IcaoSubjectDataGenerator();
        List<Doa> DoaTestDataGenerator();
        List<ConfigurationValue> ConfigurationValueDataGenerator();
        List<SdoCache> SdoCacheDataGenerator();
        List<UserProfile> UserProfileDataGenerator();
        ProposalViewModel GenerateProposalViewModel(); //string subjectId = "Ahp4582203"

        //CatchAllViewModel GenerateTokens(int icaoSubjectId, int icaoConditionId, bool[] purposes, bool[] traffics, bool immediate, bool permanent,
        //    int itemFValue = 1, UnitOfMeasure itemFUnits = UnitOfMeasure.AMSL, bool itemFSurface = false,
        //    int itemGValue = 2, UnitOfMeasure itemGUnits = UnitOfMeasure.AMSL, bool itemGUnlimited = false,
        //    string subjectId = "Ahp4582203",
        //    string subjectLocation = "491140N 1231102W", int startDayOffset = 1);

        ProposalContext GenerateMockProposalContext(IUow uow, int doaId, int vsp, string subjectId, IAeroRdsProxy aeroRdsProxy);

        ProposalContext GenerateMockProposalContextWithErrorOnSubjectType(IUow uow, int doaId, int vsp, string subjectId, IAeroRdsProxy aeroRdsProxy);
    }
}
