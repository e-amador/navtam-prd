﻿using Moq;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using NAVTAM.Helpers;
using System;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;

namespace NAVTAM.Web.Test.Data
{
    public class MockCatchAllRepoGenerator : IMockRepoGenerator
    {
        private Mock<ISdoCacheRepo> _mockSdoSubjectRepo;
        private Mock<IConfigurationValueRepo> _mockConfigurationValueRepo;
        private Mock<IUserProfileRepo> _mockUserProfileRepo;
        private Mock<IIcaoSubjectRepo> _mockIcaoSubjectRepo;
        private Mock<IDoaRepo> _mockDoaRepo;
        private Mock<IIcaoSubjectConditionRepo> _mockIcaoConditionRepo;
        private Mock<IProposalRepo> _mockProposalRepo;
        private Mock<IProposalHistoryRepo> _mockProposalHistoryRepo;
        private Mock<ISeriesNumberRepo> _mockSeriesNumberRepo;
        private Mock<IOrganizationNsdRepo> _mockOrganizationNsdRepo;
        private Mock<IOrganizationRepo> _mockOrganizationRepo;
        private Mock<ISeriesChecklistRepo> _mockSeriesChecklistRepo;
        private Mock<ISystemStatusRepo> _mockSystemStatusRepo;

        private Mock<INotamRepo> _mockDisseminatedNotamRepo;

        private FakeCatchAllDataGenerators _dataGenerator;

        public MockCatchAllRepoGenerator()
        {
            _dataGenerator = new FakeCatchAllDataGenerators();
        }

        public MockCatchAllRepoGenerator(FakeCatchAllDataGenerators dataGenerator)
        {
            if (dataGenerator == null) _dataGenerator = new FakeCatchAllDataGenerators();
            else _dataGenerator = dataGenerator;
        }

        public FakeCatchAllDataGenerators DataGenerator
        {
            get { return _dataGenerator; }
        }

        public Mock<IGeoLocationCache> MockGeoLocationCache
        {
            get
            {
                var mock = new Mock<IGeoLocationCache>();
                var geo = DbGeography.PolygonFromText("POLYGON((-150 40, 150 40, 150 80, -150 80, -150 40))", CommonDefinitions.Srid);

                mock.Setup(e => e.GetUserDoa(It.IsAny<IUow>(), It.IsAny<string>())).Returns((IUow uow, string user) => Task.FromResult(geo));

                return mock;
            }
        }

        public Mock<ISdoCacheRepo> MockSdoCacheRepo
        {
            get
            {
                if (_mockSdoSubjectRepo == null)
                {
                    //SdoSubjectRepo Setup
                    _mockSdoSubjectRepo = new Mock<ISdoCacheRepo>();
                    _mockSdoSubjectRepo.Setup(sdo => sdo.GetById(It.IsAny<string>()))
                        .Returns((string id) =>
                        {
                            return _dataGenerator.SdoSubjectsData.SingleOrDefault(x => x.Id == id);
                        });
                    _mockSdoSubjectRepo.Setup(sdo => sdo.GetByIdAsync(It.IsAny<string>()))
                        .Returns((string id) =>
                        {
                            return Task.FromResult(_dataGenerator.SdoSubjectsData.SingleOrDefault(x => x.Id == id));
                        });
                    _mockSdoSubjectRepo.Setup(sdo => sdo.GetByDesignatorAsync(It.IsAny<string>()))
                        .Returns((string designator) =>
                        {
                            return Task.FromResult(_dataGenerator.SdoSubjectsData.FirstOrDefault(x => x.Designator == designator));
                        });
                }
                return _mockSdoSubjectRepo;
            }
            set { _mockSdoSubjectRepo = value; }
        }

        public Mock<IConfigurationValueRepo> MockConfigurationValueRepo
        {
            get
            {
                if (_mockConfigurationValueRepo == null)
                {
                    //ConfigurationValueRepo Setup
                    _mockConfigurationValueRepo = new Mock<IConfigurationValueRepo>();
                    _mockConfigurationValueRepo.Setup(cv => cv.GetByCategoryandNameAsync(It.IsAny<string>(), It.IsAny<string>()))
                        .Returns((string category, string name) =>
                        {
                            return Task.FromResult(_dataGenerator.ConfigurationValuesData.SingleOrDefault(e => e.Category == category && e.Name == name));
                        });
                    _mockConfigurationValueRepo
                        .Setup(cv => cv.GetAll())
                        .Returns(_dataGenerator.ConfigurationValuesData.AsQueryable());

                    _mockConfigurationValueRepo
                        .Setup(cv => cv.GetValueAsync(It.IsAny<string>(), It.IsAny<string>()))
                        .Returns((string n, string v) =>
                        {
                            return Task.FromResult("ABCDEFGHJKLMNOPQRSTUVWXYZ");
                        });
                }
                return _mockConfigurationValueRepo;
            }
            set { _mockConfigurationValueRepo = value; }
        }

        public Mock<ISeriesChecklistRepo> MockSeriesChecklistRepo
        {
            get
            {
                if (_mockSeriesChecklistRepo == null)
                {
                    _mockSeriesChecklistRepo = new Mock<ISeriesChecklistRepo>();
                    _mockSeriesChecklistRepo.Setup(sc => sc.GetSeriesInUseAsync())
                        .Returns(() => {
                            return Task.FromResult("ABCDEFGHJKLMNOPQRSTUVWXYZ");
                        });
                }
                return _mockSeriesChecklistRepo;
            }
            set
            {
                _mockSeriesChecklistRepo = value;
            }
        }

        public Mock<ISystemStatusRepo> MockSystemStatusRepo
        {
            get
            {
                if (_mockSystemStatusRepo == null)
                {
                    _mockSystemStatusRepo = new Mock<ISystemStatusRepo>();
                    _mockSystemStatusRepo.Setup(ss => ss.GetStatus())
                        .Returns(() => new SystemStatus { Id = 0, Status = NavCanada.Core.Domain.Model.Enums.SystemStatusEnum.Ok, LastUpdated = DateTime.Now });
                }
                return _mockSystemStatusRepo;
            }
            set
            {
                _mockSystemStatusRepo = value;
            }
        }

        public Mock<IUserProfileRepo> MockUserProfileRepo
        {
            get
            {
                if (_mockUserProfileRepo == null)
                {
                    //UserProfileRepo
                    _mockUserProfileRepo = new Mock<IUserProfileRepo>();
                    _mockUserProfileRepo.Setup(up => up.GetByIdAsync(It.IsAny<string>(), It.IsAny<bool>()))
                        .Returns((string uId, bool includeRegistration) =>
                        {
                            return Task.FromResult(_dataGenerator.UserProfiles.SingleOrDefault(u => u.Id == uId));
                        });
                }
                return _mockUserProfileRepo;
            }
            set { _mockUserProfileRepo = value; }
        }

        public Mock<IOrganizationRepo> MockOrganizationRepo
        {
            get {
                if (_mockOrganizationRepo == null)
                {
                    //UserProfileRepo
                    _mockOrganizationRepo = new Mock<IOrganizationRepo>();
                    _mockOrganizationRepo.Setup(up => up.GetByIdAsync(It.IsAny<int>(), It.IsAny<bool>()))
                        .Returns((int uId) =>
                        {
                            return Task.FromResult(_dataGenerator.Organizations.SingleOrDefault(u => u.Id == uId));
                        });
                }
                return _mockOrganizationRepo;
            }
            set { _mockOrganizationRepo = value;  }
        }
        public Mock<IIcaoSubjectRepo> MockIcaoSubjectRepo
        {
            get
            {
                if (_mockIcaoSubjectRepo == null)
                {
                    _mockIcaoSubjectRepo = new Mock<IIcaoSubjectRepo>();
                    _mockIcaoSubjectRepo
                        .Setup(i => i.GetByIdAsync(It.IsAny<int>()))
                        .Returns((long id) =>
                        {
                            return Task.FromResult(_dataGenerator.IcaoSubjects.SingleOrDefault(x => x.Id == id));
                        });
                }
                return _mockIcaoSubjectRepo;
            }
            set { _mockIcaoSubjectRepo = value; }
        }

        public Mock<IDoaRepo> MockDoaRepo
        {
            get
            {
                if (_mockDoaRepo == null)
                {
                    _mockDoaRepo = new Mock<IDoaRepo>();
                    _mockDoaRepo
                        .Setup(d => d.GetById(It.IsAny<long>()))
                        .Returns((long id) =>
                        {
                            return _dataGenerator.Doas.SingleOrDefault(x => x.Id == id);
                        });
                }
                return _mockDoaRepo;
            }
            set { _mockDoaRepo = value; }
        }

        public Mock<IIcaoSubjectConditionRepo> MockIcaoConditionRepo
        {
            get
            {
                if (_mockIcaoConditionRepo == null)
                {
                    _mockIcaoConditionRepo = new Mock<IIcaoSubjectConditionRepo>();
                    _mockIcaoConditionRepo
                        .Setup(ic => ic.GetByIdAsync(It.IsAny<int>()))
                        .Returns((long id) =>
                        {
                            return Task.FromResult(_dataGenerator.IcaoSubjectConditions.SingleOrDefault(x => x.Id == id));
                        });
                }
                return _mockIcaoConditionRepo;
            }
            set { _mockIcaoConditionRepo = value; }
        }

        public Mock<IProposalRepo> MockProposalRepo
        {
            get
            {
                if (_mockProposalRepo == null)
                {
                    _mockProposalRepo = new Mock<IProposalRepo>();
                    _mockProposalRepo.Setup(pr => pr.GetByIdAsync(It.IsAny<int>())).Returns((int id) =>
                    {
                        var p = _dataGenerator.Proposals.Find(x => x.Id == id);
                        Proposal np = null;
                        if (p != null)
                        {
                            np = new Proposal();
                            p.CopyPropertiesTo(np);
                        }
                        return Task.FromResult(np);
                    });
                    _mockProposalRepo.Setup(pr => pr.Update(It.IsAny<Proposal>())).Callback((Proposal prop) =>
                    {
                        var p = _dataGenerator.Proposals.Find(x => x.Id == prop.Id);
                        if (p == null)
                        {
                            var r = _dataGenerator.Proposals.Max(x => x.Id);
                            prop.Id = r + 1;
                        }
                        _dataGenerator.Proposals.Remove(p);
                        _dataGenerator.Proposals.Add(prop);
                    });
                    _mockProposalRepo.Setup(pr => pr.Add(It.IsAny<Proposal>())).Callback((Proposal prop) =>
                    {
                        //Find a new Id
                        var r = _dataGenerator.Proposals.Max(x => x.Id);
                        prop.Id = r + 1;
                        _dataGenerator.Proposals.Add(prop);
                    });
                }
                return _mockProposalRepo;
            }
            set { _mockProposalRepo = value; }
        }

        public Mock<IProposalHistoryRepo> MockProposalHistoryRepo
        {
            get
            {
                if (_mockProposalHistoryRepo == null)
                {
                    _mockProposalHistoryRepo = new Mock<IProposalHistoryRepo>();
                    _mockProposalHistoryRepo.Setup(pr => pr.Update(It.IsAny<ProposalHistory>())).Callback((ProposalHistory prop) =>
                    {
                        //Do the update
                        var ph = _dataGenerator.ProposalHistories.Find(x => x.Id == prop.Id);
                        _dataGenerator.ProposalHistories.Remove(ph);
                        _dataGenerator.ProposalHistories.Add(prop);
                    });
                    _mockProposalHistoryRepo.Setup(pr => pr.Add(It.IsAny<ProposalHistory>())).Callback((ProposalHistory prop) =>
                    {
                        //Do the add
                        var r = _dataGenerator.ProposalHistories.Max(x => x.Id);
                        prop.Id = r + 1;
                        _dataGenerator.ProposalHistories.Add(prop);
                    });
                    _mockProposalHistoryRepo.Setup(pr => pr.GetLatestByProposalIdAsync(It.IsAny<int>())).Returns((int pid) =>
                    {
                        var p = _dataGenerator.ProposalHistories.Find(x => x.ProposalId == pid);
                        ProposalHistory np = null;
                        if (p != null)
                        {
                            np = new ProposalHistory();
                            p.CopyPropertiesTo(np);
                        }
                        return Task.FromResult(np);
                    });
                }
                return _mockProposalHistoryRepo;
            }
            set { _mockProposalHistoryRepo = value; }
        }

        public Mock<ISeriesNumberRepo> MockSeriesNumberRepo
        {
            get
            {
                if (_mockSeriesNumberRepo == null)
                {
                    _mockSeriesNumberRepo = new Mock<ISeriesNumberRepo>();
                    _mockSeriesNumberRepo.Setup(snr => snr.GetSeriesNumberAsync(It.IsAny<char>(), It.IsAny<int>()))
                        .Returns((char serie, int year) =>
                        {
                            return Task.FromResult(_dataGenerator.SeriesNumbers.Find(x => x.Series == serie.ToString() && x.Year == year));
                        });
                    _mockSeriesNumberRepo.Setup(snr => snr.GetSeriesNumber(It.IsAny<char>(), It.IsAny<int>()))
                        .Returns((char serie, int year) =>
                        {
                            return _dataGenerator.SeriesNumbers.Find(x => x.Series == serie.ToString() && x.Year == year);
                        });
                    _mockSeriesNumberRepo.Setup(snr => snr.Add(It.IsAny<SeriesNumber>())).Callback((SeriesNumber serie) =>
                    {
                        var r = _dataGenerator.SeriesNumbers.Max(x => x.Id);
                        serie.Id = r + 1;
                        _dataGenerator.SeriesNumbers.Add(serie);
                    });
                    _mockSeriesNumberRepo.Setup(snr => snr.Update(It.IsAny<SeriesNumber>())).Callback((SeriesNumber sn) =>
                    {
                        var tsn = _dataGenerator.SeriesNumbers.Find(x => x.Series == sn.Series && x.Year == sn.Year);
                        tsn.Number = sn.Number;
                    });
                }
                return _mockSeriesNumberRepo;
            }
            set { _mockSeriesNumberRepo = value; }
        }

        public Mock<IOrganizationNsdRepo> MockOrganizationNsdRepo
        {
            get
            {
                if (_mockOrganizationNsdRepo == null)
                {
                    _mockOrganizationNsdRepo = new Mock<IOrganizationNsdRepo>();
                    _mockOrganizationNsdRepo.Setup(e => e.OrganizationContainsNsd(It.IsAny<int>(), It.IsAny<int>())).Returns((int org, int nsd) => Task.FromResult(true));
                }
                return _mockOrganizationNsdRepo;
            }
        }

        public Mock<INotamRepo> MockDisseminatedNotamRepo
        {
            get
            {
                if (_mockDisseminatedNotamRepo == null)
                {
                    _mockDisseminatedNotamRepo = new Mock<INotamRepo>();
                    _mockDisseminatedNotamRepo.Setup(dnr => dnr.Add(It.IsAny<Notam>()))
                        .Callback((Notam dn) =>
                        {
                            //var maxId = 0;
                            //if (_dataGenerator.DisseminatedNotams.Count > 0)
                            //    maxId = _dataGenerator.DisseminatedNotams.Max(x => x.Id);
                            //dn.Id = maxId + 1;
                            dn.Id = Guid.NewGuid();
                            _dataGenerator.DisseminatedNotams.Add(dn);
                        });
                }
                return _mockDisseminatedNotamRepo;
            }
            set { _mockDisseminatedNotamRepo = value; }
        }

    }
}
