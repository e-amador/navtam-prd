﻿using Moq;
using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;

namespace NAVTAM.Web.Test.Data
{
    public interface IMockRepoGenerator
    {
        Mock<ISdoCacheRepo> MockSdoCacheRepo { get; }
        Mock<IConfigurationValueRepo> MockConfigurationValueRepo { get; }
        Mock<IUserProfileRepo> MockUserProfileRepo { get; }
        Mock<IIcaoSubjectRepo> MockIcaoSubjectRepo { get; }
        Mock<IDoaRepo> MockDoaRepo { get; }
        Mock<IIcaoSubjectConditionRepo> MockIcaoConditionRepo { get; }
        Mock<IProposalRepo> MockProposalRepo { get; }
        Mock<IProposalHistoryRepo> MockProposalHistoryRepo { get; }
        Mock<ISeriesNumberRepo> MockSeriesNumberRepo { get; }
        Mock<INotamRepo> MockDisseminatedNotamRepo { get; }
        Mock<IOrganizationRepo> MockOrganizationRepo { get;  }
        Mock<ISeriesChecklistRepo> MockSeriesChecklistRepo { get; }
    }
}
