﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.NsdEngine;
using NAVTAM.ViewModels;
using System;
using System.Threading.Tasks;

namespace NAVTAM.Web.Test.NsdEngineTest
{
    [TestClass]
    public class NsdManagerTest : NsdManager
    {
        private Mock<IAeroRdsProxy> _aeroRdsProxy = new Mock<IAeroRdsProxy>();

        [TestInitialize]
        public void InitializeTest()
        {
        }

        [TestMethod]
        public void NsdManager_GetItemEPrefix_WhenIsHeliportAndBilingualRegion_ShouldPass()
        {
            var response = GetItemEPrefix("EAL1", "CYOW", "OTTAWA", false, true);
            Assert.IsTrue(response.English == "EAL1 OTTAWA/CYOW (HELI)\n");
            Assert.IsTrue(response.French == "EAL1 OTTAWA/CYOW (HELI)\n");
        }

        [TestMethod]
        public void NsdManager_GetItemEPrefix_WhenCodeIsNotHeliportInWaterAndBilingualRegion_ShouldPass()
        {
            var response = GetItemEPrefix("EAL1", "CYOW", "OTTAWA", true, false);
            Assert.IsTrue(response.English == "EAL1 OTTAWA/CYOW (WATER)\n");
            Assert.IsTrue(response.French == "EAL1 OTTAWA/CYOW (HYDRO)\n");
        }


        [TestMethod]
        public void NsdManager_GetItemEPrefix_WhenCodeIsNotHeliportNotInWaterAndBilingualRegion_ShouldPass()
        {
            var response = GetItemEPrefix("EAL1", "CYOW", "OTTAWA", false, false);
            Assert.IsTrue(response.English == "EAL1 OTTAWA/CYOW \n");
            Assert.IsTrue(response.French == response.English);
        }

        [TestMethod]
        public void NsdManager_GetItemEPrefix_WhenIsICaoAirport_ShouldReturnNull()
        {
            var response = GetItemEPrefix("EAL", "CYOW", "OTTAWA", false, false);
            Assert.IsTrue(response.English == null);
            Assert.IsTrue(response.French == null);
        }

        public override TokenViewModel DeserializeToken(string tokens)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Implementation of Abstract Methods to make compiler :)
        /// <returns></returns>

        protected override TokenViewModel CreateTokenViewModel()
        {
            throw new NotImplementedException();
        }

        protected override Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            throw new NotImplementedException();
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        protected override string GetPurpose(TokenViewModel token, ProposalContext context)
        {
            throw new NotImplementedException();
        }

        public override TokenViewModel CopyTokenForGroupCancellation(TokenViewModel master, TokenViewModel current)
        {
            throw new NotImplementedException();
        }

        protected override string GetDisplayName()
        {
            throw new NotImplementedException();
        }

        protected override string GetNsdHelpLink()
        {
            throw new NotImplementedException();
        }
    }
}
