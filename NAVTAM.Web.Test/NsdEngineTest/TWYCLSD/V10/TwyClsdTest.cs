﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Controllers.api;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using NAVTAM.SignalrHub;
using NAVTAM.Web.Test.Data.TwyClsd;
using System.Threading.Tasks;
using System.Web.Http.Results;
using NavCanada.Core.Common.Common;
using System.Linq;
using System;
using NAVTAM.NsdEngine.TWYCLSD.V10;
using System.Data.Entity.Spatial;
using NavCanada.Core.Common.Contracts;

namespace NAVTAM.Web.Test.NsdEngineTest.TWYCLSD.V10
{
    [TestClass]
    public class TwyClsdTest
    {
        protected MockTwyClsdRepoGenerator _mockRepoGenerator;

        protected NsdManagerResolver _managerResolver;

        protected UserProposalController _userProposalController;
        protected NofProposalController _nofProposalController;
        protected IcaoController _icaoController;
        protected Mock<IEventHubContext> _eventHubContextMock;
        protected Mock<INotamQueuePublisher> _mockQueuePublisher;
        protected Mock<IGeoLocationCache> _geoLocationCacheMock;

        protected Mock<IUow> _mockUow;

        protected Mock<IAeroRdsProxy> _aeroRdsProxy = new Mock<IAeroRdsProxy>();

        protected Mock<ILogger> _logger = new Mock<ILogger>();


        [TestInitialize]
        public void InitializeTest()
        {
            _mockRepoGenerator = new MockTwyClsdRepoGenerator();

            _mockUow = SetUow();
            _managerResolver = new NsdManagerResolver(new NsdManagerFactories());
            _mockQueuePublisher = new Mock<INotamQueuePublisher>();
            _eventHubContextMock = new Mock<IEventHubContext>();
            _geoLocationCacheMock = new Mock<IGeoLocationCache>();


            var repoProvider = new RepositoryProvider(new RepositoryFactories());
            var dashboardContext = new DashboardContext(repoProvider, _mockUow.Object, _managerResolver, _mockQueuePublisher.Object, _aeroRdsProxy.Object, _logger.Object);

            _userProposalController = new UserProposalController(dashboardContext, _eventHubContextMock.Object, _geoLocationCacheMock.Object, _logger.Object);
            _nofProposalController = new NofProposalController(dashboardContext, _eventHubContextMock.Object, _geoLocationCacheMock.Object, _logger.Object);
            _icaoController = new IcaoController(dashboardContext.Uow, _aeroRdsProxy.Object, _geoLocationCacheMock.Object, _managerResolver, _logger.Object);

        }

        [TestMethod]
        public void TwyClsd_Validator_FailsWhenSubjectIdIsInvalid()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            model.Token.SubjectId = "A001";

            MockQuebecDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Wrong SubjectID passed and the validation didn't found the error.");
        }

        [TestMethod]
        public void TwyClsd_Validator_FailsWhenClosureReasonIdIsInvalid()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as TwyClsdViewModel).ClosureReasonId = 11;

            MockQuebecDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Wrong Closure Reason Id passed and the validation didn't found the error.");
        }

        [TestMethod]
        public void TwyClsd_Validator_FailsWhenNoTwySelected()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as TwyClsdViewModel);
            token.AllTaxiwaysSelected = false;
            token.AllTaxiways.ForEach(x => {
                x.Selected = false;
                x.Start = null;
                x.StartFr = null;
                x.End = null;
                x.EndFr = null;
            });

            MockQuebecDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "All taxiway selected in false and there is no particular taxiway selected, and the Validator didn't found the error.");
        }

        [TestMethod]
        public void TwyClsd_Validator_FailsWhenStartMissingOnPartialclosure()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as TwyClsdViewModel);
            token.AllTaxiwaysSelected = false;
            var partial = token.AllTaxiways.Where(x => x.Selected && x.Full == false);
            foreach (var part in partial) {
                part.Start = null;
            }

            MockQuebecDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Error on partial closure Start, and the Validator didn't found the error.");
        }

        [TestMethod]
        public void TwyClsd_Validator_FailsWhenEndMissingOnPartialclosure()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as TwyClsdViewModel);
            token.AllTaxiwaysSelected = false;
            var partial = token.AllTaxiways.Where(x => x.Selected && x.Full == false);
            foreach (var part in partial)
            {
                part.End = null;
            }

            MockQuebecDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Error on partial closure Start, and the Validator didn't found the error.");
        }

        /*
        [TestMethod]
        public void TwyClsd_GenerateIcao_Success()
        {
            _icaoController.ApplicationContext = SetApplicationContext().Object;

            var model = GenerateNormalProposalWithTokens();

            MockQuebecDoaAndConfigSettings();

            var results = _icaoController.GenerateIcao(model).Result;

            Assert.IsTrue(results is OkNegotiatedContentResult<ProposalViewModel>, "Data was correct, but GenerateIcao didn't return a ContentResult type");
            Assert.IsTrue(model.Code23 == "MY", "Data was correct, but GenerateIcao Code23 fails.");
            Assert.IsTrue(model.Code45 == "LC", "Data was correct, but GenerateIcao Code45 fails.");
            Assert.IsTrue(model.ItemA == "CYUL", "Data was correct, but GenerateIcao ItemA fails.");
            Assert.IsTrue(model.ItemE.Contains("TWY A BTN START A AND END A"), "Data was correct, but GenerateIcao ItemE fails.");
            Assert.IsTrue(model.Purpose == "NBO", "Data was correct, but GenerateIcao Purpose fails.");
            Assert.IsTrue(model.Scope == "A", "Data was correct, but GenerateIcao Scope fails.");
            Assert.IsTrue(model.Traffic == "IV", "Data was correct, but GenerateIcao Traffic fails.");
            Assert.IsTrue(model.EndValidity == model.Token.EndValidity, "Data was correct, but GenerateIcao EndValidity fails.");
            Assert.IsTrue(model.StartActivity == model.Token.StartActivity, "Data was correct, but GenerateIcao StartActivity fails.");
            Assert.IsTrue(model.Originator == model.Token.Originator, "Data was correct, but GenerateIcao Originator fails.");
        }
        */

        /*
        [TestMethod]
        public void TwyClsd_GenerateIcaoWithAllTaxiways_Success()
        {
            _icaoController.ApplicationContext = SetApplicationContext().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as TwyClsdViewModel);
            token.AllTaxiwaysSelected = true;
            token.AllTaxiways.ForEach(x => {
                x.Selected = false;
                x.Start = null;
                x.StartFr = null;
                x.End = null;
                x.EndFr = null;
            });

            MockQuebecDoaAndConfigSettings();

            var results = _icaoController.GenerateIcao(model).Result;

            Assert.IsTrue(results is OkNegotiatedContentResult<ProposalViewModel>, "Data was correct, but GenerateIcao didn't return a ContentResult type");
            Assert.IsTrue(model.Code23 == "MY", "Data was correct, but GenerateIcao Code23 fails.");
            Assert.IsTrue(model.Code45 == "LC", "Data was correct, but GenerateIcao Code45 fails.");
            Assert.IsTrue(model.ItemA == "CYUL", "Data was correct, but GenerateIcao ItemA fails.");
            Assert.IsTrue(model.ItemE.Contains("ALL TWYS CLSD"), "Data was correct, but GenerateIcao ItemE fails.");
            Assert.IsTrue(model.Purpose == "NBO", "Data was correct, but GenerateIcao Purpose fails.");
            Assert.IsTrue(model.Scope == "A", "Data was correct, but GenerateIcao Scope fails.");
            Assert.IsTrue(model.Traffic == "IV", "Data was correct, but GenerateIcao Traffic fails.");
            Assert.IsTrue(model.EndValidity == model.Token.EndValidity, "Data was correct, but GenerateIcao EndValidity fails.");
            Assert.IsTrue(model.StartActivity == model.Token.StartActivity, "Data was correct, but GenerateIcao StartActivity fails.");
            Assert.IsTrue(model.Originator == model.Token.Originator, "Data was correct, but GenerateIcao Originator fails.");
        }
        */

        private void MockQuebecDoaAndConfigSettings()
        {
            var montrealDoa = _mockRepoGenerator.DataGenerator.Doas.Find(d => d.Name == "Montreal");

            _mockRepoGenerator.MockDoaRepo.Setup(e => e.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(montrealDoa));
            _mockRepoGenerator.MockConfigurationValueRepo.Setup(e => e.GetValueAsync(It.Is<string>(s1 => s1 == "NOTAM"), It.Is<string>(s2 => s2 == "NOTAMUsedSeries")))
                .Returns(Task.FromResult("CFILORDGJMPUEHKNQVX"));
            _mockRepoGenerator.MockConfigurationValueRepo.Setup(e => e.GetValueAsync(It.Is<string>(s1 => s1 == "NOTAM"), It.Is<string>(s2 => s2 == "NOTAMForbiddenWords")))
                .Returns(Task.FromResult("ZCZC|NNNN|TIL|TILL|UNTIL|UNTILL"));
        }

        private ProposalViewModel GenerateNormalProposalWithTokens(NotamProposalStatusCode status = NotamProposalStatusCode.Draft)
        {
            var pModel = _mockRepoGenerator.DataGenerator.GenerateProposalViewModel();
            pModel.Token = _mockRepoGenerator.DataGenerator.GenerateTokens();
            pModel.Status = status;
            return pModel;
        }

        private Mock<IUow> SetUow()
        {
            var MockUow = new Mock<IUow>();
            MockUow.Setup(u => u.SdoCacheRepo).Returns(_mockRepoGenerator.MockSdoCacheRepo.Object);
            MockUow.Setup(u => u.ConfigurationValueRepo).Returns(_mockRepoGenerator.MockConfigurationValueRepo.Object);
            MockUow.Setup(u => u.UserProfileRepo).Returns(_mockRepoGenerator.MockUserProfileRepo.Object);
            MockUow.Setup(u => u.IcaoSubjectRepo).Returns(_mockRepoGenerator.MockIcaoSubjectRepo.Object);
            MockUow.Setup(u => u.DoaRepo).Returns(_mockRepoGenerator.MockDoaRepo.Object);
            MockUow.Setup(u => u.IcaoSubjectConditionRepo).Returns(_mockRepoGenerator.MockIcaoConditionRepo.Object);
            MockUow.Setup(u => u.ProposalRepo).Returns(_mockRepoGenerator.MockProposalRepo.Object);
            MockUow.Setup(u => u.ProposalHistoryRepo).Returns(_mockRepoGenerator.MockProposalHistoryRepo.Object);
            MockUow.Setup(u => u.SeriesNumberRepo).Returns(_mockRepoGenerator.MockSeriesNumberRepo.Object);
            MockUow.Setup(u => u.NotamRepo).Returns(_mockRepoGenerator.MockDisseminatedNotamRepo.Object);
            MockUow.Setup(u => u.AerodromeDisseminationCategoryRepo).Returns(_mockRepoGenerator.MockAerodromeDisseminationCategoryRepo.Object);
            MockUow.Setup(u => u.GeoRegionRepo).Returns(_mockRepoGenerator.MockGeoRegionRepo.Object);
            MockUow.Setup(u => u.SeriesAllocationRepo).Returns(_mockRepoGenerator.MockSeriesAllocationRepo.Object);

            return MockUow;
        }

        private Mock<IApplicationContext> SetApplicationContext(bool canCatchall = false)
        {
            var mockAppContext = new Mock<IApplicationContext>();
            mockAppContext.Setup(ctx => ctx.GetCookie(It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string name, string defaultValue) =>
                {
                    if (name == "org_id") return "4";
                    else if (name == "doa_id") return "3";
                    else if (name == "usr_id") return "45523EFB-AD9D-4060-A50A-7E835332CD78";
                    else if (name == "_vsp") return "48";
                    else if (name == "_culture") return "en";
                    return "";
                });

            mockAppContext.Setup(cs => cs.SetCookie(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()));

            if (canCatchall)
            {
                mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.Is<string>(s => s == CommonDefinitions.CanCatchAll)))
                    .Returns((string user, string role) => Task.FromResult(true));
            }
            else
            {
                mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string user, string role) => Task.FromResult(false));
            }


            mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.Is<string>(s => s == CommonDefinitions.CanCatchAll)))
                .Returns((string user, string role) => Task.FromResult(true));


            mockAppContext.Setup(ctx => ctx.GetUserId())
                .Returns("45523EFB-AD9D-4060-A50A-7E835332CD78");

            mockAppContext.Setup(ctx => ctx.GetUserName())
                .Returns("montreal_fic");

            mockAppContext.Setup(ctx => ctx.GetConfigValue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string s1, string s2) => {
                    return s2;
                });

            return mockAppContext;
        }
    }
}
