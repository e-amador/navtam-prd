﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Controllers.api;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using NAVTAM.SignalrHub;
using System.Threading.Tasks;
using System.Web.Http.Results;
using NavCanada.Core.Common.Common;
using NAVTAM.Web.Test.Data.RwyClsd;
using NAVTAM.NsdEngine.RWYCLSD.V10;
using System.Collections.Generic;
using System;
using NavCanada.Core.Domain.Model.Entitities;
using System.Linq;
using System.Text;
using NDS.Common.Contracts;
using System.Data.Entity.Spatial;
using NavCanada.Core.Common.Contracts;

namespace NAVTAM.Web.Test.NsdEngineTest.RwyClsd.V10
{

    [TestClass]
    public class RwyClsdTest
    {
        protected MockRwyClsdRepoGenerator _mockRepoGenerator;
        protected NsdManagerResolver _managerResolver;

        protected UserProposalController _userProposalController;
        protected NofProposalController _nofProposalController;
        protected IcaoController _icaoController;
        protected Mock<IEventHubContext> _eventHubContextMock;
        protected Mock<IGeoLocationCache> _geoLocationCacheMock;

        protected Mock<INotamQueuePublisher> _mockQueuePublisher;

        protected Mock<IUow> _mockUow;

        protected Mock<IAeroRdsProxy> _aeroRdsProxy = new Mock<IAeroRdsProxy>();

        protected Mock<ILogger> _logger = new Mock<ILogger>();


        [TestInitialize]
        public void IniTest()
        {
            _mockRepoGenerator = new MockRwyClsdRepoGenerator();

            _mockUow = SetUow();
            _managerResolver = new NsdManagerResolver(new NsdManagerFactories());
            _mockQueuePublisher = new Mock<INotamQueuePublisher>();
            _eventHubContextMock = new Mock<IEventHubContext>();
            _geoLocationCacheMock = _mockRepoGenerator.MockGeoLocationCache;

            var repoProvider = new RepositoryProvider(new RepositoryFactories());
            var dashboardContext = new DashboardContext(repoProvider, _mockUow.Object, _managerResolver, _mockQueuePublisher.Object, _aeroRdsProxy.Object, _logger.Object);

            _userProposalController = new UserProposalController(dashboardContext, _eventHubContextMock.Object, _geoLocationCacheMock.Object, _logger.Object);
            _nofProposalController = new NofProposalController(dashboardContext, _eventHubContextMock.Object, _geoLocationCacheMock.Object, _logger.Object);
            _icaoController = new IcaoController(dashboardContext.Uow, _aeroRdsProxy.Object, _geoLocationCacheMock.Object, _managerResolver, _logger.Object);


        }

        [TestMethod]
        public void RwyClsd_Validator_SuccessWhenModelIsOk()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNsdManagerResolver().Object;

            var proposalModel = GenerateNormalProposalWithTokens();

            MockQuebecDoaAndConfigSettings();

           var results = _userProposalController.Validate(proposalModel).Result;

            Assert.IsTrue((results is OkNegotiatedContentResult<ProposalViewModel>), $"Data is incorrect ... results did not generate {results}");

        }

        [TestMethod]
        public void RwyClsd_ItemEValidation_ReturnSuccessWhenModelOK()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNsdManagerResolver().Object;

            var proposalModel = GenerateNormalProposalWithTokens();
            MockQuebecDoaAndConfigSettings();

            var item = proposalModel.Token as RwyClsdViewModel;

            Assert.IsTrue(ItemECheckReturnTrueIfOk(item));
            
            var results = _userProposalController.Validate(proposalModel).Result;

            Assert.IsTrue((results is OkNegotiatedContentResult<ProposalViewModel>), "Data correct and passed validation");
        }
        [TestMethod]
        public void RwyClsd_ItemE_AvailAsTxywy_FailOnInvalidToken()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNsdManagerResolver().Object;

            var proposalModel = GenerateNormalProposalWithTokens();
            _mockRepoGenerator.DataGenerator.GenerateTokens_RwyClsd_WithAvailableTaxiway();
            MockQuebecDoaAndConfigSettings();

            var item = proposalModel.Token as RwyClsdViewModel;

            Assert.IsTrue(ItemECheckReturnTrueIfOk(item), "Failed on itemE business check.");

            var results = _userProposalController.Validate(proposalModel).Result;

            Assert.IsTrue((results is OkNegotiatedContentResult<ProposalViewModel>), "Data correct and passed validation");
        }
        [TestMethod]
        public void RwyClsd_ItemEValidation_FailOnBadItemE()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNsdManagerResolver().Object;

            var proposalModel = GenerateNormalProposalWithTokens();
            MockQuebecDoaAndConfigSettings();

            var item = proposalModel.Token as RwyClsdViewModel;

            Assert.IsTrue(ItemECheckReturnTrueIfOk(item), "ItemE passed business logic validations.");
        }


        #region The Rug (it tied the room together)
        private bool ItemECheckReturnTrueIfOk(RwyClsdViewModel model)
        {
            var status = true;
            while (status)
            {
                if (model.ClosureQuestion)
                {
                    status = !string.IsNullOrWhiteSpace(model.ClosureReasonId);
                }
                if (model.TxyRestrictions)
                {
                    status = !string.IsNullOrWhiteSpace(model.FreeTextTaxiwayRestriction);
                    status = !string.IsNullOrWhiteSpace(model.FreeTextTaxiwayRestrictionFr);
                }
                if (model.AcftRestrictions)
                {
                    status = (model.MaxWeightLbs.Length > 0 || model.MaxWingSpanFt.Length > 0);
                }

                break;
            }
         
            return status;
        }
        private void MockQuebecDoaAndConfigSettings()
        {
            var montrealDoa = _mockRepoGenerator.DataGenerator.Doas.Find(d => d.Name == "Montreal");

            _mockRepoGenerator.MockDoaRepo.Setup(e => e.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(montrealDoa));
            _mockRepoGenerator.MockConfigurationValueRepo.Setup(e => e.GetValueAsync(It.Is<string>(s1 => s1 == "NOTAM"), It.Is<string>(s2 => s2 == "NOTAMUsedSeries")))
                .Returns(Task.FromResult("CFILORDGJMPUEHKNQVX"));
            _mockRepoGenerator.MockConfigurationValueRepo.Setup(e => e.GetValueAsync(It.Is<string>(s1 => s1 == "NOTAM"), It.Is<string>(s2 => s2 == "NOTAMForbiddenWords")))
                .Returns(Task.FromResult("ZCZC|NNNN|TIL|TILL|UNTIL|UNTILL"));
        }

        private ProposalViewModel GenerateNormalProposalWithTokens(NotamProposalStatusCode status = NotamProposalStatusCode.Draft)
        {
            var pModel = _mockRepoGenerator.DataGenerator.GenerateProposalViewModel();
            pModel.Token = _mockRepoGenerator.DataGenerator.GenerateTokens();
            pModel.Status = status;
            return pModel;
        }

        private Mock<IApplicationContext> SetApplicationContext(bool canCatchall = false)
        {
            var mockAppContext = new Mock<IApplicationContext>();
            mockAppContext.Setup(ctx => ctx.GetCookie(It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string name, string defaultValue) =>
                {
                    if (name == "org_id") return "4";
                    else if (name == "doa_id") return "3";
                    else if (name == "usr_id") return "45523EFB-AD9D-4060-A50A-7E835332CD78";
                    else if (name == "_vsp") return "48";
                    else if (name == "_culture") return "en";
                    return "";
                });

            mockAppContext.Setup(cs => cs.SetCookie(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()));

            if (canCatchall)
            {
                mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.Is<string>(s => s == CommonDefinitions.CanCatchAll)))
                    .Returns((string user, string role) => Task.FromResult(true));
            }
            else
            {
                mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string user, string role) => Task.FromResult(false));
            }


            mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.Is<string>(s => s == CommonDefinitions.CanCatchAll)))
                .Returns((string user, string role) => Task.FromResult(true));


            mockAppContext.Setup(ctx => ctx.GetUserId())
                .Returns("45523EFB-AD9D-4060-A50A-7E835332CD78");

            mockAppContext.Setup(ctx => ctx.GetUserName())
                .Returns("montreal_fic");

            mockAppContext.Setup(ctx => ctx.GetConfigValue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string s1, string s2) => {
                    return s2;
                });

            mockAppContext.Setup(ctx => ctx.IsValidUserRequest(It.IsAny<string>())).Returns(() => true);

            return mockAppContext;
        }

        private Mock<IUow> SetUow()
        {
            var MockUow = new Mock<IUow>();
            MockUow.Setup(u => u.SdoCacheRepo).Returns(_mockRepoGenerator.MockSdoSubjectRepo.Object);
            MockUow.Setup(u => u.ConfigurationValueRepo).Returns(_mockRepoGenerator.MockConfigurationValueRepo.Object);
            MockUow.Setup(u => u.UserProfileRepo).Returns(_mockRepoGenerator.MockUserProfileRepo.Object);
            MockUow.Setup(u => u.IcaoSubjectRepo).Returns(_mockRepoGenerator.MockIcaoSubjectRepo.Object);
            MockUow.Setup(u => u.DoaRepo).Returns(_mockRepoGenerator.MockDoaRepo.Object);
            MockUow.Setup(u => u.IcaoSubjectConditionRepo).Returns(_mockRepoGenerator.MockIcaoConditionRepo.Object);
            MockUow.Setup(u => u.ProposalRepo).Returns(_mockRepoGenerator.MockProposalRepo.Object);
            MockUow.Setup(u => u.ProposalHistoryRepo).Returns(_mockRepoGenerator.MockProposalHistoryRepo.Object);
            MockUow.Setup(u => u.SeriesNumberRepo).Returns(_mockRepoGenerator.MockSeriesNumberRepo.Object);
            MockUow.Setup(u => u.NotamRepo).Returns(_mockRepoGenerator.MockDisseminatedNotamRepo.Object);

            return MockUow;
        }

        static Notam GetNotam()
        {
            return new Notam()
            {
                Series = "A",
                ItemA = "CYOW",
                Fir = "CZUL",
                Coordinates = "491140N 1231240W",
                Code23 = "12",
                Code45 = "34",
                Traffic = "IV",
                Purpose = "BO",
                Number = 11,
                Year = 2018,
                Scope = "A",
                Type = NavCanada.Core.Domain.Model.Enums.NotamType.N,
                LowerLimit = 12,
                UpperLimit = 34,
                StartActivity = DateTime.Now,
                EndValidity = DateTime.Now.AddDays(5),
                ItemE = "RWY 18/0W CLOSED DUE GEESE",
                ItemEFrench = "RWY 18/0W FERME AVEC LE CANUCK",
            };
        }

    #endregion 
    }
}
