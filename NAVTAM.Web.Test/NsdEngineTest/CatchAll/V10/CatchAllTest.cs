﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Controllers.api;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.CATCHALL.V10;
using NAVTAM.NsdEngine.Helpers;
using NAVTAM.SignalrHub;
using NAVTAM.Web.Test.Data;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Data.Entity.Spatial;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Common.Contracts;

namespace NAVTAM.Web.Test
{
    [TestClass]
    public class CatchAllTest
    {
        protected MockCatchAllRepoGenerator _mockRepoGenerator;

        protected NsdManagerResolver _managerResolver;

        protected UserProposalController _userProposalController;
        protected NofProposalController _nofProposalController;
        protected IcaoController _icaoController;
        protected Mock<IEventHubContext>  _eventHubContextMock;
        protected Mock<IGeoLocationCache> _geoLocationCacheMock;

        protected Mock<INotamQueuePublisher> _mockQueuePublisher;

        protected Mock<IUow> _mockUow;

        protected Mock<IAeroRdsProxy> _aeroRdsProxy = new Mock<IAeroRdsProxy>();
        protected Mock<ILogger> _logger = new Mock<ILogger>();


        [TestInitialize]
        public void InitializeTest()
        {
            _mockRepoGenerator = new MockCatchAllRepoGenerator();

            _mockUow = SetUow();
            _managerResolver = new NsdManagerResolver(new NsdManagerFactories());
            _mockQueuePublisher = new Mock<INotamQueuePublisher>();
            _eventHubContextMock = new Mock<IEventHubContext>();
            _geoLocationCacheMock = _mockRepoGenerator.MockGeoLocationCache;

            var repoProvider = new RepositoryProvider(new RepositoryFactories());
            var dashboardContext = new DashboardContext(repoProvider, _mockUow.Object, _managerResolver, _mockQueuePublisher.Object, _aeroRdsProxy.Object, _logger.Object);

            _userProposalController = new UserProposalController(dashboardContext, _eventHubContextMock.Object, _geoLocationCacheMock.Object, _logger.Object);
            _nofProposalController = new NofProposalController(dashboardContext, _eventHubContextMock.Object, _geoLocationCacheMock.Object, _logger.Object);
            _icaoController = new IcaoController(dashboardContext.Uow, _aeroRdsProxy.Object, _geoLocationCacheMock.Object, _managerResolver, _logger.Object);
        }

        [TestMethod]
        public void ProposalTest_Validator_SuccessWhenModelIsOk()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is OkNegotiatedContentResult<ProposalViewModel>), "Data was correct, but it fails.");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailsWhenSubjectIdIsNull()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            model.Token.SubjectId = "A001";

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is ExceptionResult), "Wrong SubjectID passed and the validation didn't found the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailsWhenTrafficError()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).TrafficI = false;
            (model.Token as CatchAllViewModel).TrafficV = false;

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Traffic info was wrong and Validator didn't find the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailsWhenScopeError()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).Scope = "H";

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Scope not valid and Validator didn't find the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailsWhenLowerLimitError()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).LowerLimit = -1;

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "LowerLimit was -1 and Validator didn't find the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailsWhenUpperLimitError()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).UpperLimit = 0;

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "UpperLimit was 0 and Validator didn't find the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_SuccessWhenFixLowerLimit()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as CatchAllViewModel);
            token.LowerLimit = 1000;

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue(token.LowerLimit > 999, "LowerLimit was higher that 999 and Validator didn't fix the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_SuccessWhenFixUpperLimit()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as CatchAllViewModel);
            token.UpperLimit = 1000;

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void ProposalTest_Validator_FailWhenLowerLimitHigherUpperLimit()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as CatchAllViewModel);
            token.LowerLimit = 100;
            token.UpperLimit = 80;

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void ProposalTest_Validator_FailWhenRadiusLessThanZero()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as CatchAllViewModel);
            token.Radius = 0;

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void ProposalTest_Validator_FailWithErrorOnPurposeField()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as CatchAllViewModel);
            token.PurposeB = true;
            token.PurposeM = true;
            token.PurposeN = true;
            token.PurposeO = true;

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult));

            model = GenerateNormalProposalWithTokens();
            token = (model.Token as CatchAllViewModel);
            token.PurposeB = false;
            token.PurposeM = false;
            token.PurposeN = false;
            token.PurposeO = false;

            results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void ProposalTest_Validator_FailWithIcaoSubjectMissing()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).IcaoSubjectId = 0;

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "IcaoSubject missing and the Validator didn't found the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailWithInvalidSubjectLocation()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).SubjectLocation = "45.47046N -73.74093W"; //CYUL Location

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Wrong Subject Location passed and Validator didn't found the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_NotImmediate_FailWithStartActivityInvalid()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as CatchAllViewModel);
            
            //StartActivity null
            token.StartActivity = null;
            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "StartActivity was null and Validator didn't found the error.");

            //StartActivity < Now
            token.StartActivity = DateTime.UtcNow.AddDays(-1);
            results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "StartActivity < Now and Validator didn't found the error.");

            //StartActivity > Now + VSP (48)
            token.StartActivity = DateTime.UtcNow.AddHours(50);
            results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "StartActivity > VSP + 48 hrs and Validator didn't found the error.");
        }

        [TestMethod]
        public void ProposalTest_Validator_NotPermanent_FailWithEndActivityInvalid()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            var token = (model.Token as CatchAllViewModel);

            //EndValidity null
            token.EndValidity = null;
            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Didn't validate the EndValidity == null");

            //EndValidity, not Immediate, not Permanent
            token.EndValidity = token.StartActivity.Value.AddHours(3000);
            results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Didn't validate the EndValidity > StartActivity + 2208 hrs");

            //EndValidity, not Immediate, not Permanent
            token.EndValidity = token.StartActivity.Value.AddSeconds(30);
            results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Didn't validate the EndValidity < StartActivity + 30 min");

            //EndValidity, Immediate, not Permanent
            token.EndValidity = DateTime.Now.AddMinutes(10);
            results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Didn't validate the EndValidity < Now + 30 min");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailWithInvalidIcaoCondition()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            //Icao Condition invalid
            (model.Token as CatchAllViewModel).IcaoConditionId = -1;

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Didn't validate the IcaoCondition == null");
        }

        //[TestMethod]
        //public void ProposalTest_Validator_FailWhenIcaoSubjectNotContainsSdoSubjectSubjectType()
        //{
        //    _userProposalController.ApplicationContext = SetApplicationContext().Object;
        //    var model = GenerateNormalProposalWithTokens();
        //    var results = _userProposalController.Validate(model).Result;
        //    Assert.IsTrue((results is BadRequestErrorMessageResult), "Didn't validate the IcaoCondition contains SdoSubject subject type.");
        //}

        [TestMethod]
        public void ProposalTest_Validator_AllowPurposeChange_FailWith4Purposes()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).PurposeB = null;
            (model.Token as CatchAllViewModel).PurposeM = null;
            (model.Token as CatchAllViewModel).PurposeN = null;
            (model.Token as CatchAllViewModel).PurposeO = null;

            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Didn't validate not Purpose on model");
        }

        [TestMethod]
        public void ProposalTest_Validator_FailWithInvalidLatitudeOrLongitude()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).SubjectLocation = "";
            var results = _userProposalController.Validate(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Empty Latitude passed and Validator didn't found the error.");
        }

        [TestMethod]
        public void ProposalTest_GenerateIcao_SuccessWhenModelIsOk()
        {
            _icaoController.ApplicationContext = SetApplicationContext().Object;

            var model = GenerateNormalProposalWithTokens();

            MockVancouverDoaAndConfigSettings();

            var results = _icaoController.GenerateIcao(model).Result;

            Assert.IsTrue(results is OkNegotiatedContentResult<ProposalViewModel>, "Data was correct, but GenerateIcao didn't return a ContentResult type");
            Assert.IsTrue(model.Code23 == "LB", "Data was correct, but GenerateIcao Code23 fails.");
            Assert.IsTrue(model.Code45 == "TT", "Data was correct, but GenerateIcao Code45 fails.");
            Assert.IsTrue(model.ItemA == "CYVR", "Data was correct, but GenerateIcao ItemA fails.");
            Assert.IsTrue(model.ItemE == "VANCOUVER INTL", "Data was correct, but GenerateIcao ItemE fails.");
            Assert.IsTrue(model.ItemF == "1FT AMSL", "Data was correct, but GenerateIcao ItemF fails.");
            Assert.IsTrue(model.ItemG == "2FT AMSL", "Data was correct, but GenerateIcao ItemG fails.");
            Assert.IsTrue(model.Nof == "CYHQ", "Data was correct, but GenerateIcao Nof fails.");
            Assert.IsTrue(model.Purpose == "NBO", "Data was correct, but GenerateIcao Purpose fails.");
            Assert.IsTrue(model.Scope == "A", "Data was correct, but GenerateIcao Scope fails.");
            Assert.IsTrue(model.Traffic == "IV", "Data was correct, but GenerateIcao Traffic fails.");
            Assert.IsTrue(model.EndValidity == model.Token.EndValidity, "Data was correct, but GenerateIcao EndValidity fails.");
            Assert.IsTrue(model.StartActivity == model.Token.StartActivity, "Data was correct, but GenerateIcao StartActivity fails.");
            Assert.IsTrue(model.Originator == model.Token.Originator, "Data was correct, but GenerateIcao Originator fails.");
            Assert.IsTrue(model.Coordinates == "491140N 1230102W", "Data was correct, but GenerateIcao Coordinates fails.");
            Assert.IsTrue(model.Fir == "CZVR", "Data was correct, but GenerateIcao FIR fails.");
        }

        [TestMethod]
        public void ProposalTest_GenerateIcao_FailWithStartActivityInvalid()
        {
            _icaoController.ApplicationContext = SetApplicationContext().Object;

            var model = GenerateNormalProposalWithTokens();

            //StartActivity < Now
            model.Token.StartActivity = DateTime.UtcNow.AddDays(-1);
            var results = _icaoController.GenerateIcao(model).Result;

            Assert.IsTrue((results is BadRequestErrorMessageResult), "Data was incorrect, but GenerateIcao didn't return an error.");
        }

        [TestMethod]
        public void ProposalTest_SaveDraft_Success_CreateNewRecordForNewDraft()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            MockVancouverDoaAndConfigSettings();

            var model = GenerateNormalProposalWithTokens();
            //model.Status = NotamProposalStatusCode.Draft;

            var results = _userProposalController.SaveDraft(model).Result;
            var proposal = _mockRepoGenerator.DataGenerator.Proposals.Find(x => x.Id == model.ProposalId);
            var proposalH = _mockRepoGenerator.DataGenerator.ProposalHistories.Find(x => x.ProposalId == model.ProposalId);

            Assert.IsTrue(proposal.Id == proposalH.ProposalId,"Data was correct, but the SaveDraft didn't save the ProposalHistory correctly");
            var success = (proposal.Status == proposalH.Status) && (proposal.Status == NotamProposalStatusCode.Draft);
            Assert.IsTrue(success, "Data was correct, but the SaveDraft didn't save the ProposalHistory correctly");
        }

        [TestMethod]
        public void ProposalTest_SaveDraft_Success_UpdateRecordForExistingDraft()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            model.ProposalId = 54;
            model.Code23 = "LB";

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.SaveDraft(model).Result;
            var proposal = _mockRepoGenerator.DataGenerator.Proposals.Find(x => x.Id == model.ProposalId);
            var proposalH = _mockRepoGenerator.DataGenerator.ProposalHistories.Find(x => x.ProposalId == model.ProposalId);

            Assert.IsTrue(proposal.Id == proposalH.ProposalId, "Data was correct, but the SaveDraft didn't save the ProposalHistory correctly");
            var success = (proposal.Status == proposalH.Status) && (proposal.Status == NotamProposalStatusCode.Draft);
            Assert.IsTrue(success, "Data was correct, but the SaveDraft didn't save the ProposalHistory correctly");
            Assert.IsTrue(proposal.Code23 == "LB", "Data was correct, but the SaveDraft didn't save the ProposalHistory correctly");
        }

        [TestMethod]
        public void ProposalTest_SubmitProposal_SuccessWithCorrectData()
        {
            _userProposalController.ApplicationContext = SetApplicationContext().Object;
            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.SubmitProposal(model).Result as HttpResponseMessage;
            var proposal = _mockRepoGenerator.DataGenerator.Proposals.Find(x => x.Id == model.ProposalId);
            var proposalH = _mockRepoGenerator.DataGenerator.ProposalHistories.Find(x => x.ProposalId == model.ProposalId);

            Assert.IsTrue(proposal.Id == proposalH.ProposalId, "Data was correct, but the SubmitProposal didn't save the ProposalHistory correctly");
            var success = (proposal.Status == proposalH.Status) && (proposal.Status == NotamProposalStatusCode.Submitted);
            Assert.IsTrue(success, "Data was correct, but the SubmitProposal didn't save the ProposalHistory correctly");
            //With this model, Code23 have to be = "LB"
            Assert.IsTrue(proposal.Code23 == "LB", "Data was correct, but the SubmitProposal didn't save the ProposalHistory correctly");
        }

        [TestMethod]
        public void ProposalTest_SubmitProposal_FailWithWrongData()
        {
            _userProposalController.ApplicationContext = SetApplicationContext(true).Object;

            //_userProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).TrafficI = false;
            (model.Token as CatchAllViewModel).TrafficV = false;

            MockVancouverDoaAndConfigSettings();

            var results = _userProposalController.SubmitProposal(model).Result;
            Assert.IsNull(model.ProposalId);
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Data was incorrect, but SubmitProposal didn't return an error.");
        }

        [TestMethod]
        public void ProposalTest_DiseminateProposal_SuccessWithCorrectData()
        {
            //We need to mock the DashboardContext, not use the one that I have on the InitializeTest
            //This means that I need to use another dashboradController, not the one that I use in the others
            //tests

            var mockDashboardContext = new Mock<IDashboardContext>();
            //Mock all the functions that I need
            mockDashboardContext.Setup(dc => dc.GetNsdManagerResolver() )
                .Returns( () => { return _managerResolver; });
            mockDashboardContext.Setup( dc => dc.GetQueuePublisher())
                .Returns( () => { return _mockQueuePublisher.Object; });
            mockDashboardContext.Setup( dc => dc.GetTempUow())
                .Returns( () => { return _mockUow.Object; });
            mockDashboardContext.Setup( dc => dc.Uow)
                .Returns( _mockUow.Object );
            mockDashboardContext.Setup(dc => dc.GetAeroRdsProxy())
                .Returns(_aeroRdsProxy.Object);

            MockVancouverDoaAndConfigSettings();

            var nofProposalController = new NofProposalController(mockDashboardContext.Object, _eventHubContextMock.Object, _geoLocationCacheMock.Object, _logger.Object);
            nofProposalController.ApplicationContext = SetApplicationContext().Object;
            //nofProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();

            var results = nofProposalController.DisseminateProposal(model).Result as HttpResponseMessage;
            var proposal = _mockRepoGenerator.DataGenerator.Proposals.Find(x => x.Id == model.ProposalId);
            var proposalH = _mockRepoGenerator.DataGenerator.ProposalHistories.Find(x => x.ProposalId == model.ProposalId);
            var serie = _mockRepoGenerator.DataGenerator.SeriesNumbers.Find(x => x.Series == proposal.Series && x.Year == proposal.Year);

            Assert.IsTrue(proposal.Id == proposalH.ProposalId, "Data was correct, but the DiseminateProposal didn't save the ProposalHistory correctly");
            var success = (proposal.Status == proposalH.Status) && (proposal.Status == NotamProposalStatusCode.Disseminated);
            Assert.IsTrue(success, "Data was correct, but the DiseminateProposal didn't save the Proposal Status correctly");
            Assert.IsTrue(proposal.Year == DateTime.UtcNow.Year, "Data was correct, but the DiseminateProposal didn't update Proposal Year correctly");
            Assert.IsTrue(serie != null, "Data was correct, but the DiseminateProposal didn't update SeriesNumber correctly");
        }

        [TestMethod]
        public void ProposalTest_RejectProposal_SuccessWithCorrectData()
        {
            _nofProposalController.ApplicationContext = SetApplicationContext().Object;
            //_nofProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var rejectionReason = "There is a reason to reject.";
            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).RejectionReason = rejectionReason;
            var prop = _mockRepoGenerator.DataGenerator.Proposals.Find(x => x.Id == 100);
            prop.CopyPropertiesTo(model);
            model.ProposalId = prop.Id;


            var results = _nofProposalController.RejectProposal(model).Result;
            var proposal = _mockRepoGenerator.DataGenerator.Proposals.Find(x => x.Id == model.ProposalId);
            var proposalH = _mockRepoGenerator.DataGenerator.ProposalHistories.Find(x => x.ProposalId == model.ProposalId);

            Assert.IsTrue(proposal.Id == proposalH.ProposalId, "Data was correct, but the RejectProposal didn't save the ProposalHistory correctly");
            Assert.IsTrue(proposal.Status == NotamProposalStatusCode.Rejected, "Data was correct, but the RejectProposal didn't save the Proposal Status correctly");
            Assert.IsTrue(proposal.Status == proposalH.Status, "Data was correct, but the RejectProposal didn't save the Proposal Status correctly");
            Assert.IsTrue(proposal.RejectionReason == rejectionReason, "Data was correct, but the RejectProposal didn't save the Proposal History Status correctly");
        }

        [TestMethod]
        public void ProposalTest_RejectProposal_FailWhenNoReason()
        {
            _nofProposalController.ApplicationContext = SetApplicationContext().Object;
            //_nofProposalController.NsdManagerDataResolver = SetNdsManagerResolver().Object;

            var model = GenerateNormalProposalWithTokens();
            (model.Token as CatchAllViewModel).RejectionReason = "";
            var prop = _mockRepoGenerator.DataGenerator.Proposals.Find(x => x.Id == 54);
            prop.CopyPropertiesTo(model);
            model.ProposalId = prop.Id;

            var results = _nofProposalController.RejectProposal(model).Result;
            Assert.IsTrue((results is BadRequestErrorMessageResult), "Data was incorrect, but RejectProposal didn't return an error.");
        }

        private ProposalViewModel GenerateNormalProposalWithTokens(NotamProposalStatusCode status = NotamProposalStatusCode.Draft)
        {
            var pModel = _mockRepoGenerator.DataGenerator.GenerateProposalViewModel();
            var purposes = new[] { true, false, true, true };
            var traffics = new[] { true, true };
            pModel.Token = _mockRepoGenerator.DataGenerator.GenerateTokens(1, 69, purposes, traffics, false, false);
            pModel.Status = status;
            return pModel;
        }

        private Mock<IUow> SetUow()
        {
            var MockUow = new Mock<IUow>();
            MockUow.Setup(u => u.SdoCacheRepo).Returns(_mockRepoGenerator.MockSdoCacheRepo.Object);
            MockUow.Setup(u => u.ConfigurationValueRepo).Returns(_mockRepoGenerator.MockConfigurationValueRepo.Object);
            MockUow.Setup(u => u.UserProfileRepo).Returns(_mockRepoGenerator.MockUserProfileRepo.Object);
            MockUow.Setup(u => u.IcaoSubjectRepo).Returns(_mockRepoGenerator.MockIcaoSubjectRepo.Object);
            MockUow.Setup(u => u.DoaRepo).Returns(_mockRepoGenerator.MockDoaRepo.Object);
            MockUow.Setup(u => u.IcaoSubjectConditionRepo).Returns(_mockRepoGenerator.MockIcaoConditionRepo.Object);
            MockUow.Setup(u => u.ProposalRepo).Returns(_mockRepoGenerator.MockProposalRepo.Object);
            MockUow.Setup(u => u.ProposalHistoryRepo).Returns(_mockRepoGenerator.MockProposalHistoryRepo.Object);
            MockUow.Setup(u => u.SeriesNumberRepo).Returns(_mockRepoGenerator.MockSeriesNumberRepo.Object);
            MockUow.Setup(u => u.NotamRepo).Returns(_mockRepoGenerator.MockDisseminatedNotamRepo.Object);
            MockUow.Setup(u => u.OrganizationNsdRepo).Returns(_mockRepoGenerator.MockOrganizationNsdRepo.Object);
            MockUow.Setup(u => u.OrganizationRepo).Returns(_mockRepoGenerator.MockOrganizationRepo.Object);
            MockUow.Setup(u => u.SeriesChecklistRepo).Returns(_mockRepoGenerator.MockSeriesChecklistRepo.Object);
            MockUow.Setup(u => u.SystemStatusRepo).Returns(_mockRepoGenerator.MockSystemStatusRepo.Object);

            return MockUow;
        }

        private Mock<IApplicationContext> SetApplicationContext( bool canCatchall = false)
        {
            var mockAppContext = new Mock<IApplicationContext>();
            mockAppContext.Setup(ctx => ctx.GetCookie(It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string name, string defaultValue) =>
                {
                    if (name == "org_id") return "1";
                    else if (name == "doa_id") return "1";
                    else if (name == "usr_id") return "573b818a-9245-4072-9746-86e25d420953";
                    else if (name == "_vsp") return "48";
                    else if (name == "_culture") return "en";
                    return "";
                });

            mockAppContext.Setup(cs => cs.SetCookie(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()));

            if( canCatchall)
            {
                mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.Is<string>(s => s == CommonDefinitions.CanCatchAll)))
                    .Returns((string user, string role) => Task.FromResult(true));
            }
            else
            {
                mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string user, string role) => Task.FromResult(false));
            }


            mockAppContext.Setup(ctx => ctx.IsUserInRole(It.IsAny<string>(), It.Is<string>(s => s == CommonDefinitions.CanCatchAll)))
                .Returns((string user, string role) => Task.FromResult(true));


            mockAppContext.Setup( ctx => ctx.GetUserId())
                .Returns("573b818a-9245-4072-9746-86e25d420953");

            mockAppContext.Setup( ctx => ctx.GetUserName())
                .Returns("ficuser");

            mockAppContext.Setup( ctx => ctx.GetConfigValue(It.IsAny<string>(),It.IsAny<string>()))
                .Returns((string s1, string s2) => {
                    return s2;
                });

            mockAppContext.Setup(ctx => ctx.IsValidUserRequest(It.IsAny<string>())).Returns(() => true);

            return mockAppContext;
        }

        private void MockVancouverDoaAndConfigSettings()
        {
            var vancouverDoa = CreateVancouverDoa();
            _mockRepoGenerator.MockDoaRepo.Setup(e => e.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(vancouverDoa));
            _mockRepoGenerator.MockConfigurationValueRepo.Setup(e => e.GetValueAsync(It.Is<string>(s1 => s1 == "NOTAM"), It.Is<string>(s2 => s2 == "NOTAMUsedSeries")))
                .Returns(Task.FromResult("CFILORDGJMPUEHKNQVX"));
            _mockRepoGenerator.MockConfigurationValueRepo.Setup(e => e.GetValueAsync(It.Is<string>(s1 => s1 == "NOTAM"), It.Is<string>(s2 => s2 == "NOTAMForbiddenWords")))
                .Returns(Task.FromResult("ZCZC|NNNN|TIL|TILL|UNTIL|UNTILL"));
        }

        private Doa CreateMontrealDoa()
        {
            return new Doa
            {
                Id = 1,
                Name = "Montreal",
                Description = "Pierre Trudeau Airport",
                DoaGeoArea = System.Data.Entity.Spatial.DbGeography.FromText(
                    "MULTIPOLYGON (((-73.62334 45.55274, -73.62334 45.55274, -73.62334 45.55274, -73.62922 45.5567, -73.63538 45.56046, -73.64179 45.56399, -73.64844 45.5673, -73.65532 45.57038, -73.6624 45.57321, -73.66967 45.5758, -73.67711 45.57813, -73.68471 45.58021, -73.69245 45.58201, -73.7003 45.58355, -73.70825 45.58482, -73.71628 45.58581, -73.72438 45.58652, -73.73251 45.58695, -73.74066 45.5871, -73.74881 45.58697, -73.75694 45.58656, -73.76504 45.58587, -73.77307 45.58489, -73.78103 45.58365, -73.78889 45.58213, -73.79663 45.58033, -73.80424 45.57828, -73.8117 45.57596, -73.81898 45.57339, -73.82608 45.57057, -73.83297 45.56751, -73.83963 45.56422, -73.84606 45.5607, -73.85223 45.55696, -73.85814 45.55301, -73.86376 45.54887, -73.86908 45.54453, -73.87409 45.54002, -73.87878 45.53534, -73.88314 45.5305, -73.88715 45.52552, -73.89081 45.52041, -73.89411 45.51517, -73.89704 45.50983, -73.89959 45.50439, -73.90176 45.49887, -73.90354 45.49329, -73.90493 45.48765, -73.90592 45.48196, -73.90652 45.47625, -73.90671 45.47053, -73.90651 45.46481, -73.90591 45.4591, -73.90491 45.45342, -73.90352 45.44778, -73.90173 45.44219, -73.89956 45.43667, -73.89701 45.43124, -73.89408 45.4259, -73.89078 45.42066, -73.88712 45.41555, -73.88312 45.41057, -73.87877 45.40573, -73.87409 45.40105, -73.86909 45.39654, -73.86378 45.3922, -73.85818 45.38806, -73.8523 45.38411, -73.84615 45.38037, -73.83974 45.37684, -73.8331 45.37354, -73.82624 45.37048, -73.81918 45.36765, -73.81193 45.36507, -73.8045 45.36275, -73.79693 45.36068, -73.78922 45.35888, -73.7814 45.35735, -73.77348 45.35609, -73.76548 45.3551, -73.75742 45.35439, -73.74932 45.35396, -73.7412 45.35382, -73.73308 45.35395, -73.72499 45.35436, -73.71692 45.35505, -73.70892 45.35601, -73.70099 45.35726, -73.69316 45.35877, -73.68544 45.36056, -73.67786 45.3626, -73.67043 45.36491, -73.66316 45.36747, -73.65609 45.37028, -73.64921 45.37333, -73.64256 45.37661, -73.63614 45.38012, -73.62997 45.38385, -73.62407 45.38778, -73.61845 45.39192, -73.61312 45.39624, -73.6081 45.40074, -73.6034 45.40541, -73.59902 45.41024, -73.59499 45.41521, -73.591310000000007 45.42032, -73.58799 45.42554, -73.58504 45.43088, -73.58246 45.43631, -73.58026 45.44182, -73.57845 45.4474, -73.57703 45.45304, -73.57601 45.45872, -73.57538 45.46442, -73.57515 45.47015, -73.57532 45.47587, -73.57589 45.48158, -73.57685 45.48727, -73.57821 45.49291, -73.57997 45.4985, -73.58211 45.50403, -73.58464 45.50947, -73.58754 45.51482, -73.59081 45.52006, -73.59445 45.52518, -73.59844 45.53017, -73.60278 45.53502, -73.60745 45.53971, -73.61244 45.54424, -73.61774 45.54858, -73.62334 45.55274)))")
            };
        }

        private Doa CreateVancouverDoa()
        {
            return new Doa
            {
                Id = 1,
                Name = "Vancouver",
                Description = "Vancouver DOA",
                DoaGeoArea =
                    System.Data.Entity.Spatial.DbGeography.FromText(
                        "MULTIPOLYGON (((-132.066 57, -132.066 57, -132.066 57, -132.06586 56.99999, -132.045 57.0451, -132.066 57)), ((-133.75 51, -133.75 51, -133.75 51, -128 48.3333, -125 48.5, -124.727 48.4934, -124.012 48.2967, -123.679 48.24, -123.541 48.2246, -123.248 48.284, -123.115 48.4228, -123.16 48.4535, -123.219 48.5487, -123.268 48.694, -123.009 48.7671, -123.009 48.8312, -123.322 49.0021, -123.091 49.0021, -123.089 49.0021, -123.075 49.0021, -123.069 49.0021, -123.068 49.0021, -123.061 49.0021, -123.038 49.0021, -123.036 49.0021, -122.757 49.0021, -122.756 49.0021, -122.755 49.0021, -122.736 49.0021, -122.735 49.0021, -122.731 49.0021, -122.714 49.0021, -122.691 49.0021, -122.669 49.0021, -122.647 49.0021, -122.64 49.0021, -122.625 49.0022, -122.602 49.0022, -122.58 49.0022, -122.559 49.0022, -122.54 49.0022, -122.524 49.0022, -122.515 49.0022, -122.497 49.0022, -122.485 49.0023, -122.476 49.0023, -122.438 49.0023, -122.427 49.0023, -122.405 49.0023, -122.392 49.0023, -122.383 49.0023, -122.361 49.0023, -122.351 49.0023, -122.338 49.0024, -122.319 49.0024, -122.293 49.0024, -122.27 49.0024, -122.265 49.0024, -122.252 49.0024, -122.233 49.0024, -122.231 49.0024, -122.216 49.0024, -122.201 49.0024, -122.186 49.0024, -122.162 49.0024, -122.148 49.0024, -122.141 49.0024, -122.127 49.0024, -122.11 49.0024, -122.098 49.0024, -122.06 49.0019, -122.028 49.0014, -122.02 49.0013, -121.972 49.0006, -121.935 49.0001, -121.851 48.9988, -121.81 48.9981, -121.76 48.9973, -121.751 48.9973, -121.741 48.9973, -121.668 48.9983, -121.63 48.9988, -121.613 48.999, -121.567 48.9993, -121.527 48.9995, -121.524 48.9995, -121.497 48.9996, -121.475 48.9997, -121.445 48.9998, -121.416 48.9999, -121.405 48.9999, -121.379 49.0003, -121.335 49.0009, -121.302 49.0009, -121.298 49.0009, -121.251 49.0008, -121.194 49.0006, -121.155 49.0004, -121.095 49.0002, -121.063 49.0002, -121.061 49.0002, -121.019 49.0003, -121.001 49.0003, -120.954 49.0003, -120.911 49.0003, -120.861 49.0003, -120.802 49.0003, -120.762 49.0002, -120.7 49.0001, -120.685 49.0001, -120.67 49.0001, -120.645 49.0002, -120.597 49.0004, -120.559 49.0006, -120.552 49.0006, -120.525 49.0006, -120.482 49.0006, -120.429 49.0005, -120.403 49.0005, -120.348 49.0004, -120.326 49.0004, -120.284 49.0003, -120.225 49.0001, -120.182 49, -120.142 48.9999, -120.085 48.9996, -120.059 48.9995, -120.037 48.9994, -120.024 48.9994, -120.003 48.9995, -119.98 48.9996, -119.945 48.9997, -119.937 48.9997, -119.879 48.9998, -119.843 48.9999, -119.789 49, -119.757 49.0001, -119.731 49.0001, -119.701 49.0001, -119.691 49.0001, -119.654 49.0001, -119.636 49.0001, -119.581 49.0001, -119.523 49.0001, -119.506 49.0001, -119.462 49.0001, -119.46 49.0001, -119.429 49.0001, -119.428 49.0001, -119.403 49.0001, -119.382 49.0001, -119.341 49.0001, -119.311 49.0001, -119.266 49.0001, -119.224 49.0001, -119.196 49.0001, -119.183 49.0001, -119.158 49.0001, -119.129 49.0001, -119.101 49.0001, -119.074 49.0001, -119.047 49.0001, -119.017 49.0001, -118.974 49.0001, -118.957 49.0001, -118.943 49.0001, -118.91 49.0001, -118.899 49.0001, -118.88 49.0001, -118.834 49.0001, -118.803 49.0001, -118.787 49.0001, -118.777 49.0001, -118.761 49.0001, -118.76 49.0001, -118.747 49.0001, -118.735 49.0001, -118.722 49.0001, -118.707 49.0001, -118.689 49.0001, -118.666 49.0001, -118.638 49.0001, -118.588 49.0001, -118.551 49.0001, -118.527 49.0001, -118.505 49.0001, -118.504 49.0001, -118.494 49.0001, -118.467 49.0001, -118.453 49.0001, -118.423 49.0001, -118.39 49, -118.377 49.0001, -118.325 49.0001, -118.287 49.0001, -118.248 49.0001, -118.23 49.0001, -118.224 49.0001, -118.208 49.0001, -118.173 49.0001, -118.164 49.0001, -118.095 49.0001, -118.063 49.0002, -118.039 49.0002, -117.992 49.0003, -117.933 49.0004, -117.894 49.0004, -117.832 49.0005, -117.831 49.0005, -117.773 49.0005, -117.738 49.0006, -117.697 49.0007, -117.657 49.0007, -117.638 49.0007, -117.628 49.0007, -117.58 49.0007, -117.508 49.0006, -117.429 49.0005, -117.391 49.0004, -117.369 49.0003, -117.355 49.0003, -117.352 49.0003, -117.3 49, -117.299 49, -117.29 48.9999, -117.245 48.9996, -117.2 48.9993, -117.151 48.9989, -117.135 48.9988, -117.073 48.9991, -117.029 48.9992, -116.991 48.9993, -116.929 48.9995, -116.889 48.9996, -116.828 48.9997, -116.789 48.9998, -116.755 48.9998, -116.727 48.9999, -116.7 48.9999, -116.652 48.9999, -116.607 48.9999, -116.577 48.9999, -116.499 48.9999, -116.439 49.0001, -116.397 49.0003, -116.348 49.0005, -116.327 49.0006, -116.278 49.0007, -116.237 49.0008, -116.213 49.0007, -116.194 49.0006, -116.183 49.0006, -116.181 49.0006, -116.179 49.0006, -116.178 49.0006, -116.139 49.0007, -116.109 49.0007, -116.062 49.0008, -116.032 49.0009, -115.972 49.0009, -115.937 49.001, -115.893 49.001, -115.861 49.001, -115.799 49.001, -115.767 49.0009, -115.729 49.0009, -115.693 49.0009, -115.655 49.0008, -115.649 49.0008, -115.645 49.0008, -115.634 49.0008, -115.578 49.0007, -115.519 49.0005, -115.5 49.0005, -115.138 49.5, -115.147 49.9625, -115.149 49.9887, -115.153 50.0147, -115.16 50.0405, -115.17 50.066, -115.182 50.091, -115.196 50.1155, -115.213 50.1394, -115.232 50.1625, -115.253 50.1849, -115.277 50.2063, -115.302 50.2267, -115.33 50.246, -115.359 50.2642, -115.39 50.2812, -115.423 50.2968, -115.457 50.3111, -115.493 50.324, -115.53 50.3354, -115.568 50.3453, -115.607 50.3536, -115.646 50.3604, -115.686 50.3655, -115.727 50.369, -115.768 50.3708, -115.809 50.371, -115.85 50.3695, -115.89 50.3663, -115.93 50.3615, -115.97 50.3551, -116.009 50.3471, -116.047 50.3375, -116.084 50.3264, -119 53.4, -123.25 56, -132.06586 56.99999, -132.124 56.8737, -131.873 56.806, -131.901 56.7537, -131.861 56.703, -131.853 56.6708, -131.851 56.6618, -131.848 56.6508, -131.844 56.6329, -131.835 56.5992, -131.802 56.601, -131.763 56.6029, -131.751 56.6036, -131.738 56.6043, -131.706 56.6059, -131.659 56.6083, -131.633 56.6097, -131.581 56.6123, -131.472 56.5527, -131.32 56.5003, -131.311 56.4973, -131.298 56.4926, -131.295 56.4918, -131.174 56.4496, -131.088 56.4061, -130.966 56.3906, -130.942 56.3876, -130.923 56.3851, -130.915 56.3841, -130.88 56.3795, -130.86 56.3769, -130.782 56.3669, -130.758 56.3517, -130.745 56.3437, -130.737 56.3386, -130.731 56.335, -130.72 56.3281, -130.71 56.3217, -130.686 56.3066, -130.668 56.295, -130.648 56.2822, -130.646 56.281, -130.627 56.2691, -130.623 56.2669, -130.616 56.2651, -130.583 56.2574, -130.571 56.2546, -130.541 56.2475, -130.468 56.2431, -130.456 56.2151, -130.452 56.2031, -130.45 56.1986, -130.44 56.1746, -130.426 56.1416, -130.35 56.1286, -130.246 56.0965, -130.23 56.0995, -130.104 56.1229, -130.083 56.099, -130.065 56.0782, -130.059 56.0714, -130.058 56.07, -130.054 56.0663, -130.037 56.0464, -130.035 56.0441, -130.012 56.0173, -130.004 56.0081, -130.004 56.0012, -130.004 56.0009, -130.005 55.993, -130.012 55.9468, -130.015 55.9299, -130.017 55.9185, -130.018 55.9122, -130.018 55.912, -130.017 55.912, -130.004 55.912, -130.004 55.9075, -130.084 55.8218, -130.125 55.8057, -130.153 55.7663, -130.148 55.7149, -130.112 55.6829, -130.128 55.5811, -130.09 55.498, -130.041 55.4517, -130.021 55.3379, -129.977 55.3004, -129.974 55.2819, -130.103 55.1926, -130.146 55.1429, -130.152 55.1246, -130.181 55.0913, -130.188 55.0629, -130.273 54.974, -130.346 54.9177, -130.423 54.8734, -130.454 54.8513, -130.48 54.8367, -130.568 54.7922, -130.629 54.7828, -130.659 54.7631, -130.626 54.7381, -130.629 54.7248, -130.616 54.7075, -132.684 54.6622, -134.95 54.2167, -136 54, -134.994 52.7147, -133.75 51)))")
            };
        }

    }
}
