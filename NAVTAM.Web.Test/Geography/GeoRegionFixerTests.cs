﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Common.Geography;

namespace NAVTAM.Web.Test.Geography
{
    [TestClass]
    public class GeoRegionFixerTests
    {
        [TestMethod]
        public void Geography_Fixer_Fix_Polygons()
        {
            var polyText = "POLYGON ((1.001 2, 3 4, 5 6, -7 8), (1 2, 3 -4.00001, 5 6, 7 -8.00))";
            var result = GeoRegionFixer.Process(polyText);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.StartsWith("POLYGON"));

            polyText = "POLYGON ((-82.6797 41.6766, -82.6797 41.6766, -82.3975 41.6766, -81.2457 42.2076, -80.0799 42.3936, -78.9354 42.8283, -78.9161 42.8786))";
            result = GeoRegionFixer.Process(polyText);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.StartsWith("POLYGON"));
        }

        [TestMethod]
        public void Geography_Fixer_Fix_MultiPolygons()
        {
            var multiPolyText = "MULTIPOLYGON (((1.001 2, 3 4, 5 6, -7 8), (1 2, 3 -4.00001, 5 6, 7 -8.00)), ((1.001 2, 3 4, 5 6, -7 8, 1 2, 3 -4.00001, 5 6, 7 -8.00)))";
            var result = GeoRegionFixer.Process(multiPolyText);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.StartsWith("MULTIPOLYGON "));

            multiPolyText = "MULTIPOLYGON (((-82.6797 41.6766, -82.6797 41.6766, -82.3975 41.6766, -81.2457 42.2076, -80.0799 42.3936, -78.9354 42.8283, -78.9161 42.8786)))";
            result = GeoRegionFixer.Process(multiPolyText);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.StartsWith("MULTIPOLYGON "));
        }

        [TestMethod]
        public void Geography_Fixer_Fix_Polygon_with_Repeated_Points()
        {
            var polyText = "POLYGON ((0 0, 0 0, 10 0, 10 0, 10 10, 0 10, 0 0))";
            var result = GeoRegionFixer.Process(polyText);
            var newPoly = GeoPolygon.TryParse(result);
            Assert.IsNotNull(newPoly);
            Assert.IsTrue(newPoly.PointCount == 5);
            Assert.IsTrue(newPoly.ToString() == "POLYGON ((0 0, 10 0, 10 10, 0 10, 0 0))");
        }

        [TestMethod]
        public void Geography_Fixer_Fix_Polygon_with_Invalid_Path()
        {
            var polyText = "POLYGON ((0 0, 20 0, 10 0, 10 10, 0 10, 0 0))";
            var result = GeoRegionFixer.Process(polyText);
            var newPoly = GeoPolygon.TryParse(result);
            Assert.IsNotNull(newPoly);
            Assert.IsTrue(newPoly.PointCount == 5);
            Assert.IsTrue(newPoly.ToString() == "POLYGON ((0 0, 10 0, 10 10, 0 10, 0 0))");
        }

        [TestMethod]
        public void Geography_Fixer_Fix_Polygon_with_Invalid_Triangle()
        {
            var polyText = "POLYGON ((0 0, 50.0001 0, 100 0, 100 10, 0 10, 0 0))";
            var result = GeoRegionFixer.Process(polyText);
            var newPoly = GeoPolygon.TryParse(result);
            Assert.IsNotNull(newPoly);
            Assert.IsTrue(newPoly.PointCount == 5);
            Assert.IsTrue(newPoly.ToString() == "POLYGON ((0 0, 100 0, 100 10, 0 10, 0 0))");
        }
    }
}
