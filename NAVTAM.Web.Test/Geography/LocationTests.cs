﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core.Common.Geography;
using NAVTAM.Helpers;
using System.Device.Location;

namespace NAVTAM.Web.Test.Geography
{
    [TestClass]
    public class LocationTests
    {
        [TestMethod]
        public void Geography_Location_RoundedCoordinates()
        {
            var original = DMSLocation.FromDMS("491140N 1231102W");
            var target = original.Round();
            var lat = target.Latitude.AsLatitudeDM();
            var lng = target.Longitude.AsLongitudeDM();

            Assert.IsTrue(lat == "4912N");
            Assert.IsTrue(lng == "12311W");

            original = DMSLocation.FromDMS("495940N 1231102W");
            target = original.Round();
            lat = target.Latitude.AsLatitudeDM();
            lng = target.Longitude.AsLongitudeDM();

            Assert.IsTrue(lat == "5000N");
            Assert.IsTrue(lng == "12311W");
        }

        [TestMethod]
        public void Geography_Location_ValidateDMSCoordinates()
        {
            // sample
            Assert.IsTrue(DMSLocation.IsValidDMS("491140N 1231102W"));
            Assert.IsTrue(DMSLocation.IsValidDMS("491140S 1231102W"));
            Assert.IsTrue(DMSLocation.IsValidDMS("491140S 1231102E"));
            Assert.IsTrue(DMSLocation.IsValidDMS("491140N 1231102E"));

            // padded latitude
            Assert.IsTrue(DMSLocation.IsValidDMS("0491140N 1231102E"));
            Assert.IsTrue(DMSLocation.IsValidDMS("0091140N 1231102E"));

            // spacing
            Assert.IsFalse(DMSLocation.IsValidDMS("491140N1231102W"));

            // garbage
            Assert.IsFalse(DMSLocation.IsValidDMS("asasa1234sas567"));

            // length
            Assert.IsFalse(DMSLocation.IsValidDMS("4911400N 1231102W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("491140N 12311020W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("4911N 12311020W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("491140N 12311W"));

            // bounds seconds
            Assert.IsFalse(DMSLocation.IsValidDMS("491161N 1231102W"));

            // bounds minutes
            Assert.IsFalse(DMSLocation.IsValidDMS("496111N 1231102W"));

            // bounds degrees
            Assert.IsTrue(DMSLocation.IsValidDMS("900000N 1800000W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("901111N 1231102W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("910000N 1231102W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("491111N 1801102W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("491111N 1810000W"));

            // directions
            Assert.IsFalse(DMSLocation.IsValidDMS("491140 1231102W"));
            Assert.IsFalse(DMSLocation.IsValidDMS("491140N 1231102"));
            Assert.IsFalse(DMSLocation.IsValidDMS("491140 1231102"));

            // rounding
            var dms = DMSLocation.FromDMS("890001N 1790100W");
            Assert.IsTrue(dms != null);
            Assert.IsTrue(dms.Round().IsValidDMS());
        }

        [TestMethod]
        public void Geography_Location_ValidateDMCoordinates()
        {
            // sample
            Assert.IsTrue(DMSLocation.IsValidDM("4911N 12311W"));
            Assert.IsTrue(DMSLocation.IsValidDM("4911S 12311W"));
            Assert.IsTrue(DMSLocation.IsValidDM("4911S 12311E"));
            Assert.IsTrue(DMSLocation.IsValidDM("4911N 12311E"));
        }

        [TestMethod]
        public void Geography_Location_ValidateDecimalCoordinates()
        {
            Assert.IsTrue(DMSLocation.IsValidDecimal("-2246.9053 -8011.9977"));

            Assert.IsTrue(DMSLocation.IsValidDecimal("-22.469053 -80.119977"));
            Assert.IsTrue(DMSLocation.IsValidDecimal("-22.469053 80.119977"));
            Assert.IsTrue(DMSLocation.IsValidDecimal("22.469053 -80.119977"));
            Assert.IsTrue(DMSLocation.IsValidDecimal("22.469053 80.119977"));

            Assert.IsFalse(DMSLocation.IsValidDecimal("122.469053 -80.119977"));
            Assert.IsFalse(DMSLocation.IsValidDecimal("22.469053 -180.119977"));
            Assert.IsFalse(DMSLocation.IsValidDecimal("not a valid pair"));

            Assert.IsFalse(DMSLocation.IsValidDecimal("22.469053N 80.119977W"));
            Assert.IsFalse(DMSLocation.IsValidDecimal("22.469053N 80.119977E"));
            Assert.IsFalse(DMSLocation.IsValidDecimal("22.469053S 80.119977W"));
            Assert.IsFalse(DMSLocation.IsValidDecimal("22.469053S 80.119977E"));

            var location = DMSLocation.FromDecimal("45.99194444 -120.68305556");
            Assert.IsNotNull(location);
            Assert.IsTrue(location.Latitude.Degrees == 45 && location.Longitude.Degrees == -120);
            Assert.IsTrue(location.Latitude.Minutes == 59 && location.Longitude.Minutes == 40);
            Assert.IsTrue(location.Latitude.Seconds == 31 && location.Longitude.Seconds == 59);

            location = DMSLocation.FromDecimal("4559.50 -12045.25");
            Assert.IsNotNull(location);
            Assert.IsTrue(location.Latitude.Degrees == 45 && location.Longitude.Degrees == -120);
            Assert.IsTrue(location.Latitude.Minutes == 59 && location.Longitude.Minutes == 45);
            Assert.IsTrue(location.Latitude.Seconds == 30 && location.Longitude.Seconds == 15);
        }

        [TestMethod]
        public void Geography_Location_ValidateDecimalToDMSConvertions()
        {
            var dms = DMSLocation.FromDMS("455931N 1204059W");
            Assert.IsTrue(RoundHelper(dms.Latitude.AsDecimal) == 45.991944);
            Assert.IsTrue(RoundHelper(dms.Longitude.AsDecimal) == -120.683056);

            dms = DMSLocation.FromDMS("805959N 1200031W");
            Assert.IsTrue(RoundHelper(dms.Latitude.AsDecimal) == 80.999722);
            Assert.IsTrue(RoundHelper(dms.Longitude.AsDecimal) == -120.008611);
        }

        [TestMethod]
        public void Geography_Location_ValidateSubfixedDecimalCoordinates()
        {
            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("22.469053N 80.119977W"));
            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("22.469053N 80.119977E"));
            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("22.469053S 80.119977W"));
            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("22.469053S 80.119977E"));

            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("2246.9053N 8059.9977W"));
            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("2246.9053N 8011.9977E"));
            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("2246.9053S 8011.9977W"));
            Assert.IsTrue(DMSLocation.IsValidSubfixedDecimal("2246.9053S 8011.9977E"));

            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("2261.9053S 8011.9977E"));
            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("2261.9053 8011.9977"));

            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("22.469053 80.119977"));
            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("22.469053 -80.119977"));
            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("-22.469053 80.119977"));

            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("22.469053N 80.119977"));
            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("22.469053 80.119977W"));

            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("-22.469053N 80.119977W"));
            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("22.469053N -80.119977W"));

            Assert.IsFalse(DMSLocation.IsValidSubfixedDecimal("not a valid pair"));

            var location = DMSLocation.FromSubfixedDecimal("45.99194444N 120.68305556W");
            Assert.IsNotNull(location);
            Assert.IsTrue(location.Latitude.Degrees == 45 && location.Longitude.Degrees == -120);
            Assert.IsTrue(location.Latitude.Minutes == 59 && location.Longitude.Minutes == 40);
            Assert.IsTrue(location.Latitude.Seconds == 31 && location.Longitude.Seconds == 59);
        }

        [TestMethod]
        public void Geography_Location_ValidateKnownFormats()
        {
            Assert.IsTrue(DMSLocation.IsKnownFormat("455931N 1204059W"));
            Assert.IsTrue(DMSLocation.IsKnownFormat("45.5931 -120.4059"));
            Assert.IsTrue(DMSLocation.IsKnownFormat("45.5931N 120.4059W"));
            Assert.IsTrue(DMSLocation.IsKnownFormat("4559N 12040W"));

            var dms = DMSLocation.FromKnownFormat("805959N 1200031W");
            Assert.IsNotNull(dms);
            Assert.IsTrue(RoundHelper(dms.Latitude.AsDecimal) == 80.999722);
            Assert.IsTrue(RoundHelper(dms.Longitude.AsDecimal) == -120.008611);

            dms = DMSLocation.FromKnownFormat("80.999722 -120.008611");
            Assert.IsNotNull(dms);
            Assert.IsTrue(RoundHelper(dms.Latitude.AsDecimal) == 80.999722);
            Assert.IsTrue(RoundHelper(dms.Longitude.AsDecimal) == -120.008611);

            dms = DMSLocation.FromKnownFormat("80.999722N 120.008611W");
            Assert.IsNotNull(dms);
            Assert.IsTrue(RoundHelper(dms.Latitude.AsDecimal) == 80.999722);
            Assert.IsTrue(RoundHelper(dms.Longitude.AsDecimal) == -120.008611);
        }

        [TestMethod]
        public void Geography_Location_ToDecimalLatitudeLongitude()
        {
            Assert.AreEqual("80.999722 -120.008611",  DMSLocation.ToDecimalLatitudeLongitude("80.999722 -120.008611"));
            Assert.AreEqual("80.999722 -120.008611",  DMSLocation.ToDecimalLatitudeLongitude("80.999722N 120.008611W"));
            Assert.AreEqual("-80.999722 120.008611",  DMSLocation.ToDecimalLatitudeLongitude("80.999722S 120.008611E"));
            Assert.AreEqual("-80.999722 -120.008611", DMSLocation.ToDecimalLatitudeLongitude("80.999722S 120.008611W"));
            Assert.AreEqual("80.999722 120.008611",   DMSLocation.ToDecimalLatitudeLongitude("80.999722N 120.008611E"));

            Assert.AreEqual("80 -120", DMSLocation.ToDecimalLatitudeLongitude("800000N 1200000W"));
        }

        [TestMethod]
        public void Geography_Location_TestCommaSeparated()
        {
            var dms = DMSLocation.FromKnownFormat("455931N,1204059W");
            Assert.IsNotNull(dms);
            Assert.AreEqual("455931N 1204059W", dms.ToDMS());
        }

        [TestMethod]
        public void Geography_MiddlePoint_between()
        {
            var from = new GeoCoordinate(45.334733, -75.693294);
            var to = new GeoCoordinate(22.404886, -79.966285);
            var mid = GeoCoordinateExt.GetCentralGeoCoordinate(from, to);
            Assert.IsTrue(mid != null);
        }

        double RoundHelper(double value)
        {
            return Math.Round(value * 1000000) / 1000000.0;
        }
    }
}
