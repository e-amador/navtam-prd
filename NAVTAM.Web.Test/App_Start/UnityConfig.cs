﻿using Microsoft.Practices.Unity;
using Unity.WebApi;
using System.Web;
using System.Web.Http;

namespace NAVTAM.Web.Test
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            GlobalConfiguration.Configuration.Dependency‌​Resolver = new UnityDependencyResolver(container);
        }
    }
}