﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class AeroRDSCacheConfig : EntityTypeConfiguration<AeroRDSCache>
    {
        public AeroRDSCacheConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasMaxLength(32);
        }
    }
}
