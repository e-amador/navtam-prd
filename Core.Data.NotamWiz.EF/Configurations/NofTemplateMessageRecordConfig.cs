﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class NofTemplateMessageRecordConfig : EntityTypeConfiguration<NofTemplateMessageRecord>
    {
        public NofTemplateMessageRecordConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            HasRequired(e => e.Template)
                .WithMany()
                .HasForeignKey(e => e.TemplateId)
            .WillCascadeOnDelete(false);
        }
    }
}


