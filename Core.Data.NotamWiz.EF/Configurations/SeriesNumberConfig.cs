﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    class SeriesNumberConfig : EntityTypeConfiguration<SeriesNumber>
    {
        public SeriesNumberConfig()
        {
            HasKey(e => e.Id);

            Property(e => e.Series).HasMaxLength(1);
            Property(e => e.RowVersion).IsConcurrencyToken();

            Property(e => e.Year)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_SeriesYear", 1) { IsUnique = true }));

            Property(e => e.Series)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_SeriesYear", 2) { IsUnique = true }));
        }
    }
}
