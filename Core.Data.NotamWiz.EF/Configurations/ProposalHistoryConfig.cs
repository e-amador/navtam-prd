﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class ProposalHistoryConfig : EntityTypeConfiguration<ProposalHistory>
    {
        public ProposalHistoryConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(e => e.Proposal)
                .WithMany()
                .HasForeignKey(e => e.ProposalId)
            .WillCascadeOnDelete(false);

            HasRequired(e => e.IcaoSubject)
                .WithMany()
                .HasForeignKey(e => e.IcaoSubjectId)
            .WillCascadeOnDelete(false);

            HasRequired(e => e.IcaoSubjectCondition)
                .WithMany()
                .HasForeignKey(e => e.IcaoConditionId)
            .WillCascadeOnDelete(false);

            HasRequired(e => e.Category)
                .WithMany()
                .HasForeignKey(e => e.CategoryId)
            .WillCascadeOnDelete(false);

            HasRequired(e => e.Category)
                .WithMany()
                .HasForeignKey(e => e.CategoryId);

            HasRequired(e => e.User)
                .WithMany()
                .HasForeignKey(e => e.UserId);

            Property(e => e.Name).IsRequired();

            Property(e => e.Name).HasMaxLength(20);
            Property(e => e.Version).HasMaxLength(10);
            Property(e => e.Nof).HasMaxLength(6);
            Property(e => e.Series).HasMaxLength(1);
            Property(e => e.ReferredSeries).HasMaxLength(1);
            Property(e => e.Fir).HasMaxLength(6);
            Property(e => e.Code23).HasMaxLength(2);
            Property(e => e.Code45).HasMaxLength(2);
            Property(e => e.Traffic).HasMaxLength(2);
            Property(e => e.Purpose).HasMaxLength(4);
            Property(e => e.Scope).HasMaxLength(4);
            Property(e => e.Coordinates).HasMaxLength(20);
            Property(e => e.ItemA).HasMaxLength(96);
            Property(e => e.Operator).HasMaxLength(20);
            Property(e => e.Originator).HasMaxLength(250);
            Property(e => e.ModifiedByUsr).HasMaxLength(20);
            Property(e => e.NotamId).HasMaxLength(15);
        }
    }
}
