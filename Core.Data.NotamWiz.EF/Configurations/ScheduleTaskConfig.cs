﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class ScheduleTaskConfig : EntityTypeConfiguration<ScheduleTask>
    {
        public ScheduleTaskConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.RowVersion).IsConcurrencyToken();
        }
    }
}