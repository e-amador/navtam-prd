﻿using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class ProposalAnnotationConfig : EntityTypeConfiguration<ProposalAnnotation>
    {
        public ProposalAnnotationConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(e => e.Proposal)
                .WithMany()
                .HasForeignKey(e => e.ProposalId)
            .WillCascadeOnDelete(false);

            HasRequired(e => e.User)
                .WithMany()
                .HasForeignKey(e => e.UserId);

            Property(e => e.Note).IsRequired();
        }
    }
}
