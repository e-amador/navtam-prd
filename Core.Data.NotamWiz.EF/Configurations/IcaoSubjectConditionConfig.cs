﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class IcaoSubjectConditionConfig : EntityTypeConfiguration<IcaoSubjectCondition>
    {
        public IcaoSubjectConditionConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Description).IsRequired();
            Property(e => e.DescriptionFrench).IsRequired();
            Property(e => e.Code).IsRequired().HasMaxLength(2);
            Property(e => e.Lower).IsRequired();
            Property(e => e.Upper).IsRequired();
        }
    }
}
