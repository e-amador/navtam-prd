﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class MessageExchangeQueueConfig : EntityTypeConfiguration<MessageExchangeQueue>
    {
        public MessageExchangeQueueConfig()
        {
            HasKey(e => e.Id);

            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired();

            Property(e => e.PriorityCode).IsRequired();
            Property(e => e.PriorityCode).HasMaxLength(2);
        }
    }
}