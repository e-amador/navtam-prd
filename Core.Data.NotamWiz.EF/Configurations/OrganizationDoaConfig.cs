﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class OrganizationDoaConfig : EntityTypeConfiguration<OrganizationDoa>
    {
        public OrganizationDoaConfig()
        {
            HasKey(e => new { e.DoaId, e.OrganizationId } );
        }
    }
}
