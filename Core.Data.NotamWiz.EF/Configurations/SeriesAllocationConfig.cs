﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class SeriesAllocationConfig : EntityTypeConfiguration<SeriesAllocation>
    {
        public SeriesAllocationConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(e => e.Region)
                .WithMany(e=>e.Series)
                .HasForeignKey(e => e.RegionId)
                .WillCascadeOnDelete(false);

            Property(e => e.Series).IsRequired();
            Property(e => e.DisseminationCategory).IsRequired();
            Property(e => e.QCode).IsRequired();
        }
    }
}

