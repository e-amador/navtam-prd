﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class RolePermissionConfig : EntityTypeConfiguration<RolePermission>
    {
        public RolePermissionConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(e => e.Role)
                .WithMany(e=>e.RolePermissions)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.Permission)
                .WithMany(e=>e.RolePermissions)
                .HasForeignKey(e => e.PermissionId)
                .WillCascadeOnDelete(false);
        }
    }
}


