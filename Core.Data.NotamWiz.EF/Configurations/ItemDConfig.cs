﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class ItemDConfig : EntityTypeConfiguration<ItemD>
    {
        public ItemDConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(e => e.Proposal)
                .WithMany()
                .HasForeignKey(e => e.ProposalId)
                .WillCascadeOnDelete(false);

            Property(e => e.Event).IsRequired();
        }
    }
}
