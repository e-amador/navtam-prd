﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class AeroRDSCacheStatusConfig : EntityTypeConfiguration<AeroRDSCacheStatus>
    {
        public AeroRDSCacheStatusConfig()
        {
            HasKey(e => e.Id);
        }
    }
}
