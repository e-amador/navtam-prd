﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class NsdCategoryConfig : EntityTypeConfiguration<NsdCategory>
    {
        public NsdCategoryConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasOptional(e => e.ParentCategory)
                .WithMany()
                .HasForeignKey(e => e.ParentCategoryId);
                //.WillCascadeOnDelete(false);

            HasMany(e => e.Organizations)
                .WithRequired(e => e.NsdCategory)
                .WillCascadeOnDelete(true);

            Property(e => e.Name).IsRequired();
        }
    }
}
