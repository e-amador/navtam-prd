﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class OriginatorInfoConfig : EntityTypeConfiguration<OriginatorInfo>
    {
        public OriginatorInfoConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(e => e.Organization)
                .WithMany()
                .HasForeignKey(e => e.OrganizationId)
                .WillCascadeOnDelete(false);
        }
    }
}
