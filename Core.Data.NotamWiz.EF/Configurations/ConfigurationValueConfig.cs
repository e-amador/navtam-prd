﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class ConfigurationValueConfig : EntityTypeConfiguration<ConfigurationValue>
    {
        public ConfigurationValueConfig()
        {
            HasKey(e => e.Name);
        }
    }
}
