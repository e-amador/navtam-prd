﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class NdsIncomingRequestConfig : EntityTypeConfiguration<NdsIncomingRequest>
    {
        public NdsIncomingRequestConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(e => e.Body).IsRequired();
            Property(e => e.MessageId).IsRequired();
            Property(e => e.RecievedDate).IsRequired();
            Property(e => e.Metadata).IsRequired();
            Property(e => e.ProcessedDate).IsOptional();
        }
    }
}