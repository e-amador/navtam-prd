﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class DoaConfig : EntityTypeConfiguration<Doa>
    {
        public DoaConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Name).IsRequired();

            HasMany(e => e.Users)
                .WithRequired(e => e.Doa)
                .WillCascadeOnDelete(true);

            HasMany(d => d.Organizations)
                .WithRequired(o => o.Doa)
                .WillCascadeOnDelete(true);
        }
    }
}
