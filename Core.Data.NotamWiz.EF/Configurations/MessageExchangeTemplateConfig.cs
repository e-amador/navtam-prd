﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class MessageExchangeTemplateConfig : EntityTypeConfiguration<MessageExchangeTemplate>
    {
        public MessageExchangeTemplateConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(e => e.Name).IsRequired();
        }
    }
}


