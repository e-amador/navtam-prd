﻿using System.Data.Entity.ModelConfiguration;
using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class NotamConfig : EntityTypeConfiguration<Notam>
    {
        public NotamConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(e => e.SequenceNumber).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasOptional(e => e.ReplaceNotam)
                .WithMany()
                .HasForeignKey(e => e.ReplaceNotamId)
                .WillCascadeOnDelete(false);

            Property(t => t.RootId)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_DISSEMINATED_ROOT_ID", 1) { IsUnique = false }));

            Property(e => e.NotamId)
                .HasMaxLength(8)
                .IsRequired()
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_NOTAM_NUMBER_ID", 1) { IsUnique = false }));

            Property(e => e.Nof).HasMaxLength(6);
            Property(e => e.Series).HasMaxLength(1);
            Property(e => e.ReferredSeries).HasMaxLength(1);
            Property(e => e.Fir).HasMaxLength(6);
            Property(e => e.Code23).HasMaxLength(2);
            Property(e => e.Code45).HasMaxLength(2);
            Property(e => e.Traffic).HasMaxLength(2);
            Property(e => e.Purpose).HasMaxLength(4);
            Property(e => e.Scope).HasMaxLength(4);
            Property(e => e.Coordinates).HasMaxLength(20);
            Property(e => e.ItemA).HasMaxLength(96);
            Property(e => e.Operator).HasMaxLength(20);
            Property(e => e.Originator).HasMaxLength(250);
            Property(e => e.ModifiedByUsr).HasMaxLength(20);
            Property(e => e.NotamId).HasMaxLength(15);
        }
    }
}
