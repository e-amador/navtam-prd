﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class UserDoaConfig : EntityTypeConfiguration<UserDoa>
    {
        public UserDoaConfig()
        {
            HasKey(e => new { e.UserId, e.DoaId });
        }
    }
}
