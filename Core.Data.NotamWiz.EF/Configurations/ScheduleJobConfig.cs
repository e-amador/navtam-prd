﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class ScheduleJobConfig : EntityTypeConfiguration<ScheduleJob>
    {
        public ScheduleJobConfig()
        {
            HasKey(e => e.Id);
        }
    }
}
