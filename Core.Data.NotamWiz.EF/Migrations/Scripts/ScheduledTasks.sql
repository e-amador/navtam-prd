IF NOT EXISTS (select top 1 Id from [dbo].[ScheduleTask] where id = 'GENERATE_CHECKLIST')
BEGIN
	INSERT [dbo].[ScheduleTask] ([Id], [IsProcessed], [ProcessedDate], [RowVersion]) VALUES (N'GENERATE_CHECKLIST',   0, CAST(N'2019-01-01T00:00:00.000' AS DateTime), N'4300a3cd-4f58-4637-9ec0-4f1c01263586')
END

IF NOT EXISTS (select top 1 Id from [dbo].[ScheduleTask] where id = 'AERORDS_CACHE_LOAD')
BEGIN
	INSERT [dbo].[ScheduleTask] ([Id], [IsProcessed], [ProcessedDate], [RowVersion]) VALUES (N'AERORDS_CACHE_LOAD',   0, CAST(N'2019-01-01T00:00:00.000' AS DateTime), N'289a510f-0872-4261-b26d-0cf6b29bf908')
END

IF NOT EXISTS (select top 1 Id from [dbo].[ScheduleTask] where id = 'AERORDS_CACHE_SYNC')
BEGIN
	INSERT [dbo].[ScheduleTask] ([Id], [IsProcessed], [ProcessedDate], [RowVersion]) VALUES (N'AERORDS_CACHE_SYNC',   0, CAST(N'2019-01-01T00:00:00.000' AS DateTime), N'02e75fc5-eb56-4fc9-9b79-dffd50cd4997')
END
