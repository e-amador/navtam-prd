﻿--NSD SEED SO WE DON'T LOSE INFORMATION



	IF(NOT Exists(SELECT * FROM  [dbo].[NSD] WHERE Id = 1))
	BEGIN

		SET IDENTITY_INSERT [dbo].[NSD] ON

		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (1, N'ARCAL', N'ARCAL', N'Aircraft Radio Control Aerodrome Lighting ', 0, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (2, N'AD Obstacle', N'Obstacles', N'Obstacles at Airport', 0, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (3, N'ABN HBN', N'Beacon', N'Airport or Hazard Beacon.', 0, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (4, N'Service unavailable', N'Services', N'Report unavailable services in AD', 0, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (5, N'OBST at AD', N'Obstacles', N'Obstacle located by LATLON auto-referenced', 0, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (6, N'RWY Lighting', N'Runway Lighting', N'Runway Lighting', 0, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (7, N'FORM OBST at AD', N'Obstacles', N'Report Obstacle using Form ', 1, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (8, N'RWY CLOSURE', N'Runway', N'RWY Closure NOTAM', 1, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (9, N'CATCH ALL', N'Catch All', N'Catch all for all other  NOTAM Proposals', 1, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (9, N'CATCH ALL', N'Catch All', N'Catch all for all other  NOTAM Proposals', 1, 0)
		INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (10, N'OBST LGT U/S', N'Obstacles', N'Obst Light Unserviceable', 1, 0)
		--INSERT [dbo].[NSD] ([Id], [Name], [Category], [Description], [Active], [LimitedAccess]) VALUES (10, N'OBST LGT U/S', N'All', N'Obst Light Unserviceable', 1, 0) --added for demo purposes - mike

		SET IDENTITY_INSERT [dbo].[NSD] OFF
		SET IDENTITY_INSERT [dbo].[NSDVerion] ON 



		INSERT [dbo].[NSDVerion] ([Id], [NsdId], [SupportedSubjects], [AssociatedRoles], [Version], [Description], [ReleaseDate], [FileLocation], [HasView], [SubjectsFilter], [ReleasedBy_UserId]) VALUES (2, 7, N'Ahp', NULL, 0.1, N'Initial version', CAST(N'2015-01-13 00:00:00.000' AS DateTime), N'Ahp-Obstacle-Auto-Calc', 1, N'SELECT 1 where 
			EXISTS (SELECT rdn.* FROM EFFECTIVE_CANADA.Rdn rdn join EFFECTIVE_CANADA.Rwy rwy on rdn.FK_RwyUid = rwy.mid join EFFECTIVE_CANADA.Ahp ahp on rwy.FK_AhpUid = ahp.mid WHERE ahp.mid = ''@subjectMid'') 
			AND 
			NOT EXISTS(SELECT 1 from EFFECTIVE_CANADA.Rdn rdn join EFFECTIVE_CANADA.Rwy rwy on rdn.FK_RwyUid = rwy.mid join EFFECTIVE_CANADA.Ahp ahp on rwy.FK_AhpUid = ahp.mid
			WHERE ahp.mid = ''@subjectMid'' and (rdn.geoLat is null or rdn.geoLong is null))', NULL)
		INSERT [dbo].[NSDVerion] ([Id], [NsdId], [SupportedSubjects], [AssociatedRoles], [Version], [Description], [ReleaseDate], [FileLocation], [HasView], [SubjectsFilter], [ReleasedBy_UserId]) VALUES (6, 8, N'Rwy', NULL, 0.1, N'Initial Version', CAST(N'2015-02-23 00:00:00.000' AS DateTime), N'Rwy-Closure', 1, NULL, NULL)
		INSERT [dbo].[NSDVerion] ([Id], [NsdId], [SupportedSubjects], [AssociatedRoles], [Version], [Description], [ReleaseDate], [FileLocation], [HasView], [SubjectsFilter], [ReleasedBy_UserId]) VALUES (8, 9, N'ALL', N'FIC_ROLE', 0.1, N'Initial Version', CAST(N'2015-02-23 00:00:00.000' AS DateTime), N'CATCH-ALL', 1, NULL, NULL)
		INSERT [dbo].[NSDVerion] ([Id], [NsdId], [SupportedSubjects], [AssociatedRoles], [Version], [Description], [ReleaseDate], [FileLocation], [HasView], [SubjectsFilter], [ReleasedBy_UserId]) VALUES (8, 9, N'Rwy', NULL, 0.1, N'Initial Version', CAST(N'2015-02-23 00:00:00.000' AS DateTime), N'Ahp-ObstacleLGT-US', 1, NULL, NULL)

		SET IDENTITY_INSERT [dbo].[NSDVerion] OFF
END