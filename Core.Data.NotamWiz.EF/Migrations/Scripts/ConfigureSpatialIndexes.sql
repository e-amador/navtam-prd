
-----------------------------------
--	SdoSubject
-----------------------------------

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='SIndx_SDOSubjects_SubjectGeoRefPoint' AND object_id = OBJECT_ID('SdoSubject'))
BEGIN
CREATE SPATIAL INDEX [SIndx_SDOSubjects_SubjectGeoRefPoint] ON [dbo].[SdoSubject]
(
	[SubjectGeoRefPoint]
)USING  GEOGRAPHY_AUTO_GRID 
WITH (
CELLS_PER_OBJECT = 12, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
