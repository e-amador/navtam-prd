-----------------------------------
--	Message
-----------------------------------

IF NOT EXISTS (SELECT * FROM sys.service_message_types WHERE name = N'//navcanada.se.notamwiz/mto')
BEGIN
	CREATE MESSAGE TYPE [//navcanada.se.notamwiz/mto] VALIDATION = NONE
END

-----------------------------------
--	Contract
-----------------------------------

IF NOT EXISTS (SELECT * FROM sys.service_contracts WHERE name = N'//navcanada.se.notamwiz/mto-queue-contract')
BEGIN
	CREATE CONTRACT [//navcanada.se.notamwiz/mto-queue-contract] ([//navcanada.se.notamwiz/mto] SENT BY INITIATOR)
END

-----------------------------------
--	Initiator to TaskEngine
-----------------------------------

IF NOT EXISTS (SELECT * FROM sys.service_queues WHERE name = N'InitiatorToTaskEngine-SendQueue')
BEGIN
	CREATE QUEUE [InitiatorToTaskEngine-SendQueue]
END

IF NOT EXISTS (SELECT * FROM sys.services WHERE name = N'//navcanada.se.notamwiz/InitiatorToTaskEngine-SendService')
BEGIN
	CREATE SERVICE [//navcanada.se.notamwiz/InitiatorToTaskEngine-SendService] ON QUEUE [InitiatorToTaskEngine-SendQueue]([//navcanada.se.notamwiz/mto-queue-contract])
END

IF NOT EXISTS (SELECT * FROM sys.service_queues WHERE name = N'InitiatorToTaskEngine-ReceiveQueue')
BEGIN
	CREATE QUEUE [InitiatorToTaskEngine-ReceiveQueue]
	ALTER QUEUE [InitiatorToTaskEngine-ReceiveQueue] WITH POISON_MESSAGE_HANDLING ( STATUS = OFF )
END

IF NOT EXISTS (SELECT * FROM sys.services WHERE name = N'//navcanada.se.notamwiz/InitiatorToTaskEngine-ReceiveService')
BEGIN
	CREATE SERVICE [//navcanada.se.notamwiz/InitiatorToTaskEngine-ReceiveService] ON QUEUE [InitiatorToTaskEngine-ReceiveQueue]([//navcanada.se.notamwiz/mto-queue-contract])
END

-----------------------------------
--	TaskEngine to Initiator
-----------------------------------

IF NOT EXISTS (SELECT * FROM sys.service_queues WHERE name = N'TaskEngineToInitiator-SendQueue')
BEGIN
	CREATE QUEUE [TaskEngineToInitiator-SendQueue]
END

IF NOT EXISTS (SELECT * FROM sys.services WHERE name = N'//navcanada.se.notamwiz/TaskEngineToInitiator-SendService')
BEGIN
	CREATE SERVICE [//navcanada.se.notamwiz/TaskEngineToInitiator-SendService] ON QUEUE [TaskEngineToInitiator-SendQueue]([//navcanada.se.notamwiz/mto-queue-contract])
END

IF NOT EXISTS (SELECT * FROM sys.service_queues WHERE name = N'TaskEngineToInitiator-ReceiveQueue')
BEGIN
	CREATE QUEUE [TaskEngineToInitiator-ReceiveQueue]
	ALTER QUEUE [TaskEngineToInitiator-ReceiveQueue] WITH POISON_MESSAGE_HANDLING ( STATUS = OFF )
END

IF NOT EXISTS (SELECT * FROM sys.services WHERE name = N'//navcanada.se.notamwiz/TaskEngineToInitiator-ReceiveService')
BEGIN
	CREATE SERVICE [//navcanada.se.notamwiz/TaskEngineToInitiator-ReceiveService] ON QUEUE [TaskEngineToInitiator-ReceiveQueue]([//navcanada.se.notamwiz/mto-queue-contract])
END

