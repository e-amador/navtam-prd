﻿BEGIN TRANSACTION

-- create developer user

	INSERT [dbo].[User] ([Id], [DefaultLanguage], [FirstName], [LastName], [LockoutEnabled], [Address], [Position], [Fax], [IsApproved], [IsDeleted], [OrganizationId], [UserPreferenceId], [RegistrationId], [DigitalSlaId], [LastPasswordChangeDate], [AccessToken], [Disabled], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [AccessFailedCount], [UserName]) 
	VALUES (N'1e65c662-58d3-4cbc-aea8-a7951d020036', 0, N'Dev', N'Dev', 0, N'280 Huntclub', N'developer', NULL, 1, 0, NULL, NULL, NULL, NULL, CAST(N'2019-09-17T14:34:11.160' AS DateTime), NULL, 0, N'admin@NavCanada.ca', 1, N'AOuBJONwl7JUVejxQODYEsOzNBFwr46SutSS9yIygv2DR0lxwUUtfz51CusE5W88og==', N'5390c56a-e077-4626-a0cd-8616c3e6a600', NULL, 0, 0, NULL, 0, N'Developer')
	
-- create developer role
	
	INSERT [dbo].[Role] ([Id], [Name], [Description], [Discriminator])
	VALUES (N'0b6a1b9e-704d-4a68-afa3-bc01586efa3c', N'Developer', NULL, N'ApplicationRole')
	

-- add user to role
	
	INSERT [dbo].[UserRole] ([UserId], [RoleId])
	VALUES (N'1e65c662-58d3-4cbc-aea8-a7951d020036', N'0b6a1b9e-704d-4a68-afa3-bc01586efa3c')
	
COMMIT