/****** Script for SelectTopNRows command from SSMS  ******/
DECLARE @NofDoaId int = 0;
DECLARE @NavCanadaOrgId int = 0;
DECLARE @CatchAllId int = 0;
DECLARE @AdminId nvarchar(128) = '';
DECLARE @AdminRoleId nvarchar(128) = '';

	IF EXISTS (select top 1 d.Id from [dbo].[Doa] d where d.Name = 'NOF')
	BEGIN
		SET @NofDoaId = (SELECT Id FROM [dbo].[Doa] WHERE Name = 'NOF');
	END;

	IF EXISTS (select top 1 d.Id from [dbo].[Organization] d WHERE Name = 'NAV CANADA')
	BEGIN
		SET @NavCanadaOrgId = (SELECT Id FROM [dbo].[Organization] WHERE Name = 'NAV CANADA');
		UPDATE [dbo].[Organization] SET Type = 3 WHERE Name = 'NAV CANADA';
	END

	IF EXISTS (select top 1 d.Id from [dbo].[NsdCategory] d WHERE Name = 'CATCHALL')
	BEGIN
		SET @CatchAllId = (SELECT Id FROM [dbo].[NsdCategory] WHERE Name = 'CATCHALL');
		UPDATE [dbo].[NsdCategory] SET Operational = 1 WHERE Name = 'CATCHALL';
	END

	IF EXISTS (select top 1 d.Id from [dbo].[User] d WHERE UserName = 'Admin')
	BEGIN
		SET @AdminId = (SELECT Id FROM [dbo].[User] WHERE UserName = 'Admin');
	END

	IF EXISTS (select top 1 d.Id from [dbo].[Role] d WHERE Name = 'Administrator')
	BEGIN
		SET @AdminRoleId = (SELECT Id FROM [dbo].[Role] WHERE Name = 'Administrator');
	END
	
	IF (@NofDoaId != 0 AND @NavCanadaOrgId != 0 AND @CatchAllId != 0 AND @AdminId != '' AND @AdminRoleId != '')
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 DoaId FROM [dbo].[OrganizationDoa] WHERE DoaId = @NofDoaId AND OrganizationId = @NavCanadaOrgId)
		BEGIN
			INSERT INTO [dbo].[OrganizationDoa] (DoaId, OrganizationId) VALUES (@NofDoaId, @NavCanadaOrgId);
		END
		IF NOT EXISTS (SELECT TOP 1 OrganizationId FROM [dbo].[OrganizationNsdCategory] WHERE OrganizationId = @NavCanadaOrgId AND [NsdCategoryId] = @CatchAllId)
		BEGIN
			INSERT INTO [dbo].[OrganizationNsdCategory] (OrganizationId, NsdCategoryId) VALUES (@NavCanadaOrgId, @CatchAllId);
		END
		IF NOT EXISTS (SELECT TOP 1 UserId FROM [dbo].[UserRole] WHERE UserId = @AdminId AND RoleId = @AdminRoleId)
		BEGIN
			INSERT INTO [dbo].[UserRole] (UserId, RoleId) VALUES (@AdminId, @AdminRoleId);
		END
		IF NOT EXISTS (SELECT TOP 1 [UserId] FROM [dbo].[UserDoa] WHERE UserId = @AdminId AND DoaId = @NofDoaId AND OrganizationId = @NavCanadaOrgId)
		BEGIN
			INSERT INTO [dbo].[UserDoa] (UserId, DoaId, OrganizationId) VALUES (@AdminId, @NofDoaId,@NavCanadaOrgId);
		END
	END

