﻿DELETE FROM Proposal;
DELETE FROM ProposalAttachment;
DELETE FROM PoisonMto;
DELETE FROM PoisonMtoRecord;
DELETE FROM Log;

declare @c uniqueidentifier
while(1=1)
begin
    select top 1 @c = conversation_handle from [dbo].[InitiatorToTaskEngine-ReceiveQueue]
    if (@@ROWCOUNT = 0)
    break
    end conversation @c with cleanup
end

while(1=1)
begin
    select top 1 @c = conversation_handle from [dbo].[InitiatorToTaskEngine-SendQueue]
    if (@@ROWCOUNT = 0)
    break
    end conversation @c with cleanup
end

while(1=1)
begin
    select top 1 @c = conversation_handle from [dbo].[TaskEngineToInitiator-ReceiveQueue]
    if (@@ROWCOUNT = 0)
    break
    end conversation @c with cleanup
end

while(1=1)
begin
    select top 1 @c = conversation_handle from [dbo].[TaskEngineToInitiator-SendQueue]
    if (@@ROWCOUNT = 0)
    break
    end conversation @c with cleanup
end

