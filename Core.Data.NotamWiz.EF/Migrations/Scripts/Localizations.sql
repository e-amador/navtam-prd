
IF OBJECT_ID(N'[dbo].[Localizations]', N'U') IS NULL
BEGIN

	SET ANSI_NULLS ON


	SET QUOTED_IDENTIFIER ON


	CREATE TABLE [dbo].[Localizations](
		[pk] [int] IDENTITY(1,1) NOT NULL,
		[ResourceId] [nvarchar](1024) NOT NULL,
		[Value] [nvarchar](max) NULL,
		[LocaleId] [nvarchar](10) NULL,
		[ResourceSet] [nvarchar](512) NULL,
		[Type] [nvarchar](512) NULL,
		[BinFile] [varbinary](max) NULL,
		[TextFile] [nvarchar](max) NULL,
		[Filename] [nvarchar](128) NULL,
		[Comment] [nvarchar](512) NULL,
		[ValueType] [int] NOT NULL,
		[Updated] [datetime] NULL,
	 CONSTRAINT [PK_Localizations] PRIMARY KEY CLUSTERED 
	(
		[pk] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



	ALTER TABLE [dbo].[Localizations] ADD  CONSTRAINT [DF_Localizations_Text]  DEFAULT ('') FOR [Value]


	ALTER TABLE [dbo].[Localizations] ADD  CONSTRAINT [DF_Localizations_LocaleId]  DEFAULT ('') FOR [LocaleId]
	

	ALTER TABLE [dbo].[Localizations] ADD  CONSTRAINT [DF_Localizations_PageId]  DEFAULT ('') FOR [ResourceSet]
	

	ALTER TABLE [dbo].[Localizations] ADD  CONSTRAINT [DF_Localizations_Type]  DEFAULT ('') FOR [Type]
	

	ALTER TABLE [dbo].[Localizations] ADD  CONSTRAINT [DF_Localizations_Filename]  DEFAULT ('') FOR [Filename]
	

	ALTER TABLE [dbo].[Localizations] ADD  CONSTRAINT [DF_Localizations_ValueType]  DEFAULT ((0)) FOR [ValueType]
	

	ALTER TABLE [dbo].[Localizations] ADD  CONSTRAINT [DF_Localizations_Updated]  DEFAULT (getutcdate()) FOR [Updated]
	
END