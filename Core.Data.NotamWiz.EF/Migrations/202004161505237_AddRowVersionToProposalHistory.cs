namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRowVersionToProposalHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProposalHistory", "RowVersion", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProposalHistory", "RowVersion");
        }
    }
}
