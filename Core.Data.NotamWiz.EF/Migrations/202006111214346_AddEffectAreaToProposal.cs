namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Spatial;
    
    public partial class AddEffectAreaToProposal : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Proposal", "EffectArea", c => c.Geography());
            AddColumn("dbo.ProposalHistory", "EffectArea", c => c.Geography());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProposalHistory", "EffectArea");
            DropColumn("dbo.Proposal", "EffectArea");
        }
    }
}
