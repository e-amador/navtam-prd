namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReceivedDateToNotam : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notam", "Received", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notam", "Received");
        }
    }
}
