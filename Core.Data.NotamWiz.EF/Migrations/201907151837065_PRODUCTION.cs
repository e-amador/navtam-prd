namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PRODUCTION : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AerodromeDisseminationCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AhpMid = c.String(nullable: false),
                        AhpCodeId = c.String(nullable: false),
                        AhpName = c.String(nullable: false),
                        DisseminationCategory = c.Int(nullable: false),
                        Location = c.Geography(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AeroRDSCache",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 32),
                        EffectiveDate = c.DateTime(),
                        Cache = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AeroRDSCacheStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastEffectiveDate = c.DateTime(),
                        LastActiveDate = c.DateTime(),
                        LastEffectiveUpdated = c.DateTime(),
                        LastActiveUpdated = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ConfigurationValue",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(),
                        Value = c.String(),
                        Category = c.String(),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.DigitalSla",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Version = c.Single(nullable: false),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Doa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        DoaGeoArea = c.Geography(),
                        DoaType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrganizationDoa",
                c => new
                    {
                        DoaId = c.Int(nullable: false),
                        OrganizationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DoaId, t.OrganizationId })
                .ForeignKey("dbo.Organization", t => t.OrganizationId, cascadeDelete: true)
                .ForeignKey("dbo.Doa", t => t.DoaId, cascadeDelete: true)
                .Index(t => t.DoaId)
                .Index(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Organization",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(),
                        EmailAddress = c.String(nullable: false),
                        EmailDistribution = c.String(),
                        Telephone = c.String(nullable: false),
                        TypeOfOperation = c.String(nullable: false),
                        IsReadOnlyOrg = c.Boolean(nullable: false),
                        Type = c.Byte(nullable: false),
                        HeadOffice = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrganizationNsdCategory",
                c => new
                    {
                        OrganizationId = c.Int(nullable: false),
                        NsdCategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.OrganizationId, t.NsdCategoryId })
                .ForeignKey("dbo.NsdCategory", t => t.NsdCategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Organization", t => t.OrganizationId, cascadeDelete: true)
                .Index(t => t.OrganizationId)
                .Index(t => t.NsdCategoryId);
            
            CreateTable(
                "dbo.NsdCategory",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ParentCategoryId = c.Int(),
                        Name = c.String(nullable: false),
                        Operational = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NsdCategory", t => t.ParentCategoryId)
                .Index(t => t.ParentCategoryId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DefaultLanguage = c.Short(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        LockoutEnabled = c.Boolean(nullable: false),
                        Address = c.String(),
                        Position = c.String(nullable: false),
                        Fax = c.String(),
                        IsApproved = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        OrganizationId = c.Int(),
                        UserPreferenceId = c.Int(),
                        RegistrationId = c.Int(),
                        DigitalSlaId = c.Int(),
                        LastPasswordChangeDate = c.DateTime(nullable: false),
                        AccessToken = c.String(),
                        Disabled = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DigitalSla", t => t.DigitalSlaId)
                .ForeignKey("dbo.Organization", t => t.OrganizationId)
                .ForeignKey("dbo.Registration", t => t.RegistrationId)
                .ForeignKey("dbo.UserPreference", t => t.UserPreferenceId)
                .Index(t => t.OrganizationId)
                .Index(t => t.UserPreferenceId)
                .Index(t => t.RegistrationId)
                .Index(t => t.DigitalSlaId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Registration",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        EmailAddress = c.String(nullable: false),
                        Telephone = c.String(nullable: false),
                        TypeOfOperation = c.String(nullable: false),
                        HeadOffice = c.String(),
                        Status = c.Int(nullable: false),
                        StatusTime = c.DateTime(nullable: false),
                        RegisteredUserId = c.String(),
                        RegisteredUserName = c.String(nullable: false),
                        RegisteredUserFirstName = c.String(),
                        RegisteredUserLastName = c.String(),
                        RegisteredUserEmail = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserPreference",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpiredRecordsDateFilter = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserDoa",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        DoaId = c.Int(nullable: false),
                        OrganizationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.DoaId })
                .ForeignKey("dbo.Doa", t => t.DoaId, cascadeDelete: true)
                .Index(t => t.DoaId);
            
            CreateTable(
                "dbo.Feedback",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Firstname = c.String(nullable: false),
                        Lastname = c.String(nullable: false),
                        EmailAddress = c.String(nullable: false),
                        Organization = c.String(nullable: false),
                        Department = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        Comment = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GeoRegion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Geo = c.Geography(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SeriesAllocation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegionId = c.Int(nullable: false),
                        Series = c.String(nullable: false),
                        DisseminationCategory = c.Int(nullable: false),
                        QCode = c.String(nullable: false),
                        Subject = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeoRegion", t => t.RegionId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IcaoSubjectCondition",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubjectId = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        DescriptionFrench = c.String(nullable: false),
                        Code = c.String(nullable: false, maxLength: 2),
                        I = c.Boolean(nullable: false),
                        V = c.Boolean(nullable: false),
                        N = c.Boolean(nullable: false),
                        B = c.Boolean(nullable: false),
                        O = c.Boolean(nullable: false),
                        M = c.Boolean(nullable: false),
                        Lower = c.Int(nullable: false),
                        Upper = c.Int(nullable: false),
                        Radius = c.Int(nullable: false),
                        CancelNotamOnly = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        AllowFgEntry = c.Boolean(nullable: false),
                        AllowPurposeChange = c.Boolean(nullable: false),
                        Version = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IcaoSubject", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.IcaoSubject",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        NameFrench = c.String(nullable: false),
                        EntityCode = c.String(nullable: false),
                        Code = c.String(nullable: false, maxLength: 2),
                        Scope = c.String(nullable: false, maxLength: 2),
                        AllowItemAChange = c.Boolean(nullable: false),
                        AllowScopeChange = c.Boolean(nullable: false),
                        AllowSeriesChange = c.Boolean(nullable: false),
                        Version = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemD",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventId = c.String(),
                        CalendarType = c.Int(nullable: false),
                        ProposalId = c.Int(nullable: false),
                        CalendarId = c.Int(nullable: false),
                        Event = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Proposal", t => t.ProposalId)
                .Index(t => t.ProposalId);
            
            CreateTable(
                "dbo.Proposal",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        CategoryId = c.Int(nullable: false),
                        Version = c.String(maxLength: 10),
                        OrganizationId = c.Int(nullable: false),
                        SubjectId = c.String(),
                        Nof = c.String(maxLength: 6),
                        Series = c.String(maxLength: 1),
                        Number = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        ProposalType = c.Int(nullable: false),
                        ReferredSeries = c.String(maxLength: 1),
                        ReferredNumber = c.Int(nullable: false),
                        ReferredYear = c.Int(nullable: false),
                        Fir = c.String(maxLength: 6),
                        Code23 = c.String(maxLength: 2),
                        IcaoSubjectId = c.Int(nullable: false),
                        Code45 = c.String(maxLength: 2),
                        IcaoConditionId = c.Int(nullable: false),
                        Traffic = c.String(maxLength: 2),
                        Purpose = c.String(maxLength: 4),
                        Scope = c.String(maxLength: 4),
                        LowerLimit = c.Int(nullable: false),
                        UpperLimit = c.Int(nullable: false),
                        Coordinates = c.String(maxLength: 20),
                        Location = c.Geography(),
                        Radius = c.Int(nullable: false),
                        ItemA = c.String(maxLength: 96),
                        StartActivity = c.DateTime(),
                        EndValidity = c.DateTime(),
                        Estimated = c.Boolean(nullable: false),
                        ItemD = c.String(),
                        ItemE = c.String(),
                        ItemEFrench = c.String(),
                        ItemF = c.String(),
                        ItemG = c.String(),
                        ItemX = c.String(),
                        Operator = c.String(maxLength: 20),
                        Status = c.Int(nullable: false),
                        StatusTime = c.DateTime(),
                        RejectionReason = c.String(),
                        Delete = c.Boolean(nullable: false),
                        NoteToNof = c.String(),
                        ContainFreeText = c.Boolean(nullable: false),
                        ModifiedByNof = c.Boolean(nullable: false),
                        IsCatchAll = c.Boolean(nullable: false),
                        Permanent = c.Boolean(nullable: false),
                        Originator = c.String(maxLength: 50),
                        OriginatorEmail = c.String(),
                        OriginatorPhone = c.String(),
                        Tokens = c.String(),
                        Received = c.DateTime(nullable: false),
                        NotamId = c.String(maxLength: 15),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Grouped = c.Boolean(nullable: false),
                        IsGroupMaster = c.Boolean(nullable: false),
                        GroupId = c.Guid(),
                        Urgent = c.Boolean(nullable: false),
                        ModifiedByOrg = c.String(),
                        ModifiedByUsr = c.String(maxLength: 20),
                        SeriesChecklist = c.Boolean(nullable: false),
                        OrganizationType = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NsdCategory", t => t.CategoryId)
                .ForeignKey("dbo.IcaoSubject", t => t.IcaoSubjectId)
                .ForeignKey("dbo.IcaoSubjectCondition", t => t.IcaoConditionId)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.IcaoSubjectId)
                .Index(t => t.IcaoConditionId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        MessageTemplate = c.String(),
                        Level = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                        Exception = c.String(),
                        Properties = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MessageExchangeQueue",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MessageType = c.Byte(nullable: false),
                        ClientType = c.Int(nullable: false),
                        Status = c.Byte(nullable: false),
                        PriorityCode = c.String(),
                        ClientAddress = c.String(),
                        ClientName = c.String(),
                        Recipients = c.String(),
                        Body = c.String(),
                        Inbound = c.Boolean(nullable: false),
                        Read = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        Delivered = c.DateTime(),
                        LastOwner = c.String(),
                        Locked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MessageExchangeTemplate",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Body = c.String(),
                        Addresses = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NdsClientItemA",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NdsClientId = c.Int(nullable: false),
                        ItemA = c.String(nullable: false, maxLength: 5),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NdsClient", t => t.NdsClientId, cascadeDelete: true)
                .Index(t => t.NdsClientId);
            
            CreateTable(
                "dbo.NdsClient",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientType = c.Int(nullable: false),
                        Client = c.String(nullable: false),
                        Address = c.String(),
                        Operator = c.String(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        French = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        LastSynced = c.DateTime(),
                        AllowQueries = c.Boolean(nullable: false),
                        Region = c.Geography(),
                        LowerLimit = c.Int(nullable: false),
                        UpperLimit = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NdsClientSeries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NdsClientId = c.Int(nullable: false),
                        Series = c.String(nullable: false, maxLength: 1),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NdsClient", t => t.NdsClientId, cascadeDelete: true)
                .Index(t => t.NdsClientId);
            
            CreateTable(
                "dbo.NdsIncomingRequest",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MessageType = c.String(),
                        MessageId = c.String(nullable: false),
                        Metadata = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        RecievedDate = c.DateTime(nullable: false),
                        ProcessedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NdsNotamMessage",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        NotamId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.MessageId, t.NotamId })
                .ForeignKey("dbo.NdsOutgoingMessage", t => t.MessageId)
                .ForeignKey("dbo.Notam", t => t.NotamId)
                .Index(t => t.MessageId)
                .Index(t => t.NotamId);
            
            CreateTable(
                "dbo.NdsOutgoingMessage",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        GroupId = c.Guid(nullable: false),
                        GroupOrder = c.Int(nullable: false),
                        NextId = c.Guid(),
                        Destination = c.String(nullable: false),
                        Format = c.Byte(nullable: false),
                        Content = c.String(nullable: false),
                        Metadata = c.String(nullable: false),
                        Status = c.Byte(nullable: false),
                        ErrorMessage = c.String(),
                        SubmitDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Notam",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RootId = c.Guid(nullable: false),
                        ReplaceNotamId = c.Guid(),
                        Type = c.Int(nullable: false),
                        ProposalId = c.Int(nullable: false),
                        SequenceNumber = c.Int(nullable: false, identity: true),
                        Nof = c.String(maxLength: 6),
                        Series = c.String(maxLength: 1),
                        Number = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        NotamId = c.String(nullable: false, maxLength: 15),
                        NumberRolled = c.Boolean(nullable: false),
                        ReferredSeries = c.String(maxLength: 1),
                        ReferredNumber = c.Int(nullable: false),
                        ReferredYear = c.Int(nullable: false),
                        ReferredNotamId = c.String(),
                        Fir = c.String(maxLength: 6),
                        Code23 = c.String(maxLength: 2),
                        Code45 = c.String(maxLength: 2),
                        Traffic = c.String(maxLength: 2),
                        Purpose = c.String(maxLength: 4),
                        Scope = c.String(maxLength: 4),
                        LowerLimit = c.Int(nullable: false),
                        UpperLimit = c.Int(nullable: false),
                        Coordinates = c.String(maxLength: 20),
                        EffectArea = c.Geography(),
                        Radius = c.Int(nullable: false),
                        ItemA = c.String(maxLength: 96),
                        StartActivity = c.DateTime(),
                        EndValidity = c.DateTime(),
                        Estimated = c.Boolean(nullable: false),
                        EffectiveEndValidity = c.DateTime(),
                        ItemD = c.String(),
                        ItemE = c.String(),
                        ItemEFrench = c.String(),
                        ItemF = c.String(),
                        ItemG = c.String(),
                        ItemX = c.String(),
                        Operator = c.String(maxLength: 20),
                        Originator = c.String(maxLength: 50),
                        OriginatorEmail = c.String(),
                        OriginatorPhone = c.String(),
                        Permanent = c.Boolean(nullable: false),
                        Published = c.DateTime(nullable: false),
                        NoteToNof = c.String(),
                        IcaoText = c.String(),
                        Urgent = c.Boolean(nullable: false),
                        ModifiedByOrg = c.String(),
                        ModifiedByUsr = c.String(maxLength: 20),
                        SeriesChecklist = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notam", t => t.ReplaceNotamId)
                .Index(t => t.RootId, name: "IX_DISSEMINATED_ROOT_ID")
                .Index(t => t.ReplaceNotamId)
                .Index(t => t.NotamId, name: "IX_NOTAM_NUMBER_ID");
            
            CreateTable(
                "dbo.NofTemplateMessageRecord",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TemplateId = c.Guid(nullable: false),
                        Username = c.String(),
                        SlotNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MessageExchangeTemplate", t => t.TemplateId)
                .Index(t => t.TemplateId);
            
            CreateTable(
                "dbo.NotamTopic",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NotamId = c.Guid(nullable: false),
                        Topic = c.String(),
                        DateSubmitted = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notam", t => t.NotamId)
                .Index(t => t.NotamId);
            
            CreateTable(
                "dbo.NotificationTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotificationType = c.Int(nullable: false),
                        TemplateType = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        SubjectEng = c.String(),
                        SubjectFr = c.String(),
                        BodyEng = c.String(),
                        BodyFr = c.String(),
                        NotificationFrequency = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NotificationTracking",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotifiationType = c.Int(nullable: false),
                        TemplateType = c.Int(nullable: false),
                        SentDatetime = c.DateTime(),
                        PackageId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NotifySeries",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        LastNotified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OriginatorInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganizationId = c.Int(nullable: false),
                        Fullname = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organization", t => t.OrganizationId)
                .Index(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Permission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RolePermission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        PermissionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Permission", t => t.PermissionId)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.PermissionId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Description = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.PoisonMtoRecord",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NumTries = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PoisonMto",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        MessageType = c.Byte(nullable: false),
                        MessageId = c.Int(nullable: false),
                        Payload = c.Binary(),
                        ReporterType = c.Int(nullable: false),
                        Reporter = c.String(),
                        Content = c.String(),
                        Exception = c.String(),
                        ReportTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProposalAttachment",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        CreationDate = c.DateTime(nullable: false),
                        FileName = c.String(),
                        Attachment = c.Binary(),
                        ProposalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Proposal", t => t.ProposalId)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ProposalId);
            
            CreateTable(
                "dbo.ProposalHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 20),
                        CategoryId = c.Int(nullable: false),
                        Version = c.String(maxLength: 10),
                        OrganizationId = c.Int(nullable: false),
                        SubjectId = c.String(),
                        Nof = c.String(maxLength: 6),
                        Series = c.String(maxLength: 1),
                        Number = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        ProposalType = c.Int(nullable: false),
                        ReferredSeries = c.String(maxLength: 1),
                        ReferredNumber = c.Int(nullable: false),
                        ReferredYear = c.Int(nullable: false),
                        Fir = c.String(maxLength: 6),
                        Code23 = c.String(maxLength: 2),
                        IcaoSubjectId = c.Int(nullable: false),
                        Code45 = c.String(maxLength: 2),
                        IcaoConditionId = c.Int(nullable: false),
                        Traffic = c.String(maxLength: 2),
                        Purpose = c.String(maxLength: 4),
                        Scope = c.String(maxLength: 4),
                        LowerLimit = c.Int(nullable: false),
                        UpperLimit = c.Int(nullable: false),
                        Coordinates = c.String(maxLength: 20),
                        Location = c.Geography(),
                        Radius = c.Int(nullable: false),
                        ItemA = c.String(maxLength: 96),
                        StartActivity = c.DateTime(),
                        EndValidity = c.DateTime(),
                        Estimated = c.Boolean(nullable: false),
                        ItemD = c.String(),
                        ItemE = c.String(),
                        ItemEFrench = c.String(),
                        ItemF = c.String(),
                        ItemG = c.String(),
                        ItemX = c.String(),
                        Operator = c.String(maxLength: 20),
                        Status = c.Int(nullable: false),
                        StatusTime = c.DateTime(),
                        RejectionReason = c.String(),
                        Delete = c.Boolean(nullable: false),
                        NoteToNof = c.String(),
                        ContainFreeText = c.Boolean(nullable: false),
                        ModifiedByNof = c.Boolean(nullable: false),
                        IsCatchAll = c.Boolean(nullable: false),
                        Permanent = c.Boolean(nullable: false),
                        Originator = c.String(maxLength: 50),
                        OriginatorEmail = c.String(),
                        OriginatorPhone = c.String(),
                        Tokens = c.String(),
                        Received = c.DateTime(nullable: false),
                        NotamId = c.String(maxLength: 15),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Grouped = c.Boolean(nullable: false),
                        IsGroupMaster = c.Boolean(nullable: false),
                        GroupId = c.Guid(),
                        Urgent = c.Boolean(nullable: false),
                        ModifiedByOrg = c.String(),
                        ModifiedByUsr = c.String(maxLength: 20),
                        OrganizationType = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NsdCategory", t => t.CategoryId)
                .ForeignKey("dbo.IcaoSubject", t => t.IcaoSubjectId)
                .ForeignKey("dbo.IcaoSubjectCondition", t => t.IcaoConditionId)
                .ForeignKey("dbo.Proposal", t => t.ProposalId)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ProposalId)
                .Index(t => t.CategoryId)
                .Index(t => t.IcaoSubjectId)
                .Index(t => t.IcaoConditionId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ScheduleJob",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        LastTrigger = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ScheduleTask",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        IsProcessed = c.Boolean(nullable: false),
                        ProcessedDate = c.DateTime(),
                        RowVersion = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SdoCache",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 32),
                        ParentId = c.String(maxLength: 32),
                        Type = c.Byte(nullable: false),
                        Name = c.String(maxLength: 128),
                        Designator = c.String(maxLength: 16),
                        Fir = c.String(),
                        RefPoint = c.Geography(),
                        Geography = c.Geography(),
                        Attributes = c.String(),
                        Keywords = c.String(maxLength: 128),
                        Flags = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        EffectiveDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SdoCache", t => t.ParentId)
                .Index(t => t.ParentId)
                .Index(t => t.Name)
                .Index(t => t.Designator)
                .Index(t => t.Keywords);
            
            CreateTable(
                "dbo.SeriesNumber",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Series = c.String(maxLength: 1),
                        Year = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        RowVersion = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.Year, t.Series }, unique: true, name: "IX_SeriesYear");
            
            CreateTable(
                "dbo.SystemStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.Byte(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        LastError = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserQueryFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FilterName = c.String(nullable: false),
                        Username = c.String(nullable: false),
                        UserFilterType = c.Int(nullable: false),
                        SlotNumber = c.Int(nullable: false),
                        JsonBundle = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRegistration",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserProfileId = c.String(nullable: false, maxLength: 128),
                        VerificationCode = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserProfileId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.DisseminationJob",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientIds = c.String(nullable: false),
                        Delay = c.Time(nullable: false, precision: 7),
                        Status = c.Byte(nullable: false),
                        LastNotam = c.Int(nullable: false),
                        BreakDate = c.DateTime(),
                        LastUpdate = c.DateTime(),
                        DisseminateXml = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SeriesChecklist",
                c => new
                    {
                        Series = c.String(nullable: false, maxLength: 1),
                        Firs = c.String(maxLength: 32),
                        Coordinates = c.String(maxLength: 32),
                        Radius = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Series);

            //Add manually to set indexes and store procedure -- NEED TO BE COPIED TO ANY NEW MIGRATION FROM SCRATCH
            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Proposal') AND name='SIndx_Proposal_location1') BEGIN CREATE SPATIAL INDEX SIndx_Proposal_location1 ON dbo.Proposal(location) END");
            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Proposal') AND name='SIndx_Proposal_location2') BEGIN CREATE SPATIAL INDEX SIndx_Proposal_location2 ON dbo.Proposal(location) USING GEOGRAPHY_GRID WITH(GRIDS = (MEDIUM, LOW, MEDIUM, HIGH), CELLS_PER_OBJECT = 64, PAD_INDEX = ON) END");
            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Proposal') AND name='SIndx_Proposal_location3') BEGIN CREATE SPATIAL INDEX SIndx_Proposal_location3 ON dbo.Proposal(location) WITH(GRIDS = (LEVEL_3 = HIGH, LEVEL_2 = HIGH)) END");

            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Notam') AND name='SIndx_Notam_EffectArea1') BEGIN CREATE SPATIAL INDEX SIndx_Notam_EffectArea1 ON dbo.Notam(EffectArea) END");
            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Notam') AND name='SIndx_Notam_EffectArea2') BEGIN CREATE SPATIAL INDEX SIndx_Notam_EffectArea2 ON dbo.Notam(EffectArea) USING GEOGRAPHY_GRID WITH(GRIDS = (MEDIUM, LOW, MEDIUM, HIGH), CELLS_PER_OBJECT = 64, PAD_INDEX = ON) END");
            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Notam') AND name='SIndx_Notam_EffectArea3') BEGIN CREATE SPATIAL INDEX SIndx_Notam_EffectArea3 ON dbo.Notam(EffectArea) WITH(GRIDS = (LEVEL_3 = HIGH, LEVEL_2 = HIGH)) END");

            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.ProposalHistory') AND name='SIndx_ProposalHistory_location1') BEGIN CREATE SPATIAL INDEX SIndx_ProposalHistory_location1 ON dbo.ProposalHistory(location) END");
            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.ProposalHistory') AND name='SIndx_ProposalHistory_location2') BEGIN CREATE SPATIAL INDEX SIndx_ProposalHistory_location2 ON dbo.ProposalHistory(location) USING GEOGRAPHY_GRID WITH(GRIDS = (MEDIUM, LOW, MEDIUM, HIGH), CELLS_PER_OBJECT = 64, PAD_INDEX = ON) END");
            Sql("IF 0 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.ProposalHistory') AND name='SIndx_ProposalHistory_location3') BEGIN CREATE SPATIAL INDEX SIndx_ProposalHistory_location3 ON dbo.ProposalHistory(location) WITH(GRIDS = (LEVEL_3 = HIGH, LEVEL_2 = HIGH)) END");

            this.CreateStoredProcedure("dbo.stats_report", c => new
            {
                from = c.DateTime(),
                to = c.DateTime(),
                @period = c.Int()
            },
            body: @"
                IF @period = 0  -- yearly
		            BEGIN
			            SELECT
				            Series, 
				            DATEADD(YEAR, DATEDIFF(YEAR, 0, Published), 0) AS StartPeriod,
				            DATEADD(YEAR, DATEDIFF(YEAR, 0, Published) +1, -1 ) AS EndPeriod,
				            Published,
				            COUNT(*) AS SeriesCount 
			            FROM 
				            Notam
			            WHERE 
				            Published >= @from AND Published < @to 
			            GROUP BY 
				            DATEADD(YEAR, DATEDIFF(YEAR, 0, Published), 0),
				            DATEADD(YEAR, DATEDIFF(YEAR, 0, Published) +1, -1 ),
				            Published,
				            Series
			            ORDER BY
				            StartPeriod,
				            EndPeriod,
				            Series
		            END
	            ELSE IF @period = 1 -- monthly
		            BEGIN
			            SELECT
				            Series, 
				            DATEADD(MONTH, DATEDIFF(month, 0, Published), 0) AS StartPeriod,
				            DATEADD(MONTH,1,Published)- day(DATEADD(MONTH,1,Published)) AS EndPeriod,
				            Published,
				            COUNT(*) AS SeriesCount 
			            FROM 
				            Notam
			            WHERE 
				            Published >= @from AND Published < @to 
			            GROUP BY 
				            DATEADD(MONTH, DATEDIFF(month, 0, Published), 0),
				            DATEADD(MONTH,1,Published)- day(DATEADD(MONTH,1,Published)),
				            Published,
				            Series
			            ORDER BY
				            StartPeriod,
				            EndPeriod,
				            Series
		            END
                ELSE IF @period = 2 --weekly
		            BEGIN
			            SELECT
				            Series, 
				            DATEADD(day, DATEDIFF(DAY, 0, Published) /7*7, 0) AS StartPeriod,
				            DATEADD(day, DATEDIFF(DAY, 6, Published-1) /7*7 + 7, 6) AS EndPeriod,
				            Published,
				            COUNT(*) AS SeriesCount 
			            FROM 
				            Notam
			            WHERE 
				            Published >= @from AND Published < @to 
			            GROUP BY 
				            DATEADD(DAY, DATEDIFF(DAY, 0, Published) /7*7, 0),
				            DATEADD(DAY, DATEDIFF(DAY, 6, Published-1) /7*7 + 7, 6),
				            Published,
				            Series
			            ORDER BY
				            StartPeriod,
				            EndPeriod,
				            Series
		            END
                ELSE IF @period = 3 --hourly
		            BEGIN
			            SELECT
				            Series, 
				            CAST(Published as date) AS StartPeriod,
				            CAST(Published as date) AS EndPeriod,
				            DATEPART(hour,Published) AS HourPerid,
				            CAST(Published as date) AS Published,
				            COUNT(*) AS SeriesCount 
			            FROM 
				            Notam
			            WHERE 
				            CAST(Published AS DATE) = CAST(@from AS DATE)
			            GROUP BY 
							DATEPART(HOUR,Published),
							CAST(Published as date),
                            Series
			            ORDER BY
	                        DATEPART(hour,Published),
	                        Series
		            END
	            ELSE
		            BEGIN -- daily (4)
			            SELECT
				            Series, 
				            Published AS StartPeriod,
				            Published AS EndPeriod,
				            Published,
				            COUNT(*) AS SeriesCount 
			            FROM 
				            Notam
			            WHERE 
				            Published >= @from AND Published < @to 
			            GROUP BY 
				            Published,
				            Series
			            ORDER BY
				            StartPeriod,
				            EndPeriod,
				            Series
		           END");


    }

    public override void Down()
        {
            DropForeignKey("dbo.UserRegistration", "UserProfileId", "dbo.User");
            DropForeignKey("dbo.SdoCache", "ParentId", "dbo.SdoCache");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.ProposalHistory", "UserId", "dbo.User");
            DropForeignKey("dbo.ProposalHistory", "ProposalId", "dbo.Proposal");
            DropForeignKey("dbo.ProposalHistory", "IcaoConditionId", "dbo.IcaoSubjectCondition");
            DropForeignKey("dbo.ProposalHistory", "IcaoSubjectId", "dbo.IcaoSubject");
            DropForeignKey("dbo.ProposalHistory", "CategoryId", "dbo.NsdCategory");
            DropForeignKey("dbo.ProposalAttachment", "UserId", "dbo.User");
            DropForeignKey("dbo.ProposalAttachment", "ProposalId", "dbo.Proposal");
            DropForeignKey("dbo.RolePermission", "RoleId", "dbo.Role");
            DropForeignKey("dbo.RolePermission", "PermissionId", "dbo.Permission");
            DropForeignKey("dbo.OriginatorInfo", "OrganizationId", "dbo.Organization");
            DropForeignKey("dbo.NotamTopic", "NotamId", "dbo.Notam");
            DropForeignKey("dbo.NofTemplateMessageRecord", "TemplateId", "dbo.MessageExchangeTemplate");
            DropForeignKey("dbo.NdsNotamMessage", "NotamId", "dbo.Notam");
            DropForeignKey("dbo.Notam", "ReplaceNotamId", "dbo.Notam");
            DropForeignKey("dbo.NdsNotamMessage", "MessageId", "dbo.NdsOutgoingMessage");
            DropForeignKey("dbo.NdsClientSeries", "NdsClientId", "dbo.NdsClient");
            DropForeignKey("dbo.NdsClientItemA", "NdsClientId", "dbo.NdsClient");
            DropForeignKey("dbo.ItemD", "ProposalId", "dbo.Proposal");
            DropForeignKey("dbo.Proposal", "UserId", "dbo.User");
            DropForeignKey("dbo.Proposal", "IcaoConditionId", "dbo.IcaoSubjectCondition");
            DropForeignKey("dbo.Proposal", "IcaoSubjectId", "dbo.IcaoSubject");
            DropForeignKey("dbo.Proposal", "CategoryId", "dbo.NsdCategory");
            DropForeignKey("dbo.IcaoSubjectCondition", "SubjectId", "dbo.IcaoSubject");
            DropForeignKey("dbo.SeriesAllocation", "RegionId", "dbo.GeoRegion");
            DropForeignKey("dbo.UserDoa", "DoaId", "dbo.Doa");
            DropForeignKey("dbo.OrganizationDoa", "DoaId", "dbo.Doa");
            DropForeignKey("dbo.User", "UserPreferenceId", "dbo.UserPreference");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.User", "RegistrationId", "dbo.Registration");
            DropForeignKey("dbo.User", "OrganizationId", "dbo.Organization");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.User");
            DropForeignKey("dbo.User", "DigitalSlaId", "dbo.DigitalSla");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.User");
            DropForeignKey("dbo.OrganizationNsdCategory", "OrganizationId", "dbo.Organization");
            DropForeignKey("dbo.NsdCategory", "ParentCategoryId", "dbo.NsdCategory");
            DropForeignKey("dbo.OrganizationNsdCategory", "NsdCategoryId", "dbo.NsdCategory");
            DropForeignKey("dbo.OrganizationDoa", "OrganizationId", "dbo.Organization");
            DropIndex("dbo.UserRegistration", new[] { "UserProfileId" });
            DropIndex("dbo.SeriesNumber", "IX_SeriesYear");
            DropIndex("dbo.SdoCache", new[] { "Keywords" });
            DropIndex("dbo.SdoCache", new[] { "Designator" });
            DropIndex("dbo.SdoCache", new[] { "Name" });
            DropIndex("dbo.SdoCache", new[] { "ParentId" });
            DropIndex("dbo.ProposalHistory", new[] { "UserId" });
            DropIndex("dbo.ProposalHistory", new[] { "IcaoConditionId" });
            DropIndex("dbo.ProposalHistory", new[] { "IcaoSubjectId" });
            DropIndex("dbo.ProposalHistory", new[] { "CategoryId" });
            DropIndex("dbo.ProposalHistory", new[] { "ProposalId" });
            DropIndex("dbo.ProposalAttachment", new[] { "ProposalId" });
            DropIndex("dbo.ProposalAttachment", new[] { "UserId" });
            DropIndex("dbo.Role", "RoleNameIndex");
            DropIndex("dbo.RolePermission", new[] { "PermissionId" });
            DropIndex("dbo.RolePermission", new[] { "RoleId" });
            DropIndex("dbo.OriginatorInfo", new[] { "OrganizationId" });
            DropIndex("dbo.NotamTopic", new[] { "NotamId" });
            DropIndex("dbo.NofTemplateMessageRecord", new[] { "TemplateId" });
            DropIndex("dbo.Notam", "IX_NOTAM_NUMBER_ID");
            DropIndex("dbo.Notam", new[] { "ReplaceNotamId" });
            DropIndex("dbo.Notam", "IX_DISSEMINATED_ROOT_ID");
            DropIndex("dbo.NdsNotamMessage", new[] { "NotamId" });
            DropIndex("dbo.NdsNotamMessage", new[] { "MessageId" });
            DropIndex("dbo.NdsClientSeries", new[] { "NdsClientId" });
            DropIndex("dbo.NdsClientItemA", new[] { "NdsClientId" });
            DropIndex("dbo.Proposal", new[] { "UserId" });
            DropIndex("dbo.Proposal", new[] { "IcaoConditionId" });
            DropIndex("dbo.Proposal", new[] { "IcaoSubjectId" });
            DropIndex("dbo.Proposal", new[] { "CategoryId" });
            DropIndex("dbo.ItemD", new[] { "ProposalId" });
            DropIndex("dbo.IcaoSubjectCondition", new[] { "SubjectId" });
            DropIndex("dbo.SeriesAllocation", new[] { "RegionId" });
            DropIndex("dbo.UserDoa", new[] { "DoaId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.User", "UserNameIndex");
            DropIndex("dbo.User", new[] { "DigitalSlaId" });
            DropIndex("dbo.User", new[] { "RegistrationId" });
            DropIndex("dbo.User", new[] { "UserPreferenceId" });
            DropIndex("dbo.User", new[] { "OrganizationId" });
            DropIndex("dbo.NsdCategory", new[] { "ParentCategoryId" });
            DropIndex("dbo.OrganizationNsdCategory", new[] { "NsdCategoryId" });
            DropIndex("dbo.OrganizationNsdCategory", new[] { "OrganizationId" });
            DropIndex("dbo.OrganizationDoa", new[] { "OrganizationId" });
            DropIndex("dbo.OrganizationDoa", new[] { "DoaId" });
            DropTable("dbo.SeriesChecklist");
            DropTable("dbo.DisseminationJob");
            DropTable("dbo.UserRegistration");
            DropTable("dbo.UserQueryFilter");
            DropTable("dbo.SystemStatus");
            DropTable("dbo.SeriesNumber");
            DropTable("dbo.SdoCache");
            DropTable("dbo.ScheduleTask");
            DropTable("dbo.ScheduleJob");
            DropTable("dbo.ProposalHistory");
            DropTable("dbo.ProposalAttachment");
            DropTable("dbo.PoisonMto");
            DropTable("dbo.PoisonMtoRecord");
            DropTable("dbo.Role");
            DropTable("dbo.RolePermission");
            DropTable("dbo.Permission");
            DropTable("dbo.OriginatorInfo");
            DropTable("dbo.NotifySeries");
            DropTable("dbo.NotificationTracking");
            DropTable("dbo.NotificationTemplate");
            DropTable("dbo.NotamTopic");
            DropTable("dbo.NofTemplateMessageRecord");
            DropTable("dbo.Notam");
            DropTable("dbo.NdsOutgoingMessage");
            DropTable("dbo.NdsNotamMessage");
            DropTable("dbo.NdsIncomingRequest");
            DropTable("dbo.NdsClientSeries");
            DropTable("dbo.NdsClient");
            DropTable("dbo.NdsClientItemA");
            DropTable("dbo.MessageExchangeTemplate");
            DropTable("dbo.MessageExchangeQueue");
            DropTable("dbo.Log");
            DropTable("dbo.Proposal");
            DropTable("dbo.ItemD");
            DropTable("dbo.IcaoSubject");
            DropTable("dbo.IcaoSubjectCondition");
            DropTable("dbo.Group");
            DropTable("dbo.SeriesAllocation");
            DropTable("dbo.GeoRegion");
            DropTable("dbo.Feedback");
            DropTable("dbo.UserDoa");
            DropTable("dbo.UserPreference");
            DropTable("dbo.UserRole");
            DropTable("dbo.Registration");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.User");
            DropTable("dbo.NsdCategory");
            DropTable("dbo.OrganizationNsdCategory");
            DropTable("dbo.Organization");
            DropTable("dbo.OrganizationDoa");
            DropTable("dbo.Doa");
            DropTable("dbo.DigitalSla");
            DropTable("dbo.ConfigurationValue");
            DropTable("dbo.AeroRDSCacheStatus");
            DropTable("dbo.AeroRDSCache");
            DropTable("dbo.AerodromeDisseminationCategory");

            //Add manually to set indexes and store procedure -- NEED TO BE COPIED TO ANY NEW MIGRATION FROM SCRATCH
            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Proposal') AND name='SIndx_Proposal_location1') BEGIN DROP INDEX SIndx_Proposal_location1 ON dbo.Proposal END");
            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Proposal') AND name='SIndx_Proposal_location2') BEGIN DROP INDEX SIndx_Proposal_location2 ON dbo.Proposal END");
            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Proposal') AND name='SIndx_Proposal_location3') BEGIN DROP INDEX SIndx_Proposal_location3 ON dbo.Proposal END");

            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Notam') AND name='SIndx_Notam_EffectArea1') BEGIN DROP INDEX SIndx_Notam_EffectArea1 ON dbo.Notam END");
            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Notam') AND name='SIndx_Notam_EffectArea2') BEGIN DROP INDEX SIndx_Notam_EffectArea2 ON dbo.Notam END");
            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.Notam') AND name='SIndx_Notam_EffectArea3') BEGIN DROP INDEX SIndx_Notam_EffectArea3 ON dbo.Notam END");

            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.ProposalHistory') AND name='SIndx_ProposalHistory_location1') BEGIN DROP INDEX SIndx_ProposalHistory_location1 ON dbo.ProposalHistory END");
            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.ProposalHistory') AND name='SIndx_ProposalHistory_location2') BEGIN DROP INDEX SIndx_ProposalHistory_location2 ON dbo.ProposalHistory END");
            Sql("IF 1 = (SELECT COUNT(*) as index_count FROM sys.indexes WHERE object_id = OBJECT_ID('dbo.ProposalHistory') AND name='SIndx_ProposalHistory_location3') BEGIN DROP INDEX SIndx_ProposalHistory_location3 ON dbo.ProposalHistory END");

            this.DropStoredProcedure("dbo.stats_report");

        }
    }
}
