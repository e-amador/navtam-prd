namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProposalAcknowledgementTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProposalAcknowledgement",
                c => new
                    {
                        ProposalId = c.Int(nullable: false),
                        OrganizationId = c.Int(nullable: false),
                        Acknowledge = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProposalId, t.OrganizationId });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProposalAcknowledgement");
        }
    }
}
