namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using Common.Common;
    using Domain.Model.Entitities;
    using Domain.Model.Enums;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<AppDbContext>
    {
        public Configuration()
        {
            //TODO change this once we are closer to release
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;

            // load Sql Server Types for migrate
            LoadSqlServerTypeIfNeeded();
        }

        //This method acts like a rapper to the DbContext.SaveChanges logging 
        //to the console all the validations errors found during the seeding with 
        //all the nested messages populated in the inner exception.
        public static int SaveChangesWithErrors(AppDbContext context)
        {
            try
            {
                return context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                var innerException = ex.InnerException;
                while (innerException != null)
                {
                    sb.AppendFormat("- InnerException : {0}", innerException.Message);
                    innerException = innerException.InnerException;
                }
                throw new DbEntityValidationException("Entity Validation Failed - errors follow:\n" + sb, ex);
                    // Add the original exception as the innerException
            }
        }

        protected override void Seed(AppDbContext context)
        {
            ////Just for Debugging... 
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //    System.Diagnostics.Debugger.Launch();

            ExecuteBcp("SdoCache", "SdoCache.bcp", "SdoCache-bcp-in.fmt", context.Database.Connection.ConnectionString);

            EnableAndConfigureServiceBroker(context);

            SeedSystemStatus(context);

            SeedNsdCategories(context);
            SeedUsersAndRoles(context);
            SeedOrganization(context);
            SeedConfigurationValues(context);
            SeedNotificationTemplates(context);
            SeedSqlScripts(context);
          
            SaveChangesWithErrors(context);
            SeedDigitalSlas(context);
            SeedSeriesNumbers(context);
            SeedNdsClients(context);

            SeedAeroRDSCacheStatus(context);
        }

        private void ConfigureSpatialIndexes(AppDbContext context)
        {
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.ConfigureSpatialIndexes.sql");
        }

        private void EnableAndConfigureServiceBroker(AppDbContext context)
        {
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.EnableServiceBroker.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.ConfigureServiceBroker.sql");
        }


        private void EnsureRoleExist(RoleManager<ApplicationRole> manager, string roleName)
        {
            if (manager.FindByName(roleName) == null)
            {
                var role = new ApplicationRole
                {
                    Name = roleName
                };
                manager.Create(role);
            }
        }

        private void EnsureUserExists(AppDbContext seedUsersAndRoles, UserProfile user, string defaultPassword,
            IEnumerable<string> addToRoles)
        {
            if (!seedUsersAndRoles.Users.Any(u => u.UserName == user.UserName))
            {
                var store = new UserStore<UserProfile>(seedUsersAndRoles);
                var manager = new UserManager<UserProfile>(store);
                manager.Create(user, defaultPassword);
                foreach (var roleName in addToRoles)
                {
                    manager.AddToRole(user.Id, roleName);
                }
            }
        }

        private void EnsureCategoryExists(AppDbContext seedUsersAndRoles, NsdCategory category)
        {
            if (!seedUsersAndRoles.NsdCategories.Any(c => c.Name == category.Name))
            {
                seedUsersAndRoles.NsdCategories.Add(category);
            }
        }

        private void ExecuteSqlFromResource(AppDbContext context, string embededResurce, bool ensureTranscation = true)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var sqlScript = string.Empty;
            using (var stream = assembly.GetManifestResourceStream(embededResurce))
            {
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        sqlScript = reader.ReadToEnd();
                    }
                }
            }
            if (!string.IsNullOrEmpty(sqlScript))
            {
                if (ensureTranscation)
                {
                    context.Database.ExecuteSqlCommand(sqlScript);
                }
                else
                {
                    context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlScript);
                }
            }
        }

        private static int ExecuteBcp(string tableName, string file, string format, string connectionString )
        {
            try
            {
                var rootDir = GetDepoymentRootDirectory();

                System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionString);

                file = rootDir + @"\Migrations\Scripts\" + file;
                format = rootDir + @"\Migrations\Scripts\" + format;

                var bcpProcess = new System.Diagnostics.Process();

                bcpProcess.StartInfo = new System.Diagnostics.ProcessStartInfo();
                bcpProcess.StartInfo.FileName = rootDir + @"\lib\bcp.exe";
                bcpProcess.StartInfo.Arguments = $"{builder.InitialCatalog}.dbo.{tableName} IN \"{file}\" -S \"{builder.DataSource}\" -f \"{format}\"  -U \"{builder.UserID}\" -P \"{builder.Password}\""; 
                bcpProcess.StartInfo.CreateNoWindow = true;
                bcpProcess.StartInfo.UseShellExecute = false;
                bcpProcess.StartInfo.RedirectStandardOutput = true;

                bcpProcess.Start();
                var result = bcpProcess.StandardOutput.ReadToEnd();
                bcpProcess.WaitForExit();
                return bcpProcess.ExitCode;
            }
            catch (Exception e)
            {
                throw new DbEntityValidationException(e.Message);

            }
        }

        private static string GetDepoymentRootDirectory()
        {
            //NOTE: WebApp will be hosted inside folder NAVTAM.WEB (same level as EF folder)
            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            var directoryName = Path.GetDirectoryName(absolutePath).ToUpper();

            var pos = directoryName.IndexOf("\\BIN");
            if(directoryName.Length - pos > 5)  
            {
                //migrations are running locally in the default WebApp folder (NAVTAM.WEB)
                var path = Directory.GetParent(
                        Directory.GetParent(
                            Directory.GetParent(directoryName).ToString())
                        .ToString())
                    .ToString();
                return string.Format(@"{0}\NAVTAM.WEB\BIN", path);
            }

            //migrations are running in the remote WEB_DEPLOYMENT folder used by Auth Deployment (JENKINS)
            return directoryName;
        }

        private void SeedConfigurationValues(AppDbContext context)
        {
            context.ConfigurationValues.AddOrUpdate(c => c.Name, new ConfigurationValue
            {
                Name = "NOTAMAllowedSymbols",
                Description = "Allowed symbols in NOTAM text (Regular Expression)",
                Value = "^[0-9A-Z?:().,' =\\/+\"-]+$",
                Category = "NOTAM"
            }); //   Un-escaped value >>>           ^[0-9A-Z -?:().,'=\/+"]+$
            context.ConfigurationValues.AddOrUpdate(c => c.Name, new ConfigurationValue
            {
                Name = "NOTAMForbiddenWords",
                Description = "Forbidden words in NOTAM text, (| separated)",
                Value = "ZCZC|NNNN|TIL|TILL|UNTIL|UNTILL|FR:E",
                Category = "NOTAM"
            });
            context.ConfigurationValues.AddOrUpdate(c => c.Name, new ConfigurationValue
            {
                Name = "NOTAMESTExpireTime",
                Description = "Value in minutes to consider that a NOTAM will expire when EST is set to true",
                Value = "50",
                Category = "NOTAM"
            });

            //context.ConfigurationValues.AddOrUpdate(c => c.Name, new ConfigurationValue
            //{
            //    Name = "IncludedGlobalAhps",
            //    Description = "Global (not Canadian) Ahps to be loaded to NOTAMWiz DB(Comma seperated).",
            //    Value = "LFVP,LFVM",
            //    Category = "NOTAMWizWorker"
            //});

            context.ConfigurationValues.AddOrUpdate(c => c.Name, new ConfigurationValue
            {
                Name = "MinValidityPeriod",
                Description = "NOTAM minimum validity period (in minutes)",
                Value = "30",
                Category = "NOTAM"
            });

            context.ConfigurationValues.AddOrUpdate(c => c.Name, new ConfigurationValue
            {
                Name = "HubDissemination",
                Description = "Disseminate NOTAMs to the NavCanHub (on/off)",
                Value = "on",
                Category = "NOTAM"
            });
        }

        private void SeedDigitalSlas(AppDbContext context)
        {
            var sampleSla = "line 1 line 1 line 1 line 1 line 1 line 1 line 1" + Environment.NewLine;
            sampleSla += "line 2 line 2 line 2 line 2 line 2 line 2 line 2" + Environment.NewLine;
            sampleSla += "line 3 line 3 line 3 line 3 line 3 line 3 line 3" + Environment.NewLine;
            sampleSla += "line 4 line 4 line 4 line 4 line 4 line 4 line 4";
            var digitalSla = new DigitalSla
            {
                Version = 1.0F,
                Content = sampleSla
            };

            // because for some reason add or update always 
            context.DigitalSlas.AddOrUpdate(d => d.Version, digitalSla);
        }
        
        private void SeedNotificationTemplates(AppDbContext context)
        {
            var notifTemplates = new List<NotificationTemplate>
            {
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.SoonToExpire,
                    Name = "SoonToExpireTemplate",
                    Description = "SoonToExpireTemplate",
                    SubjectEng = "SoonToExpire: {@Organization.Name}",
                    SubjectFr = "SoonToExpire-FR: {@Organization.Name}",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.SubmittedNotamId} is about to expire, expiry date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.SubmittedNotamId} is about to expire, expiry date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    NotificationFrequency = 30
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.Rejected,
                    Name = "RejectedTemplate",
                    Description = "RejectedTemplate",
                    SubjectEng = "Rejected: {@Organization.Name}",
                    SubjectFr = "Rejected-FR: {@Organization.Name}",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.PackageId} just got rejected, expiry date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.PackageId} just got rejected, expiry date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    NotificationFrequency = 0
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.UserApplicationRejected,
                    Name = "UserApplicationRejectedTemplate",
                    Description = "UserApplicationRejectedTemplate",
                    SubjectEng = "Rejected User: {@UserName}",
                    SubjectFr = "Rejected User-FR: {@UserName}",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@FirstName},p><p>Your NotamWiz registration application just got rejected.</p>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@FirstName},<p><p>Your NotamWiz registration application just got rejected.</p>
                                <br/><p>Thank you</p></div></body></html>",
                    NotificationFrequency = 0
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.UserApplicationApproved,
                    Name = "UserApplicationApprovedTemplate",
                    Description = "UserApplicationApprovedTemplate",
                    SubjectEng = "Approved User: {@UserName}",
                    SubjectFr = "Approved User-FR: {@UserName}",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@FirstName},p><p>Your NotamWiz registration application just got approved.</p>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@FirstName},<p><p>Your NotamWiz registration application just got approved.</p>
                                <br/><p>Thank you</p></div></body></html>",
                    NotificationFrequency = 0
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.UserApplicationSubmitted,
                    Name = "UserApplicationSubmittedTemplate",
                    Description = "UserApplicationSubmittedTemplate",
                    SubjectEng = "New User Registration: {@UserName}",
                    SubjectFr = "New User Registration-FR: {@UserName}",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear CcsUser,p><p>New User Registration arrived.</p>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear CcsUser,p><p>New User Registration arrived.</p>
                                <br/><p>Thank you</p></div></body></html>",
                    NotificationFrequency = 0
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.Expired,
                    Name = "ExpiredTemplate",
                    Description = "ExpiredTemplate",
                    SubjectEng = "Expired: {@Organization.Name}",
                    SubjectFr = "Expired-FR: {@Organization.Name}",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.SubmittedNotamId} is expired, expiry date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.SubmittedNotamId} is expired, expiry date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    NotificationFrequency = 0
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.Ended,
                    Name = "EndedTemplate",
                    Description = "EndedTemplate",
                    SubjectEng = "Ended: {@Organization.Name}",
                    SubjectFr = "Ended-FR: {@Organization.Name}",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.PackageId} is Ended, end date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@Organization.Name},<p><p>Notam {@NOTAMProposal.PackageId} is Ended, end date time: {@NOTAMProposal.EndValidity}  .</p>
                                <br/><p>Thank you</p></div></body></html>",
                    NotificationFrequency = 0
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.UserApplicationEmailConfirmation,
                    Name = "EmailConfirmationTemplate",
                    Description = "EmailConfirmationTemplate",
                    SubjectEng = "Email Confirmation",
                    SubjectFr = "Email Confirmation-FR",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@UserName},<p><p>Please click: </p>
                                <a href={@Link}>""confirmation link""</a>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@UserName},<p><p>Please click-fr: </p>
                                <a href={@Link}>""confirmation link""</a>
                                <br/><p>Thank you-fr</p></div></body></html>",
                    NotificationFrequency = 0
                },
                new NotificationTemplate
                {
                    NotificationType = NotificationType.Email,
                    TemplateType = TemplateType.ResetPasswordRequest,
                    Name = "ResetPasswordConfirmationTemplate",
                    Description = "ResetPasswordConfirmationTemplate",
                    SubjectEng = "Reset Password Request Email Confirmation",
                    SubjectFr = "Reset Password Request Email Confirmation-FR",
                    BodyEng = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@UserName},<p><p>Please click: </p>
                                <a href={@Link}>""confirmation link""</a>
                                <br/><p>Thank you</p></div></body></html>",
                    BodyFr = @"<html><body style=""color:grey; font-size:15px;"">
                                <font face=""Helvetica, Arial, sans-serif"">
                                <div style=""position:absolute; height:100px; width:600px; background-color:0d1d36; padding:30px;""></div><br/> <br/>
                                <div style=""background-color: #ece8d4; width:600px; height:200px; padding:30px; margin-top:30px;"">
                                <p>Dear {@UserName},<p><p>Please click-fr: </p>
                                <a href={@Link}>""confirmation link""</a>
                                <br/><p>Thank you-fr</p></div></body></html>",
                    NotificationFrequency = 0
                }
            };
            foreach (var notifTemplate in notifTemplates)
            {
                context.NotificationTemplates.AddOrUpdate(t => t.TemplateType, notifTemplate);
            }
            SaveChangesWithErrors(context);
        }

        private void SeedOrganization(AppDbContext context)
        {
            var users =
                context.Users.Where(
                    a => a.UserName == CommonDefinitions.AdminUser || a.UserName == CommonDefinitions.CcsUser).ToList();
            /* NavCanada Organization */
            var navCanada = context.Organizations.FirstOrDefault(c => c.Name == CommonDefinitions.NavcanadaOrg) ??
                            new Organization
                            {
                                Name = CommonDefinitions.NavcanadaOrg,
                                Address = "280 Huntclub",
                                EmailAddress = "notamwiz@navcanada.ca",
                                Telephone = "6132487777",
                                TypeOfOperation = "navigation",
                                IsReadOnlyOrg = true,
                                // adding a list of admins?
                                Users = users,
                                EmailDistribution = "notamwiz@navcanada.ca"
                            };
            context.Organizations.AddOrUpdate(c => c.Name, navCanada);
            /* NavCanada Organization Registration*/
            var navCanadaReg = context.Registrations.FirstOrDefault(c => c.Name == CommonDefinitions.NavcanadaOrg) ??
                               new Registration
                               {
                                   Name = CommonDefinitions.NavcanadaOrg,
                                   Address = "280 Huntclub",
                                   EmailAddress = "admin@mavcanada.ca",
                                   Telephone = "6132487777",
                                   TypeOfOperation = "navigation",
                                   //Users = orgUSers,
                                   Status = (int) RegistrationStatus.Completed,
                                   StatusTime = DateTime.Now,
                                   RegisteredUserName = users[0].UserName
                               };
            context.Registrations.AddOrUpdate(c => c.Name, navCanadaReg);
            SaveChangesWithErrors(context);
        }

        private void SeedSqlScripts(AppDbContext context)
        {
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.Nof.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.IcaoSubjectAndConditions.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.GeoRegion.sql");
            ExecuteSqlFromResource(context,"NavCanada.Core.Data.EF.Migrations.Scripts.AerodromeDisseminationCategory.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.SeriesAllocation.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.EnableServiceBroker.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.SdoFIRCentrePointUpdate.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.BilingualRegion.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.NorthernRegion.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.SeriesChecklist.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.SetAdminAccount.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.ScheduledTasks.sql");
            ExecuteSqlFromResource(context, "NavCanada.Core.Data.EF.Migrations.Scripts.DeveloperRoleUser.sql");
        }

        private void SeedUsersAndRoles(AppDbContext context)
        {
            var manager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(context));
            EnsureRoleExist(manager, CommonDefinitions.AdminRole);
            EnsureRoleExist(manager, CommonDefinitions.CcsRole);
            EnsureRoleExist(manager, CommonDefinitions.OrgAdminRole);
            EnsureRoleExist(manager, CommonDefinitions.UserRole);
            EnsureRoleExist(manager, CommonDefinitions.NavUserRole);
            EnsureRoleExist(manager, CommonDefinitions.FicUserRole);
            EnsureRoleExist(manager, CommonDefinitions.NofUserRole);
            EnsureRoleExist(manager, CommonDefinitions.NofAdminRole);
            //EnsureRoleExist(manager, CommonDefinitions.DeveloperRole);

            EnsureUserExists(context, new UserProfile
            {
                UserName = CommonDefinitions.AdminUser,
                FirstName = CommonDefinitions.AdminUser,
                LastName = CommonDefinitions.AdminUser,
                Email = "admin@NavCanada.ca",
                IsApproved = true,
                EmailConfirmed = true,
                Address = "280 Huntclub",
                Position = "System Administrator",
                LastPasswordChangeDate = DateTime.UtcNow,
            }, CommonDefinitions.AdminDefaultPassword, new[]
            {
                CommonDefinitions.AdminRole
            });
            //EnsureUserExists(context, new UserProfile
            //{
            //    UserName = "Developer",
            //    FirstName = "Dev",
            //    LastName = "Dev",
            //    Email = "admin@NavCanada.ca",
            //    IsApproved = true,
            //    EmailConfirmed = true,
            //    Address = "280 Huntclub",
            //    Position = "developer",
            //    LastPasswordChangeDate = DateTime.UtcNow,
            //}, CommonDefinitions.DeveloperPassword, new[]
            //{
            //    CommonDefinitions.DeveloperRole
            //});
            EnsureUserExists(context, new UserProfile
            {
                UserName = CommonDefinitions.CcsRole,
                FirstName = CommonDefinitions.CcsUser,
                LastName = CommonDefinitions.CcsUser,
                Email = "ccs@NavCanada.ca",
                IsApproved = true,
                EmailConfirmed = true,
                Address = "280 Huntclub",
                Position = "CCS",
                LastPasswordChangeDate = DateTime.UtcNow,
            }, CommonDefinitions.AdminDefaultPassword, new[]
            {
                CommonDefinitions.CcsRole
            });
            EnsureUserExists(context, new UserProfile
            {
                UserName = CommonDefinitions.BackendUser,
                FirstName = CommonDefinitions.BackendUser,
                LastName = CommonDefinitions.BackendUser,
                Email = "backend@NavCanada.ca",
                IsApproved = true,
                EmailConfirmed = true,
                Address = "280 Huntclub",
                Position = "Backend internal User",
                LastPasswordChangeDate = DateTime.UtcNow,
            }, CommonDefinitions.AdminDefaultPassword, new[]
            {
                CommonDefinitions.AdminRole
            });

            SaveChangesWithErrors(context);
        }

        const int FirstSeedYear = 2017;
        const int LastSeedYear = 2117;

        private void SeedSeriesNumbers(AppDbContext context)
        {
            var seriesNumbers = context.SeriesNumbers;

            if (seriesNumbers.Any(s => s.Year == FirstSeedYear))
                return;

            for (int year = FirstSeedYear; year <= LastSeedYear; year++)
            {
                for (var series = 'A'; series <= 'Z'; series++)
                {
                    seriesNumbers.Add(new SeriesNumber()
                    {
                        Series = series.ToString(),
                        Year = year,
                        Number = 0
                    });
                }
            }

            SaveChangesWithErrors(context);
        }

        private void SeedSystemStatus(AppDbContext context)
        {
            if (!context.SystemStatus.Any())
            {
                var status = new SystemStatus
                {
                    Status = SystemStatusEnum.Ok,
                    LastUpdated = DateTime.UtcNow
                };
                context.SystemStatus.Add(status);
            }
        }

        private void SeedAeroRDSCacheStatus(AppDbContext context)
        {
            if (!context.AeroRDSCacheStatus.Any())
            {
                var status = new AeroRDSCacheStatus();
                context.AeroRDSCacheStatus.Add(status);
            }
        }

        private void SeedNsdCategories(AppDbContext context)
        {
            EnsureCategoryExists(context, new NsdCategory() { Id = 1, ParentCategoryId = null, Name = "ALL" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 2, ParentCategoryId = 1, Name = "CATCHALL" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 3, ParentCategoryId = 1, Name = "OBSTACLES" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 4, ParentCategoryId = 3, Name = "OBST" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 5, ParentCategoryId = 3, Name = "OBST LGT U/S" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 6, ParentCategoryId = 1, Name = "RUNWAYS" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 7, ParentCategoryId = 6, Name = "RWY CLSD" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 10, ParentCategoryId = 1, Name = "TAXIWAYS" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 11, ParentCategoryId = 10, Name = "TWY CLSD" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 12, ParentCategoryId = 1, Name = "CARS" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 13, ParentCategoryId = 12, Name = "CARS" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 14, ParentCategoryId = 3, Name = "MULT OBST" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 15, ParentCategoryId = 3, Name = "MULT OBST LGT U/S" });
            EnsureCategoryExists(context, new NsdCategory() { Id = 16, ParentCategoryId = 1, Name = "CHECKLIST" });
        }

        private void SeedNdsClients(AppDbContext context)
        {
            var nofClient = new NdsClient
            {
                Client = "Nof",
                ClientType = NdsClientType.Aftn,
                Updated = DateTime.UtcNow,
                Address = "CYHQYNYX",
                Operator = "Nof",
            };
            context.NdsClients.AddOrUpdate(e => e.Client, nofClient);
        }       

        private static void LoadSqlServerTypeIfNeeded()
        {
            var processName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            if (processName.IndexOf("migrate.exe", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                //SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
            }
        }
    }
}