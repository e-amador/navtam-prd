﻿namespace NavCanada.Core.Data.NotamWiz.EF
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Threading.Tasks;
    using Contracts;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Globalization;
    using NavCanada.Core.Domain.Model.Entitities;

    public class Repository<T> : IRepository<T> where T : class
    {
        public Repository(DbContext dbContext)
        {
            if (dbContext == null)
                throw new NullReferenceException("DbContext can't be null reference at this point");
            DbContext = dbContext;

            DbSet = DbContext.Set<T>();
        }

        protected DbContext DbContext { get; set; }
        protected AppDbContext AppDbContext => DbContext as AppDbContext;

        protected IDbSet<T> DbSet
        {
            get;
            set;
        }

        public virtual IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public Task<List<T>> GetAllAsync()
        {
            return DbSet.ToListAsync();
        }

        public T GetById(object id)
        {
            return DbSet.Find(id);
        }

        public void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            if (entity is IRowVersion)
            {
                dbEntityEntry.OriginalValues["RowVersion"] = (entity as IRowVersion).RowVersion;
            }

            dbEntityEntry.State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null)
                return; // not found; assume already deleted.

            Delete(entity);
        }

        public void Detach(T entity)
        {
            var dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Detached;
            }
        }

        public Expression<Func<T, bool>> BuildWhereExpression(string parameterName, object value, string oper)
        {
            var parmExpression = Expression.Parameter(typeof(T));
            var memberExpression = Expression.Property(parmExpression, parameterName);
            var propInfo = (PropertyInfo)memberExpression.Member;
            var propType = propInfo.PropertyType;

            if (propType == typeof(DateTime) || propType == typeof(DateTime?))
            {
                return FilterDateTimeExpression(parmExpression, memberExpression, propType, value, oper);
            }

            return value != null && value.ToString().Contains("|") 
                ? FilterStringOrExpression(parmExpression, memberExpression, value, oper)
                : FilterStringExpression(parmExpression, memberExpression, value, oper);
        }

        private Expression<Func<T, bool>> FilterDateTimeExpression(ParameterExpression parm, MemberExpression member, Type type, object value, string oper)
        {
            DateTime? date = null;
            if (value != null)
            {
                DateTime tmpDate;
                var dateFormat = "yyMMddHHmm";
                if (DateTime.TryParseExact(value.ToString(), dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out tmpDate))
                {
                    date = tmpDate;
                }
                else
                    throw new InvalidOperationException("");
            }

            Expression expression = null;
            switch (oper)
            {
                case "EQ":
                    expression = Expression.Equal(member, Expression.Constant(date, type));
                    break;

                case "NEQ":
                    expression = Expression.NotEqual(member, Expression.Constant(date, type));
                    break;

                case "LT":
                    expression = Expression.LessThan(member, Expression.Constant(date, type));
                    break;

                case "GT":
                    expression = Expression.GreaterThan(member, Expression.Constant(date, type));
                    break;

                case "LTE":
                    expression = Expression.LessThanOrEqual(member, Expression.Constant(date, type));
                    break;

                case "GTE":
                    expression = Expression.GreaterThanOrEqual(member, Expression.Constant(date, type));
                    break;
            }
            if (expression == null)
                throw new NullReferenceException("Invalid Operator in Dynamic EF Expression");

            return Expression.Lambda<Func<T, bool>>(expression, parm);
        }

        private Expression<Func<T, bool>> FilterStringExpression(ParameterExpression parm, MemberExpression member, object value, string oper)
        {
            Expression expression = null;
            switch (oper)
            {
                case "EQ":
                    expression = Expression.Equal(member, GetConstantExpression(member, value) );
                    break;
                case "NEQ":
                    expression = Expression.NotEqual(member, GetConstantExpression(member, value));
                    break;

                case "LT":
                    expression = Expression.LessThan(member, GetConstantExpression(member, value));
                    break;

                case "SW":
                    expression = Expression.Call(member, startsWithMethod, GetConstantExpression(member, value));
                    break;

                case "EW":
                    expression = Expression.Call(member, endsWithMethod, GetConstantExpression(member, value));
                    break;

                case "CT":
                    expression = Expression.Call(member, containsMethod, GetConstantExpression(member, value));
                    break;

                case "GT":
                    expression = Expression.GreaterThan(member, GetConstantExpression(member, value));
                    break;

                case "LTE":
                    expression = Expression.LessThanOrEqual(member, GetConstantExpression(member, value));
                    break;

                case "GTE":
                    expression = Expression.GreaterThanOrEqual(member, GetConstantExpression(member, value));
                    break;
            }

            if (expression == null)
                throw new NullReferenceException("Invalid Operator in Dynamic EF Expression");

            return Expression.Lambda<Func<T, bool>>(expression, parm);
        }


        private Expression<Func<T, bool>> FilterStringOrExpression(ParameterExpression parm, MemberExpression member, object value, string oper)
        {
            Expression expression = null;
            Expression left = null;
            Expression right = null;

            object[] values = value.ToString().Split('|');

            for (var iter = 0; iter < values.Length; iter++)
            {
                if (iter > 0)
                {
                    switch (oper)
                    {
                        case "EQ":
                            left = expression != null ? expression : Expression.Equal(member, GetConstantExpression(member, values[iter - 1]));
                            right = Expression.Equal(member, GetConstantExpression(member, values[iter]));
                            expression = Expression.Or(left, right);
                            break;
                        case "NEQ":
                            left = expression != null ? expression : Expression.NotEqual(member, GetConstantExpression(member, values[iter - 1]));
                            right = Expression.NotEqual(member, GetConstantExpression(member, values[iter]));
                            expression = Expression.Or(left, right);
                            break;
                        case "LT":
                            left = expression != null ? expression : Expression.LessThan(member, GetConstantExpression(member, values[iter - 1]));
                            right = Expression.LessThan(member, Expression.Constant(values[iter]));
                            expression = Expression.Or(left, right);
                            break;
                        case "GT":
                            left = expression != null ? expression : Expression.GreaterThan(member, GetConstantExpression(member, values[iter - 1]));
                            right = Expression.GreaterThan(member, GetConstantExpression(member, values[iter]));
                            expression = Expression.Or(left, right);
                            break;
                        case "LTE":
                            left = expression != null ? expression : Expression.LessThanOrEqual(member, GetConstantExpression(member, values[iter - 1]));
                            right = Expression.LessThanOrEqual(member, GetConstantExpression(member, values[iter]));
                            expression = Expression.Or(left, right);
                            break;
                        case "GTE":
                            left = expression != null ? expression : Expression.GreaterThanOrEqual(member, GetConstantExpression(member, values[iter - 1]));
                            right = Expression.GreaterThanOrEqual(member, GetConstantExpression(member, values[iter]));
                            expression = Expression.Or(left, right);
                            break;
                        case "SW":
                            expression = Expression.Call(member, startsWithMethod, GetConstantExpression(member, value));
                            break;
                        case "EW":
                            expression = Expression.Call(member, endsWithMethod, GetConstantExpression(member, value));
                            break;
                        case "CT":
                            expression = Expression.Call(member, containsMethod, GetConstantExpression(member, value));
                            break;
                    }
                }
            }



            if (expression == null)
                throw new NullReferenceException("Invalid Operator in Dynamic EF Expression");

            return Expression.Lambda<Func<T, bool>>(expression, parm);
        }

        private ConstantExpression GetConstantExpression(MemberExpression member, object value)
        {
            return member.Type.IsEnum ? Expression.Constant(Enum.ToObject(member.Type, value)) : Expression.Constant(value);
        }


        private static MethodInfo containsMethod = typeof(string).GetMethod("Contains");
        private static MethodInfo startsWithMethod = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
        private static MethodInfo endsWithMethod = typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) });
    }
}