﻿using Business.Common;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.EF.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public static class DateRangeExt 
    {
        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public static IEnumerable<DateTime> EachMonth(DateTime from, DateTime thru)
        {
            for (var month = from.Date; month.Date <= thru.Date || month.Month == thru.Month; month = month.AddMonths(1))
                yield return month;
        }

        public static IEnumerable<DateTime> EachYear(DateTime from, DateTime thru)
        {
            for (var year = from.Date; year.Date <= thru.Date || year.Year == thru.Year; year = year.AddYears(1))
                yield return year;
        }

        public static IEnumerable<DateTime> EachWeek(DateTime from, DateTime thru)
        {
            for (var week = from.Date.StartOfWeek(DayOfWeek.Monday); week.Date <= thru.Date || week.Month == thru.Month; week = week.AddDays(7))
                yield return week;
        }


        public static IEnumerable<DateTime> EachDayTo(this DateTime dateFrom, DateTime dateTo)
        {
            return EachDay(dateFrom, dateTo);
        }

        public static IEnumerable<DateTime> EachWeekTo(this DateTime dateFrom, DateTime dateTo)
        {
            return EachWeek(dateFrom, dateTo);
        }

        public static IEnumerable<DateTime> EachMonthTo(this DateTime dateFrom, DateTime dateTo)
        {
            return EachMonth(dateFrom, dateTo);
        }

        public static IEnumerable<DateTime> EachYearTo(this DateTime dateFrom, DateTime dateTo)
        {
            return EachYear(dateFrom, dateTo);
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
    }

    public class NotamRepo : Repository<Notam>, INotamRepo
    {
        public NotamRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<Notam> GetByIdAsync(Guid id)
        {
            return DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public Notam GetReferredNotam(string series, int number, int year)
        {
            var notamId = ICAOTextUtils.FormatNotamId(series, number, year);
            return DbSet.FirstOrDefault(n => n.NotamId == notamId && !n.NumberRolled);
        }

        public Task<Notam> GetReferredNotamAsync(string series, int number, int year)
        {
            var notamId = ICAOTextUtils.FormatNotamId(series, number, year);
            return DbSet.FirstOrDefaultAsync(n => n.NotamId == notamId && !n.NumberRolled);
        }

        public async Task<List<NotamDto>> GetAllAsync(QueryOptions queryOptions)
        {
            var notams = await DbSet
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateNotamDto(notams);
        }

        public Task<List<Notam>> ReportAsync(DateTime start, DateTime end, QueryOptions queryOptions)
        {
            if (queryOptions.FilterByActiveInactiveOrAll == 0) //All
            {
                return DbSet
                    .Where(e => e.Published >= start && e.Published <= end)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else if (queryOptions.FilterByActiveInactiveOrAll == 1) //active
            {
                return DbSet
                    .Where(e => (e.Published >= start && e.Published <= end) && e.Type != NotamType.C && !(
                            e.EffectiveEndValidity != null || (!e.Estimated && e.EndValidity != null && e.EndValidity <= end))
                        )
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else // inactive
            {
                return DbSet
                    .Where(e => (e.Published >= start && e.Published <= end) && (
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= end))
                        )
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
        }

        public Task<List<Notam>> ReportActiveAsync(DateTime start, DateTime end, QueryOptions queryOptions)
        {
            return DbSet
                .Distinct()
                .Where(e =>
                    e.Type != NotamType.C &&
                    (
                        (e.EndValidity == null && e.EffectiveEndValidity == null && e.StartActivity <= end ) ||  //permanents starting before period
                        (e.EndValidity != null && e.EffectiveEndValidity == null && e.StartActivity <= end && e.EndValidity >= start ) ||  //active between period that has not been replaced
                        (e.EffectiveEndValidity != null && e.StartActivity <= end && e.EffectiveEndValidity >= start) // active between period that has been replaced
                    ))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();
        }

        public Task<List<Notam>> ReportCsvAsync(DateTime start, DateTime end, string filterBy)
        {
            if (filterBy == "All")
            {
                return DbSet.Where(e => e.Published >= start && e.Published <= end).ToListAsync();
            }
            else if (filterBy == "Active")
            {
                return DbSet.Where(e =>
                        (e.Published >= start && e.Published <= end) && !(
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= end))
                        )
                    .ToListAsync();
            }
            else // inactive
            {
                return DbSet.Where(e =>
                        (e.Published >= start && e.Published <= end) && (
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= end))
                        )
                    .ToListAsync();
            }
        }

        public Task<List<Notam>> ReportActiveCsvAsync(DateTime start, DateTime end)
        {
            return DbSet
                .Distinct()
                .Where(e =>
                    e.Type != NotamType.C &&
                    (
                        (e.EndValidity == null && e.EffectiveEndValidity == null && e.StartActivity <= end) ||  //permanents starting before period
                        (e.EndValidity != null && e.EffectiveEndValidity == null && e.StartActivity <= end && e.EndValidity >= start) ||  //active between period that has not been replaced
                        (e.EffectiveEndValidity != null && e.StartActivity <= end && e.EffectiveEndValidity >= start) // active between period that has been replaced
                    ))
                .ToListAsync();
        }

        public Task<int> ReportCountAsync(DateTime start, DateTime end, QueryOptions queryOptions)
        {
            if (queryOptions.FilterByActiveInactiveOrAll == 0) //All
            {
                return DbSet.CountAsync(e => e.Published >= start && e.Published <= end);
            }
            else if (queryOptions.FilterByActiveInactiveOrAll == 1) //active
            {
                return DbSet
                    .CountAsync(e => (e.Published >= start && e.Published <= end) && e.Type != NotamType.C && !(
                            e.EffectiveEndValidity != null || (!e.Estimated && e.EndValidity != null && e.EndValidity <= end))
                        );
            }
            else // inactive
            {
                return DbSet
                    .CountAsync(e =>
                        (e.Published >= start && e.Published <= end) && (
                        e.Type == NotamType.C ||
                        e.EffectiveEndValidity != null ||
                        (!e.Estimated && e.EndValidity != null && e.EndValidity <= end)));
            }
        }

        public Task<int> ReportActiveCountAsync(DateTime start, DateTime end, QueryOptions queryOptions)
        {
            return DbSet
                .Distinct()
                .CountAsync(e =>
                    e.Type != NotamType.C &&
                    (
                        (e.EndValidity == null && e.EffectiveEndValidity == null && e.StartActivity <= end) ||  //permanents starting before period
                        (e.EndValidity != null && e.EffectiveEndValidity == null && e.StartActivity <= end && e.EndValidity >= start) ||  //active between period that has not been replaced
                        (e.EffectiveEndValidity != null && e.StartActivity <= end && e.EffectiveEndValidity >= start) // active between period that has been replaced
                    ));
        }

        public async Task<List<NotamStatsDto>> StatsAsync(DateTime start, DateTime end, AggregatePeriod aggregatePeriod)
        {
            var ctx = DbContext as AppDbContext;

            object[] parameters = { start, end, (int)aggregatePeriod };
            var result = await ctx.Database.SqlQuery<StatsPartialDto>("stats_report @p0, @p1, @p2;", parameters).ToListAsync();

            return GenerateStatsReport(result, start, end, aggregatePeriod);
        }

        public async Task<List<NotamDto>> GetTrackingListByRootIdAsync(Guid rootId)
        {
            var trackingList = await DbSet
                .Where(e => e.RootId == rootId)
                .OrderByDescending(e => e.Published)
                .ToListAsync();

            return GenerateNotamDto(trackingList);
        }

        public async Task<List<NotamDto>> GetTrackingListByProposalIdAsync(int proposalId)
        {
            var trackingList = await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderByDescending(e => e.Number)
                .ToListAsync();

            return GenerateNotamDto(trackingList);
        }

        public Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            return DbSet.CountAsync();
        }

        public IQueryable<Notam> PrepareQueryFilter(FilterCriteria filterCriteria)
        {
            var expressions = new List<Expression<Func<ProposalHistory, bool>>>();
            IQueryable<Notam> query = DbSet;

            foreach (var cond in filterCriteria.Conditions)
            {
                var endValidityFilter = false;
                var field = cond.Field.ToUpper();
                var value = cond.Value.ToString().ToUpper();
                var condition = cond.Value.ToString().ToUpper();
                var oper = cond.Operator;
                switch (field)
                {
                    case "TYPE":
                        if (value != "N" || value != "R" || value != "C")
                        {
                            value = "N";
                        }
                        cond.Value = Enum.Parse(typeof(NotamType), condition);
                        break;
                    case "LOWERLIMIT":
                    case "UPPERLIMIT":
                    case "RADIUS":
                        int number;
                        if( int.TryParse(value, out number) )
                        {
                            cond.Value = number;
                        }
                        else
                        {
                            cond.Value = 0;
                        }
                        break;
                    case "PUBLISHED":
                        DateTime published;
                        if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out published))
                        {
                            cond.Value = value;
                        }
                        else
                        {
                            cond.Value = DateTime.Now;
                        }
                        break;
                    case "STARTACTIVITY":
                        if (condition == "IMMEDIATE" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime startActivity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out startActivity) )
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                    case "ENDVALIDITY":
                        endValidityFilter = true;
                        if ( condition == "PERM" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime endValidity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out endValidity))
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                }

                var expression = BuildWhereExpression(cond.Field, cond.Value, cond.Operator);
                if (endValidityFilter) //&& (oper == "EQ" || oper == "GT" || oper == "GTE")
                {
                    var left = BuildWhereExpression("Permanent", true, "EQ");
                    var right = BuildWhereExpression("EffectiveEndValidity", null, "EQ");
                    var lamda = left.And(right);

                    expression = expression.Or(lamda);
                }

                query = query.Where(expression);
            }

            return query;
        }

        public async Task<List<NotamDto>> FilterAsync(FilterCriteria filterCriteria, IQueryable<Notam> filterQuery)
        {
            var notams = await filterQuery
                .ApplySort(filterCriteria.QueryOptions.Sort)
                .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                .Take(filterCriteria.QueryOptions.PageSize)
                .ToListAsync();

            return GenerateNotamDto(notams);
        }

        public async Task<List<Notam>> ReportFilterAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery)
        {
            if (filterCriteria.filterBy == "All")
            {
                filterQuery = filterQuery.Where(e => e.Published >= filterCriteria.Start && e.StartActivity <= filterCriteria.End);
            }
            else if (filterCriteria.filterBy == "Active")
            {
                filterQuery = filterQuery.Where(e =>
                        (e.Published >= filterCriteria.Start && e.StartActivity <= filterCriteria.End) && !(
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= filterCriteria.End))
                        );
            }
            else // inactive
            {
                filterQuery = filterQuery.Where(e =>
                        (e.Published >= filterCriteria.Start && e.StartActivity <= filterCriteria.End) && (
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= filterCriteria.End))
                        );
            }

            return await filterQuery
                .ApplySort(filterCriteria.QueryOptions.Sort)
                .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                .Take(filterCriteria.QueryOptions.PageSize)
                .ToListAsync();
        }

        public async Task<List<Notam>> ReportActiveFilterAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery)
        {
            filterQuery = filterQuery
                .Distinct()
                .Where(e =>
                    e.Type != NotamType.C &&
                    (
                        (e.EndValidity == null && e.EffectiveEndValidity == null && e.StartActivity <= filterCriteria.End) ||  //permanents starting before period
                        (e.EndValidity != null && e.EffectiveEndValidity == null && e.StartActivity <= filterCriteria.End && e.EndValidity >= filterCriteria.Start) ||  //active between period that has not been replaced
                        (e.EffectiveEndValidity != null && e.StartActivity <= filterCriteria.End && e.EffectiveEndValidity >= filterCriteria.Start) // active between period that has been replaced
                    ));

            return await filterQuery
                .ApplySort(filterCriteria.QueryOptions.Sort)
                .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                .Take(filterCriteria.QueryOptions.PageSize)
                .ToListAsync();
        }

        public Task<List<Notam>> ReportFilterCsvAsync(DateTime start, DateTime end, string filterBy, IQueryable<Notam> filterQuery)
        {
            if (filterBy == "All")
            {
                filterQuery = filterQuery.Where(e => e.Published >= start && e.StartActivity <= end);
            }
            else if (filterBy == "Active")
            {
                filterQuery = filterQuery.Where(e =>
                        (e.Published >= start && e.StartActivity <= end) && !(
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= end))
                        );
            }
            else // inactive
            {
                filterQuery = filterQuery.Where(e =>
                        (e.Published >= start && e.StartActivity <= end) && (
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= end))
                        );
            }

            return filterQuery.ToListAsync();
        }

        public async Task<List<Notam>> ReportActiveFilterCsvAsync(DateTime start, DateTime end, IQueryable<Notam> filterQuery)
        {
            filterQuery = filterQuery
                .Distinct()
                .Where(e =>
                    e.Type != NotamType.C &&
                    (
                        (e.EndValidity == null && e.EffectiveEndValidity == null && e.StartActivity <= end) ||  //permanents starting before period
                        (e.EndValidity != null && e.EffectiveEndValidity == null && e.StartActivity <= end && e.EndValidity >= start) ||  //active between period that has not been replaced
                        (e.EffectiveEndValidity != null && e.StartActivity <= end && e.EffectiveEndValidity >= start) // active between period that has been replaced
                    ));

            return await filterQuery.ToListAsync();
        }

        public Task<int> FilterCountAsync(FilterCriteria filterCriteria, IQueryable<Notam> filterQuery)
        {
            return filterQuery.CountAsync();
        }

        public Task<int> ReportFilterCountAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery)
        {
            if (filterCriteria.filterBy == "All") 
            {
                filterQuery = filterQuery.Where(e=> e.Published >= filterCriteria.Start && e.StartActivity <= filterCriteria.End);
            }
            else if (filterCriteria.filterBy == "Active") 
            {
                filterQuery = filterQuery.Where(e => 
                        (e.Published >= filterCriteria.Start && e.StartActivity <= filterCriteria.End) && !(
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= filterCriteria.End))
                        );
            }
            else //"InActive"
            {
                filterQuery = filterQuery.Where(e =>
                        (e.Published >= filterCriteria.Start && e.StartActivity <= filterCriteria.End) && (
                            e.Type == NotamType.C ||
                            e.EffectiveEndValidity != null ||
                            (!e.Estimated && e.EndValidity != null && e.EndValidity <= filterCriteria.End))
                        );
            }

            return filterQuery.CountAsync();
        }

        public Task<int> ReportActiveFilterCountAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery)
        {
            filterQuery = filterQuery
                .Distinct()
                .Where(e =>
                    e.Type != NotamType.C &&
                    (
                        (e.EndValidity == null && e.EffectiveEndValidity == null && e.StartActivity <= filterCriteria.End) ||  //permanents starting before period
                        (e.EndValidity != null && e.EffectiveEndValidity == null && e.StartActivity <= filterCriteria.End && e.EndValidity >= filterCriteria.Start) ||  //active between period that has not been replaced
                        (e.EffectiveEndValidity != null && e.StartActivity <= filterCriteria.End && e.EffectiveEndValidity >= filterCriteria.Start) // active between period that has been replaced
                    ));
            return filterQuery.CountAsync();
        }

        public Task<List<Notam>> GetByLocationAsync(string location)
        {
            var effectedArea = GetEffectArea(location);
            var nowUTC = DateTime.UtcNow;
            return DbSet.Where(e => e.Type != NotamType.C && e.ReplaceNotamId == null && effectedArea.Intersects(e.EffectArea) && (!e.EndValidity.HasValue || e.Estimated || e.EndValidity > nowUTC)).ToListAsync();
        }

        public Task<List<Notam>> GetByLocationAfterDateAsync(string location, DateTime date)
        {
            var effectedArea = GetEffectArea(location);
            var nowUTC = DateTime.UtcNow;
            return DbSet.Where(e => e.Type != NotamType.C && e.ReplaceNotamId == null && effectedArea.Intersects(e.EffectArea) && e.StartActivity >= date && (!e.EndValidity.HasValue || e.Estimated || e.EndValidity > nowUTC)).ToListAsync();
        }

        public Task<int> GetNotamRefCountByNotamIdAsync(string notamId)
        {
            return DbSet.CountAsync(e => e.ReferredNotamId == notamId);
        }

        public Task<Notam> GetByNotamIdAsync(string notamId)
        {
            return DbSet.SingleOrDefaultAsync(e => e.NotamId == notamId);
        }

        public Task<List<Notam>> GetByLocationBeforeDateAsync(string location, DateTime date)
        {
            var effectedArea = GetEffectArea(location);
            var nowUTC = DateTime.UtcNow;
            return DbSet.Where(e => e.Type != NotamType.C && e.ReplaceNotamId == null && effectedArea.Intersects(e.EffectArea) && e.EndValidity <= date && (!e.EndValidity.HasValue || e.Estimated || e.EndValidity > nowUTC)).ToListAsync();
        }

        public Task<List<Notam>> GetByLocationBetweenDatesAsync(string location, DateTime startDate, DateTime endDate)
        {
            var effectedArea = GetEffectArea(location);
            var nowUTC = DateTime.UtcNow;
            return DbSet.Where(e => e.Type != NotamType.C && e.ReplaceNotamId == null && effectedArea.Intersects(e.EffectArea) && e.StartActivity >= startDate && e.EndValidity <= endDate && (!e.EndValidity.HasValue || e.Estimated || e.EndValidity > nowUTC)).ToListAsync();
        }

        public async Task<NotamDto> GetNoltamDtoByIdAsync(Guid notamId)
        {
            var notam = await DbSet.SingleOrDefaultAsync(e => e.Id == notamId);
            if (notam == null) return null;
            var culture = new CultureInfo("en-US");
            var dto = new NotamDto
            {
                Id = notam.Id,
                RootId = notam.RootId,
                NotamId = notam.NotamId,
                ReferredNotamId = notam.ReferredNotamId,
                ProposalId = notam.ProposalId,
                Originator = notam.Originator,
                OriginatorEmail = notam.OriginatorEmail,
                OriginatorPhone = notam.OriginatorPhone,
                ItemA = notam.ItemA,
                StartActivity = (notam.StartActivity.HasValue) ? notam.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                EndValidity = (notam.EndValidity.HasValue) ? notam.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((notam.Type == NotamType.C) ? "" : "PERM"),
                Published = notam.Published,
                NotamType = notam.Type,
                Series = notam.Series,
                Scope = notam.Scope,
                Code23 = notam.Code23,
                Code45 = notam.Code45,
                Traffic = notam.Traffic,
                Purpose = notam.Purpose,
                IsReplaced = notam.ReplaceNotamId.HasValue,
                IsEstimated = notam.Estimated,
                IsPermanent = notam.Permanent,
                ItemF = notam.ItemF,
                ItemG = notam.ItemG,
                Coordinates = notam.Coordinates,
                Radius = notam.Radius,
                LowerLimit = notam.LowerLimit,
                UpperLimit = notam.UpperLimit,
                ModifiedByOrg = notam.ModifiedByOrg,
                ModifiedByUsr = notam.ModifiedByUsr,
                Operator = notam.Operator,
                EffectArea = notam.EffectArea,
                Urgent = notam.Urgent,
                SeriesChecklist = notam.SeriesChecklist,
                IsObsolete = Notam.IsObsoleteNotam(notam, DateTime.UtcNow)
            };
            return dto;
        }

        /// <summary>
        /// Get Notams within a given range below
        /// </summary>
        /// <param name="series"></param>
        /// <param name="fromNumber"></param>
        /// <param name="toNumber"></param>
        /// <param name="fromYear"></param>
        /// <param name="toYear"></param>
        /// <returns>An IEnumerable of RqnNotamResultDto</returns>
        public IEnumerable<RqnNotamResultDto> GetNotamsByRange(string series, int fromNumber, int toNumber, int year)
        {
            var query = DbSet
                .Where(notam => notam.Series == series && notam.Number >= fromNumber && notam.Number <= toNumber && notam.Year == year && !notam.NumberRolled)
                .Include("ReplaceNotam")
                .OrderBy(notam => notam.Number);

            return BuildRqnNotamResultDto(query);
        }

        /// <summary>
        /// Gets Notams by series
        /// </summary>
        /// <param name="series"></param>
        /// <param name="includeChecklist">Whether to include the checklist notam numbers</param>
        /// <returns>Year / Notam numbers</returns>
        public IEnumerable<RqlNotamResultDto> GetActiveNotamNumbersBySeries(string series, bool includeChecklist)
        {
            var query = DbSet
                .Where(e => e.Series == series && !e.NumberRolled && (includeChecklist || !e.SeriesChecklist) && !e.EffectiveEndValidity.HasValue && (!e.EndValidity.HasValue || e.Estimated || e.EndValidity >= DateTime.UtcNow))
                .GroupBy(x => x.Year)
                .Select(ng =>
                    new RqlNotamResultDto
                    {
                        Year = ng.Key,
                        Numbers = ng.Select(n => n.Number).OrderBy(n => n).ToList()
                    });

            return query;
        }

        /// <summary>
        /// Gets notam the next by sequence number and reference year.
        /// </summary>
        /// <param name="lastSequenceNumber"></param>
        /// <returns></returns>
        public Notam GetNextBySequenceNumberAndReferenceDate(int lastSequenceNumber, DateTime refDate)
        {
            // What notams to include?
            // 1- find the next in sequence order (e.SequenceNumber > lastSequenceNumber)
            // 2- if after refDate (e.StartActivity >= refDate) => [OK]
            // 3- if canceled (e.Type == NotamType.C) => [IGNORE IT]
            // 4- if has EffectiveEndDate (e.EffectiveEndValidity.HasValue) => [IGNORE IT]
            // 5- if has not EndValidity (!e.EndValidity.HasValue) => [OK]
            // 6- if Estimated (e.Estimated) => [OK]
            // 7- if (e.EndValidity > nowDate) => [OK]
            var nowDate = DateTime.UtcNow;
            return DbSet.OrderBy(e => e.SequenceNumber)
                        .FirstOrDefault(e => e.SequenceNumber > lastSequenceNumber && !e.NumberRolled &&
                            (e.StartActivity >= refDate || (e.Type != NotamType.C && !e.EffectiveEndValidity.HasValue && (!e.EndValidity.HasValue || e.Estimated || e.EndValidity > nowDate))));
        }

        /// <summary>
        /// Finds the notam with matching series, marked as checklist and still active.
        /// </summary>
        /// <param name="series">Series to look for.</param>
        /// <returns></returns>
        public Notam FindActiveSeriesChecklist(string series)
        {
            return DbSet.FirstOrDefault(e => e.Series == series && e.SeriesChecklist && !e.NumberRolled &&  !e.EffectiveEndValidity.HasValue);
        }

        /// <summary>
        /// Finds the notam with matching series
        /// </summary>
        /// <param name="series">Series to look for.</param>
        /// <returns></returns>
        public async Task<bool> FindNotamWithSeriesAsync(string series)
        {
            return await DbSet.AnyAsync(e => e.Series == series);
        }

        public Task<string> GetPrevIcaoText(string notamId)
        {
            var ctx = DbContext as AppDbContext;

            if (ctx == null)
                throw new NullReferenceException("Unable to cast DbContext to AppDbContext");

            var query = DbSet
                .Join(ctx.Notams,
                    c => c.Id,
                    p => p.ReplaceNotamId,
                    (c, p) => new { c, p })
                .Where(r => r.c.NotamId == notamId && !r.c.NumberRolled)
                .Select(r=> r.p.IcaoText);

            return query.SingleOrDefaultAsync();
        }

        public Task<string> GetCurrentIcaoText(string notamId)
        {
            var query = DbSet
                    .Where(e => e.NotamId == notamId && !e.NumberRolled)
                    .Select(r => r.IcaoText);

            return query.SingleOrDefaultAsync();
        }

        public bool CheckForNewNotamWithProposalId(int proposalId)
        {
            return DbSet.Count(n => n.ProposalId == proposalId && n.Type == NotamType.N) != 0;
        }

        static IEnumerable<RqnNotamResultDto> BuildRqnNotamResultDto(IEnumerable<Notam> query)
        {
            foreach (var item in query)
            {
                yield return new RqnNotamResultDto
                {
                    Notam = item,
                    ReplaceNotamId = GenerateNotamId(item.ReplaceNotam),
                    ReplaceNotamType = GetNotamType(item.ReplaceNotam)
                };
            }
        }

        static string GenerateNotamId(Notam notam)
        {
            return notam != null ? ICAOTextUtils.FormatNotamId(notam.Series, notam.Number, notam.Year) : string.Empty;
        }

        static NotamType GetNotamType(Notam notam)
        {
            return notam != null ? notam.Type : NotamType.N;
        }

        private DbGeography GetEffectArea(string location)
        {
            var elements = location.Split(',').Select(e => double.Parse(e)).ToArray();
            var effectedArea = elements.Length == 2 ? GenratePointGeo(elements[0], elements[1]) : GeneratePolygonGeography(elements);
            return effectedArea;
        }

        private DbGeography GenratePointGeo(double longitude, double latitude)
        {
            var point = DbGeography.PointFromText($"POINT({longitude} {latitude})", CommonDefinitions.Srid);
            return point.Buffer(1.0);
        }

        private DbGeography GeneratePolygonGeography(double[] elements)
        {
            // Note: Polygon points format is [long, lat] and should be listed counter clockwise.

            var x1 = Math.Min(elements[0], elements[2]); // min long
            var y1 = Math.Max(elements[1], elements[3]); // max lat

            var x2 = Math.Max(elements[0], elements[2]); // max long
            var y2 = Math.Min(elements[1], elements[3]); // min lat

            if (x1 == x2) x2 += 0.000001;
            if (y1 == y2) y1 += 0.000001;

            var polyText = $"POLYGON(({x1} {y1}, {x1} {y2}, {x2} {y2}, {x2} {y1}, {x1} {y1}))";

            return DbGeography.PolygonFromText(polyText, CommonDefinitions.Srid);
        }

        private List<NotamDto> GenerateNotamDto(List<Notam> notams)
        {
            var dtos = new List<NotamDto>();
            var culture = new CultureInfo("en-US");
            foreach (var notam in notams)
            {
                dtos.Add(new NotamDto
                {
                    Id = notam.Id,
                    RootId = notam.RootId,
                    NotamId = notam.NotamId,
                    ReferredNotamId = notam.ReferredNotamId,
                    ProposalId = notam.ProposalId,
                    Originator = notam.Originator,
                    OriginatorEmail = notam.OriginatorEmail,
                    OriginatorPhone = notam.OriginatorPhone,
                    ItemA = notam.ItemA,
                    StartActivity = (notam.StartActivity.HasValue) ? notam.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (notam.EndValidity.HasValue) ? notam.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((notam.Type == NotamType.C) ? "" : "PERM"),
                    Published = notam.Published,
                    NotamType = notam.Type,
                    Series = notam.Series,
                    Scope = notam.Scope,
                    Code23 = notam.Code23,
                    Code45 = notam.Code45,
                    Traffic = notam.Traffic,
                    Purpose = notam.Purpose,
                    IsReplaced = notam.ReplaceNotamId.HasValue,
                    IsEstimated = notam.Estimated,
                    IsPermanent = notam.Permanent,
                    ItemF = notam.ItemF,
                    ItemG = notam.ItemG,
                    SiteId = notam.ItemA == "CXXX" ? notam.ItemE.Substring(0, 4) : "",
                    Coordinates = notam.Coordinates,
                    Radius = notam.Radius,
                    LowerLimit = notam.LowerLimit,
                    UpperLimit = notam.UpperLimit,
                    ModifiedByOrg = notam.ModifiedByOrg,
                    ModifiedByUsr = notam.ModifiedByUsr,
                    Operator = notam.Operator,
                    EffectArea = notam.EffectArea,
                    Urgent = notam.Urgent,
                    SeriesChecklist = notam.SeriesChecklist,
                    IsExpired = Notam.IsExpiredNotam(notam),
                    IsObsolete = Notam.IsObsoleteNotam(notam, DateTime.UtcNow)
                });
            }
            return dtos;
        }

        private List<NotamStatsDto> GenerateStatsReport(List<StatsPartialDto> stats, DateTime start, DateTime end, AggregatePeriod period)
        {
            var statsDic = new Dictionary<string, NotamStatsDto>();

            var seriesSummary = new int[26];
            foreach (var stat in stats)
            {
                var datePeriod = GetPeriodText(stat, period);

                var key = datePeriod.Item1 + "-" + datePeriod.Item2;

                var current = CreateNotamStatDto(stat, datePeriod, seriesSummary);

                NotamStatsDto previous = statsDic.ContainsKey(key) ? statsDic[key] : null;

                statsDic[key] = previous != null ? UpdateNotamStatDto(current, previous) : current;
            }

            var data = statsDic.Values.ToList();

            switch( period)
            {
                case AggregatePeriod.Yearly: data = AddMissingYears(start, end, data); break;
                case AggregatePeriod.Monthly: data = AddMissingMonths(start, end, data); break;
                case AggregatePeriod.Weekly: data = AddMissingWeeks(start, end, data); break;
                case AggregatePeriod.Daily: data = AddMissingDays(start, end, data); break;
                case AggregatePeriod.Hourly: data = AddMissingHours(start, end, data); break;
            }

            var result = GenerateFooterSummary(data, seriesSummary);

            return result;
        }

        private List<NotamStatsDto> AddMissingYears(DateTime start, DateTime end, List<NotamStatsDto> data)
        {
            foreach (var stat in DateRangeExt.EachYearTo(new DateTime(start.Year,1 ,1), end))
            { 
                var shortDate = stat.ToShortDateString();
                if (data.FirstOrDefault(x => x.StartPeriod == shortDate) == null)
                {
                    data.Add(new NotamStatsDto { StartPeriod = shortDate, EndPeriod = stat.AddYears(1).AddDays(-1).ToShortDateString() });
                }
            }

            return data.OrderBy(x => DateTime.Parse(x.StartPeriod)).ToList();
        }

        private List<NotamStatsDto> AddMissingWeeks(DateTime start, DateTime end, List<NotamStatsDto> data)
        {
            foreach (var week in DateRangeExt.EachWeekTo(start, end))
            {
                var shortDate = week.ToShortDateString();
                if (data.FirstOrDefault(x => x.StartPeriod == shortDate) == null)
                {
                    data.Add(new NotamStatsDto { StartPeriod = shortDate, EndPeriod = week.AddDays(6).ToShortDateString() });
                }
            }

            return data.OrderBy(x => DateTime.Parse(x.StartPeriod)).ToList();
        }

        private List<NotamStatsDto> AddMissingMonths(DateTime start, DateTime end, List<NotamStatsDto> data)
        {
            foreach (var month in DateRangeExt.EachMonthTo(start, end))
            {
                var shortDate = month.ToShortDateString();
                if (data.FirstOrDefault(x => x.StartPeriod == shortDate) == null)
                {
                    data.Add(new NotamStatsDto { StartPeriod = shortDate, EndPeriod = month.AddMonths(1).AddDays(-1).ToShortDateString() });
                }
            }

            return data.OrderBy(x => DateTime.Parse(x.StartPeriod)).ToList();
        }

        private List<NotamStatsDto> AddMissingDays(DateTime start, DateTime end, List<NotamStatsDto> data)
        {
            foreach (var day in DateRangeExt.EachDayTo(start, end))
            {
                var shortDate = day.ToShortDateString();
                if (data.FirstOrDefault(x => x.StartPeriod == shortDate) == null)
                {
                    data.Add(new NotamStatsDto { StartPeriod = shortDate });
                }
            }

            return data.OrderBy(x => DateTime.Parse(x.StartPeriod)).ToList();
        }

        private List<NotamStatsDto> AddMissingHours(DateTime start, DateTime end, List<NotamStatsDto> data)
        {
            for (var h = 0; h < 25; h++)
            {
                var hour = h.ToString().PadLeft(2, '0').PadRight(4, '0');
                if (data.FirstOrDefault(x => x.StartPeriod == h.ToString()) == null)
                {
                    data.Add(new NotamStatsDto { StartPeriod = start.ToShortDateString(), EndPeriod = hour });
                }
            }

            return data.OrderBy(x => int.Parse(x.EndPeriod)).ToList();
        }

        private Tuple<string, string> GetPeriodText(StatsPartialDto stat, AggregatePeriod period)
        {
            if( period == AggregatePeriod.Daily )
            {
                var date = stat.Published.ToShortDateString();
                return new Tuple<string, string>(date, "");
            }
            else if(period == AggregatePeriod.Hourly)
            {
                var date = stat.Published.ToShortDateString();
                return new Tuple<string, string>(date, stat.HourPerid.ToString().PadRight(4, '0'));
            }
            else
            {
                var start = stat.StartPeriod.ToShortDateString();
                var end = stat.EndPeriod.ToShortDateString();
                return new Tuple<string, string>(start, end);
            }
        }

        private NotamStatsDto CreateNotamStatDto (StatsPartialDto stat, Tuple<string, string> period, int[] seriesSummary)
        {
            var dto = new NotamStatsDto
            {
                StartPeriod = period.Item1,
                EndPeriod = period.Item2,
                Total = stat.SeriesCount
                
            };

            switch( stat.Series )
            {
                case "A": dto.A = stat.SeriesCount; seriesSummary[0] += stat.SeriesCount; break;
                case "B": dto.B = stat.SeriesCount; seriesSummary[1] += stat.SeriesCount; break;
                case "C": dto.C = stat.SeriesCount; seriesSummary[2] += stat.SeriesCount; break;
                case "D": dto.D = stat.SeriesCount; seriesSummary[3] += stat.SeriesCount; break;
                case "E": dto.E = stat.SeriesCount; seriesSummary[4] += stat.SeriesCount; break;
                case "F": dto.F = stat.SeriesCount; seriesSummary[5] += stat.SeriesCount; break;
                case "G": dto.G = stat.SeriesCount; seriesSummary[6] += stat.SeriesCount; break;
                case "H": dto.H = stat.SeriesCount; seriesSummary[7] += stat.SeriesCount; break;
                case "I": dto.I = stat.SeriesCount; seriesSummary[8] += stat.SeriesCount; break;
                case "J": dto.J = stat.SeriesCount; seriesSummary[9] += stat.SeriesCount; break;
                case "K": dto.K = stat.SeriesCount; seriesSummary[10] += stat.SeriesCount; break;
                case "L": dto.L = stat.SeriesCount; seriesSummary[11] += stat.SeriesCount; break;
                case "M": dto.M = stat.SeriesCount; seriesSummary[12] += stat.SeriesCount; break;
                case "N": dto.N = stat.SeriesCount; seriesSummary[13] += stat.SeriesCount; break;
                case "O": dto.O = stat.SeriesCount; seriesSummary[14] += stat.SeriesCount; break;
                case "P": dto.P = stat.SeriesCount; seriesSummary[15] += stat.SeriesCount; break;
                case "Q": dto.Q = stat.SeriesCount; seriesSummary[16] += stat.SeriesCount; break;
                case "R": dto.R = stat.SeriesCount; seriesSummary[17] += stat.SeriesCount; break;
                case "S": dto.S = stat.SeriesCount; seriesSummary[18] += stat.SeriesCount; break;
                case "T": dto.T = stat.SeriesCount; seriesSummary[19] += stat.SeriesCount; break;
                case "U": dto.U = stat.SeriesCount; seriesSummary[20] += stat.SeriesCount; break;
                case "V": dto.V = stat.SeriesCount; seriesSummary[21] += stat.SeriesCount; break;
                case "W": dto.W = stat.SeriesCount; seriesSummary[22] += stat.SeriesCount; break;
                case "X": dto.X = stat.SeriesCount; seriesSummary[23] += stat.SeriesCount; break;
                case "Y": dto.Y = stat.SeriesCount; seriesSummary[24] += stat.SeriesCount; break;
                case "Z": dto.Z = stat.SeriesCount; seriesSummary[25] += stat.SeriesCount; break;
            }

            return dto;
        }

        private List<NotamStatsDto> GenerateFooterSummary(List<NotamStatsDto> reportRows, int[] seriesSummary)
        {
            reportRows.Add(new NotamStatsDto
            {
                StartPeriod = "",
                EndPeriod = "",
                A = seriesSummary[0],
                B = seriesSummary[1],
                C = seriesSummary[2],
                D = seriesSummary[3],
                E = seriesSummary[4],
                F = seriesSummary[5],
                G = seriesSummary[6],
                H = seriesSummary[7],
                I = seriesSummary[8],
                J = seriesSummary[9],
                K = seriesSummary[10],
                L = seriesSummary[11],
                M = seriesSummary[12],
                N = seriesSummary[13],
                O = seriesSummary[14],
                P = seriesSummary[15],
                Q = seriesSummary[16],
                R = seriesSummary[17],
                S = seriesSummary[18],
                T = seriesSummary[19],
                U = seriesSummary[20],
                V = seriesSummary[21],
                W = seriesSummary[22],
                X = seriesSummary[23],
                Y = seriesSummary[24],
                Z = seriesSummary[25],
                Total = seriesSummary.Sum()
            });

            return reportRows;
        }

        private NotamStatsDto UpdateNotamStatDto(NotamStatsDto current, NotamStatsDto prev)
        {
            return new NotamStatsDto
            {
                StartPeriod = current.StartPeriod,
                EndPeriod = current.EndPeriod,
                A = current.A + prev.A,
                B = current.B + prev.B,
                C = current.C + prev.C,
                D = current.D + prev.D,
                E = current.E + prev.E,
                F = current.F + prev.F,
                G = current.G + prev.G,
                H = current.H + prev.H,
                I = current.I + prev.I,
                J = current.J + prev.J,
                K = current.K + prev.K,
                L = current.L + prev.L,
                M = current.M + prev.M,
                N = current.N + prev.N,
                O = current.O + prev.O,
                P = current.P + prev.P,
                Q = current.Q + prev.Q,
                R = current.R + prev.R,
                S = current.S + prev.S,
                T = current.T + prev.T,
                U = current.U + prev.U,
                V = current.V + prev.V,
                W = current.W + prev.W,
                X = current.X + prev.X,
                Y = current.Y + prev.Y,
                Z = current.Z + prev.Z,
                Total = current.Total + prev.Total
            };
        }
    }
}
