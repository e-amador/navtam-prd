﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class RolePermissionRepo : Repository<RolePermission>, IRolePermissionRepo
    {
        public RolePermissionRepo(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
