﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;

    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class PermissionRepo : Repository<Permission>, IPermissionRepo
    {
        public PermissionRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}
