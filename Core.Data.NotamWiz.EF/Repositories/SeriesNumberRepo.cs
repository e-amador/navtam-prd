﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    class SeriesNumberRepo : Repository<SeriesNumber>, ISeriesNumberRepo
    {
        public SeriesNumberRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public SeriesNumber GetSeriesNumber(char series, int year)
        {
            var snSeries = series.ToString();
            return DbSet.Where(sn => sn.Series == snSeries && sn.Year == year).FirstOrDefault();
        }

        public Task<SeriesNumber> GetSeriesNumberAsync(char series, int year)
        {
            var snSeries = series.ToString();
            return DbSet.Where(sn => sn.Series == snSeries && sn.Year == year).FirstOrDefaultAsync();
        }

        public void SafeUpdateSeriesNumber(SeriesNumber seriesNumber)
        {
            var context = AppDbContext;

            var rowVersion = seriesNumber.RowVersion;
            seriesNumber.RowVersion = Guid.NewGuid();

            var dbEntityEntry = context.Entry(seriesNumber);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(seriesNumber);
            }

            dbEntityEntry.OriginalValues["RowVersion"] = rowVersion;

            dbEntityEntry.State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}
