﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class ProposalAcknowledgementRepo : Repository<ProposalAcknowledgement>, IProposalAcknowledgementRepo
    {
        public ProposalAcknowledgementRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<ProposalAcknowledgement> GetByProposalIdAndOrgIdAsync(int proposalId, int orgId)
        {
            return DbSet
                .SingleOrDefaultAsync(e => e.ProposalId == proposalId && e.OrganizationId == orgId);
        }

        public Task<List<ProposalAcknowledgement>> GetByProposalIdAsync(int proposalId)
        {
            return DbSet.Where(e => e.ProposalId == proposalId).ToListAsync();
        }

        public async Task<bool> HasRecordsAsync(int proposalId)
        {
            return await DbSet.CountAsync(e => e.ProposalId == proposalId) > 0;
        }

    }
}
