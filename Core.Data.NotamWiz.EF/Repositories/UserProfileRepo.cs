﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using Common.Common;
    using Contracts.Repositories;
    using Domain.Model.Dtos;
    using Domain.Model.Entitities;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserProfileRepo : Repository<UserProfile>, IUserProfileRepo
    {
        public UserProfileRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public UserProfile GetById(string userId)
        {
            return DbSet.SingleOrDefault(e => e.Id == userId);
        }

        public Task<UserProfile> GetByIdAsync(string userId, bool shouldIncludeRegistration)
        {
            return shouldIncludeRegistration
                ? DbSet.Include(e => e.Registration).FirstOrDefaultAsync(e => e.Id == userId)
                : DbSet.FirstOrDefaultAsync(e => e.Id == userId);
        }

        public async Task<int> GetAllCountAsync(bool includeDisabled, bool includeDeleted, QueryOptions queryOptions)
        {
            int result = 0;
            var filter = queryOptions.FilterValue;

            if (includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.UserName != "Backend" && 
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter) 
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();
            else if (!includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.UserName != "Backend" && 
                        !u.IsDeleted &&
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();
            else if (includeDeleted && !includeDisabled)
                result = await DbSet.Where(u => u.UserName != "Backend" && 
                        !u.Disabled &&
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();
            else
                result = await DbSet.Where(u => u.UserName != "Backend" && 
                        !u.IsDeleted && !u.Disabled &&
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();

            return result;
        }

        public async Task<int> GetUserCountInOrganization(int orgId, bool includeDisabled, bool includeDeleted)
        {
            int result = 0;
            if (includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId).CountAsync();
            else if (!includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId &&
                        !u.IsDeleted)
                    .CountAsync();
            else if (includeDeleted && !includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId && 
                        !u.Disabled)
                    .CountAsync();
            else
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId && 
                        !u.IsDeleted && !u.Disabled)
                    .CountAsync();

            return result;
        }

        public async Task<int> GetUserCountInOrganization(int orgId, bool includeDisabled, bool includeDeleted, QueryOptions queryOptions)
        {
            int result = 0;
            var filter = queryOptions.FilterValue;

            if (includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId &&
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();
            else if (!includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId && !u.IsDeleted &&
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();
            else if (includeDeleted && !includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId && !u.Disabled &&
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();
            else
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId && !u.IsDeleted && !u.Disabled &&
                        (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                        || u.Position.Contains(filter) || u.UserName.Contains(filter))
                    ).CountAsync();

            return result;
        }

        public async Task<List<UserProfile>> GetUsersInOrganization(int orgId, bool includeDisabled, bool includeDeleted)
        {
            var result = new List<UserProfile>();
            if (includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId)
                    .ToListAsync();
            else if (!includeDeleted && includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId &&
                        !u.IsDeleted)
                    .ToListAsync();
            else if (includeDeleted && !includeDisabled)
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId && 
                        !u.Disabled)
                    .ToListAsync();
            else
                result = await DbSet.Where(u => u.OrganizationId.HasValue && u.OrganizationId.Value == orgId && 
                        !u.IsDeleted && !u.Disabled)
                    .ToListAsync();

            return result;
        }

        public async Task<List<UserDto>> GetAllAsync(QueryOptions queryOptions, int? orgId, bool includeDisabled, bool includeDeleted)
        {
            var ctx = DbContext as AppDbContext;
            var roles = await ctx.Roles.ToListAsync();
            var filter = queryOptions.FilterValue;

            var users = DbSet.Where(u => (!orgId.HasValue || u.OrganizationId == orgId.Value) &&
                                         (u.UserName != "Backend") &&
                                         (includeDisabled || !u.Disabled) &&
                                         (includeDeleted || !u.IsDeleted) &&
                                         (u.FirstName.Contains(filter) || u.LastName.Contains(filter) || u.Address.Contains(filter)
                                         || u.Position.Contains(filter) || u.UserName.Contains(filter)))
                             .Include(e => e.Organization)
                             .Include(e => e.Roles);

            var dtos = new List<UserDto>();
            foreach (var user in users)
            {
                dtos.Add(new UserDto
                {
                    Id = user.Id,
                    Username = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Organization = user.Organization?.Name ?? "",
                    Roles = GetRoleNames(roles, user.Roles),
                    Disabled = user.Disabled,
                    Deleted = user.IsDeleted,
                });
            }

            var dtoComparer = new DtoComparer(queryOptions.Sort);
            dtos.Sort(dtoComparer.GetComparer());

            return dtos.Skip(queryOptions.PageSize * (queryOptions.Page - 1)).Take(queryOptions.PageSize).ToList();
        }

        public async Task<List<UserRoleDto>> GetRolesAsync()
        {
            var ctx = DbContext as AppDbContext;
            return await ctx.Roles.Select(e =>
                new UserRoleDto
                {
                    RoleId = e.Id,
                    RoleName = e.Name
                }).ToListAsync();
        }

        public UserProfile GetByUserName(string userName)
        {
            return DbSet.FirstOrDefault(e => e.UserName == userName);
        }

        public Task<UserProfile> GetByUserNameAsync(string userName)
        {
            return DbSet.FirstOrDefaultAsync(e => e.UserName == userName);
        }

        public Task<List<UserProfile>> GetUnapprovedAsync()
        {
            return DbSet
                .Where(e => e.EmailConfirmed && !e.IsApproved)
                .ToListAsync();
        }

        public Task<List<UserProfile>> GetUnconfirmedEmailUsersAsync()
        {
            return DbSet
                .Where(e => e.RegistrationId.HasValue && !e.EmailConfirmed)
                .ToListAsync();
        }

        public Task<List<UserProfile>> GetNonDeletedAndFullyRegisteredUsersAsync()
        {
            return DbSet.Include(u => u.Roles)
                .Where(e => !e.IsDeleted && e.RegistrationId == null)
                .ToListAsync();
        }

        public List<UserProfile> GetUsersbyRole(string roleId)
        {
            return DbSet
               .Where(e => e.Roles.Select(y => y.RoleId).Contains(roleId))
               .ToList();
        }

        private string GetRoleNames(List<IdentityRole> roles, ICollection<IdentityUserRole> userRoles)
        {
            var roleNames = new List<string>();
            foreach (var userRole in userRoles)
            {
                roleNames.Add(roles.SingleOrDefault(e => e.Id == userRole.RoleId).Name);
            }

            return string.Join(",", roleNames);
        }

        public async Task<UserDto> GetUserInfoAsync(string userName)
        {
            var user = await DbSet.FirstOrDefaultAsync(e => e.UserName == userName);
            if (user != null)
            {
                return new UserDto
                {
                    Id = user.Id,
                    Username = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Position = user.Position,
                    Address = user.Address,
                    Email = user.Email,
                    EmailConfirmed = user.EmailConfirmed,
                    PhoneNumber = user.PhoneNumber,
                    Fax = user.Fax,
                    Approved = user.IsApproved,
                    Deleted = user.IsDeleted,
                    Organization = user.Organization.Name,
                    OrganizationId = user.Organization.Id,
                    RoleId = string.Join(",", user.Roles.Select(e => e.RoleId).ToArray()),
                    DoaIds = "",
                    Disabled = user.Disabled,
                };
            }

            return null;
        }

        /// <summary>
        /// Used to sort by columns the users
        /// </summary>
        private class DtoComparer
        {
            public DtoComparer(string field)
            {
                var tempField = (field ?? "");
                if (tempField.StartsWith("_"))
                {
                    _dir = -1;
                    _field = tempField.Substring(1).ToLower();
                }
                else
                {
                    _dir = 1;
                    _field = tempField.ToLower();
                }
            }

            public Comparison<UserDto> GetComparer()
            {
                switch (_field)
                {
                    case "firstname":
                        return CompareByFirstName;

                    case "lastname":
                        return CompareByLastName;

                    case "organization":
                        return CompareByOrganization;

                    case "roles":
                        return CompareByRole;

                    default:
                        return CompareByUsername;
                }
            }

            private int CompareByUsername(UserDto d1, UserDto d2) => Compare(d1.Username, d2.Username);

            private int CompareByFirstName(UserDto d1, UserDto d2) => Compare(d1.FirstName, d2.FirstName);

            private int CompareByLastName(UserDto d1, UserDto d2) => Compare(d1.LastName, d2.LastName);

            private int CompareByOrganization(UserDto d1, UserDto d2) => Compare(d1.Organization, d2.Organization);

            private int CompareByRole(UserDto d1, UserDto d2) => Compare(d1.Roles, d2.Roles);

            private int Compare(string s1, string s2) => _dir * string.Compare(s1, s2, StringComparison.InvariantCultureIgnoreCase);

            private readonly string _field;
            private readonly int _dir;
        }
    }
}