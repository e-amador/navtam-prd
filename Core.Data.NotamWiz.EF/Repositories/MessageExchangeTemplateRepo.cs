﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class MessageExchangeTemplateRepo : Repository<MessageExchangeTemplate>, IMessageExchangeTemplateRepo
    {
        public MessageExchangeTemplateRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<MessageExchangeTemplate> GetByIdAsync(Guid id)
        {
            return DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public Task<MessageExchangeTemplate> GetByNameAsync(string name)
        {
            return DbSet.SingleOrDefaultAsync(e => e.Name == name);
        }

        public Task<int> GetCountByName(string name)
        {
            return DbSet.CountAsync(e => e.Name == name);
        }

        public Task<List<MessageExchangeTemplateDto>> GetAllPartialsAsync()
        {
            return DbSet.Select(e=> new MessageExchangeTemplateDto
            {
                Id = e.Id,
                Name = e.Name,
                Addresses = e.Addresses,
                Created = e.Created
            }).ToListAsync();
        }

        public Task<List<MessageExchangeTemplateDto>> GetAllTemplateKeyParValuesAsync()
        {
            return DbSet.Select(e => new MessageExchangeTemplateDto
            {
                Id = e.Id,
                Name = e.Name
            }).ToListAsync();
        }
    }
}
