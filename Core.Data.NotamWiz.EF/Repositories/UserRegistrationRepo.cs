﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using Domain.Model.Entitities;
    using NavCanada.Core.Data.Contracts.Repositories;
    using System.Data.Entity;
    using System.Threading.Tasks;

    public class UserRegistrationRepo : Repository<UserRegistration>, IUserRegistrationRepo
    {
        public UserRegistrationRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<UserRegistration> GetUserRegistrationAsync(string userId)
        {
            return DbSet.SingleOrDefaultAsync(e => e.UserProfileId == userId);
        }
    }
}