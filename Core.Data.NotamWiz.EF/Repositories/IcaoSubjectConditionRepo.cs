﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using Common.Common;
    using Contracts.Repositories;
    using Domain.Model.Dtos;
    using Domain.Model.Entitities;
    using Extensions;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    public class IcaoSubjectConditionRepo : Repository<IcaoSubjectCondition>, IIcaoSubjectConditionRepo
    {
        public IcaoSubjectConditionRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<IcaoSubjectCondition> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Id == id);
        }

        public IcaoSubjectCondition GetByCode(long subjectId, string code)
        {
            return DbSet.FirstOrDefault(e => e.SubjectId == subjectId && e.Code == code);
        }

        public Task<IcaoSubjectCondition> GetByCodeAsync(long subjectId, string code)
        {
            return DbSet.FirstOrDefaultAsync(e => e.SubjectId == subjectId && e.Code == code);
        }

        public Task<List<IcaoSubjectCondition>> GetBySubjectIdAsync(int subjectId)
        {
            return DbSet.Where(e => e.SubjectId == subjectId)
                .ToListAsync();
        }

        public async Task<int> GetAllCountAsync(int subjectId)
        {
            return await DbSet.CountAsync(e => e.SubjectId == subjectId);
        }

        public async Task<List<string>> GetAllDistinctCodesAsync()
        {
            return await DbSet.Select(e => e.Code).Distinct().ToListAsync();
        }

        public async Task<IcaoSubjectConditionDto> GetModelAsync(int id)
        {
            var condition = await DbSet.SingleOrDefaultAsync(e => e.Id == id);
            if (condition != null)
            {
                return new IcaoSubjectConditionDto
                {
                    Id = condition.Id,
                    Description = condition.Description,
                    DescriptionFrench = condition.DescriptionFrench,
                    Code = condition.Code,
                    B = condition.B,
                    M = condition.M,
                    N = condition.N,
                    O = condition.O,
                    I = condition.I,
                    V = condition.V,
                    Lower = condition.Lower,
                    Upper = condition.Upper,
                    Radius = condition.Radius,
                    requiresItemFG = condition.AllowFgEntry,
                    RequiresPurpose = condition.AllowPurposeChange,
                    CancellationOnly = condition.CancelNotamOnly,
                    Active = condition.Active,
                };
            }

            return null;
        }

        public async Task<List<IcaoSubjectConditionPartialDto>> GetAllAsync(int subjectId, QueryOptions queryOptions)
        {
            var ctx = DbContext as AppDbContext;

            var icaoSubjectConditions = await DbSet
                .Where(e=> e.SubjectId == subjectId )
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            var dtos = new List<IcaoSubjectConditionPartialDto>();

            foreach (var condition in icaoSubjectConditions)
            {
                dtos.Add(new IcaoSubjectConditionPartialDto
                {
                    Id = condition.Id,
                    Code = condition.Code,
                    IsActive = condition.Active,
                    Lower = condition.Lower,
                    Upper = condition.Upper,
                    requiresItemFG = condition.AllowFgEntry,
                    RequiresPurpose = condition.AllowPurposeChange,
                    Radius = condition.Radius,
                    Purpose = GetPurposeString(condition),
                    Traffic = GetTrafficString(condition),
                    Description = condition.Description,
                    DescriptionFrench = condition.DescriptionFrench,
                    CancellationOnly = condition.CancelNotamOnly,
                });
            }
            return dtos;
        }

        public Task<List<IcaoSubjectConditionDto>> GetBySubjectIdActiveAsync(int subjectId, string lang)
        {
            return DbSet
                .Where(e => 
                    e.SubjectId == subjectId && 
                    !e.CancelNotamOnly &&
                    e.Active )
                .Select(e=> new IcaoSubjectConditionDto
                {
                    Id = e.Id,
                    Code = e.Code,
                    B = e.B,
                    I = e.I,
                    V = e.V,
                    M = e.M,
                    N = e.N,
                    O = e.O,
                    Radius = e.Radius,
                    Lower = e.Lower,
                    Upper = e.Upper,
                    requiresItemFG = e.AllowFgEntry,
                    RequiresPurpose = e.AllowPurposeChange,
                    Description = lang == "en" ? e.Description : e.DescriptionFrench
                })
                .ToListAsync();
        }

        public Task<List<IcaoSubjectConditionDto>> GetCancelledActiveOnlyAsync(int subjectId, string lang)
        {
            return DbSet
                .Where(e => 
                    e.SubjectId == subjectId && 
                    e.Active && 
                    e.CancelNotamOnly)
                .Select(e => new IcaoSubjectConditionDto
                {
                    Id = e.Id,
                    Code = e.Code,
                    B = e.B,
                    I = e.I,
                    V = e.V,
                    M = e.M,
                    N = e.N,
                    O = e.O,
                    Radius = e.Radius,
                    Lower = e.Lower,
                    Upper = e.Upper,
                    requiresItemFG = e.AllowFgEntry,
                    RequiresPurpose = e.AllowPurposeChange,
                    Description = lang == "en" ? e.Description : e.DescriptionFrench
                })
                .ToListAsync();
        }

        private string GetPurposeString( IcaoSubjectCondition condition )
        {
            var B = condition.B ? "B" : "";
            var M = condition.M ? "M" : "";
            var N = condition.N ? "N" : "";
            var O = condition.O ? "O" : "";

            return $"{B}{M}{N}{O}";
        }

        private string GetTrafficString(IcaoSubjectCondition condition)
        {
            var I = condition.I ? "I" : "";
            var V = condition.V ? "V" : "";

            return $"{I}{V}";
        }

    }
}
