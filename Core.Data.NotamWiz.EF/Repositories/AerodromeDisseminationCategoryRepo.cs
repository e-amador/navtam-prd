﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Linq;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using System.Threading.Tasks;
    using Domain.Model.Dtos;
    using System.Collections.Generic;
    using Common.Common;
    using Extensions;
    using Newtonsoft.Json;
    using NavCanada.Core.Domain.Model.Enums;

    public class AerodromeDisseminationCategoryRepo : Repository<AerodromeDisseminationCategory>, IAerodromeDisseminationCategoryRepo
    {
        public AerodromeDisseminationCategoryRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
        public AerodromeDisseminationCategory GetByAhpMid(string ahpMid)
        {
            return DbSet.FirstOrDefault(e => e.AhpMid == ahpMid);
        }

        public async Task<AerodromeDisseminationCategory> GetByAhpMidAsync(string mid)
        {
            return await DbSet.FirstOrDefaultAsync(e => e.AhpMid == mid);
        }

        public async Task<AerodromeDisseminationCategory> GetByAhpCodeIdAsync(string ahpCodeId)
        {
            return await DbSet.FirstOrDefaultAsync(e => e.AhpCodeId == ahpCodeId);
        }

        public Task<AerodromeDisseminationCategory> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<int> GetAllCountAsync()
        {
            return await DbSet.CountAsync();
        }

        public async Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            return await DbSet
                .Where(s => s.AhpCodeId.Contains(filter) || s.AhpName.Contains(filter))
                .CountAsync();
        }

        static T Deserialize<T>(string json) => JsonConvert.DeserializeObject<T>(json);

        static string ServedCity(SdoCache sdo)
        {
            if (sdo == null) return "";
            if (sdo.Type != SdoEntityType.Aerodrome) return "";
            var aerodrome = Deserialize<AerodromePartial>(sdo.Attributes);
            return aerodrome?.ServedCity ?? string.Empty;
        }

        public async Task<List<AerodromeDisseminationCategoryDto>> GetAllAsync(QueryOptions queryOptions)
        {
            var ctx = DbContext as AppDbContext;
            var filter = queryOptions.FilterValue;

            var adcs = await DbSet
                .Where(s => s.AhpCodeId.Contains(filter) || s.AhpName.Contains(filter))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            var dtos = new List<AerodromeDisseminationCategoryDto>();

            foreach (var adc in adcs)
            {
                var sdo = await ctx.SdoCaches.SingleOrDefaultAsync(a => a.Id == adc.AhpMid);
                dtos.Add(new AerodromeDisseminationCategoryDto
                {
                    Id = adc.Id,
                    AhpCodeId = adc.AhpCodeId,
                    AhpMid = adc.AhpMid,
                    AhpName = adc.AhpName,
                    DisseminationCategory = adc.DisseminationCategory,
                    DisseminationCategoryName = adc.DisseminationCategory.ToString(),
                    Latitude = (adc.Location.Latitude.HasValue ? adc.Location.Latitude.Value : 0),
                    Longitude = (adc.Location.Longitude.HasValue ? adc.Location.Longitude.Value : 0),
                    ServedCity = ServedCity(sdo),
                });
            }
            return dtos;
        }

    }
}
