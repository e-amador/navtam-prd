﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.EF.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ProposalRepo : Repository<Proposal>, IProposalRepo
    {
        public ProposalRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Proposal> GetByIdAsync(int id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<List<Proposal>> GetGroupedProposalsByGroupIdAsync(Guid groupId)
        {
            return await DbSet.Where(e => e.GroupId == groupId).ToListAsync();
        }

        public async Task<List<ProposalDto>> GetByCategoryAsync(int categoryId, int organizationId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    e.CategoryId == categoryId &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    e.OrganizationId == organizationId &&
                    !e.Delete)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllAsync(int organizationId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    e.OrganizationId == organizationId &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    !e.Delete)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalStatusDto>> GetStatusAsync(int[] ids, DateTime since, int orgId, OrganizationType orgType)
        {
            var culture = new CultureInfo("en-US");
            var proposals = await DbSet
                .Where(e => ids.Contains(e.Id))
                .ToListAsync();
            var Minutes = GetMinutesToExpire();
            var result = new List<ProposalStatusDto>();
            foreach (var p in proposals)
            {
                var proposalStatus = new ProposalStatusDto
                {
                    Id = p.Id,
                    NotamId = p.NotamId,
                    StartActivity = (p.StartActivity.HasValue) ? p.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (p.EndValidity.HasValue) ? p.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((p.ProposalType == NotamType.C) ? "" : "PERM"),
                    Received = p.Received.ToString("yyMMddHHmm", culture),
                    Status = p.Status,
                    GroupId = p.GroupId.HasValue ? p.GroupId.ToString() : null,
                    SoonToExpire = IsSoonToExpire(p, Minutes),
                    ModifiedByUsr = p.ModifiedByUsr,
                    Operator = p.Operator,
                    Originator = p.Originator,
                    ItemE = p.ItemE,
                    ItemA = p.ItemA,
                    PendingReview = p.PendingReview,
                    SiteId = p.ItemA == "CXXX" ? p.ItemE.Substring(0, 4) : "",
                    Estimated = p.Estimated,
                };

                if (proposalStatus.PendingReview)
                {
                    if (orgType == OrganizationType.Fic || orgType == OrganizationType.External)
                    {
                        proposalStatus.PendingReview = await ShouldBeMarkedAsPendingReview(p.Id, orgId);
                    }
                }

                result.Add(proposalStatus);
            }

            return result;
        }

        public async Task<int> GetCountByCategoryAsync(int categoryId, int organizationId, QueryOptions queryOptions)
        {
            if (categoryId == 1)
            {
                return await DbSet.CountAsync(e =>
                    e.OrganizationId == organizationId &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    !e.Delete);
            }

            return await DbSet.CountAsync(e =>
                e.CategoryId == categoryId &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                e.OrganizationId == organizationId &&
                !e.Delete);
        }

        public async Task<int> GetAllCountAsync(int organizationId, QueryOptions queryOptions)
        {
            return await DbSet.CountAsync(e =>
                e.OrganizationId == organizationId &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                !e.Delete);
        }

        #region user proposals

        public async Task<int> GetUserProposalsInRegionCountAsync(DbGeography region, int orgId, OrganizationType orgType, int catId, QueryOptions queryOptions)
        {
            if (orgType == OrganizationType.External)
            {
                return await DbSet.CountAsync(e =>
                    (catId == 1 || e.CategoryId == catId) &&
                    (e.OrganizationId == orgId) &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    !e.SeriesChecklist &&
                    //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                    //Replaced or Terminated
                    ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                    e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                    e.Status != NotamProposalStatusCode.Replaced) ||
                    //e.Status != NotamProposalStatusCode.Rejected && 
                    //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                    //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                    //Cancelled, Replaced, Deleted or Discarded
                    (!e.Estimated) &&
                    ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                     (
                        (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                        (e.Status != NotamProposalStatusCode.Disseminated &&
                        e.Status != NotamProposalStatusCode.DisseminatedModified &&
                        e.Status != NotamProposalStatusCode.Cancelled &&
                        e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Expired &&
                        e.Status != NotamProposalStatusCode.Discarded)
                     )
                    )
                    ) && !e.Delete &&
                    e.EffectArea != null &&
                    region.Intersects(e.EffectArea));
            }

            if (orgType == OrganizationType.Fic)
            {
                //Note 1: The FIC user have to see everything, including the NOF Notam that intersect the FIC location
                //Note 2: Eric asked to allow FIC user to see the Proposals that are Expired but have never been
                //          Disseminated, Cancelled, Replaced, Deleted or Discarded
                return await DbSet.CountAsync(e =>
                    (catId == 1 || e.CategoryId == catId) &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    !e.SeriesChecklist &&
                    //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                    //Replaced or Terminated
                    ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                    e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                    e.Status != NotamProposalStatusCode.Replaced) ||
                    //e.Status != NotamProposalStatusCode.Rejected && 
                    //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                    //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                    //Cancelled, Replaced, Deleted or Discarded
                    (!e.Estimated) &&
                    ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                     (
                        (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                        (e.Status != NotamProposalStatusCode.Disseminated &&
                        e.Status != NotamProposalStatusCode.DisseminatedModified &&
                        e.Status != NotamProposalStatusCode.Cancelled &&
                        e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Expired &&
                        e.Status != NotamProposalStatusCode.Discarded)
                     )
                    )
                    ) &&
                    !e.Delete &&
<<<<<<< HEAD
                    e.Location != null &&
                    e.Location.Intersects(region));
=======
                    e.EffectArea != null &&
                    e.EffectArea.Intersects(region));
>>>>>>> f4e06108050342a2ce25992ce3872adeae07d194
            }

            // this is for Nof (just in case!)
            return await DbSet.CountAsync(e =>
                (catId == 1 || e.CategoryId == catId) &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                !e.SeriesChecklist &&
                //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                //Replaced or Terminated
                ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                e.Status != NotamProposalStatusCode.Replaced) ||
                //e.Status != NotamProposalStatusCode.Rejected && 
                //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                //Cancelled, Replaced, Deleted or Discarded
                (!e.Estimated) && 
                ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                 (
                    (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                    (e.Status != NotamProposalStatusCode.Disseminated &&
                    e.Status != NotamProposalStatusCode.DisseminatedModified &&
                    e.Status != NotamProposalStatusCode.Cancelled &&
                    e.Status != NotamProposalStatusCode.Replaced &&
                    e.Status != NotamProposalStatusCode.Expired &&
                    e.Status != NotamProposalStatusCode.Deleted &&
                    e.Status != NotamProposalStatusCode.Discarded)
                 )
                )
                ) && !e.Delete);
        }

        public async Task<List<ProposalDto>> GetUserProposalsInRegionAsync(DbGeography region, int orgId, OrganizationType orgType, int catId, QueryOptions queryOptions)
        {
            List<Proposal> proposals = null;

            if (orgType == OrganizationType.External)
            {
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (catId == 1 || e.CategoryId == catId) &&
                        (e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        e.Status != NotamProposalStatusCode.Terminated &&
                        !e.SeriesChecklist &&
                        //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                        //Replaced or Terminated
                        ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Replaced) ||
                        //e.Status != NotamProposalStatusCode.Rejected && 
                        //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                        //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                        //Cancelled, Replaced, Deleted or Discarded
                        (!e.Estimated) &&
                        ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                         (
                            (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                            (e.Status != NotamProposalStatusCode.Disseminated &&
                            e.Status != NotamProposalStatusCode.DisseminatedModified &&
                            e.Status != NotamProposalStatusCode.Cancelled &&
                            e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Expired &&
                            e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded)
                         )
                        )
                        ) && !e.Delete &&
                        e.EffectArea != null &&
                        region.Intersects(e.EffectArea))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else if (orgType == OrganizationType.Fic)
            {
                //Note 1: The FIC user have to see everything, including the NOF Notam that intersect the FIC location
                //Note 2: Eric asked to allow FIC user to see the Proposals that are Expired but have never been
                //          Disseminated, Cancelled, Replaced, Deleted or Discarded
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    //.Include(e=> e.User)
                    .Where(e =>
                        (catId == 1 || e.CategoryId == catId) &&
                        !e.Grouped &&
                        e.Status != NotamProposalStatusCode.Terminated &&
                        !e.SeriesChecklist &&
                        //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                        //Replaced or Terminated
                        ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Replaced) || 
                        //e.Status != NotamProposalStatusCode.Rejected && 
                        //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                        //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                        //Cancelled, Replaced, Deleted or Discarded
                        (!e.Estimated) &&
                        ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                         (
                            (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                            (e.Status != NotamProposalStatusCode.Disseminated &&
                            e.Status != NotamProposalStatusCode.DisseminatedModified &&
                            e.Status != NotamProposalStatusCode.Cancelled &&
                            e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Expired &&
                            e.Status != NotamProposalStatusCode.Discarded)
                         )
                        )
                        ) &&
                        !e.Delete &&
                        e.EffectArea != null &&
                        region.Intersects(e.EffectArea))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else
            {
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (catId == 1 || e.CategoryId == catId) &&
                        !e.Grouped &&
                        e.Status != NotamProposalStatusCode.Terminated &&
                        !e.SeriesChecklist &&
                        //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                        //Replaced or Terminated
                        ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Replaced) ||
                        //e.Status != NotamProposalStatusCode.Rejected && 
                        //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                        //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                        //Cancelled, Replaced, Deleted or Discarded
                        (!e.Estimated) &&
                        ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                         (
                            (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                            (e.Status != NotamProposalStatusCode.Disseminated &&
                            e.Status != NotamProposalStatusCode.DisseminatedModified &&
                            e.Status != NotamProposalStatusCode.Cancelled &&
                            e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Expired &&
                            e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded)
                         )
                        )
                        ) && !e.Delete)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }

            var proposalDtos = GenerateProposalDto(proposals);

            if (orgType == OrganizationType.Fic || orgType == OrganizationType.External)
            {
                foreach (var p in proposalDtos)
                {
                    if(p.PendingReview)
                    {
                        p.PendingReview = await ShouldBeMarkedAsPendingReview(p.Id, orgId);
                    }
                }
            }

            return proposalDtos;
        }

        public async Task<int> GetUserProposalsExpiringSoonCountAsync(DbGeography region, int orgId, OrganizationType orgType, int MinutesToExpire)
        {
            var DateToExpire = DateTime.UtcNow.AddMinutes(MinutesToExpire);
            if (orgType == OrganizationType.External)
            {
                //For External: only the ones that the Org created and intersec their DOA
                return await DbSet.CountAsync(e =>
                    (e.OrganizationId == orgId) &&
                    !e.Grouped &&
                    (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Terminated) &&
                    !e.SeriesChecklist &&
                    e.Estimated &&
                    (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                    !e.Delete &&
                    e.EffectArea != null &&
                    region.Intersects(e.EffectArea));
            }

            if (orgType == OrganizationType.Fic)
            {
                //For FIC: all the Notams that are in the DOA, doesn't matter if it was created by a NOF
                return await DbSet.CountAsync(e =>
                    //(e.OrganizationType != OrganizationType.Nof) &&
                    !e.Grouped &&
                    (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Terminated) &&
                    !e.SeriesChecklist &&
                    e.Estimated &&
                    (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                    !e.Delete &&
                    e.EffectArea != null &&
                    region.Intersects(e.EffectArea));
            }

            // For NOF: Only the ones that the NOF created
            return await DbSet.CountAsync(e =>
                !e.Grouped &&
                (e.OrganizationType == OrganizationType.Nof) &&
                    (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Terminated) &&
                !e.SeriesChecklist &&
                e.Estimated &&
                (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                !e.Delete);
        }

        public async Task<List<ProposalDto>> GetUserProposalsExpiringSoonAsync(DbGeography region, int orgId, OrganizationType orgType, QueryOptions queryOptions, int MinutesToExpire)
        {
            var DateToExpire = DateTime.UtcNow.AddMinutes(MinutesToExpire);
            List<Proposal> proposals = null;

            if (orgType == OrganizationType.External)
            {
                //For External: only the ones that the Org created and intersec their DOA
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                            e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Terminated) &&
                        !e.SeriesChecklist &&
                        e.Estimated &&
                        (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                        !e.Delete &&
                        e.EffectArea != null &&
                        region.Intersects(e.EffectArea))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else if (orgType == OrganizationType.Fic)
            {
                //For FIC: all the Notams that are in the DOA
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        !e.Grouped &&
                        (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                            e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Terminated) &&
                        !e.SeriesChecklist &&
                        e.Estimated &&
                        (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                        !e.Delete &&
                        e.EffectArea != null &&
                        region.Intersects(e.EffectArea))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else
            {
                // For NOF: Only the ones that the NOF created
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        //(e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        (e.OrganizationType == OrganizationType.Nof) &&
                        (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                            e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Terminated) &&
                        !e.SeriesChecklist &&
                        e.Estimated &&
                        (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                        !e.Delete)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }

            return GenerateProposalDto(proposals);
        }

        public async Task<int> GetCountByChildrenCategoryIdsAsync(DbGeography region, int[] categoryIds, int? orgId, QueryOptions queryOptions)
        {
            return await DbSet.CountAsync(e =>
                (!orgId.HasValue || e.OrganizationId == orgId.Value) &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                !e.Delete &&
                categoryIds.Contains(e.CategoryId) &&
                region.Intersects(e.EffectArea));
        }

        public async Task<List<ProposalDto>> GetByChildrenCategoryIdsAsync(DbGeography region, int[] categoryIds, int? orgId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    (!orgId.HasValue || e.OrganizationId == orgId) &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    !e.Delete &&
                    categoryIds.Contains(e.CategoryId) &&
                    region.Intersects(e.EffectArea))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public IQueryable<Proposal> PrepareQueryFilter(FilterCriteria filterCriteria)
        {
            var expressions = new List<Expression<Func<Proposal, bool>>>();
            IQueryable<Proposal> query = DbSet;
            foreach (var cond in filterCriteria.Conditions)
            {
                var endValidityFilter = false;
                var field = cond.Field.ToUpper();
                var value = cond.Value.ToString().ToUpper();
                var condition = cond.Value.ToString().ToUpper();
                var oper = cond.Operator;
                switch (field)
                {
                    case "PROPOSALTYPE":
                        cond.Value = (int)Enum.Parse(typeof(NotamType), value);
                        break;
                    case "LOWERLIMIT":
                    case "UPPERLIMIT":
                    case "RADIUS":
                        int number;
                        if (int.TryParse(value, out number))
                        {
                            cond.Value = number;
                        }
                        else
                        {
                            cond.Value = 0;
                        }
                        break;
                    case "RECEIVED":
                        DateTime published;
                        if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out published))
                        {
                            cond.Value = value;
                        }
                        else
                        {
                            cond.Value = DateTime.Now;
                        }
                        break;
                    case "STARTACTIVITY":
                        if (condition == "IMMEDIATE" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime startActivity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out startActivity))
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                    case "ENDVALIDITY":
                        endValidityFilter = true; // cond.Operator == "GTE" || cond.Operator == "GT";
                        if (condition == "PERM" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime endValidity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out endValidity))
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                }

                var expression = BuildWhereExpression(cond.Field, cond.Value, cond.Operator);
                if (endValidityFilter)
                {
                    if (endValidityFilter) //&& (oper == "EQ" || oper == "GT" || oper == "GTE")
                    {
                        var otherExpression = BuildWhereExpression("Permanent", true, "EQ");
                        expression = expression.Or(otherExpression);
                    }
                }

                query = query.Where(expression);
            }

            return query;
        }

        public async Task<int> FilterCountAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<Proposal> filterQuery)
        {
            var ctx = DbContext as AppDbContext;
            ctx.Database.CommandTimeout = 120;

            if (orgType == OrganizationType.External)
            {
                return await filterQuery.CountAsync(e =>
                                (e.OrganizationId == orgId) &&
                                !e.Grouped &&
                                !e.Delete && e.EffectArea != null &&
                                e.ProposalType != NotamType.C &&
                                (
                                    e.Status == NotamProposalStatusCode.Draft ||
                                    e.Status == NotamProposalStatusCode.Discarded ||
                                    e.Status == NotamProposalStatusCode.Disseminated ||
                                    e.Status == NotamProposalStatusCode.DisseminatedModified ||
                                    e.Status == NotamProposalStatusCode.Replaced ||
                                    e.Status == NotamProposalStatusCode.Submitted ||
                                    e.Status == NotamProposalStatusCode.Withdrawn ||
                                    e.Status == NotamProposalStatusCode.WithdrawnPending ||
                                    e.Status == NotamProposalStatusCode.Rejected ||
                                    e.Status == NotamProposalStatusCode.SoontoExpire
                                ) &&
                                region.Intersects(e.EffectArea));
            }

            return await filterQuery.CountAsync(e =>
                        //(e.OrganizationType != OrganizationType.Nof) &&
                        !e.Grouped &&
                        !e.Delete &&
                        e.ProposalType != NotamType.C &&
                        (
                            e.Status == NotamProposalStatusCode.Draft ||
                            e.Status == NotamProposalStatusCode.Discarded ||
                            e.Status == NotamProposalStatusCode.Disseminated ||
                            e.Status == NotamProposalStatusCode.DisseminatedModified ||
                            e.Status == NotamProposalStatusCode.Replaced ||
                            e.Status == NotamProposalStatusCode.Submitted ||
                            e.Status == NotamProposalStatusCode.Withdrawn ||
                            e.Status == NotamProposalStatusCode.WithdrawnPending ||
                            e.Status == NotamProposalStatusCode.Rejected ||
                            e.Status == NotamProposalStatusCode.SoontoExpire
                        ) &&
                        e.EffectArea != null && region.Intersects(e.EffectArea));
        }

        public async Task<List<ProposalDto>> FilterAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<Proposal> filterQuery)
        {
            List<Proposal> proposals = null;

            var ctx = DbContext as AppDbContext;
            ctx.Database.CommandTimeout = 120;

            if (orgType == OrganizationType.External)
            {
                proposals = await filterQuery
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        !e.Delete &&
                        e.ProposalType != NotamType.C &&
                        (
                            e.Status == NotamProposalStatusCode.Draft ||
                            e.Status == NotamProposalStatusCode.Discarded ||
                            e.Status == NotamProposalStatusCode.Disseminated ||
                            e.Status == NotamProposalStatusCode.DisseminatedModified ||
                            e.Status == NotamProposalStatusCode.Replaced ||
                            e.Status == NotamProposalStatusCode.Submitted ||
                            e.Status == NotamProposalStatusCode.Withdrawn ||
                            e.Status == NotamProposalStatusCode.WithdrawnPending ||
                            e.Status == NotamProposalStatusCode.Rejected ||
                            e.Status == NotamProposalStatusCode.SoontoExpire
                        ) &&
                        e.EffectArea != null && region.Intersects(e.EffectArea))
                    .ApplySort(filterCriteria.QueryOptions.Sort)
                    .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                    .Take(filterCriteria.QueryOptions.PageSize)
                    .ToListAsync();
            }
            else
            {
                proposals = await filterQuery
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        //(e.OrganizationType != OrganizationType.Nof) &&
                        !e.Grouped &&
                        !e.Delete &&
                        e.ProposalType != NotamType.C &&
                        e.EffectArea != null &&
                        (
                            e.Status == NotamProposalStatusCode.Draft ||
                            e.Status == NotamProposalStatusCode.Discarded ||
                            e.Status == NotamProposalStatusCode.Disseminated ||
                            e.Status == NotamProposalStatusCode.DisseminatedModified ||
                            e.Status == NotamProposalStatusCode.Replaced ||
                            e.Status == NotamProposalStatusCode.Submitted ||
                            e.Status == NotamProposalStatusCode.Withdrawn ||
                            e.Status == NotamProposalStatusCode.WithdrawnPending ||
                            e.Status == NotamProposalStatusCode.Rejected ||
                            e.Status == NotamProposalStatusCode.SoontoExpire
                        ) &&
                        region.Intersects(e.EffectArea))
                    .ApplySort(filterCriteria.QueryOptions.Sort)
                    .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                    .Take(filterCriteria.QueryOptions.PageSize)
                    .ToListAsync();
            }

            var proposalDtos = GenerateProposalDto(proposals, true);

            if (orgType == OrganizationType.Fic || orgType == OrganizationType.External)
            {
                foreach (var p in proposalDtos)
                {
                    if( p.PendingReview)
                    {
                        p.PendingReview = await ShouldBeMarkedAsPendingReview(p.Id, orgId);
                    }
                }
            }

            return proposalDtos;
        }

        #endregion user proposals

        public async Task<List<ProposalDto>> GetProposalsGroupedAsync(int organizationId, Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    //e.OrganizationId == organizationId && At this time the orgId that created the grouping has to be the NOF
                    e.GroupId == groupId &&
                    !e.IsGroupMaster &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow))
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetSubmittedByCategoryAsync(int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && (e.CategoryId == categoryId && e.Status == NotamProposalStatusCode.Submitted))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<int> GetSubmittedCountByCategoryAsync(int categoryId, QueryOptions queryOptions)
        {
            return await DbSet.CountAsync(e => !e.Grouped && (e.CategoryId == categoryId && e.Status == NotamProposalStatusCode.Submitted));
        }

        // pending queue
        public async Task<int> GetNofPendingQueueCountAsync()
        {
            return await DbSet.CountAsync(e => !e.Grouped && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<int> GetNofPendingQueueCountByCategoryIdAsync(int categoryId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<int> GetNofPendingByChildrenQueueCountByCategoryIdAsync(int[] categoryIds)
        {
            return await DbSet.CountAsync(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<List<ProposalDto>> GetNofPendingQueueByCategoryIdAsync(int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    e.CategoryId == categoryId &&
                    (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofPendingQueueByChildrenCategoryIdsAsync(int[] categoryIds, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    categoryIds.Contains(e.CategoryId) &&
                    (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllNofPendingQueueAsync(QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofPendingProposalsGroupedAsync(Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.GroupId == groupId && !e.IsGroupMaster && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        // park queue
        public async Task<int> GetNofParkedQueueCountAsync()
        {
            return await DbSet.CountAsync(e => !e.Grouped && (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken));
        }

        public async Task<int> GetNofParkedQueueCountByCategoryIdAsync(int categoryId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken));
            //return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<int> GetNofParkByChildrenQueueCountByCategoryIdAsync(int[] categoryIds)
        {
            return await DbSet.CountAsync(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken));
        }

        public async Task<List<ProposalDto>> GetNofParkedQueueByCategoryIdAsync(int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && e.CategoryId == categoryId && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofParkedByChildrenCategoryIdsAsync(int[] categoryIds, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllNofParkedQueueByAsync(QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofParkedProposalsGroupedAsync(Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.GroupId == groupId && !e.IsGroupMaster && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        // review queue
        public async Task<int> GetNofReviewQueueCountAsync(int currentUserOrgId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated);
        }

        public async Task<int> GetNofReviewQueueCountByCategoryIdAsync(int currentUserOrgId, int categoryId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated);
        }

        public async Task<int> GetNofReviewByChildrenQueueCountByCategoryIdAsync(int currentUserOrgId, int[] categoryIds)
        {
            return await DbSet.CountAsync(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && e.OrganizationId == currentUserOrgId);
        }

        public async Task<List<ProposalDto>> GetNofReviewQueueByCategoryIdAsync(int currentUserOrgId, int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && e.CategoryId == categoryId && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofReviewByChildrenCategoryIdsAsync(int currentUserOrgId, int[] categoryIds, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && e.OrganizationId == currentUserOrgId)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllNofReviewQueueByAsync(int currentUserOrgId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofReviewProposalsGroupedAsync(int currentUserOrgId, Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.GroupId == groupId && !e.IsGroupMaster && e.OrganizationId == currentUserOrgId)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public List<Proposal> GetExpiredProposals()
        {
            return DbSet
                .Where(e =>
                    e.EndValidity.HasValue &&
                    !e.Estimated &&
                    (e.Status == NotamProposalStatusCode.ModifiedAndSubmitted ||
                    e.Status == NotamProposalStatusCode.Submitted) &&
                    e.EndValidity < DateTime.UtcNow)
                .ToList();
        }

        public async Task<ProposalDto> GetProposalDto(int proposalId)
        {
            var proposal = await DbSet.SingleOrDefaultAsync(e => e.Id == proposalId);
            if (proposal != null)
            {
                var culture = new CultureInfo("en-US");
                var Minutes = GetMinutesToExpire();

                var response = new ProposalDto
                {
                    Id = proposal.Id,
                    NotamId = proposal.NotamId,
                    CategoryId = proposal.CategoryId,
                    CategoryName = proposal.Category.Name,
                    ItemA = proposal.ItemA,
                    SiteId = proposal.ItemA == "CXXX" ? proposal.ItemE.Substring(0, 4) : "",
                    IcaoSubjectDesc = proposal.IcaoSubject.Name,
                    IcaoConditionDesc = proposal.IcaoSubjectCondition.Description,
                    StartActivity = (proposal.StartActivity.HasValue) ? proposal.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (proposal.EndValidity.HasValue) ? proposal.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((proposal.ProposalType == NotamType.C) ? "" : "PERM"),
                    Estimated = proposal.Estimated,
                    Received = proposal.Received,
                    ProposalType = proposal.ProposalType,
                    Status = proposal.Status,
                    GroupId = proposal.GroupId.HasValue ? proposal.GroupId.ToString() : null,
                    SeriesChecklist = proposal.SeriesChecklist,
                    IsObsolete = IsObsoleteProposal(proposal),
                    Originator = proposal.Originator,
                    ModifiedByUsr = proposal.ModifiedByUsr,
                    Operator = proposal.Operator,
                    SoonToExpire = IsSoonToExpire(proposal, Minutes),
                    ItemE = proposal.ItemE,
                    PendingReview = proposal.PendingReview
                };

                return response;
            }
            return null;
        }

        static bool IsObsoleteProposal(Proposal prop)
        {
            //if (prop.ProposalType == NotamType.C && prop.Status != NotamProposalStatusCode.Rejected) return true;
            if (prop.ProposalType == NotamType.C ) return true;
            if (prop.Status == NotamProposalStatusCode.Expired) return true;
            if (prop.Status != NotamProposalStatusCode.DisseminatedModified && prop.Status != NotamProposalStatusCode.Disseminated) return false;
            if (!prop.Estimated && prop.EndValidity.HasValue && prop.EndValidity < DateTime.UtcNow) return true;
            return false;
        }

        static bool IsSoonToExpire(Proposal prop, int min)
        {
            var retValue = false;
            if (!IsObsoleteProposal(prop) && prop.Estimated)
            {
                var DateToExpire = DateTime.UtcNow.AddMinutes(min);
                if (prop.Status != NotamProposalStatusCode.Cancelled && prop.Status != NotamProposalStatusCode.Deleted &&
                    prop.Status != NotamProposalStatusCode.Discarded && prop.Status != NotamProposalStatusCode.ModifiedCancelled &&
                    prop.Status != NotamProposalStatusCode.Rejected && prop.Status != NotamProposalStatusCode.Replaced &&
                    prop.Status != NotamProposalStatusCode.Terminated && !prop.SeriesChecklist && !prop.Delete &&
                    (prop.EndValidity.HasValue && prop.EndValidity.Value < DateToExpire)) retValue = true;
            }
            return retValue;
        }

        private int GetMinutesToExpire()
        {
            var ctx = AppDbContext;
            if (ctx == null)
                throw new NullReferenceException("Unable to cast DbContext to AppDbContext");
            var ConfigValue = ctx.ConfigurationValues.FirstOrDefault(c => c.Category == "NOTAM" && c.Name == "NOTAMESTExpireTime");
            int Minutes;
            if (ConfigValue == null || !int.TryParse(ConfigValue.Value, out Minutes)) Minutes = CommonDefinitions.DefaultExpireMinutes;
            return Minutes;
        }

        private List<ProposalDto> GenerateProposalDto(List<Proposal> proposals, bool fromFilter = false)
        {
            var dtos = new List<ProposalDto>();
            var culture = new CultureInfo("en-US");
            var Minutes = GetMinutesToExpire();

            foreach (var proposal in proposals)
            {
                dtos.Add(new ProposalDto
                {
                    Id = proposal.Id,
                    NotamId = proposal.NotamId,
                    CategoryId = proposal.CategoryId,
                    CategoryName = proposal.Category.Name,
                    ItemA = proposal.ItemA,
                    SiteId = proposal.ItemA == "CXXX" ? proposal.ItemE.Substring(0, 4) : "",
                    IcaoSubjectDesc = proposal.IcaoSubject.Name,
                    IcaoConditionDesc = proposal.IcaoSubjectCondition.Description,
                    StartActivity = (proposal.StartActivity.HasValue) ? proposal.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (proposal.EndValidity.HasValue) ? proposal.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((proposal.ProposalType == NotamType.C) ? "" : "PERM"),
                    Estimated = proposal.Estimated,
                    Received = proposal.Received,
                    ProposalType = proposal.ProposalType,
                    Status = proposal.Status,
                    GroupId = proposal.GroupId.HasValue ? proposal.GroupId.ToString() : null,
                    SeriesChecklist = proposal.SeriesChecklist,
                    IsObsolete = IsObsoleteProposal(proposal),
                    Originator = proposal.Originator,
                    ModifiedByUsr = proposal.ModifiedByUsr,
                    Operator = proposal.Operator,
                    SoonToExpire = IsSoonToExpire(proposal, Minutes),
                    ItemE = proposal.ItemE,
                    PendingReview = proposal.PendingReview
                });
            }
            return dtos;
        }

        private async Task<bool> ShouldBeMarkedAsPendingReview(int proposalId, int orgId)
        {
            var ctx = DbContext as AppDbContext;
            var acknowledgements = await ctx.ProposalAcknowledgements.Where(e => e.ProposalId == proposalId).ToListAsync();

            return acknowledgements.Count > 0 ? acknowledgements.Count(e => e.OrganizationId == orgId && !e.Acknowledge) >= 1 : false;
        }
    }
}