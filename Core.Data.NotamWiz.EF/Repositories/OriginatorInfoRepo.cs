﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class OriginatorInfoRepo : Repository<OriginatorInfo>, IOriginatorInfoRepo
    {
        public Task<OriginatorInfo> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(r => r.Id == id);
        }


        public OriginatorInfoRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<List<OriginatorInfoDto>> FilterAsync(string match, int pageSize, int orgId)
        {
            return DbSet.Where(r => r.OrganizationId == orgId && r.Fullname.Contains(match))
                                   .OrderBy(r => r.Fullname)
                                   .Take(pageSize)
                                   .Select( e => new OriginatorInfoDto
                                   {
                                       Id = e.Id,
                                       Text = e.Fullname,
                                       Phone = e.Phone,
                                       Email = e.Email,
                                       OrganizationId = e.OrganizationId
                                   })
                                   .ToListAsync();

        }

        public Task<OriginatorInfo> FindByFullNameAsync(string fullname)
        {
            return DbSet.FirstOrDefaultAsync(r => r.Fullname == fullname);
        }

        public Task<OriginatorInfo> FindAsync(string fullname, int orgId)
        {
            return DbSet.FirstOrDefaultAsync(r => r.OrganizationId == orgId && r.Fullname == fullname);
        }

    }
}
