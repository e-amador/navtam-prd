﻿using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Enums;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{

    public class DoaRepo : Repository<Doa>, IDoaRepo
    {
        public DoaRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<Doa> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Doa> GetByIdWithFiltersAsync(int id)
        {
            //return await DbSet.Include(g => g.DoaFilters).SingleOrDefaultAsync(d => d.Id == id);
            return await DbSet.SingleOrDefaultAsync(d => d.Id == id);
        }

        public async Task<bool> Exists(string name)
        {
            return await DbSet.AnyAsync(d => d.Name == name);
        }

        public async Task<bool> Exists(int id, string name)
        {
            return await DbSet.AnyAsync(d => d.Name == name && d.Id != id);
        }

        public async Task<int> GetAllCountAsync()
        {
            return await DbSet.Where(e => e.DoaType == RegionType.Doa ).CountAsync();
        }

        public async Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            return await DbSet
                .Where(e => e.DoaType == RegionType.Doa && 
                    (e.Name.Contains(queryOptions.FilterValue) || e.Description.Contains(queryOptions.FilterValue)))
                .CountAsync();
        }

        public Task<List<Doa>> GetByDoaIdsAsync(IEnumerable<string> doaIds)
        {
            return DbSet.Where(e => doaIds.Contains(e.Id.ToString())).ToListAsync();
        }

        public Doa GetBilingualRegion()
        {
            return DbSet.FirstOrDefault(e => e.DoaType == RegionType.Bilingual);
        }

        public Task<Doa> GetBilingualRegionAsync()
        {
            return DbSet.FirstOrDefaultAsync(e => e.DoaType == RegionType.Bilingual);
        }

        public Doa GetNorthernRegion()
        {
            return DbSet.FirstOrDefault(e => e.DoaType == RegionType.Northern);
        }

        public Task<Doa> GetNorthernRegionAsync()
        {
            return DbSet.FirstOrDefaultAsync(e => e.DoaType == RegionType.Northern);
        }

        public Doa GetNofDoa()
        {
            return DbSet.FirstOrDefault(e => e.DoaType == RegionType.Doa && string.Compare(e.Name,"NOF",true) == 0);
        }

        public Task<Doa> GetNofDoaAsync()
        {
            return DbSet.FirstOrDefaultAsync(e => e.DoaType == RegionType.Doa && string.Compare(e.Name, "NOF", true) == 0);
        }

        public Task<List<Doa>> GetDoasInLocationAsync(DbGeography doaGeoArea)
        {
            return DbSet.Where(e=> e.DoaGeoArea.Intersects(doaGeoArea) ).ToListAsync();
        }
    }
}
