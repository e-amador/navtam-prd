﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class NdsClientSeriesRepo : Repository<NdsClientSeries>, INdsClientSeriesRepo
    {
        public NdsClientSeriesRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<NdsClientSeries>> GetByClientIdAsync(int clientId)
        {
            return await DbSet.Where(e => e.NdsClientId == clientId).ToListAsync();
        }
    }
}
