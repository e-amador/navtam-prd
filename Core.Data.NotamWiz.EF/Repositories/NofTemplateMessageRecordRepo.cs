﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class NofTemplateMessageRecordRepo : Repository<NofTemplateMessageRecord>, INofTemplateMessageRecordRepo
    {
        public NofTemplateMessageRecordRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<NofTemplateMessageRecordDto> GetBySlotAndUsernameAsync(int slotNumber, string username)
        {
            var template =  await DbSet
                .Include(e=> e.Template)
                .SingleOrDefaultAsync(e =>
                    e.Username == username &&
                    e.SlotNumber == slotNumber);

            return CreateTemplateDto(template);
        }

        public async Task<List<NofTemplateMessageRecordDto>> GetAllByUsernameAsync(string username)
        {
            var templates = await DbSet
                .Include(e=> e.Template)
                .Where(e => e.Username == username )
                .OrderBy(e=> e.SlotNumber)
                .ToListAsync();

            var result = new List<NofTemplateMessageRecordDto>();
            foreach(var template in templates)
                result.Add(CreateTemplateDto(template));

            return result;
        }

        private NofTemplateMessageRecordDto CreateTemplateDto(NofTemplateMessageRecord template)
        {
            if (template == null)
                return null;

            return new NofTemplateMessageRecordDto
            {
                Id = template.Id,
                SlotNumber = template.SlotNumber,
                Username = template.Username,
                TemplateId = template.TemplateId,
                TemplateName = template.Template.Name
            };
        }
    }
}
