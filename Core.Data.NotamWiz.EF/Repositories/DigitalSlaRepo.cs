﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class DigitalSlaRepo : Repository<DigitalSla>, IDigitalSlaRepo
    {
        public DigitalSlaRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<DigitalSla> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e=> e.Id == id);
        }

        public Task<DigitalSla> GetLastVersionAsync()
        {
            return DbSet.OrderByDescending(e => e.Version).FirstOrDefaultAsync();
        }

        public DigitalSla GetLastVersion()
        {
            return DbSet.OrderByDescending(e => e.Version).FirstOrDefault();
        }
    }
}
