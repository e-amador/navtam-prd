﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ProposalAnnotationRepo : Repository<ProposalAnnotation>, IProposalAnnotationRepo
    {
        public ProposalAnnotationRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<ProposalAnnotation> GetByIdAsync(int id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<List<ProposalAnnotation>> GetByProposalIdAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderBy(e => e.Received)
                .ToListAsync();
        }

        public async Task<List<ProposalAnnotation>> GetByProposalIdDescAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderByDescending(e => e.Received)
                .ToListAsync();
        }
    }
}
