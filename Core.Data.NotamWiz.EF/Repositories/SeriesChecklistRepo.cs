﻿using System.Data.Entity;
using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Threading.Tasks;
using NavCanada.Core.Common.Common;
using System.Linq;
using System.Collections.Generic;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class SeriesChecklistRepo : Repository<SeriesChecklist>, ISeriesChecklistRepo
    {
        public SeriesChecklistRepo(DbContext dbContext) : base(dbContext)
        {
        }
        public Task<SeriesChecklist> GetByIdAsync(string serie)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Series == serie);
        }

        public async Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            return await DbSet.Where(e => e.Series.Contains(filter) || e.Firs.Contains(filter)).CountAsync();
        }
        public async Task<List<SeriesChecklist>> GetAllAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;

            var series = await DbSet
                .Where(e => e.Series.Contains(filter) || e.Firs.Contains(filter))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return series;
        }

        public async Task<string> GetSeriesInUseAsync()
        {
            var res = "";
            await DbSet.ForEachAsync( s => res += s.Series.Trim());
            return res;
        }

        public List<string> GetAvailableSeries()
        {
            return DbSet.Select(e => e.Series).Distinct().OrderBy(e => e).ToList();
        }

        public async Task<bool> Exists(string name)
        {
            return await DbSet.AnyAsync(d => d.Series == name);
        }

    }
}
