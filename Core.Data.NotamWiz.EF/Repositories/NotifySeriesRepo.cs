﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity;

namespace NavCanada.Core.Data.EF.Repositories
{
    class NotifySeriesRepo : Repository<NotifySeries>, INotifySeriesRepo
    {
        public NotifySeriesRepo(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
