﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.EF.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ProposalHistoryRepo : Repository<ProposalHistory>, IProposalHistoryRepo
    {
        public ProposalHistoryRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<ProposalHistory> GetByIdAsync(int id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<ProposalHistory> GetLatestByProposalIdAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderByDescending(e => e.Id)
                .FirstOrDefaultAsync();
        }

        public async Task<ProposalHistory> GetByProposalIdAndNotamIdAsync(string notamId, int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId && e.NotamId == notamId)
                .OrderByDescending(e => e.Id)
                .FirstOrDefaultAsync();
        }

        public async Task<List<ProposalHistoryDto>> GetByProposalIdPartialAsync(int proposalId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.ProposalId == proposalId)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalHistory>> GetByProposalIdAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderBy(e => e.Id)
                .ToListAsync();
        }

        public async Task<ProposalHistory> GetModifyByUserAsync(int proposalId)
        {
            return await DbSet
                .OrderByDescending(e => e.Id)
                .Skip(1) //skip the latest proposal inserted before comitting the transaction
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Submitted ||
                        e.Status == NotamProposalStatusCode.Disseminated ||
                        e.Status == NotamProposalStatusCode.DisseminatedModified
                     )
                 );
        }

        public async Task<ProposalHistory> GetLastSubmittedProposalAsync(int proposalId)
        {
            return await DbSet
                .OrderByDescending(e => e.Id)
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Submitted ||
                        e.Status == NotamProposalStatusCode.ModifiedAndSubmitted
                     )
                 );
        }

        public async Task<ProposalHistory> GetLastParkedProposalAsync(int proposalId)
        {
            return await DbSet
                .OrderBy(e => e.Id)
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Parked ||
                        e.Status == NotamProposalStatusCode.ParkedDraft
                     )
                 );
        }

        public async Task<ProposalHistory> GetLastDisseminatedProposalAsync(int proposalId)
        {
            return await DbSet
                .OrderBy(e => e.Id)
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Disseminated ||
                        e.Status == NotamProposalStatusCode.DisseminatedModified
                     )
                 );
        }

        public async Task<int> GetCountBySeries(string series)
        {
            return await DbSet.CountAsync(p => p.Series == series);
        }

        public async Task<List<ProposalHistory>> GetByProposalIdDescAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderByDescending(e => e.Id)
                .ToListAsync();
        }

        public async Task<ProposalHistory> GetLatestByStatusProposalIdAsync(int proposalId, NotamProposalStatusCode status)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId && e.Status == status)
                .OrderByDescending(e => e.Id)
                .FirstOrDefaultAsync();
        }

        public async Task<List<ProposalHistoryDto>> FilterAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<ProposalHistory> filterQuery)
        {
            List<ProposalHistory> proposalHistories = null;

            var ctx = DbContext as AppDbContext;
            ctx.Database.CommandTimeout = 120;

            if (orgType == OrganizationType.External)
            {
                proposalHistories = await filterQuery
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        (
                            e.Status == NotamProposalStatusCode.Draft ||
                            e.Status == NotamProposalStatusCode.Discarded ||
                            e.Status == NotamProposalStatusCode.Disseminated ||
                            e.Status == NotamProposalStatusCode.DisseminatedModified ||
                            e.Status == NotamProposalStatusCode.Replaced ||
                            e.Status == NotamProposalStatusCode.Terminated ||
                            e.Status == NotamProposalStatusCode.Submitted ||
                            e.Status == NotamProposalStatusCode.Withdrawn ||
                            e.Status == NotamProposalStatusCode.WithdrawnPending ||
                            e.Status == NotamProposalStatusCode.Rejected ||
                            e.Status == NotamProposalStatusCode.SoontoExpire
                        ) &&
                        e.EffectArea != null && region.Intersects(e.EffectArea))
                    .ApplySort(filterCriteria.QueryOptions.Sort)
                    .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                    .Take(filterCriteria.QueryOptions.PageSize)
                    .ToListAsync();
            }
            else
            {
                proposalHistories = await filterQuery
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        //(e.OrganizationType != OrganizationType.Nof) &&
                        !e.Grouped &&
<<<<<<< HEAD
                        e.Location != null &&
=======
                        !e.Delete &&
                        e.EffectArea != null &&
>>>>>>> f4e06108050342a2ce25992ce3872adeae07d194
                        (
                            e.Status == NotamProposalStatusCode.Draft ||
                            e.Status == NotamProposalStatusCode.Discarded ||
                            e.Status == NotamProposalStatusCode.Disseminated ||
                            e.Status == NotamProposalStatusCode.DisseminatedModified ||
                            e.Status == NotamProposalStatusCode.Replaced ||
                            e.Status == NotamProposalStatusCode.Terminated ||
                            e.Status == NotamProposalStatusCode.Submitted ||
                            e.Status == NotamProposalStatusCode.Withdrawn ||
                            e.Status == NotamProposalStatusCode.WithdrawnPending ||
                            e.Status == NotamProposalStatusCode.Rejected ||
                            e.Status == NotamProposalStatusCode.SoontoExpire
                        ) &&
                        region.Intersects(e.EffectArea))
                    .ApplySort(filterCriteria.QueryOptions.Sort)
                    .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                    .Take(filterCriteria.QueryOptions.PageSize)
                    .ToListAsync();
            }

            var result = GenerateProposalDto(proposalHistories);
            if (result.Count > 0 )
            {
                var proposalIds = result.Select(e => e.ProposalId).ToList();
                var proposals = await ctx.Proposals
                    .Where(e => proposalIds.Contains(e.Id))
                    .Select( e=> new 
                        { 
                            e.Id,
                            e.ProposalType,
                            e.RowVersion,
                            e.PendingReview
                    })
                    .ToListAsync();

                foreach (var p in proposals)
                {
                    if( p.RowVersion != null )
                    {
                        var matches = result.Where(e => e.ProposalId == p.Id).ToList();
                        foreach (var match in matches)
                        {
                            var lastInHistory = match.RowVersion != null ? p.RowVersion.SequenceEqual(match.RowVersion) : false;
                            match.LastInHistory = lastInHistory;
                            match.SoonToExpire = lastInHistory ? match.SoonToExpire : false;
                            match.RowVersion = null;
                            match.PendingReview = false;

                            if (lastInHistory && p.ProposalType != NotamType.C && p.PendingReview)
                            {
                                var acknowledgements = await ctx.ProposalAcknowledgements.Where(e => e.ProposalId == p.Id).ToListAsync();
                                if (acknowledgements.Count > 0)
                                {
                                    match.PendingReview = acknowledgements.Count(e => e.OrganizationId == orgId && e.Acknowledge == true) != 1;
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public async Task<int> FilterCountAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<ProposalHistory> filterQuery)
        {
            var ctx = DbContext as AppDbContext;
            ctx.Database.CommandTimeout = 120;

            if (orgType == OrganizationType.External)
            {
                return await filterQuery.CountAsync(e =>
                                (e.OrganizationId == orgId) &&
                                !e.Grouped &&
<<<<<<< HEAD
=======
                                !e.Delete &&
>>>>>>> f4e06108050342a2ce25992ce3872adeae07d194
                                (
                                    e.Status == NotamProposalStatusCode.Draft ||
                                    e.Status == NotamProposalStatusCode.Discarded ||
                                    e.Status == NotamProposalStatusCode.Disseminated ||
                                    e.Status == NotamProposalStatusCode.DisseminatedModified ||
                                    e.Status == NotamProposalStatusCode.Replaced ||
                                    e.Status == NotamProposalStatusCode.Terminated ||
                                    e.Status == NotamProposalStatusCode.Submitted ||
                                    e.Status == NotamProposalStatusCode.Withdrawn ||
                                    e.Status == NotamProposalStatusCode.WithdrawnPending ||
                                    e.Status == NotamProposalStatusCode.Rejected ||
                                    e.Status == NotamProposalStatusCode.SoontoExpire
                                ) &&
<<<<<<< HEAD
                                e.Location != null && region.Intersects(e.Location));
=======
                                e.EffectArea != null && region.Intersects(e.EffectArea));
>>>>>>> f4e06108050342a2ce25992ce3872adeae07d194
            }

            return await filterQuery.CountAsync(e =>
                        //(e.OrganizationType != OrganizationType.Nof) &&
                        !e.Grouped &&
                        (
                            e.Status == NotamProposalStatusCode.Draft ||
                            e.Status == NotamProposalStatusCode.Discarded ||
                            e.Status == NotamProposalStatusCode.Disseminated ||
                            e.Status == NotamProposalStatusCode.DisseminatedModified ||
                            e.Status == NotamProposalStatusCode.Replaced ||
                            e.Status == NotamProposalStatusCode.Terminated ||
                            e.Status == NotamProposalStatusCode.Submitted ||
                            e.Status == NotamProposalStatusCode.Withdrawn ||
                            e.Status == NotamProposalStatusCode.WithdrawnPending ||
                            e.Status == NotamProposalStatusCode.Rejected ||
                            e.Status == NotamProposalStatusCode.SoontoExpire
                        ) &&
                        e.EffectArea != null && region.Intersects(e.EffectArea));
        }

        public IQueryable<ProposalHistory> PrepareQueryFilter(FilterCriteria filterCriteria)
        {
            var expressions = new List<Expression<Func<ProposalHistory, bool>>>();
            IQueryable<ProposalHistory> query = DbSet;

            foreach (var cond in filterCriteria.Conditions)
            {
                var endValidityFilter = false;
                var field = cond.Field.ToUpper();
                var value = cond.Value.ToString().ToUpper();
                var condition = cond.Value.ToString().ToUpper();
                var oper = cond.Operator;
                switch (field)
                {
                    case "PROPOSALTYPE":
                        cond.Value = (int)Enum.Parse(typeof(NotamType), value);
                        break;
                    case "LOWERLIMIT":
                    case "UPPERLIMIT":
                    case "RADIUS":
                        int number;
                        if (int.TryParse(value, out number))
                        {
                            cond.Value = number;
                        }
                        else
                        {
                            cond.Value = 0;
                        }
                        break;
                    case "RECEIVED":
                        DateTime published;
                        if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out published))
                        {
                            cond.Value = value;
                        }
                        else
                        {
                            cond.Value = DateTime.Now;
                        }
                        break;
                    case "STARTACTIVITY":
                        if (condition == "IMMEDIATE" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime startActivity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out startActivity))
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                    case "ENDVALIDITY":
                        endValidityFilter = true; // cond.Operator == "GTE" || cond.Operator == "GT";
                        if (condition == "PERM" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime endValidity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out endValidity))
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                }

                var expression = BuildWhereExpression(cond.Field, cond.Value, cond.Operator);
                if (endValidityFilter)
                {
                    if (endValidityFilter) //&& (oper == "EQ" || oper == "GT" || oper == "GTE")
                    {
                        var otherExpression = BuildWhereExpression("Permanent", true, "EQ");
                        expression = expression.Or(otherExpression);
                    }
                }

                query = query.Where(expression);
            }

            return query;
        }

        private int GetMinutesToExpire()
        {
            var ctx = AppDbContext;
            if (ctx == null)
                throw new NullReferenceException("Unable to cast DbContext to AppDbContext");
            var ConfigValue = ctx.ConfigurationValues.FirstOrDefault(c => c.Category == "NOTAM" && c.Name == "NOTAMESTExpireTime");
            int Minutes;
            if (ConfigValue == null || !int.TryParse(ConfigValue.Value, out Minutes)) Minutes = CommonDefinitions.DefaultExpireMinutes;
            return Minutes;
        }

        static bool IsObsoleteProposal(ProposalHistory prop)
        {
            //if (prop.ProposalType == NotamType.C && prop.Status != NotamProposalStatusCode.Rejected) return true;
            if (prop.ProposalType == NotamType.C ) return true;
            if (prop.Status == NotamProposalStatusCode.Expired) return true;
            if (prop.Status != NotamProposalStatusCode.DisseminatedModified && prop.Status != NotamProposalStatusCode.Disseminated) return false;
            if (!prop.Estimated && prop.EndValidity.HasValue && prop.EndValidity < DateTime.UtcNow) return true;
            return false;
        }

        static bool IsSoonToExpire(ProposalHistory prop, int min, bool isObsolete)
        {
            var retValue = false;
            if (!isObsolete && prop.Estimated)
            {
                var DateToExpire = DateTime.UtcNow.AddMinutes(min);
                if (prop.Status != NotamProposalStatusCode.Cancelled && prop.Status != NotamProposalStatusCode.Deleted &&
                    prop.Status != NotamProposalStatusCode.Discarded && prop.Status != NotamProposalStatusCode.ModifiedCancelled &&
                    prop.Status != NotamProposalStatusCode.Rejected && prop.Status != NotamProposalStatusCode.Replaced &&
                    prop.Status != NotamProposalStatusCode.Terminated && !prop.Delete &&
                    (prop.EndValidity.HasValue && prop.EndValidity.Value < DateToExpire)) retValue = true;
            }
            return retValue;
        }

        private List<ProposalHistoryDto> GenerateProposalDto(List<ProposalHistory> proposalHistories)
        {
            var dtos = new List<ProposalHistoryDto>();
            var culture = new CultureInfo("en-US");
            var Minutes = GetMinutesToExpire();

            foreach (var proposalHistory in proposalHistories)
            {
                var isObsolete = IsObsoleteProposal(proposalHistory);
                dtos.Add(new ProposalHistoryDto
                {
                    Id = proposalHistory.Id,
                    ProposalId = proposalHistory.ProposalId,
                    NotamId = proposalHistory.NotamId,
                    CategoryId = proposalHistory.CategoryId,
                    CategoryName = proposalHistory.Category.Name,
                    ItemA = proposalHistory.ItemA,
                    SiteId = proposalHistory.ItemA == "CXXX" ? proposalHistory.ItemE.Substring(0, 4) : "",
                    IcaoSubjectDesc = proposalHistory.IcaoSubject.Name,
                    IcaoConditionDesc = proposalHistory.IcaoSubjectCondition.Description,
                    StartActivity = (proposalHistory.StartActivity.HasValue) ? proposalHistory.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (proposalHistory.EndValidity.HasValue) ? proposalHistory.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((proposalHistory.ProposalType == NotamType.C) ? "" : "PERM"),
                    Estimated = proposalHistory.Estimated,
                    Received = proposalHistory.Received,
                    ProposalType = proposalHistory.ProposalType,
                    Status = proposalHistory.Status,
                    GroupId = proposalHistory.GroupId.HasValue ? proposalHistory.GroupId.ToString() : null,
                    SeriesChecklist = false,
                    IsObsolete = isObsolete,
                    Originator = proposalHistory.Originator,
                    ModifiedByUsr = proposalHistory.ModifiedByUsr,
                    Operator = proposalHistory.Operator,
                    SoonToExpire = IsSoonToExpire(proposalHistory, Minutes, isObsolete),
                    ItemE = proposalHistory.ItemE,
                    PendingReview = proposalHistory.PendingReview,
                    RowVersion = proposalHistory.RowVersion
                });
            }
            return dtos;
        }
    }
}