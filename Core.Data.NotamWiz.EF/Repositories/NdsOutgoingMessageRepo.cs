﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class NdsOutgoingMessageRepo : Repository<NdsOutgoingMessage>, INdsOutgoingMessageRepo
    {
        public NdsOutgoingMessageRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public int GetCount() => DbSet.Count();
     
        public List<NdsOutgoingMessage> GetLatestMessages(int max) => DbSet.OrderByDescending(e => e.SubmitDate).Take(max).ToList();
    }
}
