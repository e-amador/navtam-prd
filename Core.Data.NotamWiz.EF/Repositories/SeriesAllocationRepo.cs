﻿using System.Collections.Generic;
using System.Linq;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Threading.Tasks;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using Domain.Model.Dtos;
    using Common.Common;
    using Extensions;

    public class SeriesAllocationRepo : Repository<SeriesAllocation>, ISeriesAllocationRepo
    {
        public SeriesAllocationRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<SeriesAllocation> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Id == id);
        }

        public Task<SeriesAllocation> GetByIdWithRegionAsync(int id)
        {
            return DbSet.Include("Region").FirstOrDefaultAsync(e => e.Id == id);
        }


        public async Task<List<SeriesAllocation>> GetByRegionIdAndCodeAndCategoryIdAsync(int regionId, string code, DisseminationCategory category )
        {
            return await DbSet.Where(e => 
                e.RegionId == regionId &&
                e.QCode == code &&
                e.DisseminationCategory == category)
                .ToListAsync();
        }

        public async Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            return await DbSet.Where(e => e.Series.Contains(filter) || e.QCode.Contains(filter)).CountAsync();
        }

        public async Task<List<SerieAllocationDto>> GetAllAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;

            var series = await DbSet
                .Where(e => e.Series.Contains(filter) || e.QCode.Contains(filter))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .Include("Region")
                .ToListAsync();

            var dtos = new List<SerieAllocationDto>();

            foreach (var serie in series)
            {
                dtos.Add(new SerieAllocationDto
                {
                    Id = serie.Id,
                    RegionId = serie.RegionId,
                    RegionName = serie.Region.Name,
                    DisseminationCategory = serie.DisseminationCategory,
                    DisseminationCategoryName = serie.DisseminationCategory.ToString(),
                    QCode = serie.QCode,
                    Series = serie.Series,
                    Subject = serie.Subject
                });
            }
            return dtos;
        }
    }
}

