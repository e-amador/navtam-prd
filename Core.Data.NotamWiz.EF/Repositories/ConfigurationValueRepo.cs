﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Linq;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using System.Threading.Tasks;
    using Common.Common;
    using System.Collections.Generic;
    using Extensions;

    public class ConfigurationValueRepo : Repository<ConfigurationValue>, IConfigurationValueRepo
    {
        public ConfigurationValueRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public IQueryable<ConfigurationValue> GetByCategory(string category)
        {
            return DbSet.Where(c => c.Category == category);
        }

        public ConfigurationValue GetByName(string name)
        {
            return DbSet.SingleOrDefault(e => e.Name == name);
        }


        public async Task<ConfigurationValue> GetByNameAsync(string name)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Name == name);
        }

        public ConfigurationValue GetByCategoryandName(string category,string name)
        {
            return DbSet.SingleOrDefault(e =>  e.Category == category && e.Name == name);
        }

        public async Task<List<ConfigurationValue>> GetByCategoryAsync(string category)
        {
            return await DbSet.Where(e => e.Category == category).ToListAsync();
        }

        public async Task<ConfigurationValue> GetByCategoryandNameAsync(string category, string name)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Category == category && e.Name == name);
        }

        public async Task<string> GetValueAsync(string category, string name)
        {
            var configValue = await DbSet.SingleOrDefaultAsync(e => e.Category == category && e.Name == name);
            return configValue?.Value ?? string.Empty;
        }

        public async Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            return await DbSet
                .Where(e => e.Name.Contains(queryOptions.FilterValue) || 
                    e.Description.Contains(queryOptions.FilterValue) || 
                    e.Value.Contains(queryOptions.FilterValue) ||
                    e.Category.Contains(queryOptions.FilterValue))
                .CountAsync();
        }

        public async Task<List<ConfigurationValue>> GetConfigValuesPartials(QueryOptions queryOptions)
        {
            var configValues = await DbSet
                .Where(e => e.Name.Contains(queryOptions.FilterValue) ||
                    e.Description.Contains(queryOptions.FilterValue) ||
                    e.Value.Contains(queryOptions.FilterValue) ||
                    e.Category.Contains(queryOptions.FilterValue))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return configValues;
        }

        public async Task<bool> Exists(string name)
        {
            return await DbSet.AnyAsync(d => d.Name == name);
        }

    }
}
