﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Linq;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using Domain.Model.Enums;

    public class NotificationTemplateRepo : Repository<NotificationTemplate>, INotificationTemplateRepo
    {
        public NotificationTemplateRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public NotificationTemplate GetByTypeAndTemplateType(TemplateType templateType, NotificationType notificationType)
        {
            return DbSet.SingleOrDefault(e => e.TemplateType == templateType && e.NotificationType == notificationType);
        }
    }
}
