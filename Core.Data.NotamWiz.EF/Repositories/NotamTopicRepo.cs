﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    class NotamTopicRepo : Repository<NotamTopic>, INotamTopicRepo
    {
        public NotamTopicRepo(DbContext dbContext) : base(dbContext)
        {

        }
    }
}
