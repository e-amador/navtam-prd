﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using System;
using NavCanada.Core.Domain.Model.Dtos;
using System.Data.Entity.Spatial;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using Newtonsoft.Json;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class SdoCacheRepo : Repository<SdoCache>, ISdoCacheRepo
    {
        public SdoCacheRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<SdoCache> GetByIdAsync(string id)
        {
            return !string.IsNullOrEmpty(id) ? await DbSet.FirstOrDefaultAsync(e => e.Id == id) : null;
        }

        public List<SdoCache> GetAllByType(SdoEntityType type)
        {
            return DbSet.Where(e => !e.Deleted && e.Type == type).ToList();
        }

        public List<SdoCache> GetAllByTypeIncludingDeleted(SdoEntityType type)
        {
            return DbSet.Where(e => e.Type == type).ToList();
        }

        public Task<List<SdoCache>> GetAllByTypeAsync(SdoEntityType type)
        {
            return DbSet.Where(e => !e.Deleted && e.Type == type).ToListAsync();
        }

        public Task<List<SdoCache>> GetChildrenByTypeAsync(string parentId, SdoEntityType childType)
        {
            return DbSet.Where(e => !e.Deleted && e.ParentId == parentId && e.Type == childType).ToListAsync();
        }

        public Task<SdoCache> GetByDesignatorAsync(string designator)
        {
            return DbSet.Where(e => e.Designator == designator).FirstOrDefaultAsync();
        }

        public List<SdoCache> GetFirs()
        {
            return GetAllByType(SdoEntityType.Fir).ToList();
        }

        public Task<List<SdoCache>> GetFirsAsync()
        {
            return GetAllByTypeAsync(SdoEntityType.Fir);
        }

        public SdoCache GetFirContaining(DbGeography geoLoc)
        {
            return DbSet.Where(e => e.Type == SdoEntityType.Fir && e.Geography != null && geoLoc.Intersects(e.Geography)).FirstOrDefault();
        }

        public bool FirExists(string designator)
        {
            return DbSet.Count(e => !e.Deleted && e.Type == SdoEntityType.Fir && e.Designator == designator) > 0; 
        }

        public DbGeography GetFirGeography(string designator)
        {
            return DbSet.Where(e => !e.Deleted && e.Type == SdoEntityType.Fir && e.Designator == designator).Select(e => e.Geography).FirstOrDefault();
        }

        public Task<SdoCache> GetFirByDesignatorAsync(string designator)
        {
            return DbSet.Where(e => !e.Deleted && e.Type == SdoEntityType.Fir && e.Designator == designator).FirstOrDefaultAsync();
        }

        public async Task<List<KeyValuePair<string, string>>> GetAerodromesAndFirsAsync()
        {
            var aerodromesAndFirs = await DbSet.Where(e => !e.Deleted && 
                    (e.Type == SdoEntityType.Aerodrome || e.Type == SdoEntityType.Fir) &&
                    e.Designator.Length == 4
                    ).ToListAsync();

            var result = new List<KeyValuePair<string, string>>(aerodromesAndFirs.Count);
            foreach (var item in aerodromesAndFirs)
            {
                result.Add(new KeyValuePair<string, string>(SdoEntityTypeToString(item.Type), item.Designator));
            }

            return result;
        }

        public async Task<List<SdoSubjectDto>> FilterAerodromesInArea(string match, int pageSize, DbGeography geoLoc)
        {
            var query = await DbSet.Where(r => !r.Deleted && r.ParentId == null &&
                                                r.Type == SdoEntityType.Aerodrome &&
                                                r.Keywords.Contains(match) &&
                                                r.Designator.Length == 4 &&
                                                r.RefPoint.Intersects(geoLoc))
                                   .OrderBy(r => r.Name)
                                   .ThenBy(r => r.Designator)
                                   .Take(pageSize)
                                   .ToListAsync();

            return ConvertToSubjectDtos(query);
        }

        public async Task<List<SdoSubjectDto>> FilterAerodromesOutsideArea(string match, int pageSize, DbGeography geoLoc)
        {
            var query = await DbSet.Where(r => !r.Deleted && r.ParentId == null &&
                                                r.Type == SdoEntityType.Aerodrome &&
                                                r.Keywords.Contains(match) &&
                                                r.Designator.Length == 4 &&
                                               !r.RefPoint.Intersects(geoLoc))
                                   .OrderBy(r => r.Name)
                                   .ThenBy(r => r.Designator)
                                   .Take(pageSize)
                                   .ToListAsync();

            return ConvertToSubjectDtos(query);
        }

        public async Task<List<SdoSubjectDto>> FilterCatchAllSubjectsAsync(string match, int pageSize, DbGeography geoLoc)
        {
            var query = await DbSet.Where(r => !r.Deleted && r.ParentId == null &&
                                                r.RefPoint.Intersects(geoLoc) &&
                                                (r.Type == SdoEntityType.Aerodrome || r.Type == SdoEntityType.Fir) &&
                                                r.Designator.Length == 4 &&
                                                r.Keywords.Contains(match))
                                   .OrderBy(r => r.Type)
                                   .ThenBy(r => r.Name)
                                   .ThenBy(r => r.Designator)
                                   .Take(pageSize)
                                   .ToListAsync();

            return ConvertToSubjectDtos(query);
        }

        public async Task<List<SdoSubjectDto>> FilterAerodromesWithRunwaysAsync(string match, int pageSize, DbGeography geoLoc)
        {
            var query = from ahp in Ctx.SdoCaches
                        join rwy in Ctx.SdoCaches on ahp.Id equals rwy.ParentId
                        where !ahp.Deleted && !rwy.Deleted &&
                               ahp.Type == SdoEntityType.Aerodrome && rwy.Type == SdoEntityType.Runway &&
                               ahp.Designator.Length == 4 &&
                               ahp.RefPoint.Intersects(geoLoc) && rwy.Designator != "RWY-W" && //filter waterdromes out
                               ahp.Keywords.Contains(match)
                        select ahp.Id;

            var uniqueIds = new HashSet<string>(query);

            var selected = await DbSet.Where(b => uniqueIds.Contains(b.Id))
                                      .OrderBy(r => r.ParentId)
                                      .ThenBy(r => r.Type)
                                      .ThenBy(r => r.Name)
                                      .ThenBy(r => r.Designator)
                                      .Take(pageSize)
                                      .ToListAsync();

            return ConvertToSubjectDtos(selected);
        }

        public async Task<List<SdoSubjectDto>> GetAerodromeRunwaysAsync(string ahpId)
        {
            var query = await DbSet.Where(e => !e.Deleted && e.ParentId == ahpId && e.Type == SdoEntityType.Runway).ToListAsync();
            return ConvertToSubjectDtos(query);
        }

        public async Task<List<SdoSubjectDto>> FilterAerodromesWithTaxiwaysAsync(string match, int pageSize, DbGeography geoLoc)
        {
            var query = from ahp in Ctx.SdoCaches
                        join twy in Ctx.SdoCaches on ahp.Id equals twy.ParentId
                        where !ahp.Deleted && !twy.Deleted &&
                               ahp.Type == SdoEntityType.Aerodrome && twy.Type == SdoEntityType.Taxiway &&
                               ahp.Designator.Length == 4 &&
                               ahp.RefPoint.Intersects(geoLoc) &&
                               ahp.Keywords.Contains(match)
                        select ahp.Id;

            var uniqueIds = new HashSet<string>(query);

            var selected = await DbSet.Where(b => uniqueIds.Contains(b.Id))
                                      .OrderBy(r => r.ParentId)
                                      .ThenBy(r => r.Type)
                                      .ThenBy(r => r.Name)
                                      .ThenBy(r => r.Designator)
                                      .Take(pageSize)
                                      .ToListAsync();

            return ConvertToSubjectDtos(selected);
        }

        public async Task<List<SdoSubjectDto>> GetAerodromeTaxiwaysAsync(string ahpId)
        {
            var query = await DbSet.Where(e => !e.Deleted && e.ParentId == ahpId && e.Type == SdoEntityType.Taxiway && e.Designator != "UNDESIGNATED").ToListAsync();
            return ConvertToSubjectDtos(query);
        }

        public List<SdoSubjectDto> GetAerodromeTaxiways(string ahpId)
        {
            var query = DbSet.Where(e => !e.Deleted && e.ParentId == ahpId && e.Type == SdoEntityType.Taxiway && e.Designator != "UNDESIGNATED").ToList();
            return ConvertToSubjectDtos(query);
        }

        public async Task<List<SdoSubjectDto>> FilterAerodromesWithCarsAsync(string match, DbGeography geoLoc)
        {
            var query = from ahp in Ctx.SdoCaches
                        join svc in Ctx.SdoCaches on ahp.Id equals svc.ParentId
                        where !ahp.Deleted && !svc.Deleted &&
                               ahp.Type == SdoEntityType.Aerodrome && svc.Type == SdoEntityType.Service &&
                               ahp.Designator.Length == 4 &&
                               ahp.RefPoint.Intersects(geoLoc) &&
                               svc.Designator == "CARS" &&
                               ahp.Keywords.Contains(match)
                        select ahp.Id;

            var uniqueIds = new HashSet<string>(query);

            var selected = await DbSet.Where(b => uniqueIds.Contains(b.Id)).ToListAsync();

            return ConvertToSubjectDtos(selected);
        }

        public async Task<int> GetTotalAerodromesWithouDisseminationCategoryAsync()
        {
            var total = await DbSet
                .Where(s => !s.Deleted && s.Type == SdoEntityType.Aerodrome && s.Designator.Length == 4 && !Ctx.AerodromeDisseminationCategories.Any(a => a.AhpMid == s.Id))
                .CountAsync();
            return total;
        }

        public async Task<int> GetTotalAerodromesWithouDisseminationCategoryAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            var total = await DbSet
                .Where(s => !s.Deleted && s.Type == SdoEntityType.Aerodrome && s.Designator.Length == 4 && s.Keywords.Contains(filter) && !Ctx.AerodromeDisseminationCategories.Any(a => a.AhpMid == s.Id))
                .CountAsync();
            return total;
        }

        public async Task<List<AerodromeWithoutDisseminationCategoryDto>> FilterAerodromesWithouDisseminationCategoryAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            var query = await DbSet.Where(s => !s.Deleted && s.Type == SdoEntityType.Aerodrome && s.Designator.Length == 4 && s.Keywords.Contains(filter) && !Ctx.AerodromeDisseminationCategories.Any(a => a.AhpMid == s.Id))
                                   .ApplySort(queryOptions.Sort)
                                   .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                                   .Take(queryOptions.PageSize)
                                   .ToListAsync();


            var subjects = new List<AerodromeWithoutDisseminationCategoryDto>();
            foreach (var elem in query)
            {
                if (elem.Designator.Length == 4) {
                    var attrObj = Deserialize<AerodromePartial>(elem.Attributes);

                    subjects.Add(new AerodromeWithoutDisseminationCategoryDto
                    {
                        Id = elem.Id,
                        Mid = elem.Id,
                        Designator = elem.Designator,
                        Name = elem.Name,
                        Fir = elem.Fir,
                        Latitude = (elem.RefPoint.Latitude.HasValue ? elem.RefPoint.Latitude.Value : 0),
                        Longitude = (elem.RefPoint.Longitude.HasValue ? elem.RefPoint.Longitude.Value : 0),
                        ServedCity = (attrObj != null) ? attrObj.ServedCity : "",
                    });
                }
            }

            return subjects;
        }



        public async Task<List<NearbyAerodromeDto>> GetNearbyAerodromesAsync(DbGeography dbGeo, double distance)
        {
            var region = dbGeo.Buffer(distance);
            var nearby = await DbSet.Where(s => !s.Deleted && 
                s.Type == SdoEntityType.Aerodrome && 
                s.Designator.Length == 4 &&
                region.Intersects(s.RefPoint)).ToListAsync();
            return nearby.Select(r => new NearbyAerodromeDto
            {
                CodeId = r.Designator,
                Distance = r.RefPoint.Distance(dbGeo) ?? 0.0,
                DisseminationCategory = GetAerodromeDisseminationCategory(r.Id),
                SdoCache = r
            }).ToList();
        }

        public async Task<List<NearbyAerodromeDto>> GetNearbyInternationalAerodromesAsync(DbGeography dbGeo, double distance)
        {
            var region = dbGeo.Buffer(distance);
            var nearby = await DbSet.Where(s => !s.Deleted &&
                s.Type == SdoEntityType.Aerodrome &&
                s.Designator.Length == 4 &&
                region.Intersects(s.RefPoint)
            ).ToListAsync();

            return nearby.Select(r => new NearbyAerodromeDto
            {
                CodeId = r.Designator,
                Distance = r.RefPoint.Distance(dbGeo) ?? 0.0,
                DisseminationCategory = GetAerodromeDisseminationCategory(r.Id),
                SdoCache = r
            }).Where(y=> y.DisseminationCategory == DisseminationCategory.International).ToList();
        }

        public bool IsWaterAerodrome(string ahpMid)
        {
            return DbSet.Count(e => e.ParentId == ahpMid && 
                                !e.Deleted && e.Type == SdoEntityType.Runway && 
                                e.Designator.StartsWith("RWY-W")) > 0;
        }

        public async Task UpdateKeywords()
        {
            var aerodromes = await DbSet.Where(s => !s.Deleted && 
                    (s.Type == SdoEntityType.Aerodrome || s.Type == SdoEntityType.Fir || s.Type == SdoEntityType.Runway || s.Type == SdoEntityType.Taxiway)
                    ).ToListAsync();
            foreach (var ahp in aerodromes)
            {
                var attrObj = Deserialize<AerodromePartial>(ahp.Attributes);
                var keywords = attrObj?.ServedCity;
                ahp.Keywords = $"{ahp.Designator ?? string.Empty}||{ahp.Name ?? string.Empty}||{keywords ?? string.Empty}";
            }
        }

        private DisseminationCategory GetAerodromeDisseminationCategory(string ahpId)
        {
            var cat = Ctx.AerodromeDisseminationCategories.FirstOrDefault(dc => dc.AhpMid == ahpId);
            return cat != null ? cat.DisseminationCategory : DisseminationCategory.National;
        }

        static List<SdoSubjectDto> ConvertToSubjectDtos(IEnumerable<SdoCache> query)
        {
            return query.Select(ahp => new SdoSubjectDto
            {
                Id = ahp.Id,
                Name = ahp.Name ?? string.Empty,//GetAerodromeName(ahp),
                Designator = ahp.Designator,
                SdoEntityName = SdoEntityTypeToString(ahp.Type),
                ParentId = ahp.ParentId,
                SubjectGeoRefPoint = ahp.RefPoint,
                SubjectGeo = null, //ahp.SubjectGeo,
                Suffix = "",
                ServedCity = ServedCity(ahp),
                TwyType = TwyType(ahp),
            }).ToList(); 
        }

        static string GetAerodromeName(SdoCache sdo)
        {
            var name = sdo.Name ?? string.Empty;
            if (sdo.Type != SdoEntityType.Aerodrome)
                return name;

            var aerodrome = Deserialize<AerodromePartial>(sdo.Attributes);
            var servedCity = aerodrome?.ServedCity ?? string.Empty;

            if (string.IsNullOrWhiteSpace(servedCity))
                return name;

            if (name.StartsWith($"{servedCity}/", StringComparison.OrdinalIgnoreCase))
                return name;

            return $"{servedCity}/{name}";
        }

        static string TwyType(SdoCache sdo)
        {
            if (sdo.Type != SdoEntityType.Taxiway) return "";
            var twy = Deserialize<TaxiwayPartial>(sdo.Attributes);
            return twy?.Type ?? string.Empty;
        }

        static string ServedCity(SdoCache sdo)
        {
            if (sdo.Type != SdoEntityType.Aerodrome) return "";
            var aerodrome = Deserialize<AerodromePartial>(sdo.Attributes);
            return aerodrome?.ServedCity ?? string.Empty;
        }

        public string GetServedCity(SdoCache sdo)
        {
            return ServedCity(sdo);
        }

        public DateTime? GetLatestEffectiveDate()
        {
            return DbSet.OrderByDescending(e => e.EffectiveDate).Take(1).FirstOrDefault()?.EffectiveDate;
        }

        static T Deserialize<T>(string json) => JsonConvert.DeserializeObject<T>(json);

        static string SdoEntityTypeToString(SdoEntityType type)
        {
            switch (type)
            {
                case SdoEntityType.Fir: return "Fir";
                case SdoEntityType.Aerodrome: return "Ahp";
                case SdoEntityType.Runway: return "Rwy";
                case SdoEntityType.Taxiway: return "Twy";
                default: return string.Empty;
            }
        }

        private AppDbContext Ctx
        {
            get
            {
                var ctx = DbContext as AppDbContext;
                if (ctx == null)
                    throw new NullReferenceException("AppDbContext");
                return ctx;
            }
        }
    }

    class AerodromePartial
    {
        public string ServedCity { get; set; }
    }

    class TaxiwayPartial
    {
        public string Type { get; set; }
        public string Width { get; set; }
        public string UOMWidth { get; set; }
    }
}