﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity;
using System.Linq;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class DisseminationJobRepo : Repository<DisseminationJob>, IDisseminationJobRepo
    {
        public DisseminationJobRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public DisseminationJob GetLatestJob()
        {
            return DbSet.OrderByDescending(e => e.Id).FirstOrDefault();
        }
    }
}
