﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class UserQueryFilterNameRepo : Repository<UserQueryFilterName>, IUserQueryFilterNameRepo
    {
        public UserQueryFilterNameRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<UserQueryFilterName> GetByUsernameAndSlotNumberAsync(string username, int slotNumber)
        {
            return DbSet.SingleOrDefaultAsync(e => e.Username == username && e.SlotNumber == slotNumber);
        }

        public Task<List<UserQueryFilterName>> GetByUsernameAsync(string username)
        {
            return DbSet.Where(e =>e.Username == username).ToListAsync();
        }

    }
}
