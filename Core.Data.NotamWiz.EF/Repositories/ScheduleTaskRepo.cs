﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ScheduleTaskRepo : Repository<ScheduleTask>, IScheduleTaskRepo
    {
        public ScheduleTaskRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public List<ScheduleTask> GetAllProcessed()
        {
            return DbSet.Where(e => e.IsProcessed).ToList();
        }

        public List<ScheduleTask> GetAllProcessedUntilYesterday()
        {
            var yesterday = DateTime.UtcNow.AddDays(-1);
            return DbSet.Where(e => e.IsProcessed && e.ProcessedDate <= yesterday ).ToList();
        }

        public void SafeUpdateScheduleTask(ScheduleTask scheduleTask)
        {
            var context = AppDbContext;

            var rowVersion = scheduleTask.RowVersion;
            scheduleTask.RowVersion = Guid.NewGuid();

            var dbEntityEntry = context.Entry(scheduleTask);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(scheduleTask);
            }

            dbEntityEntry.OriginalValues["RowVersion"] = rowVersion;

            dbEntityEntry.State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}
