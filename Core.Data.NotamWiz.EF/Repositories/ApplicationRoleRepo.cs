﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ApplicationRoleRepo : Repository<ApplicationRole>, IApplicationRoleRepo
    {
        public ApplicationRoleRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<ApplicationRole> GetRoleByName(string roleName)
        {
            return DbSet.FirstOrDefaultAsync(r => r.Name == roleName);
        }

        public List<ApplicationRole> GetByRoleName(string roleName)
        {
            return DbSet.Where(e => roleName.Contains(e.Name)).ToList();
        }

        public Task<List<ApplicationRole>> GetByRoleIdsAsync(IEnumerable<string> roleIds)
        {
            return DbSet.Where(e => roleIds.Contains(e.Id)).ToListAsync();
        }

        public Task<List<ApplicationRole>> GetDistintUserRoleAndOrgAdminRoleAsync()
        {
            return DbSet.Where(e => e.Name != CommonDefinitions.UserRole && e.Name != CommonDefinitions.OrgAdminRole).ToListAsync();
        }

        public Task<List<ApplicationRole>> GetWithUserRoleOrgAdminRoleAndCatchAllAsync()
        {
            return DbSet.Where(e =>
                    e.Name == CommonDefinitions.UserRole ||
                    e.Name == CommonDefinitions.OrgAdminRole //|| e.Name == CommonDefinitions.CanCatchAll
                    )
                .ToListAsync();
        }
    }
}
