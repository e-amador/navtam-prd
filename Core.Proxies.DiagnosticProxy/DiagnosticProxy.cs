﻿namespace NavCanada.Core.Proxies.DiagnosticProxy
{
    using System;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    using Domain.Model.Enums;

    public class DiagnosticProxy : IDiagnosticProxy
    {
        public DiagnosticProxy(string url)
        {
            _uri = new Uri(url);
        }

        public async Task<bool> PostKeepAliveMessage(string compoment, TickType type)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";

                    var postData = string.Format("{{\"component\":\"{0}\",\"tick\":\"{1}\"}}", compoment, type);

                    var response = await client.UploadDataTaskAsync(_uri, Encoding.ASCII.GetBytes(postData));

                    return response != null;
                }
            }
            catch (Exception)
            {
                //_logger.Error("Error posting to DiagnosticService", e);
                return false;
            }
        }

        //readonly ILog _logger = LogManager.GetLogger(typeof(DiagnosticProxy));
        private readonly Uri _uri;
    }
}