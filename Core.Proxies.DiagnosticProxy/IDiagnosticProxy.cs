﻿namespace NavCanada.Core.Proxies.DiagnosticProxy
{
    using System.Threading.Tasks;

    using Domain.Model.Enums;

    public interface IDiagnosticProxy
    {
        Task<bool> PostKeepAliveMessage(string compoment, TickType type);
    }
}
