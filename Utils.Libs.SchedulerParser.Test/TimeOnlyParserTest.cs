﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScheduleParserTest;

namespace Utils.Libs.SchedulerParser.ScheduleParserTest
{
    [TestClass]
    public class TimeOnlyParserTest 
    {
        
        [TestMethod] 
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenH24()
        {
            var result = Parser.Agent.Run("H24");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 0);
            Assert.IsTrue(parserResult.minutes[0].finish == 1440);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenDailyOnly()
        {
            var result = Parser.Agent.Run("DAILY");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 0);
            Assert.IsTrue(parserResult.minutes[0].finish == 1440);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod] 
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenTimeRange()
        {
            var result = Parser.Agent.Run("0101-0202");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly );
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenDailyTimeRange()
        {
            var result = Parser.Agent.Run("DAILY 0101-02023");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenSunriseSunset()
        {
            var result = Parser.Agent.Run("SR-SS");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == Common._SUNRISE_IN_MINUTES_);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenCaseDailySunriseSunset()
        {
            var result = Parser.Agent.Run("DAILY SR-SS");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == Common._SUNRISE_IN_MINUTES_);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenSunriseMinusSunsetPlus()
        {
            var result = Parser.Agent.Run("SR MINUS10-SS PLUS10");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenDailySunriseMinusSunsetPlusAndSpaces()
        {
            var result = Parser.Agent.Run("DAILY  SR MINUS10 - SS PLUS10 ");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenDailySunriseSunsetPlusAndTimeRange()
        {
            var result = Parser.Agent.Run("DAILY SR MINUS10-SS PLUS10 0120-0230");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[1].start == 80);
            Assert.IsTrue(parserResult.minutes[1].finish == 150);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenDailyTimeRangeAndSunsetPLusAndTimeRange()
        {
            var result = Parser.Agent.Run("DAILY 0100-SS PLUS20 0320-0430");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.minutes[1].start == 200);
            Assert.IsTrue(parserResult.minutes[1].finish == 270);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenDailyWithMultipleTimeRangeAndSunsetPlus()
        {
            var result = Parser.Agent.Run("DAILY 0100-SS PLUS20 0320-0430 0330-0440 0340-0450");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.TimeOnly);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 4);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.minutes[1].start == 200);
            Assert.IsTrue(parserResult.minutes[1].finish == 270);
            Assert.IsTrue(parserResult.minutes[2].start == 210);
            Assert.IsTrue(parserResult.minutes[2].finish == 280);
            Assert.IsTrue(parserResult.minutes[3].start == 220);
            Assert.IsTrue(parserResult.minutes[3].finish == 290);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenHourGreatherThan23()
        {
            var result = Parser.Agent.Run("2400-0210");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Hours between 0 and 23 Parameter name: value"));
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenHourNegativeValue()
        {
            var result = Parser.Agent.Run("-200-0210");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenMinutesGreaterThan59()
        {
            var result = Parser.Agent.Run("0260-0210");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Minutes between 0 and 59 Parameter name: value"));
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenHoursGreaterThan23()
        {
            var result = Parser.Agent.Run("2424-0210");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Hours between 0 and 23 Parameter name: value"));
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenSunriseMinusGreaterThan59()
        {
            var result = Parser.Agent.Run("SR MINUS60-0210");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Minutes between 0 and 59 Parameter name: value"));
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenSunsetPlusGreaterThan59()
        {
            var result = Parser.Agent.Run("SR MINUS14-SS PLUS60");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenMissingEndTimeRange()
        {
            var result = Parser.Agent.Run("0100");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenTimeRangeWithInvalidDigits()
        {
            var result = Parser.Agent.Run("0100-a303");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenSuriseContainsPlus()
        {
            var result = Parser.Agent.Run("SR PLUS10-SS PLUS10");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        //[TestMethod]
        //public void SchedulerParser_TimeOnly_ShouldReturnFailure_WhenSunsetContainsMinus()
        //{
        //    var result = Parser.Agent.Run("SR MINUS10-SS MINUS10");
        //    Assert.IsTrue(result.IsFailure);

        //    var parserResult = ParserUtils.CastFailure(result);
        //    Assert.IsTrue(parserResult.Length > 0);
        //}
    }
}
