﻿using NavCanada.Core.Proxies.AimslProxy;

namespace NavCanada.Core.TaskEngine.Test
{
    using Moq;

    using Common.Contracts;
    using Data.NotamWiz.Contracts;
    using Data.NotamWiz.Contracts.Repositories;
    using NotificationEngine;
    using Proxies.AeroRdsProxy;
    using Proxies.DiagnosticProxy;
    using Proxies.SignalrHubProxy;
    using Contracts;
    using Implementations;

    /// <summary>
    /// Provides common functinality for TaskEngine Task Tests
    /// </summary>
    public class BaseTaskEngineTest
    {
        // Queues mocks
        protected Mock<IClusterQueues> ClusteQueuesMock;
        protected Mock<IQueue> TaskEngineQueueMock;
        protected Mock<IQueue> WebAppQueue;
        protected Mock<IUow> UowMock {  get; set; }

        protected Mock<IAimslProxy> AimslProxyServiceMock { get; set;}
        protected Mock<ISignalrHubProxy> SignalrHubProxyServiceMock { get; set; }
        protected Mock<IAeroRdsProxy> AeroRdsProxyServiceMock { get; set; }
        protected Mock<IDiagnosticProxy> DianosticProxyMock { get; set; }
        protected Mock<INotifier> NotifierMock { get; set; }

        protected IClusterStorage ClusterStorage { get;  set; }

        // Repository mocks
        protected Mock<INotamProposalRepo> NotamProposalRepoMock { get; set; }
        protected Mock<IPoisonMtoRepo> PoisonMtoRepoMock  { get;  set; }
        protected Mock<IAimslRequestTrackingRepo> AimslRequestTrackingRepoMock { get; set; }
        protected Mock<IPoisonMtoRecordRepo> PoisonMtoRecordRepoMock { get; set; }
        protected IExternalServices ExternalServices {  get; set; }

        public virtual void Initialize()
        {
            MockDataRepositories();
            MockQueues();
            MockExternalServices();
            ClusterStorage = new ClusterStorage(UowMock.Object, this.ClusteQueuesMock.Object);
        }

        private void MockDataRepositories()
        {
            PoisonMtoRecordRepoMock = new Mock<IPoisonMtoRecordRepo>();
            PoisonMtoRepoMock = new Mock<IPoisonMtoRepo>();
            NotamProposalRepoMock =  new Mock<INotamProposalRepo>();
            AimslRequestTrackingRepoMock = new Mock<IAimslRequestTrackingRepo>();

            UowMock = new Mock<IUow>();
            UowMock.SetupGet(x => x.PoisonMtoRepo).Returns(PoisonMtoRepoMock.Object);
            UowMock.SetupGet(x => x.PoisonMtoRecordRepo).Returns(PoisonMtoRecordRepoMock.Object);
            UowMock.SetupGet(x => x.NotamProposalRepo).Returns(NotamProposalRepoMock.Object);
            UowMock.SetupGet(x => x.AimslRequestTrackingRepo).Returns(AimslRequestTrackingRepoMock.Object);
        }

        private void MockQueues()
        {
            ClusteQueuesMock = new Mock<IClusterQueues>();
            TaskEngineQueueMock = new Mock<IQueue>();
            WebAppQueue = new Mock<IQueue>();
            ClusteQueuesMock.SetupGet(x => x.TaskEngineQueue).Returns(this.TaskEngineQueueMock.Object);
            ClusteQueuesMock.SetupGet(x => x.InitiatorQueue).Returns(this.WebAppQueue.Object);
        }

        private void MockExternalServices()
        {
            AimslProxyServiceMock = new Mock<IAimslProxy>();
            SignalrHubProxyServiceMock = new Mock<ISignalrHubProxy>();
            DianosticProxyMock = new Mock<IDiagnosticProxy>();
            AeroRdsProxyServiceMock = new Mock<IAeroRdsProxy>();
            NotifierMock = new Mock<INotifier>();

            ExternalServices = new ExternalServices(
                AimslProxyServiceMock.Object, 
                SignalrHubProxyServiceMock.Object,
                AeroRdsProxyServiceMock.Object,
                DianosticProxyMock.Object,
                NotifierMock.Object );
        }
    }
}