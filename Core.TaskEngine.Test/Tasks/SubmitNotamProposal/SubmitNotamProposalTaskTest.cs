﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AimslProxy.Common;
using NotamProposalStatus = NavCanada.Core.Proxies.AimslProxy.Common.NotamProposalStatus;

namespace NavCanada.Core.TaskEngine.Test.Tasks.SubmitNotamProposal
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Common.Contracts;
    using Domain.Model.Enums;
    using MessageTask;
    using Mto;
    using TaskEngine.Tasks.WorkFlowTasks;
    using Test;

    [TestClass]
    public class SubmitNotamProposalTaskTest : BaseTaskEngineTest
    {
        [TestInitialize]
        public override void Initialize()
        {
            base.Initialize();
        }

        [TestMethod]
        public void SubmitNotamProposal_ShouldQueueProposal_WhenProposalIsSubmited()
        {
            var task = new SubmitNotamProposalTask();
            
            var mto = CreateNotamSubmitProposalMto();

            var queueMock = new Mock<IQueue>();
            ClusteQueuesMock.Setup(x => x.TaskEngineQueue).Returns(queueMock.Object);
            queueMock.Setup(x => 
                x.Publish(It.Is<MessageTransferObject>(m => 
                    (int)m[MessageDefs.MessageIdKey].Value == (int)mto[MessageDefs.MessageIdKey].Value)))
                    .Verifiable();

            var now = DateTime.UtcNow;
            var notamProposal = new NotamProposal
            {
                Id = 1,
                PackageId = 1
            };
            var aismlResponse = new AismlResponse
            {
                Code = ApiStatusCode.Success,
                ProposalStatus = new NotamProposalStatus
                {
                    InoNotamProposalId = 1,
                    Status = NotamProposalStatusCode.Queued,
                    StatusDateTime = now
                }
            };

            NotamProposalRepoMock.Setup(x => x.GetById(It.Is<long>(o => o == 1))).Returns(() => notamProposal);
            AimslProxyServiceMock.Setup(x => x.Submit(It.IsAny<NotamProposal>(), It.IsAny<string>())).Returns(() => aismlResponse);

            var context = new MessageTaskContext
            {
                Mto = mto,
                ClusterStorage = ClusterStorage,
                ExternalServices = ExternalServices
            };
            task.Execute(context);

            //Verify that the notamProposal was copied to the context for further pipe processing
            Assert.IsTrue(context.NotamProposal == notamProposal);

            //Verify that the Notam Proposal was updated with the correct AimslResponse
            Assert.IsTrue(notamProposal.InoProposalId == aismlResponse.ProposalStatus.InoNotamProposalId);
            Assert.IsTrue(notamProposal.StatusTime == aismlResponse.ProposalStatus.StatusDateTime);
            Assert.IsTrue(notamProposal.Status == aismlResponse.ProposalStatus.Status);
            Assert.IsFalse(context.DiscardSignalrNotification);

            // make sure the save methods was called once
            UowMock.Verify(x => x.SaveChanges(false), Times.Once);
        }

        [TestMethod]
        public void SubmitNotamProposal_ShouldChangeStatusToRejected_WhenAimlsRejectTheProposal()
        {
            var task = new SubmitNotamProposalTask();

            var mto = CreateNotamSubmitProposalMto();

            var queueMock = new Mock<IQueue>();
            ClusteQueuesMock.Setup(x => x.TaskEngineQueue).Returns(queueMock.Object);
            queueMock.Setup(x =>
                x.Publish(It.Is<MessageTransferObject>(m =>
                    (int)m[MessageDefs.MessageIdKey].Value == (int)mto[MessageDefs.MessageIdKey].Value)))
                    .Verifiable();

            var now = DateTime.UtcNow;
            var notamProposal = new NotamProposal
            {
                Id = 1,
                PackageId = 1
            };
            var aismlResponse = new AismlResponse
            {
                Code = ApiStatusCode.Error,
                ErrorMessage = "SOME ERROR",
                ProposalStatus = new NotamProposalStatus
                {
                    InoNotamProposalId = 1,
                    Status = NotamProposalStatusCode.QueuePending,
                    StatusDateTime = now
                }
            };

            NotamProposalRepoMock.Setup(x => x.GetById(It.Is<long>(o => o == 1))).Returns(() => notamProposal);
            AimslProxyServiceMock.Setup(x => x.Submit(It.IsAny<NotamProposal>(), It.IsAny<string>())).Returns(() => aismlResponse);

            var context = new MessageTaskContext
            {
                Mto = mto,
                ClusterStorage = ClusterStorage,
                ExternalServices = ExternalServices
            };
            task.Execute(context);

            //Verify that the notamProposal was copied to the context for further pipe processing
            Assert.IsTrue(context.NotamProposal == notamProposal);

            //Verify that the Notam Proposal was updated with the correct AimslResponse
            Assert.IsTrue(notamProposal.StatusTime == aismlResponse.ProposalStatus.StatusDateTime);
            Assert.IsTrue(notamProposal.Status == NotamProposalStatusCode.Rejected);
            Assert.IsTrue(notamProposal.ModifiedByNof == false);
            Assert.IsTrue(notamProposal.RejectionReason == "AIMSL ERROR:SOME ERROR");
            Assert.IsFalse(context.DiscardSignalrNotification);

            // make sure the save methods was called once
            UowMock.Verify(x => x.SaveChanges(false), Times.Once);
        }

        [TestMethod]
        public void SubmitNotamProposal_ShouldIgnoreMessage_WhenNotamProposalDoesNotExist()
        {
            var task = new SubmitNotamProposalTask();

            var mto = CreateNotamSubmitProposalMto();

            var queueMock = new Mock<IQueue>();
            ClusteQueuesMock.Setup(x => x.TaskEngineQueue).Returns(queueMock.Object);
            queueMock.Setup(x =>
                x.Publish(It.Is<MessageTransferObject>(m =>
                    (int)m[MessageDefs.MessageIdKey].Value == (int)mto[MessageDefs.MessageIdKey].Value)))
                    .Verifiable();

            var now = DateTime.UtcNow;

            NotamProposalRepoMock.Setup(x => x.GetById(It.IsAny<long>())).Returns(() => null);
            AimslProxyServiceMock.Setup(x => x.Submit(It.IsAny<NotamProposal>(), It.IsAny<string>())).Returns(() => null);

            var context = new MessageTaskContext
            {
                Mto = mto,
                ClusterStorage = ClusterStorage,
                ExternalServices = ExternalServices
            };
            task.Execute(context);

            //Verify that the notamProposal was copied to the context for further pipe processing
            Assert.IsTrue(context.NotamProposal == null);
            Assert.IsTrue(context.DiscardSignalrNotification);
            // make sure the save methods was never called
            UowMock.Verify(x => x.SaveChanges(false), Times.Never);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SubmitNotamProposal_ShouldThrowInvalidOperationException_WhenMtoProposalIdIsInvalid()
        {
            var task = new SubmitNotamProposalTask();

            var mto = CreateNotamSubmitProposalMtoWithInvalidProposalId();

            ConfigureMockingQueue(mto);

            var notamProposal = new NotamProposal
            {
                Id = 1,
                PackageId = 1
            };
            var aismlResponse = new AismlResponse
            {
                Code = ApiStatusCode.ConnectionError,
                ErrorMessage = "Error...."
            };

            NotamProposalRepoMock.Setup(x => x.GetById(It.Is<long>(o => o == 1))).Returns(() => notamProposal);
            AimslProxyServiceMock.Setup(x => x.Submit(It.IsAny<NotamProposal>(), It.IsAny<string>())).Returns(() => aismlResponse);

            var context = new MessageTaskContext
            {
                Mto = mto,
                ClusterStorage = ClusterStorage,
                ExternalServices = ExternalServices
            };
            task.Execute(context);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SubmitNotamProposalTask_ShouldThrowInvalidOperationException_WhenAimslServiceReturnsError()
        {
            var task = new SubmitNotamProposalTask();

            var mto = CreateNotamSubmitProposalMto();

            ConfigureMockingQueue(mto);

            var notamProposal = new NotamProposal
            {
                Id = 1,
                PackageId = 1
            };
            var aismlResponse = new AismlResponse
            {
                Code = ApiStatusCode.ConnectionError,
                ErrorMessage = "Error...."
            };

            NotamProposalRepoMock.Setup(x => x.GetById(It.Is<long>(o => o == 1))).Returns(() => notamProposal);
            AimslProxyServiceMock.Setup(x => x.Submit(It.IsAny<NotamProposal>(), It.IsAny<string>())).Returns(() => aismlResponse);

            var context = new MessageTaskContext
            {
                Mto = mto,
                ClusterStorage = ClusterStorage,
                ExternalServices = ExternalServices
            };
            task.Execute(context);
        }

        private static MessageTransferObject CreateNotamSubmitProposalMto()
        {
            var mto = new MessageTransferObject();

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)MessageTransferObjectId.SubmitNotamProposal, DataType.Int32);

            //identifier for the actual proposal
            mto.Add(MessageDefs.EntityIdentifierKey, 1, DataType.Int64);

            //Message Time
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);
            return mto;
        }

        private static MessageTransferObject CreateNotamSubmitProposalMtoWithInvalidProposalId()
        {
            var mto = new MessageTransferObject();

            //create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            //create inbound type
            mto.Add(MessageDefs.MessageIdKey, (int)MessageTransferObjectId.SubmitNotamProposal, DataType.Int32);

            //identifier for the actual proposal
            mto.Add(MessageDefs.EntityIdentifierKey, "Test", DataType.String);

            //Message Time
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);
            return mto;
        }

        private void ConfigureMockingQueue(MessageTransferObject mto)
        {
            var queueMock = new Mock<IQueue>();
            ClusteQueuesMock.Setup(x => x.TaskEngineQueue).Returns(queueMock.Object);
            queueMock.Setup(x => x.Publish(It.Is<MessageTransferObject>(m => (int)m[MessageDefs.MessageIdKey].Value == (int)mto[MessageDefs.MessageIdKey].Value))).Verifiable();
        }

    }
}