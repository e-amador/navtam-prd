﻿using NavCanada.Core.TaskEngine.Tasks.DiagnosticTask;
using NavCanada.Core.TaskEngine.Tasks.NotificationTasks.Retry;
using NavCanada.Core.TaskEngine.Tasks.ScheduleTasks;

namespace NavCanada.Core.TaskEngine.Test.MtoProcessor
{
    using TaskEngine.Tasks.ExternalServicesTasks;
    using System;
    using System.Collections.Generic;
    using TaskEngine.Tasks.CommonTasks;
    using TaskEngine.Tasks.NotificationTasks;
    using TaskEngine.Tasks.WorkFlowTasks;

    public class TaskOrderingType
    {
        public readonly List<Type> SubmitNotamProposalTasks = new List<Type>
        {
            typeof(SubmitNotamProposalTask),
            typeof(WorkFlowSignalrTask)
        };

        public readonly List<Type> EndNotamProposalTasks = new List<Type>
        {
            typeof(EndNotamProposalTask),
            typeof(NotamEndedEmailTask),
            typeof(WorkFlowSignalrTask),
        };

        public readonly List<Type> ExpireNotamProposalTasks = new List<Type>
        {
            typeof(ExpireNotamProposalTask),
            typeof(NotamExpiredEmailTask),
            typeof(WorkFlowSignalrTask),
        };

        public readonly List<Type> WithdrawNotamProposalTasks = new List<Type>
        {
            typeof(WithdrawNotamProposalTask),
            typeof(WorkFlowSignalrTask)
        };

        public readonly List<Type> NotamEndOrEnforceWorkFlowSchedulerMsgTasks = new List<Type>
        {
            typeof(NotamEndOrEnforceScheduleTask)
        };

        public readonly List<Type> NotamSoonToExpireWorkFlowSchedulerMsgTasks = new List<Type>
        {
            typeof(NotamSoonToExpireScheduleTask)
        };

        public readonly List<Type> NotamExpireWorkFlowSchedulerMsgTasks = new List<Type>
        {
            typeof(NotamExpiredScheduleTask)
        };

        public readonly List<Type> NotamEndWorkFlowSchedulerMsgTasks = new List<Type>
        {
            typeof(NotamEndScheduleTask)
        };

        public readonly List<Type> AeroRdsRunSchedulerMsgTasks = new List<Type>
        {
            typeof(AeroRdsSyncScheduleTask)
        };

        public readonly List<Type> AimslRunSchedulerMsgTasks = new List<Type>
        {
            typeof(AimslSyncScheduleTask)
        };

        public readonly List<Type> NotamSoonToExpireEmailTasks = new List<Type>
        {
            typeof(NotamSoonToExpireEmailTask),
            typeof(WorkFlowSignalrTask)
        };

        public readonly List<Type> RegistrationNotificationEmailTasks = new List<Type>
        {
            typeof(RegistrationEmailTask)
        };

        public readonly List<Type> NotamRetryRejectEmailTasks = new List<Type>
        {
            typeof(NotamRetryRejectEmailTask)
        };

        public readonly List<Type> NotamRetryEndedEmailTasks = new List<Type>
        {
            typeof(NotamRetryEndedEmailTask)
        };

        public readonly List<Type> NotamRetryExpiredEmailTasks = new List<Type>
        {
            typeof(NotamRetryExpiredEmailTask)
        };

        public readonly List<Type> NotamRetrySoonToExpiredEmailTasks = new List<Type>
        {
            typeof(NotamRetrySoonToExpireEmailTask)
        };

        public readonly List<Type> PoisonSchedulerMsgTasks = new List<Type>
        {
            typeof(PoisonMtoScheduleTask)
        };


        public readonly List<Type> RetryRegistrationEmailTask = new List<Type>
        {
            typeof(RetryRegistrationEmailTask)
        };

        public readonly List<Type> ProcessAimslSyncTasks = new List<Type>
        {
            typeof(ProcessAimslSyncTask),
            typeof(NotamRejectEmailTask),
            typeof(WorkFlowSignalrTask)
        };

        public readonly List<Type> AimslSyncTasks = new List<Type>
        {
            typeof(AimslSyncTask),
            typeof(WorkFlowSignalrTask)
        };

        public readonly List<Type> KeepAliveSchedulerMsgTasks = new List<Type>
        {
            typeof(KeepAliveTask)
        };

        public readonly List<Type> DeleteCompletedRecordsSchedulerMsgTasks = new List<Type>
        {
            typeof(DeleteCompletedRecordsTask)
        };

    }
}