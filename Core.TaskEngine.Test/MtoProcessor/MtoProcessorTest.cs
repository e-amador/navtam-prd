﻿namespace NavCanada.Core.TaskEngine.Test.MtoProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Domain.Model.Enums;
    using Contracts;
    using Implementations;
    using MessageTask;
    using Mto;
    using TaskEngine.MtoProcessor;
    using Test;

    [TestClass]
    public class MtoProcessorTest : BaseTaskEngineTest
    {
        protected const string ObuByteId = "6C4C72AA";

        private readonly TaskOrderingType _orderingTask = new TaskOrderingType();
        private IMessageTaskFactory _taskFactory;
        private Mock<IMessageTask> _taskMock;

        [TestInitialize]
        public override void Initialize()
        {
            base.Initialize();
            _taskMock = new Mock<IMessageTask>();
            _taskFactory = new MessageTaskFactory();
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunSubmitNotamProposalTask_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.WorkFlowTask, MessageTransferObjectId.SubmitNotamProposal));
            TestOrderAndType(tasks, _orderingTask.SubmitNotamProposalTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunProcessEndNotamProposalTask_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.WorkFlowTask, MessageTransferObjectId.EndNotamProposal));
            TestOrderAndType(tasks, _orderingTask.EndNotamProposalTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunExpireNotamProposalTask_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.WorkFlowTask, MessageTransferObjectId.ExpireNotamProposal));
            TestOrderAndType(tasks, _orderingTask.ExpireNotamProposalTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunWithdrawNotamProposalTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.WorkFlowTask, MessageTransferObjectId.WithdrawNotamProposal));
            TestOrderAndType(tasks, _orderingTask.WithdrawNotamProposalTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamEndOrEnforceWorkFlowSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.NotamEndOrEnforceWorkFlowSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.NotamEndOrEnforceWorkFlowSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamSoonToExpireWorkFlowSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.NotamSoonToExpireWorkFlowSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.NotamSoonToExpireWorkFlowSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamExpireWorkFlowSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.NotamExpireWorkFlowSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.NotamExpireWorkFlowSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamEndWorkFlowSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.NotamEndWorkFlowSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.NotamEndWorkFlowSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunAeroRdsRunSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.AeroRdsRunSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.AeroRdsRunSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunAimslRunSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.AimslRunSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.AimslRunSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamSoonToExpireEmailTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.NotificationTask, MessageTransferObjectId.NotamSoonToExpireEmail));
            TestOrderAndType(tasks, _orderingTask.NotamSoonToExpireEmailTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunRegistrationNotificationEmailTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.NotificationTask, MessageTransferObjectId.RegistrationNotificationEmail));
            TestOrderAndType(tasks, _orderingTask.RegistrationNotificationEmailTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamRetryRejectEmailTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.NotificationTask, MessageTransferObjectId.NotamRetryRejectEmail));
            TestOrderAndType(tasks, _orderingTask.NotamRetryRejectEmailTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamRetryEndedEmailTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.NotificationTask, MessageTransferObjectId.NotamRetryEndedEmail));
            TestOrderAndType(tasks, _orderingTask.NotamRetryEndedEmailTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamRetryExpiredEmailTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.NotificationTask, MessageTransferObjectId.NotamRetryExpiredEmail));
            TestOrderAndType(tasks, _orderingTask.NotamRetryExpiredEmailTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunNotamRetrySoonToExpiredEmailTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.NotificationTask, MessageTransferObjectId.NotamRetrySoonToExpiredEmail));
            TestOrderAndType(tasks, _orderingTask.NotamRetrySoonToExpiredEmailTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunRetryRegistrationEmailTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.NotificationTask, MessageTransferObjectId.RetryRegistrationEmailTask));
            TestOrderAndType(tasks, _orderingTask.RetryRegistrationEmailTask);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunProcessAimsAsycTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ExternalServicesTask, MessageTransferObjectId.ProcessAimslSync));
            TestOrderAndType(tasks, _orderingTask.ProcessAimslSyncTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunAimslSyncTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ExternalServicesTask, MessageTransferObjectId.AimslSync));
            TestOrderAndType(tasks, _orderingTask.AimslSyncTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunKeepAliveSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.DiagnosticTask, MessageTransferObjectId.KeepAliveSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.KeepAliveSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunDeleteCompletedRecordsSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.DeleteCompletedRecordsSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.DeleteCompletedRecordsSchedulerMsgTasks);
        }

        [TestMethod]
        public void MtoProcessor_ShouldRunPoisonSchedulerMsgTasks_WhenExcecuted()
        {
            var tasks = GetTasksProducedByTaskEngine(CreateMto(MessageType.ScheduleTask, MessageTransferObjectId.PoisonSchedulerMsg));
            TestOrderAndType(tasks, _orderingTask.PoisonSchedulerMsgTasks);
        }

        private List<IMessageTask> GetTasksProducedByTaskEngine(MessageTransferObject mto)
        {
            List<IMessageTask> tasks = null;
            var taskFactoryMock = new Mock<IMessageTaskFactory>();
            taskFactoryMock.Setup(x => x.GetTasks(It.IsAny<IMessageTaskContext>())).Returns<IMessageTaskContext>(ctx =>
            {
                tasks = _taskFactory.GetTasks(ctx);
                return tasks.Select(t => _taskMock.Object).ToList();
            });
            var mtoProcessor = new MtoProcessor(ClusterStorage, null, taskFactoryMock.Object,  new MtoProcessorSettings());
            mtoProcessor.Run(mto);
            return tasks;
        }

        private static void TestOrderAndType(List<IMessageTask> tasks, List<Type> correctTypes)
        {
            Assert.AreNotEqual(tasks, null);
            Assert.AreEqual(correctTypes.Count, tasks.Count());

            //verify the correct order and type
            for (var i = 0; i < tasks.Count(); i++)
            {
                Assert.IsInstanceOfType(tasks[i], correctTypes[i]);
            }
        }

        private MessageTransferObject CreateMto(MessageType msgType, MessageTransferObjectId msgId)
        {
            var mto = new MessageTransferObject();

            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);
            mto.Add(MessageDefs.MessageTypeKey, (byte)msgType, DataType.Byte);
            mto.Add(MessageDefs.MessageIdKey, (byte)msgId, DataType.Byte);

            return mto;
        }
    }
}