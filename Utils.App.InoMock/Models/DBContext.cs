﻿namespace NavCanada.Utils.Apps.InoMock.Models
{
    using System.Data.Entity;

    public class DBContext : DbContext
    {
        public DBContext()
            : base("name=INOMockConnection")
        {

            Database.SetInitializer<DBContext>(new MigrateDatabaseToLatestVersion<DBContext, Migrations.Configuration>("INOMockConnection"));
        }


        public DbSet<NotamProposal> NotamProposals { get; set; }
        public DbSet<Notam> Notams { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

    }
}