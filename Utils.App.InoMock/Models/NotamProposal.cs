﻿namespace NavCanada.Utils.Apps.InoMock.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("NotamProposal")]
    public class NotamProposal 
    {
        [Key]
        public long Id { get; set; }

        public string NOF { get; set; }
        public string Series { get; set; }
        public int Number { get; set; }
        public int Year { get; set; }
        public NotamTypeType Type { get; set; }
        public string ReferredSeries { get; set; }
        public int ReferredNumber { get; set; }
        public int ReferredYear { get; set; }
        public QLineType QLine { get; set; }
        public string Coordinates { get; set; }
        public int Radius { get; set; }
        public string ItemA { get; set; }
        public string StartValidity { get; set; }
        public string EndValidity { get; set; }
        public bool Estimated { get; set; }
        public string ItemD { get; set; }
        public string ItemE { get; set; }
        public string ItemEFrench { get; set; }
        public string ItemF { get; set; }
        public string ItemG { get; set; }
        public string ItemX { get; set; }
        public string Operator { get; set; }

        public NotamProposalStatusType Status { get; set; }
        public DateTime StatusTime { get; set; }

        public Nullable<long> SubmittedNotam_Id { get; set; }


        [ForeignKey("SubmittedNotam_Id")]
        public Notam SubmittedNotam { get; set; }
        public string RejectionReason { get; set; }

        public string NoteToNOF { get; set; }



        [NotMapped]
        public string ICAONOTAM
        {
            get
            {
                return "";
                throw new NotImplementedException();
            }

        }
        internal void Pick()
        {
            this.Status = NotamProposalStatusType.Pending;
        }
    }
}