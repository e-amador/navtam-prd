﻿namespace NavCanada.Utils.Apps.InoMock.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public enum NotamTypeType
    {

        /// <remarks/>
        N,

        /// <remarks/>
        R,

        /// <remarks/>
        C,
    }
    public enum TrafficType
    {

        /// <remarks/>
        I,

        /// <remarks/>
        V,

        /// <remarks/>
        IV,

        /// <remarks/>
        K,
    }

    public enum PurposeType
    {

        /// <remarks/>
        NB,

        /// <remarks/>
        BO,

        /// <remarks/>
        M,

        /// <remarks/>
        K,

        /// <remarks/>
        NBO,

        /// <remarks/>
        B,
    }
    public enum ScopeType
    {

        /// <remarks/>
        A,

        /// <remarks/>
        E,

        /// <remarks/>
        W,

        /// <remarks/>
        AE,

        /// <remarks/>
        AW,

        /// <remarks/>
        K,
    }

    public enum NotamProposalStatusType
    {
        [Description("Undefined")]
        Undefined = 0,
        [Description("Draft")]
        Draft = 1,
        [Description("Queued")]
        Queued = 2,
        [Description("Pending")]
        Pending = 3,
        [Description("Rejected")]
        Rejected = 4,
        [Description("Submitted")]
        Submitted = 5,
        [Description("Modified And Submitted")]
        ModifiedAndSubmitted = 6,
        [Description("Withdrawn")]
        Withdrawn = 7,
        [Description("Withdrawn Pending")]
        WithdrawnPending = 8,
        [Description("Queued")]
        QueuePending = 9,
        [Description("Soon To Expire")]
        SoontoExpire = 11,
        [Description("ended")]
        ended = 13,
        [Description("Enforced")]
        Enforced = 14,
        [Description("Cancelled")]
        Cancelled = 15,
        [Description("Modified and Enforced")]
        ModifiedEnforced = 16,
        [Description("Modified and Cancelled")]
        ModifiedCancelled = 17,
        
    }

    public partial class QLineType
    {
        public string FIR {get;set;}       
        public string Code23 {get;set;}
        public string Code45 {get;set;}
        public TrafficType Traffic {get;set;}
        public PurposeType Purpose {get;set;}
        public ScopeType Scope {get;set;}
        public int Lower {get;set;}
        public int Upper {get;set;}
    }



    [Table("NOTAM")]
    public class Notam
    {
        [Key]
        public long Id { get; set; }

        public string NOF { get;set;}        
        public string Series  { get;set;}
        public int Number { get;set;}
        public int Year { get;set;}
        public NotamTypeType Type { get;set;}
        public string ReferredSeries { get;set;}
        public int ReferredNumber { get;set;}
        public int ReferredYear { get;set;}
        public QLineType QLine { get;set;}
        public string Coordinates { get;set;}        
        public int Radius { get;set;}
        public string ItemA { get;set;}
        public string StartValidity { get;set;}
        public string EndValidity { get;set;}
        public bool Estimated { get;set;}        
        public string ItemD { get;set;}
        public string ItemE { get;set;}
        public string ItemF { get;set;}
        public string ItemEFrench { get; set; }
        public string ItemG { get;set;}
        public string ItemX { get;set;}
        public string Operator { get;set;}



    }
}