﻿namespace NavCanada.Utils.Apps.InoMock
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class NotamType
    {
      

        private string nOFField;

        private string seriesField;

        private int numberField;

        private int yearField;

        private NotamTypeType typeField;

        private string referredSeriesField;

        private int referredNumberField;

        private int referredYearField;

        private QLineType qLineField;

        private string coordinatesField;

        private int radiusField;

        private string itemAField;

        private string startValidityField;

        private string endValidityField;

        private EstimationType estimationField;

        private bool estimationFieldSpecified;

        private string itemDField;

        private string itemEField;

        private string itemFField;

        private string itemGField;

        private string itemXField;

        private string operatorField;

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NOF
        {
            get
            {
                return this.nOFField;
            }
            set
            {
                this.nOFField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Series
        {
            get
            {
                return this.seriesField;
            }
            set
            {
                this.seriesField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "positiveInteger")]
        public int Number
        {
            get
            {
                return this.numberField;
            }
            set
            {
                this.numberField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer")]
        public int Year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public NotamTypeType Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ReferredSeries
        {
            get
            {
                return this.referredSeriesField;
            }
            set
            {
                this.referredSeriesField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "positiveInteger")]
        public int ReferredNumber
        {
            get
            {
                return this.referredNumberField;
            }
            set
            {
                this.referredNumberField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer")]
        public int ReferredYear
        {
            get
            {
                return this.referredYearField;
            }
            set
            {
                this.referredYearField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        
        public QLineType QLine
        {
            get
            {
                return this.qLineField;
            }
            set
            {
                this.qLineField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Coordinates
        {
            get
            {
                return this.coordinatesField;
            }
            set
            {
                this.coordinatesField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "nonNegativeInteger")]
        public int Radius
        {
            get
            {
                return this.radiusField;
            }
            set
            {
                this.radiusField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute("ItemA", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemA
        {
            get
            {
                return this.itemAField;
            }
            set
            {
                this.itemAField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string StartValidity
        {
            get
            {
                return this.startValidityField;
            }
            set
            {
                this.startValidityField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string EndValidity
        {
            get
            {
                return this.endValidityField;
            }
            set
            {
                this.endValidityField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public EstimationType Estimation
        {
            get
            {
                return this.estimationField;
            }
            set
            {
                this.estimationField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EstimationSpecified
        {
            get
            {
                return this.estimationFieldSpecified;
            }
            set
            {
                this.estimationFieldSpecified = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemD
        {
            get
            {
                return this.itemDField;
            }
            set
            {
                this.itemDField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemE
        {
            get
            {
                return this.itemEField;
            }
            set
            {
                this.itemEField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemF
        {
            get
            {
                return this.itemFField;
            }
            set
            {
                this.itemFField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemG
        {
            get
            {
                return this.itemGField;
            }
            set
            {
                this.itemGField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemX
        {
            get
            {
                return this.itemXField;
            }
            set
            {
                this.itemXField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Operator
        {
            get
            {
                return this.operatorField;
            }
            set
            {
                this.operatorField = value;
            }
        }
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [Serializable()]
   // [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.teamead.com/ws/inodp")]
    public enum NotamTypeType
    {

        /// <remarks/>
        N,

        /// <remarks/>
        R,

        /// <remarks/>
        C,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
   // [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.teamead.com/ws/inodp")]
    
    public partial class QLineType
    {

        private string fIRField;

        private string code23Field;

        private string code45Field;

        private TrafficType trafficField;

        private PurposeType purposeField;

        private ScopeType scopeField;

        private int lowerField;

        private int upperField;

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string FIR
        {
            get
            {
                return this.fIRField;
            }
            set
            {
                this.fIRField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Code23
        {
            get
            {
                return this.code23Field;
            }
            set
            {
                this.code23Field = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Code45
        {
            get
            {
                return this.code45Field;
            }
            set
            {
                this.code45Field = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TrafficType Traffic
        {
            get
            {
                return this.trafficField;
            }
            set
            {
                this.trafficField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public PurposeType Purpose
        {
            get
            {
                return this.purposeField;
            }
            set
            {
                this.purposeField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ScopeType Scope
        {
            get
            {
                return this.scopeField;
            }
            set
            {
                this.scopeField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "nonNegativeInteger")]
        public int Lower
        {
            get
            {
                return this.lowerField;
            }
            set
            {
                this.lowerField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "nonNegativeInteger")]
        public int Upper
        {
            get
            {
                return this.upperField;
            }
            set
            {
                this.upperField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [Serializable()]
   // [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.teamead.com/ws/inodp")]
    public enum TrafficType
    {

        /// <remarks/>
        I,

        /// <remarks/>
        V,

        /// <remarks/>
        IV,

        /// <remarks/>
        K,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [Serializable()]
   // [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.teamead.com/ws/inodp")]
    public enum PurposeType
    {

        /// <remarks/>
        NB,

        /// <remarks/>
        BO,

        /// <remarks/>
        M,

        /// <remarks/>
        K,

        /// <remarks/>
        NBO,

        /// <remarks/>
        B,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [Serializable()]
   // [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.teamead.com/ws/inodp")]
    public enum ScopeType
    {

        /// <remarks/>
        A,

        /// <remarks/>
        E,

        /// <remarks/>
        W,

        /// <remarks/>
        AE,

        /// <remarks/>
        AW,

        /// <remarks/>
        K,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [Serializable()]
   // [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.teamead.com/ws/inodp")]
    public enum EstimationType
    {

        /// <remarks/>
        EST,
    }

}