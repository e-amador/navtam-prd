﻿namespace NavCanada.Utils.Apps.InoMock
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            // The jQuery bundle
            bundles.Add(new ScriptBundle("~/bundles/jqueryBundle").Include(
                            "~/scripts/jquery-*",
                            "~/scripts/jquery-ui-*",
                            "~/scripts/jquery.inputmask/jquery.inputmask*",
                            "~/scripts/jquery.feedBackBox.js",
                            "~/scripts/jquery.idletimer.js",
                            "~/scripts/jquery.idletimeout.js"
                            ));

            bundles.Add(new Bundle("~/bundles/jqueryLayoutBundle").Include("~/scripts/jquery.layout.js"));

            // The signalr bundle
            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                            "~/signalr/hubs", "~/scripts/jquery.signalR-*"));


            // The NavCanada bundle
            bundles.Add(new ScriptBundle("~/bundles/notamwizcommon").Include(
                            "~/scripts/navcanada.notamwiz.common.js"));


            // The Kendo JavaScript bundle
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                    "~/scripts/kendo/kendo.web.min.js", // or kendo.all.min.js if you want to use Kendo UI Web and Kendo UI DataViz
                    "~/scripts/kendo/kendo.aspnetmvc.min.js",
                    "~/scripts/kendo.web.ext.js"));


            // The Kendo CSS bundle
            StyleBundle kendoStyleBundle = new StyleBundle("~/content/kendo/kendoBundle");
            kendoStyleBundle.Include("~/content/kendo/kendo.common.min.css");
            kendoStyleBundle.Include("~/content/kendo/kendo.blueopal.min.css");
            //kendoStyleBundle.Include("~/Content/NsdEditor/bootstrap.css");
            bundles.Add(kendoStyleBundle);


            StyleBundle siteStyleBundle = new StyleBundle("~/content/site");
            siteStyleBundle.Include("~/content/site.css");
            siteStyleBundle.Include("~/content/jquery.feedBackBox.css");

            bundles.Add(siteStyleBundle);


            // Clear all items from the ignore list to allow minified CSS and JavaScript files in debug mode
            bundles.IgnoreList.Clear();


            // Add back the default ignore list rules sans the ones which affect minified files and debug mode
            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);

            //    BundleTable.EnableOptimizations = false;
        }
    }
}