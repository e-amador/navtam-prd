namespace INOMock.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_NoteToNOF : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NotamProposal", "NoteToNOF", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.NotamProposal", "NoteToNOF");
        }
    }
}
