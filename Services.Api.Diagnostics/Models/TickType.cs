﻿namespace NavCanada.Services.Api.Diagnostics.Models
{
   public enum TickType
    {
        Start = 1,
        KeepAlive = 100,
        Stop = 0,
    }
}
