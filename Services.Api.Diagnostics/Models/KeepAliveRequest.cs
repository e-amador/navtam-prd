﻿namespace NavCanada.Services.Api.Diagnostics.Models
{
    public class KeepAliveRequest
    {
        public string Component { get;  set; }

        public TickType Tick { get; set; }
    }
}