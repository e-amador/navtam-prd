﻿namespace NavCanada.Services.Api.Diagnostics.Data.Contracts
{
    using NavCanada.Services.Api.Diagnostics.Models;

    public interface IKeepAliveRepo : IRepository<KeepAlive>
    {
    }
}
