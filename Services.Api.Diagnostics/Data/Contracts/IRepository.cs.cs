﻿using System.Collections.Generic;
using System.Linq;

namespace NavCanada.Services.Api.Diagnostics.Data.Contracts
{
    using System.Threading.Tasks;

    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        Task<List<T>> GetAllAsync();

        T GetById(object id);

        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
    }
}