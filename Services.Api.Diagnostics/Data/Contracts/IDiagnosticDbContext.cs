﻿namespace NavCanada.Services.Api.Diagnostics.Data.Contracts
{
    using System.Data.Entity;

    using NavCanada.Services.Api.Diagnostics.Models;

    public interface IDiagnosticDbContext
    {
        IDbSet<Log> Logs { get; set; }
        IDbSet<KeepAlive> KeepAlives { get; set; }
    }
}
