namespace NavCanada.Services.Api.Diagnostics.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KeepAlive",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Component = c.String(),
                        Tick = c.Int(nullable: false),
                        Received = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Thread = c.String(nullable: false),
                        Level = c.String(nullable: false),
                        Logger = c.String(nullable: false),
                        Message = c.String(nullable: false),
                        Exception = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Log");
            DropTable("dbo.KeepAlive");
        }
    }
}
