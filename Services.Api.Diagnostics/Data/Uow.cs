﻿namespace NavCanada.Services.Api.Diagnostics.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Threading.Tasks;

    using Contracts;
    using Helpers;

    /// <summary>
    /// The "Unit of Work"
    ///     1) decouples the repos from the consumers
    ///     2) decouples the DbContext and EF from the consumers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="IDiagnosticUow"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext in AppDbContext).
    /// </remarks>
    public class Uow : IDiagnosticUow
    {
        private DbContextTransaction _transaction;

        public Uow(IRepositoryProvider repositoryProvider)
            :this(new DiagnosticDbContext(), repositoryProvider)
        {
        }

        public Uow(DbContext context, IRepositoryProvider repositoryProvider)
        {
            DbContext = context;
            ConfigureDbContext();
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }

        public DbContext DbContext
        {
            get;
            private set;
        }

        public ILogRepo LogRepo
        {
            get;
            set;
        }

        private IRepositoryProvider RepositoryProvider
        {
            get;
            set;
        }

        private void ConfigureDbContext()
        {
            //creating the database scheme.
            //DbContext.Database.Initialize(true);
            DbContext.Configuration.AutoDetectChangesEnabled = true;
            // Do NOT enable proxied entities, else serialization fails
            DbContext.Configuration.ProxyCreationEnabled = true;

            // Load navigation properties explicitly (avoid serialization trouble)
            DbContext.Configuration.LazyLoadingEnabled = true;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        #region IUoW

        public int SaveChanges()
        {
            return DbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return DbContext.SaveChangesAsync();
        }

        //public void FlagModified(object entity)
        //{
        //    DbContext.Entry(entity).State = EntityState.Modified;
        //}

        public void Detach()
        {
            var context = ((IObjectContextAdapter)DbContext).ObjectContext;
            foreach (var change in DbContext.ChangeTracker.Entries())
            {
                if (change.State == EntityState.Modified)
                {
                    context.Refresh(RefreshMode.StoreWins, change.Entity);
                }
                if (change.State == EntityState.Added)
                {
                    context.Detach(change.Entity);
                }
            }
        }

        public void BeginTransaction()
        {
            _transaction = DbContext.Database.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                DbContext.SaveChanges();
                if (_transaction != null)
                {
                    _transaction.Commit();
                }
            }
            finally
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
            }
        }

        public void Rollback()
        {
            try
            {
                if (_transaction != null)
                {
                    _transaction.Rollback();
                }
            }
            finally
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
            }
        }

        public Services.Api.Diagnostics.Data.Contracts.IKeepAliveRepo KeepAliveRepo
        {
            get
            {
                return GetRepo<Services.Api.Diagnostics.Data.Contracts.IKeepAliveRepo>();
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}