﻿namespace NavCanada.Services.Api.Diagnostics.Data.Configurations
{
    using System.Data.Entity.ModelConfiguration;

    using NavCanada.Services.Api.Diagnostics.Models;

    public class KeepAliveConfig : EntityTypeConfiguration<KeepAlive>
    {
        public KeepAliveConfig()
        {
            HasKey(e => e.Id);
        }
    }
}

