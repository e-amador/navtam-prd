﻿namespace NavCanada.Services.Api.Diagnostics.Data.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using NavCanada.Services.Api.Diagnostics.Models;

    public class LogConfig : EntityTypeConfiguration<Log>
    {
        public LogConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Date).IsRequired();
            Property(e => e.Thread).IsRequired();
            Property(e => e.Level).IsRequired();
            Property(e => e.Logger).IsRequired();
            Property(e => e.Message).IsRequired();
            Property(e => e.Exception).IsRequired();
        }
    }
}
