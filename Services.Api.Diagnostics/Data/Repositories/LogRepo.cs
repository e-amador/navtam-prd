﻿namespace NavCanada.Services.Api.Diagnostics.Data.Repositories
{
    using System.Data.Entity;

    using NavCanada.Services.Api.Diagnostics.Data.Contracts;
    using NavCanada.Services.Api.Diagnostics.Models;

    public class LogRepo : Repository<Log>, ILogRepo
    {
        public LogRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}
