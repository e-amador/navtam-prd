﻿namespace NavCanada.Services.Api.Diagnostics.Data.Repositories
{
    using System.Data.Entity;

    using NavCanada.Services.Api.Diagnostics.Models;

    public class KeepAliveRepo : Repository<KeepAlive>, Services.Api.Diagnostics.Data.Contracts.IKeepAliveRepo
    {
        public KeepAliveRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}
