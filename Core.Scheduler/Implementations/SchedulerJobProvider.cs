﻿using System;
using System.Collections.Generic;
using NavCanada.Core.Scheduler.Contracts;

namespace NavCanada.Core.Scheduler.Implementations
{
    public class SchedulerJobProvider : ISchedulerJobProvider
    {
        public SchedulerJobProvider(SchedulerJobFactory schedulerJobFactory)
        {
            _schedulerJobFactory = schedulerJobFactory;
        }

        public IScheduleJob GetScheduleJob(string jobEntityName)
        {
            return _schedulerJobFactory.GetSchedulerJob(jobEntityName);
        }

        public List<IScheduleJob> GetAllScheduleJobs()
        {
            return _schedulerJobFactory.GetSchedulerJobs();
        }

        private readonly SchedulerJobFactory _schedulerJobFactory;

    }
}
