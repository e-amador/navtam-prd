﻿namespace NavCanada.Core.Scheduler.Implementations
{
    using System;

    using Common.Contracts;
    using Data.NotamWiz.Contracts;
    using Contracts;

    public sealed class ClusterStorage : IClusterStorage, IDisposable
    {
        public ClusterStorage(IUow uow, IClusterQueues obuQueues)
        {
            Uow = uow;
            Queues = obuQueues;
        }

        public IUow Uow
        {
            get;
            set;
        }

        public IClusterQueues Queues
        {
            get;
            set;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ClusterStorage()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                ReleaseManagedResources();
            }
            ReleaseUnmangedResources();
        }

        private void ReleaseManagedResources()
        {
            var disposable = Uow as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }

        private void ReleaseUnmangedResources()
        {
        }
    }
}
