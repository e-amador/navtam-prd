﻿using System.Collections.Generic;

namespace NavCanada.Core.Scheduler.Contracts
{
    public interface ISchedulerJobProvider
    {
        IScheduleJob GetScheduleJob(string jobEntityName);
        List<IScheduleJob> GetAllScheduleJobs();
    }
}
