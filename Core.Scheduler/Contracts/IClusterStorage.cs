﻿namespace NavCanada.Core.Scheduler.Contracts
{
    using Common.Contracts;
    using Data.NotamWiz.Contracts;

    public interface IClusterStorage
    {
        IUow Uow { get; set; }
        IClusterQueues Queues { get; set; }
    }
}
