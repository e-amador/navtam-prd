﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Scheduler.Contracts;
using NavCanada.Core.Scheduler.Implementations;
using NavCanada.Core.SqlServiceBroker;
using Quartz;
using System;

namespace NavCanada.Core.Scheduler.ScheduleJobs
{
    public abstract class ScheduleJob : IScheduleJob
    {

        public ScheduleJob()
        {
        }

        //Just for testing purposes
        public ScheduleJob(IJobDetail job, ITrigger trigger, IClusterStorage clusterStorage)
        {
            _job = job;
            _trigger = trigger;
            _clusterStorage = clusterStorage;
        }

        public IClusterStorage ClusterStorage
        {
            get
            {
                if (_clusterStorage == null)
                {
                    var provider = new RepositoryProvider(new RepositoryFactories());
                    var uow = new Uow(provider);

                    IClusterQueues clusterQueues = new ClusterQueues(
                        new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings),
                        new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings));

                    _clusterStorage = new ClusterStorage(uow, clusterQueues);
                }
                return _clusterStorage;
            }
        }

        public abstract IJobDetail Job { get; }

        public abstract ITrigger Trigger { get; }

        public static bool TriggerShouldFire(DateTime last, DateTimeOffset current, DateTimeOffset next)
        {
            var lastOffset = DateTime.SpecifyKind(last, DateTimeKind.Utc);
            var scheduleInSecs = (next - current).TotalSeconds;

            return (current - lastOffset).TotalSeconds >= scheduleInSecs;
        }

        protected IJobDetail _job;
        protected ITrigger _trigger;
        protected IClusterStorage _clusterStorage;
    }
}
