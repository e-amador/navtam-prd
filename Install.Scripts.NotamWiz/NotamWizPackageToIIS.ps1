Add-Type -Assembly "System.IO.Compression.FileSystem";
Set-PSDebug -Trace 1

$buildNumber=$args[0];
$config = $args[1];
$deployPath = $args[2];
$iisDir = $args[3];
$workspace = $args[4];

 
#where to put the version information
$versionPath = $deployPath
$versionText =  "Version: " + $buildNumber

$strFileName = $versionPath + "\version.txt";

$pathExists = Test-Path $versionPath

#create directory
if((Test-Path $versionPath) -eq 0) {    
    mkdir -Path $versionPath
}

# delete file if exists
If (Test-Path $strFileName){
	Remove-Item $strFileName
}

#create new file for version information
New-Item -Path $versionPath -Name "version.txt" -ItemType File -value $versionText 
 
# Copy website installer to the deploy folder
$fromPath = "C:\workspace\" + $workspace + "\Install.Scripts.NotamWiz\NotamWizWebsiteInstall.ps1";
$topath = $deployPath + "\NotamWizWebsiteInstall.ps1";
Copy-Item -Path $fromPath  -Destination $toPath;

$fromPath = "C:\workspace\" + $workspace + "\Install.Scripts.NotamWiz\NotamWizWebsiteInstall.bat";
$toPath = $deployPath + "\NotamWizWebsiteInstall.bat";
Copy-Item -Path $fromPath  -Destination $toPath;

$fromPath = "C:\workspace\" + $workspace + "\Install.Scripts.NotamWiz\NotamWizServiceInstall.ps1";
$toPath = $deployPath + "\NotamWizServiceInstall.ps1";
Copy-Item -Path $fromPath  -Destination $toPath;

$fromPath = "C:\workspace\"+$workspace+"\Packages\EntityFramework.6.1.3\tools\migrate.exe";
$toPath = $deployPath + "\migrate.exe";
Copy-Item -Path $fromPath -Destination $toPath;


$path = $iisDir + "\NOTAM_TEST_Build_" + $config + "_" + $buildNumber +  ".zip"; 
if(Test-Path $path) {
    Remove-Item $path;
}

[System.IO.Compression.ZipFile]::CreateFromDirectory($deployPath, $path);
