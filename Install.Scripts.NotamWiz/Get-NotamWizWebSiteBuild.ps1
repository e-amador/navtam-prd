﻿Add-Type -Assembly "System.IO.Compression.FileSystem";

$buildNumber=$args[0];

$downloadFile = "NotamWizWebsite.zip";
$source = "http://jenkins.deveng.local/Build/NotamWiz/NotamWiz_Build_" + $buildNumber + ".zip";
$destination = "c:\Temp\Builds\" + $downloadFile;
$targetFolder= "c:\Temp\Builds\NotamWizWebsite";

# unistall NOTAMWizWorker
#if(Test-Path "c:\Temp\Builds\NotamWizWebsite\NOTAMWizWorker\NOTAMWizWorker.exe")
#{
#    Invoke-Expression -Command:"c:\Temp\Builds\NotamWizWebsite\NOTAMWizWorker\NOTAMWizWorker.exe -uninstall";
#}

# Shut down IIS, Need this because some files are locked and this prevents the install was successfully completing
stop-service W3SVC -Force
 
if(Test-Path "c:\Temp\Builds\NotamWizWebsite")
{
    Remove-Item -Path c:\Temp\Builds\NotamWizWebsite -Force -Recurse
}

Invoke-WebRequest $source -OutFile $destination

[System.IO.Compression.ZipFile]::ExtractToDirectory($destination, $targetFolder)

# Install the Web Site

Invoke-Expression -Command:"c:\Temp\Builds\NotamWizWebsite\SetupNotamWizSQLServer.ps1 -server localhost -username sa -password Navcan1! -environment Test";
Invoke-Expression -Command:"c:\Temp\Builds\NotamWizWebsite\NotamWizWebsiteInstall.bat";

#Start IIS
start-service W3SVC

#Now that the NOTAMWiz site is up and running, now setup the NOTAMWizWorker Service 

#First, we will replace some app config settings.
$NotamWizServiceFolder = "NOTAMWizWorker";
$NotamWizServiceAppConfig = $targetFolder + "\" + $NotamWizServiceFolder + "\" + "NOTAMWizWorker.exe.config";
$NotamWizService = $targetFolder + "\" + $NotamWizServiceFolder + "\" + "NOTAMWizWorker.exe";

$LogConnectionString = "Data Source=localhost;Initial Catalog=NOTAMWiz_State;User Id=NotamWizTestUser;Password=Navcan1!;";
$NotamWizDBContextConnectionString = "data source=localhost;initial catalog=NOTAMWiz_Test;user id=NotamWizTestUser;password=Navcan1!";
$NotamWizNotificationDbContextConnectionString = "data source=localhost;initial catalog=NOTAMWiz_Test;user id=NotamWizTestUser;password=Navcan1!";
$NotamWizUrlAppKey = "http://localhost:81";
$NotamWizRemoteLoginAppKey = $NotamWizUrlAppKey + "/Account/RemoteLogin";

$appConfig = New-Object XML;
# load the config file as an xml object
$appConfig.Load($NotamWizServiceAppConfig);

# iterate over the connection string settings
foreach($connectionString in $appConfig.configuration.connectionStrings.add)
{
    # write the name to the console
    # 'name: ' + $connectionString.name

    if($connectionString.name -eq "LogConnection")
    {
        $connectionString.connectionString = $LogConnectionString;
    }
    elseif($connectionString.name -eq "NOTAMWizDBContext")
    {
        $connectionString.connectionString = $NotamWizDBContextConnectionString;
    }
    elseif($connectionString.name -eq "NotamWizNotificationDbContext")
    {
        $connectionString.connectionString = $NotamWizNotificationDbContextConnectionString;
    }

    # write the connection string to the console
    # 'connectionString: ' + $connectionString.connectionString
}


# iterate over the app keys string settings
foreach($appKey in $appConfig.configuration.appSettings.add)
{
    # write the name to the console
    #'appKey: ' + $appKey.key;

    if($appKey.key -eq "NOTAMWiz_URL")
    {
        $appKey.value = $NotamWizUrlAppKey;
    }
    elseif($appKey.key -eq "NOTAMWiz_LOGIN_URL")
    {
        $appKey.value = $NotamWizRemoteLoginAppKey;
    }

    # write the connection string to the console
    #'appKey.value: ' + $appKey.value;
}

# save the updated config file
$appConfig.Save($NotamWizServiceAppConfig);

# Invoke-Expression -Command:"c:\Temp\Builds\NotamWizWebsite\NOTAMWizWorker\NOTAMWizWorker.exe -install";

