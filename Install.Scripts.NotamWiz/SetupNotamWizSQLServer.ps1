﻿ param (
    [string]$server = $(Throw "Please provide a server name or address, use the -server argument"),
    [string]$username = $(Throw "Please provide a SQL Server user name, use the -username argument"),
    [string]$password = $(Throw "Please provide a SQL Server password, use the -password argument"),
    [string]$environment = $(Throw "Please provide an environment, (either Test, Staging or Production) use the -environment argument")
 )

$StateDatabaseName="NOTAMWiz_State";
$ServerDataPath = "";
$DestDatabase = "NOTAMWiz";

$GlobalSDOReader = "GlobalSDOReader";
$GlobalSDOReaderPassword = "password";

$NotamWizUser= "NotamWizUser";
$NotamWizUserPassword = "Navcan1!";

if($environment -eq "Test")
{
    $ServerDataPath = "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA";
    $DestDatabase = "NOTAMWiz_Test"; 
    $NotamWizUser= "NotamWizTestUser";
    $NotamWizUserPassword = "Navcan1!";
}
elseif($environment -eq "Staging")
{
    $ServerDataPath = "D:\SQL\MSSQL11.MSSQLSERVER\MSSQL\DATA";
    $DestDatabase = "NOTAMWiz_Staging";

    $NotamWizUser = "NotamWizStagingUser";
    $NotamWizUserPassword = "Navcan1!";
}
elseif($environment -eq "Production")
{
    $ServerDataPath = "D:\SQL\MSSQL11.MSSQLSERVER\MSSQL\DATA";
    $DestDatabase = "NOTAMWiz_Production";
    
    $NotamWizUser = "NotamWizProductionUser";
    $NotamWizUserPassword = "!ArdsNav7";
    $GlobalSDOReaderPassword = "!GsdoRNav9";
}
else
{
    throw "Invalid environment value - please use Test, Staging or Production";
}

Function DatabaseCreationString([string]$DataPath, [string]$DatabaseName)
{
    $DatabaseLogName = $DatabaseName + "_log";

   "IF NOT EXISTS (SELECT name FROM master.sys.databases WHERE name = N'$DatabaseName')
    BEGIN
        BEGIN
            USE [master]
        END

        BEGIN
            CREATE DATABASE $DatabaseName
             CONTAINMENT = NONE
             ON  PRIMARY 
            ( NAME = N'$DatabaseName', FILENAME = N'$DataPath\$DatabaseName.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
             LOG ON 
            ( NAME = N'$DatabaseLogName', FILENAME = N'$DataPath\$DatabaseLogName.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
        END

        IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
        BEGIN
            EXEC $DatabaseName.[dbo].[sp_fulltext_database] @action = 'enable'
        END

        BEGIN
            ALTER DATABASE $DatabaseName SET ANSI_NULL_DEFAULT OFF 
            ALTER DATABASE $DatabaseName SET ANSI_NULLS OFF 
            ALTER DATABASE $DatabaseName SET ANSI_PADDING OFF 
            ALTER DATABASE $DatabaseName SET ANSI_WARNINGS OFF 
            ALTER DATABASE $DatabaseName SET ARITHABORT OFF 
            ALTER DATABASE $DatabaseName SET AUTO_CLOSE OFF 
            ALTER DATABASE $DatabaseName SET AUTO_SHRINK OFF 
            ALTER DATABASE $DatabaseName SET AUTO_UPDATE_STATISTICS ON 
            ALTER DATABASE $DatabaseName SET CURSOR_CLOSE_ON_COMMIT OFF 
            ALTER DATABASE $DatabaseName SET CURSOR_DEFAULT  GLOBAL 
            ALTER DATABASE $DatabaseName SET CONCAT_NULL_YIELDS_NULL OFF 
            ALTER DATABASE $DatabaseName SET NUMERIC_ROUNDABORT OFF 
            ALTER DATABASE $DatabaseName SET QUOTED_IDENTIFIER OFF 
            ALTER DATABASE $DatabaseName SET RECURSIVE_TRIGGERS OFF 
            ALTER DATABASE $DatabaseName SET ENABLE_BROKER 
            ALTER DATABASE $DatabaseName SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
            ALTER DATABASE $DatabaseName SET DATE_CORRELATION_OPTIMIZATION OFF 
            ALTER DATABASE $DatabaseName SET TRUSTWORTHY OFF 
            ALTER DATABASE $DatabaseName SET ALLOW_SNAPSHOT_ISOLATION OFF 
            ALTER DATABASE $DatabaseName SET PARAMETERIZATION SIMPLE 
            ALTER DATABASE $DatabaseName SET READ_COMMITTED_SNAPSHOT ON 
            ALTER DATABASE $DatabaseName SET HONOR_BROKER_PRIORITY OFF 
            ALTER DATABASE $DatabaseName SET RECOVERY FULL 
            ALTER DATABASE $DatabaseName SET MULTI_USER 
            ALTER DATABASE $DatabaseName SET PAGE_VERIFY CHECKSUM  
            ALTER DATABASE $DatabaseName SET DB_CHAINING OFF 
            ALTER DATABASE $DatabaseName SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
            ALTER DATABASE $DatabaseName SET TARGET_RECOVERY_TIME = 0 SECONDS 
            ALTER DATABASE $DatabaseName SET READ_WRITE 
        END
    END";
}


Function DatabaseDropString([string]$DatabaseName)
{
   "IF EXISTS (SELECT name FROM master.sys.databases WHERE name = N'$DatabaseName')
    BEGIN
        BEGIN
            USE [master]
        END

        BEGIN
            DROP DATABASE $DatabaseName
        END
    END";
}

Function ConnectionStringS ([string] $ServerName, [string] $DbName, [string] $User, [string] $Pwd)
{
  "Server=$ServerName;uid=$User; pwd=$Pwd;Database=$DbName;Integrated Security=False;";
} 

########## Main body ############

$sqlStatement = "";
$ConnStr = ConnectionStringS $server "master" $username $password;
$Conn  = New-Object System.Data.SqlClient.SQLConnection($ConnStr);

 Try
 {
    $Conn.Open();

    if($environment -ne "Production")
    {
        # Drop the NotamWiz_State database, if it exists
        $sqlStatement = DatabaseDropString $StateDatabaseName;
        # Write-Host $sqlStatement;
        $SqlCommand = New-Object system.Data.SqlClient.SqlCommand($sqlStatement, $Conn);
        $sqlCommand.ExecuteNonQuery();
    }

    # Create the NotamWiz_State database
    $sqlStatement = DatabaseCreationString $ServerDataPath $StateDatabaseName;
    # Write-Host $sqlStatement;
    $SqlCommand = New-Object system.Data.SqlClient.SqlCommand($sqlStatement, $Conn);
    $sqlCommand.ExecuteNonQuery();

    # SQL Server Registration Tool, for membership and such 
    $aspNetCmd = "c:\windows\microsoft.net\framework\v4.0.30319\aspnet_regsql.exe -S localhost -U $username -P $password -d $StateDatabaseName -ssadd -sstype c";
    Invoke-Expression -Command: $aspNetCmd;

    if($environment -ne "Production")
    {
        # Drop the NotamWiz database, if it exists
        $sqlStatement = DatabaseDropString $DestDatabase;
        # Write-Host $sqlStatement;
        $SqlCommand = New-Object system.Data.SqlClient.SqlCommand($sqlStatement, $Conn);
        $sqlCommand.ExecuteNonQuery();
    }

    # Create the NotamWiz database
    $sqlStatement = DatabaseCreationString $ServerDataPath $DestDatabase;
    # Write-Host $sqlStatement;
    $SqlCommand = New-Object system.Data.SqlClient.SqlCommand($sqlStatement, $Conn);
    $sqlCommand.ExecuteNonQuery();

    $sqlStatement = "IF NOT EXISTS (SELECT name  FROM master.sys.server_principals WHERE name = '$GlobalSDOReader')
                     BEGIN
                        BEGIN
    	                    CREATE LOGIN [$GlobalSDOReader] WITH PASSWORD=N'$GlobalSDOReaderPassword', DEFAULT_DATABASE=[GlobalSDO], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
                        END;
                        BEGIN
                        	USE [GlobalSDO]
                        END;
                        BEGIN
                            CREATE USER [$GlobalSDOReader] FOR LOGIN [$GlobalSDOReader]
                            GRANT SELECT TO $GlobalSDOReader;
                            GRANT EXECUTE TO $GlobalSDOReader;
                        END;
                     END";

    $SqlCommand = New-Object system.Data.SqlClient.SqlCommand($sqlStatement, $Conn);
    $sqlCommand.ExecuteNonQuery();

    $sqlStatement = "IF EXISTS (SELECT name FROM master.sys.server_principals WHERE name = '$NotamWizUser')
                     BEGIN
                        USE [Master]
                     END;
                     BEGIN
                        DROP LOGIN [$NotamWizUser]
                     END;";

    $SqlCommand = New-Object system.Data.SqlClient.SqlCommand($sqlStatement, $Conn);
    $sqlCommand.ExecuteNonQuery();


    $sqlStatement = "
                    BEGIN
    	                CREATE LOGIN [$NotamWizUser] WITH PASSWORD=N'$NotamWizUserPassword', DEFAULT_DATABASE=[$DestDatabase], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
                    END;
                    BEGIN
                        USE [Master];
                    END;
                    BEGIN
                        GRANT CREATE ANY DATABASE TO [$NotamWizUser];
                    END;

                    BEGIN
                        USE [$StateDatabaseName];
                    END;
                    BEGIN
                        CREATE USER [$NotamWizUser] FOR LOGIN [$NotamWizUser]
                        GRANT SELECT TO $NotamWizUser;
                        GRANT EXECUTE TO $NotamWizUser;
                        GRANT INSERT TO $NotamWizUser;
                        GRANT UPDATE TO $NotamWizUser;
                        GRANT DELETE TO $NotamWizUser;
                        GRANT CREATE TABLE TO [$NotamWizUser];
                        GRANT ALTER ON SCHEMA::dbo TO $NotamWizUser;
                        GRANT REFERENCES TO $NotamWizUser;
                    END;
                    BEGIN
                        USE [$DestDatabase];
                    END;
                    BEGIN
                        CREATE USER [$NotamWizUser] FOR LOGIN [$NotamWizUser]
                        GRANT SELECT TO $NotamWizUser;
                        GRANT EXECUTE TO $NotamWizUser;
                        GRANT INSERT TO $NotamWizUser;
                        GRANT UPDATE TO $NotamWizUser;
                        GRANT DELETE TO $NotamWizUser;
                        GRANT CREATE TABLE TO [$NotamWizUser];
                        GRANT ALTER ON SCHEMA::dbo TO $NotamWizUser;
                        GRANT REFERENCES TO $NotamWizUser;
                    END;";

    $SqlCommand = New-Object system.Data.SqlClient.SqlCommand($sqlStatement, $Conn);
    $sqlCommand.ExecuteNonQuery();

    # Write-Host $sqlStatement;

    Write-Host "Database users were created successfully.";
 }
 Catch [System.Exception]
 {
    $ex = $_.Exception
    Write-Host $ex.Message
 }
 Finally
 {
    $Conn.Close();
 }
