﻿namespace NavCanada.Services.Win.TaskEngine
{
    using System.Runtime.InteropServices;

    using Enum;

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public readonly long ServiceType;
        public ServiceState CurrentState;
        public readonly long ControlsAccepted;
        public readonly long Win32ExitCode;
        public readonly long ServiceSpecificExitCode;
        public readonly long CheckPoint;
        public long WaitHint;
    };
}
