﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;

namespace Apps.Web.NotamWiz.Test.Admin
{
    [TestClass]
    public class AerodromeDisseminationCategoryControllerTest
    {

        private AerodromeDisseminationCategoryController _controller;
        protected Mock<IUow> UowMock { get; set; }
        protected Mock<IClusterQueues> ClusterQueuesMock { get; set; }

        [TestInitialize]
        public void InitializeTest()
        {

            UowMock = new Mock<IUow>();
            ClusterQueuesMock = new Mock<IClusterQueues>();

            _controller = new AerodromeDisseminationCategoryController(UowMock.Object, ClusterQueuesMock.Object);
        }

        [TestMethod]
        public void Index_should_return_a_result_view_with_DisseminationLevels_in_viewbag()
        {
            var httpConterxt = MvcMockHelpers.MockHttpContext("~/Admin/");
            var controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.SetupGet(p => p.HttpContext).Returns(httpConterxt);

            _controller.ControllerContext = controllerContextMock.Object;

            var result = _controller.Index();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ActionResult));
            
            var model = (result as ViewResult).Model as List<AerodromeDisseminationCategoryController.ValueText>;

            Assert.IsNotNull(model);
            Assert.AreEqual(model.Count, 3);
        }
    }
}
