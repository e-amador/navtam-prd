﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using NavCanada.Applications.NotamWiz.Web.Code.IdentityManagers;
using NavCanada.Applications.NotamWiz.Web.NSDs.CatchAll.V0R1;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Mto;

namespace Apps.Web.NotamWiz.Test.Utilities
{
    [TestClass]
    public class UtilsTest
    {
        private IUtility _utilityMock;

        [TestInitialize]
        public void InitializeTest()
        {
            var uowMock = new Mock<IUow>();
            var clusterQueuesMock = new Mock<IClusterQueues>();
            var userStoreMock = new Mock<IUserStore<UserProfile>>();
            var roleStore = new Mock<IRoleStore<ApplicationRole, string>>();

            var controllerMock = new Mock<CatchAllController>(uowMock.Object, clusterQueuesMock.Object);
            var usrMngrMock = new Mock<AppUserManager>(userStoreMock.Object);
            var roleMngrMock = new Mock<AppRoleManager>(roleStore.Object);

            _utilityMock = new Utility(controllerMock.Object, uowMock.Object, usrMngrMock.Object, roleMngrMock.Object);

        }

        [TestMethod]
        public void CreateMtoForUserRegistration_ShouldCreateProperMto_WhenInitalRegistrationIsInvoke()
        {
            var registrationId = 1;
            var mtoId = MessageTransferObjectId.RegistrationNotificationEmail;
            var regStatus = RegistrationStatus.Initial;

            var mto = _utilityMock.CreateMtoForUserRegistration(mtoId, null, registrationId, regStatus, null);

            Assert.IsTrue(mto.Count == 6);
            Assert.IsTrue(Guid.Parse(mto[MessageDefs.UniqueIdentifierKey].ToString()) != Guid.Empty);
            Assert.IsTrue((MessageType)byte.Parse(mto[MessageDefs.MessageTypeKey].ToString()) == MessageType.WorkFlowTask);
            Assert.IsTrue((RegistrationStatus)byte.Parse(mto[MessageDefs.RegistrationStatusKey].ToString()) == regStatus);
            Assert.IsTrue((MessageTransferObjectId)byte.Parse(mto[MessageDefs.MessageIdKey].ToString()) == mtoId);
            Assert.IsTrue(int.Parse(mto[MessageDefs.EntityIdentifierKey].ToString()) == registrationId);
            Assert.IsTrue(mto[MessageDefs.MessageTimeKey].ToString() != null);
        }

        [TestMethod]
        public void CreateMtoForUserRegistration_ShouldCreateProperMto_WhenCompleteRegistrationIsInvoke()
        {
            var registrationId = 1;
            var usrId = "user";
            var mtoId = MessageTransferObjectId.RegistrationNotificationEmail;
            var regStatus = RegistrationStatus.Completed;

            var mto = _utilityMock.CreateMtoForUserRegistration(mtoId, usrId, registrationId, regStatus, null);

            Assert.IsTrue(mto.Count == 6);
            Assert.IsTrue(Guid.Parse(mto[MessageDefs.UniqueIdentifierKey].ToString()) != Guid.Empty);
            Assert.IsTrue((MessageType)byte.Parse(mto[MessageDefs.MessageTypeKey].ToString()) == MessageType.WorkFlowTask);
            Assert.IsTrue((RegistrationStatus)byte.Parse(mto[MessageDefs.RegistrationStatusKey].ToString()) == regStatus);
            Assert.IsTrue((MessageTransferObjectId)byte.Parse(mto[MessageDefs.MessageIdKey].ToString()) == mtoId);
            Assert.IsTrue(mto[MessageDefs.EntityIdentifierKey].ToString() == usrId);
            Assert.IsTrue(mto[MessageDefs.MessageTimeKey].ToString() != null);
        }

        [TestMethod]
        public void CreateMtoForUserRegistration_ShouldCreateProperMto_WhenRejectedRegistrationIsInvoke()
        {
            var registrationId = 1;
            var mtoId = MessageTransferObjectId.RegistrationNotificationEmail;
            var regStatus = RegistrationStatus.Rejected;

            var mto = _utilityMock.CreateMtoForUserRegistration(mtoId, null, registrationId, regStatus, null);

            Assert.IsTrue(mto.Count == 6);
            Assert.IsTrue(Guid.Parse(mto[MessageDefs.UniqueIdentifierKey].ToString()) != Guid.Empty);
            Assert.IsTrue((MessageType)byte.Parse(mto[MessageDefs.MessageTypeKey].ToString()) == MessageType.WorkFlowTask);
            Assert.IsTrue((RegistrationStatus)byte.Parse(mto[MessageDefs.RegistrationStatusKey].ToString()) == regStatus);
            Assert.IsTrue((MessageTransferObjectId)byte.Parse(mto[MessageDefs.MessageIdKey].ToString()) == mtoId);
            Assert.IsTrue(int.Parse(mto[MessageDefs.EntityIdentifierKey].ToString()) == registrationId);
            Assert.IsTrue(mto[MessageDefs.MessageTimeKey].ToString() != null);
        }

        [TestMethod]
        public void CreateMtoForUserRegistration_ShouldCreateProperMto_WhenConfirmRegistrationIsInvoke()
        {
            var registrationId = 1;
            var mtoId = MessageTransferObjectId.RegistrationNotificationEmail;
            var regStatus = RegistrationStatus.ConfirmEmail;
            var token = "verification-token";

            var mto = _utilityMock.CreateMtoForUserRegistration(mtoId, null, registrationId, regStatus, token);

            Assert.IsTrue(mto.Count == 7);
            Assert.IsTrue(Guid.Parse(mto[MessageDefs.UniqueIdentifierKey].ToString()) != Guid.Empty);
            Assert.IsTrue((MessageType)byte.Parse(mto[MessageDefs.MessageTypeKey].ToString()) == MessageType.WorkFlowTask);
            Assert.IsTrue((RegistrationStatus)byte.Parse(mto[MessageDefs.RegistrationStatusKey].ToString()) == regStatus);
            Assert.IsTrue((MessageTransferObjectId)byte.Parse(mto[MessageDefs.MessageIdKey].ToString()) == mtoId);
            Assert.IsTrue(int.Parse(mto[MessageDefs.EntityIdentifierKey].ToString()) == registrationId);
            Assert.IsTrue(mto[MessageDefs.MessageTimeKey].ToString() != null);
            Assert.IsTrue(mto[MessageDefs.EmailTokenConfirmationKey].ToString() == token);
        }

    }
}
