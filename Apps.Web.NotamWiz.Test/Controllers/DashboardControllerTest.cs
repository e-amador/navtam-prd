﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NavCanada.Applications.NotamWiz.Web.Controllers;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using NavCanada.Applications.NotamWiz.Web.Code.Configurations;
using NavCanada.Applications.NotamWiz.Web.Code.IdentityManagers;
using NavCanada.Applications.NotamWiz.Web.Models;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using Newtonsoft.Json;

namespace Apps.Web.NotamWiz.Test.Controllers
{
    [TestClass]
    public class DashboardControllerTest
    {
        private DashboardController _controller;

        private Mock<IUow> _uowMock;
        private Mock<IClusterQueues> _clusterQueuesMock;

        private HttpContextBase _httpContext;
        private Mock<ControllerContext> _controllerContextMock;

        private UserProfile _user;
        private UserPreference _userPreference;
        private Organization _organization;
        private Doa _doa;
        private List<Nsd>_nsds;
        private List<NotamPackage> _packages;
        private NsdVersion _nsdVersion;

        private Mock<IOrganizationRepo> _organizationRepoMock;
        private Mock<IDoaRepo> _doaRepoMock;
        private Mock<INsdRepo> _nsdRepoMock;
        private Mock<IUserPreferenceRepo> _userPreferenceRepoMock;
        private Mock<INotamPackageRepo> _notamPackageRepoMock;
        private Mock<INsdVersionRepo> _nsdVersionRepoMock;

        [TestInitialize]
        public void InitializeTest()
        {
            AutoMapperConfig.RegisterMaps();

            _uowMock = new Mock<IUow>();
            _clusterQueuesMock = new Mock<IClusterQueues>();
            _httpContext = MvcMockHelpers.MockHttpContext("~/Admin/");
            _controllerContextMock = new Mock<ControllerContext>();

            _organizationRepoMock = new Mock<IOrganizationRepo>();
            _doaRepoMock = new Mock<IDoaRepo>();
            _nsdRepoMock = new Mock<INsdRepo>();
            _userPreferenceRepoMock = new Mock<IUserPreferenceRepo>();
            _notamPackageRepoMock = new Mock<INotamPackageRepo>();
            _nsdVersionRepoMock = new Mock<INsdVersionRepo>();

            _user = new UserProfile
            {
                Id = "1",
                OrganizationId = 1,
                UserPreferenceId = 1
            };
            _doa = new Doa
            {
                Id = 1,
            };
            _organization = new Organization
            {
                Id = 1,
                AssignedDoaId = 1,
                AssignedDoa = _doa
            };
            _nsds = new List<Nsd>
            {
                new Nsd {Id = 1, Name="OBST",  Category="Obstacle", LimitedAccess=false, FilesLocation="" },
                new Nsd {Id = 2, Name="CATCH ALL",  Category="CATCH ALL", LimitedAccess=false, FilesLocation="" },
                new Nsd {Id = 3, Name="OBST LGT U/S",  Category="Obstacle", LimitedAccess=false, FilesLocation="" },
                new Nsd {Id = 4, Name="RWY CLSD",  Category="Rwy", LimitedAccess=false, FilesLocation="" },
                new Nsd {Id = 5, Name="TWY CLSD",  Category="Twy", LimitedAccess=false, FilesLocation="" },
                new Nsd {Id = 6, Name="CARs CLSD",  Category="Service", LimitedAccess=false, FilesLocation="" },
                new Nsd {Id = 7, Name="NAVAIDs",  Category="NAVAIDs", LimitedAccess=false, FilesLocation="" },
            };
            _userPreference = new UserPreference
            {
                Id =1,
                ExpiredRecordsDateFilter =  1
            };
            var proposals = new List<NotamProposal>
            {
                new NotamProposal {Id = 1, Status = NotamProposalStatusCode.JustForTestingPurposesNotToUseInProduction}
            };
            _packages = new List<NotamPackage>
            {
                new NotamPackage {Id = 1, Name = "OBST LGT U/S", NsdId = 3, SubjectCategory = DashboardItemType.Sdo, Proposals = proposals  },
                new NotamPackage {Id = 2, Name = "OBST", NsdId = 1, SubjectCategory = DashboardItemType.Sdo, Proposals = proposals},
                new NotamPackage {Id = 3, Name = "CARs CLSD", NsdId = 6, SubjectCategory = DashboardItemType.Sdo, Proposals = proposals},
                new NotamPackage {Id = 4, Name = "RWY CLSD", NsdId = 4, SubjectCategory = DashboardItemType.Sdo, Proposals = proposals},
                new NotamPackage {Id = 5, Name = "NAVAIDs", NsdId = 7, SubjectCategory = DashboardItemType.Sdo, Proposals = proposals}
            };
            _nsdVersion = new NsdVersion
            {
                Id = 1,
                NoUserInputCancel = false,
                Version = 0.1f
            };

            var store = new Mock<IUserStore<UserProfile>>(MockBehavior.Strict);
            var roleStore = new Mock<IRoleStore<NavCanada.Core.Domain.Model.Entitities.ApplicationRole, string>>(MockBehavior.Strict);

            store.As<IUserStore<UserProfile>>().Setup(x => x.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(_user);
            _controllerContextMock.SetupGet(p => p.HttpContext).Returns(_httpContext);

            var mockAppRoleManager = new Mock<AppRoleManager>(roleStore.Object);
            var mockAppUserManager = new Mock<AppUserManager>(store.Object);
            mockAppUserManager.Setup(e => e.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(_user);

            _uowMock.Setup(e => e.OrganizationRepo).Returns(_organizationRepoMock.Object);
            _uowMock.Setup(e => e.DoaRepo).Returns(_doaRepoMock.Object);
            _uowMock.Setup(e => e.NsdRepo).Returns(_nsdRepoMock.Object);
            _uowMock.Setup(e => e.UserPreferenceRepo).Returns(_userPreferenceRepoMock.Object);
            _uowMock.Setup(e => e.NotamPackageRepo).Returns(_notamPackageRepoMock.Object);
            _uowMock.Setup(e => e.NsdVersionRepo).Returns(_nsdVersionRepoMock.Object);

            _organizationRepoMock.Setup(e => e.GetById(It.IsAny<object>())).Returns(_organization);
            _organizationRepoMock.Setup(e => e.GetWithDoaIdAsync(It.IsAny<long>())).ReturnsAsync(_organization);
            _doaRepoMock.Setup(e => e.GetByIdWithFiltersAsync(It.IsAny<long>())).ReturnsAsync(_doa);
            _nsdRepoMock.Setup(e => e.GetAllActiveNSDsAsync()).ReturnsAsync(_nsds);
            _userPreferenceRepoMock.Setup(e => e.GetById(It.IsAny<object>())).Returns(_userPreference);
            _nsdRepoMock.Setup(e => e.GetById(It.IsAny<long>())).Returns(_nsds[0]);
            _nsdVersionRepoMock.Setup(e => e.GetByVersion(It.IsAny<long>(), It.IsAny<float>())).Returns(_nsdVersion);

            _controller = new DashboardController(_uowMock.Object, _clusterQueuesMock.Object)
            {
                ControllerContext = _controllerContextMock.Object,
                UserManager = mockAppUserManager.Object,
                RoleManager = mockAppRoleManager.Object
            };
        }


        [TestMethod]
        public void TreeView_ShoudLoadAllDashboardTree_WhenEndpointIsCalled()
        {
            var result = _controller.TreeView().Result as ContentResult;

            var treeData = JsonConvert.DeserializeObject<dynamic>(result.Content);

            Assert.IsNotNull(result);
            Assert.AreEqual((string)treeData[0].nodeType, "Root");
            Assert.IsTrue((int)treeData[0].items.Count >= 6);

            foreach (var node in treeData[0].items)
            {
                if (node.items.Count > 0)
                {
                    Assert.AreEqual((string)node.nodeType, "Category");
                    foreach (var subNode in node.items)
                    {
                        Assert.AreEqual((string)subNode.nodeType, "Nsd");
                    }
                }
                else
                {
                    Assert.AreEqual((string)node.nodeType, "Nsd");
                }
            }
        }

        [TestMethod]
        public void LoadReviewList_ShoudReturnsAllNsds_WhenNodeTypeIsRoot()
        {
            var proposals = new List<NotamProposal>
            {
                new NotamProposal {Id = 1, Status = NotamProposalStatusCode.JustForTestingPurposesNotToUseInProduction}
            };

            _nsdRepoMock.Setup(e => e.GetById(It.IsAny<long>())).Returns(_nsds[0]);
            _nsdVersionRepoMock.Setup(e => e.GetByVersion(It.IsAny<long>(), It.IsAny<float>())).Returns(_nsdVersion);

            _notamPackageRepoMock.Setup(e => e.GetAllInPackageAsync(It.IsAny<DateTime>(), It.IsAny<DbGeography>()))
                .ReturnsAsync(_packages);

            var result = _controller.LoadReviewList(new DataSourceRequest(), new DashboardTreeModel{NodeType =NodeType.Root }).Result as JsonResult;

            var data = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(JsonConvert.SerializeObject(result.Data));
            var list = JsonConvert.DeserializeObject<List<NotamPackageViewModel>>(data.SelectToken("Data").ToString());

            Assert.AreEqual(list.Count, 5);
        }

        [TestMethod]
        public void LoadReviewList_ShoudReturnsAllNsdsInCategory_WhenNodeTypeIsObstacleCategory()
        {
            var obstPackages = _packages.Where(e => e.Name.Contains("OBST")).ToList();

            _notamPackageRepoMock.Setup(e => e.GetAllInPackageByCategoryAsync(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DbGeography>()))
                .ReturnsAsync(obstPackages);

            var result = _controller.LoadReviewList(new DataSourceRequest(), 
                new DashboardTreeModel
                {
                    NodeType = NodeType.Category,
                    Name = "Obstacle"
                }).Result as JsonResult;

            var data = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(JsonConvert.SerializeObject(result.Data));
            var list = JsonConvert.DeserializeObject<List<NotamPackageViewModel>>(data.SelectToken("Data").ToString());

            Assert.AreEqual(list.Count, 2);
        }

        [TestMethod]
        public void LoadReviewList_ShoudReturnsSingleNsd_WhenNodeTypeIsNsd()
        {
            var rwyClosePackage = _packages.Where(e => e.Id == 4).ToList();

            _notamPackageRepoMock.Setup(e => e.GetAllInPackageByNsdIdAsync(It.IsAny<long>(), It.IsAny<DateTime>(), It.IsAny<DbGeography>()))
                .ReturnsAsync(rwyClosePackage);

            var result = _controller.LoadReviewList(new DataSourceRequest(),
                new DashboardTreeModel
                {
                    NodeType = NodeType.Nsd,
                    Id = 1
                }).Result as JsonResult;

            var data = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(JsonConvert.SerializeObject(result.Data));
            var list = JsonConvert.DeserializeObject<List<NotamPackageViewModel>>(data.SelectToken("Data").ToString());

            Assert.AreEqual(list.Count, 1);
        }


    }
}
