﻿namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IScheduleManager
    {
        void Start();
        void Stop();

        void AddToContext(string key, object value);
        void RemoveFromContext(string key);

        bool CheckIfJobExist(string jobName);
        void DeleteJob(string jobName);
        void PauseJob(string jobName);
        void ResumeJob(string jobName);
        void FireJob(string jobName);

        void CreateSdoEffectiveCacheLoadSchedulerJob();
        void CreateSdoEffectiveCacheSyncSchedulerJob();
        void CreateChecklistSchedulerJob();
        void CreateCheckSeriesNumbersSchedulerJob();
        void CreateExpiredProposalJob();
        void CreateQueryListenerGuardJob();
        void CreateHubConnectionTestSchedulerJob();
        void CreateBroadcastNotamsToSubscribersJob();
    }
}
