﻿namespace NavCanada.Core.TaskEngine.Contracts
{
    /// <summary>
    /// Message Task interface
    /// <remarks>
    ///     Tasks should be thread-safe, they must allow for concurrent threads to re-entry the Execute method.
    /// </remarks>
    /// </summary>
    public interface IMessageTask
    {
        /// <summary>
        /// execute the specified message task
        /// </summary>
        /// <param name="context"></param>
        void Execute(IMessageTaskContext context);
    }
}
