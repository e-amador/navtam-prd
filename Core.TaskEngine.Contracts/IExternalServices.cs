﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.NotificationEngine;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.Proxies.DiagnosticProxy;

namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IExternalServices
    {
        IDiagnosticProxy DiagnosticProxy { get; }

        INotifier Notifier { get; }

        ISolaceEndpoint SolaceEndpoint { get; }

        IAeroRdsFeatureProxy AeroRdsFeatureProxy { get; }

        ILogger Logger { get; }

        IScheduleManager ScheduleManager { get; }
    }
}
