﻿namespace NavCanada.Core.TaskEngine.Mto
{
    public enum DataType
    {
        Byte,
        UInt16,
        UInt32,
        UInt64,
        Int16,
        Int32,
        Int64,
        Double,
        String,
        Char,
        Boolean,
        KeyValuePair,
        DateTime,
        ArrayOfByte
    }
}