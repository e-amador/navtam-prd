﻿namespace NavCanada.Core.TaskEngine.Mto
{
    public static class OutgoingMessageUtils
    {
        /// <summary>
        /// Echo control response sent to OBU
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="initiatorConversationHandle"></param>
        /// <param name="targetCoversationHandle"></param>
        /// <returns></returns>
        public static MessageTransferObject GetSubmitNotamProposalControlMto(string messageId, string initiatorConversationHandle = null, string targetCoversationHandle = null)
        {
            var mto = new MessageTransferObject();
            mto.Add(MessageDefs.MessageIdKey, messageId, DataType.String);
            if (initiatorConversationHandle != null && targetCoversationHandle != null)
            {
                mto.Add(MessageDefs.SenderConversationIdKey, initiatorConversationHandle, DataType.String);
                mto.Add(MessageDefs.ReceiverConversationIdKey, targetCoversationHandle, DataType.String);
            }
            return mto;
        }
    }
}