﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.TaskEngine.Mto
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(Array))]
    [KnownType(typeof(uint[]))]
    [KnownType(typeof(int[]))]
    [KnownType(typeof(long[]))]
    [KnownType(typeof(byte[]))]
    [KnownType(typeof(string[]))]
    [KnownType(typeof(MessageType))]
    [KnownType(typeof(ArrayOfByte))]
    public class MessageTransferObject
    {
        [DataMember]
        private Dictionary<string, PropertyData> _properties;

        public MessageTransferObject()
        {
            _properties = new Dictionary<string, PropertyData>();
        }

        public int Count
        {
            get
            {
                return _properties.Count;
            }
        }

        public PropertyData this[string propertyName]
        {
            get
            {
                return _properties.ContainsKey(propertyName) ? _properties[propertyName] : null;
            }
        }

        /// <summary>
        /// Gets all the names of the properties
        /// </summary>
        public IEnumerable<string> PropertiesNames
        {
            get
            {
                return _properties.Keys;
            }
        }

        /// <summary>
        /// Gets all the properties
        /// </summary>
        public IEnumerable<PropertyData> Properties
        {
            get
            {
                return _properties.Values;
            }
        }

        /// <summary>
        /// Gets a name-value pair of all the property names and values
        /// </summary>
        public IEnumerable<KeyValuePair<string, PropertyData>> NameValuePair
        {
            get
            {
                return _properties;
            }
        }

        public void Add(string propertyName, object propertyValue, DataType dataType, bool isArray = false)
        {
            _properties[propertyName] = new PropertyData(propertyValue, dataType, isArray);
        }

        public T GetProperty<T>(string name, T defaultValue)
        {
            var prop = this[name];
            return prop != null ? (T)prop.Value : defaultValue;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("{");
            foreach (var property in NameValuePair)
            {
                sb.AppendLine(string.Format(" {0} : {1},", property.Key, property.Value));
            }
            sb.AppendLine("}");
            return sb.ToString();
        }
    }
}