﻿using System;
using System.Runtime.Serialization;

namespace NavCanada.Core.TaskEngine.Mto
{
    [Serializable]
    [DataContract]
    public struct ArrayOfByte
    {
        [DataMember]
        public byte[] Bytes;
    }
}