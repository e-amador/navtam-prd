﻿using NavCanada.Core.TaskEngine.Mto;

namespace NavCanada.Core.TaskEngine.Contracts
{
    /// <summary>
    /// Represents the ability to proces Message Transfer Objects
    /// </summary>
    /// <remarks></remarks>
    public interface IMtoProcessor
    {
        /// <summary>
        /// Method to execute the processing of a message
        /// </summary>
        /// <param name="mto">The message that needs processing</param>
        /// <returns>True if the message was successfully processed</returns>
        void Run(MessageTransferObject mto);
    }
}
