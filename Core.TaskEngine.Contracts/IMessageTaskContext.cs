﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.TaskEngine.Mto;

namespace NavCanada.Core.TaskEngine.Contracts
{
    /// <summary>
    /// Context for objects used by tasks -- tasks act on the context properties and can set the context properties for the next task
    /// </summary>
    public interface IMessageTaskContext
    {
        /// <summary>
        /// Message Transfer Object
        /// </summary>
        MessageTransferObject Mto { get; set; }

        /// <summary>
        /// Access to all data source repositories
        /// </summary>
        IClusterStorage ClusterStorage { get; set; }

        /// <summary>
        /// Access to all external services
        /// </summary>
        IExternalServices ExternalServices { get; set; }

        /// <summary>
        /// used to pass the proposal between a set of tasks
        /// </summary>
        Proposal Proposal { get; set; }

        /// <summary>
        /// Settings that can be used for any task 
        /// </summary>
        MtoProcessorSettings MtoProcessorSettings { get; set; }

        /// This flag is only used by the SignalR taks that requires to skip the action based on a pre-existing condition
        /// By default this value is always false. 
        /// A signalR task should always execute the action except for a few cases where this flag is then set to true 
        /// to reject the action  
        bool DiscardSignalrNotification { get; set; }
        
        /// <summary>
        /// abstract object used to pass dynamic data between concurrent tasks
        /// </summary>
        object Data { get; set; }
    }
}
