﻿using NDS.Common.Contracts;
using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface ISolaceEndpoint
    {
        void PostToTopic(string topic, string content, IEnumerable<Property> properties);
        void TestConnection();
    }
}
