﻿namespace NavCanada.Utils.Apps.Aimls
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Security.Cryptography.X509Certificates;
    using System.ServiceModel;
    using System.ServiceModel.Configuration;
    using System.Windows.Forms;

    using log4net;

    using Core.Common.Contracts;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;
    using Core.Proxies.AimslProxy;

    public partial class Form1 : Form
    {
        readonly IAimslProxy _proxy = null;
        readonly ILog _logger = LogManager.GetLogger(typeof(Form1));

        public Form1()
        {
            InitializeComponent();

            ClientSection clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
            ChannelEndpointElement endpoint = clientSection.Endpoints[0];

            EndpointIdentity identity = EndpointIdentity.CreateDnsIdentity(ConfigurationManager.AppSettings["AimslClientIdentity"]);
            EndpointAddress address = new EndpointAddress(endpoint.Address, identity);

            _proxy = new AimslProxy("INODPNotamServicePort", address, LoadCertificate());
        }

        private X509Certificate2 LoadCertificate()
        {
            var certificateName = ConfigurationManager.AppSettings["CertificateName"];
            var certificatePassword = ConfigurationManager.AppSettings["CertificatePassword"];

            if (string.IsNullOrEmpty(certificateName) || string.IsNullOrEmpty(certificatePassword))
                throw new NullReferenceException("Certificate Name and Certificate Password are both required");

            string certificateFile = string.Format(@"{0}\Certificates\{1}", AppDomain.CurrentDomain.BaseDirectory, certificateName);
            if (!File.Exists(certificateFile))
                certificateFile = string.Format(@"Certificates\{0}", certificateName);

            if (!File.Exists(certificateFile))
            {
                _logger.Error("Could load AIMSL Certificate file from :" + certificateFile);
            }

            return new X509Certificate2(certificateFile, certificatePassword);
        }

        private void btnSubmitProposal_Click(object sender, EventArgs e)
        {
            var response = _proxy.Submit(new NotamProposal
            {
                Series = "A",
                Number = 1111,
                Nof = "CYHQ",
                Coordinates = "7300N08503W",
                StartValidity = DateTime.Now.AddDays(10),
                EndValidity = DateTime.Now.AddDays(20),
                ItemA = "CYAB",
                ItemE = textBox2.Text,
                NoteToNof = "This is my notes to nof",
                ReferredNumber = 1212,
                ReferredYear = 15,
                ReferredSeries = "A",
                Operator = "Omar",
                Code23 = "LP",
                Code45 = "AH",
                Fir = "CZEG",
                Lower = 0,
                Purpose = PurposeType.BO,
                Scope = ScopeType.A,
                Traffic = TrafficType.IV,
                Upper = 999,
                Radius = 5,
                Type = NotamType.N,
                Year = 15
            }, "Original User");
            if (response.Code == ApiStatusCode.Success)
            {
                textBox1.Text = response.ProposalStatus.ProposalId;
            }
            else
                MessageBox.Show("Error" + " " + response.ErrorMessage);
        }

        private void btnCheckPorposalStatus_Click(object sender, EventArgs e)
        {
            long proposalId;
            if (long.TryParse(textBox1.Text, out proposalId))
            {
                var response = _proxy.CheckStatus(proposalId);
                if (response.Code == ApiStatusCode.Success)
                {
                    MessageBox.Show(response.ProposalStatus.ProposalId);
                }
                else
                    MessageBox.Show("Error" + " " + response.ErrorMessage);
            }
            else
                MessageBox.Show("Error: Notam Proposal Id required");
        }

        private void btnWidthdrawPorposalStatus_Click(object sender, EventArgs e)
        {
            long proposalId;
            if (long.TryParse(textBox1.Text, out proposalId))
            {
                var response = _proxy.Widthraw(proposalId);
                if (response.Code == ApiStatusCode.Success)
                {
                    MessageBox.Show("Succeded");
                }
                else
                {
                    MessageBox.Show("Error" + " " + response.ErrorMessage);
                }
                textBox1.Text = "";
                textBox2.Text = "";
            }
            else
                MessageBox.Show("Error: Notam Proposal Id required");
        }
    }
}
